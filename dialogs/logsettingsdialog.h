#ifndef LOGSETTINGS_DIALOG_H
#define LOGSETTINGS_DIALOG_H

#include <QDialog>
#include <QDialogButtonBox>

#include <QButtonGroup>
#include <QCheckBox>
#include <QComboBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QProcess>
#include <QPushButton>
#include <QRadioButton>
#include <QSettings>
#include <QSpinBox>
#include <QToolBox>
#include <QTabWidget>
#include <QWidget>

#include "../logger.h"
#include "../utils.hpp"

struct IndividualLogSettings {
    QString everyEventLogPath;
    QString sumOfEventsLogPath;

    bool saveEveryEventLog;
    bool saveSumOfEventsLog;

    //! For each sumInterval minutes, sum op the number of entries of the everyEventLog
    int sumInterval;

    bool saveFrameOfEvent;
    QString saveFrameOfEventPath;
};

class IndividualLogSettingsDialog : public QWidget
{
    Q_OBJECT;

public:
    IndividualLogSettingsDialog(QWidget* parent,
        const QString& configurationPath,
        const QString& detectorIdText,
        const IndividualLogSettings& settings);

    IndividualLogSettings getSettings();
    void setSettings(IndividualLogSettings settings);

    private slots:

    void logEveryEventGroupBoxChecked();
	void saveFrameGroupBoxChecked();
    void everyEventButtonClicked();
    void saveFrameButtonClicked();
    void logSumButtonClicked();

private:
    QGroupBox* logEveryEventGroupBox;
    QLineEdit* everyEventPath;
    QPushButton* everyEventButton;

    QGroupBox* saveFrameOfEventGroupBox;
    QLineEdit* saveFramePath;
    QPushButton* saveFrameButton;

    QGroupBox* logSumOfEventsGroupBox;
    QLineEdit* logSumPath;
    QPushButton* logSumButton;
    QSpinBox* logSumIntervalSpinBox;

    QString configurationPath;
    QString detectorIdText;
};

struct GeneralDetectorTimingSettings
{
	int consecutiveEventDelay;
	int discardEventsLessThan;
};

class GeneralDetectorTimingDialog : public QWidget
{
	Q_OBJECT;

public:
	GeneralDetectorTimingDialog(QWidget* parent,
		const QString& configurationPath,
		const QString& detectorIdText,
		const GeneralDetectorTimingSettings& settings);

	GeneralDetectorTimingSettings getSettings();
	void setSettings(const GeneralDetectorTimingSettings& settings);

	int getConsecutiveEventDelay();
	int getDiscardEventsLessThan();

	void setConsecutiveEventDelay(int value);
	void setDiscardEventsLessThan(int value);

private:
	QSpinBox* consecutiveEventDelaySpinBox;
	QSpinBox* discardEventsLessThanSpinBox;
	QFormLayout* generalTimingLayout;

	QString configurationPath;
	QString detectorIdText;
};

struct LogSettingsDisplayOptions {
    bool showTimingDetectionBox;
};

class LogSettingsDialog : public QWidget
{
	Q_OBJECT;

public:
    LogSettingsDialog(QWidget* parent, 
        LogSettingsDisplayOptions displayOptions,
        QString compoundDetectorIdText,
        QList<QString> detectorIdTexts,
        QString configurationPath);

    void loadFromFile(const cv::FileStorage& fs);
    void loadFromFile(const cv::FileStorage& fs,
        InitLogSettings& generalSettings,
        std::map<QString, InitLogSettings>& individualDetectorSettings);

    void saveToFile(cv::FileStorage& fs);
    static void saveToFile(cv::FileStorage& fs,
        const InitLogSettings& generalSettings,
        const std::map<QString, InitLogSettings>& individualDetectorSettings);

    void getLogSettings(InitLogSettings &generalSettings,
        std::map<QString, InitLogSettings>& individualDetectorSettings);
    void setLogSettings(const InitLogSettings &generalSettings,
        const std::map<QString, InitLogSettings>& individualDetectorSettings);

	QLayout* getTimingLayout() { return this->timingLayout; };
	QLayout* getLogsLayout() { return this->logsLayout; };

    int getMaxTriggeredDuration(const QString& detectorIdText);
    void setMaxTriggeredDuration(const QString& detectorIdText, const int maxActiveDuration);

    private slots:

    void enableIndividualLogFilesChecked();
	void individualGeneralTimingSwitchChecked(bool transferValues = true);
	void timingRadioToggled();

private:
    void transferIndividualLogSettings(InitLogSettings &generalSettings,
        const IndividualLogSettings& individualSettings);
    void transferIndividualLogSettings(IndividualLogSettings& individualSettings,
        const InitLogSettings& generalSettings);

    static void saveEntrySettings(cv::FileStorage& fs,
        const InitEntrySettings& entrySettings,
        const std::string& namePrefix);

    void loadEntrySettings(const cv::FileStorage& fs,
        InitEntrySettings& entrySettings,
        const std::string& namePrefix);



	// General timing settings
	QVBoxLayout* timingLayout;
	QCheckBox* enableIndividualGeneralTiming;
	QTabWidget* individualGeneralTimingSwitch;
	std::map<QString, GeneralDetectorTimingDialog*> individualGeneralTimingSettings;

    // Timing and detection of events
    QVBoxLayout* eventDetectionLayout;
    std::vector<std::pair<QString, QButtonGroup*> > detectorTransitions;    
    std::map<QString, QSpinBox*> maximumTriggeredDelays;
	QSpinBox* overlayTimingSpinBox;

	QRadioButton* useIntervalTimingRadioButton;
	QRadioButton* useOverlapTimingRadioButton;

	QGroupBox* enteringLeavingGroupBox;
	QGroupBox* maximumDelayGroupBox;
	QGroupBox* overlayTimingGroupBox;

    // Log files
	QVBoxLayout* logsLayout;
    QCheckBox* enableIndividualLogFiles;
    QTabWidget* logFilesSwitch;

    std::map<QString, IndividualLogSettingsDialog*> individualLogSettings;

    QString configurationPath;
    QList<QString> detectorIdTexts;
    QString compoundDetectorIdText;
    LogSettingsDisplayOptions displayOptions;


    std::map<QString, int> maxTriggeredDuration;

};

#endif // LOGSETTINGS_DIALOG_H
