#ifndef EXCLUSIVEMODULE_H
#define EXCLUSIVEMODULE_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "dialogs/detectortypeselectiondialog.h"
#include "abstractcompounddetector.h"

class ruba::ExclusiveModule : public AbstractCompoundDetector
{

public:
	explicit ExclusiveModule(BaseDetectorParams baseParams, QWidget *parent = 0, QString configFile = "");

	ruba::DetectorOutput GetOutput();

private:
	void setDetectorSpecificDefaultParameters();

	QList<QString> getDetectorSpecificExtendedDebugInfo();
	void updateDetectorSpecificLogic();

	QVector<int> chosenDetectors;

};

#endif // EXCLUSIVEMODULE_H
