#ifndef VIDEOHANDLER_H
#define VIDEOHANDLER_H

#include <QDateTime>
#include <QFileInfo>
#include <QFileDialog>
#include <QList>
#include <QInputDialog>
#include <QRegExp>
#include <QStandardItemModel>

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <memory>

#include "dialogs/jumptoframedialog.h"

#include "framestruct.h"
#include "ruba.h"
#include "rubabaseobject.h"
#include "synchronisedvideocapture.h"
#include "utils.hpp"
#include "viewinfo.h"

class VideoSequenceWriter
{
public:
	VideoSequenceWriter(const QList<ViewInfo>& views, const QString& saveDirectory,
		double fps, bool createOneVideoPerSequence = false, int maxConsecutiveTimeDifferenceInMillisec = 1000);

	void startRecording();
	void stopRecording();
	void pauseRecording();


	bool isRecording() { return recording; };

	bool update(const std::vector<ruba::FrameStruct>& frames, QString& message);

private:
	QDateTime previousTimestamp;
	QDateTime videoStart;
	int maxConsecutiveTimeDifferenceInMillisec;
	QList<qint64> sequenceFrameCount;

	QList<ViewInfo> views;
	QString saveDirectory;
	bool createOneVideoPerSequence;

	std::vector<cv::VideoWriter> vWriters;

	double fps;
	bool recording;
};

class ruba::VideoHandler : public RubaBaseObject
{
	Q_OBJECT

public:
    VideoHandler(QWidget *parent, QList<ViewInfo> views, bool persistentPlayback = false);

    int loadFromFile(cv::FileStorage& fs);
    int saveToFile(cv::FileStorage& fs, const std::string& entryPrefix);

    virtual int viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews);

    std::vector<ruba::FrameStruct> getCurrentFrame();
	std::vector<ruba::FrameStruct> getNextFrame();

    qint64 getFrameNbr() const;
	QDateTime getCurrentTime();


    double getFrameRate() const;
    QStandardItemModel* getVideoListModel() { return &videoListModel; }
    QList<VideoResolutionProperties> getVideoResolution() const;

    QString setVideoFiles(QString homedir = ".", VideoSyncType videoSyncType = VideoSyncType::STARTTIMEINFILENAME);
    QString addVideoFiles(QString homedir = ".", VideoSyncType videoSyncType = VideoSyncType::STARTTIMEINFILENAME);
	void addVideoFiles(const std::vector<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture>>& newVideos);
	void getVideoFiles(std::vector<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture>>& videos);

	CaptureStatus getCaptureStatus();
	bool isExternalVideoListChanged();
    
    void removeVideoFiles(QList<int> indices);
    void moveItem(int pos, int newPos);

    int setCurrentVideo(int index);
    int getCurrentFileIndex() const { return currentVideoListPos; }

    void showVideoInformation(int index);

    void play();
    void pause();

    void Release();

    int jumpToPrevFrame(int prevFrameCount);

    int jumpToFrame();
    int jumpToFrame(QDateTime requestedTime);
    int jumpToFrame(int requestedFrameNumber);

    bool getIsInitialised() const;
    
signals:
	void sendMessage(const QString& title, const QString& message, MessageType messageType);
	void sendProgress(int value, int maxValue, const QString& message);

private:
    int setPausedIcon(int index);
    int setPlayingIcon(int index);
    int getSupportedVideoFileTypesText(QString &filterText);
    QString addVideoFiles(QString homedir, bool clearExistingList, int insertionIndex, VideoSyncType videoSyncType);

    int addVideoFilesWithStartTimeInFilename(QString &homedir, bool clearExistingList, int insertionIndex);
    int addVideoFilesWithTimestampsInFile(QString &homedir, bool clearExistingList, int insertionIndex);
	int addVideoStream(QString &homedir, bool clearExistingList, int insertionIndex);

    int getCorrespondingVideoFilesForView(
        QList<InitFileProperties> const &view1Files,
        ViewInfo const &correspondingView,
        QList<InitFileProperties> &correspondingFiles);

    int getCorrespondingVideoFilesForView(
        QList<InitFilePropertiesTimestamps> const &view1Files,
        ViewInfo const &correspondingView,
        QList<InitFilePropertiesTimestamps> &correspondingFilesTimestamps);

	int insertElementInExternalVideoList(int insertionIndex, QDateTime startTime,
		QList<ViewInfo> views, QString alternativeText = QString());

    int findSimilarFilenames(const QString& filename, const ViewInfo currentView,
        QList<ViewInfo>& correspondingViews, QList<QString>& correspondingFilenames);

    int findCorrespondingTimestampsFile(const QString & filename,
        QString &timestampsFile);

    bool checkResolution(cv::Size resolution);

    void checkFrames(std::vector<ruba::FrameStruct>& frames, CaptureStatus& status);

    // Internal representation of the video list
    QList<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture>> internalVideoList;
    int currentVideoListPos;

    // External representation of the video list for MainWindow
    QStandardItemModel videoListModel;

    // Internal list for issuing warnings for videos with low resolution
    QList<VideoResolutionProperties> lowResVideos;

    CaptureStatus prevStatus;
    bool viewErrorShown;
	bool externalVideoListChanged;
	bool persistentPlayback;
};

#endif // VIDEOHANDLER_H
