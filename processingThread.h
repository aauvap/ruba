#ifndef PROCESSINGTHREAD_H
#define PROCESSINGTHREAD_H

#include <memory>

#include <QThread>
#include <QProgressDialog>

#include <opencv2/opencv.hpp>

#include "ruba.h"
#include "detectorhandler.h"
#include "videohandler.h"
#include "rubabaseobject.h"

enum RecordingState {
	NOTINITIALISED,
	PAUSED,
	RECORDING
};


class ProcessingThread : public QThread
{
	Q_OBJECT

public:
	ProcessingThread(std::shared_ptr<ruba::VideoHandler> vHandler, 
		std::shared_ptr<ruba::DetectorHandler> dHandler, 
		QObject *parent = 0);
	~ProcessingThread();

	void startProcessing();

	std::shared_ptr<ruba::DetectorHandler> getDetectorHandler();
	std::shared_ptr<ruba::VideoHandler> getVideoHandler();
	bool getCurrentFrames(std::vector<ruba::FrameStruct>& frames, CaptureStatus& status);
	bool processNextFrame(std::vector<ruba::FrameStruct>& frames);
	void unlockDisplayFrames();
	bool getDetectorCharts(std::vector<ruba::ChartStruct>& charts);

	bool pause();
	

	void releaseVideo();
	double getProcessingSpeed() { return currentFps; }

	RecordingState getRecordingState();
	void startRecording(QString videoPath);
	void resumeRecording();
	void pauseRecording();
	void stopRecording();

	void setTargetFps(int targetFps);

public slots:
	void jumpToFrame(QDateTime requestedTime);

signals:
	void sendMessage(const QString& title, const QString& message, MessageType messageType);
	void sendProgress(int value, int maxValue, const QString& message);

protected:
	void run() override;

private:
	double computeElapsedInfo(qint64& elapsed);
	double average(const std::list<double>& list);
	void updateFromFrame(std::vector<ruba::FrameStruct> &updateStructs);

	QMutex mutex;
	QMutex displayFramesMutex;
	QMutex detectorChartsMutex;
	QMutex detectorInfoMutex;
	QMutex delayMutex;

	bool abort;
	
	// Video Handler for video and time management
	std::shared_ptr<ruba::VideoHandler> VHandler;

	// Detector Handler
	std::shared_ptr<ruba::DetectorHandler> DHandler;

    std::shared_ptr<VideoSequenceWriter> vSequenceWriter;

	QTimer ProcessTimer;
	QElapsedTimer elapsedTimer;
	int elapsedCount;
	qint64 elapsedMsCount;

	int delay;
	int theoreticalDelay;
	int remainderDelay;
	int remainderInterval;

	std::list<double> historicalDelays;
	std::list<double> historicalRemainderDelays;
	

	// Counter to manage the number of potentially skipped frames
	int framesSkipped;
	int targetFps;
	int currentFps;

	QObject* parent;
	std::vector<ruba::FrameStruct> displayFrames;

	bool enableDiagnostics;
	QFile diagnostiscs;
	QTextStream* diagnosticsStream;
};

#endif // PROCESSINGTHREAD_H
