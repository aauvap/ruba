#include "utils.hpp"

QString Utils::GetModalityName(int modality)
{
    QString outputModality;

    if (modality == RGB)
    {
        outputModality = "RGB";
    } else if (modality == THERMAL)
    {
        outputModality = "Thermal";
    }

    return outputModality;

}

QString Utils::getTLStateName(tlTypeClasses type)
{
    QString stateName;

    switch (type)
    {
    case tlTypeClasses::RED:
    {
        stateName = "Red";
        break;
    }
    case tlTypeClasses::YELLOW:
    {
        stateName = "Yellow";
        break;
    }
    case tlTypeClasses::GREEN:
    {
        stateName = "Green";
        break;
    }
    case tlTypeClasses::REDYELLOW:
    {
        stateName = "RedYellow";
        break;
    }
    case tlTypeClasses::AMBIGIOUS:
    {
        stateName = "Ambiguous";
        break;
    }
    default:
        stateName = "Ambiguous";
    }

    return stateName;

}

QString Utils::getDetectorName(int detectorType, bool addDetectorPostfix)
{
    QString detectorName = "Undefined";

    switch (detectorType)
    {
    case PRESENCEDETECTOR:
        detectorName = QObject::tr("Presence");
        break;
    case MOVEMENTDETECTOR:
        detectorName = QObject::tr("Movement");
        break;
    case STATIONARYDETECTOR:
        detectorName = QObject::tr("Stationary");
        break;
    case TRAFFICLIGHTDETECTOR:
        detectorName = QObject::tr("Traffic Light");
        break;
    case DETECTORE:
        detectorName = QObject::tr("DETECTOR E");
        break;
    case RIGHTTURNINGCAR:
        detectorName = QObject::tr("Right Turning Car");
        break;
    case STRAIGHTGOINGCYCLIST:
        detectorName = QObject::tr("Straight Going Cyclist");
        break;
    case LEFTTURNINGCAR:
        detectorName = QObject::tr("Left Turning Car");
        break;
    case INTERACTIONDETECTOR:
        detectorName = QObject::tr("312 Interaction");
        break;
    case INTERACTION410DETECTOR:
        detectorName = QObject::tr("410 Interaction");
        break;
    case SINGLEMODULE:
        detectorName = QObject::tr("Single Module");
        break;
    case DOUBLEMODULE:
        detectorName = QObject::tr("Double Module");
        break;
    case GROUNDTRUTHANNOTATOR:
        return QObject::tr("Ground Truth Annotator");
        break;
	case LOGFILEREVIEWER:
		return QObject::tr("Log File Reviewer");
		break;
	case EXCLUSIVEMODULE:
		detectorName = QObject::tr("Exclusive Module");
		break;
    }

    return addDetectorPostfix ? detectorName.append(QObject::tr(" Detector")) : detectorName;
}

QString Utils::getDetectorAbbreviation(int detectorType)
{
    QString detectorAbbrev = "Undefined";

    switch (detectorType)
    {
    case PRESENCEDETECTOR:
        detectorAbbrev = "P";
        break;
    case MOVEMENTDETECTOR:
        detectorAbbrev = "M";
        break;
    case STATIONARYDETECTOR:
        detectorAbbrev = "S";
        break;
    case TRAFFICLIGHTDETECTOR:
        detectorAbbrev = "TL";
        break;
    case DETECTORE:
        detectorAbbrev = "DETECTORE";
        break;
    case RIGHTTURNINGCAR:
        detectorAbbrev = "RTC";
        break;
    case STRAIGHTGOINGCYCLIST:
        detectorAbbrev = "SGC";
        break;
    case LEFTTURNINGCAR:
        detectorAbbrev = "LTC";
        break;
    case INTERACTIONDETECTOR:
        detectorAbbrev = "312";
        break;
    case INTERACTION410DETECTOR:
        detectorAbbrev = "410";
        break;
    case SINGLEMODULE:
        detectorAbbrev = "SM";
        break;
    case DOUBLEMODULE:
        detectorAbbrev = "DM";
        break;
    case GROUNDTRUTHANNOTATOR:
        detectorAbbrev = "GT";
        break;
    case LOGFILEREVIEWER:
        detectorAbbrev = "LF";
        break;
	case EXCLUSIVEMODULE:
		detectorAbbrev = "XM";
    }

    return detectorAbbrev;
    
}

void Utils::showWarning(QWidget* parent, QString title, QString errorMessage)
{
    QMessageBox::warning(parent, title, errorMessage, QMessageBox::Ok);
}

void Utils::getWarningMessage(QWidget * parent, cv::Exception e, QString& title, QString& message)
{
    message = QString(QObject::tr("An unexpected error occurred.\n\nPlease report the bug by using the details below:\nError: %1\nError code: %2\nFunction: %3\nLine: %4\nMessage: "))
        .arg(QString::fromStdString(e.err))
        .arg(e.code)
        .arg(QString::fromStdString(e.func))
        .arg(e.line)
        .arg(QString::fromStdString(e.msg));

	title = QObject::tr("An error occurred");
}

int Utils::getTotalModalityCount()
{
    return IMAGEMODALITIES_NR_ITEMS;
}

QString Utils::getOrdinalSuffix(const int number)
{
    QString suffix;
    
    switch (number)
    {
    case 1:
        suffix = "st";
        break;
    case 2:
        suffix = "nd";
        break;
    case 3:
        suffix = "rd";
        break;
    default:
        suffix = "th";
    }

    return suffix;
}

QList<QString> Utils::readQStringListFromFileStorage(const cv::FileStorage& fs, std::string node)
{
    QList<QString> stringList;
    
    cv::FileNode filenameFN = fs[node];
    cv::FileNodeIterator it = filenameFN.begin(), itEnd = filenameFN.end();

    // Iterate through the vector using FileNodeIterator
    for (; it != itEnd; ++it)
    {
        std::string entry = (std::string)(*it);
        stringList.append(QString::fromStdString(entry));
    }

    return stringList;
}

QString Utils::GetModalityFileSuffix(int modality)
{
    switch (modality)
    {
    case RGB:
        return "cam1";
    case THERMAL:
        return "cam2";
    }

    return "";
}

QString Utils::GetModalityFileFolder(int modality)
{
    switch (modality)
    {
    case RGB:
        return "rgb";
    case THERMAL:
        return "thermal";
    }

    return "";
}

bool Utils::checkIsModalityValid(ImageModalities modality)
{
    if (modality >= IMAGEMODALITIES_NR_ITEMS || modality < 0)
    {
        // The modality is definitely out of range.
        return false;
    }

    return true;
}


bool checkIsModalityValid(int modality)
{
    ImageModalities cModality = static_cast<ImageModalities>(modality);

    if (modality >= IMAGEMODALITIES_NR_ITEMS || modality < 0)
    {
        // The modality is definely out of range.
        return false;
    }

    return true;
}

cv::Size Utils::getResolution(QString filename)
{
    cv::Size s;

    // Get the video filename of the requested modality
    cv::VideoCapture cap(filename.toStdString());

    // Detect size of the input video
    s.height = cap.get(cv::CAP_PROP_FRAME_HEIGHT);
    s.width = cap.get(cv::CAP_PROP_FRAME_WIDTH);

    return s;
}

QModelIndex Utils::getAncestor(const QModelIndex index)
{
    // Check if parent exists
    QModelIndex child = index;
    QModelIndex parent = index;

    while (child.parent().isValid())
    {
        parent = child.parent(); // The parent has been verified - save it for now

        child = child.parent(); // Test if parent has parents next time while is run
    }

    return parent;
}