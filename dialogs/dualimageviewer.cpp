#include "dualimageviewer.h"
#include "ui_dualimageviewer.h"

DualImageViewer::DualImageViewer(QWidget *parent, cv::Mat img1, cv::Mat img2) :
    QDialog(parent),
    ui(new Ui::DualImageViewer)
{
    ui->setupUi(this);
    QPalette Pal = this->palette();
    Pal.setColor(QPalette::Background, Qt::white);
    this->ui->DecisionText1->setAutoFillBackground(true);
    this->ui->DecisionText1->setPalette(Pal);
    this->setAutoFillBackground(true);
    this->setPalette(Pal);

    cv::Mat rgbImg1, rgbImg2;
    cv::cvtColor(img1, rgbImg1, cv::COLOR_BGR2RGB);
    cv::cvtColor(img2, rgbImg2, cv::COLOR_BGR2RGB);

    this->ui->ImageView1->setPixmap(QPixmap::fromImage(QImage((unsigned char*) rgbImg1.data, rgbImg1.cols, rgbImg1.rows, QImage::Format_RGB888)));
    this->ui->ImageView2->setPixmap(QPixmap::fromImage(QImage((unsigned char*) rgbImg2.data, rgbImg2.cols, rgbImg2.rows, QImage::Format_RGB888)));

    this->ui->verticalLayout_3->setContentsMargins(11,11,11,11);
}

DualImageViewer::~DualImageViewer()
{
    delete ui;
}

void DualImageViewer::SetDecisionText(QString text1, QString text2)
{
    this->ui->DecisionText1->setText(text1);
    this->ui->DecisionText2->setText(text2);
}

void DualImageViewer::SetImageCaption(QString cap1, QString cap2)
{
    this->ui->ImageCaption1->setText(cap1);
    this->ui->ImageCaption2->setText(cap2);
}

void DualImageViewer::SetButtonText(QString text1, QString text2)
{
    this->ui->ImageButton1->setText(text1);
    this->ui->ImageButton2->setText(text2);
}

void DualImageViewer::on_ImageButton1_clicked()
{
    this->accept();
}

void DualImageViewer::on_ImageButton2_clicked()
{
    this->reject();
}
