#ifndef JUMPTOFRAMEDIALOG_H
#define JUMPTOFRAMEDIALOG_H

#include <QtWidgets>
#include <QDateTime>

namespace Ui {
class jumptoframedialog;
}

class jumptoframedialog : public QDialog
{
    Q_OBJECT

public:
    explicit jumptoframedialog(QWidget *parent, QDateTime defaultDateTime);

    QDateTime GetOutput();

private:
    QLabel *descriptionLabel;
    QDateTimeEdit *dateTimeEdit;

    QDialogButtonBox *buttonBox;

};

#endif // JUMPTOFRAMEDIALOG_H
