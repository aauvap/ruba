#include "movementdetector.h"
#include <QDebug>

using namespace cv;
using namespace std;
using namespace ruba;


MovementDetector::MovementDetector(BaseDetectorParams baseParams, std::vector<Mat> masks, std::vector<std::vector<double>> minVecLengthThreshold,
                           std::vector<std::vector<cv::Point>> flowDirectionRange,
                           std::vector<std::vector<int>> triggerThreshold, bool UseScrollingChart,
                           int upperThresholdBoundMultiplier)
    : AbstractMultiModalDetector(configureBaseDetectorParams(baseParams, MOVEMENTDETECTOR),
		initialiseChartColors())
{
    if (masks.size() != minVecLengthThreshold.size()
            || flowDirectionRange.size() != minVecLengthThreshold.size()
            || triggerThreshold.size() != minVecLengthThreshold.size())
    {
        IsInitialized = false;
        return;
    }

    for (size_t i = 0; i < masks.size(); ++i)
    {
        BaseDetectorParams newBaseParams;
        newBaseParams.detectorID = 100 + i;
        newBaseParams.detectorType = MOVEMENTDETECTOR;
        newBaseParams.initFrames = vector<FrameStruct>(1, baseParams.initFrames[i]);

        shared_ptr<MovementDetectorSingleModal> d = make_shared<MovementDetectorSingleModal>(newBaseParams, masks[i], minVecLengthThreshold[i], flowDirectionRange[i],
                                                                 triggerThreshold[i], UseScrollingChart, 
                                                                 upperThresholdBoundMultiplier);
        MDetectors.push_back(d);
        singleModalDetectors.push_back(d);
    }

    this->UseScrollingChart = UseScrollingChart;
    this->confidence.resize(1);
    this->IsTriggered.resize(1);
    this->lastChange.resize(1);
    IS_FIRST_RUN = true;


    // Initialize the confidence, flags and last change vectors
    if (!flowDirectionRange.empty())
    {
        for (size_t i = 0; i < flowDirectionRange[0].size(); ++i)
        {
            this->confidence.push_back(0);
            this->IsTriggered.push_back(false);
            this->lastChange.push_back(QDateTime());
        }
    } else {
        IsInitialized = false;
        return;
    }

    initTriggeredHistory(int(minVecLengthThreshold.size()));

}


void MovementDetector::SetMinVecLengthThreshold(vector<vector<double>> minVecLengthThreshold)
{
    for (size_t i = 0; i < MDetectors.size(); ++i)
    {
        MDetectors[i]->SetMinVecLengthThreshold(minVecLengthThreshold[i]);
    }
}


void MovementDetector::SetFlowDirectionRange(vector<vector<cv::Point>> flowDirectionRange)
{
    for (size_t i = 0; i < MDetectors.size(); ++i)
    {
        MDetectors[i]->SetFlowDirectionRange(flowDirectionRange[i]);
    }
}


void MovementDetector::SetTriggerThreshold(vector<vector<int>> triggerThreshold)
{
    for (size_t i = 0; i < MDetectors.size(); ++i)
    {
        MDetectors[i]->SetTriggerThreshold(triggerThreshold[i]);
    }
}


QList<QString> MovementDetector::getDetectorSpecificDebugInfo()
{
    debugInfo.clear();

    return this->debugInfo;
}

void MovementDetector::InitDetector()
{
    for (size_t i = 0; i < MDetectors.size(); ++i)
    {
        MDetectors[i]->InitDetector();
    }
}

ChartColors ruba::MovementDetector::initialiseChartColors()
{
	ChartColors chartColors;
	chartColors.backgroundColors.enabledTriggeredColor = cv::Scalar(200, 200, 255);
	chartColors.backgroundColors.enabledNotTriggeredColor = cv::Scalar(220, 220, 255);
	chartColors.backgroundColors.notEnabledColor = cv::Scalar::all(255);

	chartColors.lineUnderColors.enabledTriggeredColor = cv::Scalar(100, 100, 255);
	chartColors.lineUnderColors.enabledNotTriggeredColor = cv::Scalar(100, 100, 255);
	chartColors.lineUnderColors.notEnabledColor = cv::Scalar::all(255);

	chartColors.lineOverColors.enabledTriggeredColor = cv::Scalar(0, 0, 220);
	chartColors.lineOverColors.enabledNotTriggeredColor = cv::Scalar(100, 100, 255);
	chartColors.lineOverColors.notEnabledColor = cv::Scalar::all(255);

	return chartColors;
}

std::vector<QString*> MovementDetector::GetChartTitles()
{
    if (UseScrollingChart)
    {
        std::vector<QString*> chartTitles;
        std::vector<QString*> tmpTitles;

        for (size_t i = 0; i < MDetectors.size(); ++i)
        {
            tmpTitles = MDetectors[i]->GetChartTitles();

            for (size_t j = 0; j < tmpTitles.size(); ++j)
            {
                chartTitles.push_back(tmpTitles[j]);
            }
        }

        return tmpTitles;

    }
    else
    {
        vector<QString*> tmpTitles;
        tmpTitles.push_back(NULL);

        return tmpTitles;
    }
}

