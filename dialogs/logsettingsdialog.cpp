#include "logsettingsdialog.h"

LogSettingsDialog::LogSettingsDialog(QWidget* parent,
    LogSettingsDisplayOptions displayOptions,
    QString compoundDetectorIdText,
    QList<QString> detectorIdTexts,
    QString configurationPath)
	: QWidget(parent)
{
    this->configurationPath = configurationPath;
    this->detectorIdTexts = detectorIdTexts;
    this->compoundDetectorIdText = compoundDetectorIdText;
    this->displayOptions = displayOptions;
	
    // General timing
    QGroupBox *generalTimingGroup = new QGroupBox(tr("General detector timing"));
	QVBoxLayout* generalTimingLayout = new QVBoxLayout();

	GeneralDetectorTimingSettings generalTimingSettings;

	if (detectorIdTexts.size() > 1)
	{
		enableIndividualGeneralTiming = new QCheckBox(tr("Enable individual detector timing settings"));
		generalTimingLayout->addWidget(enableIndividualGeneralTiming);

		connect(enableIndividualGeneralTiming,
			SIGNAL(clicked(bool)),
			this, SLOT(individualGeneralTimingSwitchChecked()));
	}
	else {
		enableIndividualGeneralTiming = nullptr;
	}

	individualGeneralTimingSwitch = new QTabWidget();
	individualGeneralTimingSwitch->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum));

	individualGeneralTimingSettings["General"] = new GeneralDetectorTimingDialog(nullptr,
		configurationPath, "", generalTimingSettings);
	individualGeneralTimingSwitch->addTab(individualGeneralTimingSettings["General"], tr("General"));

	// Add individual log settings to the map, but don't include them in the tabWidget 
	// until the user checks the enableIndividualLogFiles CheckBox
	for (auto detectorIdText : detectorIdTexts)
	{
		individualGeneralTimingSettings[detectorIdText] = new GeneralDetectorTimingDialog(nullptr,
			configurationPath, detectorIdText, generalTimingSettings);
	}

	generalTimingLayout->addWidget(individualGeneralTimingSwitch);
    generalTimingGroup->setLayout(generalTimingLayout);


    // Timing and detection of events
    QGroupBox* eventDetectionGroupBox = new QGroupBox(tr("Timing between detectors"));
    eventDetectionLayout = new QVBoxLayout();

	useIntervalTimingRadioButton = new QRadioButton(tr("Use interval timing"));
	useOverlapTimingRadioButton = new QRadioButton(tr("Use overlap timing"));
	eventDetectionLayout->addWidget(useIntervalTimingRadioButton);
	eventDetectionLayout->addWidget(useOverlapTimingRadioButton);

	connect(useIntervalTimingRadioButton,
		SIGNAL(toggled(bool)),
		this, SLOT(timingRadioToggled()));
	
    enteringLeavingGroupBox = new QGroupBox(tr("Start and end points of timing interval"));
    QGridLayout* enteringLeavingLayout = new QGridLayout();
    enteringLeavingLayout->addWidget(new QLabel(tr("Entering")), 0, 1, Qt::AlignHCenter);
    enteringLeavingLayout->addWidget(new QLabel(tr("Leaving")), 0, 2, Qt::AlignHCenter);
    enteringLeavingLayout->addWidget(new QLabel(tr("First triggered detector")), 1, 0);
    enteringLeavingLayout->addWidget(new QLabel(tr("Last triggered detector")), 2, 0);

    for (auto i = 0; i < detectorIdTexts.size(); ++i)
    {
        QButtonGroup* detectorTransition = new QButtonGroup();
        detectorTransition->addButton(new QRadioButton(), Transition::ENTERING);
        detectorTransition->addButton(new QRadioButton(), Transition::LEAVING);
        detectorTransition->button(Transition::ENTERING)->setChecked(true);

        enteringLeavingLayout->addWidget(detectorTransition->button(Transition::ENTERING), i + 1, 1, Qt::AlignHCenter);
        enteringLeavingLayout->addWidget(detectorTransition->button(Transition::LEAVING), i + 1, 2, Qt::AlignHCenter);
        detectorTransitions.push_back(std::make_pair(detectorIdTexts[i], detectorTransition));
    }

    enteringLeavingLayout->setColumnStretch(0, 2); // Make the first column stretch twice as much as the other columns
    enteringLeavingLayout->setColumnStretch(1, 1);
    enteringLeavingLayout->setColumnStretch(2, 1);
    enteringLeavingGroupBox->setLayout(enteringLeavingLayout);

    eventDetectionLayout->addWidget(enteringLeavingGroupBox);
	enteringLeavingGroupBox->hide();
	int maximumTimingWidth = enteringLeavingGroupBox->baseSize().width();

    maximumDelayGroupBox = new QGroupBox(tr("Maximum time gaps"));
    QFormLayout* maximumDelayLayout = new QFormLayout();

    for (auto detectorIdText : detectorIdTexts)
    {
        maximumTriggeredDelays[detectorIdText] = new QSpinBox();
        maximumTriggeredDelays[detectorIdText]->setMaximum(9999999);
        maximumTriggeredDelays[detectorIdText]->setSingleStep(50);
        maximumTriggeredDelays[detectorIdText]->setSpecialValueText(tr("Disabled"));
        maximumTriggeredDelays[detectorIdText]->setSuffix(" ms");

        QLabel* delayLabel = new QLabel(tr("Maximum time gap if %1 is triggered first").
            arg(detectorIdText));
        delayLabel->setWordWrap(true);
        maximumDelayLayout->addRow(delayLabel, maximumTriggeredDelays[detectorIdText]);
    }
    maximumDelayGroupBox->setLayout(maximumDelayLayout);
    eventDetectionLayout->addWidget(maximumDelayGroupBox);
	maximumDelayGroupBox->hide();

	if (maximumDelayGroupBox->baseSize().width() > maximumTimingWidth)
	{
		maximumTimingWidth = maximumDelayGroupBox->baseSize().width();
	}

	overlayTimingGroupBox = new QGroupBox(tr("Overlap between detectors"));
	QFormLayout* overlayTimingLayout = new QFormLayout();

	overlayTimingSpinBox = new QSpinBox();
	overlayTimingSpinBox->setMaximum(9999999);
	overlayTimingSpinBox->setSpecialValueText(tr("None"));
	overlayTimingSpinBox->setSingleStep(50);
	overlayTimingSpinBox->setSuffix(tr(" ms"));
	overlayTimingLayout->addRow(tr("Additional overlap buffer"), overlayTimingSpinBox);

	overlayTimingGroupBox->setLayout(overlayTimingLayout);
	eventDetectionLayout->addWidget(overlayTimingGroupBox);
	overlayTimingGroupBox->hide();

	if (overlayTimingGroupBox->baseSize().width() > maximumTimingWidth)
	{
		maximumTimingWidth = overlayTimingGroupBox->baseSize().width();
	}

	eventDetectionLayout->addItem(new QSpacerItem(maximumTimingWidth, 0, QSizePolicy::Minimum, QSizePolicy::Maximum));

    eventDetectionGroupBox->setLayout(eventDetectionLayout);

	// Overall timing layout
	timingLayout = new QVBoxLayout();
	timingLayout->addWidget(generalTimingGroup);

	if (displayOptions.showTimingDetectionBox && (detectorIdTexts.size() > 1))
	{
		timingLayout->addWidget(eventDetectionGroupBox);
	}
	
	timingLayout->addStretch();
	timingLayout->setContentsMargins(0, 0, 0, 0);

    // Log files
    logsLayout = new QVBoxLayout();

    if (detectorIdTexts.size() > 1)
    {    
        enableIndividualLogFiles = new QCheckBox(tr("Enable log files for individual detectors"));
		logsLayout->addWidget(enableIndividualLogFiles);

        connect(enableIndividualLogFiles,
            SIGNAL(clicked(bool)),
            this, SLOT(enableIndividualLogFilesChecked()));
    }
    else {
        enableIndividualLogFiles = nullptr;
    }

    logFilesSwitch = new QTabWidget();

    individualLogSettings["General"] = new IndividualLogSettingsDialog(this,
        configurationPath, "", IndividualLogSettings());
    logFilesSwitch->addTab(individualLogSettings["General"], tr("General"));

    // Add individual log settings to the map, but don't include them in the tabWidget 
    // until the user checks the enableIndividualLogFiles CheckBox
    for (auto detectorIdText : detectorIdTexts)
    {
        individualLogSettings[detectorIdText] = new IndividualLogSettingsDialog(this,
            configurationPath, detectorIdText, IndividualLogSettings());
    }

	logsLayout->addWidget(logFilesSwitch);
	logsLayout->addStretch();

	useIntervalTimingRadioButton->setChecked(true);

	setLayout(new QVBoxLayout());

}

void LogSettingsDialog::loadFromFile(const cv::FileStorage & fs)
{
    InitLogSettings generalSettings;
    std::map<QString, InitLogSettings> individualDetectorSettings;

    loadFromFile(fs, generalSettings, individualDetectorSettings);
    setLogSettings(generalSettings, individualDetectorSettings);
}

void LogSettingsDialog::loadFromFile(const cv::FileStorage & fs, InitLogSettings & generalSettings, 
    std::map<QString, InitLogSettings>& individualDetectorSettings)
{
    // Load general settings
    fs["EnableEveryEventLog"] >> generalSettings.saveEveryEventLog;
    fs["EnableSumOfEventsLog"] >> generalSettings.saveSumOfEventsLog;
    generalSettings.everyEventLogPath = QString::fromStdString(fs["LogEveryPath"]);
    generalSettings.sumOfEventsLogPath = QString::fromStdString(fs["LogSumPath"]);
    fs["LogSumInterval"] >> generalSettings.sumInterval;
    generalSettings.saveFrameOfEventPath = QString::fromStdString(fs["SaveFrameOfEventPath"]);

    // Load the entry settings of the general settings
    loadEntrySettings(fs, generalSettings.entrySettings, "Log-");

    // Load the individual detector log settings
    individualDetectorSettings.clear();

    QList<QString> individualDetectorNames = 
        Utils::readQStringListFromFileStorage(fs, "Log-individualDetectorNames");
  
    for (auto detectorName : individualDetectorNames)
    {
        std::string baseName = detectorName.toStdString();

        fs[baseName + "-EnableEveryEventLog"] >> individualDetectorSettings[detectorName].saveEveryEventLog;
        fs[baseName + "-EnableSumOfEventsLog"] >> individualDetectorSettings[detectorName].saveSumOfEventsLog;
        individualDetectorSettings[detectorName].everyEventLogPath = QString::fromStdString(fs[baseName + "-LogEveryPath"]);
        individualDetectorSettings[detectorName].sumOfEventsLogPath = QString::fromStdString(fs[baseName + "-LogSumPath"]);
        fs[baseName + "-LogSumInterval"] >> individualDetectorSettings[detectorName].sumInterval;
        individualDetectorSettings[detectorName].saveFrameOfEventPath = QString::fromStdString(fs[baseName + "-SaveFrameOfEventPath"]);

        loadEntrySettings(fs, individualDetectorSettings[detectorName].entrySettings, baseName + "-");
    }

}

void LogSettingsDialog::saveToFile(cv::FileStorage & fs)
{
    InitLogSettings generalSettings;
    std::map<QString, InitLogSettings> individualDetectorSettings;

    getLogSettings(generalSettings, individualDetectorSettings);

    saveToFile(fs, generalSettings, individualDetectorSettings);
}

void LogSettingsDialog::saveToFile(cv::FileStorage & fs, 
    const InitLogSettings & generalSettings, 
    const std::map<QString, InitLogSettings>& individualDetectorSettings)
{
    // Save general settings
    fs << "EnableEveryEventLog" << generalSettings.saveEveryEventLog;
    fs << "EnableSumOfEventsLog" << generalSettings.saveSumOfEventsLog;
    fs << "LogEveryPath" << generalSettings.everyEventLogPath.toStdString();
    fs << "LogSumPath" << generalSettings.sumOfEventsLogPath.toStdString();
    fs << "LogSumInterval" << generalSettings.sumInterval;
    fs << "SaveFrameOfEventPath" << generalSettings.saveFrameOfEventPath.toStdString();

    // Save the entry settings of the general settings
    saveEntrySettings(fs, generalSettings.entrySettings, "Log-");

    // Save the individual detector log settings
    std::vector<std::string> individualDetectorNames;

    for (auto individualSettings : individualDetectorSettings)
    {
        individualDetectorNames.push_back(individualSettings.first.toStdString());
        std::string baseName = individualSettings.first.toStdString();

        fs << baseName + "-EnableEveryEventLog" << individualSettings.second.saveEveryEventLog;
        fs << baseName + "-EnableSumOfEventsLog" << individualSettings.second.saveSumOfEventsLog;
        fs << baseName + "-LogEveryPath" << individualSettings.second.everyEventLogPath.toStdString();
        fs << baseName + "-LogSumPath" << individualSettings.second.sumOfEventsLogPath.toStdString();
        fs << baseName + "-LogSumInterval" << individualSettings.second.sumInterval;
        fs << baseName + "-SaveFrameOfEventPath" << individualSettings.second.saveFrameOfEventPath.toStdString();

        saveEntrySettings(fs, individualSettings.second.entrySettings, baseName + "-");
    }

    fs << "Log-individualDetectorNames" << individualDetectorNames;
}

void LogSettingsDialog::getLogSettings(InitLogSettings & generalSettings, 
    std::map<QString, InitLogSettings>& individualDetectorSettings)
{
    // Get the general log settings
    IndividualLogSettings generalLogSettings = individualLogSettings["General"]->getSettings();
    transferIndividualLogSettings(generalSettings, generalLogSettings);

    // Set the stored max active duration 
    for (auto duration : maxTriggeredDuration)
    {
        generalSettings.entrySettings.maxTriggeredDuration[duration.first] = duration.second;   
    }

	bool enableIndividualGeneralT = (enableIndividualGeneralTiming != nullptr && enableIndividualGeneralTiming->isChecked());

	for (auto& generalTimingSettings : individualGeneralTimingSettings)
	{

		if (generalTimingSettings.first == tr("General") && !enableIndividualGeneralT)
		{
			generalSettings.entrySettings.consecutiveEventDelay[generalTimingSettings.first] = generalTimingSettings.second->getConsecutiveEventDelay();
			generalSettings.entrySettings.discardEventsLessThan[generalTimingSettings.first] = generalTimingSettings.second->getDiscardEventsLessThan();
		}
		else if (enableIndividualGeneralT)
		{
			generalSettings.entrySettings.consecutiveEventDelay[generalTimingSettings.first] = generalTimingSettings.second->getConsecutiveEventDelay();
			generalSettings.entrySettings.discardEventsLessThan[generalTimingSettings.first] = generalTimingSettings.second->getDiscardEventsLessThan();
		}
	}

    generalSettings.entrySettings.intervalType.clear();
	generalSettings.entrySettings.overlapTimingBuffer = 0;

	if (displayOptions.showTimingDetectionBox)
	{
		for (auto transition : detectorTransitions)
		{
			if (useIntervalTimingRadioButton->isChecked())
			{
				generalSettings.entrySettings.intervalType.push_back(
					Transition(transition.second->checkedId()));
			}
			else {
				generalSettings.entrySettings.intervalType.push_back(
					Transition::OVERLAP);
				generalSettings.entrySettings.overlapTimingBuffer =
					overlayTimingSpinBox->value();
			}
			
		}

		for (auto delay : maximumTriggeredDelays)
		{
			CombinedEventDelays eventDelay;
			eventDelay.createEventMinDelay = 0;
			eventDelay.createEventMaxDelay = delay.second->value();

			generalSettings.entrySettings.eventDelayIntervals[delay.first] = eventDelay;
		}
	}

    if (displayOptions.showTimingDetectionBox)
    {
        // We save one frame per. detector that corresponds to the chosen transition

        if (generalLogSettings.saveFrameOfEvent)
        {
            generalSettings.entrySettings.saveFrameOfEventSettings[compoundDetectorIdText] = 
                generalSettings.entrySettings.intervalType;
        }
        else {
            // Otherwise, save nothing
            generalSettings.entrySettings.saveFrameOfEventSettings[compoundDetectorIdText] =
                std::vector<Transition>();
        }

    }
    else {
        // We have one event to detect - check, if we should save the frames
        if (generalLogSettings.saveFrameOfEvent)
        {
            // If so, save the entering frame of the event
            generalSettings.entrySettings.saveFrameOfEventSettings[compoundDetectorIdText] =
                std::vector<Transition>{ ENTERING };
        }
        else {
            // Otherwise, save nothing
            generalSettings.entrySettings.saveFrameOfEventSettings[compoundDetectorIdText] =
                std::vector<Transition>();
        }
    }

    // Get the individual log settings
    individualDetectorSettings.clear();

    if ((enableIndividualLogFiles != nullptr) && (enableIndividualLogFiles->isChecked()))
    {
        for (auto settingsObject : individualLogSettings)
        {
            if (settingsObject.first != "General")
            {
                QString detectorIdText = settingsObject.first;
                individualDetectorSettings[detectorIdText] = generalSettings; // Copy the general settings as a point of departure

                // Reset the saveFrameOfEventSettings, eventDelayIntervals
                individualDetectorSettings[detectorIdText].entrySettings.saveFrameOfEventSettings.clear();
                individualDetectorSettings[detectorIdText].entrySettings.eventDelayIntervals.clear();

                // Remove any other entry from the maxTriggeredDuration, eventDelayInterals that the current detectorIdText
                int maxTriggeredDuration = individualDetectorSettings[detectorIdText].entrySettings.maxTriggeredDuration[detectorIdText];
                individualDetectorSettings[detectorIdText].entrySettings.maxTriggeredDuration.clear();
                individualDetectorSettings[detectorIdText].entrySettings.maxTriggeredDuration[detectorIdText] = maxTriggeredDuration;

                transferIndividualLogSettings(individualDetectorSettings[detectorIdText], settingsObject.second->getSettings());

                // When using log files for a single detector, the transition type is only "Entering"
                individualDetectorSettings[detectorIdText].entrySettings.intervalType = { Transition::ENTERING };

                if (settingsObject.second->getSettings().saveFrameOfEvent)
                {
                    individualDetectorSettings[detectorIdText].entrySettings.saveFrameOfEventSettings[detectorIdText] =
                        std::vector<Transition>{ Transition::ENTERING };

                    individualDetectorSettings[detectorIdText].saveFrameOfEventPath =
                        settingsObject.second->getSettings().saveFrameOfEventPath;
                }
                else {
                    individualDetectorSettings[detectorIdText].entrySettings.saveFrameOfEventSettings[detectorIdText] =
                        std::vector<Transition>();

                    individualDetectorSettings[detectorIdText].saveFrameOfEventPath = QString();
                }
            }
        }
    }
}

void LogSettingsDialog::setLogSettings(const InitLogSettings& generalSettings, const std::map<QString, InitLogSettings>& individualDetectorSettings)
{
    // Fetch the maxTriggeredDuration and save it within the class
    maxTriggeredDuration = generalSettings.entrySettings.maxTriggeredDuration;
    
    // Set the general settings
    
    if (individualLogSettings.count("General") == 1)
    {
        IndividualLogSettings initSettings;
        transferIndividualLogSettings(initSettings, generalSettings);
        
        individualLogSettings["General"]->setSettings(initSettings);
    }
	

	for (auto& cEventDelay : generalSettings.entrySettings.consecutiveEventDelay)
	{
		auto it = individualGeneralTimingSettings.find(cEventDelay.first);

		if (it != individualGeneralTimingSettings.end())
		{
			it->second->setConsecutiveEventDelay(cEventDelay.second);
		}
	}

	if (enableIndividualGeneralTiming != nullptr)
	{
		if (generalSettings.entrySettings.consecutiveEventDelay.size() > 1 && generalSettings.entrySettings.discardEventsLessThan.size() > 1)
		{
			enableIndividualGeneralTiming->setChecked(true);
		}
		else {
			enableIndividualGeneralTiming->setChecked(false);
		}
	}

	individualGeneralTimingSwitchChecked(false);

	for (auto& discardEvents : generalSettings.entrySettings.discardEventsLessThan)
	{
		auto it = individualGeneralTimingSettings.find(discardEvents.first);

		if (it != individualGeneralTimingSettings.end())
		{
			it->second->setDiscardEventsLessThan(discardEvents.second);
		}
	}



    // Timing and detection of events    
	if (!generalSettings.entrySettings.intervalType.empty() &&
		(generalSettings.entrySettings.intervalType.front() == Transition::OVERLAP))
	{
		useOverlapTimingRadioButton->setChecked(true);
		overlayTimingSpinBox->setValue(generalSettings.entrySettings.overlapTimingBuffer);
	}
	else {
		for (auto i = 0; i < detectorTransitions.size(); ++i)
		{
			if (generalSettings.entrySettings.intervalType.size() > i)
			{
				detectorTransitions[i].second->button(int(generalSettings.entrySettings.intervalType[i]))->setChecked(true);
			}
		}
	}

    for (auto delay : generalSettings.entrySettings.eventDelayIntervals)
    {
        if (maximumTriggeredDelays.count(delay.first) == 1)
        {
            maximumTriggeredDelays[delay.first]->setValue(delay.second.createEventMaxDelay);
        }
    }
    

    if (individualLogSettings.count("General") == 1)
    {
        IndividualLogSettings initSettings;
        transferIndividualLogSettings(initSettings, generalSettings);

        individualLogSettings["General"]->setSettings(initSettings);
    }

    // Set the individual log settings
    if (!individualDetectorSettings.empty())
    {
        enableIndividualLogFiles->setChecked(true);

        for (auto settingsObject : individualDetectorSettings)
        {
            IndividualLogSettings initSettings;
            transferIndividualLogSettings(initSettings, settingsObject.second);

            if (individualLogSettings.count(settingsObject.first) == 1)
            {
                individualLogSettings[settingsObject.first]->setSettings(initSettings);
            }
        }

        enableIndividualLogFilesChecked();
    }
}

int LogSettingsDialog::getMaxTriggeredDuration(const QString & detectorIdText)
{
    std::map<QString, InitLogSettings> individualSettings;
    InitLogSettings generalSettings;

    getLogSettings(generalSettings, individualSettings);

    auto it = generalSettings.entrySettings.maxTriggeredDuration.find(detectorIdText);

    if (it != generalSettings.entrySettings.maxTriggeredDuration.end())
    {
        return it->second;
    }
    else {
        return -1;
    }
}

void LogSettingsDialog::setMaxTriggeredDuration(const QString & detectorIdText, const int maxTriggeredDuration)
{
    std::map<QString, InitLogSettings> individualSettings;
    InitLogSettings generalSettings;

    getLogSettings(generalSettings, individualSettings);

    generalSettings.entrySettings.maxTriggeredDuration[detectorIdText] = maxTriggeredDuration;

    setLogSettings(generalSettings, individualSettings);
}

void LogSettingsDialog::enableIndividualLogFilesChecked()
{
    if (enableIndividualLogFiles->isChecked())
    {
        for (auto& individualLog : individualLogSettings)
        {
            logFilesSwitch->addTab(individualLog.second, individualLog.first);
        }        
    }
    else {
        // Remove all tabs and only introduce the general
        logFilesSwitch->clear();
        if (individualLogSettings.count("General") == 1)
        {
            logFilesSwitch->addTab(individualLogSettings["General"], tr("General"));
        }
    }
}


void LogSettingsDialog::individualGeneralTimingSwitchChecked(bool transferValues)
{
	if (enableIndividualGeneralTiming != nullptr && enableIndividualGeneralTiming->isChecked())
	{
		individualGeneralTimingSwitch->clear();

		for (auto& individualTiming : individualGeneralTimingSettings)
		{
            QString generalTimingValue = tr("General");

            if (individualTiming.first != generalTimingValue)
			{
				if (transferValues)
                {
					// Transfer the "General" values
                    const auto& it = individualGeneralTimingSettings.find(generalTimingValue);

					if (it != individualGeneralTimingSettings.end())
					{
						individualTiming.second->setConsecutiveEventDelay(it->second->getConsecutiveEventDelay());
						individualTiming.second->setDiscardEventsLessThan(it->second->getDiscardEventsLessThan());
					}
				}

				individualGeneralTimingSwitch->addTab(individualTiming.second, individualTiming.first);
			}
		}
	}
	else {
		// Remove all tabs and introduce the general
		if (individualGeneralTimingSettings.count(tr("General")) == 1)
		{
			if (transferValues)
			{
				auto it = individualGeneralTimingSettings.find(individualGeneralTimingSwitch->tabText(individualGeneralTimingSwitch->currentIndex()));

				individualGeneralTimingSwitch->clear();

				if (it != individualGeneralTimingSettings.end())
				{
					individualGeneralTimingSettings[tr("General")]->setConsecutiveEventDelay(it->second->getConsecutiveEventDelay());
					individualGeneralTimingSettings[tr("General")]->setDiscardEventsLessThan(it->second->getDiscardEventsLessThan());
				}
			}
			
			individualGeneralTimingSwitch->addTab(individualGeneralTimingSettings[tr("General")], tr("General"));
		}
	}
}

void LogSettingsDialog::timingRadioToggled()
{
	maximumDelayGroupBox->hide();
	enteringLeavingGroupBox->hide();
	overlayTimingGroupBox->hide();

	if (useIntervalTimingRadioButton->isChecked())
	{
		maximumDelayGroupBox->show();
		enteringLeavingGroupBox->show();
	}
	else if (useOverlapTimingRadioButton->isChecked())
	{
		overlayTimingGroupBox->show();
	}

	
}

void LogSettingsDialog::transferIndividualLogSettings(
    InitLogSettings & generalSettings, 
    const IndividualLogSettings & individualSettings)
{
    generalSettings.everyEventLogPath = individualSettings.everyEventLogPath;
    generalSettings.saveEveryEventLog = individualSettings.saveEveryEventLog;

    generalSettings.sumOfEventsLogPath = individualSettings.sumOfEventsLogPath;
    generalSettings.saveSumOfEventsLog = individualSettings.saveSumOfEventsLog;
    generalSettings.sumInterval = individualSettings.sumInterval;

    generalSettings.saveFrameOfEventPath = individualSettings.saveFrameOfEventPath;

}

void LogSettingsDialog::transferIndividualLogSettings(
    IndividualLogSettings & individualSettings, 
    const InitLogSettings & generalSettings)
{
    individualSettings.everyEventLogPath = generalSettings.everyEventLogPath;
    individualSettings.saveEveryEventLog = generalSettings.saveEveryEventLog;

    if (generalSettings.entrySettings.saveFrameOfEventSettings.begin() !=
        generalSettings.entrySettings.saveFrameOfEventSettings.end())
    {
        individualSettings.saveFrameOfEvent = generalSettings.entrySettings.saveFrameOfEventSettings.begin()->second.empty() ?
            false : true;
    }
    else {
        individualSettings.saveFrameOfEvent = false;
    }

    individualSettings.saveFrameOfEventPath = generalSettings.saveFrameOfEventPath;
    individualSettings.saveSumOfEventsLog = generalSettings.saveSumOfEventsLog;
    individualSettings.sumInterval = generalSettings.sumInterval;
    individualSettings.sumOfEventsLogPath = generalSettings.sumOfEventsLogPath;
}


void LogSettingsDialog::saveEntrySettings(cv::FileStorage & fs, 
    const InitEntrySettings & entrySettings,
    const std::string& namePrefix)
{
	std::vector<std::string> entryNameList;
	
	for (auto& cEventDelay : entrySettings.consecutiveEventDelay)
	{
		entryNameList.push_back(cEventDelay.first.toStdString());
		fs << namePrefix + "entrySettings-consecutiveEventDelay-" + cEventDelay.first.toStdString() << cEventDelay.second;
	}

	fs << namePrefix + "entrySettings-consecutiveEventDelay-entryNames" << entryNameList;

	entryNameList.clear();
	for (auto& discardEvents : entrySettings.discardEventsLessThan)
	{
		entryNameList.push_back(discardEvents.first.toStdString());
		fs << namePrefix + "entrySettings-discardEventsLessThan-" + discardEvents.first.toStdString() << discardEvents.second;
	}

	fs << namePrefix + "entrySettings-discardEventsLessThan-entryNames" << entryNameList;

    std::vector<int> convertedIntervalType;
    for (auto t : entrySettings.intervalType)
    {
		convertedIntervalType.push_back(t);
    }
    fs << namePrefix + "entrySettings-intervalType" << convertedIntervalType;

    // eventDelayIntervals
    entryNameList.clear();
    std::vector<int> minDelays, maxDelays;

    for (auto& delayInterval : entrySettings.eventDelayIntervals)
    {
        entryNameList.push_back(delayInterval.first.toStdString());
        minDelays.push_back(delayInterval.second.createEventMinDelay);
        maxDelays.push_back(delayInterval.second.createEventMaxDelay);
    }

    fs << namePrefix + "entrySettings-eventDelayIntervals-entryNames" << entryNameList;
    fs << namePrefix + "entrySettings-eventDelayIntervals-minDelays" << minDelays;
    fs << namePrefix + "entrySettings-eventDelayIntervals-maxDelays" << maxDelays;
	
	// Overlap settings
	fs << namePrefix + "entrySettings-overlayTimingBuffer" << entrySettings.overlapTimingBuffer;

    // saveFrameOfEventSettings
    entryNameList.clear();

    for (auto& saveFrameDetector : entrySettings.saveFrameOfEventSettings)
    {
        entryNameList.push_back(saveFrameDetector.first.toStdString());

        std::vector<int> saveFrameSettings;

        for (auto saveFrame : saveFrameDetector.second)
        {
            saveFrameSettings.push_back(saveFrame);
        }

        fs << namePrefix + QString("entrySettings-saveFrameOfEventSettings-%1").arg(saveFrameDetector.first).toStdString()
            << saveFrameSettings;
    }

    fs << namePrefix + "entrySettings-saveFrameOfEventSettings-entryNames" << entryNameList;

    // Max active duration
    entryNameList.clear();
    std::vector<int> durations;

    for (auto detectorDuration : entrySettings.maxTriggeredDuration)
    {
        entryNameList.push_back(detectorDuration.first.toStdString());
        durations.push_back(detectorDuration.second);
    }

    fs << namePrefix + "entrySettings-maxTriggeredDuration-entryNames" << entryNameList;
    fs << namePrefix + "entrySettings-maxTriggeredDuration-durations" << durations;
}

void LogSettingsDialog::loadEntrySettings(const cv::FileStorage & fs, 
    InitEntrySettings & entrySettings, 
    const std::string & namePrefix)
{
	QList<QString> entryNameList = Utils::readQStringListFromFileStorage(fs, namePrefix + "entrySettings-consecutiveEventDelay-entryNames");

	for (auto& entryName : entryNameList)
	{
		int eventDelay;
		fs[namePrefix + "entrySettings-consecutiveEventDelay-" + entryName.toStdString()]
			>> eventDelay;
		entrySettings.consecutiveEventDelay[entryName] = eventDelay;
	}

    if (entryNameList.empty() && !entrySettings.consecutiveEventDelay.empty() && entrySettings.consecutiveEventDelay.begin()->first >= 0)
    {
        // Try to load the settings using the legacy strings
		fs[namePrefix + "entrySettings-consecutiveEventDelay"] >> entrySettings.consecutiveEventDelay[tr("General")];

		if (entrySettings.consecutiveEventDelay[tr("General")] == 0)
		{
			fs["ConsecutiveEventDelay"] >> entrySettings.consecutiveEventDelay[tr("General")];
		}
    }

	if (enableIndividualGeneralTiming != nullptr)
	{
		if (entryNameList.size() < 2)
		{
			enableIndividualGeneralTiming->setChecked(false);
		}
		else {
			enableIndividualGeneralTiming->setChecked(true);
		}
	}

	entryNameList.clear();
	entryNameList = Utils::readQStringListFromFileStorage(fs, namePrefix + "entrySettings-discardEventsLessThan-entryNames");

	for (auto& entryName : entryNameList)
	{
		int eventDelay;
		fs[namePrefix + "entrySettings-discardEventsLessThan-" + entryName.toStdString()]
			>> eventDelay;
		entrySettings.discardEventsLessThan[entryName] = eventDelay;
	}

	if (entryNameList.empty() && !entrySettings.discardEventsLessThan.empty() && entrySettings.discardEventsLessThan.begin()->first >= 0)
	{
		// Try to load the settings using the legacy strings
		fs[namePrefix + "entrySettings-discardEventsLessThan"] >> entrySettings.discardEventsLessThan[tr("General")];

		if (entrySettings.discardEventsLessThan[tr("General")] == 0)
		{
			fs["DiscardEventsLessThan"] >> entrySettings.discardEventsLessThan[tr("General")];
		}
	}

	if (enableIndividualGeneralTiming != nullptr)
	{
		if (entryNameList.size() < 2)
		{
			enableIndividualGeneralTiming->setChecked(false);
		}
		else {
			enableIndividualGeneralTiming->setChecked(true);
		}
	}

	individualGeneralTimingSwitchChecked();

    std::vector<int> convertedIntervalType;
    fs[namePrefix + "entrySettings-intervalType"] >> convertedIntervalType;
    entrySettings.intervalType.clear();

    for (auto t : convertedIntervalType)
    {
        if (t < ENTERING || t > OVERLAP)
        {
            entrySettings.intervalType.push_back(ENTERING);
        }
        else {
            entrySettings.intervalType.push_back(static_cast<Transition>(t));
        }
    }

    // eventDelayIntervals
    entryNameList = Utils::readQStringListFromFileStorage(fs, namePrefix 
        + "entrySettings-eventDelayIntervals-entryNames");
    std::vector<int> minDelays, maxDelays;
        
    fs[namePrefix + "entrySettings-eventDelayIntervals-minDelays"] >> minDelays;
    fs[namePrefix + "entrySettings-eventDelayIntervals-maxDelays"] >> maxDelays;

    for (auto i = 0; i < entryNameList.size(); ++i)
    {
        CombinedEventDelays eventDelays;

        if ((minDelays.size() > i) && (maxDelays.size() > i))
        {
            eventDelays.createEventMinDelay = minDelays[i];
            eventDelays.createEventMaxDelay = maxDelays[i];
            entrySettings.eventDelayIntervals[entryNameList[i]] 
                = eventDelays;
        }
    }

	// Overlap settings
	int overlap;
	fs[namePrefix + "entrySettings-overlayTimingBuffer"] >> entrySettings.overlapTimingBuffer;

    // saveFrameOfEventSettings
    entryNameList.clear();
    entryNameList = Utils::readQStringListFromFileStorage(fs, namePrefix
        + "entrySettings-saveFrameOfEventSettings-entryNames");

    for (auto i = 0; i < entryNameList.size(); ++i)
    {
        std::vector<int> saveFrameSettings;

        fs[namePrefix + "entrySettings-saveFrameOfEventSettings-" + entryNameList[i].toStdString()]
            >> saveFrameSettings;

        std::vector<Transition> transitions;

        for (auto saveFrame : saveFrameSettings)
        {
            transitions.push_back(Transition(saveFrame));
        }

        entrySettings.saveFrameOfEventSettings[entryNameList[i]] = transitions;
    }


    // Max active duration
    entryNameList.clear();
    entryNameList = Utils::readQStringListFromFileStorage(fs, namePrefix
        + "entrySettings-maxTriggeredDuration-entryNames");

    std::vector<int> durations;
    fs[namePrefix + "entrySettings-maxTriggeredDuration-durations"] >> durations;

    for (auto i = 0; i < entryNameList.size(); ++i)
    {
        if (durations.size() > i)
        {
            entrySettings.maxTriggeredDuration[entryNameList[i]] = durations[i];
        }
    }    
}

IndividualLogSettingsDialog::IndividualLogSettingsDialog(QWidget * parent,
    const QString& configurationPath, 
    const QString& detectorIdText,
    const IndividualLogSettings& settings)
    : QWidget(parent)
{
    this->configurationPath = configurationPath;
    this->detectorIdText = detectorIdText;

    // Log every event
    logEveryEventGroupBox = new QGroupBox(tr("Log every event"));
    logEveryEventGroupBox->setCheckable(true);

    connect(logEveryEventGroupBox,
        SIGNAL(clicked(bool)),
        this, SLOT(logEveryEventGroupBoxChecked()));


    QGridLayout* logEveryEventLayout = new QGridLayout();
    logEveryEventLayout->addWidget(everyEventPath = new QLineEdit(), 0, 0);
    logEveryEventLayout->addWidget(everyEventButton = new QPushButton(tr("Browse")), 0, 1);
    logEveryEventGroupBox->setLayout(logEveryEventLayout);

    connect(everyEventButton,
        SIGNAL(clicked(bool)),
        this, SLOT(everyEventButtonClicked()));

    // Save frame of events
    saveFrameOfEventGroupBox = new QGroupBox(tr("Save frame of every event"));
    saveFrameOfEventGroupBox->setCheckable(true);
    saveFrameOfEventGroupBox->setEnabled(false);

	connect(saveFrameOfEventGroupBox,
		SIGNAL(clicked(bool)),
		this, SLOT(saveFrameGroupBoxChecked()));

    QGridLayout* saveFrameOfEventLayout = new QGridLayout();
    saveFrameOfEventLayout->addWidget(saveFramePath = new QLineEdit(), 0, 0);
    saveFrameOfEventLayout->addWidget(saveFrameButton = new QPushButton(tr("Browse")), 0, 1);
    saveFrameOfEventGroupBox->setLayout(saveFrameOfEventLayout);
    
    connect(saveFrameButton,
        SIGNAL(clicked(bool)),
        this, SLOT(saveFrameButtonClicked()));

    // Log sum of events
    logSumOfEventsGroupBox = new QGroupBox(tr("Log sum of events"));
    logSumOfEventsGroupBox->setCheckable(true);

    QGridLayout* logSumOfEventsLayout = new QGridLayout();
    logSumOfEventsLayout->addWidget(logSumPath = new QLineEdit(), 0, 0);
    logSumOfEventsLayout->addWidget(logSumButton = new QPushButton(tr("Browse")), 0, 1);
    logSumOfEventsLayout->addWidget(new QLabel(tr("Log interval")), 1, 0);
    logSumOfEventsLayout->addWidget(logSumIntervalSpinBox = new QSpinBox(), 1, 1);
    logSumIntervalSpinBox->setValue(15);
    logSumIntervalSpinBox->setMaximum(999000);
    logSumIntervalSpinBox->setMinimum(1);
    logSumIntervalSpinBox->setSuffix(tr(" min"));
    logSumOfEventsGroupBox->setLayout(logSumOfEventsLayout);

    connect(logSumButton,
        SIGNAL(clicked(bool)),
        this, SLOT(logSumButtonClicked()));

    QVBoxLayout* generalLayout = new QVBoxLayout();
    generalLayout->addWidget(logEveryEventGroupBox);
    generalLayout->addWidget(saveFrameOfEventGroupBox);
    generalLayout->addWidget(logSumOfEventsGroupBox);
    setLayout(generalLayout);

    setSettings(settings);
}

IndividualLogSettings IndividualLogSettingsDialog::getSettings()
{
    IndividualLogSettings settings;

    settings.everyEventLogPath = everyEventPath->text();
    settings.saveEveryEventLog = logEveryEventGroupBox->isChecked();

    settings.sumOfEventsLogPath = logSumPath->text();
    settings.saveSumOfEventsLog = logSumOfEventsGroupBox->isChecked();
    settings.sumInterval = logSumIntervalSpinBox->value();

    settings.saveFrameOfEvent = saveFrameOfEventGroupBox->isChecked();
    settings.saveFrameOfEventPath = saveFramePath->text();

    return settings;
}

void IndividualLogSettingsDialog::setSettings(IndividualLogSettings settings)
{
    everyEventPath->setText(settings.everyEventLogPath);
    logEveryEventGroupBox->setChecked(settings.saveEveryEventLog);
    saveFrameOfEventGroupBox->setEnabled(settings.saveEveryEventLog);

    logSumPath->setText(settings.sumOfEventsLogPath);
    logSumOfEventsGroupBox->setChecked(settings.saveSumOfEventsLog);
    if (settings.sumInterval > 0)
    {    
        logSumIntervalSpinBox->setValue(settings.sumInterval);
    }

    saveFrameOfEventGroupBox->setChecked(settings.saveFrameOfEvent);
    saveFramePath->setText(settings.saveFrameOfEventPath);
}

void IndividualLogSettingsDialog::logEveryEventGroupBoxChecked()
{
    saveFrameOfEventGroupBox->setEnabled(logEveryEventGroupBox->isChecked());
}


void IndividualLogSettingsDialog::saveFrameGroupBoxChecked()
{
	if (saveFramePath->text().isEmpty())
	{
		QString filePath = everyEventPath->text();
		if (!filePath.isEmpty())
		{
			// Create a new directory called "Frames" in the base directory of the log file
			QDir baseDir = QFileInfo(filePath).absoluteDir();
			QString framesDir = "Frames";
			if (!detectorIdText.isEmpty())
			{
				framesDir.append(QString("_%1").arg(detectorIdText));
			}

			baseDir.mkdir(framesDir);
			baseDir.cd(framesDir);
			saveFramePath->setText(baseDir.path());
		}
	}
}

void IndividualLogSettingsDialog::everyEventButtonClicked()
{
    QString logFileName;

    if (everyEventPath->text().isEmpty())
    {
		if (configurationPath.isEmpty())
		{
			QSettings settings;
			settings.beginGroup("logfiles");
			QString path = settings.value("everyEventPath").toString();
			logFileName = path;
		}
		else {
			logFileName = configurationPath;
		}

        logFileName.remove(".yml");
        if (!detectorIdText.isEmpty())
        {
            logFileName.append("_" + detectorIdText);
        }

        logFileName.append( + "_events.csv");
    }
    else {
        logFileName = everyEventPath->text();
    }

    QString filePath = QFileDialog::getSaveFileName(this,
        tr("Set log file for every event"), 
        logFileName, 
        tr("Log files (*.csv)"));

    if (!filePath.isEmpty())
    {
        everyEventPath->setText(filePath);

		QSettings settings;
		settings.beginGroup("logfiles");
		settings.setValue("everyEventPath", filePath);
    }
}

void IndividualLogSettingsDialog::saveFrameButtonClicked()
{
    QString startPath = saveFramePath->text();

    if (startPath.isEmpty())
    {
        startPath = configurationPath;
    }

    QString filePath = QFileDialog::getExistingDirectory(this, 
        tr("Select directory to save frames"),
        startPath,
        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if (!filePath.isEmpty())
    {
        saveFramePath->setText(filePath);
    }
}

void IndividualLogSettingsDialog::logSumButtonClicked()
{
    QString logFileName;

    if (logSumPath->text().isEmpty())
    {
		if (configurationPath.isEmpty())
		{
			QSettings settings;
			settings.beginGroup("logfiles");
			QString path = settings.value("sumOfEvents").toString();
			logFileName = path;
		}
		else {
			logFileName = configurationPath;
		}

        logFileName.remove(".yml");

        if (!detectorIdText.isEmpty())
        {
            logFileName.append("_" + detectorIdText);
        }

        logFileName.append("_counts.csv");
    }
    else {
        logFileName = logSumPath->text();
    }

    QString filePath = QFileDialog::getSaveFileName(this,
        tr("Set log file for sum of events"),
        logFileName,
        tr("Log files (*.csv)"));

    if (!filePath.isEmpty())
    {
		QSettings settings;
		settings.beginGroup("logfiles");
		settings.setValue("sumOfEvents", filePath);

        logSumPath->setText(filePath);
    }
}

GeneralDetectorTimingDialog::GeneralDetectorTimingDialog(QWidget * parent, 
	const QString & configurationPath, 
	const QString & detectorIdText, 
	const GeneralDetectorTimingSettings & settings)
	: QWidget(parent)
{
	this->configurationPath = configurationPath;
	this->detectorIdText = detectorIdText;

	generalTimingLayout = new QFormLayout();

	discardEventsLessThanSpinBox = nullptr;
	consecutiveEventDelaySpinBox = nullptr;

	setDiscardEventsLessThan(settings.discardEventsLessThan);
	setConsecutiveEventDelay(settings.consecutiveEventDelay);
		
	setLayout(generalTimingLayout);
	setSettings(settings);
}

GeneralDetectorTimingSettings GeneralDetectorTimingDialog::getSettings()
{
	GeneralDetectorTimingSettings settings;

	settings.consecutiveEventDelay = -1;
	settings.discardEventsLessThan = -1;

	if (consecutiveEventDelaySpinBox != nullptr)
	{
		settings.consecutiveEventDelay = consecutiveEventDelaySpinBox->value();
	}

	if (discardEventsLessThanSpinBox != nullptr)
	{
		settings.discardEventsLessThan = discardEventsLessThanSpinBox->value();
	}

	return settings;
}

void GeneralDetectorTimingDialog::setSettings(const GeneralDetectorTimingSettings & settings)
{
	setConsecutiveEventDelay(settings.consecutiveEventDelay);
	setDiscardEventsLessThan(settings.discardEventsLessThan);
	
}

int GeneralDetectorTimingDialog::getConsecutiveEventDelay()
{
	if (consecutiveEventDelaySpinBox != nullptr)
	{
		return consecutiveEventDelaySpinBox->value();
	}
	else {
		return -1;
	}
}

int GeneralDetectorTimingDialog::getDiscardEventsLessThan()
{
	if (discardEventsLessThanSpinBox != nullptr)
	{
		return discardEventsLessThanSpinBox->value();
	}
	else {
		return -1;
	}
}

void GeneralDetectorTimingDialog::setConsecutiveEventDelay(int value)
{
	if (value >= 0 && consecutiveEventDelaySpinBox != nullptr)
	{
		consecutiveEventDelaySpinBox->setValue(value);
	}
	else if (value >= 0)
	{
		consecutiveEventDelaySpinBox = new QSpinBox(this);
		consecutiveEventDelaySpinBox->setSingleStep(50);
		consecutiveEventDelaySpinBox->setMaximum(9999999);
		consecutiveEventDelaySpinBox->setSuffix(tr(" ms"));
		consecutiveEventDelaySpinBox->setValue(value);
		generalTimingLayout->addRow(tr("Collate events within"), consecutiveEventDelaySpinBox);
	} 
	else if(consecutiveEventDelaySpinBox != nullptr) 
	{
		generalTimingLayout->removeWidget(consecutiveEventDelaySpinBox);
		delete consecutiveEventDelaySpinBox;
		consecutiveEventDelaySpinBox = nullptr;
	}
}

void GeneralDetectorTimingDialog::setDiscardEventsLessThan(int value)
{
	if (value >= 0 && discardEventsLessThanSpinBox != nullptr)
	{
		discardEventsLessThanSpinBox->setValue(value);
	}
	else if (value >= 0)
	{
		discardEventsLessThanSpinBox = new QSpinBox(this);
		discardEventsLessThanSpinBox->setSingleStep(50);
		discardEventsLessThanSpinBox->setMaximum(9999999);
		discardEventsLessThanSpinBox->setSuffix(tr(" ms"));
		discardEventsLessThanSpinBox->setValue(value);
		generalTimingLayout->addRow(tr("Delete events smaller than"), discardEventsLessThanSpinBox);
	}
	else if (consecutiveEventDelaySpinBox != nullptr){
		generalTimingLayout->removeWidget(discardEventsLessThanSpinBox);
		delete discardEventsLessThanSpinBox;
		discardEventsLessThanSpinBox = nullptr;
	}
}
