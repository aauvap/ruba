#ifndef PRESENCEDETECTOR_H
#define PRESENCEDETECTOR_H

#include <QDateTime>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <QList>
#include <QDateTime>
#include "ruba.h"
#include "abstractmultimodaldetector.h"
#include "scrollingchart.h"
#include "framestruct.h"
#include "ChartStruct.h"
#include "detectoroutputstruct.h"

class ruba::PresenceDetector : public AbstractMultiModalDetector
{
public:
    PresenceDetector(BaseDetectorParams baseParams, std::vector<int> minCoveredArea,  
        std::vector<cv::Mat> masks, std::vector<cv::Mat> backImages, bool usechart);


    void InitDetector();

    virtual std::vector<QString*>  GetChartTitles();
    QList<QString> getDetectorSpecificDebugInfo();

protected:
	virtual ChartColors initialiseChartColors();

private:
	
    std::vector<std::shared_ptr<ruba::PresenceDetectorSingleModal> > PDetectors;
};

#endif // PRESENCEDETECTOR_H
