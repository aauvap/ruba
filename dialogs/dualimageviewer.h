#ifndef DUALIMAGEVIEWER_H
#define DUALIMAGEVIEWER_H


#include <QDialog>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include "opencvviewer.h"

namespace Ui {
class DualImageViewer;
}

class DualImageViewer : public QDialog
{
    Q_OBJECT
    
public:
    explicit DualImageViewer(QWidget *parent, cv::Mat img1, cv::Mat img2);
    ~DualImageViewer();

    void SetDecisionText(QString text1, QString text2);
    void SetImageCaption(QString cap1, QString cap2);
    void SetButtonText(QString text1, QString text2);
    
private slots:
    void on_ImageButton1_clicked();

    void on_ImageButton2_clicked();

private:
    Ui::DualImageViewer *ui;

    OpenCVViewer *iviewer1;
    OpenCVViewer *iviewer2;
};

#endif // DUALIMAGEVIEWER_H
