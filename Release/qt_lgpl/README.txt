Compliance with Qt LGPL
========================
RUBA uses the Qt Toolkit under the terms of the GNU Lesser
General Public License v. 3 and the GNU Lesser General Public License v. 2.1
(both jointly "LGPL"). In compliance with LGPL, this document collects the
relevant information about downloading, installing, and building the Qt
Toolkit from sources. 


LGPL License Text
=================
The first step in compliance with LGPL is that every recipient of the RUBA
software receives also the full text of the LGPL licenses used. RUBA complies
with this requirement by including the full text of the LGPL licenses
in every RUBA distribution (folder qt_lgpl, files lgpl-2.1.txt and lgpl-3.txt).


How RUBA Modeling Tool Uses Qt
============================
Dynamic Linking
---------------
On each supported platform (Windows, and macOS), RUBA
*dynamically* links to the unmodified Qt libraries, as provided by the Qt
Company in the pre-compiled binary format.

LGPL Information in RUBA GUI
--------------------------
Also, in compliance with LGPL, the "About RUBA" dialog box of the RUBA
application prominently states that this program uses the Qt Toolkit under
the terms of LGPL.

Qt Version Information
----------------------
Additionally, the RUBA 'Window' Menu includes the standard "About Qt" dialog box,
which informs the user about the exact version of Qt and explains the Qt
licensing terms. Finally, the RUBA online manual prominently states that this program
uses the Qt Toolkit under the terms of LGPL.


Qt Library Source Code
===============
========
The RUBA Modeling tool has used several Qt versions. Please choose the Qt
version displayed by the RUBA "About Qt Dialog Box":

Windows Hosts
-------------
* Qt 5.13.0 http://download.qt.io/official_releases/qt/5.13/5.13.0/qt-opensource-windows-x86-5.13.0.exe (3.7 GiB)

MacOS Hosts
-----------
* Qt 5.10.1 http://download.qt.io/official_releases/qt/5.10/5.10.1/qt-opensource-mac-x64-clang-5.10.1.dmg (1.1GB)


Written Offer to Provide Source Code
====================================
In case you have any trouble downloading the Qt Source Code from the official
links provided above, the RUBA authors makes hereby a written offer to provide
such source code to you. To make a Qt source code request, send email to the
following address: cb@create.aau.dk. Please indicate which specific RUBA
version you are using. Each Qt version will be available for three (3) years
after the release date of the corresponding RUBA tool.
