#ifndef GROUNDTRUTHANNOTATORDIALOG_H
#define GROUNDTRUTHANNOTATORDIALOG_H

#include <memory>
#include <set>
#include <QDialog>
#include <QMessageBox>
#include <QStandardItemModel>
#include <QtGui>
#include <QDateTime>
#include <QFileDialog>
#include <QShortcut>
#include <QHash>
#include <opencv2/opencv.hpp>
#include "ruba.h"
#include "maskoverlaystruct.h"
#include "framestruct.h"

struct PlaybtnShortcut{
    /** Placeholder name - in reality the struct (Function) is used directly */
    enum e {
        playPause, ///< Play/pause the current video.
        prev5Frames, ///< Go back five frames in the video
        prevFrame, ///< Go back one frame in the video
        nextFrame, ///< Go to the next frame in the video
        next5Frames, ///< Go to the fifth frame in the video
		fiveSecPrev, ///< Go back five seconds in the video
		oneSecPrev, ///< Go back one second in the video
		oneSecForw, ///< Go forward one second in the video
		fiveSecForw, ///< Go forward five seconds in the video
}; };

struct ProgramModes{
    enum e {
        VALIDATIONMODE, ///< In validation mode, we will highlight the timestamps as they occur in the video
        ANNOTATIONMODE, ///< In annotation mode, no such thing will happen
    };
};

enum CustomDataRoles {
	FileName = 1030,
	TimestampDate = 1031
};


namespace Ui {
class GroundTruthAnnotatorDialog;
}

class GroundTruthAnnotatorDialog : public QDialog
{
	Q_OBJECT

public:
    explicit GroundTruthAnnotatorDialog(QWidget *parent, const std::vector<ruba::FrameStruct> &initFrames);
    ~GroundTruthAnnotatorDialog();

    bool eventFilter(QObject* object, QEvent* event);
    void updateFrame(std::vector<ruba::FrameStruct>& frames);
    QList<QString> getLatestTimestamps();
    std::vector<std::vector<ruba::MaskOverlayStruct> > GetMasks();

signals:
     void jumpToFrame(QDateTime requestedTime);

private slots:
    void autoSave();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_saveLogButton_clicked();

    void on_checkBox_clicked(bool checked);

    void on_editEventTypeButton_clicked();

    void on_finishEditingButton_clicked();

    void on_saveLogAsButton_clicked();

    void on_openLogButton_clicked();

    void on_enableValidationModeCheckBox_clicked(bool checked);

    void on_tableView_doubleClicked(const QModelIndex& index);

	void on_addNewEntriesAtBottomCheckbox_clicked(bool checked);

private:
    Ui::GroundTruthAnnotatorDialog *ui;
    QStandardItemModel *gtTable;
    QStandardItemModel *headerLabels;

    void initMasks(const std::vector<ruba::FrameStruct> &initFrames);
    void findMatchingTimestamp(QDateTime time);
    void updateLatestTimestamps(const QString &timestamp, int column, int row);
    int openLog(QString filename);
    void saveLog();
    void checkFixHeaderLabels();
    void setTableHeaders();
    void enableEditingMode(bool state);
    void insertTableEntry(QStandardItem &item, int column, int row);
	void moveItemsUpIfRequired(int startRow, int excludedColumn);
	void removeEmptyRows();
    void deleteItemFromTable(int row, int column);
    void newTableEntry(int column);
	bool checkApperanceFileDateInRow(QStandardItem& item, int row);

    bool dataChanged;
    QList<QString> latestTimestamps; // Queue containing the five latest timestamps
    QTimer autoSaveTimer;
    QDateTime currentTime;
	QFileInfo videoFilename;
    QWidget* parent;    

	QSet<QString> knownFileNames;
	QSet<QString> knownDates;

    // Functionality mode of the annotator. Supports ProgramModes::ValidationMode
    // and ProgramModes::AnnotationMode
    int mode;
	bool mimickLogFileAppearance;

    // Mask generation
    cv::Mat baseMask;
    std::vector<std::vector<ruba::MaskOverlayStruct>> maskOverlays;


    QHash<int, std::shared_ptr<QShortcut>> playbackShortcuts;
};

#endif // GROUNDTRUTHANNOTATORDIALOG_H
