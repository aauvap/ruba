#! /bin/bash

#LIB_DIR=/Users/markpp/Downloads/opencv-3.3.0/build/lib
#APP_DIR=/Users/markpp/Desktop/code/VAPprojects/build-trafficDetector-Desktop_Qt_5_6_0_clang_64bit-Debug/TrafficDetector.app
FFMPEG_LIB_DIR=/usr/local/Cellar/ffmpeg/3.3.3/lib
APP_DIR=/Users/chris/code/build-ruba-Desktop_Qt_5_14_0_clang_64bit-Release/RUBA.app
# This script supposes here that you are in the parent directory of the app.




# Extra dependencies exist: otool -L libopencv_* will show you all the dependencies that you need to copy.
# Here we will also need to add imgcodecs and videoio to the .app
#cp $LIB_DIR/libopencv_imgcodecs.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_imgcodecs.3.3.0.dylib
#cp $LIB_DIR/libopencv_videoio.3.3.0.dylib $APP_DIR/Contents/Frameworks/libopencv_videoio.3.3.0.dylib




# Now we need to fix the path to all the dependencies of each library file.
cd $APP_DIR/Contents/Frameworks

# Again, with otool -L libopencv_* , you will see that you need to change their paths so that your copied libraries are pointed to within the app.
# for each opencv lib that is not properly referenced, change it from the absolute or wrong-relative path to @executable_path/../Frameworks/

# fix libavcodec
install_name_tool -change $FFMPEG_LIB_DIR/libswresample.2.dylib @executable_path/../Frameworks/libswresample.2.dylib libavcodec.57.dylib
install_name_tool -change $FFMPEG_LIB_DIR/libavutil.55.dylib @executable_path/../Frameworks/libavutil.55.dylib libavcodec.57.dylib

# fix libavutil - not needed

# fix libswresample
install_name_tool -change $FFMPEG_LIB_DIR/libavutil.55.dylib @executable_path/../Frameworks/libavutil.55.dylib.dylib libavcodec.57.dylib
install_name_tool -change $FFMPEG_LIB_DIR/libavutil.55.dylib @executable_path/../Frameworks/libavutil.55.dylib libswresample.2.dylib

# fix libavformat
install_name_tool -change $FFMPEG_LIB_DIR/libswresample.2.dylib @executable_path/../Frameworks/libswresample.2.dylib libavformat.57.dylib
install_name_tool -change $FFMPEG_LIB_DIR/libavutil.55.dylib @executable_path/../Frameworks/libavutil.55.dylib libavformat.57.dylib
install_name_tool -change $FFMPEG_LIB_DIR/libavcodec.57.dylib @executable_path/../Frameworks/libavcodec.57.dylib libavformat.57.dylib

# fix libswscale
install_name_tool -change $FFMPEG_LIB_DIR/libavutil.55.dylib @executable_path/../Frameworks/libavutil.55.dylib libswscale.4.dylib

#fix libavresample
install_name_tool -change $FFMPEG_LIB_DIR/libavutil.55.dylib @executable_path/../Frameworks/libavutil.55.dylib libavresample.3.dylib

# Fix OpenCV libs

install_name_tool -rpath /usr/local/Cellar/opencv/3.3.0_3/lib @executable_path/../Frameworks/ libopencv_core.3.3.dylib
install_name_tool -rpath /usr/local/Cellar/opencv/3.3.0_3/lib @executable_path/../Frameworks/ libopencv_features2d.3.3.dylib
install_name_tool -rpath /usr/local/Cellar/opencv/3.3.0_3/lib @executable_path/../Frameworks/ libopencv_flann.3.3.dylib
install_name_tool -rpath /usr/local/Cellar/opencv/3.3.0_3/lib @executable_path/../Frameworks/ libopencv_highgui.3.3.dylib
install_name_tool -rpath /usr/local/Cellar/opencv/3.3.0_3/lib @executable_path/../Frameworks/ libopencv_imgcodecs.3.3.dylib
install_name_tool -rpath /usr/local/Cellar/opencv/3.3.0_3/lib @executable_path/../Frameworks/ libopencv_imgproc.3.3.dylib
install_name_tool -rpath /usr/local/Cellar/opencv/3.3.0_3/lib @executable_path/../Frameworks/ libopencv_video.3.3.dylib
install_name_tool -rpath /usr/local/Cellar/opencv/3.3.0_3/lib @executable_path/../Frameworks/ libopencv_videoio.3.3.dylib
install_name_tool -rpath /usr/local/Cellar/ffmpeg/3.3.3/lib @executable_path/../Frameworks/ libopencv_videoio.3.3.dylib