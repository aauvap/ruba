#include "setdatetimedialog.h"
#include "ui_setdatetimedialog.h"

SetDateTimeDialog::SetDateTimeDialog(QWidget *parent, const QString& filename,
    int dateTimeFormat, int userDefinedYear, bool forceNoGui, QString prevFilename, 
    QDateTime prevDateTime) :
    QDialog(parent),
    ui(new Ui::SetDateTimeDialog)
{
    isInitialized = false;

    QFileInfo filePath(filename);
    basename = filePath.completeBaseName(); // The base name of the file is used for setting the date and time

    ui->setupUi(this);

    this->prevFilename = prevFilename;
    this->prevDateTime = prevDateTime;
    this->dateTimeFormat = dateTimeFormat;
    this->userDefinedYear = userDefinedYear;
    this->parent = parent;
    this->forceNoGui = forceNoGui;
    this->userDefinedHour = -1;

    ui->filename->setText(basename);
    ui->formatBox->blockSignals(true);
    ui->formatBox->addItem("MM-dd-HH");
    ui->formatBox->addItem("yyyy-MM-dd");
    ui->formatBox->addItem("yyyyMMdd-HH");
    ui->formatBox->addItem("yyyy-MM-dd-HH");
    ui->formatBox->addItem("yyyyMMdd-HH-mm-ss");
	ui->formatBox->addItem("yyyy-MM-dd-HH-ss.zzz");
	ui->formatBox->addItem("ddMMyyyy");
    ui->formatBox->addItem(tr("User defined"));

    if (ui->formatBox->count() > dateTimeFormat)
    {
        ui->formatBox->setCurrentIndex(dateTimeFormat);
    }
    else {
        ui->formatBox->setCurrentIndex(USERDEFINED);
    }
    ui->formatBox->blockSignals(false);

    if (dateTimeFormat == USERDEFINED || dateTimeFormat == USERDEFINED_USE_END_TIME_AS_START_TIME)
    {
        ui->dateTimeEdit->setEnabled(true);
        ui->useEndTimeAsStartTimeCheckBox->show();

        if (dateTimeFormat == USERDEFINED_USE_END_TIME_AS_START_TIME)
        {
            ui->useEndTimeAsStartTimeCheckBox->setChecked(true);
        }
    }
    else {
        ui->useEndTimeAsStartTimeCheckBox->hide();
    }

    isInitialized = true;
}

SetDateTimeDialog::~SetDateTimeDialog()
{
    delete ui;
}

int SetDateTimeDialog::findStartTime(QDateTime& computedStartTime, int& dateTimeFormat)
{
    int retVal = findStartTimeFromFile(computedStartTime);
    
    while (retVal != 0 && !forceNoGui)
    {
        ui->dateTimeEdit->setDateTime(computedStartTime);
        ui->buttonBox->setEnabled(false);
        exec();
        retVal = findStartTimeFromFile(computedStartTime);
    }

    dateTimeFormat = this->dateTimeFormat;
    return 0;
}

void SetDateTimeDialog::on_formatBox_currentIndexChanged(int val)
{        
    if (isInitialized)
    {
        dateTimeFormat = val;

        if (dateTimeFormat == USERDEFINED || dateTimeFormat == USERDEFINED_USE_END_TIME_AS_START_TIME)
        {
            ui->dateTimeEdit->setEnabled(true);
            ui->useEndTimeAsStartTimeCheckBox->show();
            ui->messageLabel->setText("Please enter the date and time manually and press OK");
        }
        else {
            ui->dateTimeEdit->setEnabled(false);
            ui->useEndTimeAsStartTimeCheckBox->hide();
        }

        QDateTime computedDatetime;
        int retVal = findStartTimeFromFile(computedDatetime);
        ui->dateTimeEdit->setDateTime(computedDatetime);

        if (retVal == 0)
        {
            // Enable the user to exit the dialog
            ui->buttonBox->setEnabled(true);

            if (dateTimeFormat != USERDEFINED || dateTimeFormat != USERDEFINED)
            {
                ui->messageLabel->setText("Recognized the date and time!");
            }
        }
        else {
            ui->buttonBox->setEnabled(false);

            if (dateTimeFormat != USERDEFINED || dateTimeFormat != USERDEFINED_USE_END_TIME_AS_START_TIME)
            {
                ui->messageLabel->setText("");
            }

        }
    }
}

void SetDateTimeDialog::on_dateTimeEdit_dateTimeChanged(const QDateTime& datetime)
{
    if (dateTimeFormat == USERDEFINED || dateTimeFormat == USERDEFINED_USE_END_TIME_AS_START_TIME)
    {
        QDateTime computedDatetime;
        int retVal = findStartTimeFromFile(computedDatetime);

        if (retVal == 0)
        {
            ui->buttonBox->setEnabled(true);
        }
        else {
            ui->buttonBox->setEnabled(false);
        }
    }
}

//!
/*!

*/
int SetDateTimeDialog::findStartTimeFromFile(QDateTime& computedDateTime)
{
    QDate tmpDate;
    QTime tmpTime = QTime(0, 0, 0, 0);

    switch (dateTimeFormat)
    {
    case MM_dd_HH:
    {
        QRegExp rxlen("([0-9]+)-([0-9]+)-([0-9]+)-*");
        int pos = rxlen.indexIn(basename);

        if (pos > -1)
        {
            QString monthStr = rxlen.cap(1);
            QString dateStr = rxlen.cap(2);
            QString hourStr = rxlen.cap(3);

            // Check the sanity of the strings - the exact year does not matter here
            tmpDate.setDate(2001, monthStr.toInt(), dateStr.toInt());
            tmpTime.setHMS(hourStr.toInt(), 0, 0, 0);

            if (!tmpDate.isValid() || !tmpTime.isValid()) {
                return 1;
            }

            if (userDefinedYear <= 0)
            {
                // If the year has not been defined, prompt the user
                bool ok;

                // Define the year of the video.
                QString message = tr("Set year of video \"%1\":").arg(basename);
                userDefinedYear = QInputDialog::getInt(parent, "Set year", message, QDateTime::currentDateTime().date().year(), 1900, 2200, 1, &ok);
            }

            tmpDate.setDate(userDefinedYear, 1, 1);
            tmpDate.setDate(tmpDate.year(), monthStr.toInt(), dateStr.toInt());       
        }
        break;
    }
    case yyyy_MM_DD:
    {
        QRegExp rxlen("([0-9]+)-([0-9]+)-([0-9]+)-*");
        int pos = rxlen.indexIn(basename);

        if (pos > -1)
        {
            QString yearStr = rxlen.cap(1);
            QString monthStr = rxlen.cap(2);
            QString dateStr = rxlen.cap(3);

            tmpDate.setDate(yearStr.toInt(), monthStr.toInt(), dateStr.toInt());

            // Check the sanity of the strings
            if (!tmpDate.isValid()) {
                return 1;
            }

            if (userDefinedHour == -1)
            {
                // Prompt the user to define the hour
                bool ok = false;

                // Define the year of the video.

                while (!ok)
                {
                    QString message = tr("Set hour of video \"%1\":").arg(basename);
                    userDefinedHour = QInputDialog::getInt(parent, "Set hour", message, QDateTime::currentDateTime().time().hour(), 0, 23, 1, &ok);
                }
            }
            
            tmpTime.setHMS(userDefinedHour, 0, 0, 0);
        }

        break;
    }
    case yyyyMMdd_HH:
    {
        QRegExp rxlen("(\\d{4})(\\d{2})(\\d{2})-(\\d{2})");
        int pos = rxlen.indexIn(basename);

        if (pos > -1)
        {
            QString yearStr = rxlen.cap(1);
            QString monthStr = rxlen.cap(2);
            QString dateStr = rxlen.cap(3);
            QString hourStr = rxlen.cap(4);

            tmpDate.setDate(yearStr.toInt(), monthStr.toInt(), dateStr.toInt());
            tmpTime.setHMS(hourStr.toInt(), 0, 0, 0);
        }
        break;
    }
    case yyyy_MM_dd_HH:
    {
        QRegExp rxlen("([0-9]+)-([0-9]+)-([0-9]+)-([0-9]+)-*");
        int pos = rxlen.indexIn(basename);

        if (pos > -1)
        {
            QString yearStr = rxlen.cap(1);
            QString monthStr = rxlen.cap(2);
            QString dateStr = rxlen.cap(3);
            QString hourStr = rxlen.cap(4);

            tmpDate.setDate(yearStr.toInt(), monthStr.toInt(), dateStr.toInt());
            tmpTime.setHMS(hourStr.toInt(), 0, 0, 0);
        }

        break;
    }
    case yyyyMMDD_HH_mm_ss:
    {
        QRegExp rxlen("(\\d{4})(\\d{2})(\\d{2})-(\\d{2})-(\\d{2})-(\\d{2})");
        int pos = rxlen.indexIn(basename);

        bool dateTimeFound = false;

        if (pos > -1)
        {
            dateTimeFound = true;
        }
        else 
        {
            rxlen.setPattern("(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})(\\d{2})");
            pos = rxlen.indexIn(basename);

            if (pos > -1) {
                dateTimeFound = true;
            }
        }

        if (dateTimeFound) 
        {
            QString yearStr = rxlen.cap(1);
            QString monthStr = rxlen.cap(2);
            QString dateStr = rxlen.cap(3);
            QString hourStr = rxlen.cap(4);
            QString minStr = rxlen.cap(5);
            QString secStr = rxlen.cap(6);

            tmpDate.setDate(yearStr.toInt(), monthStr.toInt(), dateStr.toInt());
            tmpTime.setHMS(hourStr.toInt(), minStr.toInt(), secStr.toInt(), 0);
        }

        break;
    }
	case yyyy_MM_dd_HH_ss_zzz:
	{
		QRegExp rxlen("(\\d{4})-(\\d{2})-(\\d{2})-(\\d{2})-(\\d{2})-(\\d{2}).(\\d{3})");
		int pos = rxlen.indexIn(basename);

		bool dateTimeFound = false;

		if (pos > -1)
		{
			dateTimeFound = true;
		}

		if (dateTimeFound)
		{
			QString yearStr = rxlen.cap(1);
			QString monthStr = rxlen.cap(2);
			QString dateStr = rxlen.cap(3);
			QString hourStr = rxlen.cap(4);
			QString minStr = rxlen.cap(5);
			QString secStr = rxlen.cap(6);
			QString mSecStr = rxlen.cap(7);

			tmpDate.setDate(yearStr.toInt(), monthStr.toInt(), dateStr.toInt());
			tmpTime.setHMS(hourStr.toInt(), minStr.toInt(), secStr.toInt(), mSecStr.toInt());
		}

		break;
	}
	case ddMMyyyy:
	{
		QRegExp rxlen("(\\d{2})(\\d{2})(\\d{4})");
		int pos = rxlen.indexIn(basename);

		bool dateTimeFound = false;

		if (pos > -1 && (rxlen.cap(0).length() == 8))
		{
			dateTimeFound = true;
		}
		else
		{
			rxlen.setPattern("^*(\\d{2})(\\d{2})(\\d{4})");
			pos = rxlen.indexIn(basename);

			auto rte = rxlen.capturedTexts();

			if (pos > -1 && (rxlen.cap(0).length() == 8)) {
				dateTimeFound = true;
			}
		}

		if (dateTimeFound)
		{
			QString dateStr = rxlen.cap(1);
			QString monthStr = rxlen.cap(2);
			QString yearStr = rxlen.cap(3);
			
			tmpDate.setDate(yearStr.toInt(), monthStr.toInt(), dateStr.toInt());
			tmpTime.setHMS(0, 0, 0, 0);
		}

		break;
	}
    case USERDEFINED:
    {
        getUserDefinedDateTime(tmpDate, tmpTime);

        break;
    }
    case USERDEFINED_USE_END_TIME_AS_START_TIME:
    {
        getUserDefinedDateTime(tmpDate, tmpTime);

        break;
    }
    }

    computedDateTime.setDate(tmpDate);
    computedDateTime.setTime(tmpTime);

    if (tmpDate.isValid() && tmpTime.isValid())
    {
        return 0;
    }
    else {
        return 1;
    }
}

void SetDateTimeDialog::getUserDefinedDateTime(QDate& tmpDate, QTime& tmpTime)
{
    int retVal = 1;

    // Check if the checkbox is triggered. If so, we need to extract the 
    // date and time according to the estimated end time of the previous video
    if (ui->useEndTimeAsStartTimeCheckBox->isChecked())
    {
        // Change the date-time format
        dateTimeFormat = USERDEFINED_USE_END_TIME_AS_START_TIME;

        // Try to compute the start time from the end of the previous video file
        if (prevDateTime.isNull())
        {
            retVal = getDateTimeFromEndOfPreviousFile(tmpDate, tmpTime);
        }
        else {
            tmpDate = prevDateTime.date();
            tmpTime = prevDateTime.time();

            retVal = 0;
        }        
    }
    else {
        dateTimeFormat = USERDEFINED;
    }

    if (retVal != 0)
    {
        // If we have not checked the checkbox or the computation of the start time was
        // unsuccessfull, input the date and time manually

        // Check if the date is different from the default
        if ((ui->dateTimeEdit->date().year() != 2000 || ui->dateTimeEdit->date().month() != 1
            || ui->dateTimeEdit->date().day() != 1) && ui->dateTimeEdit->dateTime().isValid())
        {
            tmpDate = ui->dateTimeEdit->date();
            tmpTime = ui->dateTimeEdit->time();
        }
    }
}

int SetDateTimeDialog::getDateTimeFromEndOfPreviousFile(QDate& tmpDate, QTime& tmpTime)
{
    if (prevFilename.isEmpty() || prevDateTime.isNull())
    {
        return 1;
    }

    // Open the previous video file
    cv::VideoCapture cap(prevFilename.toStdString());

    // Get the properties of the video file
    QSettings settings;
    settings.beginGroup("mainWindow");
    double userDefinedFps = settings.value("videoFps",0).toDouble();
    settings.endGroup();

    double fps;
    if (userDefinedFps == 0)
    {
        // Get the frame rate manually from the video
        fps = cap.get(cv::CAP_PROP_FPS);
    }
    else {
        // The frame rate has been set globally
        fps = userDefinedFps;
    }

    qint64 frameCount = cap.get(cv::CAP_PROP_FRAME_COUNT);

    if (fps > 0 && frameCount > 0)
    {
        // If the results are sane, we will calculate the end time of the previous video
        qint64 msecsPrFrame = 1. / fps * 1000;
        qint64 msecsToAdd = msecsPrFrame * (frameCount + 1);
        prevDateTime = prevDateTime.addMSecs(msecsToAdd);

        tmpDate = prevDateTime.date();
        tmpTime = prevDateTime.time();
        return 0;
    }
    else {
        return 1;
    }
}

