#ifndef DOUBLEMODULE_H
#define DOUBLEMODULE_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "dialogs/detectortypeselectiondialog.h"
#include "abstractcompounddetector.h"

class ruba::DoubleModule : public AbstractCompoundDetector
{

public:
    explicit DoubleModule(BaseDetectorParams baseParams, QWidget *parent = 0, QString configFile = "");

    ~DoubleModule();

    ruba::DetectorOutput GetOutput();

private:
    void setDetectorSpecificDefaultParameters();
    void InitLogs();
    ruba::LogInfoStruct createNewEvent();

    QList<QString> getDetectorSpecificExtendedDebugInfo();
    void updateDetectorSpecificLogic();

    QVector<int> chosenDetectors;
};

#endif // DOUBLEMODULE_H
