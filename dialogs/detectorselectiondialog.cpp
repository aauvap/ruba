#include <cmath>
#include "detectorselectiondialog.h"
#include "ui_detectorselectiondialog.h"
#include "opencvviewer.h"


using namespace std;
using namespace cv;
using namespace ruba;


DetectorSelectionDialog::DetectorSelectionDialog(QWidget *parent, std::vector<FrameStruct> frames, int detectorType,
                                                 QString moduleTitle, QString windowTitle, QIcon windowIcon,
                                                 QImage designImg, std::vector<ruba::MaskHandlerOutputStruct> dParams,
                                                 ruba::AdditionalSettingsStruct addSettings):
    QDialog(parent),
    ui(new Ui::DetectorSelectionDialog)
{
    ui->setupUi(this);

	Qt::WindowFlags flags;
	flags = Qt::Window | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint;
	setWindowFlags(flags);

     QGridLayout* mlayout = new QGridLayout();
     mlayout->setContentsMargins(0,0,0,0);
     mviewer = new OpenCVViewer(ui->maskViewer);
     mviewer->setGeometry(ui->maskViewer->geometry().x(), ui->maskViewer->geometry().y(), ui->maskViewer->width(), ui->maskViewer->height());
     mlayout->addWidget(mviewer);
     ui->maskViewer->setLayout(mlayout);
     mviewer->setImage(frames[0].Image);

     for (auto i = 0; i < frames.size(); ++i) {
         views.push_back(frames[i].viewInfo);
     }

     images.clear();
     overlayedImages.clear();
     overlayedImages.resize(views.size());

     // Get default size
     int width = 0, height = 0;
     if (!frames.empty())
     {
         width = frames[0].Image.cols;
         height = frames[0].Image.rows;
     }

     for (int i = 0; i < views.size(); ++i)
     {
         if (frames.size() > i)
         {
             images.push_back(frames[i].Image);
             frames[i].Image.copyTo(overlayedImages[i]);
         }
         else {
             images.push_back(cv::Mat::zeros(height, width, CV_8UC3)); // Create empty 3-channel image
             overlayedImages[i] = cv::Mat::zeros(height, width, CV_8UC3);
         }
     }

     this->layout()->setContentsMargins(11,11,11,11);

     setWindowTitle(windowTitle);
     setWindowIcon(windowIcon);
     
     this->detectorType = detectorType;
     this->moduleTitle = moduleTitle;
     this->views = views;

     // Initialize the additional settings struct and detector parameters
     this->addSettings = addSettings;
     this->nbrPDetectors = addSettings.nbrPDetectors;
     this->nbrMDetectors = addSettings.nbrMDetectors;
     this->nbrSDetectors = addSettings.nbrSDetectors;
     this->nbrTLDetectors = addSettings.nbrTLDetectors;

	 auto buttonPolicy = ui->defineBackgroundImageButton->sizePolicy();
	 buttonPolicy.setRetainSizeWhenHidden(true);
	 ui->defineBackgroundImageButton->setSizePolicy(buttonPolicy);

     initDefaults();

     // Connect the mouse signals
     connect(mviewer, SIGNAL(mouseClicked(QMouseEvent*)), this, SLOT(mouseHandler(QMouseEvent*)));
     connect(mviewer, SIGNAL(mouseDoubleClicked(QMouseEvent*)), this, SLOT(mouseHandler(QMouseEvent*)));
     connect(mviewer, SIGNAL(mouseMoved(QMouseEvent*)), this, SLOT(mouseHandler(QMouseEvent*)));
     connect(mviewer, SIGNAL(mouseReleased(QMouseEvent*)), this, SLOT(mouseHandler(QMouseEvent*)));


     // Connect the timer that draws the flow vectors of the MovementDetector
     ProcessTimer.setParent(this);
     ProcessTimer.setInterval(25);
     connect(&ProcessTimer, SIGNAL(timeout()), this, SLOT(processTimerTick()));
     ProcessTimer.start();

     this->ui->comboBox->blockSignals(false);

     // Set up the modality switch tabs
     this->currentViewIndex = 0;
     this->ui->viewSwitch->blockSignals(true);
     this->ui->viewSwitch->clear(); // Clear all tabs

	 if (views.size() > 1)
	 {
		 // Add tabs according to the input views
		 for (int i = 0; i < views.size(); ++i)
		 {
			 this->ui->viewSwitch->addTab(new QWidget(), tr("View %1")
				 .arg(views[i].viewName()));
		 }
	 }
	 else {
		 this->ui->viewSwitch->hide();
	 }
     this->ui->viewSwitch->blockSignals(false);
    
	 detectorTabs[Detectors::backgroundSubtraction] = new QGroupBox();
	 detectorTabs[Detectors::backgroundSubtraction]->setLayout(this->ui->backgroundSubtractionGroupBox->layout());
	 detectorTabs[Detectors::correctDirection] = new QGroupBox();
	 detectorTabs[Detectors::correctDirection]->setLayout(this->ui->correctDirectionGroupBox->layout());
	 detectorTabs[Detectors::wrongDirection] = new QGroupBox();
	 detectorTabs[Detectors::wrongDirection]->setLayout(this->ui->wrongDirectionGroupBox->layout());
	 detectorTabs[Detectors::trafficLight] = new QGroupBox();
	 detectorTabs[Detectors::trafficLight]->setLayout(this->ui->trafficLightGroupBox->layout());
	 detectorTabs[Detectors::timing] = new QGroupBox();
	 detectorTabs[Detectors::timing]->setLayout(this->ui->timingGroupBox->layout());

	 this->ui->tabWidget->hide();
	 int maxHorisontalSize = 0;

	 // Populate the new detector areas
	 for (int i = Detectors::backgroundSubtraction; i <= Detectors::timing; ++i)
	 {
		 this->ui->detectorConfigPage->layout()->addWidget(detectorTabs[i]);

		 if (detectorTabs[i]->sizeHint().width() > maxHorisontalSize)
		 {
			 maxHorisontalSize = detectorTabs[i]->sizeHint().width();
		 }
		 detectorTabs[i]->hide();
	 }

	 auto horizontalFiller = new QSpacerItem(maxHorisontalSize, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	 this->ui->detectorConfigPage->layout()->addItem(horizontalFiller);

     // Transfer the detector parameters
     if (!dParams.empty())
     {
         setDetectorParameters(dParams);
     }

     // Set the common parameters for all detectors
     for (int i = 0; i < detectorParams.size(); ++i)
     {
         detectorParams[i].IsInitialized = detectorParams[i].Masks.empty() ? false : true;
     }

     // Set up the ComboBox where the detectors are listed
     initDetectorComboBox();
     
     // Set up the logger GUI
     LogSettingsDisplayOptions displayOptions;
     displayOptions.showTimingDetectionBox = detectorType == DOUBLEMODULE ? true : false;

     QList<QString> detectorIdTexts;

     for (auto detector : detectorParams)
     {
         detectorIdTexts.push_back(detector.DetectorIdText);
     }

     logDialog = make_shared<LogSettingsDialog>(this, 
         displayOptions, 
         Utils::getDetectorAbbreviation(detectorType),
         detectorIdTexts,
         addSettings.initialLogPath);
     logDialog->setLogSettings(addSettings.generalLogSettings,
         addSettings.individualLogSettings);

	 ui->timingConfigPage->setLayout(logDialog->getTimingLayout());
	 ui->logsPage->setLayout(logDialog->getLogsLayout());
     //ui->newLogLayout->addWidget(logDialog.get());
     //ui->newLogLayout->setStretchFactor(logDialog.get(), 1);

	 connect(this->ui->contentsWidget,
		 SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
		 this, SLOT(changePage(QListWidgetItem*, QListWidgetItem*)));

     // Show the appropriate image in the design hint section
     if (!designImg.isNull())
     {
         QGroupBox* designHintBox = new QGroupBox(tr("Design hint"));
         AspectRatioPixmapLabel* designHintViewer = new AspectRatioPixmapLabel(designHintBox);
         designHintViewer->setPixmap(QPixmap::fromImage(designImg));

         QVBoxLayout* designHintLayout = new QVBoxLayout();
         designHintLayout->addWidget(designHintViewer);
         designHintBox->setLayout(designHintLayout);

		 ui->logsPage->layout()->addWidget(designHintBox);
     }

     
     ui->configSaveFilePathBox->setText(addSettings.configSaveFilePath);

	 if (detectorParams.size() == 1)
	 {
		 this->ui->comboBox->setCurrentIndex(this->ui->comboBox->count() - 1);
	 }

	 QSettings settings;
	 settings.beginGroup("detectorSelectionDialog");
	 restoreGeometry(settings.value("geometry").toByteArray());
	 settings.endGroup();

}

DetectorSelectionDialog::~DetectorSelectionDialog()
{
    ProcessTimer.stop();

	QSettings settings;
	settings.beginGroup("detectorSelectionDialog");
	settings.setValue("geometry", saveGeometry());
	settings.endGroup();

	delete ui;

}

QList<QString> DetectorSelectionDialog::initDetectorComboBox()
{
    this->ui->comboBox->blockSignals(true);
    this->ui->comboBox->clear();

    QStringList comboBoxList("Show all");
    QList<QString> outputDetectorList;

    for (size_t i = 0; i < detectorParams.size(); ++i)
    {
        QString tmpString = detectorParams[i].DetectorIdText + " - " + Utils::getDetectorName(detectorParams[i].DetectorType);

        outputDetectorList.append(tmpString);
        comboBoxList.append(tmpString);
    }

    this->ui->comboBox->addItems(comboBoxList);
    this->ui->comboBox->setCurrentIndex(1);
    this->ui->comboBox->blockSignals(false);

    // And then set the comboBox to the initial state
    this->ui->comboBox->setCurrentIndex(0);

    this->prevDetectorType = -1;

    return outputDetectorList;
}

void DetectorSelectionDialog::initDefaults()
{
    if (detectorParams.empty())
    {
        for (auto i = 0; i < addSettings.nbrPDetectors; ++i)
        {
            MaskHandlerOutputStruct tmpParams;
            tmpParams.DetectorType = PRESENCEDETECTOR;
            detectorParams.push_back(tmpParams);
        }

        for (auto i = 0; i < addSettings.nbrMDetectors; ++i)
        {
            MaskHandlerOutputStruct tmpParams;
            tmpParams.DetectorType = MOVEMENTDETECTOR;
            detectorParams.push_back(tmpParams);
        }

        for (auto i = 0; i < addSettings.nbrSDetectors; ++i)
        {
            MaskHandlerOutputStruct tmpParams;
            tmpParams.DetectorType = STATIONARYDETECTOR;
            detectorParams.push_back(tmpParams);
        }

        for (auto i = 0; i < addSettings.nbrTLDetectors; ++i)
        {
            MaskHandlerOutputStruct tmpParams;
            tmpParams.DetectorType = TRAFFICLIGHTDETECTOR;
            detectorParams.push_back(tmpParams);
        }
    }

    // Go through the defined detectors and initialize them to the defaults
    for (size_t i = 0; i < detectorParams.size(); ++i)
    {
        setDefaultDetectorParameters(int(i));
    }

}

void DetectorSelectionDialog::setDefaultDetectorParameters(int index)
{
    ProcessTimer.stop(); // Temporarily stop the timer while loading parameters
    int i = index;

    if (i >= detectorParams.size())
    {
        // Size mismatch
        return;
    }

    // Common settings
    detectorParams[i].views = views;

    detectorParams[i].Masks.clear();
	detectorParams[i].maskPoints.clear();
	detectorParams[i].IsMaskInitialized.clear();
	detectorParams[i].TriggerThreshold.clear();

    for (size_t j = 0; j < views.size(); ++j)
    {
        vector<Point> mPoints;
        detectorParams[i].maskPoints.push_back(mPoints);
        detectorParams[i].Masks.push_back(Mat());
        detectorParams[i].IsMaskInitialized.push_back(false);
        detectorParams[i].DisableDetectorAfter = -1; // Deactivated pr. default
        detectorParams[i].DisableDetectorDuration = -1;
    }

    if (detectorParams[i].DetectorType == PRESENCEDETECTOR)
    {
        detectorParams[i].BackgroundImages.resize(views.size());

        for (size_t j = 0; j < views.size(); ++j)
        {
            vector<int> tThreshold;
            tThreshold.push_back(30);
            detectorParams[i].TriggerThreshold.push_back(tThreshold);

            if ((images.size() > j) && !images[j].empty())
            {
                images[j].copyTo(detectorParams[i].BackgroundImages[j]);
            }
        }


    } else if (detectorParams[i].DetectorType == MOVEMENTDETECTOR)
    {
        detectorParams[i].MinVecLengthThreshold.clear();
        detectorParams[i].FlowRange.clear();
        detectorParams[i].RoadUserType.clear();


        for (size_t j = 0; j < views.size(); ++j)
        {
            vector<int> tThreshold = {200};
            vector<double> minVecLengthThreshold = {1.5};
            vector<Point> fRange = {Point(0,0)};
            vector<int> roadUserType = {-1};

            detectorParams[i].upperThresholdBoundMultiplier = 3;

            detectorParams[i].TriggerThreshold.push_back(tThreshold);
            detectorParams[i].MinVecLengthThreshold.push_back(minVecLengthThreshold);
            detectorParams[i].FlowRange.push_back(fRange);
            detectorParams[i].RoadUserType.push_back(roadUserType);
        }
    } else if (detectorParams[i].DetectorType == STATIONARYDETECTOR)
    {
        detectorParams[i].BackgroundImages.resize(views.size());
        detectorParams[i].MinVecLengthThreshold.clear();
        detectorParams[i].FlowRange.clear();
        detectorParams[i].RoadUserType.clear();

        for (size_t j = 0; j < views.size(); ++j)
        {
            vector<int> tThreshold = {30,100};
            vector<double> minVecLengthThreshold = {1.5};
            vector<Point> fRange = {Point(0,0)};
            vector<int> roadUserType = {-1};

            images[j].copyTo(detectorParams[i].BackgroundImages[j]);

            detectorParams[i].upperThresholdBoundMultiplier = 3;
            detectorParams[i].TriggerThreshold.push_back(tThreshold);
            detectorParams[i].MinVecLengthThreshold.push_back(minVecLengthThreshold);
            detectorParams[i].FlowRange.push_back(fRange);
            detectorParams[i].RoadUserType.push_back(roadUserType);
        }
    } else if (detectorParams[i].DetectorType == TRAFFICLIGHTDETECTOR)
	{
		detectorParams[i].trafficLightPositions.clear();

		for (int j = tlTypeClasses::RED; j <= tlTypeClasses::GREEN; ++j)
		{
			detectorParams[i].trafficLightPositions.push_back(Point(-1, -1));
		}
		
        detectorParams[i].trafficLightTypeTrigger = tlTypeClasses::AMBIGIOUS;
	} 
}

int DetectorSelectionDialog::setDetectorParameters(const vector<MaskHandlerOutputStruct>& dParams)
{
    bool timerRunning = ProcessTimer.isActive();
    ProcessTimer.stop();

   // Update the current configuration
    int nbrParams = int(dParams.size());

    if (nbrParams == (nbrPDetectors + nbrMDetectors + nbrSDetectors + nbrTLDetectors))
    {
        // Check the individual number of detectors
        int nbrP = 0, nbrM = 0, nbrS = 0, nbrTL = 0;
        for (size_t i = 0; i < dParams.size(); ++i)
        {
            switch (dParams[i].DetectorType)
            {
            case PRESENCEDETECTOR:
                nbrP++;
                break;
            case MOVEMENTDETECTOR:
                nbrM++;
                break;
            case STATIONARYDETECTOR:
                nbrS++;
                break;
			case TRAFFICLIGHTDETECTOR:
				nbrTL++;
				break;
            default:
                return -1;
            }
        }


        // Check number of views
        if (!dParams.empty() && (this->views.size() != dParams[0].views.size()))
        {
            return -1;
        }

        for (size_t i = 0; i < dParams.size(); ++i)
        {
            if (detectorParams[i].DetectorType != dParams[i].DetectorType)
            {
                // If the detector types are different, i.e. we are switching the detector type,
                // re-initialize the current detector parameters
                detectorParams[i].DetectorType = dParams[i].DetectorType;
                setDefaultDetectorParameters(i);
            }

            if (dParams[i].DetectorID > -1)
            {
                detectorParams[i].DetectorID = dParams[i].DetectorID;
            }

            if (!dParams[i].DetectorIdText.isEmpty())
            {
                detectorParams[i].DetectorIdText = dParams[i].DetectorIdText;
            }

            if (!dParams[i].DisableDetectorAfter > -2)
            {
                detectorParams[i].DisableDetectorAfter = dParams[i].DisableDetectorAfter;
            }
            else {
                detectorParams[i].DisableDetectorAfter = -1;
            }

            if (!dParams[i].DisableDetectorDuration > -2)
            {
                detectorParams[i].DisableDetectorDuration = dParams[i].DisableDetectorDuration;
            }
            else {
                detectorParams[i].DisableDetectorDuration = -1;
            }

            detectorParams[i].IsInitialized = true;
            if (!dParams[i].Masks.empty())
            {
                detectorParams[i].Masks = dParams[i].Masks;
                detectorParams[i].IsMaskInitialized.clear();
                for (int kk = 0; kk < detectorParams[i].Masks.size(); ++kk)
                {
                    if (detectorParams[i].Masks[kk].empty())
                    {
                        detectorParams[i].IsInitialized = false;
                        detectorParams[i].IsMaskInitialized.push_back(false);
                    } else {
                        // Check that mask dimensions correspond 
                        if (images.size() > kk &&
                            detectorParams[i].Masks[kk].size() == images[kk].size()) {
                            detectorParams[i].IsMaskInitialized.push_back(true);
                        }
                        else {
                            detectorParams[i].Masks[kk] = Mat();
                            detectorParams[i].IsMaskInitialized.push_back(false);
                        }
                    }
                }
            }

            if (!dParams[i].TriggerThreshold.empty())
            {
                detectorParams[i].TriggerThreshold = dParams[i].TriggerThreshold;
            }

            if (!dParams[i].maskPoints.empty())
            {
                detectorParams[i].maskPoints = dParams[i].maskPoints;
            }

            if (dParams[i].DetectorType == PRESENCEDETECTOR || dParams[i].DetectorType == STATIONARYDETECTOR)
            {
                if (!dParams[i].BackgroundImages.empty() && dParams[i].BackgroundImages.size() == views.size())
                {
                    detectorParams[i].BackgroundImages = dParams[i].BackgroundImages;

                    for (auto j = 0; j < detectorParams[i].BackgroundImages.size(); ++j) {
                        // Check if the dimensions of the background images conforms with the images 
                        // of the videos
                        if (images.size() > j && detectorParams[i].BackgroundImages[j].size() != images[j].size()) {
                            // The image sizes of the background images and the video images does not match. 
                            // Use the images of the videos as initial background image, instead
                            detectorParams[i].BackgroundImages[j] = images[j].clone();
                        }
                    }
                }
            }

            if (dParams[i].DetectorType == MOVEMENTDETECTOR || dParams[i].DetectorType == STATIONARYDETECTOR)
            {
                if (!dParams[i].FlowRange.empty())
                {
                    detectorParams[i].FlowRange = dParams[i].FlowRange;
                }

                if (!dParams[i].MinVecLengthThreshold.empty())
                {
                    detectorParams[i].MinVecLengthThreshold = dParams[i].MinVecLengthThreshold;
                }

                if (dParams[i].upperThresholdBoundMultiplier > 0)
                {
                    detectorParams[i].upperThresholdBoundMultiplier = dParams[i].upperThresholdBoundMultiplier;
                }

                detectorParams[i].EnableWrongDirection = dParams[i].EnableWrongDirection;

                if (detectorParams[i].EnableWrongDirection)
                {
                    // Make sure that the vectors all contain two elements, one for the right direction, another for the wrong direction
                    detectorParams[i].MinVecLengthThreshold.resize(views.size());
                    detectorParams[i].TriggerThreshold.resize(views.size());
                    detectorParams[i].FlowRange.resize(views.size());

                    for (auto j = 0; j < detectorParams[i].MinVecLengthThreshold.size(); ++j)
                    {
                        detectorParams[i].MinVecLengthThreshold[j].resize(2);
                        detectorParams[i].TriggerThreshold[j].resize(2);
                        detectorParams[i].FlowRange[j].resize(2);
                    }
                }
            }

            if (dParams[i].DetectorType == TRAFFICLIGHTDETECTOR)
            {
				detectorParams[i].trafficLightPositions.resize(3);
				
				for (auto j = 0; j < dParams[i].trafficLightPositions.size(); ++j)
				{
					if (dParams[i].trafficLightPositions[j].x > 0 && dParams[i].trafficLightPositions[j].y > 0)
					{
						detectorParams[i].trafficLightPositions[j] = dParams[i].trafficLightPositions[j];
					}
				}

                detectorParams[i].trafficLightTypeTrigger = dParams[i].trafficLightTypeTrigger;
            }
        }

    } else {
        return -1;
    }

    initDetectorComboBox(); // As the order of the detectors might have changed, re-initialize the comboBox as well
    checkConfiguration();    

    if (timerRunning)
    {
        ProcessTimer.start();
    }

    return 0;
}

void DetectorSelectionDialog::changePage(QListWidgetItem* current, QListWidgetItem* previous)
{
	if (!current)
	{
		current = previous;
	}

	this->ui->pagesWidget->setCurrentIndex(this->ui->contentsWidget->row(current));
}

void DetectorSelectionDialog::processTimerTick()
{
    // processTimerTick manages the task of drawing the flow vectors of all initialized MovementDetectors
    vector<int> maskIndices;

    // If "show all" is chosen, we will need to create a list of all masks
    // that correspond to an initialized MovementDetector

    if (this->ui->comboBox->currentText().at(0) == 'S' && this->ui->comboBox->currentText().at(1) == 'h')
    {
        for (int i = 1; i < (this->ui->comboBox->count()); ++i)
        {
            if (detectorParams[i-1].IsMaskInitialized[currentViewIndex] &&
                    (this->ui->comboBox->itemText(i).at(0) == 'M' ||
                     this->ui->comboBox->itemText(i).at(0) == 'S'))
            {
                maskIndices.push_back(i-1);
            }
        }
    } else if (this->ui->comboBox->currentText().at(0) == 'M' || this->ui->comboBox->currentText().at(0) == 'S') {
     // Otherwise, we need to check if we have currently picked a MovementDetector or a StationaryDetector
        if (detectorParams[this->ui->comboBox->currentIndex()-1].IsMaskInitialized[currentViewIndex])
        {
            maskIndices.push_back(this->ui->comboBox->currentIndex()-1);
        }
    }

    if (!maskIndices.empty())
    {
        cv::Mat paintedImg;
        overlayedImages[currentViewIndex].copyTo(paintedImg);

        for (size_t i = 0; i < maskIndices.size(); ++i)
        {
            int vectorNbr = 0;

            if (this->ui->comboBox->currentText().at(1) == 'h' || !(this->ui->comboBox->currentText().at(0) == 'M'))
            {
                vectorNbr = 0;
            } else if (detectorParams[maskIndices[i]].EnableWrongDirection)
            {
                //vectorNbr = this->ui->tabWidget->currentIndex(); Not supported
            }

            drawFlowVectors(paintedImg, maskIndices[i], vectorNbr);
        }

        mviewer->setImage(paintedImg);
    }

}

void DetectorSelectionDialog::drawFlowVectors(Mat img, int maskIndex, int vectorNbr)
{
    // Draw small flow vectors in the entire mask space

    // Check validity of the input
    if (int(detectorParams.size()) <= (maskIndex))
    {
        qDebug() << "Vectors out of range in drawFlowVectors (maskIndex)";
        return;
    } else if (int(detectorParams[maskIndex].FlowRange[currentViewIndex].size()) <= vectorNbr){
        qDebug() << "Vectors out of range in drawFlowVectors (vectorNbr)";
        return;
    }

    int angle1 = detectorParams[maskIndex].FlowRange[currentViewIndex][vectorNbr].x;
    int angle2 = detectorParams[maskIndex].FlowRange[currentViewIndex][vectorNbr].y;

    if (flowVecDirec.size() <= maskIndex)
    {
        for (int j = flowVecDirec.size(); j < maskIndex + 1; ++j)
        {
            std::vector<Directions> tmpVec;
            std::vector<int> tmpAngle;

            for (auto i = 0; i < vectorNbr + 1; ++i)
            {
                tmpVec.push_back(CLOCKWISE);
                tmpAngle.push_back(0);
            }

            flowVecDirec.push_back(tmpVec);
            currentFlowVecAngle.push_back(tmpAngle);
        }
    }

    if ((flowVecDirec.size() > maskIndex) && (flowVecDirec[maskIndex].size() <= vectorNbr))
    {
        for (auto i = flowVecDirec[maskIndex].size(); i < vectorNbr + 1; ++i)
        {
            flowVecDirec[maskIndex].push_back(CLOCKWISE);
            currentFlowVecAngle[maskIndex].push_back(0);

        }
    }

    if (flowVecDirec[maskIndex][vectorNbr] == 1)
    {
        // We are going counterclockwise. Distinguish between two cases: angle1 is above or below angle2
        if (angle1 < angle2)
        {
            if (currentFlowVecAngle[maskIndex][vectorNbr] < angle2)
            {
                currentFlowVecAngle[maskIndex][vectorNbr] += 2;
            } else {
                currentFlowVecAngle[maskIndex][vectorNbr] -= 2;
                flowVecDirec[maskIndex][vectorNbr] = CLOCKWISE;
            }
        } else {
            if (currentFlowVecAngle[maskIndex][vectorNbr] < 360 && (currentFlowVecAngle[maskIndex][vectorNbr] >= angle1))
            {
                currentFlowVecAngle[maskIndex][vectorNbr] += 2;
            } else if(currentFlowVecAngle[maskIndex][vectorNbr] < angle2){
                currentFlowVecAngle[maskIndex][vectorNbr] += 2;
            } else if(currentFlowVecAngle[maskIndex][vectorNbr] >= 360){
                currentFlowVecAngle[maskIndex][vectorNbr] = 1;
            } else {
                currentFlowVecAngle[maskIndex][vectorNbr] -= 2;
                flowVecDirec[maskIndex][vectorNbr] = CLOCKWISE;
            }
        }
    } else {
        // We are going clockwise. Distinguish between two cases: angle1 is above or below angle2
        if (angle1 < angle2)
        {
            if (currentFlowVecAngle[maskIndex][vectorNbr] > angle1)
            {
                currentFlowVecAngle[maskIndex][vectorNbr] -= 2;
            } else {
                currentFlowVecAngle[maskIndex][vectorNbr] += 2;
                flowVecDirec[maskIndex][vectorNbr] = COUNTERCLOCKWISE;
            }
        } else {
            if ((currentFlowVecAngle[maskIndex][vectorNbr] > 0) && (currentFlowVecAngle[maskIndex][vectorNbr] <= angle2))
            {
                currentFlowVecAngle[maskIndex][vectorNbr] -= 2;
            } else if (currentFlowVecAngle[maskIndex][vectorNbr] <= 0){
                currentFlowVecAngle[maskIndex][vectorNbr] = 359;
            } else if (currentFlowVecAngle[maskIndex][vectorNbr] > angle1)
            {
                currentFlowVecAngle[maskIndex][vectorNbr] -= 2;
            } else {
                currentFlowVecAngle[maskIndex][vectorNbr] += 2;
                flowVecDirec[maskIndex][vectorNbr] = COUNTERCLOCKWISE;
            }
        }
    }

    Mat flowVecImg, maskImage, redMask;
    img.copyTo(flowVecImg);

    maskImage = detectorParams[maskIndex].Masks[currentViewIndex];
    Mat tmpChannels[] =  {Mat::zeros(maskImage.rows, maskImage.cols, CV_8UC1), Mat::zeros(maskImage.rows, maskImage.cols, CV_8UC1), maskImage};
    merge(tmpChannels, 3, redMask);

	cv::Point minMaskPoints = cv::Point(20, 20);
	cv::Point maxMaskPoints = cv::Point(maskImage.cols, maskImage.rows);

	if (detectorParams[maskIndex].maskPoints[currentViewIndex].size() > 2)
	{
		// Determine the min and max points from the mask points to speed up the arrow generation
		minMaskPoints = cv::Point(maskImage.cols, maskImage.rows);
		maxMaskPoints = cv::Point(0, 0);

		for (auto i = 0; i < detectorParams[maskIndex].maskPoints[currentViewIndex].size(); ++i)
		{
			if (detectorParams[maskIndex].maskPoints[currentViewIndex][i].x < minMaskPoints.x)
			{
				minMaskPoints.x = detectorParams[maskIndex].maskPoints[currentViewIndex][i].x;
			}

			if (detectorParams[maskIndex].maskPoints[currentViewIndex][i].y < minMaskPoints.y)
			{
				minMaskPoints.y = detectorParams[maskIndex].maskPoints[currentViewIndex][i].y;
			}

			if (detectorParams[maskIndex].maskPoints[currentViewIndex][i].x > maxMaskPoints.x)
			{
				maxMaskPoints.x = detectorParams[maskIndex].maskPoints[currentViewIndex][i].x;
			}

			if (detectorParams[maskIndex].maskPoints[currentViewIndex][i].y > maxMaskPoints.y)
			{
				maxMaskPoints.y = detectorParams[maskIndex].maskPoints[currentViewIndex][i].y;
			}
		}

		minMaskPoints.x = minMaskPoints.x < 20 ? 20 : minMaskPoints.x;
		minMaskPoints.y = minMaskPoints.y < 20 ? 20 : minMaskPoints.y;
	}

    // Scale the flow vectors based on the dimensions of the input image. Width == 640 corresponds to a scale of 20
    int flowVecSizeMultiplier = img.cols > 640 ? (img.cols * 20) / 640: 20;
	int lineWidth = img.cols > 640 ? (img.cols * 1) / 640 : 1;

    for (int y = minMaskPoints.y; y < maxMaskPoints.y; y = y + flowVecSizeMultiplier)
    {
        for (int x = minMaskPoints.x; x < maxMaskPoints.x; x = x + flowVecSizeMultiplier)
        {
            Point arrowPos1(x,y), arrowPos2;
            arrowPos2.x = arrowPos1.x + cos(currentFlowVecAngle[maskIndex][vectorNbr]*M_PI/180)*(flowVecSizeMultiplier - flowVecSizeMultiplier/5);
            arrowPos2.y = arrowPos1.y - sin(currentFlowVecAngle[maskIndex][vectorNbr]*M_PI/180)*(flowVecSizeMultiplier - flowVecSizeMultiplier/5);

            if (vectorNbr == 0)
            {
                cvUtils::drawArrow(flowVecImg, arrowPos1, arrowPos2, Scalar(255,255,255), flowVecSizeMultiplier/3, 1, LINE_AA);
            } else {
                // Draw the arrows for the wrong direction as black
                cvUtils::drawArrow(flowVecImg, arrowPos1, arrowPos2, Scalar(0,0,0), flowVecSizeMultiplier/3, 1, LINE_AA);
            }

        }
    }

    flowVecImg.copyTo(img, redMask);

}

void DetectorSelectionDialog::computeMaskCentre()
{
    int index = this->ui->comboBox->currentIndex()-1;

    // Find the centre of the mask points:
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    Mat tmpMask;
    detectorParams[index].Masks[currentViewIndex].copyTo(tmpMask);

    if (!tmpMask.empty())
    {
        findContours(tmpMask, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0) );

        // Get the moments
        vector<Moments> mu(contours.size() );
        for( size_t j = 0; j < contours.size(); j++ )
        {
            mu[j] = moments( contours[j], false );
        }

        // Get the mass centers:
        vector<Point2f> mc( contours.size() );
        for( size_t j = 0; j < contours.size(); j++ )
        {
            mc[j] = Point2f( mu[j].m10/mu[j].m00 , mu[j].m01/mu[j].m00 );
        }

        if (!mc.empty())
        {
            maskCentre = mc[0];
        }
    }
}

void DetectorSelectionDialog::drawTLPoints() {

    int index = this->ui->comboBox->currentIndex() - 1;
    cv::Mat paintedImg;
    overlayedImages[currentViewIndex].copyTo(paintedImg);
    
    int circleSize = ceil(paintedImg.rows * 0.005); // We want around 7 at a height of 1080pixels
    if (index >= 0) {
		for (int i = tlTypeClasses::RED; i <= tlTypeClasses::GREEN; ++i)
		{
			if (detectorParams[index].trafficLightPositions[i].x != 0
				&& detectorParams[index].trafficLightPositions[i].y != 0)
			{
				int greeenColor = i == GREEN || i == YELLOW ? 255 : 0;
				int redColor = i == RED || i == YELLOW ? 255 : 0;
				int blueColor = 0;

				Scalar drawingColor = Scalar(blueColor, greeenColor, redColor);
				Scalar lightDrawingColor = Scalar(blueColor, greeenColor, redColor) * 0.5;

				cv::circle(paintedImg, detectorParams[index].trafficLightPositions[i], circleSize * 1.5, lightDrawingColor,
					cv::FILLED, 8, 0);
				cv::circle(paintedImg, detectorParams[index].trafficLightPositions[i], circleSize, drawingColor,
					cv::FILLED, 8, 0);
				
			}
		}
    }
    mviewer->setImage(paintedImg); 
}

void DetectorSelectionDialog::drawAngleHelper(int angle1, int angle2, bool recomputeCentre)
{
    // drawAngleHelper draws helper lines from the centre of the masks that visualizes the chosen flow angle range

    if (recomputeCentre)
    {
        computeMaskCentre();
    }

    // Calculate the distance between the angles
    int angleDiff;
    if (angle2 >= angle1)
    {
        angleDiff = angle2 - angle1;
    } else {
        angleDiff = 360-angle1 + angle2;
    }

    Mat paintedImg;
    overlayedImages[currentViewIndex].copyTo(paintedImg);

	// Define the length of the helper lines:
	int helplerLineLength = paintedImg.cols > 640 ? (paintedImg.cols * 60) / 640 : 60;
	int lowerCircleRadius = paintedImg.cols > 640 ? (paintedImg.cols * 7) / 640 : 7;
	int lineWidth = paintedImg.cols > 640 ? (paintedImg.cols * 1) / 640 : 1;

    // Draw line 1:
    Point newPos;
    newPos.x = maskCentre.x + cos(angle1*M_PI/180)*helplerLineLength;
    newPos.y = maskCentre.y - sin(angle1*M_PI/180)*helplerLineLength;
    line(paintedImg, maskCentre, newPos, Scalar(255,255,255), lineWidth ,LINE_AA);

    // Draw line 2
    newPos.x = maskCentre.x + cos(angle2*M_PI/180)*helplerLineLength;
    newPos.y = maskCentre.y - sin(angle2*M_PI/180)*helplerLineLength;
    line(paintedImg, maskCentre, newPos, Scalar(255,255,255), lineWidth, LINE_AA);

    // Draw the circle arcs:
    ellipse(paintedImg, maskCentre, Size(lowerCircleRadius, lowerCircleRadius), -angle1, 0, -angleDiff, Scalar(255,255,255), -1);
    ellipse(paintedImg, maskCentre, Size(helplerLineLength/2, helplerLineLength/2), -angle1, 0, -angleDiff, Scalar(255,255,255), lineWidth, LINE_AA);

    mviewer->setImage(paintedImg);
}

void DetectorSelectionDialog::on_comboBox_currentIndexChanged(const QString &arg1)
{
    on_comboBox_currentIndexChanged();
}

void DetectorSelectionDialog::on_comboBox_currentIndexChanged()
{
    int index = this->ui->comboBox->currentIndex()-1;

    vector<int> maskIndices;
    bool isInitialized;

    if (index >= 0 && index < detectorParams.size())
    {
        isInitialized = detectorParams[index].IsMaskInitialized[currentViewIndex];
    } else {
        isInitialized = false;
    }

    int detectorType = -1;

    if ((index >= 0) && (index < detectorParams.size()))
    {
        detectorType = detectorParams[index].DetectorType;
    }

    setUpLayout(isInitialized, index);

    if (detectorType == MOVEMENTDETECTOR || detectorType == STATIONARYDETECTOR || detectorType == -1)
    {
        this->ProcessTimer.start(); // Start the timer
    }

    // Deactivate the drawing on the screen if the mask is already defined
    if (isInitialized)
    {
		this->drawingMode = DrawingMode::Disabled;
    } else {
		this->drawingMode = DrawingMode::Mask;
    }

    if (isInitialized && (detectorType != -1))
    {
        // Draw the mask
        maskIndices.push_back(index);
        drawMasks(maskIndices);

        // Find the centre of the mask
        computeMaskCentre();

        // Show proper text on the drawFinishResetMaskButton
        if (!detectorParams[index].maskPoints[currentViewIndex].empty())
        {
            this->ui->drawFinishResetMaskButton->setText("Redraw mask");
            ui->drawFinishResetMaskButton->setToolTip("Redraw mask");
            this->ui->drawFinishResetMaskButton->setIcon(QIcon("://icons/pencil--plus.png"));
        } else {
            this->ui->drawFinishResetMaskButton->setText("Reset mask");
            this->ui->drawFinishResetMaskButton->setToolTip("Reset mask");
            this->ui->drawFinishResetMaskButton->setIcon(QIcon("://icons/cross-script.png"));
        }

    }
    else if (index >= 0 && index < detectorParams.size() && !detectorParams[index].maskPoints[currentViewIndex].empty()) {
        // The mask has not been finished but mask points do exist
		drawingMode = DrawingMode::Mask;

        this->ui->drawFinishResetMaskButton->setText(tr("Finish mask"));
        this->ui->drawFinishResetMaskButton->setIcon(QIcon("://icons/tick.png"));
        this->ui->drawFinishResetMaskButton->setToolTip(tr("Finish mask"));

        if (detectorParams[index].maskPoints[currentViewIndex].size() < 2)
        {
            this->ui->drawFinishResetMaskButton->setEnabled(false);
        }
        else {
            this->ui->drawFinishResetMaskButton->setEnabled(true);
        }

        drawMasks(vector<int>(1, index));


    } else {
        // Show the non-overlayed image as no mask has been set
        images[currentViewIndex].copyTo(overlayedImages[currentViewIndex]);
        mviewer->setImage(images[currentViewIndex]);

        // Show proper text on the drawFinishResetMaskButton
        this->ui->drawFinishResetMaskButton->setText("Draw mask");
        this->ui->drawFinishResetMaskButton->setIcon(QIcon("://icons/pencil.png"));
        ui->drawFinishResetMaskButton->setToolTip("Draw mask");
    }

    if (detectorType == -1)
    {
        // The "show all" button has been chosen. Show all masks

        for (int i = 1; i < (this->ui->comboBox->count()); ++i)
        {
            if (detectorParams[i-1].IsMaskInitialized[currentViewIndex])
            {
                maskIndices.push_back(i-1);
            }
        }

        drawMasks(maskIndices);        
        drawingMode = DrawingMode::Disabled; // We cannot draw a mask when showing all the masks at once
    }


}

int DetectorSelectionDialog::convertFileFormat(QString file)
{
    QFile fileh(file);

    if (!fileh.open(QIODevice::ReadOnly)) {
        QMessageBox::information(this, tr("Error"), tr("Failed to convert detector. Could not open file"));
        return 1;
    }

    QTextStream in(&fileh);

    std::vector<QString> lines;

    while (!in.atEnd()) {
        QString line = in.readLine();
        
        // Convert EdgeDetector -> PresenceDetector
        line = line.replace(QRegExp("E(\\d{1})"), "P\\1");

        // Convert FlowDetector -> MovementDetector
        line = line.replace(QRegExp("F(\\d{1})"), "M\\1");

        // Convert RGB -> View 1
        line = line.replace(QRegExp("-RGB:"), "-View1:");

        // Convert Thermal -> View 2
        line = line.replace(QRegExp("-Thermal:"), "-View2:");

        lines.push_back(line);
    }

    fileh.close(); // Close the old file

    // Open the file for write access
    QFile wFileh(file);

    if (!wFileh.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Error"), tr("Failed to convert detector. Could not open file"));
        return 1;
    }

    QTextStream out(&wFileh);

    for (auto i = 0; i < lines.size(); ++i) {
        if (i == 1 && !lines[i].contains("rubaVersion")) {
            // If we are on the second line, write the rubaVersion info if it does not exist already
            out << QString("rubaVersion: [ %1, %2, %3 ]\n").arg(RUBA_VERSION_MAJOR).arg(RUBA_VERSION_MINOR).arg(RUBA_VERSION_REVISION);
        }
        else if (lines[i].contains("rubaVersion"))
        {
            // The version is too old. Update string to current version
            lines[i] = QString("rubaVersion: [ %1, %2, %3 ]").arg(RUBA_VERSION_MAJOR).arg(RUBA_VERSION_MINOR).arg(RUBA_VERSION_REVISION);
        }

        out << lines[i] << "\n";
    }

    return 0;

}

void DetectorSelectionDialog::setUpLayout(bool isDetectorInitialized, int index)
{
    int detectorType = -1;
    
    if ((index >= 0) && (index < detectorParams.size()))
    {
        detectorType = detectorParams[index].DetectorType;
    }

	ui->tabWidget->hide();

	if (prevDetectorType != detectorType)
	{
		for (int i = Detectors::backgroundSubtraction; i <= Detectors::timing; ++i)
		{
			detectorTabs[i]->hide();
		}
	}


    if (detectorType == PRESENCEDETECTOR)
    {
        // Set up the GUI for defining an PresenceDetector
        ui->defineBackgroundImageButton->setVisible(true);
        ui->saveBackgroundImageButton->setVisible(true);
        ui->backgroundImageLabel->setVisible(true);      

        // Reintroduce the relevant tabs for the PresenceDetector
        if (prevDetectorType != detectorType)
        {
			detectorTabs[Detectors::backgroundSubtraction]->show();
			detectorTabs[Detectors::backgroundSubtraction]->setTitle(tr("Background subtraction"));
        }

		detectorTabs[Detectors::backgroundSubtraction]->setEnabled(isDetectorInitialized);

        // Configure buttons
        this->ui->paramLabelTab11->setText(tr("Minimum occupation percentage"));

        // Change the value of the parameter boxes
        configureValueBox(this->ui->parameterBoxTab11, detectorParams[index].TriggerThreshold[currentViewIndex][0], 0, 1);

    } else if (detectorType == MOVEMENTDETECTOR)
    {
        // Set up the GUI for defining an MovementDetector
        this->ui->defineBackgroundImageButton->setVisible(false);
        ui->saveBackgroundImageButton->setVisible(false);
        ui->backgroundImageLabel->setVisible(false);

        // Reintroduce the relevant tabs for the MovementDetector
        if (prevDetectorType != detectorType)
        {
			detectorTabs[Detectors::correctDirection]->show();
			detectorTabs[Detectors::correctDirection]->setTitle(tr("Movement detection"));

            if (detectorParams[index].EnableWrongDirection)
            {
				detectorTabs[Detectors::wrongDirection]->show();
				detectorTabs[Detectors::wrongDirection]->setTitle(tr("Wrong direction of movement"));
            }
        }

		detectorTabs[Detectors::correctDirection]->setEnabled(isDetectorInitialized);
		detectorTabs[Detectors::wrongDirection]->setEnabled(isDetectorInitialized);


        this->ui->paramLabelTab21->setText(tr("Minimum speed"));
        this->ui->paramLabelTab31->setText(tr("Minimum speed"));
        this->ui->paramLabelTab22->setText(tr("Trigger threshold"));
        this->ui->paramLabelTab32->setText(tr("Trigger threshold"));

		ui->correctMovementDirectionGroupBox->setVisible(true);


        // Change the value of the parameter boxes
        configureValueBox(this->ui->parameterBoxTab21, detectorParams[index].MinVecLengthThreshold[currentViewIndex][0], 1, 0.1);
        configureValueBox(this->ui->parameterBoxTab22, detectorParams[index].TriggerThreshold[currentViewIndex][0], 0, 10);
        configureValueBox(this->ui->angleBox21, detectorParams[index].FlowRange[currentViewIndex][0].x, 0, 1, false);
        configureValueBox(this->ui->angleBox22, detectorParams[index].FlowRange[currentViewIndex][0].y, 0, 1, false);

        if (detectorParams[index].EnableWrongDirection)
        {
			if (detectorParams[index].MinVecLengthThreshold[currentViewIndex].size() < 2 ||
				detectorParams[index].TriggerThreshold[currentViewIndex].size() < 2)
			{
				detectorParams[index].MinVecLengthThreshold[currentViewIndex].resize(2);
				detectorParams[index].TriggerThreshold[currentViewIndex].resize(2);
			}

            configureValueBox(this->ui->parameterBoxTab31, detectorParams[index].MinVecLengthThreshold[currentViewIndex][1], 1, 0.1);
            configureValueBox(this->ui->parameterBoxTab32, detectorParams[index].TriggerThreshold[currentViewIndex][1], 0, 10);
            configureValueBox(this->ui->angleBox31, detectorParams[index].FlowRange[currentViewIndex][1].x, 0, 1);
            configureValueBox(this->ui->angleBox32, detectorParams[index].FlowRange[currentViewIndex][1].y, 0, 1);
        }

    } else if (detectorType == STATIONARYDETECTOR){
        // Set up the GUI for defining an StationaryDetector
        ui->defineBackgroundImageButton->setVisible(true);
        ui->saveBackgroundImageButton->setVisible(true);
        ui->backgroundImageLabel->setVisible(true);

        // Reintroduce the relevant tabs for the StationaryDetector
        if (prevDetectorType != detectorType)
        {            
			detectorTabs[Detectors::backgroundSubtraction]->show();
			detectorTabs[Detectors::backgroundSubtraction]->setTitle(tr("Background subtraction"));

			detectorTabs[Detectors::correctDirection]->show();
			detectorTabs[Detectors::correctDirection]->setTitle(tr("Movement in all directions"));
        }

		detectorTabs[Detectors::backgroundSubtraction]->setEnabled(isDetectorInitialized);
		detectorTabs[Detectors::correctDirection]->setEnabled(isDetectorInitialized);

        // Configure buttons
        this->ui->paramLabelTab11->setText(tr("Minimum occupation percentage"));

        this->ui->paramLabelTab21->setText(tr("Minimum speed"));
        this->ui->paramLabelTab22->setText(tr("Max vector count"));

        // Show relevant controls and hide irrelevant controls
        this->ui->parameterBoxTab11->setVisible(true);
        ui->correctMovementDirectionGroupBox->setVisible(false);

        // Change the value of the parameter boxes
        configureValueBox(this->ui->parameterBoxTab11, detectorParams[index].TriggerThreshold[currentViewIndex][0], 0, 1);

        configureValueBox(this->ui->parameterBoxTab21, detectorParams[index].MinVecLengthThreshold[currentViewIndex][0], 1, 0.1);
        configureValueBox(this->ui->parameterBoxTab22, detectorParams[index].TriggerThreshold[currentViewIndex][1], 0, 10);

    }
    else if (detectorType == TRAFFICLIGHTDETECTOR)
    {
        // Set up the GUI for defining an TrafficLightDetector
        this->ui->defineBackgroundImageButton->setVisible(false);
        ui->saveBackgroundImageButton->setVisible(false);
        ui->backgroundImageLabel->setVisible(false);

        // Reintroduce the relevant tabs for the TrafficLightDetector
        if (prevDetectorType != detectorType)
        {
			detectorTabs[Detectors::trafficLight]->show();
			detectorTabs[Detectors::trafficLight]->setTitle(tr("Traffic Light"));
        }

		detectorTabs[Detectors::trafficLight]->setEnabled(isDetectorInitialized);

        // Configure buttons if not drawing
		if (drawingMode != DrawingMode::TrafficLight)
		{
			configureTrafficLightAnnotationButton(ui->redAnnotateButton, ui->redCheckBox, detectorParams[index].trafficLightPositions[RED], RED);
			configureTrafficLightAnnotationButton(ui->yellowAnnotateButton, ui->yellowCheckBox, detectorParams[index].trafficLightPositions[YELLOW], YELLOW);
			configureTrafficLightAnnotationButton(ui->greenAnnotateButton, ui->greenCheckBox, detectorParams[index].trafficLightPositions[GREEN], GREEN);
		}

		switch (detectorParams[index].trafficLightTypeTrigger)
		{
		case tlTypeClasses::RED:
		{
			ui->redButton->setChecked(true);
			break;
		}
		case tlTypeClasses::YELLOW:
		{
			ui->yellowButton->setChecked(true);
			break;
		}
		case tlTypeClasses::GREEN:
		{
			ui->greenButton->setChecked(true);
			break;
		}
		}

	} else {
        // We have chosen to show no detector. Hide all controls

		ui->maskManipulationTab->hide();
        ui->drawMaskTab->setEnabled(false);
		ui->loadSaveTab->setEnabled(false);
    }

    if (detectorType >= 0)
    {
        // Introduce common UI for all detectors
		ui->maskManipulationTab->show();
        ui->drawMaskTab->setEnabled(true);
        ui->loadSaveTab->setEnabled(true);

        ui->drawFinishResetMaskButton->setEnabled(true);

        ui->addToMaskButton->setEnabled(drawingMode == DrawingMode::Mask);
        ui->subtractFromMaskButton->setEnabled(drawingMode == DrawingMode::Mask);
        ui->selectPrevMaskPointButton->setEnabled(drawingMode == DrawingMode::Mask);
        ui->selectNextMaskPointButton->setEnabled(drawingMode == DrawingMode::Mask);
        
        ui->loadMaskButton->setEnabled(true);
        ui->loadMaskPointsButton->setEnabled(true);

        ui->saveMaskButton->setEnabled(drawingMode == DrawingMode::Disabled && isDetectorInitialized);
        ui->saveMaskPointsButton->setEnabled(drawingMode == DrawingMode::Disabled && isDetectorInitialized);

		bool maskArrowsEnabled = (drawingMode == DrawingMode::Mask || drawingMode == DrawingMode::TrafficLight);
        ui->moveMaskPointUpButton->setEnabled(maskArrowsEnabled);
        ui->moveMaskPointDownButton->setEnabled(maskArrowsEnabled);
        ui->moveMaskPointLeftButton->setEnabled(maskArrowsEnabled);
        ui->moveMaskPointRightButton->setEnabled(maskArrowsEnabled);

        // If any of the timing functionalities are enabled, show the timing tab
        int maxTriggeredDuration = logDialog->getMaxTriggeredDuration(detectorParams[index].DetectorIdText);

        if ((detectorParams[index].DisableDetectorAfter >= 0) ||
            (detectorParams[index].DisableDetectorDuration >= 0) ||
            (maxTriggeredDuration >= 0))
        {
            if (prevDetectorType != detectorType)
            {
                // Reintroduce the timing tab
				detectorTabs[Detectors::timing]->show();
				detectorTabs[Detectors::timing]->setTitle(tr("Timing"));

            }

            // Enable/disable the individual timing options

            if (detectorParams[index].DisableDetectorAfter < 0)
            {
                // Disable the functionality
                this->ui->detectorTimingBox1->setVisible(false);
                this->ui->detectorTimingLabel1->setVisible(false);
            }
            else {
                // Set the current value
                this->ui->detectorTimingBox1->setVisible(true);
                this->ui->detectorTimingLabel1->setVisible(true);
                
                configureValueBox(this->ui->detectorTimingBox1, detectorParams[index].DisableDetectorAfter, 0, 50);
            }

            if (detectorParams[index].DisableDetectorDuration < 0)
            {
                // Disable the functionality
                this->ui->detectorTimingBox2->setVisible(false);
                this->ui->detectorTimingLabel2->setVisible(false);
            }
            else {
                // Set the current value
                this->ui->detectorTimingBox2->setVisible(true);
                this->ui->detectorTimingLabel2->setVisible(true);

                configureValueBox(this->ui->detectorTimingBox2, detectorParams[index].DisableDetectorDuration, 0, 50);
            }

            if (maxTriggeredDuration < 0)
            {
                // Disable the functionality
                this->ui->detectorTimingBox3->setVisible(false);
                this->ui->detectorTimingLabel3->setVisible(false);
            }
            else {
                // Set the current value
                this->ui->detectorTimingBox3->setVisible(true);
                this->ui->detectorTimingLabel3->setVisible(true);

                configureValueBox(this->ui->detectorTimingBox3, maxTriggeredDuration, 0, 50);
            }            
        }
    }

    prevDetectorType = detectorType;
	
}

void DetectorSelectionDialog::configureValueBox(QDoubleSpinBox* boxHandle, double value,
                                                int decimals, double singleStep, bool blockSignals)
{
    boxHandle->blockSignals(blockSignals);
    boxHandle->setDecimals(decimals);
    boxHandle->setSingleStep(singleStep);
    boxHandle->setValue(value);
    boxHandle->blockSignals(false);
}

void DetectorSelectionDialog::configureTrafficLightAnnotationButton(QToolButton* buttonHandle, QCheckBox* checkBoxHandle, cv::Point point, tlTypeClasses lightType)
{
    if (point.x >= 0 && point.y >= 0)
    {
        checkBoxHandle->setChecked(true);

        switch (lightType)
        {
        case tlTypeClasses::RED:
            buttonHandle->setIcon(QIcon("://icons/traffic-light-red.png"));
            break;
        case tlTypeClasses::YELLOW:
            buttonHandle->setIcon(QIcon("://icons/traffic-light-yellow.png"));
            break;
        case tlTypeClasses::GREEN:
            buttonHandle->setIcon(QIcon("://icons/traffic-light-green.png"));
            break;
        }
    }
    else {
        checkBoxHandle->setChecked(false);
    }
}

void DetectorSelectionDialog::drawMasks(vector<int> maskIndices)
{
    // Show the mask as overlay on the background image
    DType detectorType;
    Scalar maskColor;

    // Check if the image exist
    int index = currentViewIndex;

    if (images.size() <= index)
    {
        // The image does not exist
        return;
    }
    else if (images[index].empty()){
        // The requested image is empty
        return;
    }

    if (maskIndices.size() > 0)
    {
        if (maskIndices.size() > 1 || detectorParams[maskIndices[0]].DetectorType == MOVEMENTDETECTOR
            || detectorParams[maskIndices[0]].DetectorType == TRAFFICLIGHTDETECTOR ||
            (ui->comboBox->currentText() == tr("Show all")))
        {
            // If we have chosen to show masks of several detectors, or the chosen detector
            // is a MovementDetector or TrafficLightDetector, we will show them upon the default background image
            images[currentViewIndex].copyTo(overlayedImages[currentViewIndex]);

        }
        else {
            // If we have chosen only one, and the detector is an PresenceDetector or StationaryDetector,
            // use the specific background image of that detector
            detectorParams[maskIndices[0]].BackgroundImages[currentViewIndex].copyTo(overlayedImages[currentViewIndex]);
        }
    }


    for (size_t i = 0; i < maskIndices.size(); ++i)
    {
        if (this->ui->comboBox->itemText(maskIndices[i]+1).at(0) == 'P')
        {
            detectorType = PRESENCEDETECTOR;
            maskColor = Scalar(255,0,0); // Paint the mask as blue
        } else if (this->ui->comboBox->itemText(maskIndices[i]+1).at(0) == 'M') {
            detectorType = MOVEMENTDETECTOR;
            maskColor = Scalar(0,0,255); // Paint the mask as red
        } else if (this->ui->comboBox->itemText(maskIndices[i]+1).at(0) == 'S'
                   && !(this->ui->comboBox->itemText(maskIndices[i]+1).at(1) == 'h'))
        {
            detectorType = STATIONARYDETECTOR;
            maskColor = Scalar(0,255,0); // Paint the mask as green
		}
		else if (ui->comboBox->itemText(maskIndices[i] + 1).at(0) == 'T')
		{
			detectorType = TRAFFICLIGHTDETECTOR;
			maskColor = Scalar(0, 242, 255);
		}

		if (detectorParams[maskIndices[i]].IsMaskInitialized[currentViewIndex]
			&& detectorParams[maskIndices[i]].Masks[currentViewIndex].rows > 0
			&& drawingMode != DrawingMode::TrafficLight)
        {
            // The mask is finished. Show the filled mask
            overlayedImages[currentViewIndex] = cvUtils::colorOverlay(maskColor, float(0.4), overlayedImages[currentViewIndex],
                                                                     detectorParams[maskIndices[i]].Masks[currentViewIndex]);

        } else {
            // The mask is not yet finished. Show the points and lines
            int sizeMultiplier = overlayedImages[currentViewIndex].cols / 640;

            if (sizeMultiplier < 1)
            {
                sizeMultiplier = 1;
            }


            if (detectorParams[maskIndices[i]].maskPoints[currentViewIndex].size() > 1)
            {
                polylines(overlayedImages[currentViewIndex], detectorParams[maskIndices[i]].maskPoints[currentViewIndex],
                        false, maskColor, sizeMultiplier, LINE_AA);

				if (drawingMode != DrawingMode::TrafficLight)
				{
					// Get the selected point in the line and paint it
					if (currentMaskPoint >= detectorParams[maskIndices[i]].maskPoints[currentViewIndex].size() || currentMaskPoint < 0) {
						// The current mask point is outside the range of the array. Reset it 
						currentMaskPoint = detectorParams[maskIndices[i]].maskPoints[currentViewIndex].size() - 1;
						qDebug() << "Resetting currentMaskPoint";
					}

					circle(overlayedImages[currentViewIndex], detectorParams[maskIndices[i]].maskPoints[currentViewIndex][currentMaskPoint],
						3 * sizeMultiplier, Scalar(255, 255, 255), -1, LINE_AA);
					circle(overlayedImages[currentViewIndex], detectorParams[maskIndices[i]].maskPoints[currentViewIndex][currentMaskPoint],
						2 * sizeMultiplier, maskColor, -1, LINE_AA);
				}
				else {
					// The mask is finished, but we don't want to show the filled version. Connect the first and last points
					polylines(overlayedImages[currentViewIndex], 
						std::vector<cv::Point>{detectorParams[maskIndices[i]].maskPoints[currentViewIndex].front(), 
						detectorParams[maskIndices[i]].maskPoints[currentViewIndex].back()},
						false, maskColor, sizeMultiplier, LINE_AA);

				}

            } else if (detectorParams[maskIndices[i]].maskPoints[currentViewIndex].size() == 1){
                // We have only marked one point. Create a dot
                circle(overlayedImages[currentViewIndex], detectorParams[maskIndices[i]].maskPoints[currentViewIndex][0],
                    3 * sizeMultiplier, Scalar(255, 255, 255), -1, LINE_AA);
                circle(overlayedImages[currentViewIndex], detectorParams[maskIndices[i]].maskPoints[currentViewIndex][0],
                        2 * sizeMultiplier, maskColor, -1, LINE_AA);
            }

        }


    }

    mviewer->setImage(overlayedImages[currentViewIndex]);

	if (detectorType == TRAFFICLIGHTDETECTOR)
	{
		drawTLPoints();
	}
}

vector<MaskHandlerOutputStruct> DetectorSelectionDialog::getDetectorParameters()
{
    this->ProcessTimer.stop();
    return detectorParams;
}

AdditionalSettingsStruct DetectorSelectionDialog::getAdditionalSettings()
{
    // Get the log settings
    logDialog->getLogSettings(addSettings.generalLogSettings, addSettings.individualLogSettings);

    return addSettings;
}

 void DetectorSelectionDialog::setAdditionalSettings(AdditionalSettingsStruct additionalSettings)
{
    addSettings = additionalSettings;

    // Set the log settings
    logDialog->setLogSettings(addSettings.generalLogSettings, addSettings.individualLogSettings);
}

int DetectorSelectionDialog::currentDetectorType()
{
    int detectorType;

    if (this->ui->comboBox->count() > 0)
    {
        if (this->ui->comboBox->currentText().at(0) == 'P')
        {
            detectorType = PRESENCEDETECTOR;
            this->ProcessTimer.stop(); // Stop the timer.
        } else if (this->ui->comboBox->currentText().at(0) == 'M')
        {
            detectorType = MOVEMENTDETECTOR;
        } else if (this->ui->comboBox->currentText().at(0) == 'S'
                   && !(this->ui->comboBox->currentText().at(1) == 'h')){
            detectorType = STATIONARYDETECTOR;
        } else if (this->ui->comboBox->currentText().at(0) == 'T') {
			detectorType = TRAFFICLIGHTDETECTOR;
		}
		else {
            detectorType = -1;
        }
    } else {
        detectorType = -1;
    }

    return detectorType;
}

void DetectorSelectionDialog::on_parameterBoxTab11_valueChanged(double arg1)
{
    // Find the detector type:
    int detectorType = currentDetectorType();
    int detectorNbr = this->ui->comboBox->currentIndex()-1;


    if (detectorType == PRESENCEDETECTOR)
    {
        detectorParams[detectorNbr].TriggerThreshold[currentViewIndex].clear();
        detectorParams[detectorNbr].TriggerThreshold[currentViewIndex].push_back(arg1);
    } else if (detectorType == MOVEMENTDETECTOR)
    {
        // Do nothing. No parameter set here
    } else if (detectorType == STATIONARYDETECTOR)
    {
        detectorParams[detectorNbr].TriggerThreshold[currentViewIndex][0] = arg1;
	} else if (detectorType == TRAFFICLIGHTDETECTOR)
	{
		// Do nothing. No parameter set here
	}

}

void DetectorSelectionDialog::on_parameterBoxTab21_valueChanged(double arg1)
{
    // Find the detector type:
    int detectorType = currentDetectorType();
    int detectorNbr = this->ui->comboBox->currentIndex()-1;

    if (detectorType == PRESENCEDETECTOR)
    {
        // Do nothing. No parameter set here
    } else if (detectorType == MOVEMENTDETECTOR)
    {
        detectorParams[detectorNbr].MinVecLengthThreshold[currentViewIndex][0] = arg1;
    } else if (detectorType == STATIONARYDETECTOR)
    {
        detectorParams[detectorNbr].MinVecLengthThreshold[currentViewIndex][0] = arg1;
    } else if (detectorType == TRAFFICLIGHTDETECTOR)
	{
		// Do nothing. No parameter set here
	}
}


void DetectorSelectionDialog::on_parameterBoxTab22_valueChanged(double arg1)
{
    // Find the detector type:
    int detectorType = currentDetectorType();
    int detectorNbr = this->ui->comboBox->currentIndex()-1;

    if (detectorType == PRESENCEDETECTOR)
    {
        // Do nothing. No parameter set here
    } else if (detectorType == MOVEMENTDETECTOR)
    {
        detectorParams[detectorNbr].TriggerThreshold[currentViewIndex][0] = arg1;
    } else if (detectorType == STATIONARYDETECTOR)
    {
        detectorParams[detectorNbr].TriggerThreshold[currentViewIndex][1] = arg1;
    } else if (detectorType == TRAFFICLIGHTDETECTOR)
	{
		// Do nothing. No parameter set here
	}
}

void DetectorSelectionDialog::on_parameterBoxTab31_valueChanged(double arg1)
{
    // Find the detector type:
    int detectorType = currentDetectorType();
    int detectorNbr = this->ui->comboBox->currentIndex()-1;

    if (detectorType == PRESENCEDETECTOR)
    {
        // Do nothing. No parameter set here
    } else if (detectorType == MOVEMENTDETECTOR)
    {
        detectorParams[detectorNbr].MinVecLengthThreshold[currentViewIndex][1] = arg1;
    } else if (detectorType == STATIONARYDETECTOR)
    {
        // Do nothing. No parameter set here
    } else if (detectorType == TRAFFICLIGHTDETECTOR)
	{
		// Do nothing. No parameter set here
	}
}

void DetectorSelectionDialog::on_parameterBoxTab32_valueChanged(double arg1)
{
    // Find the detector type:
    int detectorType = currentDetectorType();
    int detectorNbr = this->ui->comboBox->currentIndex()-1;

    if (detectorType == PRESENCEDETECTOR)
    {
        // Do nothing. No parameter set here
    } else if (detectorType == MOVEMENTDETECTOR)
    {
        detectorParams[detectorNbr].TriggerThreshold[currentViewIndex][1] = arg1;
    } else if (detectorType == STATIONARYDETECTOR)
    {
        // Do nothing. No parameter set here
    } else if (detectorType == TRAFFICLIGHTDETECTOR)
	{
		// Do nothing. No parameter set here
	}
}

void DetectorSelectionDialog::on_detectorTimingBox1_valueChanged(double arg1)
{
    int detectorNbr = this->ui->comboBox->currentIndex()-1;

    detectorParams[detectorNbr].DisableDetectorAfter = arg1;
}

void DetectorSelectionDialog::on_detectorTimingBox2_valueChanged(double arg1)
{
    int detectorNbr = this->ui->comboBox->currentIndex()-1;

    detectorParams[detectorNbr].DisableDetectorDuration = arg1;
}

void DetectorSelectionDialog::on_detectorTimingBox3_valueChanged(double arg1)
{
    int detectorNbr = this->ui->comboBox->currentIndex() - 1;

    logDialog->setMaxTriggeredDuration(detectorParams[detectorNbr].DetectorIdText, int(arg1));

}

void DetectorSelectionDialog::on_saveConfigButton_clicked()
{
    if (!addSettings.configSaveFilePath.isEmpty())
    {
        logDialog->getLogSettings(addSettings.generalLogSettings, addSettings.individualLogSettings);
        SaveConfiguration(addSettings.configSaveFilePath, detectorParams, addSettings, detectorType, views);

        ui->configSaveFilePathBox->setText(addSettings.configSaveFilePath);
    }
    else {
        on_saveConfigAsButton_clicked();
    }
}

void DetectorSelectionDialog::on_saveConfigAsButton_clicked()
{
    // Save the parameters of the current configuration
    QString filename;
    filename = QFileDialog::getSaveFileName(this, tr("Save configuration parameters"), 
        addSettings.configSaveFilePath.toStdString().c_str(), tr("Configuration files (*.yml)"));

    QProgressDialog progress("Saving configuration", QString(), 0, 0, this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimumDuration(1500);

    logDialog->getLogSettings(addSettings.generalLogSettings, addSettings.individualLogSettings);

    int retVal = SaveConfiguration(filename, detectorParams, addSettings, detectorType, views);

    if (retVal == 0)
    {
        ui->configSaveFilePathBox->setText(filename);
        addSettings.configSaveFilePath = filename;
        ui->saveConfigAsButton->setEnabled(true);
    }

    progress.close();
}


int DetectorSelectionDialog::SaveConfiguration(QString filename, std::vector<ruba::MaskHandlerOutputStruct> detectorParams,
                                                ruba::AdditionalSettingsStruct addSettings, int detectorType, QList<ViewInfo> views)
{
    FileStorage fs;

    if (filename.contains(".yml"))
    {
        fs.open(filename.toStdString(), FileStorage::WRITE);

        fs << "rubaVersion" << std::vector<int>{RUBA_VERSION_MAJOR, RUBA_VERSION_MINOR, RUBA_VERSION_REVISION};
        fs << "detectorType"  << detectorType;

        QString paramName;
        int detectorCount = int(detectorParams.size());

        // Save the order for which the parameters should be put back in detectorParams
        vector<int> detectorOrder;

        for (int j = 0; j < detectorCount; ++j)
        {
            detectorOrder.push_back(detectorParams[j].DetectorType);
        }

        fs << "detectorOrder" << detectorOrder;

        // Save the parameters for the different detectors
        int pCount = 0, mCount = 0, sCount = 0, tlCount = 0;

        for (int i = 0; i < detectorCount; ++i)
        {
            int currentDetector = detectorParams[i].DetectorType;

            QString namePrefix = Utils::getDetectorAbbreviation(detectorParams[i].DetectorType);
            int currentCount;

            switch (currentDetector)
            {
            case PRESENCEDETECTOR:
                pCount++;
                currentCount = pCount;
                break;
            case MOVEMENTDETECTOR:
                mCount++;
                currentCount = mCount;
                break;
			case STATIONARYDETECTOR:
				sCount++;
				currentCount = sCount;
				break;
			case TRAFFICLIGHTDETECTOR:
				tlCount++;
				currentCount = tlCount;
				break;
            }

            // At the beginning of the file, save anything but the masks for increased readability of the files
            paramName = namePrefix + QString::number(currentCount) + "-TriggerThreshold";
            saveMultiViewParameter(fs, paramName, detectorParams[i].TriggerThreshold, views);

            paramName = namePrefix + QString::number(currentCount) + "-DisableDetectorDuration";
            fs << paramName.toStdString() << detectorParams[i].DisableDetectorDuration;

            if (currentDetector == MOVEMENTDETECTOR || currentDetector == STATIONARYDETECTOR)
            {
                // Transfer the FlowRange from the cv::Point to two vectors of int
                vector<vector<int>> FlowRangeX, FlowRangeY;

                for (size_t j = 0; j < detectorParams[i].FlowRange.size(); ++j)
                {
                    vector<int> tmpRangeX, tmpRangeY;

                    for (size_t k = 0; k < detectorParams[i].FlowRange[j].size(); ++k)
                    {
                        tmpRangeX.push_back(detectorParams[i].FlowRange[j][k].x);
                        tmpRangeY.push_back(detectorParams[i].FlowRange[j][k].y);
                    }

                    FlowRangeX.push_back(tmpRangeX);
                    FlowRangeY.push_back(tmpRangeY);
                }

                paramName = namePrefix + QString::number(currentCount) + "-FlowRangeX";
                saveMultiViewParameter(fs, paramName, FlowRangeX, views);

                paramName = namePrefix + QString::number(currentCount) + "-FlowRangeY";
                saveMultiViewParameter(fs, paramName, FlowRangeY, views);

                paramName = namePrefix + QString::number(currentCount) + "-MinVecLengthThreshold";
                saveMultiViewParameter(fs, paramName, detectorParams[i].MinVecLengthThreshold, views);

                paramName = namePrefix + QString::number(currentCount) + "-upperThresholdBoundMultiplier";
                fs << paramName.toStdString() << detectorParams[i].upperThresholdBoundMultiplier;

                paramName = namePrefix + QString::number(currentCount) + "-EnableWrongDirection";
                fs << paramName.toStdString() << detectorParams[i].EnableWrongDirection;
            }

            if (currentDetector == TRAFFICLIGHTDETECTOR)
            {
                paramName = namePrefix + QString::number(currentCount) + "-tlIndiceRed";
                fs << paramName.toStdString() << detectorParams[i].trafficLightPositions[tlTypeClasses::RED];

                paramName = namePrefix + QString::number(currentCount) + "-tlIndiceYellow";
                fs << paramName.toStdString() << detectorParams[i].trafficLightPositions[tlTypeClasses::YELLOW];

                paramName = namePrefix + QString::number(currentCount) + "-tlIndiceGreen";
                fs << paramName.toStdString() << detectorParams[i].trafficLightPositions[tlTypeClasses::GREEN];

                paramName = namePrefix + QString::number(currentCount) + "-tlTypeTrigger";
                fs << paramName.toStdString() << detectorParams[i].trafficLightTypeTrigger;


            }
        }


        // Save the configuration of the log file(s)
        LogSettingsDialog::saveToFile(fs, addSettings.generalLogSettings, addSettings.individualLogSettings);

        // Save the masks and background images for the detectors
        // This is done as the last write operation in order to improve the readability
        // of the output file
		pCount = 0; mCount = 0; sCount = 0; tlCount = 0;

        for (int i = 0; i < detectorCount; ++i)
        {
            QString namePrefix = Utils::getDetectorAbbreviation(detectorParams[i].DetectorType);
            int currentCount;

            switch (detectorParams[i].DetectorType)
            {
            case PRESENCEDETECTOR:
                pCount++;
                currentCount = pCount;
                break;
            case MOVEMENTDETECTOR:
                mCount++;
                currentCount = mCount;
                break;
            case STATIONARYDETECTOR:
                sCount++;
                currentCount = sCount;
                break;
			case TRAFFICLIGHTDETECTOR:
				tlCount++;
				currentCount = tlCount;
				break;
            }

            // Transfer the maskPoints from the cv::Point to two vectors of int
            vector<vector<int>> maskPointsX, maskPointsY;
			bool sufficientMaskPoints = true;

            for (size_t j = 0; j < detectorParams[i].maskPoints.size(); ++j)
            {
                vector<int> tmpPointsX, tmpPointsY;

                for (size_t k = 0; k < detectorParams[i].maskPoints[j].size(); ++k)
                {
                    tmpPointsX.push_back(detectorParams[i].maskPoints[j][k].x);
                    tmpPointsY.push_back(detectorParams[i].maskPoints[j][k].y);
                }

				if (detectorParams[i].maskPoints[j].size() < 3)
				{
					sufficientMaskPoints = false;
				}

                maskPointsX.push_back(tmpPointsX);
                maskPointsY.push_back(tmpPointsY);
            }

            paramName.clear();
            paramName = namePrefix + QString::number(currentCount) + "-MaskPointsX";
            saveMultiViewParameter(fs, paramName, maskPointsX, views);

            paramName = namePrefix + QString::number(currentCount) + "-MaskPointsY";
            saveMultiViewParameter(fs, paramName, maskPointsY, views);

			// Don't save the mask if mask points are available - better performance, lower file size
			if (!sufficientMaskPoints)
			{
				paramName.clear(); 
				paramName = namePrefix + QString::number(currentCount) + "-Mask";
				saveMultiViewParameter(fs, paramName, detectorParams[i].Masks, views);
			}

            if (detectorParams[i].DetectorType == PRESENCEDETECTOR || detectorParams[i].DetectorType == STATIONARYDETECTOR)
            {
                paramName.clear();
                paramName = namePrefix + QString::number(currentCount) + "-BackgroundImage";
                saveMultiViewParameter(fs, paramName, detectorParams[i].BackgroundImages, views);
            }

        }

        fs.release();
        return 0;
    } else {
        return 1;
    }

}

void DetectorSelectionDialog::saveMultiViewParameter(cv::FileStorage& fs, QString baseName, 
    const std::vector<std::vector<int>>& var, const QList<ViewInfo>& views)
{
    if (views.size() == var.size())
    {
        for (size_t i = 0; i < var.size(); ++i)
        {
            QString paramName = baseName + "-View" + QString::number(views[i].viewId);
            fs << paramName.toStdString() << var[i];
        }
    }
}

void DetectorSelectionDialog::saveMultiViewParameter(cv::FileStorage& fs, QString baseName, 
    const std::vector<std::vector<double>>& var, const QList<ViewInfo>& views)
{
    if (views.size() == var.size())
    {
        for (size_t i = 0; i < var.size(); ++i)
        {
            QString paramName = baseName + "-View" + QString::number(views[i].viewId);
            fs << paramName.toStdString() << var[i];
        }
    }
}

void DetectorSelectionDialog::saveMultiViewParameter(cv::FileStorage& fs, QString baseName, 
    const std::vector<cv::Mat>& var, const QList<ViewInfo>& views)
{
    if (views.size() == var.size())
    {
        for (size_t i = 0; i < var.size(); ++i)
        {
            QString paramName = baseName + "-View" + QString::number(views[i].viewId);
            fs << paramName.toStdString() << var[i];
        }
    }
}

void DetectorSelectionDialog::loadMultiViewParameter(cv::FileStorage fs, QString baseName, 
    std::vector<std::vector<int>> &var)
{
    var.clear();

    for (size_t i = 0; i < views.size(); ++i)
    {
        vector<int> tmpVec;
        QString paramName = baseName + "-View" + QString::number(views[i].viewId);
        fs[paramName.toStdString()] >> tmpVec;

        if (!tmpVec.empty())
        {
            var.push_back(tmpVec);
        } else {
            break;
        }
    }

}

void DetectorSelectionDialog::loadMultiViewParameter(cv::FileStorage fs, QString baseName, std::vector<std::vector<double>> &var)
{
    var.clear();

    for (size_t i = 0; i < views.size(); ++i)
    {
        vector<double> tmpVec;
        QString paramName = baseName + "-View" + QString::number(views[i].viewId);
        fs[paramName.toStdString()] >> tmpVec;

        if (!tmpVec.empty())
        {
            var.push_back(tmpVec);
        } else {
            break;
        }
    }
}

void DetectorSelectionDialog::loadMultiViewParameter(cv::FileStorage fs, QString baseName, std::vector<cv::Mat> &var)
{
    var.clear();

    for (size_t i = 0; i < views.size(); ++i)
    {
        Mat tmpImg;
        QString paramName = baseName + "-View" + QString::number(views[i].viewId);
        fs[paramName.toStdString()] >> tmpImg;

        if (!tmpImg.empty())
        {
            var.push_back(tmpImg);
        } else {
            break;
        }
    }
}

int DetectorSelectionDialog::loadConfiguration(QString filename)
{
    bool silentMode;
	prevDetectorType = -1;

    if (filename.isEmpty())
    {
        QSettings settings;
        settings.beginGroup("detectorSelectionDialog");
        QString detectorBaseDir = settings.value("detectorBaseDir", "").toString();
        settings.endGroup();

        filename = QFileDialog::getOpenFileName(this, tr("Load configuration parameters"), 
            detectorBaseDir, tr("Configuration files (*.yml)"));
        silentMode = false;
    } else {
        silentMode = true;
    }

    if (filename.contains(".yml"))
    {
        // Add path to settings
        QSettings settings;
        QString detectorBaseDir = QFileInfo(filename).absolutePath();
        settings.beginGroup("detectorSelectionDialog");
        settings.setValue("detectorBaseDir", detectorBaseDir);
        settings.endGroup();

        addSettings.configSaveFilePath = filename;

        int nbrP = 0, nbrM = 0, nbrS = 0, nbrTL = 0, nDetectorType;

        FileStorage fs;

        try {
            fs.open(filename.toStdString(), FileStorage::READ);
        } catch (cv::Exception e) {
            qDebug() << e.what();
            if (!silentMode)
            {
                QMessageBox::warning(this, tr(this->windowTitle().toStdString().c_str()), "Unable to load file", QMessageBox::Ok);
            }
            return 1;
        }


		QApplication::processEvents();

        // Check the version of RUBA that saved the file. If the version is before 1.2.0, propose to convert the file
        vector<int> savedRubaVersion;
        fs["rubaVersion"] >> savedRubaVersion;
        bool oldVersion = false;

        if (savedRubaVersion.empty()) {
            oldVersion = true;
        } else if (savedRubaVersion.size() > 2 && 
            ((savedRubaVersion[0] == 1 && savedRubaVersion[1] < 2) ||
              savedRubaVersion[0] < 1)) 
        { 
            oldVersion = true;
        }

        if (oldVersion) {
            int answer = QMessageBox::question(this, tr("Old file format detected"), tr("The detector was created using an older version of RUBA. In order to use it, RUBA will "
                "convert the file. OK?"));

            if (answer == QMessageBox::Yes) {
                // Convert the file. First, close the current file stream to perform processing independently
                fs.release();

                int retVal = convertFileFormat(filename);

                if (retVal != 0) {
                    return 1;
                }

                fs.open(filename.toStdString(), FileStorage::READ); // Open the converted file
            }
            else {
                return 1;
            }
        }

        ui->configSaveFilePathBox->setText(filename);

        QProgressDialog progress(this);
		QApplication::processEvents();

        if (!silentMode)
        {
            progress.setLabelText("Loading configuration");
            progress.setCancelButtonText("");
            progress.setMinimum(1);
            progress.setMaximum(100);
            progress.setWindowModality(Qt::WindowModal);
            progress.setMinimumDuration(3000);
        }

        // Check if the file is of the right detector type and contains the right number of detectors
        // First, load the order for which the detector parameters are listed in
        // the detectorParams vector and deduce the detectors from here
        vector<int> detectorOrder;
        fs["detectorOrder"] >> detectorOrder;

        for (auto i = 0; i < detectorOrder.size(); ++i) 
        {
            switch (detectorOrder[i]) {
            case PRESENCEDETECTOR: 
            {
                nbrP++;
                break;
            }
            case MOVEMENTDETECTOR:
            {
                nbrM++;
                break;
            }
            case STATIONARYDETECTOR:
            {
                nbrS++;
                break;
            }
            case TRAFFICLIGHTDETECTOR:
                nbrTL++;
                break;
            }
        }

        nDetectorType = fs["detectorType"];

        if (nDetectorType != detectorType)
        {
            if (!silentMode)
            {
                QString message = "Unable to load configuration from file.\n\nThe file does not contain the settings of a " + this->moduleTitle + " detector.";
                progress.close();
                QMessageBox::warning(this, tr(this->windowTitle().toStdString().c_str()), message.toStdString().c_str(), QMessageBox::Ok);
            }
            return 1;
        }

        if (nbrP != this->nbrPDetectors || nbrM != this->nbrMDetectors || nbrS != this->nbrSDetectors || nbrTL != this->nbrTLDetectors)
        {
            if (!silentMode)
            {
                QString message = QString("Unable to load configuration from file.\n\nThe file should contain:\n%1 Presence Detectors\n%2 Movement Detectors\n%3 Stationary Detectors\n%4 Traffic Light Detectors\n\nThe current file contains:\n%5 Presence Detectors\n%6 Movement Detectors\n%7 Stationary Detectors\n%8 Traffic Light Detectors").arg(nbrPDetectors).arg(nbrMDetectors).arg(nbrSDetectors).arg(nbrTLDetectors).arg(nbrP).arg(nbrM).arg(nbrS).arg(nbrTL);
                progress.close();
                QMessageBox::warning(this, tr(this->windowTitle().toStdString().c_str()), message.toStdString().c_str(), QMessageBox::Ok);
            }
            return 1;
        }

        QString paramName;
        int pCount = 0, mCount = 0, sCount = 0, tlCount = 0;
        int detectorCount = nbrPDetectors + nbrMDetectors + nbrSDetectors + nbrTLDetectors;
        vector<bool> loadedBackgroundIsDifferent;
        loadedBackgroundIsDifferent.resize(views.size());
        vector<Mat> detectorBackgroundImages;
        detectorBackgroundImages.resize(views.size());
		QApplication::processEvents();


        for (int i = 0; i < detectorCount; ++i)
        {
			QApplication::processEvents();

            int currentDetector;

            if (detectorOrder.size() > 0 && detectorOrder.size() == detectorCount)
            {
                currentDetector = detectorOrder[i];

                if (currentDetector != detectorParams[i].DetectorType)
                {
                    // Change the order of the detectors. In order to do so, a full initialization is needed
                    detectorParams[i].DetectorType = currentDetector;
                    setDefaultDetectorParameters(i);
                    initDetectorComboBox();
                }
            } else {
                // Not supported. Use the current configuration
                currentDetector = detectorParams[i].DetectorType;
            }

            QString namePrefix;
            int currentCount;

            switch (currentDetector)
            {
            case PRESENCEDETECTOR:
                namePrefix = "P";
                pCount++;
                currentCount = pCount;
                break;
            case MOVEMENTDETECTOR:
                namePrefix = "M";
                mCount++;
                currentCount = mCount;
                break;
            case STATIONARYDETECTOR:
                namePrefix = "S";
                sCount++;
                currentCount = sCount;
                break;
			case TRAFFICLIGHTDETECTOR:
				namePrefix = "TL";
				tlCount++;
				currentCount = tlCount;
				break;
            }

            // Load the common settings of each detector
            try{
                paramName = namePrefix + QString::number(currentCount) + "-TriggerThreshold";
                vector<vector<int>> tmpThreshold;
                loadMultiViewParameter(fs, paramName, tmpThreshold);

                if (!tmpThreshold.empty() && (tmpThreshold.size() == views.size())
                        && (tmpThreshold[0].size() == detectorParams[i].TriggerThreshold[0].size()))
                {
                    detectorParams[i].TriggerThreshold = tmpThreshold;
                }

                paramName.clear();
                paramName = namePrefix + QString::number(currentCount) + "-DisableDetectorAfter";
                int tmpVar;
                fs[paramName.toStdString()] >> tmpVar;
                if (tmpVar > 0 || tmpVar == -1)
                {
                    detectorParams[i].DisableDetectorAfter = tmpVar;
                }


                // Old way of setting maxTriggeredDuration - new way is during the LogSettingsDialog
                paramName = namePrefix + QString::number(currentCount) + "-MaxActiveDuration";
                fs[paramName.toStdString()] >> tmpVar;
                if (tmpVar > 0 || tmpVar == -1)
                {
                    logDialog->setMaxTriggeredDuration(detectorParams[i].DetectorIdText, tmpVar);
                }

                paramName.clear();
                paramName = namePrefix + QString::number(currentCount) + "-DisableDetectorDuration";
                fs[paramName.toStdString()] >> tmpVar;
                if (tmpVar >= 0 || tmpVar == -1)
                {
                    detectorParams[i].DisableDetectorDuration = tmpVar;
                }

                
				QApplication::processEvents();

                // x and y coordinates of the masks are loaded separately
                vector<vector<int>> maskPointsX, maskPointsY;
                paramName = namePrefix + QString::number(currentCount) + "-MaskPointsX";
                loadMultiViewParameter(fs, paramName, maskPointsX);
                paramName = namePrefix + QString::number(currentCount) + "-MaskPointsY";
                loadMultiViewParameter(fs, paramName, maskPointsY);

                if (!maskPointsX.empty() && maskPointsX.size() == maskPointsY.size()
                        && maskPointsX.size() == views.size())
                {
                    detectorParams[i].maskPoints.clear();

                    // Transfer the points to the cv::Point format
                    for (size_t j = 0; j < views.size(); ++j)
                    {
                        if (maskPointsX[j].size() == maskPointsY[j].size())
                        {
                            vector<Point> innerPoints;
                            for (size_t k = 0; k < maskPointsX[j].size(); ++k)
                            {
                                Point point;
                                point.x = maskPointsX[j][k];
                                point.y = maskPointsY[j][k];
                                innerPoints.push_back(point);
                            }
                            detectorParams[i].maskPoints.push_back(innerPoints);
                        } else {
                            vector<Point> emptyPoints;
                            detectorParams[i].maskPoints.push_back(emptyPoints);
                            break;
                        }
                    }
                }

				bool pointsToMaskTransferSuccess = true;

				for (size_t j = 0; j < views.size(); ++j)
				{
					if (detectorParams[i].maskPoints.size() > j && detectorParams[i].maskPoints[j].size() > 2)
					{
						Mat maskImage = Mat::zeros(images[j].rows, images[j].cols, CV_8UC1);

						fillConvexPoly(maskImage, detectorParams[i].maskPoints[j], Scalar(255), 8);
						maskImage.copyTo(detectorParams[i].Masks[j]);
					}
					else {
						pointsToMaskTransferSuccess = false;
					}
				}
				
				if (!pointsToMaskTransferSuccess)
				{
					// Masks of different views are saved with their specific view name
					vector<Mat> masks;
					paramName = namePrefix + QString::number(currentCount) + "-Mask";
					loadMultiViewParameter(fs, paramName, masks);

					if (!masks.empty() && masks.size() == views.size())
					{
						detectorParams[i].Masks = masks;

						for (auto j = 0; j < detectorParams[i].Masks.size(); ++j)
						{
							detectorParams[i].Masks[j] = detectorParams[i].Masks[j] * 255; // Make sure that values are either 0 or 255

							// Check if the dimensions of the mask corresponds to the dimensions of the loaded images
							if (images.size() > j && (images[j].cols != masks[j].cols || images[j].rows != masks[j].rows))
							{
								// Create an empty mask
								detectorParams[i].Masks[j] = Mat();
							}
						}
					}
				}


                detectorParams[i].IsInitialized = true;

                for (size_t j = 0; j < views.size(); ++j)
                {
                    if (detectorParams[i].Masks.empty() || detectorParams[i].Masks[j].empty())
                    {
                        detectorParams[i].IsInitialized = false;
                        detectorParams[i].IsMaskInitialized[j] = false;

                    } else {
                        detectorParams[i].IsMaskInitialized[j] = true;
                    }
                }

                if (currentDetector == PRESENCEDETECTOR || currentDetector == STATIONARYDETECTOR)
                {
                    paramName.clear();
                    paramName = namePrefix + QString::number(currentCount) + "-BackgroundImage";
                    vector<Mat> backgroundImages;
                    loadMultiViewParameter(fs, paramName, backgroundImages);

                    if (backgroundImages.size() == views.size())
                    {
                        for (size_t j = 0; j < views.size(); ++j)
                        {
                            backgroundImages[j].copyTo(detectorParams[i].BackgroundImages[j]);
                        }
                    }

                    for (size_t j = 0; j < views.size(); ++j)
                    {
                        if (images.size() > j && !images[j].empty())
                        {
                            Mat absDiff;
                            absdiff(detectorParams[i].BackgroundImages[j], images[j], absDiff);
                            Scalar diffCount = sum(absDiff);
                            if (diffCount[0] > 0)
                            {
                                loadedBackgroundIsDifferent[j] = true;
                                detectorParams[i].BackgroundImages[j].copyTo(detectorBackgroundImages[j]);
                            }
                        }
                    }
                }

				QApplication::processEvents();

                if (currentDetector == MOVEMENTDETECTOR || currentDetector == STATIONARYDETECTOR)
                {
                    vector<vector<int>> flowRangeX, flowRangeY;
                    paramName = namePrefix + QString::number(currentCount) + "-FlowRangeX";
                    loadMultiViewParameter(fs, paramName, flowRangeX);

                    paramName = namePrefix + QString::number(currentCount) + "-FlowRangeY";
                    loadMultiViewParameter(fs, paramName, flowRangeY);

                    if (!flowRangeX.empty() && flowRangeX.size() == flowRangeY.size()
                            && flowRangeX.size() == views.size())
                    {
                        detectorParams[i].FlowRange.clear();

                        // Transfer the points to the cv::Point format
                        for (size_t j = 0; j < views.size(); ++j)
                        {
                            if (flowRangeX[j].size() == flowRangeY[j].size())
                            {
                                vector<Point> innerPoints;
                                for (size_t k = 0; k < flowRangeX[j].size(); ++k)
                                {
                                    Point point;
                                    point.x = flowRangeX[j][k];
                                    point.y = flowRangeY[j][k];
                                    innerPoints.push_back(point);
                                }
                                detectorParams[i].FlowRange.push_back(innerPoints);
                            } else {
                                vector<Point> emptyPoints;
                                detectorParams[i].FlowRange.push_back(emptyPoints);
                                break;
                            }
                        }
                    }

                    paramName = namePrefix + QString::number(currentCount) + "-MinVecLengthThreshold";
                    vector<vector<double>> tmpVecLength;
                    loadMultiViewParameter(fs, paramName, tmpVecLength);

                    if ((tmpVecLength.size() == views.size())) // Safeguard the vector
                    {
                        detectorParams[i].MinVecLengthThreshold = tmpVecLength;
                    }

                    paramName.clear();
                    paramName = namePrefix + QString::number(currentCount) + "-upperThresholdBoundMultiplier";
                    int tmpMultiplier;
                    fs[paramName.toStdString()] >> tmpMultiplier;
                    if (tmpMultiplier > 0)
                    {
                        detectorParams[i].upperThresholdBoundMultiplier = tmpMultiplier;
                    }

                    paramName = namePrefix + QString::number(currentCount) + "-EnableWrongDirection";
                    fs[paramName.toStdString()] >> detectorParams[i].EnableWrongDirection;
                }

                if (currentDetector == TRAFFICLIGHTDETECTOR)
                {
                    paramName = namePrefix + QString::number(currentCount) + "-tlIndiceRed";
                    fs[paramName.toStdString()] >> detectorParams[i].trafficLightPositions[RED];

                    paramName = namePrefix + QString::number(currentCount) + "-tlIndiceYellow";
                    fs[paramName.toStdString()] >> detectorParams[i].trafficLightPositions[YELLOW];

                    paramName = namePrefix + QString::number(currentCount) + "-tlIndiceGreen";
                    fs[paramName.toStdString()] >> detectorParams[i].trafficLightPositions[GREEN];

                    paramName = namePrefix + QString::number(currentCount) + "-tlTypeTrigger";
                    fs[paramName.toStdString()] >> detectorParams[i].trafficLightTypeTrigger;


                }


            } catch (cv::Exception e) {
                detectorParams[i].IsInitialized = false;
                qDebug() << e.what();

                if (silentMode)
                {
                    // Something went wrong. We cannot load the masks. Abort.
                    return -1;
                }
            }

            progress.setValue((1./(detectorCount)*i)*90);
        }


        // Read the configuration of the log files
        logDialog->loadFromFile(fs, addSettings.generalLogSettings, addSettings.individualLogSettings);
        logDialog->setLogSettings(addSettings.generalLogSettings, addSettings.individualLogSettings);

        fs.release();
        checkConfiguration();

		this->ui->comboBox->blockSignals(true);

		if (detectorParams.size() == 1)
		{
			this->ui->comboBox->setCurrentIndex(this->ui->comboBox->count() - 1);
		}
		else {
			this->ui->comboBox->setCurrentIndex(0);
		}

		on_comboBox_currentIndexChanged();
		this->ui->comboBox->blockSignals(false);
        

        if (!silentMode)
        {
            this->ProcessTimer.start();
        }
        progress.close();

        for (size_t j = 0; j < views.size(); ++j)
        {
            if (loadedBackgroundIsDifferent[j] && !silentMode 
                && (images.size() > j) && !images[j].empty())
            {
                DualImageViewer* imgViewer = new  DualImageViewer(this, detectorBackgroundImages[j], images[j]);
                imgViewer->SetImageCaption("Background image of detectors from configuration file", "Current image");
                imgViewer->SetButtonText("Use image from conf. file", "Use current image");
                imgViewer->SetDecisionText("The background image from the configuration file is different from the current image.","Which image do you want to use as background for the detectors?");
                imgViewer->setWindowTitle("Set background image");

                int ret = imgViewer->exec();

                if (ret == QDialog::Rejected)
                {
                    for (int i = 0; i < detectorCount; ++i)
                    {
                        int currentDetector = detectorParams[i].DetectorType;

                        if (currentDetector == PRESENCEDETECTOR || currentDetector == STATIONARYDETECTOR)
                        {
                            images[j].copyTo(detectorParams[i].BackgroundImages[j]);
                        }
                    }
                }
            }
        }
    } else {
        return 1;
    }

    for (auto i = 0; i < detectorParams.size(); ++i)
    {
        if (detectorParams[i].IsInitialized == false)
        {
            return -1;
        }
        
    }

    return 0;
}

void DetectorSelectionDialog::on_loadConfigButton_clicked()
{
    loadConfiguration();
}


void DetectorSelectionDialog::on_loadMaskButton_clicked()
{
    int index = this->ui->comboBox->currentIndex()-1;
    vector<int> maskIndices;

    QSettings settings;
    settings.beginGroup("detectorSelectionDialog");
    QString baseDir = settings.value("maskBackgroundBaseDir", "").toString();
    settings.endGroup();

    QString filename = QFileDialog::getOpenFileName(this, tr("Load existing mask"), 
        baseDir, tr("Images (*.png *.bmp *.jpg)"));

    if(filename.isEmpty())
    {
        return;
    }

    settings.beginGroup("detectorSelectionDialog");
    settings.setValue("maskBackgroundBaseDir", QFileInfo(filename).absolutePath());
    settings.endGroup();

    ProcessTimer.stop();
    Mat newMask = imread(filename.toStdString(), IMREAD_GRAYSCALE);

    if (!newMask.empty())
    {
        detectorParams[index].Masks[currentViewIndex] = newMask;
        detectorParams[index].IsMaskInitialized[currentViewIndex] = true;
        detectorParams[index].maskPoints.clear();

        // Draw the mask
        maskIndices.push_back(index);
        drawMasks(maskIndices);

        this->ui->drawFinishResetMaskButton->setText("Reset mask");
        this->ui->drawFinishResetMaskButton->setToolTip("Reset mask");
        this->ui->drawFinishResetMaskButton->setIcon(QIcon("://icons/cross-script.png"));
		drawingMode = DrawingMode::Disabled;

        setUpLayout(true, index);

    }

    checkConfiguration();
    ProcessTimer.start();
}

void DetectorSelectionDialog::on_saveMaskButton_clicked()
{
    QSettings settings;
    settings.beginGroup("detectorSelectionDialog");
    QString detectorBaseDir = settings.value("maskBackgroundBaseDir", "").toString();
    settings.endGroup();


    QString filename = QFileDialog::getSaveFileName(this, tr("Save mask"), detectorBaseDir, tr("Images (*.png *.bmp *.jpg)"));
    int index = this->ui->comboBox->currentIndex()-1;

    if (!detectorParams[index].Masks.empty() && !filename.isEmpty())
    {
        if (!detectorParams[index].Masks[currentViewIndex].empty())
        {
            imwrite(filename.toStdString(),detectorParams[index].Masks[currentViewIndex]);
        }
    }

}

void DetectorSelectionDialog::on_loadMaskPointsButton_clicked()
{
    QSettings settings;
    settings.beginGroup("detectorSelectionDialog");
    QString detectorBaseDir = settings.value("detectorBaseDir", "").toString();
    settings.endGroup();

    QString filename = QFileDialog::getOpenFileName(this, tr("Load mask points from file"),
        detectorBaseDir, tr("YAML-file (*.yml)"));

    if (filename.isEmpty())
    {
        return;
    }

    ProcessTimer.stop();

    FileStorage fs(filename.toStdString(), FileStorage::READ);

    if (fs.isOpened())
    {
        std::vector<Point> points;
        fs["maskPoints"] >> points;

        if (points.empty())
        {
            return;
        }


        int index = this->ui->comboBox->currentIndex() - 1;

        if (detectorParams[index].maskPoints.size() > currentViewIndex)
        {
            detectorParams[index].maskPoints[currentViewIndex] = points;

            detectorParams[index].IsMaskInitialized[currentViewIndex] = false;

            vector<int> maskIndices;
            maskIndices.push_back(index);
            drawMasks(maskIndices);


            if (ui->drawFinishResetMaskButton->text() == tr("Draw mask") || 
                ui->drawFinishResetMaskButton->text() == tr("Redraw mask"))
            {
                // If we have not selected the tool for drawing, do so now
                ui->drawFinishResetMaskButton->click();
            }
            else {
                setUpLayout(true, index);
                checkConfiguration();
            }

        }
    }

    ProcessTimer.start();
}

void DetectorSelectionDialog::on_saveMaskPointsButton_clicked()
{
    QSettings settings;
    settings.beginGroup("detectorSelectionDialog");
    QString detectorBaseDir = settings.value("detectorBaseDir", "").toString();
    settings.endGroup();

    QString filename = QFileDialog::getSaveFileName(this, tr("Save mask points"), detectorBaseDir, tr("YAML-file (*.yml)"));

    int index = this->ui->comboBox->currentIndex() - 1;

    if (detectorParams[index].maskPoints.size() > currentViewIndex && !filename.isEmpty()
        && !detectorParams[index].maskPoints[currentViewIndex].empty())
    {
        FileStorage fs(filename.toStdString(), FileStorage::WRITE);

        fs << "maskPoints" << detectorParams[index].maskPoints[currentViewIndex];

        fs.release();
    }
}



void DetectorSelectionDialog::on_subtractFromMaskButton_clicked()
{
    int index = this->ui->comboBox->currentIndex()-1;
	drawingMode = DrawingMode::Mask;
    detectorParams[index].IsMaskInitialized[currentViewIndex] = false;

    // Remove the current mask point
    if (detectorParams[index].maskPoints[currentViewIndex].size() > currentMaskPoint)
    {
        detectorParams[index].maskPoints[currentViewIndex].erase(detectorParams[index].maskPoints[currentViewIndex].begin() + currentMaskPoint);
        currentMaskPoint--;

        vector<int> maskIndices;
        maskIndices.push_back(index);
        drawMasks(maskIndices);

        if (detectorParams[index].maskPoints[currentViewIndex].size() > 2)
        {
            // If more than two points are selected, we may finish the mask by choice
            this->ui->drawFinishResetMaskButton->setEnabled(true);
        }
        else {
            this->ui->drawFinishResetMaskButton->setEnabled(false);
        }

        if (detectorParams[index].maskPoints[currentViewIndex].size() == 0)
        {
            ui->subtractFromMaskButton->setEnabled(false);
        }
        checkConfiguration();
    }
}

void DetectorSelectionDialog::on_addToMaskButton_clicked()
{
    // Insert a new point between the current point and its adjacent point

    int index = this->ui->comboBox->currentIndex() - 1;
	drawingMode = DrawingMode::Mask;

    if (detectorParams[index].maskPoints[currentViewIndex].size() > currentMaskPoint && currentMaskPoint > 0)
    {
        // Get the position between currentMaskPoint and currentMaskPoint - 1
        Point newPoint = detectorParams[index].maskPoints[currentViewIndex][currentMaskPoint-1] + 
            (detectorParams[index].maskPoints[currentViewIndex][currentMaskPoint] -
            detectorParams[index].maskPoints[currentViewIndex][currentMaskPoint - 1])/2;

        // Insert the new point before the currentMaskPoint
        detectorParams[index].maskPoints[currentViewIndex].insert(
            detectorParams[index].maskPoints[currentViewIndex].begin() + currentMaskPoint,
            newPoint);

        drawMasks(vector<int>{index});
    }
}



void DetectorSelectionDialog::finishMask(int index)
{
    // Finish mask, i.e. if a mask is created from a set of points, fill the interior
    if (detectorParams[index].maskPoints[currentViewIndex].size() > 2)
    {
        // We cannot create a mask from less than 3 points
        Mat maskImage = Mat::zeros(images[currentViewIndex].rows, images[currentViewIndex].cols, CV_8UC1);

        fillConvexPoly(maskImage, detectorParams[index].maskPoints[currentViewIndex], Scalar(255), 8);
        maskImage.copyTo(detectorParams[index].Masks[currentViewIndex]);
        detectorParams[index].IsMaskInitialized[currentViewIndex] = true;

        // Update the GUI
        this->ui->drawFinishResetMaskButton->setText("Redraw mask");
        this->ui->drawFinishResetMaskButton->setIcon(QIcon("://icons/pencil.png"));
        this->ui->drawFinishResetMaskButton->setToolTip("Redraw mask");
        ui->saveMaskButton->setEnabled(true);
        ui->saveMaskPointsButton->setEnabled(true);
		drawingMode = DrawingMode::Disabled;
        setUpLayout(true, index);


        // Show the mask
        vector<int> maskIndices;
        maskIndices.push_back(index);
        drawMasks(maskIndices);

        // Compute the centre of the mask
        computeMaskCentre();

        // Enable controls if appropriate
        checkConfiguration();
    }



}


void DetectorSelectionDialog::keyPressEvent(QKeyEvent *e)
{
    int index = this->ui->comboBox->currentIndex() - 1;

    
    if (drawingMode == DrawingMode::Mask || drawingMode == DrawingMode::TrafficLight)
    {
        // We are currently drawing a mask or a traffic light
        if (detectorParams[index].maskPoints[currentViewIndex].empty())
        {
            return;
        }

        if (currentMaskPoint >= detectorParams[index].maskPoints[currentViewIndex].size()) {
            // The current mask point is outside the range of the array. Reset it 
            currentMaskPoint = detectorParams[index].maskPoints[currentViewIndex].size() - 1;
            qDebug() << "Resetting currentMaskPoint";
        }
        else if (currentMaskPoint < 0){
            currentMaskPoint = 0;
        }

            
		cv::Point currentPointDisplacement = { 0, 0 };

        switch (e->key()) {
        case Qt::Key_S:
			currentPointDisplacement.y++;
            break;
        case Qt::Key_W:
			currentPointDisplacement.y--;
            break;
        case Qt::Key_A:
			currentPointDisplacement.x--;
            break;
        case Qt::Key_D:
			currentPointDisplacement.x++;
            break;
        case Qt::Key_Home:
            currentMaskPoint--;

            if (currentMaskPoint < 0) {
                currentMaskPoint = detectorParams[index].maskPoints[currentViewIndex].size() - 1; // Cycle to the end of the index
            }

            break;
        case Qt::Key_End:
            currentMaskPoint++;
            
            if (currentMaskPoint == detectorParams[index].maskPoints[currentViewIndex].size()) {
                currentMaskPoint = 0; // Cycle to the beginning of the index
            }

            break;
        case Qt::Key_Delete:
            on_subtractFromMaskButton_clicked();
            break;
        case Qt::Key_Insert:
            on_addToMaskButton_clicked();
            break;
        }

		if (currentPointDisplacement.x != 0 || currentPointDisplacement.y != 0)
		{
			switch (drawingMode)
			{
			case DrawingMode::Mask:
			{
				detectorParams[index].maskPoints[currentViewIndex][currentMaskPoint] += currentPointDisplacement;
				break;
			}
			case DrawingMode::TrafficLight:
			{
				if (annotatedTrafficLight < detectorParams[index].trafficLightPositions.size() && annotatedTrafficLight >= 0)
				{
					detectorParams[index].trafficLightPositions[annotatedTrafficLight] += currentPointDisplacement;
				}
				break;
			}
			}
		}


		drawMasks(vector<int>(1, index));
    }
}


void DetectorSelectionDialog::mouseHandler(QMouseEvent* event)
{
    Point pt = mviewer->mapPoint(Point(event->x(), event->y()));
    int index = this->ui->comboBox->currentIndex()-1;
    vector<int> maskIndices;
    maskIndices.push_back(index);

    if (drawingMode == DrawingMode::Mask)
    {
        // If drawing is enabled but we have not enabled the drawing by pressing the drawFinishResetMaskButton, manually press
        // the button to enter a proper drawing state
        if (ui->drawFinishResetMaskButton->text() == tr("Draw mask"))
        {
            ui->drawFinishResetMaskButton->click();
        }

        if (event->type() == QEvent::MouseButtonDblClick && (event->button() == Qt::LeftButton))
        {
            // A double click finishes the mask
            finishMask(index);

        } else if (event->button() == Qt::RightButton && (event->buttons() & Qt::RightButton) == Qt::RightButton){ // Right click event

            if (!detectorParams[index].IsMaskInitialized[currentViewIndex])
            {
                // Remove the current mask point from the list.
                if (detectorParams[index].maskPoints[currentViewIndex].size() > 0)
                {
                    detectorParams[index].maskPoints[currentViewIndex].erase(detectorParams[index].maskPoints[currentViewIndex].begin() + currentMaskPoint);
                    currentMaskPoint--;
                    drawMasks(maskIndices);
                }
            }

        } else if(event->button() == Qt::LeftButton && (event->buttons() & Qt::LeftButton) == Qt::LeftButton) { // Left click event.

            if (!detectorParams[index].IsMaskInitialized[currentViewIndex])
            {
                // Check if we are near an existing point

                bool nearExistingPoint = false;
                int distanceMultiplier = overlayedImages[currentViewIndex].cols / 640;

                if (distanceMultiplier < 1)
                {
                    distanceMultiplier = 1;
                }

                for (int i = 0; i < detectorParams[index].maskPoints[currentViewIndex].size(); ++i)
                {
                    double distToPoint = cv::norm(cv::Mat(detectorParams[index].maskPoints[currentViewIndex][i]), cv::Mat(pt));

                    if (distToPoint < (10 * distanceMultiplier))
                    {
                        // The selected point is within a radius of 10 pixels to an existing point. 
                        // Move this point around instead of creating a new point
                        currentMaskPoint = i;
                        nearExistingPoint = true;
                    }
                }

                if (!nearExistingPoint)
                {
                    // Insert a new point
                    detectorParams[index].maskPoints[currentViewIndex].push_back(pt);
                    this->ui->drawFinishResetMaskButton->setText("Finish mask");
                    currentMaskPoint = detectorParams[index].maskPoints[currentViewIndex].size() - 1;
                }

                drawMasks(maskIndices);
            }

        } else if(event->button() == Qt::NoButton && (event->buttons() & Qt::LeftButton) == 1) { // Move event, left button down

            if (!detectorParams[index].IsMaskInitialized[currentViewIndex] && 
                detectorParams[index].maskPoints[currentViewIndex].size() > currentMaskPoint)
            {
                detectorParams[index].maskPoints[currentViewIndex][currentMaskPoint] = pt;
                drawMasks(maskIndices);
            }

        } else if(event->button() == Qt::LeftButton && (event->buttons() & Qt::LeftButton) == 0) { // Release left button
            // Do something
            if (!detectorParams[index].IsMaskInitialized[currentViewIndex] &&
                detectorParams[index].maskPoints[currentViewIndex].size() > currentMaskPoint)
            {
                // As we have released the left mouse button, we have now successfully placed a point in the mask
                detectorParams[index].maskPoints[currentViewIndex][currentMaskPoint] = pt;
                drawMasks(maskIndices);
            }
        }

        if (detectorParams[index].maskPoints[currentViewIndex].size() > 2)
        {
            // If more than two points are selected, we may finish the mask by choice
            this->ui->drawFinishResetMaskButton->setEnabled(true);
        } else {
            this->ui->drawFinishResetMaskButton->setEnabled(false);
        }
    }
    
    if (drawingMode == DrawingMode::TrafficLight)
    {

        if (event->button() == Qt::LeftButton && (event->buttons() & Qt::LeftButton) == Qt::LeftButton) { // Left click event.

			if (annotatedTrafficLight < detectorParams[index].trafficLightPositions.size() && annotatedTrafficLight >= 0)
			{
				detectorParams[index].trafficLightPositions[annotatedTrafficLight] = pt;
			}
        }
		else if (event->button() == Qt::NoButton && (event->buttons() & Qt::LeftButton) == 1) { // Move event, left button down

			if (annotatedTrafficLight < detectorParams[index].trafficLightPositions.size() && annotatedTrafficLight >= 0)
			{
				detectorParams[index].trafficLightPositions[annotatedTrafficLight] = pt;
			}
		}
		else if (event->button() == Qt::LeftButton && (event->buttons() & Qt::LeftButton) == 0) { // Release left button
			if (annotatedTrafficLight < detectorParams[index].trafficLightPositions.size() && annotatedTrafficLight >= 0)
			{
				detectorParams[index].trafficLightPositions[annotatedTrafficLight] = pt;
			}
		}
        else if (event->button() == Qt::RightButton && (event->buttons() & Qt::RightButton) == Qt::RightButton) { // Right click event

			bool annotationSuccess = false;

            if (annotatedTrafficLight == RED){
				drawingMode = DrawingMode::Disabled;
                this->ui->redAnnotateButton->setToolTip("Red light annotated");
				this->ui->redAnnotateButton->setIcon(QIcon("://icons/traffic-light-red.png"));
				annotatedTrafficLight = tlTypeClasses::AMBIGIOUS;
				ui->redCheckBox->setChecked(true);
				drawingMode = DrawingMode::Disabled;
				annotationSuccess = true;
            }
            else if (annotatedTrafficLight == YELLOW) {
				drawingMode = DrawingMode::Disabled;
                this->ui->yellowAnnotateButton->setToolTip("Yellow light annotated");
				this->ui->yellowAnnotateButton->setIcon(QIcon("://icons/traffic-light-yellow.png"));
				annotatedTrafficLight = tlTypeClasses::AMBIGIOUS;
				ui->yellowCheckBox->setChecked(true);
				drawingMode = DrawingMode::Disabled;
				annotationSuccess = true;
            }
            else if (annotatedTrafficLight == GREEN) {
				drawingMode = DrawingMode::Disabled;
                this->ui->greenAnnotateButton->setToolTip("Green light annotated");
				this->ui->greenAnnotateButton->setIcon(QIcon("://icons/traffic-light-green.png"));
				annotatedTrafficLight = tlTypeClasses::AMBIGIOUS;
				drawingMode = DrawingMode::Disabled;
				ui->greenCheckBox->setChecked(true);
				annotationSuccess = true;
            }

			if (annotationSuccess)
			{
				checkConfiguration();
				setUpLayout(true, this->ui->comboBox->currentIndex() - 1);
			}
        } 

		drawMasks(maskIndices);
	}
    

}
void DetectorSelectionDialog::on_redAnnotateButton_clicked() {
    
	// Change the button icon to ticked off icon. 
	if (drawingMode == DrawingMode::TrafficLight && annotatedTrafficLight == tlTypeClasses::RED) {
		annotatedTrafficLight = tlTypeClasses::AMBIGIOUS;
		drawingMode = DrawingMode::Disabled;
		this->ui->redAnnotateButton->setToolTip(tr("Red light annotated"));
		this->ui->redAnnotateButton->setIcon(QIcon("://icons/traffic-light-red.png"));
		ui->redCheckBox->setChecked(true);
	}
	else if (drawingMode == DrawingMode::Disabled) {
		annotatedTrafficLight = tlTypeClasses::RED;
		drawingMode = DrawingMode::TrafficLight;
		ui->redCheckBox->setChecked(false);
		this->ui->redAnnotateButton->setIcon(QIcon("://icons/pencil.png"));
		this->ui->redAnnotateButton->setToolTip("Annotate Red light");
	}
	checkConfiguration();
	setUpLayout(true, this->ui->comboBox->currentIndex() - 1);

	vector<int> maskIndices = { this->ui->comboBox->currentIndex() - 1 };
	drawMasks(maskIndices);
}

void DetectorSelectionDialog::on_yellowAnnotateButton_clicked() {
    
    // Change the button icon to ticked off icon. 
    if (drawingMode == DrawingMode::TrafficLight && annotatedTrafficLight == tlTypeClasses::YELLOW) {
        annotatedTrafficLight = tlTypeClasses::AMBIGIOUS;
		drawingMode = DrawingMode::Disabled;
        this->ui->yellowAnnotateButton->setToolTip(tr("Yellow light annotated"));
        this->ui->yellowAnnotateButton->setIcon(QIcon("://icons/traffic-light-yellow.png"));
        ui->yellowCheckBox->setChecked(true);
    }
    else if (drawingMode == DrawingMode::Disabled) {
		annotatedTrafficLight = tlTypeClasses::YELLOW;
		drawingMode = DrawingMode::TrafficLight;
        ui->yellowCheckBox->setChecked(false);
        this->ui->yellowAnnotateButton->setIcon(QIcon("://icons/pencil.png"));
		this->ui->yellowAnnotateButton->setToolTip("Annotate Yellow light");
    }
    checkConfiguration();
	setUpLayout(true, this->ui->comboBox->currentIndex() - 1);

	vector<int> maskIndices = { this->ui->comboBox->currentIndex() - 1 };
	drawMasks(maskIndices);
}

void DetectorSelectionDialog::on_greenAnnotateButton_clicked() {
    
	// Change the button icon to ticked off icon. 
	if (drawingMode == DrawingMode::TrafficLight && annotatedTrafficLight == tlTypeClasses::GREEN) {
		annotatedTrafficLight = tlTypeClasses::AMBIGIOUS;
		drawingMode = DrawingMode::Disabled;
		this->ui->greenAnnotateButton->setToolTip(tr("Green light annotated"));
		this->ui->greenAnnotateButton->setIcon(QIcon("://icons/traffic-light-green.png"));
		ui->greenCheckBox->setChecked(true);
	}
	else if (drawingMode == DrawingMode::Disabled) {
		annotatedTrafficLight = tlTypeClasses::GREEN;
		drawingMode = DrawingMode::TrafficLight;
		ui->greenCheckBox->setChecked(false);
		this->ui->greenAnnotateButton->setIcon(QIcon("://icons/pencil.png"));
		this->ui->greenAnnotateButton->setToolTip("Annotate Green light");
	}
	checkConfiguration();
	setUpLayout(true, this->ui->comboBox->currentIndex() - 1);

	vector<int> maskIndices = { this->ui->comboBox->currentIndex() - 1 };
	drawMasks(maskIndices);
}

void DetectorSelectionDialog::on_redButton_clicked(bool checked)
{
    int index = this->ui->comboBox->currentIndex() - 1;

    if (index >= 0) {        
        detectorParams[index].trafficLightTypeTrigger = tlTypeClasses::RED;
        checkConfiguration();
    }
}

void DetectorSelectionDialog::on_yellowButton_clicked(bool checked)
{
    int index = this->ui->comboBox->currentIndex() - 1;

    if (index >= 0) {
        detectorParams[index].trafficLightTypeTrigger = tlTypeClasses::YELLOW;
        checkConfiguration();
    }
}

void DetectorSelectionDialog::on_greenButton_clicked(bool checked)
{
    int index = this->ui->comboBox->currentIndex() - 1;

    if (index >= 0) {
        detectorParams[index].trafficLightTypeTrigger = tlTypeClasses::GREEN;
        checkConfiguration();
    }
}




void DetectorSelectionDialog::on_drawFinishResetMaskButton_clicked()
{
    // The drawFinishReset Button either enables the user to draw the mask, finish the mask or reset an already existing mask
    int index = this->ui->comboBox->currentIndex()-1;
    vector<int> maskIndices;
    maskIndices.push_back(index);
    prevDetectorType = detectorType;


    if (this->ui->drawFinishResetMaskButton->text() == tr("Draw mask") || this->ui->drawFinishResetMaskButton->text() == tr("Redraw mask"))
    {
        // We have chosen to actively draw the mask by hand
		drawingMode = DrawingMode::Mask;
        this->ui->drawFinishResetMaskButton->setText(tr("Finish mask"));
        this->ui->drawFinishResetMaskButton->setIcon(QIcon("://icons/tick.png"));
        this->ui->drawFinishResetMaskButton->setToolTip(tr("Finish mask"));
        setUpLayout(false, index);

        if (detectorParams[index].maskPoints[currentViewIndex].size() < 2)
        {
            this->ui->drawFinishResetMaskButton->setEnabled(false);
        }
        else {
            this->ui->drawFinishResetMaskButton->setEnabled(true);
        }

        setUpLayout(false, index);


        ui->loadMaskButton->setEnabled(false);
        ui->saveMaskButton->setEnabled(false);
        ui->loadMaskPointsButton->setEnabled(false);
        ui->saveMaskPointsButton->setEnabled(false);
        detectorParams[index].IsMaskInitialized[currentViewIndex] = false;
        drawMasks(maskIndices);
        mviewer->setFocus();

    } else if (this->ui->drawFinishResetMaskButton->text() == "Finish mask")
    {
        finishMask(index);
		drawingMode = DrawingMode::Disabled;
        setUpLayout(true, index);


    } else if (this->ui->drawFinishResetMaskButton->text() == "Reset mask")
    {
        this->ui->drawFinishResetMaskButton->setText("Draw mask");
        this->ui->drawFinishResetMaskButton->setIcon(QIcon("://icons/pencil.png"));
        this->ui->drawFinishResetMaskButton->setToolTip("Draw mask");
        ui->saveMaskButton->setEnabled(false);
        ui->saveMaskPointsButton->setEnabled(false);

        detectorParams[index].IsMaskInitialized[currentViewIndex] = false;
		drawingMode = DrawingMode::Disabled;
        drawMasks(maskIndices);
        setUpLayout(false, index);
    }

    checkConfiguration();
}

void DetectorSelectionDialog::on_defineBackgroundImageButton_clicked()
{
    QSettings settings;
    settings.beginGroup("detectorSelectionDialog");
    QString baseDir = settings.value("maskBackgroundBaseDir", "").toString();
    settings.endGroup();

    QString filename = QFileDialog::getOpenFileName(this, tr("Load background image"), 
        baseDir, tr("Images (*.png *.bmp *.jpg)"));

    if(filename.isEmpty())
    {
        return;
    }

    settings.beginGroup("detectorSelectionDialog");
    settings.setValue("maskBackgroundBaseDir", QFileInfo(filename).absolutePath());
    settings.endGroup();

    Mat backImage =  cv::imread(filename.toStdString(), 1);

    if ((backImage.cols != images[currentViewIndex].cols) || (backImage.rows != images[currentViewIndex].rows))
    {
        QString errorStr = tr("The background image must have the same dimensions as the video frame, ");
        errorStr = errorStr + QString::number(images[currentViewIndex].cols) + "x" + QString::number(images[currentViewIndex].rows) + tr(" pixels .");
        errorStr.append(tr("\nThe dimensions of the input image are "));
        errorStr = errorStr + QString::number(backImage.cols) + "x" + QString::number(backImage.rows) + tr("pixels. ");
        QMessageBox::warning(this, tr("Error loading image"), errorStr, QMessageBox::Close);
        return;
    }

    int index = this->ui->comboBox->currentIndex()-1;
    vector<int> maskIndices;
    maskIndices.push_back(index);

    detectorParams[index].BackgroundImages[currentViewIndex] = cv::imread(filename.toStdString(), 1);
    drawMasks(maskIndices);

}

void DetectorSelectionDialog::on_saveBackgroundImageButton_clicked()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save background image"), tr(""), tr("Images (*.png *.bmp *.jpg)"));

    int index = this->ui->comboBox->currentIndex()-1;

    if (!filename.isEmpty() && !detectorParams[index].BackgroundImages[currentViewIndex].empty())
    {
        cv::imwrite(filename.toStdString(), detectorParams[index].BackgroundImages[currentViewIndex]);
    }

}


void DetectorSelectionDialog::on_angleDial21_valueChanged(int value)
{
    if (value >= 90)
    {
        this->ui->angleBox21->setValue(value-90);
    } else {
        this->ui->angleBox21->setValue(value+270);
    }
}

void DetectorSelectionDialog::on_angleDial22_valueChanged(int value)
{
    if (value >= 90)
    {
        this->ui->angleBox22->setValue(value-90);
    } else {
        this->ui->angleBox22->setValue(value+270);
    }
}

void DetectorSelectionDialog::on_angleBox21_valueChanged(double arg1)
{
    if (arg1 < 270)
    {
        this->ui->angleDial21->setSliderPosition(arg1+90);
    } else {
        this->ui->angleDial21->setSliderPosition(arg1-270);
    }

    this->ProcessTimer.stop();
    drawAngleHelper(this->ui->angleBox21->value(), this->ui->angleBox22->value(), false);

    // Save the angle
    int index = this->ui->comboBox->currentIndex()-1;
    detectorParams[index].FlowRange[currentViewIndex][0].x = this->ui->angleBox21->value();

}


void DetectorSelectionDialog::on_angleBox22_valueChanged(double arg1)
{
    if (arg1 < 270)
    {
        this->ui->angleDial22->setSliderPosition(arg1+90);
    } else {
        this->ui->angleDial22->setSliderPosition(arg1-270);
    }

    this->ProcessTimer.stop();
    drawAngleHelper(this->ui->angleBox21->value(), this->ui->angleBox22->value(), false);

    // Save the angle
    int index = this->ui->comboBox->currentIndex()-1;
    detectorParams[index].FlowRange[currentViewIndex][0].y = this->ui->angleBox22->value();

}

void DetectorSelectionDialog::on_angleDial22_sliderReleased()
{
    QTimer::singleShot(1000, this, SLOT(restartTimer()));
}

void DetectorSelectionDialog::on_angleDial21_sliderReleased()
{
    QTimer::singleShot(1000, this, SLOT(restartTimer()));
}

void DetectorSelectionDialog::on_angleBox21_editingFinished()
{
    QTimer::singleShot(1000, this, SLOT(restartTimer()));
}

void DetectorSelectionDialog::on_angleBox22_editingFinished()
{
    QTimer::singleShot(1000, this, SLOT(restartTimer()));
}

void DetectorSelectionDialog::on_angleDial21_sliderPressed()
{
    this->ProcessTimer.stop();
    drawAngleHelper(this->ui->angleBox21->value(), this->ui->angleBox22->value(), false);

}

void DetectorSelectionDialog::on_angleDial22_sliderPressed()
{
    this->ProcessTimer.stop();
    drawAngleHelper(this->ui->angleBox21->value(), this->ui->angleBox22->value(), false);
}

void DetectorSelectionDialog::on_angleDial31_valueChanged(int value)
{
    if (value >= 90)
    {
        this->ui->angleBox31->setValue(value-90);
    } else {
        this->ui->angleBox31->setValue(value+270);
    }
}

void DetectorSelectionDialog::on_angleDial32_valueChanged(int value)
{
    if (value >= 90)
    {
        this->ui->angleBox32->setValue(value-90);
    } else {
        this->ui->angleBox32->setValue(value+270);
    }
}

void DetectorSelectionDialog::on_angleBox31_valueChanged(double arg1)
{
    if (arg1 < 270)
    {
        this->ui->angleDial31->setSliderPosition(arg1+90);
    } else {
        this->ui->angleDial31->setSliderPosition(arg1-270);
    }

    this->ProcessTimer.stop();
    drawAngleHelper(this->ui->angleBox31->value(), this->ui->angleBox32->value(), false);

    // Save the angle
    int index = this->ui->comboBox->currentIndex()-1;
    detectorParams[index].FlowRange[currentViewIndex][1].x = arg1;

}


void DetectorSelectionDialog::on_angleBox32_valueChanged(double arg1)
{
    if (arg1 < 270)
    {
        this->ui->angleDial32->setSliderPosition(arg1+90);
    } else {
        this->ui->angleDial32->setSliderPosition(arg1-270);
    }

    this->ProcessTimer.stop();
    drawAngleHelper(this->ui->angleBox31->value(), this->ui->angleBox32->value(), false);

    // Save the angle
    int index = this->ui->comboBox->currentIndex()-1;
    detectorParams[index].FlowRange[currentViewIndex][1].y = arg1;

}

void DetectorSelectionDialog::on_angleDial32_sliderReleased()
{
    QTimer::singleShot(1000, this, SLOT(restartTimer()));
}

void DetectorSelectionDialog::on_angleDial31_sliderReleased()
{
    QTimer::singleShot(1000, this, SLOT(restartTimer()));
}

void DetectorSelectionDialog::on_angleBox31_editingFinished()
{
    QTimer::singleShot(1000, this, SLOT(restartTimer()));
}

void DetectorSelectionDialog::on_angleBox32_editingFinished()
{
    QTimer::singleShot(1000, this, SLOT(restartTimer()));
}

void DetectorSelectionDialog::on_angleDial31_sliderPressed()
{
    this->ProcessTimer.stop();
    drawAngleHelper(this->ui->angleBox31->value(), this->ui->angleBox32->value(), false);

}

void DetectorSelectionDialog::on_angleDial32_sliderPressed()
{
    this->ProcessTimer.stop();
    drawAngleHelper(this->ui->angleBox31->value(), this->ui->angleBox32->value(), false);
}

void DetectorSelectionDialog::restartTimer()
{
    vector<int> maskIndices;
    int index = this->ui->comboBox->currentIndex()-1;
    maskIndices.push_back(index);
    drawMasks(maskIndices); // Delete the angleHelper
	processTimerTick();

    currentFlowVecAngle[index][0] = this->ui->angleBox21->value(); // Reset the flow vector position

    if (this->detectorParams[index].EnableWrongDirection)
    {
        if (currentFlowVecAngle[index].size() <= 1)
        {
            currentFlowVecAngle[index].push_back(0);
        }

        currentFlowVecAngle[index][1] = this->ui->angleBox31->value(); // Reset the flow vector position
    }

    this->ProcessTimer.start();
}

void DetectorSelectionDialog::checkConfiguration()
{
    // checkConfiguration detects if all all detectors + logs are initialized
    // and enables the saveConfiguration and OK button accordingly
    bool isConfigurationInitialized = true;

    for (size_t i = 0; i < detectorParams.size(); ++i)
    {   
        bool isDetectorInitialized = true;

        for (int j = 0; j < views.size(); ++j)
        {
            if (!detectorParams[i].IsMaskInitialized[j])
            {
                isConfigurationInitialized = false;
                isDetectorInitialized = false;
                break;
            }
        }
        detectorParams[i].IsInitialized = isDetectorInitialized;

        if (detectorParams[i].DetectorType == TRAFFICLIGHTDETECTOR)
        {
			bool tlTriggerSelected = false;

			for (int j = tlTypeClasses::RED; j <= tlTypeClasses::GREEN; j++)
			{
				if (detectorParams[i].trafficLightPositions[j].x <= 0 || detectorParams[i].trafficLightPositions[j].y <= 0)
				{
					isConfigurationInitialized = false;
				}

				if (detectorParams[i].trafficLightTypeTrigger == j)
				{
					tlTriggerSelected = true;
				}
			}

			if (!tlTriggerSelected)
			{
				isConfigurationInitialized = false;
			}
        }
    }    

    if (isConfigurationInitialized)
    {
        this->ui->okButtonBox->setEnabled(true);
        this->ui->saveConfigButton->setEnabled(true);
        this->ui->saveConfigAsButton->setEnabled(true);
    } else
    {
        this->ui->okButtonBox->setEnabled(false);
        this->ui->saveConfigButton->setEnabled(false);
        this->ui->saveConfigAsButton->setEnabled(false);
    }

}

void DetectorSelectionDialog::on_viewSwitch_currentChanged(int index)
{
    // Change the current layout based on the chosen modality
    if (views.size() > index)
    {
        currentViewIndex = index;
    }
    else {
        currentViewIndex = 0;
    }

    

    on_comboBox_currentIndexChanged();

    //if (isDrawingEnabled) {
    //    // If drawing was enabled on previous modality, enable it again
    //    on_drawFinishResetMaskButton_clicked();
    //}
}

void DetectorSelectionDialog::on_moveMaskPointUpButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, Qt::Key_W, Qt::NoModifier);
    QCoreApplication::postEvent(this, event);
}

void DetectorSelectionDialog::on_moveMaskPointDownButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, Qt::Key_S, Qt::NoModifier);
    QCoreApplication::postEvent(this, event);
}

void DetectorSelectionDialog::on_moveMaskPointLeftButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, Qt::Key_A, Qt::NoModifier);
    QCoreApplication::postEvent(this, event);
}

void DetectorSelectionDialog::on_moveMaskPointRightButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, Qt::Key_D, Qt::NoModifier);
    QCoreApplication::postEvent(this, event);
}

void DetectorSelectionDialog::on_selectPrevMaskPointButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, Qt::Key_Home, Qt::NoModifier);
    QCoreApplication::postEvent(this, event);
}

void DetectorSelectionDialog::on_selectNextMaskPointButton_clicked()
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, Qt::Key_End, Qt::NoModifier);
    QCoreApplication::postEvent(this, event);
}