#include "detectortypeselectiondialog.h"
#include "ui_detectortypeselectiondialog.h"

DetectorTypeSelectionDialog::DetectorTypeSelectionDialog(QWidget *parent, int detectorCount, bool exclusive) :
    QDialog(parent),
    ui(new Ui::DetectorTypeSelectionDialog)
{
    ui->setupUi(this);

    if  (detectorCount < 1)
    {
        this->reject();
    }

	this->filename = "";
    this->detectorCount = detectorCount;
    this->detectorIsSelected.resize(detectorCount);

    // Set up the buttons
    this->ui->detector1GroupBox->setVisible(true);

    bool secondDetectorVisible = false;
    if (detectorCount >= 2)
    {
        secondDetectorVisible = true;
    }

	if (exclusive)
	{
        ui->detector1GroupBox->setTitle(tr("Choose type of main detector"));
        ui->detector2GroupBox->setTitle(tr("Choose type of excluding detector"));
	}

    this->ui->detector2GroupBox->setVisible(secondDetectorVisible);

    // Disable the ok button when no detector is chosen
    this->ui->okButtonBox->setEnabled(false);

	auto size = this->ui->verticalLayout->sizeHint();
	resize(size);

}

DetectorTypeSelectionDialog::~DetectorTypeSelectionDialog()
{
    delete ui;
}

void DetectorTypeSelectionDialog::on_detectorButton11_toggled(bool checked)
{
    if (checked)
    {
        // Uncheck the other buttons
        this->ui->detectorButton12->blockSignals(true);
        this->ui->detectorButton12->setChecked(false);
        this->ui->detectorButton12->blockSignals(false);

        this->ui->detectorButton13->blockSignals(true);
        this->ui->detectorButton13->setChecked(false);
        this->ui->detectorButton13->blockSignals(false);

		this->ui->detectorButton14->blockSignals(true);
		this->ui->detectorButton14->setChecked(false);
		this->ui->detectorButton14->blockSignals(false);

        this->detectorIsSelected[0] = true;
        this->setUpLayout();
    } else {
        // The buttons are working as radio buttons, which means you cannot untoggle them directly
        this->ui->detectorButton11->blockSignals(true);
        this->ui->detectorButton11->setChecked(true);
        this->ui->detectorButton11->blockSignals(false);
    }
}

void DetectorTypeSelectionDialog::on_detectorButton12_toggled(bool checked)
{
    if (checked)
    {
        // Uncheck the other buttons
        this->ui->detectorButton11->blockSignals(true);
        this->ui->detectorButton11->setChecked(false);
        this->ui->detectorButton11->blockSignals(false);

        this->ui->detectorButton13->blockSignals(true);
        this->ui->detectorButton13->setChecked(false);
        this->ui->detectorButton13->blockSignals(false);

		this->ui->detectorButton14->blockSignals(true);
		this->ui->detectorButton14->setChecked(false);
		this->ui->detectorButton14->blockSignals(false);

        this->detectorIsSelected[0] = true;
        this->setUpLayout();
    } else {
        // The buttons are working as radio buttons, which means you cannot untoggle them directly
        this->ui->detectorButton12->blockSignals(true);
        this->ui->detectorButton12->setChecked(true);
        this->ui->detectorButton12->blockSignals(false);
    }
}

void DetectorTypeSelectionDialog::on_detectorButton13_toggled(bool checked)
{
    if (checked)
    {
        // Uncheck the other buttons
        this->ui->detectorButton11->blockSignals(true);
        this->ui->detectorButton11->setChecked(false);
        this->ui->detectorButton11->blockSignals(false);

        this->ui->detectorButton12->blockSignals(true);
        this->ui->detectorButton12->setChecked(false);
        this->ui->detectorButton12->blockSignals(false);

		this->ui->detectorButton14->blockSignals(true);
		this->ui->detectorButton14->setChecked(false);
		this->ui->detectorButton14->blockSignals(false);

        this->detectorIsSelected[0] = true;
        this->setUpLayout();
    } else {
        // The buttons are working as radio buttons, which means you cannot untoggle them directly
        this->ui->detectorButton13->blockSignals(true);
        this->ui->detectorButton13->setChecked(true);
        this->ui->detectorButton13->blockSignals(false);
    }
}

void DetectorTypeSelectionDialog::on_detectorButton14_toggled(bool checked)
{
	if (checked)
	{
		// Uncheck the other buttons
		this->ui->detectorButton11->blockSignals(true);
		this->ui->detectorButton11->setChecked(false);
		this->ui->detectorButton11->blockSignals(false);

		this->ui->detectorButton12->blockSignals(true);
		this->ui->detectorButton12->setChecked(false);
		this->ui->detectorButton12->blockSignals(false);

		this->ui->detectorButton13->blockSignals(true);
		this->ui->detectorButton13->setChecked(false);
		this->ui->detectorButton13->blockSignals(false);

		this->detectorIsSelected[0] = true;
		this->setUpLayout();
	}
	else {
		// The buttons are working as radio buttons, which means you cannot untoggle them directly
		this->ui->detectorButton14->blockSignals(true);
		this->ui->detectorButton14->setChecked(true);
		this->ui->detectorButton14->blockSignals(false);
		
	}
}

void DetectorTypeSelectionDialog::on_detectorButton21_toggled(bool checked)
{
    if (checked)
    {
        // Uncheck the other buttons
        this->ui->detectorButton22->blockSignals(true);
        this->ui->detectorButton22->setChecked(false);
        this->ui->detectorButton22->blockSignals(false);

        this->ui->detectorButton23->blockSignals(true);
        this->ui->detectorButton23->setChecked(false);
        this->ui->detectorButton23->blockSignals(false);

		this->ui->detectorButton24->blockSignals(true);
		this->ui->detectorButton24->setChecked(false);
		this->ui->detectorButton24->blockSignals(false);

        this->detectorIsSelected[1] = true;
        this->setUpLayout();
    } else {
        // The buttons are working as radio buttons, which means you cannot untoggle them directly
        this->ui->detectorButton21->blockSignals(true);
        this->ui->detectorButton21->setChecked(true);
        this->ui->detectorButton21->blockSignals(false);
    }
}

void DetectorTypeSelectionDialog::on_detectorButton22_toggled(bool checked)
{
    if (checked)
    {
        // Uncheck the other buttons
        this->ui->detectorButton21->blockSignals(true);
        this->ui->detectorButton21->setChecked(false);
        this->ui->detectorButton21->blockSignals(false);

        this->ui->detectorButton23->blockSignals(true);
        this->ui->detectorButton23->setChecked(false);
        this->ui->detectorButton23->blockSignals(false);

		this->ui->detectorButton24->blockSignals(true);
		this->ui->detectorButton24->setChecked(false);
		this->ui->detectorButton24->blockSignals(false);

        this->detectorIsSelected[1] = true;
        this->setUpLayout();
    } else {
        // The buttons are working as radio buttons, which means you cannot untoggle them directly
        this->ui->detectorButton22->blockSignals(true);
        this->ui->detectorButton22->setChecked(true);
        this->ui->detectorButton22->blockSignals(false);
    }
}

void DetectorTypeSelectionDialog::on_detectorButton23_toggled(bool checked)
{
    if (checked)
    {
        // Uncheck the other buttons
        this->ui->detectorButton21->blockSignals(true);
        this->ui->detectorButton21->setChecked(false);
        this->ui->detectorButton21->blockSignals(false);

        this->ui->detectorButton22->blockSignals(true);
        this->ui->detectorButton22->setChecked(false);
        this->ui->detectorButton22->blockSignals(false);

		this->ui->detectorButton24->blockSignals(true);
		this->ui->detectorButton24->setChecked(false);
		this->ui->detectorButton24->blockSignals(false);

        this->detectorIsSelected[1] = true;
        this->setUpLayout();
    } else {
        // The buttons are working as radio buttons, which means you cannot untoggle them directly
        this->ui->detectorButton23->blockSignals(true);
        this->ui->detectorButton23->setChecked(true);
        this->ui->detectorButton23->blockSignals(false);
    }
}

void DetectorTypeSelectionDialog::on_detectorButton24_toggled(bool checked)
{
	if (checked)
	{
		// Uncheck the other buttons
		this->ui->detectorButton21->blockSignals(true);
		this->ui->detectorButton21->setChecked(false);
		this->ui->detectorButton21->blockSignals(false);

		this->ui->detectorButton22->blockSignals(true);
		this->ui->detectorButton22->setChecked(false);
		this->ui->detectorButton22->blockSignals(false);

		this->ui->detectorButton23->blockSignals(true);
		this->ui->detectorButton23->setChecked(false);
		this->ui->detectorButton23->blockSignals(false);

		this->detectorIsSelected[1] = true;
		this->setUpLayout();
	}
	else {
		// The buttons are working as radio buttons, which means you cannot untoggle them directly
		this->ui->detectorButton24->blockSignals(true);
		this->ui->detectorButton24->setChecked(true);
		this->ui->detectorButton24->blockSignals(false);
	}
}

void DetectorTypeSelectionDialog::setUpLayout()
{
    bool allDetectorsChosen = true;

    for (int i = 0; i  < detectorCount; ++i)
    {
        if (!detectorIsSelected[i])
        {
            allDetectorsChosen = false;
        }
    }

    this->ui->okButtonBox->setEnabled(allDetectorsChosen);
}

QVector<int> DetectorTypeSelectionDialog::getOutput()
{
    // Get the selected detector types

    if (detectorOrder.isEmpty())
    {
        generateDetectorOrder();
    }


    return detectorOrder;
}

void DetectorTypeSelectionDialog::generateDetectorOrder()
{
    if (detectorCount > 0)
    {
        detectorOrder.resize(detectorCount);
    }

    if (detectorCount > 0)
    {
        if (this->ui->detectorButton11->isChecked())
        {
            detectorOrder[0] = PRESENCEDETECTOR;
        }

        if (this->ui->detectorButton12->isChecked())
        {
            detectorOrder[0] = MOVEMENTDETECTOR;
        }

        if (this->ui->detectorButton13->isChecked())
        {
            detectorOrder[0] = STATIONARYDETECTOR;
        }
		if (this->ui->detectorButton14->isChecked())
		{
			detectorOrder[0] = TRAFFICLIGHTDETECTOR;
		}
    }

    if (detectorCount > 1)
    {
        if (this->ui->detectorButton21->isChecked())
        {
            detectorOrder[1] = PRESENCEDETECTOR;
        }

        if (this->ui->detectorButton22->isChecked())
        {
            detectorOrder[1] = MOVEMENTDETECTOR;
        }

        if (this->ui->detectorButton23->isChecked())
        {
            detectorOrder[1] = STATIONARYDETECTOR;
        }
		if (this->ui->detectorButton24->isChecked())
		{
			detectorOrder[1] = TRAFFICLIGHTDETECTOR;
		}
    }
}

void DetectorTypeSelectionDialog::on_loadConfigButton_clicked()
{
    filename = QFileDialog::getOpenFileName(this, tr("Load configuration parameters"), tr(""), tr("Configuration files (*.yml)"));

    int result = LoadConfiguration(filename);

    switch (result)
    {
    case 0:
        this->accept(); // Close the dialog
        break;
    case 1:
        {
        QMessageBox::warning(this, tr(this->windowTitle().toStdString().c_str()), "Unable to load file", QMessageBox::Ok);
        break;
        }
    case 2:
    {
        QMessageBox::warning(this, tr(this->windowTitle().toStdString().c_str()), "Unable to load detector configuration", QMessageBox::Ok);
    }
    case 3:
        break;
    }

}

int DetectorTypeSelectionDialog::LoadConfiguration(QString file)
{
    filename = file;

    if (!filename.isEmpty() && filename.contains(".yml"))
    {
        cv::FileStorage fs;

        try {
            fs.open(filename.toStdString(), cv::FileStorage::READ);
        } catch (cv::Exception e) {
            QString errorString = e.err.c_str();
            QString errorMsg = e.msg.c_str();
            qDebug() << "Error opening file: " << errorString;
            QString errorStr = "Trouble reading file. Something is wrong.\n\nError:\n" + errorString + "\n\n" +
                    errorMsg;
            QMessageBox::warning(this,"File error", errorStr);
            return 1;
        }

        std::vector<int> tmpOrder;
        fs["detectorOrder"] >> tmpOrder;

        if (!tmpOrder.empty())
        {
            detectorOrder.clear();

            for (size_t i = 0; i < tmpOrder.size(); ++i)
            {
                detectorOrder.push_back(tmpOrder[i]);
            }
            return 0;
        } else {
            return 2;
        }
    } else{
        return 3;
    }
}

QString DetectorTypeSelectionDialog::getConfigurationFilename()
{
    return filename;
}
