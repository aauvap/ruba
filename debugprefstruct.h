#ifndef DEBUGPREFSTRUCT_H
#define DEBUGPREFSTRUCT_H

#include <QVector>
#include "ruba.h"

struct ruba::DebugPrefStruct{
    QVector<bool> OverlayMasks;
    QVector<bool> ShowDebugWindows;
    QVector<bool> OverlayDebugInfo;
    QVector<bool> OverlayExtDebugInfo;

    bool OverlayTimestamp;
};


#endif // DEBUGPREFSTRUCT_H

