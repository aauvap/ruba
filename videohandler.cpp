#include "videohandler.h"
#include "QDebug"

using namespace cv;
using namespace std;
using namespace ruba;


VideoHandler::VideoHandler(QWidget *parent, QList<ViewInfo> views, bool persistentPlayback)
    : RubaBaseObject(parent, views)
{
    currentVideoListPos = 0;

    prevStatus = CaptureStatus::OK;
	externalVideoListChanged = false;
	viewErrorShown = false;
	this->persistentPlayback = persistentPlayback;
}

int ruba::VideoHandler::viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews)
{
    // Do not notify the videos in the list
    // We will take care of the changed views when retrieving frames

    return 0;
}

int VideoHandler::saveToFile(cv::FileStorage& fs, const std::string& entryPrefix)
{
    if (fs.isOpened())
    {
        // Save the current video position
        fs << "VHandler-currentVideoListPos" << currentVideoListPos;

        fs << "VHandler-FileCount" << internalVideoList.size();

        string entryBaseName = "VHandler-File";
        // Save the parameters of the synchronised videos
        for (auto i = 0; i < internalVideoList.size(); ++i)
        {
            string prefix = entryBaseName  + "-" + std::to_string(i);

            fs << prefix + "-SyncType" << internalVideoList[i]->getSyncType();

            internalVideoList[i]->saveToFile(fs, prefix);
        }

        return 0;
    }
    else {
        return 1;
    }
}

int VideoHandler::loadFromFile(cv::FileStorage& fs)
{   
    if (fs.isOpened())
    {
        // Reset the current lists
        internalVideoList.clear();
        videoListModel.clear();
		externalVideoListChanged = true;

        // Load the number of synchronised video captures in the previous video list
        int fileCount = 0;
        fs["VHandler-FileCount"] >> fileCount;
        string entryBaseName = "VHandler-File";

        // Try to restore the video captures
        for (auto i = 0; i < fileCount; ++i)
        {
			emit sendProgress(i + 1, fileCount, QObject::tr("Loading previous configuration - video %1 of %2").arg(i + 1).arg(fileCount));

            // Define the prefix string which the synchronised video capture
            // uses to load its previous configuration
            string entryPrefix = entryBaseName + "-" + std::to_string(i);

            // Get the type of the VideoCapture
            int tmpSyncType;
            fs[entryPrefix + "-SyncType"] >> tmpSyncType;
            VideoSyncType syncType = (VideoSyncType)tmpSyncType;
            bool videoAdded = false;

            switch (syncType)
            {
            case VideoSyncType::STARTTIMEINFILENAME:
            {
                auto syncCap = make_shared<SynchronisedVideoCaptureFromFile>(
                    getParent(),
                    fs,
                    entryPrefix);

                CaptureStatus status = syncCap->getCaptureStatus();

                if (status != CaptureStatus::OK)
                {
                    continue;
                }

                internalVideoList.append(syncCap);
				connect(syncCap.get(), SIGNAL(sendMessage(QString, QString, MessageType)), this, SIGNAL(sendMessage(QString, QString, MessageType)));
				connect(syncCap.get(), SIGNAL(sendProgress(int, int, QString)), this, SIGNAL(sendProgress(int, int, QString)));

                videoAdded = true;
                break;
            }
            case VideoSyncType::TIMESTAMPSINLOGFILE:
            {
                auto syncCap = make_shared<SynchronisedVideoCaptureFromFileTimestamps>(
                    getParent(),
                    fs,
                    entryPrefix);

                CaptureStatus status = syncCap->getCaptureStatus();

                if (status != CaptureStatus::OK)
                {
                    continue;
                }

                internalVideoList.append(syncCap);
				connect(syncCap.get(), SIGNAL(sendMessage(QString, QString, MessageType)), this, SIGNAL(sendMessage(QString, QString, MessageType)));
				connect(syncCap.get(), SIGNAL(sendProgress(int, int, QString)), this, SIGNAL(sendProgress(int, int, QString)));

                videoAdded = true;
                break;
            }
            case VideoSyncType::CAPTUREFROMSTREAM:
                // Fall-through as not implemented
            default:
                // Do nothing - not implemented
                break;
            }

            if (videoAdded)
            {
                // Add the entries to the external video list
                QDateTime startTime = internalVideoList.back()->getStartTime();
                QList<ViewInfo> views = internalVideoList.back()->getCurrentViews();

                // Update the external video list
                insertElementInExternalVideoList(internalVideoList.size()-1,
                    startTime, views);
            }

        }       

        // Load the previous position of the video
        int prevPos;
        fs["VHandler-currentVideoListPos"] >> prevPos;

        // Try to restore the video position
        setCurrentVideo(prevPos);

        return 0;
    }
    else {
        return 1;
    }
}

//! Overload for showing a GUI which lets the user select a desired dateTime
//! which should be found in the videoList
int VideoHandler::jumpToFrame()
{
    std::vector<FrameStruct> frames = getCurrentFrame();
    if (frames.empty())
    {
        return 1;
    }

    QDateTime time = frames[0].Timestamp;

    // Show GUI for selecting the requested dateTime
    jumptoframedialog d(getParent(), time);

    if (d.exec())
    {
        // If Ok button was pressed, jump to the requested dateTime
        int retVal = jumpToFrame(d.GetOutput());

        if (retVal == 0)
        {
            return 0;
        }
        else {
            QString dateTime = d.GetOutput().toString("dd-MM-yyyy hh:mm:ss");
            QString infoString = tr("Could not find the date and time \"") +
                dateTime + tr("\" in the video list.");

            emit sendMessage(tr("Date and time not found"), 
                infoString, MessageType::Information);
        }   
    }
          

    return 1;
}

//! Jump to the requested frame in the current video, if the frame exists
/*!
\param requestedFrameNumber
\return error code. Success == 0. If the start time of the video is invalid, return 1.
*/
int ruba::VideoHandler::jumpToFrame(int requestedFrameNumber)
{
    qDebug() << "JumpToFrame: Requested frame at: " << requestedFrameNumber;

    if (requestedFrameNumber < 0) 
    {
        requestedFrameNumber = 0;
    }

    if (currentVideoListPos < internalVideoList.size())
    {
        return internalVideoList[currentVideoListPos]->jumpToFrame(requestedFrameNumber);
    }
    else
    {

        return 1;
    }
}

bool ruba::VideoHandler::getIsInitialised() const
{
    if (currentVideoListPos >= 0 && currentVideoListPos < internalVideoList.size())
    {
        CaptureStatus status;
        std::vector<ruba::FrameStruct> frames = internalVideoList[currentVideoListPos]->getCurrentFrames(status);

        if (status != CaptureStatus::OK)
        {
            return false;
        }

        // Investigate if the frames are non-empty
        for (auto frame : frames)
        {
            if (frame.Image.empty())
            {
                return false;
            }
        }

        // We have passed the tests, i.e. we may return true
        return true;
    }
    else {
        return false;
    }

}
 


//! If any video in the current list contains the requestedTime, jump to that particular video at the corresponding frame
/*!
    \param requestedTime the date and time which is searched for
    \return 0 = the requestedTime is found and we have jumped to the corresponding point in the video, 1 = requestedTime was not found
*/

int ruba::VideoHandler::jumpToFrame(QDateTime requestedTime)
{
    qDebug() << "JumpToFrame: Requested time at: " << requestedTime.toString("dd-MM-yyyy hh:mm:ss.zzz");;



    // Check if the requestedTime is found within the current file
    if (currentVideoListPos >= internalVideoList.size())
    { 
		QString dateTime = requestedTime.toString("dd-MM-yyyy hh:mm:ss");
		QString infoString = tr("Could not find the date and time \"") +
			dateTime + tr("\" in the video list.");

		emit sendMessage(tr("Date and time not found"),
			infoString, MessageType::Information);
		
		return 1;
    }

    int retVal = internalVideoList[currentVideoListPos]->jumpToFrame(requestedTime);

    if (retVal == 0)
    {
        // If it is within the video, we are done. Make sure to extract the current frame once the call has ended
        return 0;
    }

    // If not, we need to traverse the videoList for potential matches
    for (int i = 0; i < internalVideoList.size(); ++i)
    {
        if (i != currentVideoListPos)
        {
            retVal = internalVideoList[i]->jumpToFrame(requestedTime);

            if (retVal == 0)
            {
                // We have found frames that correspond to the requested time.
                // Set the new video position
                setCurrentVideo(i);

                return 0; // Success!
            }
        }
    }
    
    // We have traversed the entire video list and found no entry to match 
    // the requested time. Return failure
	QString dateTime = requestedTime.toString("dd-MM-yyyy hh:mm:ss");
	QString infoString = tr("Could not find the date and time \"") +
		dateTime + tr("\" in the video list.");

	emit sendMessage(tr("Date and time not found"),
		infoString, MessageType::Information);

    return 1;
    
}

void VideoHandler::Release()
{
	for (auto& video : internalVideoList)
	{
		video->releaseVideo();
	}
}

int ruba::VideoHandler::jumpToPrevFrame(int prevFrameCount)
{
    long currentFrame = getFrameNbr();

    int retVal = 0;

    if ((currentFrame - prevFrameCount) >= 0)
    {
        jumpToFrame(currentFrame - prevFrameCount);
    }
    else {
        // Go back to the previous video, if possible, and open the last frame
        if (currentVideoListPos > 0 && (currentVideoListPos < internalVideoList.size()))
        {
            // Get the end time of the previous video
            int frameNumberAtEndTime = internalVideoList[currentVideoListPos - 1]->getFrameNumberAtEndTime();

            retVal += setCurrentVideo(currentVideoListPos - 1);

            // Find the exact frame in the previous video
            int requestedFrameNumber = frameNumberAtEndTime - prevFrameCount + currentFrame;
            
            retVal += jumpToFrame(requestedFrameNumber);
        }
    }

    return retVal;
}



//! Public: Clears the existing video list and replaces it with new videos
/*!
\param homedir home directory where the search for the video files begins
\sa addVideoFiles
*/
QString VideoHandler::setVideoFiles(QString homedir, VideoSyncType videoSyncType)
{
    return addVideoFiles(homedir, true, 0, videoSyncType);
}

QString VideoHandler::addVideoFiles(QString homedir, VideoSyncType videoSyncType)
{
    return addVideoFiles(homedir, false, internalVideoList.size(), videoSyncType);
}

void ruba::VideoHandler::addVideoFiles(const std::vector<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture>>& newVideos)
{
	for (auto& newVideo : newVideos)
	{
		auto syncCap = newVideo;
		syncCap->releaseVideo();

		int insertionIndex = internalVideoList.size();	

		connect(syncCap.get(), SIGNAL(sendMessage(QString, QString, MessageType)), this, SIGNAL(sendMessage(QString, QString, MessageType)));
		connect(syncCap.get(), SIGNAL(sendProgress(int, int, QString)), this, SIGNAL(sendProgress(int, int, QString)));

		QDateTime startTime = syncCap->getStartTime();
		internalVideoList.insert(insertionIndex, syncCap);

		// Update the external list
		insertElementInExternalVideoList(insertionIndex,
			startTime, getCurrentViews());
	}
}

void ruba::VideoHandler::getVideoFiles(std::vector<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture>>& videos)
{
	videos.clear();

	for (auto& video : internalVideoList)
	{
		videos.push_back(video);
	}
}

CaptureStatus ruba::VideoHandler::getCaptureStatus()
{
	return prevStatus;
}

bool ruba::VideoHandler::isExternalVideoListChanged()
{
	if (externalVideoListChanged)
	{
		externalVideoListChanged = false;
		videoListModel.layoutChanged();
		return true;
	}
	else {
		return false;
	}
}

//! Public: Updates internal video lists with user interaction and eventually auto-updates remaining modalities
/*!
\param homedir home directory where the search for the video files begins
\param clearExistingList clears the existing video list, if true
\param insertionIndex if clearExistingList is false, insert the new videos at this position
\return returns the updated path to homedir
\sa setVideoFiles
*/
QString VideoHandler::addVideoFiles(QString homedir, bool clearExistingList, int insertionIndex, VideoSyncType videoSyncType)
{
    prevStatus = CaptureStatus::OK;

    if (getCurrentViews().empty())
    {
		emit sendMessage(tr("No views are enabled"),
            tr("No views are currently enabled. Enable one or more views to import video files."), 
			MessageType::Information);

        return homedir;
    }

    // Clear the list of low resolution videos. We only want to deal with current videos
    lowResVideos.clear();

    // Based on the VideoSyncType, prompt the user to load video files of the corresponding type
    switch (videoSyncType)
    {
    case TIMESTAMPSINLOGFILE:
    {
        addVideoFilesWithTimestampsInFile(homedir, clearExistingList, insertionIndex);
        break;
    }
    case CAPTUREFROMSTREAM:
    {
		int retVal = addVideoStream(homedir, clearExistingList, insertionIndex);

		if (retVal != 0)
		{
			return QString();
		}

        break;
    }
    case STARTTIMEINFILENAME:
    {
        // Make fall-through to default
    }
    default:
    {
        addVideoFilesWithStartTimeInFilename(homedir, clearExistingList, insertionIndex);
    }
    }


    // Check if any low-resolution videos have been imported. If this is the case,
    // and the program is set to issue a warning, the list of "lowResVideos" is non-zero
    if (!lowResVideos.empty())
    {
        QSettings settings;
        settings.beginGroup("video");
        QString minWidth = QString::number(settings.value("lowResWidth", 320).toInt());
        QString minHeight = QString::number(settings.value("lowResHeight", 160).toInt());
        settings.endGroup();

        QString errorText = tr("The resolution of the imported videos is low "
            "which might affect the performance of the detectors.\nUse videos with a resolution "
            "above %1x%2 pixel to obtain better results.\n\nImported videos with low resolution:\n").arg(
                minWidth, minHeight);

        for (int i = 0; i < lowResVideos.size(); ++i)
        {
            QString basename = QFileInfo(lowResVideos[i].videoPath).fileName();

            errorText = errorText + tr("%1: \t%2x%3 pixel\n").arg(basename,
                QString::number(lowResVideos[i].resolution.width),
                QString::number(lowResVideos[i].resolution.height));
        }

        emit sendMessage(tr("Low video resolution"), errorText, MessageType::Warning);
        lowResVideos.clear();
    }

    if (clearExistingList)
    {
        // Open the first video on the list
        setCurrentVideo(0);
    }

    return homedir;

}

//! Private: Prompt the user to specify one or more video files to insert in the video list
/*!
    Specify one or more video files which share the attribute that they provide the start time
    of the video file in the file name

    \param homedir path to the directory where the user should start the search for files
    \param insertionIndex index in the existing video list where the videos should be inserted. 
*/
int VideoHandler::addVideoFilesWithStartTimeInFilename(QString &homedir, bool clearExistingList, int insertionIndex)
{
    QList<QList<InitFileProperties> > multiViewFileList;
    
    // Find files for the first modality in the list
    QList<ViewInfo> views = getCurrentViews();

    if (views.empty())
    {
        return 1;
    }

    ViewInfo currentView = views[0];
    QString filterText;
    getSupportedVideoFileTypesText(filterText);

    QList<QString> filenames = QFileDialog::getOpenFileNames(getParent(), tr("Open video file(s) for view %1")
        .arg(currentView.viewName()),
        homedir, filterText);

    if (filenames.empty())
    {
        return 1;
    }

    // Update the base directory
    QFileInfo dirInfo(filenames[0]);
    homedir = dirInfo.dir().absolutePath();

    QProgressDialog progress(tr("Adding video files"), tr("Cancel"),
        0, 100, getParent());

    QList<InitFileProperties> singleViewFileList;

    // Create file properties from the file names
    for (auto i = 0; i < filenames.size(); ++i)
    {
        InitFileProperties properties;
        properties.videofile = filenames[i];
        properties.viewInfo = currentView;
        
        if (i > 0)
        {
            properties.prevFilename = filenames[i - 1];
        }

        singleViewFileList.append(properties);
    }

    multiViewFileList.append(singleViewFileList);

    

    // If there are additional views enabled, try to get the corresponding files automatically
    
    for (auto i = 1; i < views.size(); ++i)
    {
        progress.setLabelText(tr("Searching for additional views..."));
        progress.setMaximum(views.size());
        progress.setValue(i);

        QList<InitFileProperties> correspondingFiles;

        int retVal = getCorrespondingVideoFilesForView(singleViewFileList,
            views[i], correspondingFiles);

        if (retVal != 0 || singleViewFileList.size() != correspondingFiles.size())
        {
            bool filelistComplete = false;
            QList<QString> filenames;

            while (!filelistComplete)
            {
                // We could not retrieve the corresponding files automatically. Prompt the user to specify 
                // them manually
                 filenames = QFileDialog::getOpenFileNames(getParent(), tr("Open corresponding video file(s) for view %1")
                    .arg(views[i].viewName()),
                    homedir, filterText);

                 if (filenames.empty())
                 {
                     return 1;
                 }

                if (filenames.size() != singleViewFileList.size())
                {
                    int decision = QMessageBox::warning(getParent(), tr("Number of files does not match"),
                        tr("The number of files specified for view %1 is not equal to the "
                            "number of files for view %2.\n"
                            "Number of files for view %1: %3\n"
                            "Number of files for view %2: %4\n\n"
                            "Try again?")
                        .arg(views[0].viewName())
                        .arg(views[i].viewName())
                        .arg(singleViewFileList.size())
                        .arg(filenames.size()),
                        
                        QMessageBox::Yes,
                        QMessageBox::No);

                    if (decision == QMessageBox::No)
                    {
                        return 1;
                    }
                }
                else {
                    filelistComplete = true;
                }
            }

            // Convert the file list to initFileProperties
            correspondingFiles.clear();

            for (auto j = 0; j < filenames.size(); ++j)
            {
                InitFileProperties prop;
                prop.videofile = filenames[j];
                prop.viewInfo = views[i];

                if (j > 0)
                {
                    prop.prevFilename = filenames[j - 1];
                }

                correspondingFiles.append(prop);
            }
        }

        // Finally add the file properties of view i to the file list
        multiViewFileList.append(correspondingFiles);
    }

    // Create new SynchonisedVideoCapture objects for each (multi-)view file and insert 
    // them in the internal and external video lists
    // The multiViewFileList is a QList<QList of InitFileProperties which holds:
    // n QList<InitFileProperties> for each view
    // each of these n QList<InitFileProperties> should hold m files
    // However, in order to initialise a SynchronisedVideoCapture, we should have
    // m of QList<InitFileProperties> which each holds n files. This restructuring will 
    // be performed when going through the double for loop

    if (clearExistingList)
    {
        internalVideoList.clear();
        videoListModel.clear();
		externalVideoListChanged = true;
    }
    
    if (multiViewFileList.empty())
        return 1;

    auto nbrSingleModalFiles = multiViewFileList[0].size();

    for (auto i = 1; i < multiViewFileList.size(); ++i)
    {
        if (nbrSingleModalFiles != multiViewFileList[i].size())
        {
            return 1;
        }
        else {
            nbrSingleModalFiles = multiViewFileList[i].size();
        }
    }

    progress.setLabelText(tr("Adding videos..."));
    progress.setMaximum(nbrSingleModalFiles);

    QDateTime prevDateTime;

    for (auto i = 0; i < multiViewFileList[0].size(); ++i)
    {
        if (progress.wasCanceled())
            return 1;

        progress.setValue(i);
        
        QList<InitFileProperties> multiViewFileSet;

        for (auto j = 0; j < multiViewFileList.size(); ++j)
        {
            InitFileProperties prop = multiViewFileList[j][i];
            prop.prevDateTime = prevDateTime;

            multiViewFileSet.append(prop);
        }

        // Update the internal list
        auto syncCap = make_shared<SynchronisedVideoCaptureFromFile>(
            getParent(),
            views,
            multiViewFileSet);

        CaptureStatus status = syncCap->getCaptureStatus();

        if (status != CaptureStatus::OK)
        {
            insertionIndex--;
            continue;
        }

        // Get resolution and check if it fulfils the minimum requirements
        QList<VideoResolutionProperties> resProps = syncCap->getVideoResolution();

        for (auto j = 0; j < resProps.size(); ++j)
        {
            if (checkResolution(resProps[j].resolution))
            {
                lowResVideos.append(resProps[j]);
            }
        }

        QDateTime startTime = syncCap->getStartTime();
        internalVideoList.insert(insertionIndex + i, syncCap);

        // Update the external list
        insertElementInExternalVideoList(insertionIndex + i,
            startTime, views);

        // Update the previous date time
        prevDateTime = syncCap->getEndTime();
    }

    progress.setValue(progress.maximum());
    
    return 0;
}

//! Check if the resolution of the provided video file is above the user-specified limits
/*!
\param resolution resolution of the video file to check
\return if the resolution of the video file is below the user-specified limit, the parameter is true. Else, false 
*/
bool VideoHandler::checkResolution(cv::Size resolution)
{
    bool videoIsLowRes = false;

    QSettings settings;
    settings.beginGroup("video");
    bool checkLowRes = settings.value("lowResWarning", 1).toBool();

    if (checkLowRes)
    {
        int lowResWidth = settings.value("lowResWidth", 320).toInt();
        int lowResHeight = settings.value("lowResHeight", 160).toInt();

        long minsum = lowResWidth * lowResHeight;
        long currentSum = resolution.width * resolution.height;

        if (minsum > currentSum)
        {
            videoIsLowRes = true;
        }
    }


    settings.endGroup();

    return videoIsLowRes;
}

//! Private: Prompt the user to specify one or more video files to insert in the video list
/*!
Specify one or more video files which share the attribute that they provide the start time
of the video file in the file name

\param homedir path to the directory where the user should start the search for files
\param insertionIndex index in the existing video list where the videos should be inserted.
*/
int VideoHandler::addVideoFilesWithTimestampsInFile(QString &homedir, bool clearExistingList, int insertionIndex)
{
    QList<QList<InitFilePropertiesTimestamps> > multiViewFileList;


    // Find files for the first modality in the list
    QList<ViewInfo> views = getCurrentViews();

    if (views.empty())
    {
        return 1;
    }

    QString filterText;
    getSupportedVideoFileTypesText(filterText);

    for (auto i = 0; i < views.size(); ++i)
    {
        QList<InitFilePropertiesTimestamps> singleViewFileList;

        bool filesFound = false;

        if (i > 0)
        {
            int retVal = getCorrespondingVideoFilesForView(multiViewFileList[0],
                views[i], singleViewFileList);

            if (retVal == 0 && multiViewFileList[0].size() == singleViewFileList.size())
            {
                filesFound = true;
            }
        }

        if (!filesFound)
        {
            bool filelistComplete = false;
            QList<QString> filenames;

            while (!filelistComplete)
            {
                // We could not retrieve the corresponding files automatically. Prompt the user to specify 
                // them manually
                QString corresponding = "";
                if (i > 0)
                    corresponding = " corresponding";

                filenames = QFileDialog::getOpenFileNames(getParent(), tr("Open%1 video file(s) for view %2")
                    .arg(corresponding)
                    .arg(views[i].viewName()),
                    homedir, filterText);

                if (filenames.empty())
                {
                    return 1;
                }

                if (i > 0 && filenames.size() != multiViewFileList[0].size())
                {
                    int decision = QMessageBox::warning(getParent(), tr("Number of files does not match"),
                        tr("The number of files specified for view %1 is not equal to the "
                            "number of files for view %2.\n"
                            "Number of files for view %1: %3\n"
                            "Number of files for view %2: %4\n\n"
                            "Try again?")
                        .arg(views[0].viewName())
                        .arg(views[i].viewName())
                        .arg(multiViewFileList[0].size())
                        .arg(filenames.size()),

                        QMessageBox::Yes,
                        QMessageBox::No);

                    if (decision == QMessageBox::No)
                    {
                        return 1;
                    }
                }
                else {
                    filelistComplete = true;
                }
            }

            // Update the base directory
            QFileInfo dirInfo(filenames[0]);
            homedir = dirInfo.dir().absolutePath();

            // Try to get the corresponding time stamps of the video files
            bool timestampsFileMissing = false;
            QString missingTimestampsText;

            // Create file properties from the file names
            for (auto j = 0; j < filenames.size(); ++j)
            {
                InitFilePropertiesTimestamps properties;
                properties.videofile = filenames[j];
                properties.viewInfo = views[i];

                int retVal = findCorrespondingTimestampsFile(properties.videofile,
                    properties.timestampsFile);

                if (retVal != 0 || properties.timestampsFile.isEmpty())
                {
                    timestampsFileMissing = true;
                    missingTimestampsText += tr("%1: %2\n").arg(j+1).arg(filenames[j]);
                }

                singleViewFileList.append(properties);
            }

            if (timestampsFileMissing)
            {
                // We could not automatically retrieve the corresponding log file of the time stamps
                // Prompt the user to input these manually
                QMessageBox::information(getParent(), tr("Could not find timestamps"),
                    tr("Could not find the corresponding timestamp files for the videos. \n"
                        "Make sure that each video file has a corresponding log file or change "
                        "the search string to find the log files in Settings.\n\n"
                        "The following video files are missing a timestamp:\n%1")
                    .arg(missingTimestampsText),
                    QMessageBox::Ok);
                filenames = QFileDialog::getOpenFileNames(getParent(), tr("Specify the log files manually for view %1")
                    .arg(views[i].viewName()),
                    homedir);

                if (filenames.size() == singleViewFileList.size())
                {
                    // We have got a list of files that contains timestamps! Update the properties 

                    for (auto j = 0; j < singleViewFileList.size(); ++j)
                    {
                        singleViewFileList[j].timestampsFile = filenames[j];
                    }

                }
                else {
                    QMessageBox::warning(getParent(), tr("File mismatch"), tr("The number of log files for view %1 "
                        "does not match the number of video files provided. Aborting.\n\n"
                        "Number of video files: %2\n"
                        "Number of log files with timestamps: %3")
                        .arg(views[i].viewName())
                        .arg(singleViewFileList.size())
                        .arg(filenames.size()),
                        QMessageBox::Ok);

                    return 0;
                }
            }

            // Finally add the file properties of view i to the file list
            multiViewFileList.append(singleViewFileList);
        }
    }

    if (clearExistingList)
    {
        internalVideoList.clear();
        videoListModel.clear();
		externalVideoListChanged = true;
    }

    // Create new SynchonisedVideoCapture objects for each (multi-)view file and insert 
    // them in the internal and external video lists
    // The multiViewFileList is a QList<QList of InitFileProperties which holds:
    // n QList<InitFileProperties> for each view
    // each of these n QList<InitFileProperties> should hold m files
    // However, in order to initialise a SynchronisedVideoCapture, we should have
    // m of QList<InitFileProperties> which each holds n files. This restructuring will 
    // be performed when going through the double for loop

    if (multiViewFileList.empty())
        return 1;

    auto nbrSingleModalFiles = multiViewFileList[0].size();

    for (auto i = 1; i < multiViewFileList.size(); ++i)
    {
        if (nbrSingleModalFiles != multiViewFileList[i].size())
        {
            return 1;
        }
        else {
            nbrSingleModalFiles = multiViewFileList[i].size();
        }
    }

    QProgressDialog progress(tr("Adding video files"), tr("Cancel"),
            0, 100, getParent());
    

    progress.setLabelText(tr("Adding videos..."));
    progress.setMaximum(nbrSingleModalFiles);

    // Create new SynchonisedVideoCapture objects for each (multi-)view file and insert 
    // them in the internal and external video lists
    for (auto i = 0; i < multiViewFileList[0].size(); ++i)
    {
        if (progress.wasCanceled())
            return 1;

        progress.setValue(i);

        QList<InitFilePropertiesTimestamps> multiViewFileSet;

        for (auto j = 0; j < multiViewFileList.size(); ++j)
        {
            multiViewFileSet.append(multiViewFileList[j][i]);
        }


        // Update the internal list
        auto syncCap = make_shared<SynchronisedVideoCaptureFromFileTimestamps>(
            getParent(),
            views,
            multiViewFileSet);


        CaptureStatus status = syncCap->getCaptureStatus();

        if (status != CaptureStatus::OK)
        {
            insertionIndex--;
            continue;
        }

        // Get resolution and check if it fulfils the minimum requirements
        QList<VideoResolutionProperties> resProps = syncCap->getVideoResolution();

        for (auto j = 0; j < resProps.size(); ++j)
        {
            if (checkResolution(resProps[j].resolution))
            {
                lowResVideos.append(resProps[j]);
            }
        }

        QDateTime startTime = syncCap->getStartTime();
        internalVideoList.insert(insertionIndex + i, syncCap);

        // Update the external list
        insertElementInExternalVideoList(insertionIndex + i,
            startTime, views);
    }

    progress.setValue(progress.maximum());


    return 0;
}

int ruba::VideoHandler::addVideoStream(QString &homedir, bool clearExistingList, int insertionIndex)
{
	QList<QList<InitFileProperties> > multiViewFileList;

	// Find files for the first modality in the list
	QList<ViewInfo> views = getCurrentViews();

	if (views.empty())
	{
		return 1;
	}

	for (auto i = 0; i < views.size(); ++i)
	{
		ViewInfo currentView = views[i];
		QString filterText;
		QString streamUrl = QInputDialog::getText(this, tr("Open video stream"),
			tr("Enter the network URL for view %1:").arg(currentView.viewName()), QLineEdit::Normal, homedir);

		if (streamUrl.isEmpty())
		{
			return 1;
		}

		QSettings settings;
		settings.beginGroup("video");
		settings.setValue("streamLastPath", streamUrl);
		settings.endGroup();

		QList<InitFileProperties> singleViewFileList;

		// Create file properties from the stream url
		InitFileProperties properties;
		properties.videofile = streamUrl;
		properties.viewInfo = currentView;

		singleViewFileList.append(properties);
		multiViewFileList.append(singleViewFileList);
	}

	// Create new SynchonisedVideoCapture objects for each (multi-)view file and insert 
	// them in the internal and external video lists
	// The multiViewFileList is a QList<QList of InitFileProperties which holds:
	// n QList<InitFileProperties> for each view
	// each of these n QList<InitFileProperties> should hold m files
	// However, in order to initialise a SynchronisedVideoCapture, we should have
	// m of QList<InitFileProperties> which each holds n files. This restructuring will 
	// be performed when going through the double for loop

	if (clearExistingList)
	{
		internalVideoList.clear();
		videoListModel.clear();
		externalVideoListChanged = true;
	}

	if (multiViewFileList.empty())
		return 1;


	QDateTime prevDateTime;

	for (auto i = 0; i < multiViewFileList[0].size(); ++i)
	{
		QList<InitFileProperties> multiViewFileSet;

		for (auto j = 0; j < multiViewFileList.size(); ++j)
		{
			InitFileProperties prop = multiViewFileList[j][i];
			prop.prevDateTime = prevDateTime;

			multiViewFileSet.append(prop);
		}

		// Update the internal list
		auto syncCap = make_shared<SynchronisedVideoCaptureFromStream>(
			getParent(),
			views,
			multiViewFileSet);

		connect(syncCap.get(), SIGNAL(sendMessage(QString, QString, MessageType)), this, SIGNAL(sendMessage(QString, QString, MessageType)));
		connect(syncCap.get(), SIGNAL(sendProgress(int, int, QString)), this, SIGNAL(sendProgress(int, int, QString)));

		CaptureStatus status = syncCap->getCaptureStatus();

		if (status != CaptureStatus::OK)
		{
			insertionIndex--;
			continue;
		}

		// Get resolution and check if it fulfils the minimum requirements
		QList<VideoResolutionProperties> resProps = syncCap->getVideoResolution();

		for (auto j = 0; j < resProps.size(); ++j)
		{
			if (checkResolution(resProps[j].resolution))
			{
				lowResVideos.append(resProps[j]);
			}
		}

		QDateTime startTime = syncCap->getStartTime();
		internalVideoList.insert(insertionIndex + i, syncCap);

		// Update the external list
		insertElementInExternalVideoList(insertionIndex + i,
			startTime, views, tr("Streaming video"));

		// Update the previous date time
		prevDateTime = syncCap->getEndTime();
	}

	return 0;
}

int VideoHandler::getCorrespondingVideoFilesForView(
    QList<InitFileProperties> const &view1Files,
    ViewInfo const &correspondingView,
    QList<InitFileProperties> &correspondingFiles)
{
    // Try to fetch the corresponding video files automatically
    correspondingFiles.clear();

    for (auto i = 0; i < view1Files.size(); ++i)
    {
        QList<QString> correspondingFilenames;
        QList<ViewInfo> correspondingViews{correspondingView};

        int retVal = findSimilarFilenames(view1Files[i].videofile, view1Files[i].viewInfo,
            correspondingViews, correspondingFilenames);

        if (retVal == 0 && !correspondingFilenames.isEmpty())
        {
            InitFileProperties correspondingProperties;
            correspondingProperties.videofile = correspondingFilenames[0];
            correspondingProperties.viewInfo = correspondingView;

            if (correspondingFilenames.size() > 1)
            {
                correspondingProperties.prevFilename = correspondingFiles[i - 1].videofile;
            }

            correspondingFiles.append(correspondingProperties);

        }
        else {
            // We did not find a corresponding video file for this particular file,
            // which means that we cannot provide a full list of corresponding files.
            // Thus, return with an error code
            return 1;
        }
    }

    if (correspondingFiles.size() == view1Files.size())
    {
        return 0;
    }
    else {
        return 1;
    }

}

int VideoHandler::getCorrespondingVideoFilesForView(
    QList<InitFilePropertiesTimestamps> const &view1Files,
    ViewInfo const &correspondingView,
    QList<InitFilePropertiesTimestamps> &correspondingFilesTimestamps)
{
    // Try to fetch the corresponding video files automatically
    correspondingFilesTimestamps.clear();

    for (auto i = 0; i < view1Files.size(); ++i)
    {
        QList<QString> correspondingFilenames;
        QList<ViewInfo> correspondingViews{correspondingView};

        int retVal = findSimilarFilenames(view1Files[i].videofile, view1Files[i].viewInfo,
            correspondingViews, correspondingFilenames);

        if (retVal == 0 && !correspondingFilenames.isEmpty())
        {
            InitFilePropertiesTimestamps correspondingProperties;
            correspondingProperties.videofile = correspondingFilenames[0];
            correspondingProperties.viewInfo = correspondingView;
            
            QString timestamps;
            int retVal = findCorrespondingTimestampsFile(
                correspondingProperties.videofile,
                correspondingProperties.timestampsFile);
            
            if (retVal != 0 && !correspondingProperties.timestampsFile.isEmpty())
            {
                // The timestamps file could not be found automatically. 
                // Abort, and return failure
                return 1;
            }

            correspondingFilesTimestamps.append(correspondingProperties);
        }
        else {
            // We did not find a corresponding video file for this particular file,
            // which means that we cannot provide a full list of corresponding files.
            // Thus, return with an error code
            return 1;
        }
    }

    if (correspondingFilesTimestamps.size() == view1Files.size())
    {
        return 0;
    }
    else {
        return 1;
    }
}

int VideoHandler::insertElementInExternalVideoList(int insertionIndex, QDateTime startTime,
    QList<ViewInfo> views, QString alternativeText)
{
    // The external list is constructed as the following:
    // ------------------Video1-----------------------------
    // isPlayingIcon -- StartTime -- View1Icon -- View2Icon - View3Icon
    // ------------------Video2-----------------------------

    // Construct the new row    
    QList<QStandardItem*> newRow;

    // Update the startTime of the file
	if (alternativeText.isEmpty())
	{
		alternativeText = startTime.toString("dd-MM-yyyy HH:mm:ss");
	}

	QStandardItem* newItem = new QStandardItem(QIcon("://illustrations/emptyicon.png"),
		alternativeText);
	newRow.push_back(newItem);

    for (int i = 0; i < views.size(); ++i)
    {
        QString viewIconPath;
        
        // If the modality at enum i is found in the list, we distinguish between the current 
        // input modality and all other modalities
        QString viewName = Utils::GetModalityName(views[i].modality);
        viewIconPath.append("://icons/" + viewName);
        viewIconPath.append("-icon-small.png");

        QStandardItem* newItem = new QStandardItem(QIcon(viewIconPath), "");
        newRow.push_back(newItem);
    }

    videoListModel.insertRow(insertionIndex, newRow);
	externalVideoListChanged = true;

    return 0;
}

//! Private: Tries to find similar files to filename in the directory of filename, and in adjacent modality folders. Only finds files according to enabled modalities
/*!
\param filename file for which a corresponding file in other (enabled) modalities should be found
\param currentView the view of filename
\param correspondingViews list of views for which corresponding files should be found
\param correspondingFilenames returns list of corresponding (similar) files in the views specified in correspondingViews
\return returns 0 if corresponding files were found, 1 otherwise
*/
int VideoHandler::findSimilarFilenames(const QString& filename, const ViewInfo currentView,
    QList<ViewInfo>& correspondingViews, QList<QString>& correspondingFilenames)
{
    
    // Find the base name of the filename 
    QFileInfo filePath(filename);
    QString baseName = filePath.baseName();
    baseName.remove(QRegExp("cam\\d"));
    baseName.remove(QRegExp("[a-zA-Z]")); // Remove remaining characters

    for (auto i = 0; i < correspondingViews.size(); ++i)
    {
        // Check if the supplied corresponding views are different
        if (correspondingViews[i].compare(currentView) != 0)
        {
            // Search for similar files
            // Update the search string in future versions of RUBA
            QString fileSuffix = Utils::GetModalityFileSuffix(correspondingViews[i].modality);
            QString nameFilter = "*" + baseName + fileSuffix + "*";
            QDir dir(filePath.absolutePath(), nameFilter);

            // Try to search in different directories
            QList<QString> relativeDirectoryList;
            relativeDirectoryList.append(""); // Current directory
            relativeDirectoryList.append("../" + Utils::GetModalityFileFolder(correspondingViews[i].modality)); // Specific modality folder

            bool modalityFound = false;
            int j = 0;

            while (!modalityFound && (j < relativeDirectoryList.size()))
            {
                dir.setPath(filePath.absolutePath());
                dir.cd(relativeDirectoryList[j]);
                QString path = dir.absolutePath();

                if (dir.entryList().count() == 1)
                {
                    // If there is exactly one entry with this string, we have found a similar file
                    // and hopefully the right one
                    QFileInfo similarFile(dir, dir.entryList()[0]);

                    correspondingFilenames.append(similarFile.absoluteFilePath());
                    modalityFound = true;
                }

                ++j;
            }

        }
    }

    if (correspondingViews.size() == (correspondingFilenames.size()))
    {
        return 0;
    }
    else {
        return 1;
    }
}

//! Private: Find the accompanying file of time stamps for video file filename
/*!
\param filename file name of the video for which corresponding time stamps should be found
\param timestampsFile file name of the corresponding list of timestamps
\return if 0, a corresponding timestampsFile was found, > 0 otherwise
*/
int VideoHandler::findCorrespondingTimestampsFile(const QString & filename,
    QString &timestampsFile)
{
    QFileInfo fileInfo(filename);
    QString baseName = fileInfo.path() + "/" +fileInfo.completeBaseName();

    // Get the current VideoSyncType
    QSettings settings;
    settings.beginGroup("synchronisation");
    QString extension = settings.value("logFileExtension", ".log").toString();
    settings.endGroup();

    QString logName = baseName + extension;
    
    QFileInfo checkFile(logName);

    if (checkFile.exists() && checkFile.isFile())
    {
        // The log file exists! Return the proper file name
        timestampsFile = logName;
        return 0;
    }

    return 1;
}

//! Private: Get video file types that might be supported by ffmpeg 
/*!
Get video file types that might be supported by ffmpeg and returns a list of strings
compatible with the filter string in QFileDialog

\param filterText returned filter text of supported file types for QFileDialog
\return always 0
*/
int VideoHandler::getSupportedVideoFileTypesText(QString &filterText)
{
    QList<QString> supportedFileTypes{ "*.avi","*.mp4", "*.mpg","*.mpeg","*.mkv","*.asf","*.MTS",
        "*.flv","*.h264", "*.mjpeg","*.wmv", "*.divx", "*.mov" };

    filterText = "Video Files (";

    for (int i = 0; i < supportedFileTypes.size(); ++i)
    {
        filterText.append(supportedFileTypes[i] + " ");
    }

    filterText.append(")\n");

    for (int i = 0; i < supportedFileTypes.size(); ++i)
    {
        filterText.append(supportedFileTypes[i] + "\n");
    }

    return 0;
}

std::vector<FrameStruct> VideoHandler::getCurrentFrame()
{
    CaptureStatus status;

    if (currentVideoListPos < internalVideoList.size())
    {
        std::vector<FrameStruct> frames = internalVideoList[currentVideoListPos]->getCurrentFrames(status);

        switch (status)
        {
        case CaptureStatus::OK:
        {
            // We have successfully read the frame.
            break;
        }
        case CaptureStatus::NOMOREFRAMES:
        {
            // There are no more frames in the current video. Fall through to default handling
        }
        default:
        {
            // Open the next video on the list if the status is not treated above

            if (prevStatus != status)
            {
                emit sendMessage(tr("Video not available"),
                    tr("The selected video is not available or does not have any more frames to read. "
                        "Select another video in the list and retry"), 
					MessageType::Information);
            }
        }
        }

        checkFrames(frames, status);

        prevStatus = status;
        return frames;
    }
    else {
        // No video selected, or none in the video list. Return empty 
        prevStatus = status;
        return std::vector<ruba::FrameStruct>();
    }
}

std::vector<FrameStruct> VideoHandler::getNextFrame()
{
    CaptureStatus status;
    std::vector<FrameStruct> frames;


    if (currentVideoListPos >= 0)
    {
        frames = internalVideoList[currentVideoListPos]->getNextFrames(status);

        bool openNextFile = false;

		if (videoListModel.item(currentVideoListPos, 0)->font().bold())
		{
			// Change the appearance to default
			videoListModel.item(currentVideoListPos, 0)->setForeground(QBrush(QColor("black")));
			auto font = videoListModel.item(currentVideoListPos, 0)->font();
			font.setBold(false);
			videoListModel.item(currentVideoListPos, 0)->setFont(font);
			videoListModel.item(currentVideoListPos, 0)->setIcon(QIcon("://icons/emptyicon.png"));

		}

        switch (status)
        {
        case CaptureStatus::OK:
        {
            // We have successfully read the frame. Return the list
            checkFrames(frames, status);
            prevStatus = status;
            return frames;
        }
		case CaptureStatus::VIDEONOTFOUND:
		{
			// The specified video was not found. Don't open the next file on the list automatically
			if (persistentPlayback)
			{
				openNextFile = true;
			}
			else {
				openNextFile = false;
			}

			videoListModel.item(currentVideoListPos, 0)->setForeground(QBrush(QColor("red")));
			auto font = videoListModel.item(currentVideoListPos, 0)->font();
			font.setBold(true);
			videoListModel.item(currentVideoListPos, 0)->setFont(font);
			videoListModel.item(currentVideoListPos, 0)->setIcon(QIcon("://icons/exclamation-circle.png"));

			break;
		}
        case CaptureStatus::NOMOREFRAMES:
        {
            // There are no more frames in the current video. Open the next on the list
            openNextFile = true;
            break;
        }
        case CaptureStatus::VIDEOISRELEASED:
        {
            // The video has been released by an event loop, and we should just skip 
            // the current frames
            openNextFile = false;
            return getCurrentFrame();
        }
        default:
            // Open the next video on the list if the status is not treated above
            openNextFile = true;
        }


        if (openNextFile && (currentVideoListPos + 1 < internalVideoList.size()))
        {
            setCurrentVideo(currentVideoListPos + 1);
        }
    }


    prevStatus = status;
    // Return the "current" frame if the have opened a new video file
    return getCurrentFrame();
}

void ruba::VideoHandler::checkFrames(std::vector<ruba::FrameStruct>& frames, CaptureStatus& status)
{
    // Check if the frames are consistent with the enabled views

    if (!viewErrorShown)
    {
        QString errorMessage;

        QList<ViewInfo> currentViews = getCurrentViews();

        for (auto i = 0; i < currentViews.size(); ++i)
        {
            if (i >= frames.size())
            {
                errorMessage += tr("No video files specified for view %1.\n")
                    .arg(currentViews[i].viewName());
            }

            if (i < frames.size() && currentViews[i].compare(frames[i].viewInfo) != 0)
            {
                errorMessage += tr("Expected a frame for view %1, but received a frame of view %2.\n")
                    .arg(currentViews[i].viewName())
                    .arg(frames[i].viewInfo.viewName());
            }
        }

        if (!errorMessage.isEmpty())
        {
            QString currentViewsText = tr("Current views:\n");
            QString videoViewsText = tr("Views of the selected video:\n");

            for (auto i = 0; i < currentViews.size(); ++i)
            {
                currentViewsText += tr("View %1\n").arg(currentViews[i].viewName());
            }

            for (auto i = 0; i < frames.size(); ++i)
            {
                videoViewsText += tr("View %1\n")
                    .arg(frames[i].viewInfo.viewName());
            }

            emit sendMessage(tr("Views do not match"),
                tr("The current views and the views of the selected video do not match.\n\n"
                    "You can resolve this problem by: \n"
                    "a) Reconfigure the current views so that they match with the views of the video, or.\n"
                    "b) Delete the video from the list and add another video.\n\n") +
                currentViewsText + "\n" + videoViewsText +
                tr("\nDetails about this error:\n") + errorMessage,
                MessageType::Warning);

            viewErrorShown = true;
        }
    } 
}

QList<VideoResolutionProperties> ruba::VideoHandler::getVideoResolution() const
{
    if (currentVideoListPos < internalVideoList.size())
    {
        return internalVideoList[currentVideoListPos]->getVideoResolution();
    } 
    else
    {
        return QList<VideoResolutionProperties>();
    }
}

//! Public: Removes the modality file at the index supplied.
/*!
\param indices 0-based indices in the external and internal list where the entry should be removed at the specified modality
\return return code
*/
void VideoHandler::removeVideoFiles(QList<int> indices)
{
    // Get the current file that is playing. Figure out if it is to be removed
    bool isPlayingDeletedFile = indices.contains(currentVideoListPos);

    if (isPlayingDeletedFile && currentVideoListPos < internalVideoList.size())
    {
        internalVideoList[currentVideoListPos]->releaseVideo();
    }

    // Remove the files from the video list
    // Go through the list in reverse order to preserve the indices in the list
    for (int i = indices.size() - 1; i >= 0; --i)
    {
        // Remove the entire entry
        internalVideoList.removeAt(indices[i]);

        // Remove the entry from the videoList
        videoListModel.takeRow(indices[i]);
		externalVideoListChanged = true;

        // If the index is lesser than the currentVideoListPos, decrement it
        // If it is equal to, the VideoHandler will reset the current video
        if (indices[i] < currentVideoListPos)
        {
            currentVideoListPos--;
        }
    }

    if (isPlayingDeletedFile)
    {
        // Try to open the first file on the list
        setCurrentVideo(0);
    }

    return;
}

//! Move the videos at position pos to position newPos in the internal and external video lists.
/*!
Adjusts the index of the current video according to the move
\param pos current position of the videos that should be moved
\param newPos where the videos at pos should be moved
*/
void VideoHandler::moveItem(int pos, int newPos)
{
    if (newPos >= 0 && pos >= 0 && pos != newPos && newPos < internalVideoList.size())
    {
        auto prop = std::move(internalVideoList.takeAt(pos));
        internalVideoList.insert(newPos, std::move(prop));

        QList<QStandardItem*> row = videoListModel.takeRow(pos);
        videoListModel.insertRow(newPos, row);
		externalVideoListChanged = true;

        // adjust the currentVideoListPos
        if (newPos > currentVideoListPos && pos > currentVideoListPos)
        {
            return;
        }

        if (newPos < currentVideoListPos && pos < currentVideoListPos)
        {
            return;
        }

        if (newPos < currentVideoListPos && pos != currentVideoListPos)
        {
            currentVideoListPos++;
            return;
        }

        if (newPos > currentVideoListPos && pos != currentVideoListPos)
        {
            currentVideoListPos--;
            return;
        }

        if (pos == currentVideoListPos)
        {
            // we have moved the current video.
            currentVideoListPos = newPos;
            return;
        }

        if (newPos == currentVideoListPos)
        {
            currentVideoListPos++;
        }
    }
}

void VideoHandler::showVideoInformation(int index)
{
    if (index < internalVideoList.size())
    {
        internalVideoList[index]->showVideoProperties();
    }    
}


void VideoHandler::play()
{
    setPlayingIcon(currentVideoListPos);
}

void VideoHandler::pause()
{
    setPausedIcon(currentVideoListPos);
}

//! Set the 'playing' icon at position index in the external video list
/*!
\param index of the paused icon
\return 0 if the index was found in the list, 1 otherwise
\sa setPausedIcon()
*/
int VideoHandler::setPlayingIcon(int index)
{
    if (index >= 0 && index < videoListModel.rowCount())
    {
        if (currentVideoListPos != index && (currentVideoListPos >= 0))
        {
			if (prevStatus != CaptureStatus::VIDEONOTFOUND)
			{
				// Update the video list for the previously played video - display empty icon as this is not played anymore
				videoListModel.item(currentVideoListPos, 0)->setIcon(QIcon("://illustrations/emptyicon.png"));
			}
        }

        // Update the new index
        currentVideoListPos = index;
        videoListModel.item(currentVideoListPos, 0)->setIcon(QIcon("://icons/control-000-small.png"));
		externalVideoListChanged = true;
    }
    else {
        return 1;
    }

    return 0;
}

//! Set the 'paused' icon at position index in the external video list
/*!
\param index position of the paused icon
\return 0 if the index was found in the list, 1 otherwise
\sa setPlayingIcon()
*/
int VideoHandler::setPausedIcon(int index)
{
	if (index >= 0 && index < videoListModel.rowCount())
	{
		if (currentVideoListPos != index && (currentVideoListPos >= 0) &&
			(videoListModel.rowCount() > currentVideoListPos))
		{
			if (prevStatus != CaptureStatus::VIDEONOTFOUND)
			{
				// Update the video list for the previously played video - display empty icon as this is not played anymore
				videoListModel.item(currentVideoListPos, 0)->setIcon(QIcon("://illustrations/emptyicon.png"));
			}
		}

		// Update the new index
		currentVideoListPos = index;
		videoListModel.item(currentVideoListPos)->setIcon(QIcon("://icons/control-pause-small.png"));
		externalVideoListChanged = true;

		return 0;
	}
	else {
		return 1;
	}
	

	return 0;
}

int VideoHandler::setCurrentVideo(int index)
{
    viewErrorShown = false;
    int oldVideo = currentVideoListPos;
    
    int retVal = setPausedIcon(index);

    if (retVal != 0)
    {
        // If the video list returned non-zero, the index is out of range. Return
        return 1;
    }

    // Release the old videos
    if (oldVideo < internalVideoList.size())
    {
        internalVideoList[oldVideo]->releaseVideo();
    }

	// Reset the statuses
	prevStatus = CaptureStatus::OK;

    // Open the new videos
    getNextFrame();

    return 0;
}

qint64 VideoHandler::getFrameNbr() const
{
    if (currentVideoListPos >= 0 && currentVideoListPos < internalVideoList.size())
    {
        return internalVideoList[currentVideoListPos]->getFrameNumber();
    }
    else {
        return -1;
    }
}

QDateTime ruba::VideoHandler::getCurrentTime()
{
	if (currentVideoListPos < internalVideoList.size())
	{
		return internalVideoList[currentVideoListPos]->getCurrentTime();
	}
	else {
		return QDateTime();
	}
}

double ruba::VideoHandler::getFrameRate() const
{
    if (currentVideoListPos < internalVideoList.size())
    {
        return internalVideoList[currentVideoListPos]->getFrameRate();
    } 
    else {
        return 0.0;
    }
}

VideoSequenceWriter::VideoSequenceWriter(const QList<ViewInfo>& views, const QString& saveDirectory, 
	double fps, bool createOneVideoPerSequence, int maxConsecutiveTimeDifferenceInMillisec)
{
	this->views = views;
	this->saveDirectory = saveDirectory;
	this->createOneVideoPerSequence = createOneVideoPerSequence;
	this->maxConsecutiveTimeDifferenceInMillisec = maxConsecutiveTimeDifferenceInMillisec;

	vWriters.resize(views.size());

	recording = false;
	this->fps = fps;
	
	for (auto& view : views)
	{
		sequenceFrameCount.append(0);
	}
}

void VideoSequenceWriter::startRecording()
{
	recording = true;
}

void VideoSequenceWriter::pauseRecording()
{
	recording = false;
}


void VideoSequenceWriter::stopRecording()
{
	recording = false;
	for (auto& vWriter : vWriters)
	{
		vWriter.release();
	}
}

bool VideoSequenceWriter::update(const std::vector<ruba::FrameStruct>& frames, QString& message)
{
	if (recording)
	{
		if (frames.size() == views.size() && !frames.empty())
		{
			QDateTime currentTime = frames[0].Timestamp;

			if ((previousTimestamp.isValid() &&
				(previousTimestamp.msecsTo(currentTime) > maxConsecutiveTimeDifferenceInMillisec)) ||
				!previousTimestamp.isValid())
			{
				qDebug() << "Current: " << currentTime.toString("yyyy-MM-dd-HH-mm-ss.zzz") << " Previous: " << previousTimestamp.toString("yyyy-MM-dd-HH-mm-ss.zzz");
				previousTimestamp = currentTime;

				// We should close the existing video and start a new file
				QString baseName = saveDirectory + '/' + currentTime.toString("yyyy-MM-dd-HH-mm-ss.zzz") + "-";

				for (auto i = 0; i < frames.size(); ++i)
				{
					if (vWriters[i].isOpened() && createOneVideoPerSequence)
					{
						vWriters[i].release();
					}

					if (!vWriters[i].isOpened() || createOneVideoPerSequence)
					{
						QString viewFileName = baseName + QObject::tr("View%1").arg(frames[i].viewInfo.viewId) + ".mp4";

						// Reset video start and frame counters
						videoStart = currentTime;

						for (auto& frameCount : sequenceFrameCount)
						{
							frameCount = -1;
						}

						bool success = vWriters[i].open(viewFileName.toStdString(), VideoWriter::fourcc('H', '2', '6', '4'),
							fps, frames[i].Image.size());

						if (!success)
						{
							// Fall back on motion JPEG
							viewFileName = baseName + QObject::tr("View%1").arg(frames[i].viewInfo.viewId) + ".mpg";

							vWriters[i].open(viewFileName.toStdString(), VideoWriter::fourcc('M', 'J', 'P', 'G'),
								fps, frames[i].Image.size());
						}
					}
				}
			}
			else {
				previousTimestamp = currentTime;
			}

			qint64 totalDurationSecs = videoStart.secsTo(currentTime);
			int displayHours = totalDurationSecs / 3600;
			int displayMins = (totalDurationSecs % 3600) / 60;
			int displaySecs = (totalDurationSecs - (displayHours * 3600) - (displayMins * 60));
			
			message = QObject::tr("Recording video (%1:%2:%3)")
				.arg(displayHours, 2, 10, QChar('0'))
				.arg(displayMins, 2, 10, QChar('0'))
				.arg(displaySecs, 2, 10, QChar('0'));


			for (auto i = 0; i < frames.size(); ++i)
			{
				if (vWriters[i].isOpened())
				{
					if (sequenceFrameCount.size() <= i)
					{
						break;
					}

					QDateTime theoreticalCurrentFrameTime = videoStart.addMSecs(sequenceFrameCount[i] * 1000. / fps);
					int theoreticalTimeBetweenFrames = int(1000. / fps);

					int writtenFrameCount = 0;

					// If the current frame is needed as reflected in the theoretical time, write one or several 
					// frames to disk. If not, just skip the current frame
					while (theoreticalCurrentFrameTime.msecsTo(frames[i].Timestamp) >= theoreticalTimeBetweenFrames)
					{
						vWriters[i].write(frames[i].Image);

						// Calculate the "theoretical" frame time based on the start time and 
						// the originally supplied frame rate
						sequenceFrameCount[i] += 1;
						theoreticalCurrentFrameTime = videoStart.addMSecs(sequenceFrameCount[i] * 1000. / fps);

						writtenFrameCount++;
					}

				}
				else {
					return false;
				}
			}
		}
	}

	return false;
}
