#include "exclusivemodule.h"


using namespace std;
using namespace cv;

using namespace ruba;


ExclusiveModule::ExclusiveModule(BaseDetectorParams baseParams, QWidget *parent, QString configFile)
	: AbstractCompoundDetector(configureBaseDetectorParams(baseParams, EXCLUSIVEMODULE))
{
	// Find the configuration of the single module
	bool showGui = configFile.isEmpty();


	DetectorTypeSelectionDialog *tSDialog = new DetectorTypeSelectionDialog(parent, 2, true);
	tSDialog->setWindowTitle("Exclusive Module Detector");

	// If we have input a configFile to the SingleModule, do not show the DetectorTypeSelectionDialog.
	// Instead, prompt the dialog to determine which detector type should populate the SingleModule
	// according to the configFile
	bool goodConfig = false;

	if (showGui)
	{
		goodConfig = tSDialog->exec();
	}
	else {
		int res = tSDialog->LoadConfiguration(configFile);

		if (res == 0)
		{
			goodConfig = true;
		}
	}

	if (goodConfig)
	{
		chosenDetectors = tSDialog->getOutput();

		if (chosenDetectors.size() > 1)
		{
			std::vector<DType> detectors;

			for (int i = 0; i < chosenDetectors.size(); ++i)
			{
				detectors.push_back(DType(chosenDetectors[i]));
			}

			setDetectorCount(detectors);
		}
		else {
			IsInitialized = false;
		}

		configFile = tSDialog->getConfigurationFilename();
		delete tSDialog;

	}
	else {
		IsInitialized = false;
	}

	windowIcon = new QIcon("://icons/layer--exclamation.png");

	if (IsInitialized)
	{
		LoadConfiguration(baseParams.initFrames, parent, configFile, showGui);
	}
}

DetectorOutput ExclusiveModule::GetOutput()
{
	DetectorOutput outputStruct;

	if (this->IsInitialized)
	{
		outputStruct.Confidence.push_back(1);
	}
	else {
		outputStruct.Confidence.push_back(0);
	}

	outputStruct.DetectorID = getDetectorID();
	outputStruct.DetectorType = getDetectorType();
	outputStruct.IsTriggered = IsTriggered;

	if (generalLog != nullptr)
	{
		if (generalLog->getLastEnteringTime() > generalLog->getLastLeavingTime())
		{
			outputStruct.LastChange.push_back(generalLog->getLastEnteringTime());
		}
		else {
			outputStruct.LastChange.push_back(generalLog->getLastLeavingTime());
		}
	}

	outputStruct.Timestamp = getCurrentTime();

	return outputStruct;
}

void ExclusiveModule::setDetectorSpecificDefaultParameters()
{
	InitLogSettings generalSettings = getGeneralLogSettings();

	for (size_t i = 0; i < detectorParams.size(); ++i)
	{
		// Set common parameters
		detectorParams[i].DisableDetectorDuration = -1; // Time in ms. Deactivated pr. default
		detectorParams[i].DisableDetectorAfter = -1;
		generalSettings.entrySettings.maxTriggeredDuration[detectorParams[i].DetectorIdText] = 5000;
		detectorParams[i].views = getCurrentViews();
		detectorParams[i].EnableWrongDirection = false;

		if (i != 0)
		{
			// The activation of all other detectors than the first should deactivate the first
			// for 2000 ms. The exact amount is to be customized by the user
			detectorParams[i].DisableDetectorDuration = 2000;
		}
	}

	setGeneralLogSettings(generalSettings);

	disableAllDetectorsAt = getCurrentTime().addYears(100);

	detectorInfo.clear();

	// Set flags of the SingleDetector
	IsTriggered.resize(1);
	IsTriggered[0] = false;
}

void ExclusiveModule::updateDetectorSpecificLogic()
{
	/****** Update the internal logic of the ExclusiveModule detector:  ******/
	QDateTime currentTime = getCurrentTime();
	
	for (auto &detectorInf : detectorInfo)
	{
		detectorInf.IsEnabled = true;
	}

	for (auto i = 1; i < detectorInfo.size(); ++i)
	{
		if (detectorInfo[i].IsTriggered)
		{
			detectorInfo[0].DisableDetectorUntil = currentTime.addMSecs(detectorInfo[i].DisableDetectorDuration);
			break;
		}
	}

	for (size_t i = 0; i < detectorInfo.size(); ++i)
	{
		if (detectorInfo[i].DisableDetectorUntil > currentTime)
		{
			detectorInfo[i].IsEnabled = false;
			detectorInfo[i].IsTriggered = false;
		}
	}


	if (detectorInfo[0].IsEnabled && detectorInfo[0].IsTriggered)
	{
		this->IsTriggered[0] = true;
	}
	else {
		this->IsTriggered[0] = false;
	}

	if ((detectorInfo[0].IsTriggered == true) && (getPreviousDetectorInfo()[0].IsTriggered == false))
	{
		// Check if the state has changed            
		generalLog->updateLog(getCurrentTime(), frames, Transition::ENTERING,
			Utils::getDetectorAbbreviation(getDetectorType()),
			maskOutlines[detectorInfo[0].getDetectorId()]);

	}
	else if ((detectorInfo[0].IsTriggered == false) && (getPreviousDetectorInfo()[0].IsTriggered == true))
	{
		// Check if the state has changed        
		// If it has, an object has just left the conflict zone
		generalLog->updateLog(getCurrentTime(), frames, Transition::LEAVING,
			Utils::getDetectorAbbreviation(getDetectorType()),
			maskOutlines[detectorInfo[0].getDetectorId()]);
	}
}

QList<QString> ExclusiveModule::getDetectorSpecificExtendedDebugInfo()
{
	QList<QString> extDebugList;
	return extDebugList;
}