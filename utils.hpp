#ifndef TRAFFICUTILS_H
#define TRAFFICUTILS_H

#include <QList>
#include <QString>
#include <QModelIndex> 
#include <QMessageBox>

#include <opencv2/opencv.hpp>
#include "ruba.h"


class Utils
{
public:
    static QString GetModalityName(int modality);

    static QString getTLStateName(tlTypeClasses type);

    static QList<QString> GetModalityAbbreviation(int modality);

    static QString getDetectorName(int detectorType, bool addDetectorPostfix = true);
    static QString getDetectorAbbreviation(int detectorType);

    static void showWarning(QWidget* parent, QString title, QString errorMessage);

    static void getWarningMessage(QWidget* parent, cv::Exception e, QString& title, QString& message);

    static int getTotalModalityCount();

    static QString getOrdinalSuffix(const int number);

    static QString GetModalityFileSuffix(int modality);
    static QString GetModalityFileFolder(int modality);

    static QList<QString> readQStringListFromFileStorage(const cv::FileStorage& fs, std::string node);

    static QModelIndex getAncestor(const QModelIndex index);

    static bool checkIsModalityValid(ImageModalities modality);

    static cv::Size getResolution(QString filename);
};

#endif // TRAFFICUTILS_H
