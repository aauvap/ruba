#ifndef DETECTORINFO_H
#define DETECTORINFO_H

#include <QDateTime>
#include <QString>
#include <opencv2/opencv.hpp>
#include "ruba.h"

class ruba::DetectorInfo{

public:
    DetectorInfo(int detectorType, int detectorId,
        QString detectorIdText)
    {
        this->DetectorType = detectorType;
        this->DetectorId = detectorId;
        this->DetectorIdText = detectorIdText;
    }

    int getDetectorType() const { return DetectorType; }
    int getDetectorId() const { return DetectorId; }
    QString getDetectorIdText() const { return DetectorIdText; }

    bool IsEnabled;
    bool IsTriggered;
    bool IsWrongDirectionTriggered;
    QDateTime DisableDetectorUntil;
    QDateTime DisableDetectorAt;
    qint64 DisableDetectorDuration;
    qint64 DisableDetectorAfter;


private:
    int DetectorType;
    int DetectorId;
    QString DetectorIdText;
};


#endif // DETECTORINFO_H

