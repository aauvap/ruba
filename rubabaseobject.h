#ifndef RUBABASEOBJECT_H
#define RUBABASEOBJECT_H

#include <opencv2/opencv.hpp>
#include <vector>

#include <QProgressDialog>
#include <QSplashScreen>
#include <QWidget>

#include "ruba.h"
#include "viewinfo.h"

enum MessageType
{
	Information,
	Warning,
	Critical,
	Question,
	MessageBar
};

Q_DECLARE_METATYPE(MessageType)


class ruba::RubaBaseObject : public QWidget
{
	Q_OBJECT;

public:
	RubaBaseObject(QWidget* parent, QList<ViewInfo> views);
	RubaBaseObject(QWidget* parent, cv::FileStorage& fs,
		const std::string& entryPrefix);

	virtual int saveToFile(cv::FileStorage& fs, const std::string& entryPrefix = "") = 0;

	int addView(ViewInfo newView);
	int removeView(ViewInfo view);
	int setViews(QList<ViewInfo> newViews);

	QList<ViewInfo> getCurrentViews() const { return views; }
	QWidget* getParent() const { return parent; }

	int findViewInList(const ViewInfo& view);
	static int findViewInList(const ViewInfo& view, const QList<ViewInfo>& viewList);

	int saveViewsToFile(cv::FileStorage & fs, const std::string& entryPrefix);
	static int saveViewsToFile(cv::FileStorage & fs, const std::string& entryPrefix,
		const QList<ViewInfo>& views);

	static QList<ViewInfo> getViewsFromFileStorage(cv::FileStorage& fs, const std::string& entryPrefix);

protected:
	virtual int viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews) = 0;




private:
	QList<ViewInfo> views;
	QWidget* parent;



};

#endif // RUBABASEOBJECT_H