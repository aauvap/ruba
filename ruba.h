#ifndef RUBA_H
#define RUBA_H

namespace ruba {
  class PresenceDetector;
  class PresenceDetectorSingleModal;
  class MovementDetector;
  class MovementDetectorSingleModal;
  class RubaBaseObject;
  class StationaryDetector;
  class TrafficLightDetector;
  class TrafficLightDetectorSingleModal;
  class VideoHandler;
  class VideoList;
  class AbstractSynchronisedVideoCapture;
  class SynchronisedVideoCaptureFromFile;
  class SynchronisedVideoCaptureFromFileTimestamps;
  class SynchronisedVideoCaptureFromStream;
  class DetectorHandler;
  class AbstractTrafficDetector;
  class AbstractCompoundDetector;
  class AbstractSingleModalDetector;
  class AbstractMultiModalDetector;
  class RightTurningCar;
  class LeftTurningCar;
  class StraightGoingCyclist;
  class InteractionDetector;
  class SingleModule;
  class DoubleModule;
  class ExclusiveModule;
  class OldLogger;
  class Logger;
  class GroundTruthAnnotator;
  class LogFileReviewer;
  struct FrameStruct;
  struct DetectorOutput;
  class DetectorInfo;
  struct LogInfoStruct;
  struct MaskHandlerOutputStruct;
  struct TrafficDetectorOutputStruct;
  struct ChartStruct;
  struct MaskOverlayStruct;
  struct MaskOutlineStruct;
  struct LogSettingsStruct;
  struct AdditionalSettingsStruct;
  struct DebugPrefStruct;
}

enum DType{PRESENCEDETECTOR, MOVEMENTDETECTOR, STATIONARYDETECTOR, TRAFFICLIGHTDETECTOR, DETECTORE,
           RIGHTTURNINGCAR, STRAIGHTGOINGCYCLIST, LEFTTURNINGCAR, INTERACTIONDETECTOR,
    INTERACTION410DETECTOR, SINGLEMODULE, DOUBLEMODULE, GROUNDTRUTHANNOTATOR, LOGFILEREVIEWER,
	EXCLUSIVEMODULE};

enum ImageModalities {RGB = 0, THERMAL = 1, IMAGEMODALITIES_NR_ITEMS = 2};

enum VideoSyncType {STARTTIMEINFILENAME = 0, TIMESTAMPSINLOGFILE = 1, CAPTUREFROMSTREAM = 3};


enum tlTypeClasses{ RED, YELLOW, GREEN, REDYELLOW, AMBIGIOUS };

enum format{MM_dd_HH, yyyy_MM_DD, yyyyMMdd_HH, yyyy_MM_dd_HH, yyyyMMDD_HH_mm_ss, yyyy_MM_dd_HH_ss_zzz, ddMMyyyy, USERDEFINED, USERDEFINED_USE_END_TIME_AS_START_TIME};

struct FamilyInfo {
    int detectorID;
    int detectorType;
};

#endif // RUBA_H
