#include "trafficlightdetectorsinglemodal.h"
#include <QDebug>

using namespace cv;
using namespace std;
using namespace ruba;


TrafficLightDetectorSingleModal::TrafficLightDetectorSingleModal(BaseDetectorParams baseParams, cv::Mat maskimage, bool UseScrollingChart, std::vector<cv::Point> tlPlacements, 
                                                                int tlTypeTrigger)
    : AbstractSingleModalDetector(configureBaseDetectorParams(baseParams, TRAFFICLIGHTDETECTOR))
{
    this->tlTypeTrigger = tlTypeTrigger;
    //tlTypeTrigger = tlTypeTrigger;
    SetMask(maskimage);
    IS_FIRST_RUN = true;
    /*SetBackground(backimage);
    
    ScrollingChart *Chart = new ScrollingChart(300, 300, modality);
    Chart->SetThreshold(TriggerThreshold);
    Chart->SetTitle("TrafficLightDetector");
    Chart->SetVisualLineThresholds(int(TriggerThreshold), int(TriggerThreshold*1.2));
    Charts.push_back(Chart); 
	InitDetector(); */
    localTlPlacements = tlPlacements;
    
}

void TrafficLightDetectorSingleModal::InitDetector()
{
    
	cv::Mat tempmaskimage;
	mask.copyTo(tempmaskimage);
    
	std::vector<std::vector<cv::Point>> contours;
	cv::findContours(tempmaskimage, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE);

	cv::Rect BB = cv::boundingRect(contours.at(0));

	ROI = BB;
    //imshow("Zhe Masasdk", mask);
	tempmaskimage.release();
    
}

void TrafficLightDetectorSingleModal::SetBackground(cv::Mat backImage)
{
	cv::Mat roiBackImage = backImage(ROI);
	/*cv::Mat tempCannyImage(roiBackImage.rows, roiBackImage.cols, roiBackImage.type());
	cv::Canny(roiBackImage, tempCannyImage, CannyBigThres, CannyLowThres);

	cv::bitwise_and(tempCannyImage, mask, tempCannyImage);

	tempCannyImage.copyTo(this->CannyBackImage);

	roiBackImage.copyTo(this->PrevImage);

	roiBackImage.release();
	tempCannyImage.release(); */
	backImage.release();

}

DetectorOutput TrafficLightDetectorSingleModal::GetOutput()
{
	DetectorOutput outputStruct;

	outputStruct.Confidence = this->confidence;
	outputStruct.DetectorID = getDetectorID();
	outputStruct.DetectorType = TRAFFICLIGHTDETECTOR;
	outputStruct.IsTriggered = this->IsTriggered;
	outputStruct.LastChange = this->lastChange;
	outputStruct.Timestamp = getCurrentTime();
    outputStruct.tlDetectionClass = whatClassValPosition;
    outputStruct.valAtTlIndices.push_back(valAtRed);
    outputStruct.valAtTlIndices.push_back(valAtYellow);
    outputStruct.valAtTlIndices.push_back(valAtGreen);

	return outputStruct;
}


QList<QString> TrafficLightDetectorSingleModal::getDetectorSpecificDebugInfo()
{
	return this->debugInfo;
}

void TrafficLightDetectorSingleModal::updateDetectorSpecifics()
{    
	if (IS_FIRST_RUN) {
		// Extract contours for the masks
		extractContours();
		IS_FIRST_RUN = false;
		initTriggeredHistory(1);
	}
    
	extractROI();

    if (prev.empty())
    {
        initStabilisation();
    }

    stabiliseFrame();
    extractColors();

    if (whatClassValPosition == tlTypeTrigger)
    {
        //if (IsTriggered[0] == false) { lastChange.clear(); lastChange[0] = frame.Timestamp; }
        IsTriggered.clear();
        IsTriggered.push_back(true); 
    }
    else
    {
        IsTriggered.clear();
        IsTriggered.push_back(false);
    }
    
    
}


void TrafficLightDetectorSingleModal::initStabilisation() {
    k = 1;
    prev = roiColor.clone(); //get the first frame.ch
    cvtColor(prev, prev_grey, cv::COLOR_BGR2GRAY);
    if (HORIZONTAL_BORDER_CROP>0) vert_border = HORIZONTAL_BORDER_CROP * prev.rows / prev.cols;
    else vert_border = 0;
    Q = Trajectory(pstd, pstd, pstd);
    R = Trajectory(cstd, cstd, cstd);
    T.create(2, 3, CV_64F);

}

void TrafficLightDetectorSingleModal::stabiliseFrame() {
    cur = roiColor.clone();
    cvtColor(cur, cur_grey, cv::COLOR_BGR2GRAY);
    cv::goodFeaturesToTrack(prev_grey, prev_corner, 200, 0.005, 1.5);

    if (prev_corner.size()>0)
    {
        cv::calcOpticalFlowPyrLK(prev_grey, cur_grey, prev_corner, cur_corner, status, err);

        // weed out bad matches
        prev_corner2.clear();
        cur_corner2.clear();
        for (size_t i = 0; i < status.size(); i++) {
            if (status[i]) {
                prev_corner2.push_back(prev_corner[i]);
                cur_corner2.push_back(cur_corner[i]);
            }
        }

        cv::Mat T;
        // translation + rotation only
        if (prev_corner2.size() > 0 && cur_corner2.size() > 0) 
        {
            T = estimateRigidTransform(prev_corner2, cur_corner2, false);
        }
    

        if (T.data == NULL) {
            last_T.copyTo(T);
        }

        if (last_T.data == NULL)
        {
            stabRoiColor = roiColor.clone();

            warpedRedPlacement = convertedRedTlPlacement;
            warpedYellowPlacement = convertedYellowTlPlacement;
            warpedGreenPlacement = convertedGreenTlPlacement;
            return;
        }

        T.copyTo(last_T);
        // decompose T
        double dx = T.at<double>(0, 2);
        double dy = T.at<double>(1, 2);
        double da = atan2(T.at<double>(1, 0), T.at<double>(0, 0));

        // Accumulated frame to frame transform
        x += dx;
        y += dy;
        a += da;

        //
        z = Trajectory(x, y, a);
        //
        if (k == 1) {
            // intial guesses
            X = Trajectory(0, 0, 0); //Initial estimate,  set 0
            P = Trajectory(1, 1, 1); //set error variance,set 1
        }
        else
        {
            //time update��prediction��
            X_ = X; //X_(k) = X(k-1);
            P_ = P + Q; //P_(k) = P(k-1)+Q;
                        // measurement update correction
            K = P_ / (P_ + R); //gain;K(k) = P_(k)/( P_(k)+R );
            X = X_ + K*(z - X_); //z-X_ is residual,X(k) = X_(k)+K(k)*(z(k)-X_(k)); 
            P = (Trajectory(1, 1, 1) - K)*P_; //P(k) = (1-K(k))*P_(k);
        }
        //smoothed_trajectory.push_back(X);

        //-
        // target - current
        double diff_x = X.x - x;//
        double diff_y = X.y - y;
        double diff_a = X.a - a;

        dx = dx + diff_x;
        dy = dy + diff_y;
        da = da + diff_a;

        //new_prev_to_cur_transform.push_back(TransformParam(dx, dy, da));
        //
        //
        T.at<double>(0, 0) = cos(da);
        T.at<double>(0, 1) = -sin(da);
        T.at<double>(1, 0) = sin(da);
        T.at<double>(1, 1) = cos(da);

        T.at<double>(0, 2) = dx;
        T.at<double>(1, 2) = dy;


        warpedRedPlacement = warpTlPoint(convertedRedTlPlacement,T);
        warpedYellowPlacement = warpTlPoint(convertedYellowTlPlacement, T);
        warpedGreenPlacement = warpTlPoint(convertedGreenTlPlacement, T);

        warpAffine(prev, stabRoiColor, T, cur.size());

        stabRoiColor = stabRoiColor(cv::Range(vert_border, stabRoiColor.rows - vert_border), cv::Range(HORIZONTAL_BORDER_CROP, stabRoiColor.cols - HORIZONTAL_BORDER_CROP));

    
        // Resize cur2 back to cur size, for better side by side comparison
        resize(stabRoiColor, stabRoiColor, cur.size());
        /*
        // Now draw the original and stablised side by side for coolness
        cv::Mat canvas = cv::Mat::zeros(stabRoiColor.rows, stabRoiColor.cols * 2 + 10, stabRoiColor.type());

        prev.copyTo(canvas(cv::Range::all(), cv::Range(0, stabRoiColor.cols)));
        stabRoiColor.copyTo(canvas(cv::Range::all(), cv::Range(stabRoiColor.cols + 10, stabRoiColor.cols * 2 + 10)));

        // If too big to fit on the screen, then scale it down by 2, hopefully it'll fit :)
        if (canvas.cols > 1920) {
            resize(canvas, canvas, cv::Size(canvas.cols / 2, canvas.rows / 2));
        }
        //outputVideo<<canvas;
        imshow("before and after", canvas); */
    }
    else {
        stabRoiColor = roiColor.clone();

        warpedRedPlacement = convertedRedTlPlacement;
        warpedYellowPlacement = convertedYellowTlPlacement;
        warpedGreenPlacement = convertedGreenTlPlacement;
    }
    k++;
    prev = roiColor.clone(); //cur.copyTo(prev);
    cur_grey.copyTo(prev_grey); 
}

cv::Point TrafficLightDetectorSingleModal::warpTlPoint(cv::Point pointToConvert, cv::Mat tMat) {
    return cv::Point( (pointToConvert.x*tMat.at<double>(0, 0) + pointToConvert.y*tMat.at<double>(0, 1) + 1 * tMat.at<double>(0, 2)) , (pointToConvert.x*tMat.at<double>(1, 0) + pointToConvert.y*tMat.at<double>(1, 1) + 1 * tMat.at<double>(1, 2)) );
}

void TrafficLightDetectorSingleModal::extractContours()
{
	/* Extract the contours from the ROI of the image. These are used for generating the
	* bounding box that is used in extractROI */
    
	vector<Vec4i> hierarchy;
	vector<vector<Point>> contours;
	Mat tempMask = Mat::zeros(frame.Image.rows, frame.Image.cols, CV_8UC1);

	// Find the contours of the region of interest
	
	threshold(mask, thresholdedMask, 100, 255, THRESH_BINARY);
	findContours(thresholdedMask, contours, hierarchy, RETR_EXTERNAL,
		CHAIN_APPROX_SIMPLE, Point(0, 0));

	boundRect = boundingRect(Mat(contours[0])); 
    
    convertedRedTlPlacement = { abs(localTlPlacements[0].x - boundRect.x), abs(localTlPlacements[0].y - boundRect.y) };
    convertedYellowTlPlacement = { abs(localTlPlacements[1].x - boundRect.x), abs(localTlPlacements[1].y - boundRect.y) };
    convertedGreenTlPlacement = { abs(localTlPlacements[2].x - boundRect.x), abs(localTlPlacements[2].y - boundRect.y) };

}

void TrafficLightDetectorSingleModal::extractROI()
{
	/* Use the bounding box of the contours to create a limited view of the current frame,
	* and use the mask to filter out pixels outside the mask. */
    Mat tmpRoiColor;
    frame.Image.copyTo(tmpRoiColor, thresholdedMask);

    roiColor = tmpRoiColor(boundRect);
}

cv::Mat TrafficLightDetectorSingleModal::prepareImage(cv::Mat inputImage) {
    std::vector<cv::Mat> imageChannels;

    split(inputImage, imageChannels);

    cv::Mat yChannel = imageChannels[0].clone();

    //GaussianBlur(yChannel, yChannel, cv::Size(5, 5), 0, 0);

    //imshow("Y channel",imageChannels[0]);

    int dilation_size;
    if (frame.Image.rows > 480) dilation_size = 9;
    else dilation_size = 3;


    cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
        cv::Size(2 * dilation_size + 1, 2 * dilation_size + 1),
        cv::Point(dilation_size, dilation_size));

    GaussianBlur(yChannel, yChannel, Size(3, 3), 0, 0);
    morphologyEx(yChannel, yChannel, 3, element);

    cv::Mat thresholdedYChannel;

    threshold(yChannel, thresholdedYChannel, 50, 255, 3);
    //dilate(thresholdedYChannel, thresholdedYChannel, element );

    //imshow("Y channel - morphed",yChannel);
    return thresholdedYChannel;
}

void TrafficLightDetectorSingleModal::tlColorAnalysis() {
    cv::Mat src = roiColor.clone();
    int dilation_size = 1;
    cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
        cv::Size(2 * dilation_size + 1, 2 * dilation_size + 1),
        cv::Point(dilation_size, dilation_size));

    cv::Mat finalImageGray;
    cv::cvtColor(src, finalImageGray, cv::COLOR_RGB2GRAY);

}

void TrafficLightDetectorSingleModal::tlDifferenceImageAnalysis() {
    cv::Mat src = roiColor.clone();
    cv::Mat frameRoiClone = src.clone();

    //imshow("roiColor - src", roiColor);
    cv::cvtColor(src, src, cv::COLOR_RGB2YCrCb);
    //cv::GaussianBlur(src, src, cv::Size(5, 5), 0, 0);
    
    motionFound = false;

    cv::Mat preparedImage = prepareImage(src);
    //imshow("preparedImage", preparedImage);


    //calcROIHisto(preparedImage);
    //imshow("calcHist yChannel", histImageFinal);

    if ( !prevFrameRoi.empty() ) { // Guard as preFrameRoi is null in first iteration.

        cv::Mat thresYchannel;
        cv::adaptiveThreshold(preparedImage, thresYchannel, 255,
            cv::ADAPTIVE_THRESH_GAUSSIAN_C,
            cv::THRESH_BINARY, 75, -15);
    
        cv::Mat differenceImage, thresholdedDiffImage;
        cv::absdiff(preparedImage, prevFrameRoi, differenceImage);

        cv::adaptiveThreshold(differenceImage, thresholdedDiffImage, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 75, -20);

        int whitePixels = cv::countNonZero(thresholdedDiffImage);
        if (whitePixels != 0)
        {
            //imshow("thresholdedDiffImage kept", thresholdedDiffImage);
            cv::Mat myBitWiseMask = cv::Mat::zeros(frameRoiClone.rows, frameRoiClone.cols, CV_8U);
            cv::bitwise_and(thresYchannel, thresholdedDiffImage, myBitWiseMask);
            cv::Mat FinalImage;
            motionFound = true;
            frameRoiClone.copyTo(FinalImage, myBitWiseMask);
            FinalDiffImage = FinalImage;
            //imshow("The final ROI", FinalDiffImage);

        }
    }
    prevFrameRoi = preparedImage;
}
void TrafficLightDetectorSingleModal::tlStructuralAnalysis() {
    //if (!FinalDiffImage.empty() && filterTrigger > 5) // Guard for only one switch per 15 frames.
    if(motionFound)
    {
        filterTrigger = 0;
        cv::Mat src = roiColor.clone();
        cv::Mat canny_output;
       
        std::vector<std::vector<cv::Point>> contours;
        std::vector<cv::Vec4i> hierarchy;
        cv::Mat finalImageGray;
        int thresh = 100;
        int max_thresh = 255;
        cv::RNG rng(12345);
        
        
        cv::cvtColor(FinalDiffImage, finalImageGray, cv::COLOR_RGB2GRAY);

        int dilation_size = 1;
        cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
        cv::Size(2 * dilation_size + 1, 2 * dilation_size + 1),
        cv::Point(dilation_size, dilation_size));

        // Apply the dilation operation
        cv::dilate(finalImageGray, finalImageGray, element);

        // Detect edges using canny
        cv::Canny(finalImageGray, canny_output, thresh, thresh * 2, 3);

        // Find contours
        cv::findContours(canny_output, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

        std::vector<cv::Moments> mu(contours.size());
        for (int i = 0; i < contours.size(); i++)
        {
            mu[i] = cv::moments(contours[i], false);
        }

        ///  Get the mass centers:
        std::vector<cv::Point2f> mc(contours.size());
        if (contours.size() > 0)
        {
            int i = 0;
            
            mc[i] = cv::Point2f(mu[i].m10 / mu[i].m00, mu[i].m01 / mu[i].m00);
            std::cout << "Center Point is: " << mc[i] << std::endl;
            mcX = mc[i].x;

            if (mc[i].x != 'nan' && lastYVal != mc[i].y )
            {
                lastYVal = mc[i].y;
                states.push_back(mc[i].y);
            }
           
        }
    }
    else {
        filterTrigger++;
    }
    tlStructuralMotionAnalysis();
}
void TrafficLightDetectorSingleModal::tlStructuralMotionAnalysis(){
    if (states.size() > 2) {
        if (states.size() == 3)
        {
            std::vector<float>::iterator minIt = std::min_element(states.begin(), states.begin() + 2);
            std::vector<float>::iterator maxIt = std::max_element(states.begin(), states.begin() + 2);

            redHeightAvg = *minIt;
            greenHeightAvg = *maxIt;
            for (int k = 0; k < 3; ++k)
            {
                if (states[k] != redHeightAvg && states[k] != greenHeightAvg)
                {
                    yellowHeightAvg = states[k];
                }
            }
        }
        
        bool goingUp = false;
        bool goingDown = false;

        for (int k = 0; k < 2; k++) {
            if (states[states.size() - 1 - k] < states[states.size() - 2 - k])
            {
                goingUp = true;
            }
            if (states[states.size() - 1 - k] > states[states.size() - 2 - k])
            {
                goingDown = true;
            }
        }


        if (goingUp && !goingDown)
        {
            //std::cout <<"Motion: RED" << std::endl;
            whatClassMotion = 0;
        }
        if (goingUp && goingDown)
        {
            //std::cout <<"Motion: YELLOW" << std::endl;
            whatClassMotion = 1;
        }
        if (!goingUp && goingDown)
        {
            //std::cout <<"Motion: GREEN" << std::endl;
            whatClassMotion = 2;
        }

        cv::Mat presentColor = cv::Mat::zeros(200, 200, CV_8UC3);
        if (whatClassMotion == 0) presentColor(cv::Rect(0, 0, 200, 66)) = cv::Scalar(0, 0, 255);
        if (whatClassMotion == 1) presentColor(cv::Rect(0, 66, 200, 67)) = cv::Scalar(0, 255, 255);
        if (whatClassMotion == 2) presentColor(cv::Rect(0, 133, 200, 67)) = cv::Scalar(0, 255, 0);
        if (whatClassMotion == 3) presentColor(cv::Rect(0, 0, 200, 66)) = cv::Scalar(255, 255, 255);

        char buff[50];
        sprintf(buff, "%f", states[states.size() - 1]);
        putText(presentColor, buff, cv::Point(30, 30),
            FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, cv::LINE_AA);
        //imshow("Motion: Structul Analysis", presentColor);
    }
}

void TrafficLightDetectorSingleModal::tlStructuralNearestAnalysis() {
    
        float distToRed = std::abs(states[states.size() - 1] - redHeightAvg);
        float distToYellow = std::abs(states[states.size() - 1] - yellowHeightAvg);
        float distToGreen = std::abs(states[states.size() - 1] - greenHeightAvg);

        float distConfA = 0;
        float distConfB = 0;

        float trustVal = 0.6;
        whatClassNearest = 3;
        trustNearest = false;
        if (distToRed < distToYellow && distToRed < distToGreen)
        {
            float confAvar = distToYellow;
            float confBvar = distToGreen;
            distConfA = std::abs(distToRed - confAvar) / (std::abs(distToRed - confAvar) + std::abs(distToRed - confBvar));
            distConfB = std::abs(distToRed - confBvar) / (std::abs(distToRed - confAvar) + std::abs(distToRed - confBvar));


            std::cout << "Nearest: RED - conf level(yellow): " << distConfA << " conf level(green): " << distConfB << std::endl;
            whatClassNearest = 0;
            if (distConfA > trustVal && distConfB > trustVal) trustNearest = true;
        }
        if (distToYellow < distToRed && distToYellow < distToGreen)
        {
            float confAvar = distToRed;
            float confBvar = distToGreen;
            distConfA = std::abs(distToYellow - confAvar) / (std::abs(distToYellow - confAvar) + std::abs(distToYellow - confBvar));
            distConfB = std::abs(distToYellow - confBvar) / (std::abs(distToYellow - confAvar) + std::abs(distToYellow - confBvar));

            std::cout << "Nearest: Yellow - conf level(red): " << distConfA << " conf level(green): " << distConfB << std::endl;
            whatClassNearest = 1;
            if (distConfA > trustVal && distConfB > trustVal) trustNearest = true;

        }
        if (distToGreen < distToRed && distToGreen < distToYellow)
        {
            float confAvar = distToRed;
            float confBvar = distToYellow;
            distConfA = std::abs(distToGreen - confAvar) / (std::abs(distToGreen - confAvar) + std::abs(distToGreen - confBvar));
            distConfB = std::abs(distToGreen - confBvar) / (std::abs(distToGreen - confAvar) + std::abs(distToGreen - confBvar));

            std::cout << "Nearest: Green - conf level(red): " << distConfA << " conf level(yellow): " << distConfB << std::endl;
            whatClassNearest = 2;
            if (distConfA > trustVal && distConfB > trustVal) trustNearest = true;
 
        }


        cv::Mat presentColorNearest = cv::Mat::zeros(200, 200, CV_8UC3);
        if (whatClassNearest == 0) presentColorNearest(cv::Rect(0, 0, 200, 66)) = cv::Scalar(0, 0, 255);
        if (whatClassNearest == 1) presentColorNearest(cv::Rect(0, 66, 200, 67)) = cv::Scalar(0, 255, 255);
        if (whatClassNearest == 2) presentColorNearest(cv::Rect(0, 133, 200, 67)) = cv::Scalar(0, 255, 0);
        //if (whatClassNearest == 3) presentColorNearest(cv::Rect(0, 0, 200, 66)) = cv::Scalar(255, 255, 255);
        imshow("Nearest: Structul Analysis", presentColorNearest);

        cv::Mat distToClassesMat = cv::Mat::zeros(200, 600, CV_8UC3);
        char buff[50];
        sprintf(buff, "Distance to RED Height Average(%.2f): %.2f", redHeightAvg,distToRed);
        putText(distToClassesMat, buff, cv::Point(20, 30),
            FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, cv::LINE_AA);
        sprintf(buff, "Distance to YELLOW Height Average(%.2f): %.2f", yellowHeightAvg,distToYellow);
        putText(distToClassesMat, buff, cv::Point(20, 70),
            FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, cv::LINE_AA);
        sprintf(buff, "Distance to GREEN Height Average(%.2f): %.2f", greenHeightAvg, distToGreen);
        putText(distToClassesMat, buff, cv::Point(20, 110), 
            FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, cv::LINE_AA);
        imshow("Distances to Height averages", distToClassesMat);
}

void TrafficLightDetectorSingleModal::tlStructuralValPositionAnalysis() {
    //cv::imshow("stabroicolor", stabRoiColor);
    cv::Mat plottedStabRoiColor;
    plottedStabRoiColor = stabRoiColor.clone();
    cv::circle(plottedStabRoiColor, warpedRedPlacement,2, Scalar(255,255,255),cv::FILLED, 8,0);
    cv::circle(plottedStabRoiColor, warpedYellowPlacement,2, Scalar(150,150,150), cv::FILLED, 8,0);
    cv::circle(plottedStabRoiColor, warpedGreenPlacement,2, Scalar(25,25,25), cv::FILLED, 8,0);

    //cv::imshow("Plotted stabroi", plottedStabRoiColor);


    cv::Mat preparedImage = prepareImage(stabRoiColor.clone());
    //cv::imshow("PreparedImage", preparedImage);
    
    if (warpedRedPlacement.x >= 0 && warpedRedPlacement.y >= 0 && warpedRedPlacement.x < preparedImage.cols && warpedRedPlacement.y < preparedImage.rows) valAtRed = preparedImage.at<uchar>(warpedRedPlacement.y, warpedRedPlacement.x);
    else valAtRed = preparedImage.at<uchar>(convertedRedTlPlacement.y, convertedRedTlPlacement.x);

    if (warpedYellowPlacement.x >= 0 && warpedYellowPlacement.y >= 0 && warpedYellowPlacement.x < preparedImage.cols && warpedYellowPlacement.y < preparedImage.rows) valAtYellow = preparedImage.at<uchar>(warpedYellowPlacement.y, warpedYellowPlacement.x);
    else valAtYellow = preparedImage.at<uchar>(convertedYellowTlPlacement.y, convertedYellowTlPlacement.x);;

    if (warpedGreenPlacement.x >= 0 && warpedGreenPlacement.y >= 0 && warpedGreenPlacement.x < preparedImage.cols && warpedGreenPlacement.y < preparedImage.rows) valAtGreen = preparedImage.at<uchar>(warpedGreenPlacement.y, warpedGreenPlacement.x);
    else valAtGreen = preparedImage.at<uchar>(convertedGreenTlPlacement.y, convertedGreenTlPlacement.x);;
        
    int averageMultiplier = 10;
    if (valAtRed>valAtYellow && valAtRed>valAtGreen && valAtRed > (avgRedVal*0.5) )
    {
        whatClassValPosition = tlTypeClasses::RED;

        if (avgRedVal == NULL) avgRedVal = valAtRed;
        else avgRedVal = ( (avgRedVal*(averageMultiplier - 1)) + valAtRed) / averageMultiplier;

    }
    else if (valAtYellow>valAtRed && valAtYellow>valAtGreen && valAtYellow > (avgYellowVal*0.5) )
    {

        whatClassValPosition = tlTypeClasses::YELLOW;

        if (avgYellowVal == NULL) avgYellowVal = valAtYellow;
        else avgYellowVal = ((avgYellowVal * (averageMultiplier - 1)) + valAtYellow) / averageMultiplier;

    }
    else if (valAtGreen>valAtRed && valAtGreen>valAtYellow && valAtGreen > (avgGreenVal*0.5) )
    {

        whatClassValPosition = tlTypeClasses::GREEN;
        if (avgGreenVal == NULL) avgGreenVal = valAtGreen;
        else avgGreenVal = ((avgGreenVal * (averageMultiplier-1)) + valAtGreen) / averageMultiplier;

    }
    else {
        whatClassValPosition = tlTypeClasses::AMBIGIOUS;
    }
    int minTlIndicesDiff = 15;
    if ( abs(valAtGreen - valAtRed) < minTlIndicesDiff && abs(valAtGreen - valAtYellow) < minTlIndicesDiff )
    {
        whatClassValPosition = tlTypeClasses::AMBIGIOUS;
    }

    if (redYellowCheck <= 10) redYellowCheck = 10;
    else redYellowCheck = (abs(avgRedVal - avgYellowVal)) * 3;
    
    
    if (valAtRed>valAtGreen && valAtYellow>valAtGreen && abs(valAtRed - valAtYellow) < redYellowCheck )
    {

        whatClassValPosition = tlTypeClasses::REDYELLOW;
    }

    tlHistory.resize(199, tlTypeClasses::AMBIGIOUS);
    tlHistory.push_front(whatClassValPosition);

    int latestTlHistVar = tlHistory[0];
    int latestTlHistCounter = 1;
    std::vector<cv::Point> latestTlOverview;

    for (size_t k = 0; k < tlHistory.size()-1 ; k++)
    {
        if (tlHistory[k] == tlHistory[k+1] && latestTlHistVar != -1)
        {
            latestTlHistCounter++;
        }
        else {
            latestTlOverview.push_back( cv::Point(latestTlHistVar, latestTlHistCounter) );
            latestTlHistCounter = 1;
            latestTlHistVar = tlHistory[k + 1];
        }
    }

    int lengthOfTlHist = 0;
    bool tlHistoryOverviewExit = false;
    for (size_t k = 0; k < latestTlOverview.size(); k++)
    {
        lengthOfTlHist = lengthOfTlHist + latestTlOverview[k].y;

        if (lengthOfTlHist >= 18 && k < 2){ // If there are less that 2 of same detections in the past 25 frames. We trust it will remain the same.
            if (latestTlOverview[k].y >= 3 && tlHistoryOverviewExit == false)
            {
                whatClassValPosition = static_cast<tlTypeClasses>(latestTlOverview[0].x);
                tlHistoryOverviewExit = true;
                break;
            }
        }
        else if(k > 5 && lengthOfTlHist < 60 ) // If the past detection is noisy
        {
            whatClassValPosition = tlTypeClasses::AMBIGIOUS;
        }
        else if( k > 10 ) {
            whatClassValPosition = tlTypeClasses::AMBIGIOUS;
        }

        if (tlHistoryOverviewExit)
       { 
            break;
        }

    }


}
std::vector<cv::Point> TrafficLightDetectorSingleModal::getValsAtTls() {
    std::vector<cv::Point> mergedTlIndices;

    return mergedTlIndices;

}

void TrafficLightDetectorSingleModal::tlStructuralPrediction() {
    cv::Mat presentPrediction = cv::Mat::zeros(200, 200, CV_8UC3);
    int  noZeroNumbersInFinalDetections = 0;

    for (int i = 0; i < finalDetections.size(); ++i)
    {
        if (finalDetections[i] != 0)
        {
            noZeroNumbersInFinalDetections++;
        }
    }
    if (noZeroNumbersInFinalDetections >= 3)
    {
        int iteratorStart = finalDetections.size()-1;
        if (finalDetections[iteratorStart] == 0) // If last detection was Red
        {
            whatClassPredict = 1; // Next must be yellow
        }
        else if (finalDetections[iteratorStart] == 2) // If last detection was Green
        {
            whatClassPredict = 1; // Next must be yellow
        }
        else if (finalDetections[iteratorStart] == 1 && finalDetections[iteratorStart - 1] == 0) // If last detection was yellow, and 2nd last was red.
        {
            whatClassPredict = 2; // Next must be Green
        }
        else if (finalDetections[iteratorStart] == 1 && finalDetections[iteratorStart - 1] == 2) // If last detection was yellow, and 2nd last was red.
        {
            whatClassPredict = 0; // Next must be Red
        }


        if (whatClassPredict == 0) presentPrediction(cv::Rect(0, 0, 200, 66)) = cv::Scalar(0, 0, 255);
        if (whatClassPredict == 1) presentPrediction(cv::Rect(0, 66, 200, 67)) = cv::Scalar(0, 255, 255);
        if (whatClassPredict == 2) presentPrediction(cv::Rect(0, 133, 200, 67)) = cv::Scalar(0, 255, 0);
        imshow("Prediciton class", presentPrediction);
    }

}
void TrafficLightDetectorSingleModal::updateClassParameters() {
    updateAverages = false;
    detectionCritSatisfied = false;
    highConf = false;

    finalDetectionID = 3;
    if (whatClassMotion == whatClassValPosition && whatClassMotion == whatClassNearest) // Oh happy days
    {
        highConf = true;
        detectionCritSatisfied = true;
        finalDetectionID = whatClassMotion;
    }
    else if ((whatClassMotion == whatClassPredict) || (whatClassMotion == whatClassValPosition && whatClassMotion == whatClassNearest) )
    {
        highConf = true;
        detectionCritSatisfied = true;
        finalDetectionID = whatClassMotion;
    }
    else if (trustNearest && whatClassNearest == whatClassValPosition) {
        highConf = true;
        detectionCritSatisfied = true;
        finalDetectionID = whatClassNearest;
    }
    else {
        detectionCritSatisfied = true;
        highConf = false;
        finalDetectionID = whatClassMotion;
    }

    if (highConf)
    {
        //std::cout << "Averages updated." << std::endl;
        if (finalDetectionID == 0)
        {
            if (redHeightAvg == 'nan') {
                redHeightAvg = redHeightAvg; // Do nothing
            }
            else {
                if (isnan(redHeightAvg)) redHeightAvg = states[states.size() - 1];
                redHeightAvg = (redHeightAvg + redHeightAvg + redHeightAvg + states[states.size()-1]) / 4;
            }
        }
        else if (finalDetectionID == 1)
        {
            if (yellowHeightAvg == 'nan') {
                yellowHeightAvg = yellowHeightAvg; // Do nothing
            }
            else {
                if ( isnan(yellowHeightAvg) ) yellowHeightAvg = states[states.size() - 1];
                yellowHeightAvg = (yellowHeightAvg + yellowHeightAvg + yellowHeightAvg + states[states.size() - 1]) / 4;
            }
            if (yellowHeightAvg < redHeightAvg) redHeightAvg = yellowHeightAvg - yellowHeightAvg*0.1;
            if (yellowHeightAvg > greenHeightAvg) greenHeightAvg = yellowHeightAvg + yellowHeightAvg*0.1;
        }
        else if (finalDetectionID == 2)
        {
            if (greenHeightAvg == 'nan') {
                greenHeightAvg = greenHeightAvg; // Do nothing
            }
            else  {
                if (isnan(greenHeightAvg)) greenHeightAvg = states[states.size() - 1];
                greenHeightAvg = (greenHeightAvg + greenHeightAvg + greenHeightAvg + states[states.size() - 1]) / 4;
            }
        }
    }    
}

void TrafficLightDetectorSingleModal::finalClassification() {
    cv::Mat presentFinal = cv::Mat::zeros(200, 200, CV_8UC3);
    int orgFinalDetectionsSize = finalDetections.size();
    if (detectionCritSatisfied) // Oh happy days..
    {
        if (finalDetectionID == 0) {
            presentFinal(cv::Rect(0, 0, 200, 66)) = cv::Scalar(0, 0, 255);
            if (finalDetections.size() == 0) { finalDetections.push_back(0); }
            else {
                if (finalDetections[finalDetections.size() - 1] != 0) { finalDetections.push_back(0); }
            }
            
        }
        if (finalDetectionID == 1) {
            presentFinal(cv::Rect(0, 66, 200, 67)) = cv::Scalar(0, 255, 255);
            if (finalDetections.size() == 0) { finalDetections.push_back(1); }
            else {
            if (finalDetections[finalDetections.size() - 1] != 1) finalDetections.push_back(1);
            }
            //finalDetections.erase(finalDetections.begin() + orgFinalDetectionsSize);
        }
        if (finalDetectionID == 2) {
            presentFinal(cv::Rect(0, 133, 200, 67)) = cv::Scalar(0, 255, 0);
            if (finalDetections.size() == 0) { finalDetections.push_back(2); }
            else {
            if (finalDetections[finalDetections.size() - 1] != 2) finalDetections.push_back(2);
            }
            //finalDetections.erase(finalDetections.begin() + orgFinalDetectionsSize);
        }
    }
    //imshow("Final Classification", presentFinal);
}


void TrafficLightDetectorSingleModal::extractColors()
{
    
    tlStructuralValPositionAnalysis();

    //tlDifferenceImageAnalysis();
    /*if(motionFound){
        tlColorAnalysis();
        tlStructuralAnalysis();
        if (states.size() > 2) {
            tlStructuralNearestAnalysis();
            tlStructuralValPositionAnalysis();
            if (finalDetections.size() > 2) tlStructuralPrediction();
            updateClassParameters();
            finalClassification();
        }
    } */
}

void TrafficLightDetectorSingleModal::calcROIHisto(cv::Mat histInput)
{
    /// Establish the number of bins
    int histSize = 256;

    /// Set the ranges ( for B,G,R) )
    float range[] = { 0, 256 };
    const float* histRange = { range };

    bool uniform = true; bool accumulate = false;

    Mat b_hist;

    /// Compute the histograms:
    calcHist(&histInput, 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);

    // Draw the histograms for B, G and R
    int hist_w = 256; int hist_h = 200;
    int bin_w = cvRound((double)hist_w / histSize);

    cv::Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

    /// Normalize the result to [ 0, histImage.rows ]
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());


    /// Draw for each channel
    for (int i = 1; i < histSize; i++)
    {
        line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(b_hist.at<float>(i - 1))),
            Point(bin_w*(i), hist_h - cvRound(b_hist.at<float>(i))),
            Scalar(255, 0, 0), 2, 8, 0);

    }
    histImageFinal = histImage;
    /// Display
    
}

std::vector<QString*> TrafficLightDetectorSingleModal::GetChartTitles()
{
	if (UseScrollingChart)
	{
		vector<QString*> tempTitles;

		for (size_t i = 0; i < Charts.size(); ++i)
		{
			QString* tempWindowName = new QString("Range ");
			tempWindowName->append(QString("%1: %2 to %3 deg").
				arg(i).arg(flowDirectionRange[i].x).arg(flowDirectionRange[i].y));
			tempTitles.push_back(tempWindowName);
		}
		return tempTitles;

	}
	else
	{
		vector<QString*> tempTitles;
		tempTitles.push_back(NULL);

		return tempTitles;
	}
}

