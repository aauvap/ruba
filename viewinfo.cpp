#include "viewinfo.h"

int ViewInfo::compare(const ViewInfo& anotherView) const
{
    if (viewName() != anotherView.viewName()) {
        return 1;
    }

    if (viewId != anotherView.viewId) {
        return 2;
    }

    if (modality != anotherView.modality) {
        return 3;
    }

    return 0;
}

const bool operator < (ViewInfo const& lhs, ViewInfo const& rhs)
{
    return lhs.viewId < rhs.viewId;
}

