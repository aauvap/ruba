#ifndef DETECTORSELECTIONDIALOG_H
#define DETECTORSELECTIONDIALOG_H

#include "utils.hpp"

#include <QCheckBox>
#include <QDialog>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QGridLayout>
#include <QList>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QProgressDialog>
#include <QSizePolicy>
#include <QToolBox>
#include <QToolButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <opencv2/opencv.hpp>

#include "aspectratiopixmaplabel.h"
#include "opencvviewer.h"
#include "maskhandleroutputstruct.h"
#include "logsettingsstruct.h"
#include "logsettingsdialog.h"
#include "addsettingsstruct.h"
#include "ruba.h"
#include "viewinfo.h"
#include "dualimageviewer.h"
#include "framestruct.h"
#include "version.h"

struct Detectors {
	enum e {
		backgroundSubtraction,
		correctDirection,
		wrongDirection,
		trafficLight,
		timing,
	};
};

enum DrawingMode
{
	Disabled,
	Mask,
	TrafficLight
};


namespace Ui {
class DetectorSelectionDialog;
}

class DetectorSelectionDialog : public QDialog
{
    Q_OBJECT

public:  
    explicit DetectorSelectionDialog(QWidget *parent, std::vector<ruba::FrameStruct> frames, int detectorType,
        QString moduleTitle, QString windowTitle, QIcon windowIcon, QImage designImg, 
        std::vector<ruba::MaskHandlerOutputStruct> dParams, ruba::AdditionalSettingsStruct addSettings);
    ~DetectorSelectionDialog();

    std::vector<ruba::MaskHandlerOutputStruct> getDetectorParameters();
    ruba::AdditionalSettingsStruct getAdditionalSettings();
    void setAdditionalSettings(ruba::AdditionalSettingsStruct additionalSettings);
    int setDetectorParameters(const std::vector<ruba::MaskHandlerOutputStruct>& dParams);

    int loadConfiguration(QString filename = QString());
    static int SaveConfiguration(QString filename, std::vector<ruba::MaskHandlerOutputStruct> detectorParams,
        ruba::AdditionalSettingsStruct addSettings, int detectorType, 
        QList<ViewInfo> views);

private slots:
	void changePage(QListWidgetItem* current, QListWidgetItem* previous);

    void mouseHandler(QMouseEvent* event);

    void on_comboBox_currentIndexChanged(const QString &arg1);

    void processTimerTick();

    void restartTimer();

    void on_parameterBoxTab11_valueChanged(double arg1);

    void on_parameterBoxTab21_valueChanged(double arg1);
    void on_parameterBoxTab22_valueChanged(double arg1);

    void on_parameterBoxTab31_valueChanged(double arg1);
    void on_parameterBoxTab32_valueChanged(double arg1);

    void on_saveConfigButton_clicked();

    void on_loadConfigButton_clicked();

    void on_loadMaskButton_clicked();

    void on_drawFinishResetMaskButton_clicked();

    void on_defineBackgroundImageButton_clicked();

    void on_angleDial21_valueChanged(int value);

    void on_angleDial22_valueChanged(int value);

    void on_angleBox21_valueChanged(double arg1);

    void on_angleBox22_valueChanged(double arg1);

    void on_angleDial21_sliderReleased();

    void on_angleDial22_sliderReleased();

    void on_angleBox21_editingFinished();

    void on_angleBox22_editingFinished();

    void on_angleDial21_sliderPressed();

    void on_angleDial22_sliderPressed();

    void on_angleDial31_valueChanged(int value);

    void on_angleDial32_valueChanged(int value);

    void on_angleBox31_valueChanged(double arg1);

    void on_angleBox32_valueChanged(double arg1);

    void on_angleDial31_sliderReleased();

    void on_angleDial32_sliderReleased();

    void on_angleBox31_editingFinished();

    void on_angleBox32_editingFinished();

    void on_angleDial31_sliderPressed();

    void on_angleDial32_sliderPressed();

    void on_detectorTimingBox1_valueChanged(double arg1);

    void on_detectorTimingBox2_valueChanged(double arg1);

    void on_detectorTimingBox3_valueChanged(double arg1);

    void on_saveMaskButton_clicked();

    void on_saveMaskPointsButton_clicked();

    void on_loadMaskPointsButton_clicked();

    void on_subtractFromMaskButton_clicked();

    void on_addToMaskButton_clicked();

    void on_saveBackgroundImageButton_clicked();

    void on_saveConfigAsButton_clicked();

    void on_viewSwitch_currentChanged(int index);

    void on_redAnnotateButton_clicked();

    void on_yellowAnnotateButton_clicked();

    void on_greenAnnotateButton_clicked();

    void on_redButton_clicked(bool checked);

    void on_yellowButton_clicked(bool checked);

    void on_greenButton_clicked(bool checked);

    void on_moveMaskPointUpButton_clicked();

    void on_moveMaskPointDownButton_clicked();

    void on_moveMaskPointLeftButton_clicked();

    void on_moveMaskPointRightButton_clicked();

    void on_selectPrevMaskPointButton_clicked();

    void on_selectNextMaskPointButton_clicked();

private:
    QList<QString> initDetectorComboBox();
    static void saveMultiViewParameter(cv::FileStorage& fs, QString baseName,
        const std::vector<std::vector<int>>& var, const QList<ViewInfo>& views);
    static void saveMultiViewParameter(cv::FileStorage& fs, QString baseName,
        const std::vector<std::vector<double>>& var, const QList<ViewInfo>& views);
    static void saveMultiViewParameter(cv::FileStorage& fs, QString baseName,
        const std::vector<cv::Mat>& var, const QList<ViewInfo>& views);
    void loadMultiViewParameter(cv::FileStorage fs, QString baseName, std::vector<std::vector<int>> &var);
    void loadMultiViewParameter(cv::FileStorage fs, QString baseName, std::vector<std::vector<double>> &var);
    void loadMultiViewParameter(cv::FileStorage fs, QString baseName, std::vector<cv::Mat> &var);

    int currentDetectorType();
    void drawMasks(std::vector<int> maskIndices);
    void drawFlowVectors(cv::Mat img, int maskIndex, int vectorNbr = 0);
    void computeMaskCentre();
    void drawTLPoints();
    void drawAngleHelper(int angle1, int angle2, bool recomputeCentre);
    void setUpLayout(bool isDetectorInitialized, int index);
    void finishMask(int index);
    void initDefaults();
    void setDefaultDetectorParameters(int index);
    void checkConfiguration();

    void configureValueBox(QDoubleSpinBox* boxHandle, double value, int decimals, double singleStep, bool blockSignals = true);
    void configureTrafficLightAnnotationButton(QToolButton* buttonHandle, QCheckBox* checkBoxHandle, cv::Point point, tlTypeClasses lightType);

    void on_comboBox_currentIndexChanged();

    int convertFileFormat(QString file);

    void keyPressEvent(QKeyEvent *e);


    Ui::DetectorSelectionDialog *ui;
    OpenCVViewer *mviewer;
    OpenCVViewer *designViewer;
    std::vector<cv::Mat> images, overlayedImages;
    QTimer ProcessTimer;
    int nbrPDetectors;
    int nbrMDetectors;
    int nbrSDetectors;
	int nbrTLDetectors;
    int detectorType;
    QList<ViewInfo> views;
    int currentViewIndex;

    DrawingMode drawingMode;
	tlTypeClasses annotatedTrafficLight;
    
    cv::Point maskCentre;
    QString moduleTitle;
    int currentMaskPoint;

    std::vector<ruba::MaskHandlerOutputStruct> detectorParams;
    ruba::AdditionalSettingsStruct addSettings;
    std::vector<std::vector<int> > currentFlowVecAngle;

    enum Directions{CLOCKWISE, COUNTERCLOCKWISE};
    std::vector<std::vector<Directions> > flowVecDirec;

    std::shared_ptr<LogSettingsDialog> logDialog;

    int prevDetectorType;

	QHash<int, QGroupBox*> detectorTabs;
	QSpacerItem* emptyVerticalFiller;
};

#endif // DETECTORSELECTIONDIALOG_H
