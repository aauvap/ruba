﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.picImage = New System.Windows.Forms.PictureBox()
        Me.grpRecordData = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDetector2Title = New System.Windows.Forms.Label()
        Me.lblDetector2 = New System.Windows.Forms.Label()
        Me.lblDetector1Title = New System.Windows.Forms.Label()
        Me.lblDetector1 = New System.Windows.Forms.Label()
        Me.lblTimeGapTitle = New System.Windows.Forms.Label()
        Me.lblTimeGap = New System.Windows.Forms.Label()
        Me.lblFirstTriggeredTitle = New System.Windows.Forms.Label()
        Me.lblFirstTriggered = New System.Windows.Forms.Label()
        Me.lblImageFileTitle = New System.Windows.Forms.Label()
        Me.lblImageFile = New System.Windows.Forms.Label()
        Me.lblFileTitle = New System.Windows.Forms.Label()
        Me.lblFile = New System.Windows.Forms.Label()
        Me.pnlSetStatus = New System.Windows.Forms.Panel()
        Me.btnStatus = New System.Windows.Forms.Button()
        Me.imlList = New System.Windows.Forms.ImageList(Me.components)
        Me.btnFalseDetection = New System.Windows.Forms.Button()
        Me.btnNotSure = New System.Windows.Forms.Button()
        Me.btnCorrectDetection = New System.Windows.Forms.Button()
        Me.pnlNavigation = New System.Windows.Forms.Panel()
        Me.btnJumpBack = New System.Windows.Forms.Button()
        Me.btnJumpForward = New System.Windows.Forms.Button()
        Me.btnPrevious = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.lblRecordCount = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileOpen = New System.Windows.Forms.ToolStripMenuItem()
        Me.MergeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NumberOfDetectorsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuData = New System.Windows.Forms.ToolStripMenuItem()
        Me.FilterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDataFilterNotChecked = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDataFilterCorrect = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDataFilterFalseAlarms = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDataFilterNotSure = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGapTimeWindow = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.SummaryFilseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button0 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.Other_data = New System.Windows.Forms.Panel()
        Me.Chosen = New System.Windows.Forms.Button()
        Me.OtherData = New System.Windows.Forms.CheckBox()
        Me.OutputName = New System.Windows.Forms.TextBox()
        Me.ChangeText = New System.Windows.Forms.Button()
        Me.Num_Detectors = New System.Windows.Forms.Button()
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpRecordData.SuspendLayout()
        Me.pnlSetStatus.SuspendLayout()
        Me.pnlNavigation.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.Other_data.SuspendLayout()
        Me.SuspendLayout()
        '
        'picImage
        '
        Me.picImage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picImage.Location = New System.Drawing.Point(429, 27)
        Me.picImage.Name = "picImage"
        Me.picImage.Size = New System.Drawing.Size(281, 519)
        Me.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picImage.TabIndex = 0
        Me.picImage.TabStop = False
        '
        'grpRecordData
        '
        Me.grpRecordData.Controls.Add(Me.Label3)
        Me.grpRecordData.Controls.Add(Me.Label2)
        Me.grpRecordData.Controls.Add(Me.Label1)
        Me.grpRecordData.Controls.Add(Me.lblDetector2Title)
        Me.grpRecordData.Controls.Add(Me.lblDetector2)
        Me.grpRecordData.Controls.Add(Me.lblDetector1Title)
        Me.grpRecordData.Controls.Add(Me.lblDetector1)
        Me.grpRecordData.Controls.Add(Me.lblTimeGapTitle)
        Me.grpRecordData.Controls.Add(Me.lblTimeGap)
        Me.grpRecordData.Controls.Add(Me.lblFirstTriggeredTitle)
        Me.grpRecordData.Controls.Add(Me.lblFirstTriggered)
        Me.grpRecordData.Controls.Add(Me.lblImageFileTitle)
        Me.grpRecordData.Controls.Add(Me.lblImageFile)
        Me.grpRecordData.Controls.Add(Me.lblFileTitle)
        Me.grpRecordData.Controls.Add(Me.lblFile)
        Me.grpRecordData.Controls.Add(Me.pnlSetStatus)
        Me.grpRecordData.Enabled = False
        Me.grpRecordData.Location = New System.Drawing.Point(12, 27)
        Me.grpRecordData.Name = "grpRecordData"
        Me.grpRecordData.Size = New System.Drawing.Size(411, 277)
        Me.grpRecordData.TabIndex = 10
        Me.grpRecordData.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(270, 179)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(15, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "D"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(207, 179)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(14, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "S"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(139, 179)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(14, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "A"
        '
        'lblDetector2Title
        '
        Me.lblDetector2Title.AutoSize = True
        Me.lblDetector2Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetector2Title.Location = New System.Drawing.Point(3, 82)
        Me.lblDetector2Title.Name = "lblDetector2Title"
        Me.lblDetector2Title.Size = New System.Drawing.Size(71, 13)
        Me.lblDetector2Title.TabIndex = 21
        Me.lblDetector2Title.Text = "Detector 2:"
        '
        'lblDetector2
        '
        Me.lblDetector2.Location = New System.Drawing.Point(77, 82)
        Me.lblDetector2.Name = "lblDetector2"
        Me.lblDetector2.Size = New System.Drawing.Size(328, 13)
        Me.lblDetector2.TabIndex = 20
        Me.lblDetector2.Text = "Detector2"
        '
        'lblDetector1Title
        '
        Me.lblDetector1Title.AutoSize = True
        Me.lblDetector1Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetector1Title.Location = New System.Drawing.Point(3, 60)
        Me.lblDetector1Title.Name = "lblDetector1Title"
        Me.lblDetector1Title.Size = New System.Drawing.Size(71, 13)
        Me.lblDetector1Title.TabIndex = 19
        Me.lblDetector1Title.Text = "Detector 1:"
        '
        'lblDetector1
        '
        Me.lblDetector1.Location = New System.Drawing.Point(77, 60)
        Me.lblDetector1.Name = "lblDetector1"
        Me.lblDetector1.Size = New System.Drawing.Size(328, 13)
        Me.lblDetector1.TabIndex = 18
        Me.lblDetector1.Text = "Detector1"
        '
        'lblTimeGapTitle
        '
        Me.lblTimeGapTitle.AutoSize = True
        Me.lblTimeGapTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTimeGapTitle.Location = New System.Drawing.Point(11, 126)
        Me.lblTimeGapTitle.Name = "lblTimeGapTitle"
        Me.lblTimeGapTitle.Size = New System.Drawing.Size(63, 13)
        Me.lblTimeGapTitle.TabIndex = 17
        Me.lblTimeGapTitle.Text = "Time gap:"
        '
        'lblTimeGap
        '
        Me.lblTimeGap.Location = New System.Drawing.Point(77, 126)
        Me.lblTimeGap.Name = "lblTimeGap"
        Me.lblTimeGap.Size = New System.Drawing.Size(328, 13)
        Me.lblTimeGap.TabIndex = 16
        Me.lblTimeGap.Text = "time gap"
        '
        'lblFirstTriggeredTitle
        '
        Me.lblFirstTriggeredTitle.AutoSize = True
        Me.lblFirstTriggeredTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstTriggeredTitle.Location = New System.Drawing.Point(46, 104)
        Me.lblFirstTriggeredTitle.Name = "lblFirstTriggeredTitle"
        Me.lblFirstTriggeredTitle.Size = New System.Drawing.Size(28, 13)
        Me.lblFirstTriggeredTitle.TabIndex = 15
        Me.lblFirstTriggeredTitle.Text = "1st:"
        '
        'lblFirstTriggered
        '
        Me.lblFirstTriggered.Location = New System.Drawing.Point(77, 104)
        Me.lblFirstTriggered.Name = "lblFirstTriggered"
        Me.lblFirstTriggered.Size = New System.Drawing.Size(328, 13)
        Me.lblFirstTriggered.TabIndex = 14
        Me.lblFirstTriggered.Text = "First triggered"
        '
        'lblImageFileTitle
        '
        Me.lblImageFileTitle.AutoSize = True
        Me.lblImageFileTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImageFileTitle.Location = New System.Drawing.Point(8, 38)
        Me.lblImageFileTitle.Name = "lblImageFileTitle"
        Me.lblImageFileTitle.Size = New System.Drawing.Size(66, 13)
        Me.lblImageFileTitle.TabIndex = 13
        Me.lblImageFileTitle.Text = "Image file:"
        '
        'lblImageFile
        '
        Me.lblImageFile.Location = New System.Drawing.Point(77, 38)
        Me.lblImageFile.Name = "lblImageFile"
        Me.lblImageFile.Size = New System.Drawing.Size(328, 13)
        Me.lblImageFile.TabIndex = 12
        Me.lblImageFile.Text = "Image file"
        '
        'lblFileTitle
        '
        Me.lblFileTitle.AutoSize = True
        Me.lblFileTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileTitle.Location = New System.Drawing.Point(10, 16)
        Me.lblFileTitle.Name = "lblFileTitle"
        Me.lblFileTitle.Size = New System.Drawing.Size(64, 13)
        Me.lblFileTitle.TabIndex = 11
        Me.lblFileTitle.Text = "Video file:"
        '
        'lblFile
        '
        Me.lblFile.Location = New System.Drawing.Point(77, 16)
        Me.lblFile.Name = "lblFile"
        Me.lblFile.Size = New System.Drawing.Size(328, 13)
        Me.lblFile.TabIndex = 10
        Me.lblFile.Text = "File"
        '
        'pnlSetStatus
        '
        Me.pnlSetStatus.Controls.Add(Me.btnStatus)
        Me.pnlSetStatus.Controls.Add(Me.btnFalseDetection)
        Me.pnlSetStatus.Controls.Add(Me.btnNotSure)
        Me.pnlSetStatus.Controls.Add(Me.btnCorrectDetection)
        Me.pnlSetStatus.Location = New System.Drawing.Point(110, 195)
        Me.pnlSetStatus.Name = "pnlSetStatus"
        Me.pnlSetStatus.Size = New System.Drawing.Size(214, 76)
        Me.pnlSetStatus.TabIndex = 9
        '
        'btnStatus
        '
        Me.btnStatus.ImageIndex = 0
        Me.btnStatus.ImageList = Me.imlList
        Me.btnStatus.Location = New System.Drawing.Point(5, 3)
        Me.btnStatus.Name = "btnStatus"
        Me.btnStatus.Size = New System.Drawing.Size(64, 64)
        Me.btnStatus.TabIndex = 4
        Me.btnStatus.TabStop = False
        Me.btnStatus.UseVisualStyleBackColor = True
        '
        'imlList
        '
        Me.imlList.ImageStream = CType(resources.GetObject("imlList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imlList.TransparentColor = System.Drawing.Color.Black
        Me.imlList.Images.SetKeyName(0, "NotChecked.png")
        Me.imlList.Images.SetKeyName(1, "Correct.png")
        Me.imlList.Images.SetKeyName(2, "False.png")
        Me.imlList.Images.SetKeyName(3, "NotSure.png")
        '
        'btnFalseDetection
        '
        Me.btnFalseDetection.ImageIndex = 2
        Me.btnFalseDetection.ImageList = Me.imlList
        Me.btnFalseDetection.Location = New System.Drawing.Point(4, 3)
        Me.btnFalseDetection.Name = "btnFalseDetection"
        Me.btnFalseDetection.Size = New System.Drawing.Size(64, 64)
        Me.btnFalseDetection.TabIndex = 1
        Me.btnFalseDetection.TabStop = False
        Me.btnFalseDetection.UseVisualStyleBackColor = True
        '
        'btnNotSure
        '
        Me.btnNotSure.ImageIndex = 3
        Me.btnNotSure.ImageList = Me.imlList
        Me.btnNotSure.Location = New System.Drawing.Point(74, 3)
        Me.btnNotSure.Name = "btnNotSure"
        Me.btnNotSure.Size = New System.Drawing.Size(64, 64)
        Me.btnNotSure.TabIndex = 11
        Me.btnNotSure.TabStop = False
        Me.btnNotSure.UseVisualStyleBackColor = True
        '
        'btnCorrectDetection
        '
        Me.btnCorrectDetection.ImageIndex = 1
        Me.btnCorrectDetection.ImageList = Me.imlList
        Me.btnCorrectDetection.Location = New System.Drawing.Point(144, 3)
        Me.btnCorrectDetection.Name = "btnCorrectDetection"
        Me.btnCorrectDetection.Size = New System.Drawing.Size(64, 64)
        Me.btnCorrectDetection.TabIndex = 2
        Me.btnCorrectDetection.TabStop = False
        Me.btnCorrectDetection.UseVisualStyleBackColor = True
        '
        'pnlNavigation
        '
        Me.pnlNavigation.Controls.Add(Me.btnJumpBack)
        Me.pnlNavigation.Controls.Add(Me.btnJumpForward)
        Me.pnlNavigation.Controls.Add(Me.btnPrevious)
        Me.pnlNavigation.Controls.Add(Me.btnNext)
        Me.pnlNavigation.Controls.Add(Me.lblRecordCount)
        Me.pnlNavigation.Enabled = False
        Me.pnlNavigation.Location = New System.Drawing.Point(80, 320)
        Me.pnlNavigation.Name = "pnlNavigation"
        Me.pnlNavigation.Size = New System.Drawing.Size(298, 50)
        Me.pnlNavigation.TabIndex = 10
        '
        'btnJumpBack
        '
        Me.btnJumpBack.Location = New System.Drawing.Point(2, 2)
        Me.btnJumpBack.Name = "btnJumpBack"
        Me.btnJumpBack.Size = New System.Drawing.Size(45, 45)
        Me.btnJumpBack.TabIndex = 9
        Me.btnJumpBack.TabStop = False
        Me.btnJumpBack.Text = "<<"
        Me.btnJumpBack.UseVisualStyleBackColor = True
        '
        'btnJumpForward
        '
        Me.btnJumpForward.Location = New System.Drawing.Point(250, 2)
        Me.btnJumpForward.Name = "btnJumpForward"
        Me.btnJumpForward.Size = New System.Drawing.Size(45, 45)
        Me.btnJumpForward.TabIndex = 8
        Me.btnJumpForward.TabStop = False
        Me.btnJumpForward.Text = ">>"
        Me.btnJumpForward.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.Location = New System.Drawing.Point(47, 2)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(45, 45)
        Me.btnPrevious.TabIndex = 1
        Me.btnPrevious.TabStop = False
        Me.btnPrevious.Text = "<"
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(205, 2)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(45, 45)
        Me.btnNext.TabIndex = 2
        Me.btnNext.TabStop = False
        Me.btnNext.Text = ">"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'lblRecordCount
        '
        Me.lblRecordCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRecordCount.Location = New System.Drawing.Point(92, 2)
        Me.lblRecordCount.Name = "lblRecordCount"
        Me.lblRecordCount.Size = New System.Drawing.Size(113, 45)
        Me.lblRecordCount.TabIndex = 7
        Me.lblRecordCount.Text = "25 of 150"
        Me.lblRecordCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuData})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(726, 24)
        Me.MenuStrip1.TabIndex = 11
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileOpen, Me.MergeToolStripMenuItem, Me.NumberOfDetectorsToolStripMenuItem})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(37, 20)
        Me.mnuFile.Text = "File"
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Name = "mnuFileOpen"
        Me.mnuFileOpen.Size = New System.Drawing.Size(184, 22)
        Me.mnuFileOpen.Text = "Open"
        '
        'MergeToolStripMenuItem
        '
        Me.MergeToolStripMenuItem.Name = "MergeToolStripMenuItem"
        Me.MergeToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.MergeToolStripMenuItem.Text = "Merge"
        '
        'NumberOfDetectorsToolStripMenuItem
        '
        Me.NumberOfDetectorsToolStripMenuItem.Name = "NumberOfDetectorsToolStripMenuItem"
        Me.NumberOfDetectorsToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.NumberOfDetectorsToolStripMenuItem.Text = "Number of detectors"
        '
        'mnuData
        '
        Me.mnuData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FilterToolStripMenuItem, Me.mnuGapTimeWindow, Me.ToolStripSeparator2, Me.SummaryFilseToolStripMenuItem})
        Me.mnuData.Name = "mnuData"
        Me.mnuData.Size = New System.Drawing.Size(43, 20)
        Me.mnuData.Text = "Data"
        '
        'FilterToolStripMenuItem
        '
        Me.FilterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDataFilterNotChecked, Me.ToolStripSeparator1, Me.mnuDataFilterCorrect, Me.mnuDataFilterFalseAlarms, Me.mnuDataFilterNotSure})
        Me.FilterToolStripMenuItem.Name = "FilterToolStripMenuItem"
        Me.FilterToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.FilterToolStripMenuItem.Text = "Filter"
        '
        'mnuDataFilterNotChecked
        '
        Me.mnuDataFilterNotChecked.Name = "mnuDataFilterNotChecked"
        Me.mnuDataFilterNotChecked.Size = New System.Drawing.Size(141, 22)
        Me.mnuDataFilterNotChecked.Text = "Not checked"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(138, 6)
        '
        'mnuDataFilterCorrect
        '
        Me.mnuDataFilterCorrect.Name = "mnuDataFilterCorrect"
        Me.mnuDataFilterCorrect.Size = New System.Drawing.Size(141, 22)
        Me.mnuDataFilterCorrect.Text = "Correct"
        '
        'mnuDataFilterFalseAlarms
        '
        Me.mnuDataFilterFalseAlarms.Name = "mnuDataFilterFalseAlarms"
        Me.mnuDataFilterFalseAlarms.Size = New System.Drawing.Size(141, 22)
        Me.mnuDataFilterFalseAlarms.Text = "False alarms"
        '
        'mnuDataFilterNotSure
        '
        Me.mnuDataFilterNotSure.Name = "mnuDataFilterNotSure"
        Me.mnuDataFilterNotSure.Size = New System.Drawing.Size(141, 22)
        Me.mnuDataFilterNotSure.Text = "Not sure"
        '
        'mnuGapTimeWindow
        '
        Me.mnuGapTimeWindow.Name = "mnuGapTimeWindow"
        Me.mnuGapTimeWindow.Size = New System.Drawing.Size(180, 22)
        Me.mnuGapTimeWindow.Text = "Time Gap interval"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(177, 6)
        '
        'SummaryFilseToolStripMenuItem
        '
        Me.SummaryFilseToolStripMenuItem.Name = "SummaryFilseToolStripMenuItem"
        Me.SummaryFilseToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.SummaryFilseToolStripMenuItem.Text = "Summary files"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(2, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(83, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(164, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 15
        Me.Button3.Text = "3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(245, 4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 16
        Me.Button4.Text = "4"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(326, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 17
        Me.Button5.Text = "5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(2, 56)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 18
        Me.Button6.Text = "6"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(83, 56)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 19
        Me.Button7.Text = "7"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(164, 56)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 23)
        Me.Button8.TabIndex = 20
        Me.Button8.Text = "8"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(245, 56)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(75, 23)
        Me.Button9.TabIndex = 21
        Me.Button9.Text = "9"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button0
        '
        Me.Button0.Location = New System.Drawing.Point(326, 56)
        Me.Button0.Name = "Button0"
        Me.Button0.Size = New System.Drawing.Size(75, 23)
        Me.Button0.TabIndex = 22
        Me.Button0.Text = "0"
        Me.Button0.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(3, 30)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(74, 20)
        Me.TextBox1.TabIndex = 23
        Me.TextBox1.Text = "1"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(84, 30)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(74, 20)
        Me.TextBox2.TabIndex = 24
        Me.TextBox2.Text = "2"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(165, 30)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(74, 20)
        Me.TextBox3.TabIndex = 25
        Me.TextBox3.Text = "3"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(247, 30)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(74, 20)
        Me.TextBox4.TabIndex = 26
        Me.TextBox4.Text = "4"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(327, 30)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(74, 20)
        Me.TextBox5.TabIndex = 27
        Me.TextBox5.Text = "5"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(3, 83)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(74, 20)
        Me.TextBox6.TabIndex = 28
        Me.TextBox6.Text = "6"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(83, 83)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(74, 20)
        Me.TextBox7.TabIndex = 29
        Me.TextBox7.Text = "7"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(165, 83)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(74, 20)
        Me.TextBox8.TabIndex = 30
        Me.TextBox8.Text = "8"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(247, 83)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(74, 20)
        Me.TextBox9.TabIndex = 31
        Me.TextBox9.Text = "9"
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(327, 83)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.ReadOnly = True
        Me.TextBox10.Size = New System.Drawing.Size(74, 20)
        Me.TextBox10.TabIndex = 32
        Me.TextBox10.Text = "10"
        '
        'Other_data
        '
        Me.Other_data.AllowDrop = True
        Me.Other_data.Controls.Add(Me.Chosen)
        Me.Other_data.Controls.Add(Me.TextBox6)
        Me.Other_data.Controls.Add(Me.TextBox7)
        Me.Other_data.Controls.Add(Me.TextBox8)
        Me.Other_data.Controls.Add(Me.TextBox9)
        Me.Other_data.Controls.Add(Me.TextBox10)
        Me.Other_data.Controls.Add(Me.TextBox5)
        Me.Other_data.Controls.Add(Me.TextBox3)
        Me.Other_data.Controls.Add(Me.TextBox4)
        Me.Other_data.Controls.Add(Me.TextBox2)
        Me.Other_data.Controls.Add(Me.TextBox1)
        Me.Other_data.Controls.Add(Me.Button6)
        Me.Other_data.Controls.Add(Me.Button7)
        Me.Other_data.Controls.Add(Me.Button8)
        Me.Other_data.Controls.Add(Me.Button9)
        Me.Other_data.Controls.Add(Me.Button0)
        Me.Other_data.Controls.Add(Me.Button5)
        Me.Other_data.Controls.Add(Me.Button4)
        Me.Other_data.Controls.Add(Me.Button3)
        Me.Other_data.Controls.Add(Me.Button2)
        Me.Other_data.Controls.Add(Me.Button1)
        Me.Other_data.Enabled = False
        Me.Other_data.Location = New System.Drawing.Point(12, 435)
        Me.Other_data.Name = "Other_data"
        Me.Other_data.Size = New System.Drawing.Size(411, 111)
        Me.Other_data.TabIndex = 32
        Me.Other_data.Visible = False
        '
        'Chosen
        '
        Me.Chosen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Chosen.Enabled = False
        Me.Chosen.Location = New System.Drawing.Point(0, 0)
        Me.Chosen.Name = "Chosen"
        Me.Chosen.Size = New System.Drawing.Size(411, 111)
        Me.Chosen.TabIndex = 33
        Me.Chosen.Text = "Chosen"
        Me.Chosen.UseVisualStyleBackColor = True
        Me.Chosen.Visible = False
        '
        'OtherData
        '
        Me.OtherData.AutoSize = True
        Me.OtherData.Enabled = False
        Me.OtherData.Location = New System.Drawing.Point(12, 412)
        Me.OtherData.Name = "OtherData"
        Me.OtherData.Size = New System.Drawing.Size(130, 17)
        Me.OtherData.TabIndex = 12
        Me.OtherData.Text = "Further image analysis"
        Me.OtherData.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.OtherData.UseVisualStyleBackColor = True
        Me.OtherData.Visible = False
        '
        'OutputName
        '
        Me.OutputName.Enabled = False
        Me.OutputName.Location = New System.Drawing.Point(186, 409)
        Me.OutputName.Name = "OutputName"
        Me.OutputName.ReadOnly = True
        Me.OutputName.Size = New System.Drawing.Size(150, 20)
        Me.OutputName.TabIndex = 33
        Me.OutputName.Text = "output.csv"
        Me.OutputName.Visible = False
        '
        'ChangeText
        '
        Me.ChangeText.Enabled = False
        Me.ChangeText.Location = New System.Drawing.Point(342, 409)
        Me.ChangeText.Name = "ChangeText"
        Me.ChangeText.Size = New System.Drawing.Size(75, 20)
        Me.ChangeText.TabIndex = 34
        Me.ChangeText.Text = "Change text"
        Me.ChangeText.UseVisualStyleBackColor = True
        Me.ChangeText.Visible = False
        '
        'Num_Detectors
        '
        Me.Num_Detectors.Location = New System.Drawing.Point(87, 2)
        Me.Num_Detectors.Name = "Num_Detectors"
        Me.Num_Detectors.Size = New System.Drawing.Size(102, 20)
        Me.Num_Detectors.TabIndex = 35
        Me.Num_Detectors.Text = "2 Detectors"
        Me.Num_Detectors.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 558)
        Me.Controls.Add(Me.Num_Detectors)
        Me.Controls.Add(Me.ChangeText)
        Me.Controls.Add(Me.OutputName)
        Me.Controls.Add(Me.OtherData)
        Me.Controls.Add(Me.Other_data)
        Me.Controls.Add(Me.pnlNavigation)
        Me.Controls.Add(Me.grpRecordData)
        Me.Controls.Add(Me.picImage)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "RUBA Control"
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpRecordData.ResumeLayout(False)
        Me.grpRecordData.PerformLayout()
        Me.pnlSetStatus.ResumeLayout(False)
        Me.pnlNavigation.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Other_data.ResumeLayout(False)
        Me.Other_data.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents picImage As PictureBox
    Friend WithEvents grpRecordData As GroupBox
    Friend WithEvents pnlNavigation As Panel
    Friend WithEvents btnPrevious As Button
    Friend WithEvents btnNext As Button
    Friend WithEvents lblRecordCount As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents mnuFile As ToolStripMenuItem
    Friend WithEvents mnuFileOpen As ToolStripMenuItem
    Friend WithEvents mnuData As ToolStripMenuItem
    Friend WithEvents FilterToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mnuDataFilterNotChecked As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents mnuDataFilterCorrect As ToolStripMenuItem
    Friend WithEvents mnuDataFilterFalseAlarms As ToolStripMenuItem
    Friend WithEvents mnuDataFilterNotSure As ToolStripMenuItem
    Friend WithEvents mnuGapTimeWindow As ToolStripMenuItem
    Friend WithEvents btnCorrectDetection As Button
    Friend WithEvents btnNotSure As Button
    Friend WithEvents btnFalseDetection As Button
    Friend WithEvents pnlSetStatus As Panel
    Friend WithEvents imlList As ImageList
    Friend WithEvents btnJumpBack As Button
    Friend WithEvents btnJumpForward As Button
    Friend WithEvents lblFile As Label
    Friend WithEvents lblImageFileTitle As Label
    Friend WithEvents lblImageFile As Label
    Friend WithEvents lblFileTitle As Label
    Friend WithEvents lblTimeGapTitle As Label
    Friend WithEvents lblTimeGap As Label
    Friend WithEvents lblFirstTriggeredTitle As Label
    Friend WithEvents lblFirstTriggered As Label
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents SummaryFilseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblDetector2Title As Label
    Friend WithEvents lblDetector2 As Label
    Friend WithEvents lblDetector1Title As Label
    Friend WithEvents lblDetector1 As Label
    Friend WithEvents btnStatus As Button
    Friend WithEvents MergeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Button0 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents TextBox10 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Other_data As Panel
    Friend WithEvents OtherData As CheckBox
    Friend WithEvents OutputName As TextBox
    Friend WithEvents ChangeText As Button
    Friend WithEvents Chosen As Button
    Friend WithEvents NumberOfDetectorsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Num_Detectors As Button
End Class
