#ifndef ABSTRACTTRAFFICDETECTOR_H
#define ABSTRACTTRAFFICDETECTOR_H

#include <QDialog>
#include <QFileDialog>
#include <QTranslator>

#include <opencv2/opencv.hpp>
#include "opencvviewer.h"
#include "framestruct.h"
#include "detectoroutputstruct.h"
#include "ChartStruct.h"
#include "ruba.h"
#include "maskoverlaystruct.h"
#include "scrollingchart.h"
#include "maskhandleroutputstruct.h"
#include "dialogs/detectorselectiondialog.h"

struct BaseDetectorParams {
    int detectorID;
    int detectorType;

    std::vector<ruba::FrameStruct> initFrames;
};

typedef enum {
    Working,
    Error, 
    Exception
} DiagnosticState;

struct DetectorDiagnostics {
    DiagnosticState state;
    QString errorMessage;

    bool errorStateChanged;
};




class ruba::AbstractTrafficDetector
{

public:
    AbstractTrafficDetector(BaseDetectorParams baseParams);

    ~AbstractTrafficDetector();

    virtual ruba::DetectorOutput GetOutput() = 0;
    QList<QString> GetDebugInfo();
    virtual QList<QString> getDetectorSpecificDebugInfo() = 0;

    bool GetIsInitialized();
    std::vector<bool> GetIsTriggered();
    std::vector<QQueue<bool> > GetTriggeredHistory();
    void ResetTriggeredHistory();
    DetectorDiagnostics getDiagnostics();


    virtual void SetUseScrollingChart(bool useScrollingChart) = 0;

    virtual std::vector<ruba::ChartStruct> GetChartStructs() = 0;

    int getDetectorType();
    int getDetectorID();
    QString getDetectorAbbreviation();
    
    QString getDetectorName();
    QString getShortDetectorName();


    std::vector<QDateTime> GetLastChange();

    void Update(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput = std::vector<DetectorOutput>());

    virtual void InitDetector() = 0;

    // Dependency upon other TrafficDetectors
    void AddParent(FamilyInfo parent);
    void RemoveParent(FamilyInfo parent);
    bool HasParents();

    QList<FamilyInfo> GetChildren();
    bool HasChildren();

    // Modality handling
    bool checkCurrentViews(QList<ViewInfo> enabledModalities);

protected:
    virtual void updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput = std::vector<DetectorOutput>()) = 0;
    static BaseDetectorParams configureBaseDetectorParams(BaseDetectorParams initBaseParams, int detectorType);
    void reconfigureBaseDetectorParams(BaseDetectorParams newBaseParams);

    QDateTime getCurrentTime();
    QList<ViewInfo> getCurrentViews();

    std::vector<ruba::FrameStruct> frames;

    // Dependency upon other TrafficDetectors
    QList<FamilyInfo> childDetectors;
    QList<FamilyInfo> parentDetectors;

    // External flags that are accessible through a get function
    bool IsInitialized;
    std::vector<bool> IsTriggered;
    std::vector<QQueue<bool>> triggeredHistory;
    std::vector<QDateTime> lastChange;

    void initTriggeredHistory(int dim);

private:
    void updateTime(std::vector<ruba::FrameStruct> frames);
    void updateTime(ruba::FrameStruct frame);

    // Settings:
    QList<ViewInfo> views;
    QList<cv::Size> initFrameResolution;
    int detectorID;
    int detectorType;

    // Diagnostic facilities to check detector conformity
    DetectorDiagnostics diagnostics;

    bool reshapeUpdateStruct();
    bool checkFrameResolution();

    QDateTime currentTime;
};

#endif // ABSTRACTTRAFFICDETECTOR_H
