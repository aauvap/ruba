#include "dialogs/groundtruthannotatordialog.h"
#include "ui_groundtruthannotatordialog.h"

GroundTruthAnnotatorDialog::GroundTruthAnnotatorDialog(QWidget *parent, const std::vector<ruba::FrameStruct>& initFrames) :
    QDialog(parent),
    ui(new Ui::GroundTruthAnnotatorDialog)
{
    this->parent = parent;
    gtTable = new QStandardItemModel();

    // Populate the list of header labels
    headerLabels = new QStandardItemModel();
    headerLabels->setColumnCount(1);
    QStandardItem *item1 = new QStandardItem(QIcon("://icons/notification-counter.png"),"A");
    headerLabels->setItem(0, 0, item1);
    QStandardItem *item2 = new QStandardItem(QIcon("://icons/notification-counter-02.png"),"B");
    headerLabels->setItem(1, 0, item2);
    QStandardItem *item3 = new QStandardItem(QIcon("://icons/notification-counter-03.png"),"C");
    headerLabels->setItem(2, 0, item3);
    QStandardItem *item4 = new QStandardItem(QIcon("://icons/notification-counter-04.png"),"");
    headerLabels->setItem(3, 0, item4);
    QStandardItem *item5 = new QStandardItem(QIcon("://icons/notification-counter-05.png"),"");
    headerLabels->setItem(4, 0, item5);
    QStandardItem *item6 = new QStandardItem(QIcon("://icons/notification-counter-06.png"),"");
    headerLabels->setItem(5, 0, item6);


    // Set up the default mode
    mode = ProgramModes::ANNOTATIONMODE;
	mimickLogFileAppearance = true;

    // Add 3 base columns
    gtTable->setRowCount(0);

    ui->setupUi(this);
    ui->tableView->setModel(gtTable);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    setTableHeaders();

    ui->eventTypeEditorView->setModel(headerLabels);
    ui->eventTypeEditorView->setVisible(false);
    ui->finishEditingButton->setVisible(false);

    // Configure the auto save timer
    autoSaveTimer.setParent(this);
    autoSaveTimer.setInterval(5000); // Run every five seconds
    connect(&autoSaveTimer, SIGNAL(timeout()), this, SLOT(autoSave()));

    // Configure shortcuts that communicate directly with MainWindow
    playbackShortcuts[PlaybtnShortcut::playPause] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_Space, this));
    playbackShortcuts[PlaybtnShortcut::prev5Frames] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_A, this));
    playbackShortcuts[PlaybtnShortcut::prevFrame] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_S, this));
    playbackShortcuts[PlaybtnShortcut::nextFrame] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_D, this));
    playbackShortcuts[PlaybtnShortcut::next5Frames] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_F, this));

	playbackShortcuts[PlaybtnShortcut::fiveSecPrev] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_Q, this));
	playbackShortcuts[PlaybtnShortcut::oneSecPrev] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_W, this));
	playbackShortcuts[PlaybtnShortcut::oneSecForw] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_E, this));
	playbackShortcuts[PlaybtnShortcut::fiveSecForw] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_R, this));


    QObject::connect(playbackShortcuts[PlaybtnShortcut::playPause].operator ->(),SIGNAL(activated()), parent, SLOT(on_StartPause_button_clicked()));
    QObject::connect(playbackShortcuts[PlaybtnShortcut::prev5Frames].operator ->(),SIGNAL(activated()), parent, SLOT(on_videoControl_Prev5Frames_clicked()));
    QObject::connect(playbackShortcuts[PlaybtnShortcut::prevFrame].operator ->(),SIGNAL(activated()), parent, SLOT(on_videoControl_PrevFrame_clicked()));
    QObject::connect(playbackShortcuts[PlaybtnShortcut::nextFrame].operator ->(),SIGNAL(activated()), parent, SLOT(on_videoControl_NextFrame_clicked()));
    QObject::connect(playbackShortcuts[PlaybtnShortcut::next5Frames].operator ->(),SIGNAL(activated()), parent, SLOT(on_videoControl_Next5Frames_clicked()));

	QObject::connect(playbackShortcuts[PlaybtnShortcut::fiveSecPrev].operator ->(), SIGNAL(activated()), parent, SLOT(on_videoControl_FiveSecPrev_clicked()));
	QObject::connect(playbackShortcuts[PlaybtnShortcut::oneSecPrev].operator ->(), SIGNAL(activated()), parent, SLOT(on_videoControl_OneSecPrev_clicked()));
	QObject::connect(playbackShortcuts[PlaybtnShortcut::oneSecForw].operator ->(), SIGNAL(activated()), parent, SLOT(on_videoControl_OneSecForw_clicked()));
	QObject::connect(playbackShortcuts[PlaybtnShortcut::fiveSecForw].operator ->(), SIGNAL(activated()), parent, SLOT(on_videoControl_FiveSecForw_clicked()));

    QObject::connect(this, SIGNAL(jumpToFrame(QDateTime)), parent, SLOT(jumpToFrame(QDateTime)));

    dataChanged = false;

	if (!initFrames.empty())
	{
		currentTime = initFrames[0].Timestamp;
		videoFilename.setFile(initFrames[0].Filename);
	}

    installEventFilter(this);
    initMasks(initFrames);



	QSettings settings;
	settings.beginGroup("logfiles");
	ui->addNewEntriesAtBottomCheckbox->setChecked(settings.value("addNewEntriesAtBottomChecked", "").toBool());
	settings.endGroup();	
}

void GroundTruthAnnotatorDialog::initMasks(const std::vector<ruba::FrameStruct>& initFrames)
{
    if (!initFrames.empty()) {
        initFrames[0].Image.cols;
        baseMask = cv::Mat::zeros(initFrames[0].Image.rows,initFrames[0].Image.cols, CV_8UC1);
    } else {
        baseMask = cv::Mat::zeros(0,0,CV_8UC1);
    }

    std::vector<ruba::MaskOverlayStruct> tmpVec;
    ruba::MaskOverlayStruct tmpMask;
    tmpMask.View = initFrames[0].viewInfo; // Only support one modality. TODO; Do this for all modalities
    tmpMask.DetectorType = -1;
    tmpMask.DetectorID = -1;
    tmpMask.Color = cv::Scalar(255,255,255);
    tmpMask.Alpha = 0.8;
    baseMask.copyTo(tmpMask.Mask);


    tmpVec.push_back(tmpMask);
    maskOverlays.push_back(tmpVec);

}

std::vector<std::vector<ruba::MaskOverlayStruct> > GroundTruthAnnotatorDialog::GetMasks()
{
    return maskOverlays;
}

QList<QString> GroundTruthAnnotatorDialog::getLatestTimestamps()
{
    return latestTimestamps;
}

void GroundTruthAnnotatorDialog::setTableHeaders()
{
    int numberNonEmptyHeaders = 0;

    for (int i = 0; i < headerLabels->rowCount(); ++i)
    {
        bool buttonEnabled = false;

        if (!(headerLabels->item(i)->text().isEmpty()))
        {
            ++numberNonEmptyHeaders;
            buttonEnabled = true;
        }
        QString tooltip = "Marks an entry to the log that a \"" + headerLabels->item(i)->text() + "\" is detected (" + QString::number(i+1) + ")";

        // TODO: Put pushbuttons in container. This is ugly
        switch(i)
        {
        case 0:
            ui->pushButton->setText(headerLabels->item(i)->text());
            ui->pushButton->setVisible(buttonEnabled);
            ui->pushButton->setShortcut(tr("1"));
            ui->pushButton->setToolTip(tooltip);
            break;
        case 1:
            ui->pushButton_2->setText(headerLabels->item(i)->text());
            ui->pushButton_2->setVisible(buttonEnabled);
            ui->pushButton_2->setShortcut(tr("2"));
            ui->pushButton_2->setToolTip(tooltip);
            break;
        case 2:
            ui->pushButton_3->setText(headerLabels->item(i)->text());
            ui->pushButton_3->setVisible(buttonEnabled);
            ui->pushButton_3->setShortcut(tr("3"));
            ui->pushButton_3->setToolTip(tooltip);
            break;
        case 3:
            ui->pushButton_4->setText(headerLabels->item(i)->text());
            ui->pushButton_4->setVisible(buttonEnabled);
            ui->pushButton_4->setShortcut(tr("4"));
            ui->pushButton_4->setToolTip(tooltip);
            break;
        case 4:
            ui->pushButton_5->setText(headerLabels->item(i)->text());
            ui->pushButton_5->setVisible(buttonEnabled);
            ui->pushButton_5->setShortcut(tr("5"));
            ui->pushButton_5->setToolTip(tooltip);
            break;
        case 5:
            ui->pushButton_6->setText(headerLabels->item(i)->text());
            ui->pushButton_6->setVisible(buttonEnabled);
            ui->pushButton_6->setShortcut(tr("6"));
            ui->pushButton_6->setToolTip(tooltip);
            break;
        }
    }

	int additionalLogFileHeaders = 0;

	if (mimickLogFileAppearance)
	{
		additionalLogFileHeaders = 2;
	}

    gtTable->setColumnCount(numberNonEmptyHeaders + additionalLogFileHeaders);

    QStringList stringLabels;

	if (mimickLogFileAppearance)
	{
		stringLabels.push_back("File");
		stringLabels.push_back("Date");
	}

    for (int i = 0; i < numberNonEmptyHeaders; ++i)
    {
        stringLabels.push_back(headerLabels->item(i)->text());

        // Set column width of table
        ui->tableView->setColumnWidth(i + additionalLogFileHeaders,75);
    }

    gtTable->setHorizontalHeaderLabels(stringLabels);
}

void GroundTruthAnnotatorDialog::checkFixHeaderLabels()
{
    // Goes through the headerLabels and ensures that there is no empty items between non-empty items,
    // i.e. transfers a list of
    // 1 - "Item 3"
    // 2 - "Item 1"
    // 3 - ""
    // 4 - "Item 4"
    //
    // Into
    // 1 - "Item 3"
    // 2 - "Item 1"
    // 3 - "Item 4"
    // 4 - ""

    // Go from the top of the list (2nd item) to the bottom (6th item)
    for (int i = 1; i < headerLabels->rowCount(); ++i)
    {
        if (!headerLabels->item(i)->text().isEmpty())
        {
            int prevEntryIndex = i-1;
            int newIndex = i;
            bool prevEntryIsEmpty = headerLabels->item(prevEntryIndex)->text().isEmpty();

            while (prevEntryIsEmpty)
            {
                newIndex = prevEntryIndex;
                prevEntryIndex--;

                if (prevEntryIndex > 0) {
                    prevEntryIsEmpty = headerLabels->item(prevEntryIndex)->text().isEmpty();
                } else {
                    prevEntryIsEmpty = false;
                }
            }

            if (newIndex != i)
            {
                // Move text
                headerLabels->item(newIndex)->setText(headerLabels->item(i)->text());
                headerLabels->item(i)->setText("");
            }
        }
    }

}

void GroundTruthAnnotatorDialog::updateFrame(std::vector<ruba::FrameStruct>& frames)
{
	if (!frames.empty())
	{
		currentTime = frames[0].Timestamp;
		videoFilename.setFile(frames[0].Filename);
	}
	

    if (mode == ProgramModes::VALIDATIONMODE)
    {
        // Try to find if any time stamp matches the current time. If so, highlight it
        findMatchingTimestamp(currentTime);
        if (maskOverlays[0][0].Alpha > 0)
        {
            maskOverlays[0][0].Alpha = maskOverlays[0][0].Alpha - 0.01; // Slowly decrement the alpha value
        }
    }
}

void GroundTruthAnnotatorDialog::findMatchingTimestamp(QDateTime time)
{
    QString currentTimestamp = time.toString("HH:mm:ss.zzz");
    int matchColumnPos = -1;
    int matchRowPos = -1;
    bool matchFound = false;
    QString timestamp = "";


    // Go through all columns of the table to find the timestamp, if possible
    for (int column = 0; column < gtTable->columnCount(); ++column)
    {
        int row = 0;

        // Go from the bottom of the list to the top. If we pass a timestamp that is greater than the current time,
        // break the loop - the timestamp we are searching for is not in this column
        while(row < gtTable->rowCount())
        {
			QString timestamp;

			if (gtTable->item(row, column) != nullptr)
			{
				timestamp = gtTable->item(row, column)->text();
			}

            if (timestamp == currentTimestamp)
            {
                matchRowPos = row;
                matchColumnPos = column;
                matchFound = true;
                break;
            } else {
                ++row;
            }
        }

        if (matchFound)
        {
            // Break out of for loop
            break;
        }
    }

    if (matchFound)
    {
        // Highlight item
        QModelIndex index = gtTable->index(matchRowPos, matchColumnPos);
        ui->tableView->setCurrentIndex(index);

        updateLatestTimestamps(timestamp, matchColumnPos, matchRowPos);

        // Set text + alpha on mask overlay
        maskOverlays[0][0].Alpha = 0.9; // Highest value
        QString maskOverlayText =  headerLabels->item(matchColumnPos)->text() +  " ("
                + QString::number(matchRowPos+1) + ")";

        cv::Point textOrg(300, 240);
        baseMask.copyTo(maskOverlays[0][0].Mask);
        cv::rectangle(maskOverlays[0][0].Mask,cv::Point(300-10,240-35),cv::Point(300+200,240+25),cv::Scalar(255,255,255),cv::FILLED);
        cv::putText(maskOverlays[0][0].Mask, maskOverlayText.toStdString(), textOrg, cv::FONT_HERSHEY_DUPLEX, 1, cv::Scalar::all(0), 2, 8);
    }

}

bool GroundTruthAnnotatorDialog::eventFilter(QObject* object, QEvent* event)
{
    // Some code thanks to http://www.qtforum.org/article/19166/can-t-stop-delete-key-on-qtableview.html

    if (event->type() != QEvent::KeyPress)
    {
        // default processing for non-keystrokes
        return false;
    }

        QKeyEvent* pKeyEvent = reinterpret_cast<QKeyEvent*>(event);
        qDebug() << "Hello from key event";
        switch (pKeyEvent->key())
        {
        case Qt::Key_Delete:
        {
          auto indexes = ui->tableView->selectionModel()->selection().indexes();

            if (ui->addNewEntriesAtBottomCheckbox->isChecked())
            {
                // Delete the entire row
                if (!indexes.empty())
                {
                    auto removedRow = gtTable->takeRow(indexes[0].row());

                    for (auto item : removedRow)
                    {
                        if (item != NULL)
                        {
                            delete item;
                        }
                    }

                    dataChanged = true;
                    gtTable->dataChanged(indexes.first(), indexes.last());
                }
            }
            else {
                // We have constrained the selectionMode to only select one item. Select that and pass the item for deletion
                if (!indexes.isEmpty())
                {
                    QModelIndex index = indexes[0];
                    deleteItemFromTable(index.row(), index.column());
                    
                    if (indexes[0].row() < gtTable->rowCount())
                    {
                        ui->tableView->setCurrentIndex(indexes[0]);
                    }
                    else {
                        // The previously selected index does not exist anymore.
                        // Select the row above
                        auto indexAbove = indexes[0].siblingAtRow(indexes[0].row() - 1);
                        
                        if (indexAbove.isValid())
                        {
                            ui->tableView->setCurrentIndex(indexAbove);
                        }
                    }

                    gtTable->dataChanged(index, index);
                }
                
            }

            event->accept();
            return true;
        }
        }

    qDebug() << "Passing event " << pKeyEvent->key();
    return QObject::eventFilter(object,event);
}

void GroundTruthAnnotatorDialog::deleteItemFromTable(int row, int column)
{
	int tableColumnPosition = column;
	
	if (mimickLogFileAppearance)
	{
		tableColumnPosition = column - 2;
	}

	if (gtTable->columnCount() > column)
	{
		if (gtTable->rowCount() > row)
		{
			QStandardItem* removedItem = gtTable->takeItem(row, column);

			if (removedItem == NULL)
			{
				// If the removed item is empty, we have nothing to do here
				return;
			}

			delete removedItem;

			moveItemsUpIfRequired(row, -1);

			dataChanged = true;
			removeEmptyRows();
		}
	}
}

GroundTruthAnnotatorDialog::~GroundTruthAnnotatorDialog()
{
	QSettings settings;
	settings.beginGroup("logfiles");
	settings.setValue("addNewEntriesAtBottomChecked", ui->addNewEntriesAtBottomCheckbox->isChecked());
	settings.endGroup();
	
	delete ui;
}

void GroundTruthAnnotatorDialog::updateLatestTimestamps(const QString& timestamp, int column, int row)
{
    QString timestampDescription = timestamp + " (" + QString::number(column+1) + "," +
            QString::number(row+1) + "): " + headerLabels->item(column)->text();
    latestTimestamps.push_back(timestampDescription);

    if (latestTimestamps.size() > 5)
    {
        latestTimestamps.pop_front();
    }

}

void GroundTruthAnnotatorDialog::insertTableEntry(QStandardItem& item, int column, int row)
{
	int tableColumnPosition = column;
	
	if (mimickLogFileAppearance)
	{
		tableColumnPosition = column + 2;
	}

	int newRowPosition = gtTable->rowCount();

    // Add new timestamp to the dedicated position
	if (row == newRowPosition)
	{
		// If we are assigning to the bottom of the row, things are easier
		// Check that the date and time of the entry mimicks the current row

		while (!checkApperanceFileDateInRow(item, newRowPosition))
		{
			newRowPosition++;
		}

		gtTable->setItem(newRowPosition, tableColumnPosition, &item);

		// Check if there exists at file and date at this row
		if (mimickLogFileAppearance)
		{
			if (gtTable->item(newRowPosition, 0) == nullptr ||
				gtTable->item(newRowPosition, 1) == nullptr)
			{
				QString file = qvariant_cast<QString>(item.data(CustomDataRoles::FileName));
				QString date = qvariant_cast<QString>(item.data(CustomDataRoles::TimestampDate));

				gtTable->setItem(newRowPosition, 0, new QStandardItem(file));
				gtTable->setItem(newRowPosition, 1, new QStandardItem(date));

				if (knownFileNames.find(file) == knownFileNames.end())
				{
					knownFileNames.insert(file);
				}

				if (knownDates.find(date) == knownDates.end())
				{
					knownDates.insert(date);
				}
			}
		}

    } else {
        // If not, things get a little messier. We will insert a new row and then try to move
		// entries in other columns afterwards
		QList<QStandardItem*> newRow;

		if (mimickLogFileAppearance)
		{
			QString file = qvariant_cast<QString>(item.data(CustomDataRoles::FileName));
			QString date = qvariant_cast<QString>(item.data(CustomDataRoles::TimestampDate));


			newRow.append(new QStandardItem(file));
			newRow.append(new QStandardItem(date));
		}

		// Add empty entries (for now)
		for (int i = 0; i < column; ++i)
		{
			newRow.append(new QStandardItem(""));
		}

		// And add the new item
		newRow.append(&item);

		int columnCount = gtTable->columnCount();

		if (mimickLogFileAppearance)
		{
			columnCount = columnCount > 2 ? columnCount - 2 : 0;
		}

		// Add the remaining columns
		for (int i = column + 1; i < columnCount; ++i)
		{
			newRow.append(new QStandardItem(""));
		}

		gtTable->insertRow(row, newRow);

		moveItemsUpIfRequired(row, column);
    }
	
	removeEmptyRows();
	

    // Update the selected item in the table view
    QModelIndex index = gtTable->index(row, tableColumnPosition);
    ui->tableView->setCurrentIndex(index);

    dataChanged = true;
    updateLatestTimestamps(item.text(), column, row);
}

void GroundTruthAnnotatorDialog::moveItemsUpIfRequired(int startRow, int excludedColumn)
{
	int columnCount = gtTable->columnCount();

	if (mimickLogFileAppearance)
	{
		columnCount = columnCount > 2 ? columnCount - 2 : 0;
	}

	// Then try to move items in other columns
	for (int i = 0; i < columnCount; ++i)
	{
		// Go through every column

		// Traverse from the inserted row and go down until we cannot move
		// any more entries
		if (i != excludedColumn)
		{
			for (int j = startRow; j < gtTable->rowCount(); j++)
			{
				int columnPos = i;

				if (mimickLogFileAppearance)
				{
					columnPos = columnPos + 2;
				}

				if (mimickLogFileAppearance)
				{
					if (gtTable->item(j, 0) != nullptr &&
						gtTable->item(j, 1) != nullptr &&
						gtTable->item(j + 1, 0) != nullptr &&
						gtTable->item(j + 1, 1) != nullptr)
					{
						// Check for file and date info
						QString fileCurrentRow = qvariant_cast<QString>(gtTable->item(j, 0)->text());
						QString dateCurrentRow = qvariant_cast<QString>(gtTable->item(j, 1)->text());

						QString fileNextRow = qvariant_cast<QString>(gtTable->item(j + 1, 0)->text());
						QString dateNextRow = qvariant_cast<QString>(gtTable->item(j + 1, 1)->text());

						if (fileCurrentRow != fileNextRow ||
							dateCurrentRow != dateNextRow)
						{
							break;
						}
					}
				}

				if (gtTable->item(j + 1, columnPos) == nullptr)
				{
					break;
				}

				if (gtTable->item(j, columnPos) != nullptr && !gtTable->item(j, columnPos)->text().isEmpty())
				{
					// If we were to copy the next item in the row onto this position, 
					// we would effectively delete the current entry. Thus continue
					qDebug() << gtTable->item(j, columnPos)->text();
					continue;
				}


				// We are safe to continue with the next row
				QStandardItem* movedItem = gtTable->takeItem(j + 1, columnPos);

				if (movedItem == nullptr)
				{
					break;
				}

				gtTable->setItem(j, columnPos, movedItem);
			}
		}
	}
}

void GroundTruthAnnotatorDialog::removeEmptyRows()
{
	if (mimickLogFileAppearance)
	{
		// See if there are completely empty rows

		for (auto j = 0; j < gtTable->rowCount(); ++j)
		{
			int emptyRowCount = 0;

			for (auto i = 2; i < gtTable->columnCount(); ++i)
			{
				if (gtTable->item(j, i) == nullptr)
				{
					emptyRowCount++;
					continue;
				}

				if (gtTable->item(j, i)->text().isEmpty())
				{
					emptyRowCount++;
				}
			}

			if (emptyRowCount == gtTable->columnCount() - 2)
			{
				// Completely empty - remove row
				auto row = gtTable->takeRow(j);

                for (auto item : row)
                {
                    if (item != NULL)
                    {
                        delete item;
                    }
                }

				break;
			}
		}
	}
}

void GroundTruthAnnotatorDialog::newTableEntry(int column)
{
    // Fetch the current timestamp
    QStandardItem *timestamp = new QStandardItem(currentTime.toString("HH:mm:ss.zzz"));

	QString fileName = videoFilename.fileName();
	timestamp->setData(fileName, CustomDataRoles::FileName);

	QString date = currentTime.toString("yyyy MM dd");
	timestamp->setData(date, CustomDataRoles::TimestampDate);


    // Find out where the timestamp should be positioned. If the user have not rewinded the video,
    // place the item at the end of the list. Otherwise, we need to loop through the other timestamps

    if (gtTable->rowCount() == 0)
    {
        insertTableEntry(*timestamp, column, 0);
        return;
    }

	int tableColumnPosition = column;

	if (mimickLogFileAppearance)
	{
		tableColumnPosition = column + 2;
	}

	int itemPos = gtTable->rowCount();
	QStandardItem *prevItem = gtTable->item(itemPos - 1, tableColumnPosition);

	if (ui->addNewEntriesAtBottomCheckbox->isChecked() == false)
	{
		// Run as long as the previous item in the list has a timestamp greater than the current time
		bool runThroughList = true;

		if (mimickLogFileAppearance)
		{
			if (knownFileNames.find(fileName) == knownFileNames.end() ||
				knownDates.find(date) == knownDates.end())
			{
				// The file name or date are new to the list. Therefore, we should add this entry
				// to the very bottom of the list
				runThroughList = false;
			}
		}

		if (runThroughList)
		{
			bool positionFound = false;

			while (itemPos > 0 && !positionFound)
			{
				QString prevTimestamp;

				if (prevItem != nullptr)
				{
					prevTimestamp = prevItem->text();
				}
				
				if (prevTimestamp <= timestamp->text() && !prevTimestamp.isEmpty())
				{
					if (mimickLogFileAppearance)
					{
						if (gtTable->item(itemPos - 1, 0) != nullptr &&
							gtTable->item(itemPos - 1, 1) != nullptr)
						{
							QString filePrevRow = qvariant_cast<QString>(gtTable->item(itemPos - 1, 0)->text());
							QString datePrevRow = qvariant_cast<QString>(gtTable->item(itemPos - 1, 1)->text());

							if (filePrevRow == fileName &&
								datePrevRow == date)
							{
								positionFound = true;
								break;
							}
						}
					}
					else {
						positionFound = true;
						break;
					}	
				}

				if (mimickLogFileAppearance)
				{				
					if (gtTable->item(itemPos - 1, 0) != nullptr &&
						gtTable->item(itemPos - 1, 1) != nullptr &&
						gtTable->item(itemPos, 0) != nullptr &&
						gtTable->item(itemPos, 1) != nullptr)
					{
						QString filePrevRow = qvariant_cast<QString>(gtTable->item(itemPos-1, 0)->text());
						QString datePrevRow = qvariant_cast<QString>(gtTable->item(itemPos-1, 1)->text());

						QString fileCurrentRow = qvariant_cast<QString>(gtTable->item(itemPos, 0)->text());
						QString dateCurrentRow = qvariant_cast<QString>(gtTable->item(itemPos, 1)->text());

						if ((fileCurrentRow == fileName && dateCurrentRow == date) &&
							(filePrevRow != fileName || datePrevRow != date))
						{
							// If the file and date fits with the current row and 
							// we have another file and date above, just stay here
							// and insert a row
							positionFound = true;
							break;
						}
					}
				}

				itemPos--;
				prevItem = gtTable->item(itemPos - 1, tableColumnPosition);
			}
		}
	}

    insertTableEntry(*timestamp, column, itemPos);

}

bool GroundTruthAnnotatorDialog::checkApperanceFileDateInRow(QStandardItem & item, int row)
{
	if (!mimickLogFileAppearance)
	{
		// We live in the old system - always fits
		return true;
	}
	
	// First, check if the item contains the entries "File" and "Date" to check up against
	// If not, we live in the old system yet
	QString file = qvariant_cast<QString>(item.data(CustomDataRoles::FileName));
	QString date = qvariant_cast<QString>(item.data(CustomDataRoles::TimestampDate));

	if (file.isEmpty() || date.isEmpty())
	{
		return true;
	}

	// If we are still here, we have to check if the file and date of the item
	// is comparable to the file and date of the particular row
	if ((gtTable->item(row, 0) == nullptr) ||
		(gtTable->item(row, 1) == nullptr))
	{
		return true;
	}

	if ((gtTable->item(row, 0)->text() != file) ||
		(gtTable->item(row, 1)->text() != date))
	{
		return false;
	}

	// If the file and date fits, we should be here by now. Return true
	return true;

}

void GroundTruthAnnotatorDialog::on_pushButton_clicked()
{
    newTableEntry(0);
}

void GroundTruthAnnotatorDialog::on_pushButton_2_clicked()
{
    newTableEntry(1);
}

void GroundTruthAnnotatorDialog::on_pushButton_3_clicked()
{
    newTableEntry(2);
}

void GroundTruthAnnotatorDialog::on_pushButton_4_clicked()
{
    newTableEntry(3);

}

void GroundTruthAnnotatorDialog::on_pushButton_5_clicked()
{
    newTableEntry(4);
}

void GroundTruthAnnotatorDialog::on_pushButton_6_clicked()
{
    newTableEntry(5);
}

int GroundTruthAnnotatorDialog::openLog(QString filename)
{
    if(!filename.isEmpty() && filename.contains(".csv"))
    {
        QFile file(filename);

        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			knownFileNames.clear();
			knownDates.clear();

            QTextStream fileStr(&file);

            // Read header of .csv-file
            QString line = fileStr.readLine();
            QStringList headers = line.split(';');

			if (headers.last().isEmpty())
			{
				// Remove the last column if description is empty
				headers.pop_back();
			}

            if (headers.size() == 0) {
                qDebug() << "Log headers of " << filename << " are empty";
                return 1;
            } 

			int maxHeaderSize = 6;
			int headerTimestampStartIndex = 0;
			mimickLogFileAppearance = false;

			if (headers[0] == "File" && headers[1] == "Date")
			{
				mimickLogFileAppearance = true;
				headerTimestampStartIndex = 2;
			}
			
			if (headers.size() > maxHeaderSize) {
                qDebug() << "Header count is larger than 6. Truncating.";
            }

            int headerSize = (std::min(headers.size() - headerTimestampStartIndex, maxHeaderSize));
            for (int i = headerTimestampStartIndex; i < headerSize + headerTimestampStartIndex; ++i)
            {
                // Add headers to headerLabels which is accessible from the GUI eventTypeEditorView
                headerLabels->item(i - headerTimestampStartIndex)->setText(headers[i]);
            }

            checkFixHeaderLabels();
            setTableHeaders();

            // Clear table entries, if any
			gtTable->setRowCount(0);

            QStringList gtList;
			int currentFileRow = 0;

            // Read the rest of the log file
            while (!fileStr.atEnd()) {
                line = fileStr.readLine();
                gtList = (line.split(';'));

				QString fileName;
				QString date;

				if (mimickLogFileAppearance && gtList.size() > 2)
				{
					fileName = gtList[0];
					date = gtList[1];

					if (knownFileNames.find(fileName) == knownFileNames.end() ||
						knownDates.find(date) == knownFileNames.end())
					{
						knownFileNames.insert(fileName);
						knownDates.insert(date);
					}
				}

                // Insert the entries in the table
                for (int i = 0; i < std::min(gtList.size(),headerSize + headerTimestampStartIndex); ++i)
                {
                    if (!gtList[i].isEmpty()) {
                        QStandardItem *item = new QStandardItem(gtList[i]);

						if (mimickLogFileAppearance)
						{
							item->setData(fileName, CustomDataRoles::FileName);
							item->setData(date, CustomDataRoles::TimestampDate);
						}
						gtTable->setItem(currentFileRow, i, item);						
                    }
                }

				currentFileRow++;
            }


        } else {
            qDebug() << file.errorString();
            return 1;
        }
    }
    return 0;
}

void GroundTruthAnnotatorDialog::saveLog()
{
    // Overwrites the existing log file and replaces it with the new log table

    QString savePath = ui->savePath->text();
    QFile file(savePath);

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        // Write header of .csv-file
		if (mimickLogFileAppearance)
		{
			stream << "File;Date;";
		}

        for (int i = 0; i < headerLabels->rowCount(); ++i)
        {
			if (!headerLabels->item(i)->text().isEmpty())
			{
				stream << headerLabels->item(i)->text() << ";";
			}
        }
        stream << endl;


        for(int i = 0; i < gtTable->rowCount(); ++i)
        {
            for (int j = 0; j < gtTable->columnCount(); ++j)
            {
                QStandardItem* item = gtTable->item(i, j);
                QString itemText = "";

                if (item != NULL)
                {
                    itemText = item->text();
                }

                stream << itemText + ";";
            }
            stream << endl;
        }

        stream.flush();
        file.close();
    }

    ui->tableView->setFocus();
}

void GroundTruthAnnotatorDialog::on_openLogButton_clicked()
{
    if (dataChanged)
    {
        int reply = QMessageBox::question(this,"Save current log","Do you want to save the current log?\nIf not, changes to the current log will be lost.",
                              QMessageBox::Yes|QMessageBox::No);

        if (reply==QMessageBox::Yes) {
            if (ui->savePath->text().isEmpty()) {
                on_saveLogAsButton_clicked();
            } else {
                saveLog();
            }
        }
    }

	QSettings settings;
	settings.beginGroup("logfiles");
	QString logFileDir = settings.value("logFileDir", "").toString();
	settings.endGroup();

    QString filename = QFileDialog::getOpenFileName(this, tr("Open existing log file"), logFileDir, tr("Log files (*.csv)"));

    int returnCode = openLog(filename);

    if (returnCode == 0) {
		QSettings settings;
		QString logFileDir = QFileInfo(filename).absolutePath();
		settings.beginGroup("logfiles");
		settings.setValue("logFileDir", logFileDir);
		settings.endGroup();
		
        ui->savePath->setText(filename);
        ui->checkBox->setEnabled(true); // Enable the auto-save check box
        ui->saveLogButton->setEnabled(true);
    }
}


void GroundTruthAnnotatorDialog::on_saveLogButton_clicked()
{
    if (ui->savePath->text().isEmpty()) {
        on_saveLogAsButton_clicked();
    } else {
        saveLog();
    }
}

void GroundTruthAnnotatorDialog::on_saveLogAsButton_clicked()
{
	QSettings settings;
	settings.beginGroup("logfiles");
	QString logFileDir = settings.value("logFileDir", "").toString();
	settings.endGroup();

    QString filename = QFileDialog::getSaveFileName(this, tr("Save log file as"), logFileDir, tr("Log files (*.csv)"));

    if (!filename.isEmpty())
    {
		QSettings settings;
		QString logFileDir = QFileInfo(filename).absolutePath();
		settings.beginGroup("logfiles");
		settings.setValue("logFileDir", logFileDir);
		settings.endGroup();

        ui->savePath->setText(filename);
        ui->checkBox->setEnabled(true); // Enable the auto-save check box
        ui->saveLogButton->setEnabled(true);

        saveLog();
    }

    ui->tableView->setFocus();
}


void GroundTruthAnnotatorDialog::autoSave()
{
    if (dataChanged)
    {
        qDebug() << "AutoSave is saving...";
        dataChanged = false;
        saveLog();
    } else {
        qDebug() << "AutoSave has skipped...";
    }
}

void GroundTruthAnnotatorDialog::on_checkBox_clicked(bool checked)
{
    if (checked)
    {
        autoSaveTimer.start();
    } else
    {
        autoSaveTimer.stop();
    }
}

void GroundTruthAnnotatorDialog::on_enableValidationModeCheckBox_clicked(bool checked)
{
    if (checked)
    {
        mode = ProgramModes::VALIDATIONMODE;
    }
    else {
        mode = ProgramModes::ANNOTATIONMODE;
    }
}

void GroundTruthAnnotatorDialog::on_tableView_doubleClicked(const QModelIndex & index)
{
    // Get the timestamp currently selected
    QTime tableEntryTime = QTime::fromString(gtTable->item(index.row(), index.column())->text(),
        "HH:mm:ss.zzz");

    // Get the date information from the current time and get the combined datetime
    QDateTime tableEntryDateTime = currentTime;
    tableEntryDateTime.setTime(tableEntryTime);

    // Notify the mainWindow to jump to the particular datetime
    emit jumpToFrame(tableEntryDateTime);
}

void GroundTruthAnnotatorDialog::on_addNewEntriesAtBottomCheckbox_clicked(bool checked)
{
	
	if (checked)
	{
		ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
	}
	else {
		ui->tableView->setSelectionBehavior(QAbstractItemView::SelectItems);
	}
}

void GroundTruthAnnotatorDialog::on_editEventTypeButton_clicked()
{
    enableEditingMode(true);
}

void GroundTruthAnnotatorDialog::enableEditingMode(bool state)
{
    ui->eventTypeEditorView->setVisible(state);
    ui->editEventTypeButton->setVisible(!state);
    ui->finishEditingButton->setVisible(state);

    ui->pushButton->setDisabled(state);
    ui->pushButton_2->setDisabled(state);
    ui->pushButton_3->setDisabled(state);
    ui->pushButton_4->setDisabled(state);
    ui->pushButton_5->setDisabled(state);
    ui->pushButton_6->setDisabled(state);
    ui->tableView->setDisabled(state);

    if (state)
    {
        ui->eventTypeVerticalSpacer->changeSize(10, 10, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        ui->verticalLayout->invalidate();
    }
    else {
        ui->eventTypeVerticalSpacer->changeSize(0, 0, QSizePolicy::Preferred, QSizePolicy::Maximum);
        ui->verticalLayout->invalidate();
    }
}

void GroundTruthAnnotatorDialog::on_finishEditingButton_clicked()
{
    enableEditingMode(false);
    checkFixHeaderLabels();
    setTableHeaders();
}
