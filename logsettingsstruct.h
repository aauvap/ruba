#ifndef LOGSETTINGSSTRUCT_H
#define LOGSETTINGSSTRUCT_H

#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "ruba.h"

struct ruba::LogSettingsStruct{
    std::vector<QString> Filepath;
    bool EnableEveryEventLog;
    bool EnableSumOfEventsLog;
    bool SaveFrameOfEvent;
    QString SaveFrameOfEventPath;

    std::vector<int> SumInterval;
};


#endif // LOGSETTINGSSTRUCT_H
