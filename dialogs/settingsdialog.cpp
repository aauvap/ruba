#include "settingsdialog.h"
#include "settingspages.h"

SettingsDialog::SettingsDialog(QWidget* parent, int devState)
    : QDialog(parent)
{
    contentsWidget = new QListWidget();
    contentsWidget->setViewMode(QListView::IconMode);
    contentsWidget->setIconSize(QSize(32, 32));
    contentsWidget->setMovement(QListView::Static);
    contentsWidget->setMaximumHeight(80);
    contentsWidget->setSpacing(10);

    pagesWidget = new QStackedWidget();
    
    // Add here
    AbstractSettingsPage *generalPage = new GeneralPage();
    AbstractSettingsPage *synchronisationPage = new SynchronisationPage();
    AbstractSettingsPage *processingPage = new ProcessingPage();
	AbstractSettingsPage *recordingPage = new RecordingPage();

    pagesWidget->addWidget(generalPage);
    pages.push_back(generalPage);
    pagesWidget->addWidget(synchronisationPage);
    pages.push_back(synchronisationPage);
    pagesWidget->addWidget(processingPage);
    pages.push_back(processingPage);
	pagesWidget->addWidget(recordingPage);
	pages.push_back(recordingPage);

	this->devState = devState;
    if (devState == RUBA_DEVSTATE)
    {
        // Developer version - show debug pane
        AbstractSettingsPage *debugPage = new DebugPage();
        pagesWidget->addWidget(debugPage);
        pages.push_back(debugPage);
    }
	
    QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
        | QDialogButtonBox::Cancel);

    createIcons();
    contentsWidget->setCurrentRow(0);

    connect(buttonBox, SIGNAL(rejected()), this, SLOT(close()));
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(saveSettingsAndClose()));
    

    QVBoxLayout* verticalLayout = new QVBoxLayout;
    verticalLayout->addWidget(contentsWidget);
    verticalLayout->addWidget(pagesWidget, 1);

    QHBoxLayout* buttonsLayout = new QHBoxLayout;
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(buttonBox);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(verticalLayout);
    mainLayout->addStretch(1);
    mainLayout->addSpacing(12);
    mainLayout->addLayout(buttonsLayout);
    setLayout(mainLayout);

    setWindowTitle(tr("Settings"));
}

void SettingsDialog::createIcons()
{
    QListWidgetItem* generalButton = new QListWidgetItem(contentsWidget);
    generalButton->setIcon(QIcon(":/icons/application-32.png"));
    generalButton->setText(tr("General"));
    generalButton->setTextAlignment(Qt::AlignHCenter);
    generalButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    QListWidgetItem* synchronisationButton = new QListWidgetItem(contentsWidget);
    synchronisationButton->setIcon(QIcon(":/icons/clockBig.png"));
    synchronisationButton->setText(tr("Synchronisation"));
    synchronisationButton->setTextAlignment(Qt::AlignHCenter);
    synchronisationButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    QListWidgetItem* processingButton = new QListWidgetItem(contentsWidget);
    processingButton->setIcon(QIcon(":/icons/monitor-sidebar.png"));
    processingButton->setText(tr("Processing"));
    processingButton->setTextAlignment(Qt::AlignHCenter);
    processingButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

	QListWidgetItem* recordingButton = new QListWidgetItem(contentsWidget);
	recordingButton->setIcon(QIcon(":/icons/film-32.png"));
	recordingButton->setText(tr("Recording"));
	recordingButton->setTextAlignment(Qt::AlignHCenter);
	recordingButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

    if (devState == RUBA_DEVSTATE)
    {
        QListWidgetItem* debugButton = new QListWidgetItem(contentsWidget);
        debugButton->setIcon(QIcon(":/icons/exclamation.png"));
        debugButton->setText(tr("Debug"));
        debugButton->setTextAlignment(Qt::AlignHCenter);
        debugButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    }

    connect(contentsWidget,
        SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
        this, SLOT(changePage(QListWidgetItem*, QListWidgetItem*)));
}

void SettingsDialog::changePage(QListWidgetItem* current, QListWidgetItem* previous)
{
    if (!current)
    {
        current = previous;
    }

    pagesWidget->setCurrentIndex(contentsWidget->row(current));
}

void SettingsDialog::saveSettingsAndClose()
{
    for (int i = 0; i < pages.size(); ++i)
    {
        pages[i]->saveSettings();
    }

    // Close the widget
    close();
}