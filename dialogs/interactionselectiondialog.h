#ifndef INTERACTIONSELECTIONDIALOG_H
#define INTERACTIONSELECTIONDIALOG_H



#include <opencv2/opencv.hpp>

#include <QDebug>
#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>
#include <QSettings>


#include "addsettingsstruct.h"
#include "detectoroutputstruct.h"
#include "utils.hpp"
#include "ruba.h"




namespace Ui {
class interactionselectiondialog;
}

class interactionselectiondialog : public QDialog
{
    Q_OBJECT

public:
    explicit interactionselectiondialog(QWidget *parent, std::vector<ruba::DetectorOutput> detectorInfo,
                                        QString windowTitle, QIcon windowIcon);
    ~interactionselectiondialog();

    ruba::AdditionalSettingsStruct getAdditionalSettings();
    int loadConfiguration(QString filename = QString());
    int saveConfiguration(QString filename);

private slots:
    void on_choose312Button_clicked(bool checked);

    void on_choose410Button_clicked(bool checked);

    void on_logFileSavePathButton1_clicked();

    void on_logFileSavePathButton2_clicked();

    void on_logEveryCheckBox1_toggled(bool checked);

    void on_logSumCheckBox_toggled(bool checked);

    void on_saveFrameEventCheckBox_toggled(bool checked);

    void on_logFileSavePathLine1_textChanged(const QString &arg1);

    void on_logFileSavePathLine2_textChanged(const QString &arg1);

    void on_saveFrameEventPathLine_textChanged(const QString &arg1);

    void on_saveFrameEventPathButton_clicked();

    void on_saveConfigButton_clicked();

    void on_saveConfigAsButton_clicked();

    void on_loadConfigButton_clicked();

    void on_moduleBox1_currentIndexChanged(int index);

    void on_moduleBox2_currentIndexChanged(int index);

private:
    void checkSetUpLayout();
    void checkConfiguration();

    ruba::AdditionalSettingsStruct addSettings;


    Ui::interactionselectiondialog *ui;
    std::vector<ruba::DetectorOutput> detectorInfo;
    int detectorID;
    int detectorType;
    std::vector<int> requestedDetectorIDs;

    QString basePrefix;

    QVector<int> rightTurningCarID, leftTurningCarID, straightGoingCyclistsID;


};

#endif // INTERACTIONSELECTIONDIALOG_H
