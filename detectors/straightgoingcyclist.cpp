#include <cmath>
#include "straightgoingcyclist.h"
#include "opencvviewer.h"
#include "presencedetector.h"
#include "movementdetector.h"

using namespace std;
using namespace cv;

using namespace ruba;

StraightGoingCyclist::StraightGoingCyclist(BaseDetectorParams baseParams, QWidget *parent, QString configFile)
    : AbstractCompoundDetector(configureBaseDetectorParams(baseParams, STRAIGHTGOINGCYCLIST))
{
    // In order to identify right turning cars, we need 3 PresenceDetectors and 1 MovementDetector
    setDetectorCount(3, 1, 0, 0);

    windowIcon = new QIcon("://icons/arrow-090.png");
    designImg.load("://illustrations/straightgoingcyclists.png");

    LoadConfiguration(baseParams.initFrames, parent, configFile, false);

}

DetectorOutput StraightGoingCyclist::GetOutput()
{
    DetectorOutput outputStruct;

    if (this->IsInitialized)
    {
        outputStruct.Confidence.push_back(1);
    } else {
        outputStruct.Confidence.push_back(0);
    }

    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = IsTriggered;
    outputStruct.EnteringConflictZone = EnteringConflictZone;
    outputStruct.LeavingConflictZone = LeavingConflictZone;

    if (EnteringConflictZone > LeavingConflictZone)
    {
        outputStruct.LastChange.push_back(EnteringConflictZone);
    } else {
        outputStruct.LastChange.push_back(LeavingConflictZone);
    }

    outputStruct.Timestamp = getCurrentTime();


    return outputStruct;

}

void StraightGoingCyclist::setDetectorSpecificDefaultParameters()
{
    InitLogSettings generalSettings = getGeneralLogSettings();

    CustomLogHeader header;
    header.additionalTransitionText[Transition::ENTERING] = " CZ";
    header.additionalTransitionText[Transition::LEAVING] = " CZ";
    generalSettings.customLogHeader = { header }; // Entering CZ, Leaving CZ

    QDateTime currentTime = getCurrentTime();
    disableAllDetectorsAt = currentTime.addYears(1);
    size_t nbrModalities = frames.size();


    // Reconfigure aspects of the generic configuration
    for (size_t i = 0; i < detectorParams.size(); ++i)
    {
        if (detectorParams[i].DetectorType == PRESENCEDETECTOR)
        {
            detectorParams[i].DisableDetectorAfter = 1500; // Wait for 1500 ms before closing the E1 and E2
        }
        else if (detectorParams[i].DetectorType == PRESENCEDETECTOR && ((int)i == 2))
        {
            detectorParams[i].DisableDetectorAfter = 500; // Wait for 300 ms before closing the E3
        }
        else
        {        
            detectorParams[i].DisableDetectorAfter = 2000;           
        }

        if (detectorParams[i].DetectorType == MOVEMENTDETECTOR)
        {
            detectorParams[i].DisableDetectorDuration = 1500; // Wait for 1500 ms before closing the F1

            // Reconfigure the minimum vector length threshold
            detectorParams[i].MinVecLengthThreshold.clear();

            for (size_t j = 0; j < nbrModalities; ++j)
            {
                vector<double> tmpVector;
                tmpVector.push_back(0.8);
                tmpVector.push_back(0.8);
                detectorParams[i].MinVecLengthThreshold.push_back(tmpVector);
            }

        }

        if (detectorParams[i].DetectorType == MOVEMENTDETECTOR && ((int)i == 3))
        {
            detectorParams[i].EnableWrongDirection = true; // Enable wrong direction support for the movement detector
        }

        if (detectorParams[i].DetectorType == STATIONARYDETECTOR)
        {
            detectorParams[i].DisableDetectorAfter = 1500;
        }

    }

    generalSettings.entrySettings.maxTriggeredDuration.clear(); // Feature not supported


    setGeneralLogSettings(generalSettings);

    // Set flags of the StraightGoingCyclist detector
    IsTriggered.resize(1);
    IsTriggered[0] = false;
    EnteringConflictZone = QDateTime();
    LeavingConflictZone = QDateTime();
}


void StraightGoingCyclist::setDetectorSpecificEnabledStates()
{
    detectorInfo[0].IsEnabled = true; // The PresenceDetector E1 is always enabled
}

void StraightGoingCyclist::updateDetectorSpecificLogic()
{
    QDateTime currentTime = getCurrentTime();
	bool IsWrongDirectionTriggered = false;

    for (auto detector : detectors)
    {
		if (detector->getDetectorType() == MOVEMENTDETECTOR)
		{
			DetectorOutput fOutput = detector->GetOutput();

			if (fOutput.Confidence.size() > 2)

			if (fOutput.Confidence[2] >= 1.0)
			{
				IsWrongDirectionTriggered = true;
			}
			else if (fOutput.IsTriggered[2] &&
				detector->GetTriggeredHistory()[2].count(true) >= 3)
			{
				IsWrongDirectionTriggered = true;
			}
			else
			{
				IsWrongDirectionTriggered = false;
			}
		}
    }

    /****** Update the internal logic of the StraigtGoingCyclist detector:  ******/

    /***** Step 1: Based on the internal states of the detectors, set enable/disable flags of the detectors *****/

    // If PresenceDetector E1 is triggered, enable PresenceDetector E2
    if (detectorInfo[0].IsTriggered == true)
    {
        detectorInfo[1].IsEnabled = true; // PresenceDetector E2

        // Automatically disable detectors after a fixed amount of time
        detectorInfo[1].DisableDetectorAt = currentTime.addMSecs(detectorInfo[1].DisableDetectorAfter);
    }

    // If PresenceDetector E2 is triggered, enable MovementDetector F1
    if (detectorInfo[1].IsTriggered == true)
    {
        detectorInfo[3].IsEnabled = true; // MovementDetector F1

        // Automatically disable detectors after a fixed amount of time
        detectorInfo[3].DisableDetectorAt = currentTime.addMSecs(detectorInfo[3].DisableDetectorAfter);
    }

    // If MovementDetector F1 is triggered, enable PresenceDetector E3
    if (detectorInfo[3].IsTriggered == true)
    {
        detectorInfo[2].IsEnabled = true;

        // Automatically disable detectors after a fixed amount of time
        detectorInfo[2].DisableDetectorAt = currentTime.addMSecs(detectorInfo[2].DisableDetectorAfter);
    }

    // If MovementDetector F1 is triggered in the wrong direction, disable PresenceDetector E3 for x seconds
    if (detectorInfo[3].IsWrongDirectionTriggered == true)
    {
        detectorInfo[2].DisableDetectorUntil = currentTime.addMSecs(detectorInfo[2].DisableDetectorDuration);
    }

    /**** Step 2: Disable the detectors accordingly to the flags *****/
    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        if ((detectorInfo[i].DisableDetectorAt <= currentTime) && (detectorInfo[i].IsTriggered == false))
        {
            detectorInfo[i].IsEnabled = false;
        }
    }

    if ((disableAllDetectorsAt <= currentTime))
    {
        for (size_t i = 1; i < detectorInfo.size(); ++i)
        {
            detectorInfo[i].IsEnabled = false;
        }
    }

    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        if (detectorInfo[i].DisableDetectorUntil > currentTime)
        {
            detectorInfo[i].IsEnabled = false;
        }
    }

    /***** Step 3: Update the TrafficDetector and set the flags that may be accessed by the outside *****/

    this->IsTriggered[0] = false;

    if (detectorInfo[2].IsTriggered == true)
    {
        this->IsTriggered[0] = true;

        // Check if the state has changed
        if (getPreviousDetectorInfo()[2].IsTriggered == false)
        {
            // We have a new, unique event. A straight going cyclist has just entered the conflict zone
            this->EnteringConflictZone = currentTime;
            generalLog->updateLog(getCurrentTime(), frames, Transition::ENTERING,
                detectorInfo[2].getDetectorIdText(),
                maskOutlines[detectorInfo[2].getDetectorId()]);
        }
    } else if (detectorInfo[2].IsTriggered == false)
    {
        // Check if the state has changed
        if (getPreviousDetectorInfo()[2].IsTriggered == true)
        {
            // If it has, a straight going cyclist has just left the conflict zone
            this->LeavingConflictZone = currentTime;
            generalLog->updateLog(getCurrentTime(), frames, Transition::LEAVING,
                detectorInfo[2].getDetectorIdText(),
                maskOutlines[detectorInfo[2].getDetectorId()]);
        }
    }
}


QList<QString> StraightGoingCyclist::getDetectorSpecificDebugInfo()
{
    QList<QString> debugList;

    debugList.push_back("Entering CZ: " + this->EnteringConflictZone.toString("hh:mm:ss:zzz"));
    debugList.push_back("Leaving CZ: " + this->LeavingConflictZone.toString("hh:mm:ss:zzz"));


    return debugList;
}

QList<QString> StraightGoingCyclist::getDetectorSpecificExtendedDebugInfo()
{
    QList<QString> extDebugList;

    return extDebugList;
}