#ifndef SCROLLINGCHART_H
#define SCROLLINGCHART_H

#include <opencv2/opencv.hpp>
#include <QQueue>
#include <QString>
#include "ruba.h"
#include "viewinfo.h"

class ScrollingChart
{
public:
    ScrollingChart(int width = 300, int height = 300, ViewInfo view = ViewInfo());
    void AddDataPoint(int datapointY);
    cv::Mat GetChartImage();
    QString GetTitle(void);
    ViewInfo GetView();
    double GetThreshold();
    void SetTitle(QString title);
    void SetThreshold(double threshold);
    void SetBackground(cv::Scalar color);
    cv::Scalar GetBackground();
    void SetVisualLineThresholds(int l0, int l1);

    void SetLineUnder(cv::Scalar color);
    void SetLineOver(cv::Scalar color);
    int GetDataPoint();

private:
    cv::Scalar BackgroundColor;
    cv::Scalar LineColorUnder;
    cv::Scalar LineColorOver;
    int ChartHeight;
    int ChartWidth;
    QQueue<int> DataQueue;
    cv::Mat *ChartImage;
    QString Title;
    double ColorThreshold;
    int l0, l1;
    int currentLineState;
    ViewInfo view;
};



class DoubleScrollingChart
{
public:
    DoubleScrollingChart(int width, int height);
    void AddDataPoints(int datapointY1, int datapointY2);
    cv::Mat GetChartImage();
    QString GetTitle(void);
    void SetTitle(QString title);
    void SetThresholds(double threshold1, double threshold2);
    void SetBackground(cv::Scalar color);
    cv::Scalar GetBackground();
    void SetVisualLineThresholds(int l0, int l1);

    void SetLineUnder(cv::Scalar color1, cv::Scalar color2);
    void SetLineOver(cv::Scalar color1, cv::Scalar color2);

private:
    cv::Scalar BackgroundColor;
    cv::Scalar LineColorUnder1, LineColorUnder2;
    cv::Scalar LineColorOver1, LineColorOver2;
    int ChartHeight;
    int ChartWidth;
    QQueue<int> DataQueue1;
    QQueue<int> DataQueue2;
    cv::Mat *ChartImage;
    QString Title;
    double ColorThreshold1, ColorThreshold2;
    int l0, l1;
    int currentLineState;
};

class ScrollingTLChart
{
public:
    ScrollingTLChart(int width, int height, ViewInfo view);
    void AddDataPoint(tlTypeClasses type);
    cv::Mat GetChartImage();
    QString GetTitle(void);
    ViewInfo GetView();
    void SetTitle(QString title);
    tlTypeClasses GetDataPoint();

private:
    cv::Scalar BackgroundColor;
    int ChartHeight;
    int ChartWidth;
    QQueue<tlTypeClasses> DataQueue;
    cv::Mat *ChartImage;
    QString Title;
    ViewInfo view;
};

#endif // SCROLLINGCHART_H
