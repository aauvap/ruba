#include "multithreadedprocessingdialog.h"

MultiThreadedProcessingDialog::MultiThreadedProcessingDialog(QWidget * parent, QList<ViewInfo> views,
	const std::vector<ruba::FrameStruct>& frames, std::shared_ptr<ruba::VideoHandler> vHandler)
	: QDialog(parent)
{
	setWindowTitle(tr("Multi-threaded processing"));
	this->views = views;
	this->frames = frames;
	this->parent = parent;
	this->processingIsStarted = false;
	this->vHandler = vHandler;

	processingThreadsGroupBox = new QGroupBox(tr("Number of threads"));
	QVBoxLayout* processingThreadsLayout = new QVBoxLayout();

	QHBoxLayout* threadCountLayout = new QHBoxLayout();
	threadCountSpinBox = new QSpinBox();
	threadCountSpinBox->setMinimum(2);
	threadCountLayout->addWidget(new QLabel(tr("Number of processing threads")));
	threadCountLayout->addWidget(threadCountSpinBox);

	applyThreadsButton = new QPushButton(tr("Apply"));
	threadCountLayout->addWidget(applyThreadsButton);

	connect(applyThreadsButton, SIGNAL(clicked(bool)), this, SLOT(on_applyThreadsButton_clicked(bool)));

	//threadCountLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);

	processingThreadsLayout->addLayout(threadCountLayout);
	addMoreVideosInfo = new QLabel(tr("Load more video files to increase the number of threads"));
	addMoreVideosInfo->setStyleSheet("font-weight: bold");
	processingThreadsLayout->addWidget(addMoreVideosInfo);
	processingThreadsGroupBox->setLayout(processingThreadsLayout);

	playbackGroupBox = new QGroupBox(tr("Playback"));
	QVBoxLayout* playbackLayout = new QVBoxLayout();
	QHBoxLayout* playLayout = new QHBoxLayout;

	playPauseButton = new QToolButton();
	playPauseButton->setIcon(QIcon(":/icons/control.png"));
	playPauseButton->setMinimumSize(QSize(35, 35));
	playPauseButton->setMaximumSize(QSize(35, 35));
	playLayout->addWidget(playPauseButton);

	connect(playPauseButton, SIGNAL(clicked(bool)), this, SLOT(playPauseButtonClicked()));


	QHBoxLayout* playbackThreadsLayout = new QHBoxLayout();
	playbackThreadsLayout->addWidget(playbackGroupBox);
	playbackThreadsLayout->addWidget(processingThreadsGroupBox);
	playbackThreadsLayout->addStretch();
	playbackGroupBox->setEnabled(false); // Disable playback until "Apply" has been pressed

	actualProcessingGroupBox = new QGroupBox(tr("Threads"));
	overallActualProcessingLayout = new QVBoxLayout();
	overallActualProcessingLayout->setContentsMargins(0, 0, 0, 0);

	QHBoxLayout* speedProcessingLayout = new QHBoxLayout();
	videoPlaybackFpsIcon = new QLabel();
	videoPlaybackFpsIcon->setPixmap(QPixmap(":/icons/speedometer.png"));
	videoPlaybackFpsIcon->hide();
	videoPlaybackFpsLabel = new QLabel();
	speedProcessingLayout->addStretch();
	speedProcessingLayout->addWidget(videoPlaybackFpsIcon);
	speedProcessingLayout->addWidget(videoPlaybackFpsLabel);
	speedProcessingLayout->addStretch();

	actualProcessingLayout = new QHBoxLayout();
	overallActualProcessingLayout->addLayout(speedProcessingLayout);
	overallActualProcessingLayout->addLayout(actualProcessingLayout);

	actualProcessingGroupBox->setLayout(overallActualProcessingLayout);
	actualProcessingGroupBox->setEnabled(false);

	playbackLayout->addLayout(playLayout);
	playbackGroupBox->setLayout(playbackLayout);

	combineLogFilesGroupBox = new QGroupBox(tr("Status"));
	QVBoxLayout* combineLogFilesLayout = new QVBoxLayout();
	statusTable = new QTableView();
	statusText = new QStandardItemModel;
	statusText->setHorizontalHeaderLabels(QStringList({ "Type", "Message" }));
	statusText->setColumnCount(2);
	statusTable->setModel(statusText);
	auto hHeader = statusTable->horizontalHeader();
	hHeader->setStretchLastSection(true);
	statusTable->setHorizontalHeader(hHeader);
	statusTable->resizeRowsToContents();
	
	combineLogFilesLayout->addWidget(statusTable);

	combineLogFilesGroupBox->setLayout(combineLogFilesLayout);
	combineLogFilesGroupBox->hide();

	QVBoxLayout* overallLayout = new QVBoxLayout();
	overallLayout->addLayout(playbackThreadsLayout);
	overallLayout->addWidget(actualProcessingGroupBox);
	overallLayout->addWidget(combineLogFilesGroupBox);

	setLayout(overallLayout);

	DisplayTimer.setParent(this);
	DisplayTimer.setInterval(50); // 20 fps
	connect(&DisplayTimer, SIGNAL(timeout()), this, SLOT(displayTimerTick()));
}

MultiThreadedProcessingDialog::~MultiThreadedProcessingDialog()
{
	for (auto& thread : threads)
	{
		thread->pause();
		thread->quit();

		thread->releaseVideo();
	}

	
	detectors.clear();
	threads.clear();
	videoFiles.clear();

	DisplayTimer.stop();


	for (auto& view : threadViews)
	{
		delete view;
	}
}

void MultiThreadedProcessingDialog::setVideoFiles(const std::vector<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture>>& videoFiles)
{
	this->videoFiles = videoFiles;

	// Find the best number of threads available for processing that 
	// correlates with the CPU and the number of video files submitted
	int idealThreadCount = QThread::idealThreadCount();

	if (idealThreadCount < 0)
	{
		idealThreadCount = 4;
	}

	idealThreadCount -= 1; // Save one thread to update the GUI
	
	if (videoFiles.size() > idealThreadCount)
	{
		addMoreVideosInfo->hide();
	}
	else {
		idealThreadCount = videoFiles.size();
		addMoreVideosInfo->show();
	}

	threadCountSpinBox->setMaximum(idealThreadCount);
	threadCountSpinBox->setValue(idealThreadCount);
}

void MultiThreadedProcessingDialog::on_applyThreadsButton_clicked(bool checked)
{
	playbackGroupBox->setEnabled(false);
	
	initialiseProcessingThreads();
	playbackGroupBox->setEnabled(true);
}

void MultiThreadedProcessingDialog::setDetectors(const std::vector<std::shared_ptr<ruba::AbstractCompoundDetector> >& detectors)
{
	this->detectors = detectors;
}

void MultiThreadedProcessingDialog::playPauseButtonClicked()
{
	if (DisplayTimer.isActive())
	{
		DisplayTimer.stop();
		playPauseButton->setIcon(QIcon(":/icons/control.png"));
		processingThreadsGroupBox->setEnabled(true);
		videoPlaybackFpsLabel->hide();
		videoPlaybackFpsIcon->hide();

		for (auto& thread : threads)
		{
			thread->pause();
			thread->quit();
		}
	}
	else {
		if (!processingIsStarted)
		{
			vHandler->Release();
		}
		
		playPauseButton->setIcon(QIcon(":/icons/control-pause.png"));
		processingThreadsGroupBox->setEnabled(false);
		videoPlaybackFpsLabel->show();
		videoPlaybackFpsIcon->show();
		processingIsStarted = true;

		for (auto& thread : threads)
		{
			thread->startProcessing();
		}

		DisplayTimer.start();
		combineLogFilesGroupBox->show();
	}
}

void MultiThreadedProcessingDialog::displayTimerTick()
{
	int stoppedCount = 0;
	
	for (auto i = 0; i < threads.size(); ++i)
	{
		std::vector<ruba::FrameStruct> frames;
		CaptureStatus status;

		processingSpeeds[i] = threads[i]->getProcessingSpeed();
		bool success = threads[i]->getCurrentFrames(frames, status);

		if (success)
		{
			threads[i]->unlockDisplayFrames();
			
			if (i < threadViews.size() && !frames.empty())
			{
				threadViews[i]->setDateTime(frames[0].Timestamp);
				qDebug() << "Thread " << i << frames[0].Timestamp.toString("yyyy-MM-dd HH-mm-ss.zzz");
			}

			QApplication::processEvents();
		}

		if (status != CaptureStatus::OK)
		{
			threads[i]->pause();
			threads[i]->quit();
			stoppedCount++;

			if (i < threadViews.size())
			{
				threadViews[i]->setIsFinished(true);
			}
		}
	}
	
	double totalSpeed = 0;
	for (auto& speed : processingSpeeds)
	{
		totalSpeed += speed;
	}
	videoPlaybackFpsLabel->setText(QString::number(totalSpeed, 'f', 0) + " frames/sec");


	if (stoppedCount == threads.size())
	{
		DisplayTimer.stop();
		playPauseButton->setIcon(QIcon(":/icons/control.png"));

		// We are about to finish - thus deactivate all functionality
		processingThreadsGroupBox->setEnabled(false);
		playbackGroupBox->setEnabled(false);
		videoPlaybackFpsLabel->hide();
		videoPlaybackFpsIcon->hide();

	combineLogFiles();
	}
}

void MultiThreadedProcessingDialog::recieveMessage(const QString & title, const QString & message, MessageType messageType)
{
	qDebug() << title << message;

	QList<QStandardItem*> newRow;

	switch (messageType)
	{
	case Information:
		//QMessageBox::information(this, title, message);
		newRow.append(new QStandardItem("Information"));
		break;
	case Warning:
		//QMessageBox::warning(this, title, message);
		newRow.append(new QStandardItem("Warning"));

		break;
	case Critical:
		//QMessageBox::critical(this, title, message);
		newRow.append(new QStandardItem("Critical"));

		break;
	case Question:
		QMessageBox::question(this, title, tr("Not yet implemented"));
		break;
	case MessageBar:
		//QMessageBox::information(this, title, message);
		newRow.append(new QStandardItem("Status"));
	}

	newRow.append(new QStandardItem(message));
	statusText->insertRow(statusText->rowCount(), newRow);
	statusTable->resizeRowsToContents();
}

void MultiThreadedProcessingDialog::closeEvent(QCloseEvent * e)
{
	if (DisplayTimer.isActive())
	{
		int answer = QMessageBox::question(this, tr("Close window?"),
			tr("Do you want to close the multi-threaded processing dialog? \n"
				"The current processing will be cancelled, the detectors will be closed, and any progress that is not "
				"saved to the log will be lost."), QMessageBox::Ok, QMessageBox::Cancel);

		if (answer == QMessageBox::Ok)
		{
			for (auto& thread : threads)
			{
				thread->pause();
				thread->quit();

				thread->releaseVideo();
			}
			DisplayTimer.stop();

			
			e->accept();
		}
		else {
			e->ignore();
		}
	}
	else {
		e->accept();
	}
}

void MultiThreadedProcessingDialog::initialiseProcessingThreads()
{
	int threadCount = threadCountSpinBox->value();

	if (threadCount == threads.size())
	{
		return;
	}
	else {
		threads.clear();

		for (auto& view : threadViews)
		{
			delete view;
		}

		threadViews.clear();

		delete actualProcessingLayout;
	}

	processingSpeeds.resize(threadCount);

	QProgressDialog waitDialog(tr("Copying detectors to enable multi-threaded processing"), QString(), 0, detectors.size() * (threadCount + 1) );
	waitDialog.setMinimumDuration(2000);

	std::vector<std::vector<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture> > > distributedVideoFiles;
	distributedVideoFiles.resize(threadCount);

	int videoFileCount = 0;
	int videoFilesPrThread = videoFiles.size() / threadCount;
	int remainderVideoFiles = videoFiles.size() % threadCount;
	
	for (auto i = 0; i < threadCount; ++i)
	{
		// Distribute the video files throughout the threads
		for (auto j = 0; j < videoFilesPrThread; ++j)
		{
			if (videoFileCount < videoFiles.size())
			{
				distributedVideoFiles[i].push_back(videoFiles[videoFileCount]);
				videoFileCount++;
				QApplication::processEvents();
			}
		}

		if (i < remainderVideoFiles)
		{
			if (videoFileCount < videoFiles.size())
			{
				distributedVideoFiles[i].push_back(videoFiles[videoFileCount]);
				videoFileCount++;
				QApplication::processEvents();
			}
		}
	}
	

	// Go through the detectors and make sure that they are saved on disk to that we can 
	// load multiple instances of them 
	int detectorCount = 1;

	for (auto& detector : detectors)
	{
		waitDialog.setValue(detectorCount);
		detectorCount++;

		if (detector->GetConfigSaveFilePath().isEmpty())
		{
			// Get temporary location to save the detector
			QString tempPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);

			if (!QDir(tempPath).exists()) {
				QDir().mkpath(tempPath);
			}

			QDir tempDir(tempPath);
			detector->SaveCurrentConfiguration(tempDir.absoluteFilePath("tempDetector.yml"));
		}
		else {
			detector->SaveCurrentConfiguration();
		}
		QApplication::processEvents();
	}

	actualProcessingLayout = new QHBoxLayout();
	

	for (auto i = 0; i < threadCount; ++i)
	{
		std::shared_ptr<ruba::VideoHandler> vHandler = std::make_shared<ruba::VideoHandler>(parent, views, true);
		vHandler->addVideoFiles(distributedVideoFiles[i]);

		std::shared_ptr<ruba::DetectorHandler> dHandler = std::make_shared<ruba::DetectorHandler>(parent, views);

		int detectorCount = 1;

		for (auto& detector : detectors)
		{
			dHandler->createDetector(-1, frames, detector->GetConfigSaveFilePath());

			waitDialog.setValue((i + 1)*detectors.size() + detectorCount);
			detectorCount++;
			QApplication::processEvents();
		}

		std::vector<std::shared_ptr<ruba::AbstractCompoundDetector> > newDetectors;
		dHandler->getDetectors(newDetectors);

		for (auto& newDetector : newDetectors)
		{
			newDetector->setLogFileNamePostfix(i);
		}
		

		std::shared_ptr<ProcessingThread> thread = std::make_shared<ProcessingThread>(vHandler, dHandler, this);
		threads.push_back(thread);

		connect(thread.get(), SIGNAL(sendMessage(QString, QString, MessageType)),
			this, SLOT(recieveMessage(QString, QString, MessageType)));

		SingleThreadWidget* threadView = new SingleThreadWidget(this, i);
		threadView->setVideoModel(vHandler->getVideoListModel());
		threadView->setDetectorModel(dHandler->getDetectorListModel());
		actualProcessingLayout->addWidget(threadView);
		threadViews.push_back(threadView);
	}

	//actualProcessingGroupBox->setLayout(actualProcessingLayout);
	overallActualProcessingLayout->addLayout(actualProcessingLayout);
	actualProcessingGroupBox->setEnabled(true);

	// Open the first video file on the list of all threads
	for (auto& thread : threads)
	{
		auto vHandler = thread->getVideoHandler();

		if (vHandler)
		{
			vHandler->setCurrentVideo(0);
		}
	}

	adjustSize();
}

void MultiThreadedProcessingDialog::combineLogFiles()
{
	// Close all existing logs from the threads and get their file names
	std::vector<std::vector<std::vector<std::pair<std::string, LogRole> > > > logFileNames; // Threads - Detectors - Logs
	logFileNames.resize(threads.size());

	int minDetectorCount = 0;

	for (auto i = 0; i < threads.size(); ++i)
	{
		auto dHandler = threads[i]->getDetectorHandler();

		if (dHandler)
		{
			std::vector<std::shared_ptr<ruba::AbstractCompoundDetector> > threadedDetectors;
			dHandler->getDetectors(threadedDetectors);

			if (minDetectorCount == 0 || minDetectorCount > threadedDetectors.size())
			{
				minDetectorCount = threadedDetectors.size();
			}

			logFileNames[i].clear();

			for (auto& detector : threadedDetectors)
			{
				detector->closeAllLogs();

				auto logFiles = detector->getLogFileNames();				
				logFileNames[i].push_back(logFiles);
			}
		}
	}

	// Get the original log file names from the detectors
	std::vector<std::vector<std::pair<std::string, LogRole> > > originalLogFileNames;

	for (auto& detector : detectors)
	{
		auto logFiles = detector->getLogFileNames();
		detector->closeAllLogs();
		originalLogFileNames.push_back(logFiles);
	}

	// Combine the log files, detector by detector
	for (auto i = 0; i < minDetectorCount; ++i)
	{
		std::vector<std::vector<std::pair<std::string, LogRole> > > logFilesByThreads; // LogFiles - threads
		
		for (auto j = 0; j < threads.size(); ++j)
		{
			if (logFilesByThreads.empty() || logFileNames[j][i].size() < logFilesByThreads.size())
			{
				logFilesByThreads.resize(logFileNames[j][i].size());
			}

			for (auto k = 0; k < logFileNames[j][i].size(); ++k)
			{
				if (logFilesByThreads.size() > k)
				{
					logFilesByThreads[k].push_back(logFileNames[j][i][k]);
				}
			}
		}

		// Write the combined file
		if (originalLogFileNames.size() > i)
		{
			for (auto j = 0; j < logFilesByThreads.size(); ++j)
			{
				if (originalLogFileNames[i].size() > j)
				{
					QFile combinedFile(QString::fromStdString(originalLogFileNames[i][j].first));
					if (combinedFile.open(QIODevice::Append))
					{
						QTextStream combinedStream(&combinedFile);

						QList<QStandardItem*> newRow;
						newRow.append(new QStandardItem("Logs"));
						newRow.append(new QStandardItem(tr("Combined log file to %1").arg(combinedFile.fileName())));
						statusText->insertRow(statusText->rowCount(), newRow);
						statusTable->resizeRowsToContents();

						auto previousInterval = EntryInterval();
						qint64 previousStreamPos = -1;
						QString previousLine = QString();

						for (auto k = 0; k < logFilesByThreads[j].size(); ++k)
						{
							QFile inputLog(QString::fromStdString(logFilesByThreads[j][k].first));

							if (inputLog.open(QIODevice::ReadOnly))
							{
								QTextStream inputStream(&inputLog);
								QStringList header = inputStream.readLine().split(";");
								// We already have the header in the combined file
								// so we just trow this line out								
								bool firstLine = true;

								while (!inputStream.atEnd())
								{
									QString line = inputStream.readLine();									

									if (firstLine && logFilesByThreads[j][k].second == SumTypeLog
										&& !previousInterval.getStartTime().isNull())
									{
										// If this log file is a sum-type log, we should try to handle overlapping time intervals
										// at the first line in the new sum log
										auto currentInterval = EntryInterval(line.split(";"), 0, 0, 0, header);

										if (previousInterval.getStartTime() == currentInterval.getStartTime() ||
											previousInterval.getEndTime() == currentInterval.getEndTime())
										{
											qDebug() << "We are combining sum logs from different threads within the same interval";

											// We have to go through some hoops to combine the two sum logs
											// First, we have to check if there is an entry called "Detections" at the previous entry
											auto previousRow = previousLine.split(";");
											auto currentRow = line.split(";");

											if (header.contains("Detections") && previousRow.size() > header.indexOf("Detections") &&
												currentRow.size() > header.indexOf("Detections"))
											{
												// It seems to be the case. Now read the numbers and multiply
												int combinedCount = (previousRow[header.indexOf("Detections")].toInt() +
													currentRow[header.indexOf("Detections")].toInt());
												currentRow[header.indexOf("Detections")] = QString::number(combinedCount);
												
												// And add the previous file name to the current line
												if (header.contains("File") && previousRow.size() > header.indexOf("File") && 
													currentRow.size() > header.indexOf("File"))
												{
													currentRow[header.indexOf("File")] = previousRow[header.indexOf("File")] + "," + currentRow[header.indexOf("File")];
												}

												// Try to seek back to the previous line to write the combined count
												if (combinedStream.seek(previousStreamPos))
												{
													// Now combine the redefined row into the line to be written
													line = currentRow.join(";");
												}
											}
										}

									}

									if (logFilesByThreads[j][k].second == SumTypeLog)
									{
										previousStreamPos = combinedStream.pos();
									}


									combinedStream << line << endl;

									if (logFilesByThreads[j][k].second == SumTypeLog)
									{
										previousInterval.setInterval(line.split(';'), 0, 0, 0, header);
										previousLine = line;
									}

									firstLine = false;
								}

								inputLog.close();
								inputLog.remove(); // Delete the now redundant log from the thread
							}
						}
						
						combinedFile.close();
					}
				}
			}
		}
	}

	QList<QStandardItem*> newRow;
	newRow.append(new QStandardItem("Logs"));
	newRow.append(new QStandardItem(tr("Finished processing and combining all log files. You may now close this window.")));
	statusText->insertRow(statusText->rowCount(), newRow);
	statusTable->resizeRowsToContents();

}


SingleThreadWidget::SingleThreadWidget(QWidget * parent, int number)
{
	threadNumber = number;
	overallGroupBox = new QGroupBox(tr("Thread %1").arg(number + 1));
	QVBoxLayout* overallGroupBoxLayout = new QVBoxLayout();

	QBoxLayout* dateTimeLayout = new QVBoxLayout();
	dateEdit = new QDateEdit();
	dateEdit->setCurrentSection(QDateTimeEdit::DaySection);
	dateEdit->setEnabled(false);
	dateEdit->setReadOnly(true);
	dateEdit->setFrame(false);
	dateEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);
	timeEdit = new QTimeEdit();
	timeEdit->setCurrentSection(QDateTimeEdit::HourSection);
	timeEdit->setEnabled(false);
	timeEdit->setReadOnly(true);
	timeEdit->setFrame(false);
	timeEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);
	timeEdit->setDisplayFormat(QString("HH:mm:ss:zzz"));

	dateTimeLayout->addWidget(dateEdit);
	dateTimeLayout->addWidget(timeEdit);
	
	QVBoxLayout* videoFilesLayout = new QVBoxLayout();

	videoFilesView = new QTreeView();
	videoFilesView->setHeaderHidden(true);
	videoFilesView->setAllColumnsShowFocus(true);
	videoFilesView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	videoFilesView->setDropIndicatorShown(false);
	videoFilesView->setDragEnabled(false);
	videoFilesView->setTextElideMode(Qt::TextElideMode::ElideLeft);
	videoFilesView->setRootIsDecorated(false);
	videoFilesView->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAsNeeded);
	auto policy = videoFilesView->sizePolicy();
	policy.setVerticalPolicy(QSizePolicy::Preferred);
	videoFilesView->setSizePolicy(policy);
	
	QLabel* videoFilesLabel = new QLabel(tr("Video Files"));
	videoFilesLayout->addWidget(videoFilesLabel);
	videoFilesLayout->addWidget(videoFilesView);


	//QGroupBox* detectorsGroupBox = new QGroupBox(tr("Detectors"));
	QVBoxLayout* detectorsLayout = new QVBoxLayout();
	detectorsView = new QTreeView();
	detectorsView->setHeaderHidden(true);
	detectorsView->setAllColumnsShowFocus(true);
	detectorsView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	detectorsView->setRootIsDecorated(true);
	policy = detectorsView->sizePolicy();
	policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
	detectorsView->setSizePolicy(policy);

	QLabel* detectorsLabel = new QLabel(tr("Detectors"));
	detectorsLayout->addWidget(detectorsLabel);
	detectorsLayout->addWidget(detectorsView);
	//detectorsGroupBox->setLayout(detectorsLayout);
	
	overallGroupBoxLayout->addLayout(dateTimeLayout);
	QFrame* line = new QFrame();
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	overallGroupBoxLayout->addWidget(line);
	overallGroupBoxLayout->addLayout(videoFilesLayout);

	QFrame* line2 = new QFrame();
	line2->setFrameShape(QFrame::HLine);
	line2->setFrameShadow(QFrame::Sunken);
	overallGroupBoxLayout->addWidget(line2);

	overallGroupBoxLayout->addLayout(detectorsLayout);
	overallGroupBox->setLayout(overallGroupBoxLayout);

	
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(overallGroupBox);

	setLayout(layout);
}

void SingleThreadWidget::setVideoModel(QStandardItemModel * model)
{
	videoFilesView->setModel(model);

	videoFilesView->header()->setStretchLastSection(false);

	if (videoFilesView->header()->count() > 1) {
		videoFilesView->header()->setSectionResizeMode(0, QHeaderView::Stretch);

		for (auto i = 1; i < videoFilesView->header()->count(); ++i) {
			//videoFilesView->header()->setSectionResizeMode(i, QHeaderView::Fixed);
			videoFilesView->header()->setSectionHidden(i, true);
		}
	}
}

void SingleThreadWidget::setDetectorModel(QStandardItemModel * model)
{
	detectorsView->setModel(model);

	if (detectorsView->header()->count() > 1) {
		detectorsView->header()->setSectionResizeMode(0, QHeaderView::Fixed);

		for (auto i = 1; i < detectorsView->header()->count(); ++i) {
			//detectorsView->header()->setSectionResizeMode(i, QHeaderView::Fixed);
			detectorsView->header()->setSectionHidden(i, true);
		}
	}
}

void SingleThreadWidget::setDateTime(const QDateTime dateTime)
{
	dateEdit->setDateTime(dateTime);
	timeEdit->setDateTime(dateTime);
}

void SingleThreadWidget::setIsFinished(bool value)
{
	if (value)
	{
		overallGroupBox->setTitle(tr("Thread %1 - finished!").arg(threadNumber + 1));
	}
	else {
		overallGroupBox->setTitle(tr("Thread %1").arg(threadNumber + 1));
	}
}
