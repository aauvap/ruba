#include "jumptoframedialog.h"

jumptoframedialog::jumptoframedialog(QWidget *parent, QDateTime defaultDateTime) :
    QDialog(parent)
{
    QGridLayout *mainLayout = new QGridLayout(this);

    descriptionLabel = new QLabel;
    descriptionLabel->setText("Jump to the selected date and time in the video list:");
    
    dateTimeEdit = new QDateTimeEdit();
    dateTimeEdit->setDateTime(defaultDateTime);
    dateTimeEdit->setCalendarPopup(true);
    dateTimeEdit->setDisplayFormat("dd-MM-yyyy HH:mm:ss");
   
    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok 
        | QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    mainLayout->addWidget(descriptionLabel);
    mainLayout->addWidget(dateTimeEdit);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);


    setWindowTitle(tr("Jump to frame"));
}


QDateTime jumptoframedialog::GetOutput()
{
    return dateTimeEdit->dateTime();
}
