#ifndef LOGGER_H
#define LOGGER_H


#include <QDateTime>
#include <QDebug>
#include <QList>
#include <QFile>

#include <iostream>
#include <map>
#include <memory>
#include <deque>
#include <set>
#include <vector>

#include "ruba.h"
#include "loginfostruct.h"
#include "framestruct.h"
#include "maskoverlaystruct.h"
#include "utils.hpp"
#include "cvutilities.h"
#include "viewinfo.h"

// New log system
enum Transition {
    ENTERING,
    LEAVING,
	OVERLAP
};

enum LogInsertionPosition {
    NONE = -1,
    FIRST = 0,
    SECOND = 1,
    THIRD = 2
};

enum LogEntryState {
    OPEN,
    INVALID,
    CLOSED
};

enum LogRole {
	EventTypeLog,
	SumTypeLog
};

/*!
    If this detector (detector 1) is triggered initially, detector 2
    should be activated within [CreateEventMinDelay, CreateEventMaxDelay]
    if the activation of detector 2 is to be combined with the event from
    detector 1
*/
struct CombinedEventDelays {
    int createEventMinDelay; 
    int createEventMaxDelay;
};

struct CustomLogHeader {
    std::map<Transition, QString> additionalTransitionText;
};

struct InitEntrySettings {
    //! Specifies the time, in ms, for which consecutive events should be collated
    /*!
        Example: consecutiveEventDelay = 300 ms
        Detector 1 goes high at time 50 ms, and goes low at time 1200 ms. 
        The duration of this event is 1150 ms. 
        Detector 1 goes high at 1400 ms. Because the time to the last triggered instance
        of detector 1 is 250 ms, and thus lower than the consecutiveEventDelay, 
        this event is grouped with the former event, and the leaving time of the 
        former event is cleared, awaiting detector 1 to go low again.
    */
    std::map<QString, int> consecutiveEventDelay;

    //! Discards events for which the duration between the entering and leaving time are below this threshold, in ms
    std::map<QString, int> discardEventsLessThan;

    //! Specifies the internal type of chained events, i.e. ENTERING - ENTERING
    /*!
    If multiple events are chained into one log entry, i.e. one line in the log,
    the intervalType specifies the way we should measure the time interval between
    these events.

    Example:
    We want to chain detections from detector 1 and detector 2 if detector 2 is
    triggered minimum 100 ms and maximum 2000 ms after detector 1 has gone from
    high to low - i.e. the state of detector 1 is "Leaving".

    The intervalType is then specified as {LEAVING, ENTERING}
    and the CombinedEventDelays struct is created as following:

    createEventMinDelay = 100
    createEventMaxDelay = 2000
    firstDetectorIdText = "unique Id of detector 1"
    */
    std::vector<Transition> intervalType;

    //! Contains the createEventMinDelay and the createEventMaxDelay for each detector
    std::map<QString, CombinedEventDelays> eventDelayIntervals;

    //! Contains the settings for saving frames on Entering and/or Leaving events for each detector
    std::map<QString, std::vector<Transition>> saveFrameOfEventSettings;

    //! Contains the maximum allowed active duration (in ms) for each detector
    std::map<QString, int> maxTriggeredDuration;

	//! If overlap timing is used, allow the use of a buffer to note the allowed leeway
	// between detector activation overlaps
	int overlapTimingBuffer;
};

struct InitLogSettings {
    QString everyEventLogPath;
    QString sumOfEventsLogPath;

    bool saveEveryEventLog;
    bool saveSumOfEventsLog;

    //! For each sumInterval minutes, sum op the number of entries of the everyEventLog
    int sumInterval;

    //! Text that is added to the log headers, e.g. in order to go from "Entering" to "Entering Detector 1"
    std::vector<CustomLogHeader> customLogHeader;

    InitEntrySettings entrySettings;

    QString saveFrameOfEventPath;
};



/*!
    A log event contains an entering time and a leaving time for a particular detector
*/
class LogEvent {
public :
    LogEvent(const QDateTime &enteringTime, 
        const std::vector<ruba::FrameStruct> &currentFrames,
        const QString &detectorIdText, 
        const std::vector<Transition> &saveFrameOfEventSettings, 
        const std::vector<ruba::MaskOutlineStruct>& maskOutline);

    QDateTime getEnteringTime() const { qDebug() << "Retrieving entering time for " << detectorIdText << "Entering: " << enteringTime.toString("yyyy MM dd hh:mm:ss.zzz") << "Leaving:" << leavingTime.toString("yyyy MM dd hh:mm:ss.zzz"); return enteringTime; }
    QDateTime getLeavingTime() const { qDebug() << "Retrieving leaving time for " << detectorIdText << "Entering: " << enteringTime.toString("yyyy MM dd hh:mm:ss.zzz") << "Leaving:" << leavingTime.toString("yyyy MM dd hh:mm:ss.zzz"); return leavingTime;  }
    int duration() const { return enteringTime.msecsTo(leavingTime); }

    void setLeavingTime(const QDateTime &leavingTime,
        const std::vector<ruba::FrameStruct> &currentFrames);
    void resetLeavingTime();


    QString getDetectorIdText() const { return detectorIdText; }

    void getEventFrames(std::map<Transition, std::vector<ruba::FrameStruct> >& eventFrames);

private:
    QString detectorIdText;
    QDateTime enteringTime;
    QDateTime leavingTime;

    std::map<Transition, std::vector<ruba::FrameStruct> > eventFrames;

    std::vector<Transition> saveFrameOfEventSettings;
    std::vector<ruba::MaskOutlineStruct> maskOutline;

};

/*!
    A LogEntry consists of one or more LogEvents which are ultimately combined to one, 
    comma-separated line in a log file
*/
class LogEntry {
public:
    LogEntry(const InitEntrySettings& entrySettings,
        const QDateTime &enteringTime, 
        const QFileInfo &filename,
        const std::vector<ruba::FrameStruct>& currentFrames,
        const std::vector<ruba::MaskOutlineStruct>& maskOutline,
        const QString& detectorIdText,
        const QString& compoundDetectorIdText);

    void updateEntry(const QDateTime& currentTime,
        const Transition& transition,
        const std::vector<ruba::FrameStruct>& currentFrames,
        const QString& detectorIdText,
        const std::vector<ruba::MaskOutlineStruct>& maskOutline,
        LogInsertionPosition &eventPos);

    QString getEntryAsString() const;
    std::map<ViewInfo, cv::Mat> getEntryFrames();
    QDateTime getEarliestEnteringTime();

    QList<QString> getDebugInfo() const;

    static int getEventTimegap(const LogEvent &firstEvent,
        const LogEvent &secondEvent,
        const std::vector<Transition> &intervalType);

    void checkEntryState(const QDateTime &currentTime,
        const std::vector<ruba::FrameStruct> &currentFrames,
        LogEntryState &state);


private:
    void checkSaveFrameOfEventSettings(const QString& detectorIdText);
	int getConsecutiveEventDelay(const QString& detectorIdText);
	int getDiscardEventsLessThan(const QString& detectorIdText);

    QDateTime startTime;
    QFileInfo filename;

    std::vector<std::shared_ptr<LogEvent> > events;

    QString compoundDetectorIdText;
    InitEntrySettings settings;
    LogEntryState state;
};

struct SumLogEntry
{
    QFileInfo file;
    QDateTime countStart;
    QDateTime countEnd;
    int count;
};

class SumLog {
public:
    SumLog(const QString& sumOfEventsLogPath,
        int sumInterval, const QDateTime& currentTime,
        const QFileInfo & currentFile);

    void increment(const QDateTime& currentTime,
        const QFileInfo& currentFile);
    void update(const QDateTime& currentTime,
        const QFileInfo& currentFile);

    void saveCurrentEntry();
	void closeLog();
	std::string getFileName();

private:
    SumLogEntry currentEntry;

    int sumInterval;
    QFile fileHandle;
};

/*!
    The Logger class is available to the detectors and 
    has the overall responsibility for maintaining 
    and updating the log with respect to the 
    settings that is given in the constructor
*/
class ruba::Logger
{
public:
    // Constructor
    Logger(const InitLogSettings& settings, 
        const QString& compoundDetectorIdText,
        const QDateTime& currentTime,
        const std::vector<FrameStruct>& currentFrames);

    void updateLog(const QDateTime& currentTime, 
        const std::vector<ruba::FrameStruct>& currentFrames,
        const Transition& transition,
        const QString& detectorIdText,
        const std::vector<ruba::MaskOutlineStruct>& maskOutlines);

    void saveLog(const QDateTime& currentTime,
        const std::vector<ruba::FrameStruct>& currentFrames,
        bool forceSave = false);

    void reset(const QDateTime& currentTime, 
        const std::vector<ruba::FrameStruct>& currentFrames);

    void setCustomisedLogInfo(const QList<QString>& customInfo) { this->customInfo = customInfo;  }

	void setLogFileNamePostfix(int number);

	void closeAllLogs();
	std::vector<std::pair<std::string, LogRole> > getFileNames();

    qint64 getCompletedEventCount();
    QDateTime getLastEnteringTime();
    QDateTime getLastLeavingTime();

    static QString getTransitionAsString(Transition state);
    QList<QString> getDebugInfo() const;

private:
    void initialiseEveryEventLog();

    InitLogSettings settings;

    std::vector<std::shared_ptr<LogEntry> > everyEventEntries;
    qint64 completedEventCounter;
    std::deque<std::shared_ptr<LogEntry> > completedEveryEventEntries;
    std::set<QString> availableDetectorIdTexts;

    QFile everyEventLog;

    std::shared_ptr<SumLog> sumLog;

    std::vector<QString> savedFramesPaths;
    QString compoundDetectorIdText;

    QDateTime lastEnteringTime, lastLeavingTime;
    QList<QString> customInfo;
};


#endif // LOGGER_H
