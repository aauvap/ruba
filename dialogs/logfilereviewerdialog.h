#ifndef LOGFILEREVIEWERDIALOG_H
#define LOGFILEREVIEWERDIALOG_H

#include <memory>
#include <QDialog>
#include <QMessageBox>
#include <QStandardItemModel>
#include <QtGui>
#include <QDateTime>
#include <QFileDialog>
#include <QShortcut>
#include <QTreeView>
#include <QHash>
#include <QMap>
#include <opencv2/opencv.hpp>


#include "ruba.h"
#include "framestruct.h"
#include "groundtruthannotatordialog.h"

enum dateTimeFormat {
    hh_mm_ss_zzz,
    yyyy_MM_DD_hh_mm_ss_zzz,
    hh_mm_ss,
};

class DateTimeEventInfo {
public:
	DateTimeEventInfo(const QDateTime& dateTime, const QString& header, 
		const std::vector<std::pair<QString, QString> >&  additionalEntryInfo,
		const int rowNumber, const int dateTimeColumnNumber);

	QDateTime getDateTime() const { return dateTime; }
	QString getHeader() const { return header; }
	std::vector<std::pair<QString, QString> > getAdditionalEntryInfo() const{ return additionalEntryInfo; }
	int getRowNumber() const { return rowNumber; }
	int getDateTimeColumnNumber() const { return dateTimeColumnNumber; }


private:
	QDateTime dateTime;
	QString header;
	std::vector<std::pair<QString, QString> > additionalEntryInfo;
	int rowNumber;
	int dateTimeColumnNumber;
};

class EntryInterval
{
public:
    EntryInterval();

    EntryInterval(const QList<QStandardItem*>& row, int secondsBefore, int secondsAfter, 
        int rowNumber, QStringList headerLabels);
	EntryInterval(const QStringList& row, int secondsBefore, int secondsAfter,
		int rowNumber, QStringList headerLabels);

    int getRowNumber() const { return rowNumber; }
    void setStartTime(const QDateTime& newStartTime) { startTime = newStartTime; }
    QDateTime getStartTime() const { return startTime; }
    QDateTime getEndTime() const { return endTime; }
    
    void setInterval(const QList<QStandardItem*>& row, int secondsBefore, int secondsAfter, int rowNumber, QStringList headerLabels);
	void setInterval(const QStringList row, int secondsBefore, int secondsAfter, int rowNumber, QStringList headerLabels);
    qint64 getIntervalInMsecs() const { return startTime.msecsTo(endTime); }

    void update(const QDateTime& currentTime, qint64 &msecsFromStartTime,
        std::vector<std::pair<QString, QString> >& timeToEvents,
        std::vector<std::pair<QString, QString> >& entryInfo,
        int& lastEventNumber);

	bool isDateTimeBeforeInterval(const QDateTime& currentTime) const;
    bool isDateTimeInInterval(const QDateTime& currentTime) const;
	bool isDateTimeAfterInterval(const QDateTime& currentTime) const;

	std::vector<std::pair<QString, QDateTime> > getEntryEventDateTimes() const { return entryEventDateTimes; }
	std::vector<std::pair<QString, QString> > getEntryInfo() const { return entryInfo; }

private:
    void getIntervalFromRow(const QStringList& row, QDateTime& earliestTime, QDateTime& latestTime);
    QDateTime getDateTimeFromEntry(const QString& text, dateTimeFormat format, bool recursive, const QDate &latestDate);
    QDate getDateFromEntry(const QString& text);

    QDateTime startTime;
    QDateTime endTime;

    int rowNumber;
    dateTimeFormat timestampFormat;

    std::vector<std::pair<QString, QDateTime> > entryEventDateTimes;
    std::vector<std::pair<QString, QString> > entryInfo;
    QStringList headerLabels;
};

enum PlaybackSource {
	SourceFile,
	SortedSourceFile,
	FilteredFile,
	Undefined
};

Q_DECLARE_METATYPE(PlaybackSource)


class LogFileProcessor : public QObject
{
	Q_OBJECT

public:
	LogFileProcessor(QObject *parent, int startEarlier, int endAfter);

	void updateTime(QDateTime time, bool isPlaying);

	void setStartEarlier(int value) { startEarlier = value; }
	void setEndAfter(int value) { endAfter = value; }
	void enableContinuousPlayback(bool value);
	void setCurrentPlaybackTable(PlaybackSource source);
	QStandardItemModel* getCurrentPlaybackTable() { return currentPlaybackTable; }
	QStandardItemModel* getSourceTable() { return sourceTable; }
	QStandardItemModel* getSortedSourceTable() { return sortedSourceTable; }
	QStandardItemModel* getFilteredTable() { return filteredTable; }
	QStandardItemModel* getCurrentEntryEventTable() { return currentEntryEventTable; }
	QStandardItemModel* getCurrentEntryInfoTable() { return currentEntryInfoTable; }
	QDateTime getStartTime() { return currentEntry.getStartTime(); }
	QDateTime getEndTime() { return currentEntry.getEndTime(); }
	qint64 getInterval() { return currentEntry.getIntervalInMsecs(); }

	void setHeaderLabels(const QStringList& headerLabels) { this->headerLabels = headerLabels; }

	bool setCurrentEntry(int currentRow);
	

signals:
	void play();
	void pause();
	void updateCurrentEntryUi(QStandardItemModel* table, PlaybackSource view, int rowNumber,
		const QDateTime& startTime, const QDateTime& endTime, qint64 intervalInMsecs);
	void updateCurrentEntryItem(const QModelIndex &index);
	void setProgressBar(qint64 msecsFromStartTime);
	void setTimeLabel(const QString& timeLabel);
	void currentRowChanged(QModelIndex index);
	void clearRowSelection();

private:
	QDateTime currentTime;
	QDateTime previousTime;
	bool isCreatingEntry;

	EntryInterval currentEntry;
	EntryInterval previousEntry;
	PlaybackSource currentTable;

	QStandardItemModel* currentPlaybackTable;

	QStandardItemModel* currentEntryEventTable;
	QStandardItemModel* currentEntryInfoTable;

	QStandardItemModel *sourceTable;
	QStandardItemModel *filteredTable;
	QStandardItemModel *sortedSourceTable;

	int startEarlier;
	int endAfter;

	bool continuousPlayBack;
	QStringList headerLabels;

};

namespace Ui {
class LogFileReviewerDialog;
}

class LogFileReviewerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LogFileReviewerDialog(QWidget *parent, const std::vector<ruba::FrameStruct> &initFrames);
    ~LogFileReviewerDialog();

    void updateTime(QDateTime time);

	bool eventFilter(QObject* object, QEvent* event);

signals:
     void jumpToFrame(QDateTime requestedTime);
     void play();
     void pause();

public slots:
	void updateCurrentEntryUi(QStandardItemModel* table, PlaybackSource view, int rowNumber,
		const QDateTime& startTime, const QDateTime& endTime, qint64 intervalInMsecs);

	void setProgressBar(qint64 msecsFromStartTime);

	void setTimeLabel(const QString& timeLabel);

	void currentRowChanged(QModelIndex index);

	void clearRowSelection();

	void updateCurrentEntryItem(const QModelIndex &index);

	void pausePlayback();

	void startPlayback();

private slots:
    void autoSave();

	void on_enableContinuousPlaybackCheckBox_clicked(bool checked);

	void on_startEarlierSpinBox_valueChanged();

	void on_endAfterSpinBox_valueChanged();

    void on_loadFilteredButton_clicked();
    
    void on_saveFilteredButton_clicked();
    
    void on_saveFilteredAsButton_clicked();

    void on_autoSaveCheckBox_clicked(bool checked);

    void on_loadSourceFileButton_clicked();

    void on_sourceFileViewer_doubleClicked(const QModelIndex& index);
    
    void on_filteredFileViewer_doubleClicked(const QModelIndex& index);

    void on_progressBar_sliderReleased();

    void on_playPauseButton_pressed();

    void on_deleteLogEntryButton_pressed();

    void on_insertLogEntryButton_pressed();

    void on_filteredTable_rowsInserted(const QModelIndex &parent, int first, int last);

	void on_oneTimeStampPerRowRadioButton_toggled();


private:
    int openSourceLog(const QString& filename);
    int openFilteredLog(const QString& filename);

    void saveFilteredLog();

    void deleteSelected();
    void insertSelected();

	std::shared_ptr<LogFileProcessor> logFileProcessor;

    Ui::LogFileReviewerDialog *ui;
    QTimer autoSaveTimer;

    QWidget* parent;    
    bool dataChanged;

	QStringList headerLabels;

	PlaybackSource currentPlaybackTable;
	bool isPlaying;

    QHash<int, std::shared_ptr<QShortcut>> playbackShortcuts;
};

#endif // LOGFILEREVIEWERDIALOG_H
