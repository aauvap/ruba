#include "presencedetector.h"

using namespace std;
using namespace cv;
using namespace ruba;

PresenceDetector::PresenceDetector(BaseDetectorParams baseParams, std::vector<int> minCoveredArea,
	std::vector<cv::Mat> masks, std::vector<cv::Mat> backImages, bool usechart)
	: AbstractMultiModalDetector(configureBaseDetectorParams(baseParams, PRESENCEDETECTOR),
		initialiseChartColors())
{
    if (minCoveredArea.size() != masks.size())
    {
        IsInitialized = false;
        return;
    } else {
        IsInitialized = true;
    }


    for (auto i = 0; i < minCoveredArea.size(); ++i)
    {
        BaseDetectorParams newBaseParams;
        newBaseParams.detectorID = 100 + i;
        newBaseParams.detectorType = PRESENCEDETECTOR;
        newBaseParams.initFrames = vector<FrameStruct>(1, baseParams.initFrames[i]);

        shared_ptr<PresenceDetectorSingleModal> d = make_shared<PresenceDetectorSingleModal>(newBaseParams, minCoveredArea[i], 
            masks[i], backImages[i], usechart);
        PDetectors.push_back(d);
        singleModalDetectors.push_back(d);
    }
	
    IsTriggered.clear();
    IsTriggered.push_back(false);

    lastChange.clear();
    lastChange.resize(1);

    confidence.clear();
    confidence.resize(1);
}

void PresenceDetector::InitDetector()
{
    for (size_t i = 0; i < PDetectors.size(); ++i)
    {
        PDetectors[i]->InitDetector();
    }
}

QList<QString> PresenceDetector::getDetectorSpecificDebugInfo()
{       
    QList<QString> debugList;
    debugList.push_back("Confidence: " + QString::number(this->confidence[0]));
    debugList.push_back("LastChange: " + this->lastChange[0].toString());
    return debugList;
}

ChartColors ruba::PresenceDetector::initialiseChartColors()
{
	ChartColors chartColors;
	chartColors.backgroundColors.enabledTriggeredColor = cv::Scalar(255, 200, 200);
	chartColors.backgroundColors.enabledNotTriggeredColor = cv::Scalar(255, 220, 220);
	chartColors.backgroundColors.notEnabledColor = cv::Scalar::all(255);

	chartColors.lineUnderColors.enabledTriggeredColor = cv::Scalar(255, 100, 100);
	chartColors.lineUnderColors.enabledNotTriggeredColor = cv::Scalar(255, 100, 100);
	chartColors.lineUnderColors.notEnabledColor = cv::Scalar::all(150);

	chartColors.lineOverColors.enabledTriggeredColor = cv::Scalar(220, 0, 0);
	chartColors.lineOverColors.enabledNotTriggeredColor = cv::Scalar(255, 100, 100);
	chartColors.lineOverColors.notEnabledColor = cv::Scalar::all(150);

	return chartColors;
}

std::vector<QString*> PresenceDetector::GetChartTitles()
{
    if (UseScrollingChart)
    {
        vector<QString*> tempTitles;

        QString* tempWindowName = new QString("Chart 1");
        tempTitles.push_back(tempWindowName);

        return tempTitles;

    }
    else
    {
        vector<QString*> tempTitles;
        tempTitles.push_back(NULL);

        return tempTitles;
    }
}




