#include "videoinformationdialog.h"

VideoInformationDialog::VideoInformationDialog(QWidget* parent, QVBoxLayout* layout)
    : QDialog(parent)
{
    setWindowTitle(tr("Video properties"));

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(buttonBox);

    layout->addStretch(1);
    layout->addSpacing(12);
    layout->addLayout(buttonsLayout);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(close()));

    setLayout(layout);
}