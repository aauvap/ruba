#ifndef TRAFFICLIGHTDETECTOR_H
#define TRAFFICLIGHTDETECTOR_H


#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include <QDialog>
#include <QMessageBox>
#include <QFileInfoList>
#include <QFile>
#include <QFileDialog>
#include <QList>
#include <QDateTime>

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

#include <cmath>
#include <fstream>
#include <memory>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>

#include "ruba.h"
#include "abstractmultimodaldetector.h"
#include "scrollingchart.h"
#include "framestruct.h"
#include "ChartStruct.h"
#include "detectoroutputstruct.h"
#include "trafficlightdetectorsinglemodal.h"


class ruba::TrafficLightDetector : public AbstractMultiModalDetector {

public:
	// Constructor
	TrafficLightDetector(BaseDetectorParams baseParams, std::vector<cv::Mat> backimages,std::vector<cv::Mat> maskimages, 
        std::vector<cv::Point> tlPlacements, int tlTypeTrigger, bool usechart);

    DetectorOutput GetOutput();

	std::vector<ruba::ChartStruct> GetChartStructs();
    QList<QString> getDetectorSpecificDebugInfo();

	void SetUseScrollingChart(bool useScrollingChart);
        std::vector<QString*> GetChartTitles();
	void InitDetector();

protected:
	virtual ChartColors initialiseChartColors();


private:
    virtual void updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput = std::vector<DetectorOutput>());

    std::vector<std::shared_ptr<ruba::TrafficLightDetectorSingleModal> > TLDetectors;
    std::vector<ScrollingTLChart*> TLCharts;
    tlTypeClasses tlDetectionClass;
    std::vector<int> tlClassVals;



	bool IS_FIRST_RUN;
};


#endif // TRAFFICLIGHTDETECTOR_H
