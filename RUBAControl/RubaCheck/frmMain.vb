﻿Public Class frmMain
  Private MyInputFile As String
  Private MyInputFolder As String
  Private MyOriginalDetections As List(Of clsDoubleDetection)
    Private MyWorkingDetections As List(Of clsDoubleDetection)
    Private MyCurrentDetectionIndex As Integer
  Private MyLowerThreshold As Integer
    Private MyUpperThreshold As Integer
    Public setting As Integer




    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call SetAllToZero()
        setting = 2
        MyLowerThreshold = 0
        MyUpperThreshold = 2000
    End Sub


  Private Sub SetAllToZero()
    mnuFile.Visible = True
    mnuData.Visible = False

    grpRecordData.Visible = False
    pnlNavigation.Visible = False
    picImage.Visible = False

    Me.Text = "RUBA Control"


    MyInputFile = ""
    MyInputFolder = ""
    MyOriginalDetections = Nothing
    MyWorkingDetections = Nothing
    MyCurrentDetectionIndex = -1
  End Sub


  Private Sub MakeProjectReady()

    mnuData.Visible = True
    grpRecordData.Visible = True
    pnlNavigation.Visible = True
    picImage.Visible = True
  End Sub



  Private Sub mnuFileOpen_Click(sender As Object, e As EventArgs) Handles mnuFileOpen.Click

        Dim _OpenDialog As OpenFileDialog
        _OpenDialog = New OpenFileDialog
        With _OpenDialog
      .CheckFileExists = True
      .Multiselect = False
      .Title = "Select the detection file"
      .Filter = "Comma-separated values (*.csv)|*.csv"
      .FileName = ""
      .ShowDialog()

      If .FileName.Length > 0 Then
                If Not LoadFile(.FileName, setting) Then
                    Call SetAllToZero()
                End If
            End If
    End With
  End Sub


    Private Function LoadFile(ByVal FileName As String, setting As Integer) As Boolean

        Call SetAllToZero()
        Num_Detectors.Enabled = False
        NumberOfDetectorsToolStripMenuItem.Enabled = False

        MyInputFile = FileName
        MyOriginalDetections = New List(Of clsDoubleDetection)
        MyWorkingDetections = New List(Of clsDoubleDetection)
        If ReadInputFile(setting) Then
            If OpenFolder() Then

                If setting = 2 Then
                    Call SetGapTimeInterval()
                Else
                    MyLowerThreshold = 0
                    MyUpperThreshold = 2000
                End If

                Call SelectWorkDetections(clsDoubleDetection.DetectionStatuses.NotChecked)
                Call MakeProjectReady()
                    Me.Text = MyInputFile
                    Me.KeyPreview = True
                    Return True
                Else
                    Return False
            End If
        Else
            Return False
        End If

    End Function

    Private Function ReadInputFile(setting As Integer) As Boolean
        Dim _FileIndex As Integer
        Dim _Line As String
        Dim _Fields() As String
        Dim _Detection As clsDoubleDetection


        _FileIndex = FreeFile()
        Try
            FileOpen(_FileIndex, MyInputFile, OpenMode.Input)
        Catch ex As Exception
            MsgBox("File seems to be already open by other program.")
            Return False
        End Try

        Try
            Do Until EOF(_FileIndex)
                _Line = LineInput(_FileIndex)
                If Not _Line.StartsWith("File;Date;Entering") Then
                    _Fields = _Line.Split(";")

                    If setting = 1 Then
                        _Detection = New clsDoubleDetection
                        With _Detection
                            .File = _Fields(0)
                            .DetectionDate = TextToShortDate(_Fields(1))
                            .Entering1 = TextToFullDate(_Fields(2))
                            .Exiting1 = TextToFullDate(_Fields(3))
                            .Duration1 = CInt(_Fields(4))
                            .ImageFileName = _Fields(5)

                            .Entering2 = .Entering1
                            .Exiting2 = .Exiting1
                            .Duration2 = .Duration1
                            .TimeGap = 1
                            .FirstTriggered = "Single detector"
                            .other_data = ""

                        End With
                        MyOriginalDetections.Add(_Detection)

                    Else

                        _Detection = New clsDoubleDetection

                        With _Detection
                            .File = _Fields(0)
                            .DetectionDate = TextToShortDate(_Fields(1))
                            .Entering1 = TextToFullDate(_Fields(2))
                            .Exiting1 = TextToFullDate(_Fields(3))
                            .Duration1 = CInt(_Fields(4))
                            .Entering2 = TextToFullDate(_Fields(5))
                            .Exiting2 = TextToFullDate(_Fields(6))
                            .Duration2 = CInt(_Fields(7))
                            .TimeGap = CInt(_Fields(8))
                            .FirstTriggered = _Fields(9)
                            .ImageFileName = _Fields(10)
                            .other_data = ""
                        End With
                        MyOriginalDetections.Add(_Detection)
                    End If
                End If
            Loop
            FileClose(_FileIndex)

        Catch ex As Exception
            MsgBox("Failed to read the data structure in the .csv file.")
            FileClose(_FileIndex)
            Return False
        End Try

        Return True
    End Function

    Private Function OpenFolder() As Boolean
    Dim _OpenFolderDialog As FolderBrowserDialog


    _OpenFolderDialog = New FolderBrowserDialog
    With _OpenFolderDialog
      .SelectedPath = IO.Path.GetDirectoryName(MyInputFile)
      .ShowNewFolderButton = False
      .Description = "Select folder containing frames with detections"
      If .ShowDialog(Me) = DialogResult.OK Then
        If LoadInputFolder(.SelectedPath) Then Return True Else Return False
      Else
        Return False
      End If
    End With

  End Function

  Private Function LoadInputFolder(ByVal InputFolder As String) As Boolean


    MyInputFolder = InputFolder

    Try
      If Not IO.Directory.Exists(MyInputFolder & "\Results\") Then IO.Directory.CreateDirectory(MyInputFolder & "\Results\")
      If Not IO.Directory.Exists(MyInputFolder & "\Results\Correct\") Then IO.Directory.CreateDirectory(MyInputFolder & "\Results\Correct\")
      If Not IO.Directory.Exists(MyInputFolder & "\Results\False alarms\") Then IO.Directory.CreateDirectory(MyInputFolder & "\Results\False alarms\")
      If Not IO.Directory.Exists(MyInputFolder & "\Results\Not sure\") Then IO.Directory.CreateDirectory(MyInputFolder & "\Results\Not sure\")
    Catch ex As Exception
      MsgBox("Failed to create the necessary output folders!!!")
      Return False
    End Try


    For Each _Detection In MyOriginalDetections
      'If Not IO.File.Exists(MyInputFolder & "\" & _Detection.ImageFileName) Then
      '  MsgBox("File '" & _Detection.ImageFileName & "' is missing in the input folder!!!")
      '  Return False
      'End If

      If IO.File.Exists(MyInputFolder & "\Results\Correct\" & _Detection.ImageFileName) Then
        _Detection.Status = clsDoubleDetection.DetectionStatuses.Correct
      ElseIf IO.File.Exists(MyInputFolder & "\Results\False alarms\" & _Detection.ImageFileName) Then
        _Detection.Status = clsDoubleDetection.DetectionStatuses.FalseDetection
      ElseIf IO.File.Exists(MyInputFolder & "\Results\Not sure\" & _Detection.ImageFileName) Then
        _Detection.Status = clsDoubleDetection.DetectionStatuses.NotSure
      Else
        _Detection.Status = clsDoubleDetection.DetectionStatuses.NotChecked
      End If
    Next

    Return True
  End Function


  Private Sub SelectWorkDetections(ByVal StatusFilter As clsDoubleDetection.DetectionStatuses)


        MyWorkingDetections.Clear()

        For Each _Detection In MyOriginalDetections
            If (_Detection.Status = StatusFilter) And IsBetweenOrdered(_Detection.TimeGap, MyLowerThreshold, MyUpperThreshold) Then
                MyWorkingDetections.Add(_Detection)
            End If
        Next

        If MyWorkingDetections.Count > 0 Then MyCurrentDetectionIndex = 0 Else MyCurrentDetectionIndex = -1
    Call DisplayDetection(MyCurrentDetectionIndex)


    Select Case StatusFilter
      Case clsDoubleDetection.DetectionStatuses.NotChecked
        mnuDataFilterNotChecked.Checked = True
        mnuDataFilterCorrect.Checked = False
        mnuDataFilterFalseAlarms.Checked = False
        mnuDataFilterNotSure.Checked = False

      Case clsDoubleDetection.DetectionStatuses.Correct
        mnuDataFilterNotChecked.Checked = False
        mnuDataFilterCorrect.Checked = True
        mnuDataFilterFalseAlarms.Checked = False
        mnuDataFilterNotSure.Checked = False

      Case clsDoubleDetection.DetectionStatuses.NotSure
        mnuDataFilterNotChecked.Checked = False
        mnuDataFilterCorrect.Checked = False
        mnuDataFilterFalseAlarms.Checked = False
        mnuDataFilterNotSure.Checked = True

      Case clsDoubleDetection.DetectionStatuses.FalseDetection
        mnuDataFilterNotChecked.Checked = False
        mnuDataFilterCorrect.Checked = False
        mnuDataFilterFalseAlarms.Checked = True
        mnuDataFilterNotSure.Checked = False
    End Select

    End Sub


    Private Sub DisplayDetection(ByVal DetectionIndex As Integer)
    Dim _ImageFilePath As String
    Dim _Bitmap As Bitmap


    If IsBetweenOrdered(DetectionIndex, 0, MyWorkingDetections.Count - 1) Then
      With MyWorkingDetections(DetectionIndex)
        lblFile.Text = .File
        lblImageFile.Text = .ImageFileName
        lblFirstTriggered.Text = .FirstTriggered
        lblTimeGap.Text = .TimeGap.ToString &" ms."
        lblDetector1.Text = Format(.Entering1, "yyyy-MM-dd   HH:mm:ss.fff") & " - " & Format(.Exiting1, "HH:mm:ss.fff") & " (" & .Duration1.ToString & ")"
                lblDetector2.Text = Format(.Entering2, "yyyy-MM-dd   HH:mm:ss.fff") & " - " & Format(.Exiting2, "HH:mm:ss.fff") & " (" & .Duration2.ToString & ")"

                If .Status = 0 Then
                    btnCorrectDetection.Visible = True
                    btnNotSure.Visible = True
                    btnFalseDetection.Visible = True
                    btnStatus.Visible = False
                Else
                    btnCorrectDetection.Visible = False
                    btnNotSure.Visible = False
                    btnFalseDetection.Visible = False
                    btnStatus.Visible = True
                    btnStatus.ImageIndex = .Status
                End If

                lblRecordCount.Text = (MyCurrentDetectionIndex + 1).ToString & " of " & MyWorkingDetections.Count & " (" & MyOriginalDetections.Count.ToString & ")"

        _ImageFilePath = MyInputFolder & "\" & MyWorkingDetections(DetectionIndex).ImageFileName
        If IO.File.Exists(_ImageFilePath) Then
          _Bitmap = New Bitmap(_ImageFilePath)
                    picImage.Image = _Bitmap
                    'picImage.Width = _Bitmap.Width
                    'picImage.Height = _Bitmap.Height
                    'Me.Width = picImage.Right + 30
                    'Me.Height = picImage.Bottom + 50
                Else
          picImage.Image = Nothing
        End If
      End With

            grpRecordData.Enabled = True
            pnlNavigation.Enabled = True
            OtherData.Visible = True
            OtherData.Enabled = True
            picImage.Enabled = True

            If MyOriginalDetections(MyCurrentDetectionIndex).other_data IsNot "" Then

                Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
                Chosen.Visible = True
                Chosen.Enabled = True
            Else
                Chosen.Text = "Chosen"
                Chosen.Visible = False
                Chosen.Enabled = False
            End If

            'btnFalseDetection.Visible = False
            'btnCorrectDetection.Visible = False
            'btnNotSure.Visible = False
            'btnStatus.Visible = True

        Else
            grpRecordData.Enabled = False
            pnlNavigation.Enabled = False
            OtherData.Visible = False
            Other_data.Visible = False
            Other_data.Enabled = False
            OutputName.Visible = False
            OutputName.Enabled = False
            picImage.Enabled = False
            picImage.Enabled = False
            picImage.Image = Nothing
            lblRecordCount.Text = (MyCurrentDetectionIndex + 1).ToString & " of " & MyWorkingDetections.Count & " (" & MyOriginalDetections.Count.ToString & ")"
      MsgBox("No records to display.")
    End If

  End Sub


  Private Sub UpdateDetectionStatus(ByVal DetectionIndex As Integer, ByVal Status As clsDoubleDetection.DetectionStatuses)
    Dim _ImageFilePath,
        _DestCorrectFilePath,
        _DestFalseFilePath,
        _DestNotSureFilePath As String


    _ImageFilePath = MyInputFolder & "\" & MyWorkingDetections(DetectionIndex).ImageFileName
    _DestCorrectFilePath = MyInputFolder & "\Results\Correct\" & MyWorkingDetections(DetectionIndex).ImageFileName
    _DestFalseFilePath = MyInputFolder & "\Results\False alarms\" & MyWorkingDetections(DetectionIndex).ImageFileName
    _DestNotSureFilePath = MyInputFolder & "\Results\Not sure\" & MyWorkingDetections(DetectionIndex).ImageFileName

    Try
      File.Delete(_DestCorrectFilePath)
      File.Delete(_DestFalseFilePath)
      File.Delete(_DestNotSureFilePath)

      Select Case Status
        Case clsDoubleDetection.DetectionStatuses.Correct
          File.Copy(_ImageFilePath, _DestCorrectFilePath)
        Case clsDoubleDetection.DetectionStatuses.FalseDetection
          File.Copy(_ImageFilePath, _DestFalseFilePath)
        Case clsDoubleDetection.DetectionStatuses.NotSure
          File.Copy(_ImageFilePath, _DestNotSureFilePath)
      End Select

      ' btnStatus.ImageIndex = Status
      If Status = 0 Then
        btnCorrectDetection.Visible = True
        btnNotSure.Visible = True
        btnFalseDetection.Visible = True
        btnStatus.Visible = False
      Else
        btnCorrectDetection.Visible = False
        btnNotSure.Visible = False
        btnFalseDetection.Visible = False
        btnStatus.Visible = True
        btnStatus.ImageIndex = Status
      End If
      MyWorkingDetections(DetectionIndex).Status = Status

    Catch ex As Exception
      MsgBox("Sorry, I failed...")
    End Try


    'btnFalseDetection.Visible = False
    'btnCorrectDetection.Visible = False
    'btnNotSure.Visible = False
    'btnStatus.Visible = True

  End Sub


  Private Sub CreateSummaryFiles()
    Dim _FileIndex As Integer
    Dim _FilePath As String


    _FileIndex = FreeFile()

        If OtherData.CheckState = 1 Then
            _FilePath = MyInputFolder & "\Results\" & OutputName.Text
            MsgBox("Printing to: " & _FilePath)
            Try
                FileOpen(_FileIndex, _FilePath, OpenMode.Output)
            Catch ex As Exception
                MsgBox("The file '" & _FilePath & "' seems to be open already. Close the file before atempting to re-write a new verssion of it.")
                Exit Sub
            End Try

            If setting = 1 Then
                PrintLine(_FileIndex, "File;Date;Entering;Leaving;Duration;Frame;Additional Data")
            Else
                PrintLine(_FileIndex, "File;Date;Entering Detector 1;Leaving Detector 1;Duration;Entering Detector 2;Leaving Detector 2;Duration;Time Gap 1-2;First Triggered;Frame;Additional Data")
            End If

            For Each _Detection In MyOriginalDetections
                With _Detection
                    If setting = 1 Then
                        If File.Exists(MyInputFolder & "\" & .ImageFileName) And IsBetweenOrdered(_Detection.TimeGap, MyLowerThreshold, MyUpperThreshold) Then
                            PrintLine(_FileIndex, .File & ";" &
                                        .DetectionDate.Year.ToString & " " & .DetectionDate.Month.ToString("00") & " " & .DetectionDate.Day.ToString("00") & ";" &
                                        .Entering1.Year.ToString & " " & .Entering1.Month.ToString("00") & " " & .Entering1.Day.ToString("00") & " " & .Entering1.Hour.ToString("00") & ":" & .Entering1.Minute.ToString("00") & ":" & .Entering1.Second.ToString("00") & "." & .Entering1.Millisecond.ToString("000") & ";" &
                                        .Exiting1.Year.ToString & " " & .Exiting1.Month.ToString("00") & " " & .Exiting1.Day.ToString("00") & " " & .Exiting1.Hour.ToString("00") & ":" & .Exiting1.Minute.ToString("00") & ":" & .Exiting1.Second.ToString("00") & "." & .Exiting1.Millisecond.ToString("000") & ";" &
                                        .Duration1.ToString & ";" &
                                        .ImageFileName & ";" & .other_data)
                        End If
                    Else
                        If File.Exists(MyInputFolder & "\" & .ImageFileName) And IsBetweenOrdered(_Detection.TimeGap, MyLowerThreshold, MyUpperThreshold) Then
                            PrintLine(_FileIndex, .File & ";" &
                                        .DetectionDate.Year.ToString & " " & .DetectionDate.Month.ToString("00") & " " & .DetectionDate.Day.ToString("00") & ";" &
                                        .Entering1.Year.ToString & " " & .Entering1.Month.ToString("00") & " " & .Entering1.Day.ToString("00") & " " & .Entering1.Hour.ToString("00") & ":" & .Entering1.Minute.ToString("00") & ":" & .Entering1.Second.ToString("00") & "." & .Entering1.Millisecond.ToString("000") & ";" &
                                        .Exiting1.Year.ToString & " " & .Exiting1.Month.ToString("00") & " " & .Exiting1.Day.ToString("00") & " " & .Exiting1.Hour.ToString("00") & ":" & .Exiting1.Minute.ToString("00") & ":" & .Exiting1.Second.ToString("00") & "." & .Exiting1.Millisecond.ToString("000") & ";" &
                                        .Duration1.ToString & ";" &
                                        .Entering2.Year.ToString & " " & .Entering2.Month.ToString("00") & " " & .Entering2.Day.ToString("00") & " " & .Entering2.Hour.ToString("00") & ":" & .Entering2.Minute.ToString("00") & ":" & .Entering2.Second.ToString("00") & "." & .Entering2.Millisecond.ToString("000") & ";" &
                                        .Exiting2.Year.ToString & " " & .Exiting2.Month.ToString("00") & " " & .Exiting2.Day.ToString("00") & " " & .Exiting2.Hour.ToString("00") & ":" & .Exiting2.Minute.ToString("00") & ":" & .Exiting2.Second.ToString("00") & "." & .Exiting2.Millisecond.ToString("000") & ";" &
                                        .Duration2.ToString & ";" &
                                        .TimeGap.ToString & ";" &
                                        .FirstTriggered & ";" &
                                        .ImageFileName & ";" & .other_data)
                        End If
                    End If
                End With
            Next

        Else
            _FilePath = MyInputFolder & "\Results\Correct\" & IO.Path.GetFileNameWithoutExtension(MyInputFile) & "_Correct (" & MyLowerThreshold.ToString & " - " & MyUpperThreshold.ToString & ").csv"
            Try
                FileOpen(_FileIndex, _FilePath, OpenMode.Output)
            Catch ex As Exception
                MsgBox("The file '" & _FilePath & "' seems to be open already. Close the file before atempting to re-write a new verssion of it.")
                Exit Sub
            End Try

            If setting = 1 Then
                PrintLine(_FileIndex, "File;Date;Entering;Leaving;Duration;Frame")
            Else
                PrintLine(_FileIndex, "File;Date;Entering Detector 1;Leaving Detector 1;Duration;Entering Detector 2;Leaving Detector 2;Duration;Time Gap 1-2;First Triggered;Frame")
            End If

            For Each _Detection In MyOriginalDetections
                With _Detection
                    If setting = 1 Then
                        If File.Exists(MyInputFolder & "\Results\Correct\" & .ImageFileName) And IsBetweenOrdered(_Detection.TimeGap, MyLowerThreshold, MyUpperThreshold) Then
                            PrintLine(_FileIndex, .File & ";" &
                                        .DetectionDate.Year.ToString & " " & .DetectionDate.Month.ToString("00") & " " & .DetectionDate.Day.ToString("00") & ";" &
                                        .Entering1.Year.ToString & " " & .Entering1.Month.ToString("00") & " " & .Entering1.Day.ToString("00") & " " & .Entering1.Hour.ToString("00") & ":" & .Entering1.Minute.ToString("00") & ":" & .Entering1.Second.ToString("00") & "." & .Entering1.Millisecond.ToString("000") & ";" &
                                        .Exiting1.Year.ToString & " " & .Exiting1.Month.ToString("00") & " " & .Exiting1.Day.ToString("00") & " " & .Exiting1.Hour.ToString("00") & ":" & .Exiting1.Minute.ToString("00") & ":" & .Exiting1.Second.ToString("00") & "." & .Exiting1.Millisecond.ToString("000") & ";" &
                                        .Duration1.ToString & ";" &
                                        .ImageFileName)
                        End If

                    Else
                        If File.Exists(MyInputFolder & "\Results\Correct\" & .ImageFileName) And IsBetweenOrdered(_Detection.TimeGap, MyLowerThreshold, MyUpperThreshold) Then
                            PrintLine(_FileIndex, .File & ";" &
                                        .DetectionDate.Year.ToString & " " & .DetectionDate.Month.ToString("00") & " " & .DetectionDate.Day.ToString("00") & ";" &
                                        .Entering1.Year.ToString & " " & .Entering1.Month.ToString("00") & " " & .Entering1.Day.ToString("00") & " " & .Entering1.Hour.ToString("00") & ":" & .Entering1.Minute.ToString("00") & ":" & .Entering1.Second.ToString("00") & "." & .Entering1.Millisecond.ToString("000") & ";" &
                                        .Exiting1.Year.ToString & " " & .Exiting1.Month.ToString("00") & " " & .Exiting1.Day.ToString("00") & " " & .Exiting1.Hour.ToString("00") & ":" & .Exiting1.Minute.ToString("00") & ":" & .Exiting1.Second.ToString("00") & "." & .Exiting1.Millisecond.ToString("000") & ";" &
                                        .Duration1.ToString & ";" &
                                        .Entering2.Year.ToString & " " & .Entering2.Month.ToString("00") & " " & .Entering2.Day.ToString("00") & " " & .Entering2.Hour.ToString("00") & ":" & .Entering2.Minute.ToString("00") & ":" & .Entering2.Second.ToString("00") & "." & .Entering2.Millisecond.ToString("000") & ";" &
                                        .Exiting2.Year.ToString & " " & .Exiting2.Month.ToString("00") & " " & .Exiting2.Day.ToString("00") & " " & .Exiting2.Hour.ToString("00") & ":" & .Exiting2.Minute.ToString("00") & ":" & .Exiting2.Second.ToString("00") & "." & .Exiting2.Millisecond.ToString("000") & ";" &
                                        .Duration2.ToString & ";" &
                                        .TimeGap.ToString & ";" &
                                        .FirstTriggered & ";" &
                                        .ImageFileName)
                        End If
                    End If
                End With
            Next

        End If
        FileClose(_FileIndex)

    Process.Start("explorer.exe", Path.GetDirectoryName(_FilePath))

  End Sub



  Private Sub btnJumpBack_Click(sender As Object, e As EventArgs) Handles btnJumpBack.Click
    If IsBetweenOrdered(MyCurrentDetectionIndex - 25, 0, MyWorkingDetections.Count - 1) Then
      MyCurrentDetectionIndex = MyCurrentDetectionIndex - 25
      Call DisplayDetection(MyCurrentDetectionIndex)
    End If
  End Sub

  Private Sub btnPrevious_Click(sender As Object, e As EventArgs) Handles btnPrevious.Click
    Call PreviousRecord()
  End Sub

  Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
    Call NextRecord()
  End Sub

  Private Sub btnJumpForward_Click(sender As Object, e As EventArgs) Handles btnJumpForward.Click
    If IsBetweenOrdered(MyCurrentDetectionIndex + 25, 0, MyWorkingDetections.Count - 1) Then
      MyCurrentDetectionIndex = MyCurrentDetectionIndex + 25
      Call DisplayDetection(MyCurrentDetectionIndex)
    End If
  End Sub





  Private Sub btnStatus_Click(sender As Object, e As EventArgs) Handles btnStatus.Click
    btnStatus.Visible = False

    btnFalseDetection.Visible = True
    btnCorrectDetection.Visible = True
    btnNotSure.Visible = True
    Call UpdateDetectionStatus(MyCurrentDetectionIndex, clsDoubleDetection.DetectionStatuses.NotChecked)
  End Sub
  Private Sub FalseDetection()
    Call UpdateDetectionStatus(MyCurrentDetectionIndex, clsDoubleDetection.DetectionStatuses.FalseDetection)
  End Sub
  Private Sub NotSure()
    Call UpdateDetectionStatus(MyCurrentDetectionIndex, clsDoubleDetection.DetectionStatuses.NotSure)
  End Sub

  Private Sub CorrectDetection()
    Call UpdateDetectionStatus(MyCurrentDetectionIndex, clsDoubleDetection.DetectionStatuses.Correct)
  End Sub


  Private Sub NextRecord()
    If IsBetweenOrdered(MyCurrentDetectionIndex + 1, 0, MyWorkingDetections.Count - 1) Then
      MyCurrentDetectionIndex = MyCurrentDetectionIndex + 1
      Call DisplayDetection(MyCurrentDetectionIndex)
    End If
  End Sub

  Private Sub PreviousRecord()
    If IsBetweenOrdered(MyCurrentDetectionIndex - 1, 0, MyWorkingDetections.Count - 1) Then
      MyCurrentDetectionIndex = MyCurrentDetectionIndex - 1
      Call DisplayDetection(MyCurrentDetectionIndex)
    End If
  End Sub

  Private Sub btnCorrectDetection_Click(sender As Object, e As EventArgs) Handles btnCorrectDetection.Click
    Call CorrectDetection()
  End Sub

  Private Sub btnFalseDetection_Click(sender As Object, e As EventArgs) Handles btnFalseDetection.Click
    Call FalseDetection()
  End Sub

  Private Sub btnNotSure_Click(sender As Object, e As EventArgs) Handles btnNotSure.Click
    Call NotSure()
  End Sub

  Private Sub btnNotChecked_Click(sender As Object, e As EventArgs)
    Call UpdateDetectionStatus(MyCurrentDetectionIndex, clsDoubleDetection.DetectionStatuses.NotChecked)
  End Sub

  Private Sub mnuDataFilterCorrect_Click(sender As Object, e As EventArgs) Handles mnuDataFilterCorrect.Click
    Call SelectWorkDetections(clsDoubleDetection.DetectionStatuses.Correct)
  End Sub

  Private Sub mnuDataFilterFalseAlarms_Click(sender As Object, e As EventArgs) Handles mnuDataFilterFalseAlarms.Click
    Call SelectWorkDetections(clsDoubleDetection.DetectionStatuses.FalseDetection)
  End Sub

  Private Sub mnuDataFilterNotSure_Click(sender As Object, e As EventArgs) Handles mnuDataFilterNotSure.Click
    Call SelectWorkDetections(clsDoubleDetection.DetectionStatuses.NotSure)
  End Sub

  Private Sub mnuDataFilterNotChecked_Click(sender As Object, e As EventArgs) Handles mnuDataFilterNotChecked.Click
    Call SelectWorkDetections(clsDoubleDetection.DetectionStatuses.NotChecked)
  End Sub

  Private Sub SummaryFilseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SummaryFilseToolStripMenuItem.Click
    Call CreateSummaryFiles()
  End Sub


  Private Sub SetGapTimeInterval()
    Dim _Form As frmGapWindow

    _Form = New frmGapWindow
    _Form.LowerThreshold = MyLowerThreshold
    _Form.UpperThreshold = MyUpperThreshold
    _Form.txtLowerThreshold.Text = MyLowerThreshold.ToString()
    _Form.txtUpperThreshold.Text = MyUpperThreshold.ToString()

    _Form.ShowDialog(Me)

    MyLowerThreshold = _Form.LowerThreshold
    MyUpperThreshold = _Form.UpperThreshold

    mnuGapTimeWindow.Text = "Time Gap interval (" & MyLowerThreshold.ToString() & " ; " & MyUpperThreshold.ToString() & ")"

    _Form.Dispose()


  End Sub


  Private Sub RemoveFilterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuGapTimeWindow.Click
    Call SetGapTimeInterval()
    Call SelectWorkDetections(clsDoubleDetection.DetectionStatuses.NotChecked)
  End Sub

    Private Sub frmMain_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown

        If OtherData.CheckState = 1 Then

            If e.KeyCode = Keys.A Then
                Call FalseDetection()
                Call NextRecord()
            ElseIf e.KeyCode = Keys.S Then
                Call NotSure()
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D Then
                Call CorrectDetection()
                Call NextRecord()
            ElseIf e.KeyCode = Keys.Left Then
                Call PreviousRecord()
            ElseIf e.KeyCode = Keys.Right Then
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D1 Or e.KeyCode = Keys.NumPad1 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox1.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D2 Or e.KeyCode = Keys.NumPad2 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox2.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D3 Or e.KeyCode = Keys.NumPad3 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox3.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D4 Or e.KeyCode = Keys.NumPad4 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox4.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D5 Or e.KeyCode = Keys.NumPad5 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox5.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D6 Or e.KeyCode = Keys.NumPad6 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox6.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D7 Or e.KeyCode = Keys.NumPad7 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox7.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D8 Or e.KeyCode = Keys.NumPad8 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox8.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D9 Or e.KeyCode = Keys.NumPad9 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox9.Text
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D0 Or e.KeyCode = Keys.NumPad0 Then
                MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox10.Text
                Call NextRecord()
            End If

        Else

            If e.KeyCode = Keys.A Then
                Call FalseDetection()
                Call NextRecord()
            ElseIf e.KeyCode = Keys.S Then
                Call NotSure()
                Call NextRecord()
            ElseIf e.KeyCode = Keys.D Then
                Call CorrectDetection()
                Call NextRecord()
            ElseIf e.KeyCode = Keys.Left Then
                Call PreviousRecord()
            ElseIf e.KeyCode = Keys.Right Then
                Call NextRecord()
            End If

        End If

    End Sub

    Private Sub MergeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MergeToolStripMenuItem.Click
    Dim _MergeProgect As clsMergingSingleProject

    _MergeProgect = New clsMergingSingleProject("F:\Aliaksei\Documents\Site1_21Apr\Site1_312_M1_car_events_21.04.2016.csv",
                                                "F:\Aliaksei\Documents\Site1_21Apr\312_car\",
                                                "F:\Aliaksei\Documents\Site1_21Apr\Site1_312_M2_cyclist_events_21.04.2016.csv",
                                                "F:\Aliaksei\Documents\Site1_21Apr\312_bicycle\")

    _MergeProgect.Merge("F:\Aliaksei\Documents\Site1_21Apr\Output\Output.csv",
                        "F:\Aliaksei\Documents\Site1_21Apr\Output\Images\")

  End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox1.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox2.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox3.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox4.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox5.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox6.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox7.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox8.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox9.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub Button0_Click(sender As Object, e As EventArgs) Handles Button0.Click
        MyOriginalDetections(MyCurrentDetectionIndex).other_data = TextBox10.Text
        Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
        Chosen.Visible = True
        Chosen.Enabled = True
    End Sub

    Private Sub OtherData_CheckedChanged(sender As Object, e As EventArgs) Handles OtherData.CheckedChanged

        If OtherData.Checked Then
            Other_data.Visible = True
            Other_data.Enabled = True

            If MyOriginalDetections(MyCurrentDetectionIndex).other_data IsNot "" Then

                Chosen.Text = MyOriginalDetections(MyCurrentDetectionIndex).other_data
                Chosen.Visible = True
                Chosen.Enabled = True
            Else
                Chosen.Text = "Chosen"
                Chosen.Visible = False
                Chosen.Enabled = False
            End If

            OutputName.Enabled = True
            OutputName.Visible = True
            ChangeText.Visible = True
            ChangeText.Enabled = True
        Else
            Other_data.Visible = False
            Other_data.Enabled = False
            OutputName.Visible = False
            OutputName.Enabled = False
            ChangeText.Visible = False
            ChangeText.Enabled = False
        End If

    End Sub

    Private Sub ChangeText_Click(sender As Object, e As EventArgs) Handles ChangeText.Click

        Dim _Form As frmChangeText

        _Form = New frmChangeText
        _Form.Show()

    End Sub

    Private Sub Chosen_Click(sender As Object, e As EventArgs) Handles Chosen.Click

        MyOriginalDetections(MyCurrentDetectionIndex).other_data = ""
        Chosen.Enabled = False
        Chosen.Visible = False

    End Sub

    Private Sub NumberOfDetectorsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NumberOfDetectorsToolStripMenuItem.Click

        Dim sett As frmSetting
        sett = New frmSetting
        sett.Show()

    End Sub

    Private Sub Num_Detectors_Click(sender As Object, e As EventArgs) Handles Num_Detectors.Click

        If setting = 2 Then

            setting = 1
            Num_Detectors.Text = "1 Detector"

        Else

            setting = 2
            Num_Detectors.Text = "2 Detectors"

        End If

    End Sub

End Class
