#include "singlemodule.h"


using namespace std;
using namespace cv;

using namespace ruba;


SingleModule::SingleModule(BaseDetectorParams baseParams, QWidget *parent, QString configFile)
    : AbstractCompoundDetector(configureBaseDetectorParams(baseParams, SINGLEMODULE))
{
    // Find the configuration of the single module
    bool showGui = configFile.isEmpty();


    DetectorTypeSelectionDialog *tSDialog = new DetectorTypeSelectionDialog(parent, 1);
    tSDialog->setWindowTitle("Single Module Detector");

    // If we have input a configFile to the SingleModule, do not show the DetectorTypeSelectionDialog.
    // Instead, prompt the dialog to determine which detector type should populate the SingleModule
    // according to the configFile
    bool goodConfig = false;

    if (showGui)
    {
        goodConfig = tSDialog->exec();
    } else {
        int res = tSDialog->LoadConfiguration(configFile);

        if (res == 0)
        {
            goodConfig = true;
        }
    }

    if (goodConfig)
    {
        QVector<int> chosenDetector = tSDialog->getOutput();

        if (chosenDetector.size() > 0)
        {
            IsInitialized = true;

            switch (chosenDetector[0])
            {
            case PRESENCEDETECTOR:
                setDetectorCount(1, 0, 0, 0);
                break;
            case MOVEMENTDETECTOR:
                setDetectorCount(0, 1, 0, 0);
                break;
            case STATIONARYDETECTOR:
                setDetectorCount(0, 0, 1, 0);
                break;
			case TRAFFICLIGHTDETECTOR:
                setDetectorCount(0, 0, 0, 1);
				break;
            default:
                IsInitialized = false;
            }
        } else {
            IsInitialized = false;
        }

    } else {
        IsInitialized = false;
    }

    configFile = tSDialog->getConfigurationFilename();
    delete tSDialog;

    windowIcon = new QIcon("://icons/layer.png");

    if (IsInitialized)
    {
        LoadConfiguration(baseParams.initFrames, parent, configFile, showGui);
    }
}

DetectorOutput SingleModule::GetOutput()
{
    DetectorOutput outputStruct;

    if (this->IsInitialized)
    {
        outputStruct.Confidence.push_back(1);
    } else {
        outputStruct.Confidence.push_back(0);
    }

    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = IsTriggered;

    if (generalLog != nullptr)
    {
        if (generalLog->getLastEnteringTime() > generalLog->getLastLeavingTime())
        {
            outputStruct.LastChange.push_back(generalLog->getLastEnteringTime());
        }
        else {
            outputStruct.LastChange.push_back(generalLog->getLastLeavingTime());
        }
    }

    outputStruct.Timestamp = getCurrentTime();

    return outputStruct;
}

void SingleModule::setDetectorSpecificDefaultParameters()
{
    InitLogSettings generalSettings = getGeneralLogSettings();

    setGeneralLogSettings(generalSettings);

    for (size_t i = 0; i < detectorParams.size(); ++i)
    {
        // Set common parameters
        detectorParams[i].DisableDetectorDuration = -1; // Time in ms. Deactivated pr. default
        detectorParams[i].DisableDetectorAfter = -1;
        generalSettings.entrySettings.maxTriggeredDuration[detectorParams[i].DetectorIdText] = 5000;
        detectorParams[i].views = getCurrentViews();
		detectorParams[i].EnableWrongDirection = false;
    }

    setGeneralLogSettings(generalSettings);

    disableAllDetectorsAt = getCurrentTime().addYears(100);

    detectorInfo.clear();

    size_t nbrModalities = frames.size();

    // Set flags of the SingleDetector
    IsTriggered.resize(1);
    IsTriggered[0] = false;
}

void SingleModule::updateDetectorSpecificLogic()
{
    /****** Update the internal logic of the SingleModule detector:  ******/

    if (detectorInfo[0].IsTriggered)
    {
        this->IsTriggered[0] = true;
    } else {
        this->IsTriggered[0] = false;
    }

    if ((detectorInfo[0].IsTriggered == true) && (getPreviousDetectorInfo()[0].IsTriggered == false))
    {
        // Check if the state has changed            
        generalLog->updateLog(getCurrentTime(), frames, Transition::ENTERING,
            Utils::getDetectorAbbreviation(getDetectorType()),
            maskOutlines[detectorInfo[0].getDetectorId()]);
        
    } else if ((detectorInfo[0].IsTriggered == false) && (getPreviousDetectorInfo()[0].IsTriggered == true))
    {
        // Check if the state has changed        
        // If it has, an object has just left the conflict zone
        generalLog->updateLog(getCurrentTime(), frames, Transition::LEAVING,
            Utils::getDetectorAbbreviation(getDetectorType()),
            maskOutlines[detectorInfo[0].getDetectorId()]);
        
    }
}

QList<QString> SingleModule::getDetectorSpecificExtendedDebugInfo()
{
    QList<QString> extDebugList;
    return extDebugList;
}