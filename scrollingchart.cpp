#include "scrollingchart.h"

ScrollingChart::ScrollingChart(int width, int height, ViewInfo view)
{
    this->ChartWidth = width;
    this->ChartHeight = height;

    this->LineColorOver = cv::Scalar::all(0);
    this->LineColorUnder = cv::Scalar::all(150);
    this->BackgroundColor = cv::Scalar::all(255);
    this->view = view;

    ColorThreshold = 0;
    l0 = 0;
    l1 = 0;

//    for (int i = 0; i < this->ChartWidth; ++i)
//    {
//        DataQueue.enqueue(0);
//    }
    DataQueue.enqueue(0);

    this->ChartImage = new cv::Mat(this->ChartHeight, this->ChartWidth, CV_8UC3, this->BackgroundColor);

}

void ScrollingChart::AddDataPoint(int datapointY)
{
    DataQueue.enqueue(datapointY);
    DataQueue.dequeue();
}

int ScrollingChart::GetDataPoint()
{
    int dataPoint;
    if (!DataQueue.empty())
    {
        dataPoint = DataQueue.last();
    }

    return dataPoint;
}

ViewInfo ScrollingChart::GetView()
{
    return this->view;
}

cv::Mat ScrollingChart::GetChartImage()
{
    // Move all pixels 1 pixel to the left
    int channels = ChartImage->channels();
    int bytewidth = this->ChartWidth*channels;
    for( int i = 0; i < this->ChartHeight; i++ )
    {
        // when the arrays are continuous,
        // the outer loop is executed only once
        uchar* ptr = ChartImage->ptr<uchar>(i);

        for( int j = 0; j < bytewidth-channels; j += channels )
        {
            ptr[j] = ptr[j + channels + 0];
            ptr[j + 1] = ptr[j + channels + 1];
            ptr[j + 2] = ptr[j + channels + 2];
        }
    }

    // Draw the new data points
    cv::Point2i top(this->ChartWidth-1, 0);
    cv::Point2i bottom(this->ChartWidth-1, this->ChartHeight-1);

    cv::line(*ChartImage, top, bottom, this->BackgroundColor);

    int y = DataQueue[0];
    int x = this->ChartWidth-1;
    if (y > this->ChartHeight-1) y = this->ChartHeight-1;
    cv::Point2i ytop(x,this->ChartHeight-y);
    cv::line(*ChartImage, ytop, bottom, this->LineColorUnder);

    if (y > this->ColorThreshold)
    {
        cv::Point2i ythres(x, this->ChartHeight-this->ColorThreshold);
        cv::line(*ChartImage, ytop, ythres, this->LineColorOver);
    }

    if (l0 > 0)
    {
        int invL0 = this->ChartHeight-l0;
        ChartImage->at<cv::Vec3b>(invL0,x)[0] = 255;
        ChartImage->at<cv::Vec3b>(invL0,x)[1] = 255;
        ChartImage->at<cv::Vec3b>(invL0,x)[2] = 255;
    }

    if (l1 > 0)
    {
        if (currentLineState > 0)
        {
            int invL1 = this->ChartHeight-l1;
            ChartImage->at<cv::Vec3b>(invL1,x)[0] = 255;
            ChartImage->at<cv::Vec3b>(invL1,x)[1] = 255;
            ChartImage->at<cv::Vec3b>(invL1,x)[2] = 255;

            if (currentLineState > 2)
            {
                currentLineState = -2;
            }
            currentLineState++;
        } else {
            currentLineState++;
        }
    }

    return *ChartImage;
}

QString ScrollingChart::GetTitle()
{
    return this->Title;
}

void ScrollingChart::SetTitle(QString title)
{
    this->Title = title;
}

void ScrollingChart::SetThreshold(double threshold)
{
    this->ColorThreshold = threshold;
}

void ScrollingChart::SetVisualLineThresholds(int l0, int l1)
{
    this->l0 = l0;
    this->l1 = l1;
    this->currentLineState = -2;
}

double ScrollingChart::GetThreshold()
{
    return this->ColorThreshold;
}

void ScrollingChart::SetBackground(cv::Scalar color)
{
    this->BackgroundColor = color;
}

cv::Scalar ScrollingChart::GetBackground()
{
    return this->BackgroundColor;
}

void ScrollingChart::SetLineUnder(cv::Scalar color)
{
    this->LineColorUnder = color;
}

void ScrollingChart::SetLineOver(cv::Scalar color)
{
    this->LineColorOver = color;
}

DoubleScrollingChart::DoubleScrollingChart(int width, int height)
{
    this->ChartWidth = width;
    this->ChartHeight = height;

    this->LineColorOver1 = cv::Scalar::all(0);
    this->LineColorUnder1 = cv::Scalar::all(150);
    this->LineColorOver2 = cv::Scalar::all(0);
    this->LineColorUnder2 = cv::Scalar::all(150);

    this->BackgroundColor = cv::Scalar::all(255);

    ColorThreshold1 = 0;
    ColorThreshold2 = 0;

//    for (int i = 0; i < this->ChartWidth; ++i)
//    {
//        DataQueue.enqueue(0);
//    }
    DataQueue1.enqueue(0);
    DataQueue2.enqueue(0);

    this->ChartImage = new cv::Mat(this->ChartHeight, this->ChartWidth, CV_8UC3, this->BackgroundColor);
}

void DoubleScrollingChart::AddDataPoints(int datapointY1, int datapointY2)
{
    DataQueue1.enqueue(datapointY1);
    DataQueue1.dequeue();

    DataQueue2.enqueue(datapointY2);
    DataQueue2.dequeue();
}

cv::Mat DoubleScrollingChart::GetChartImage()
{
    // Move all pixels 1 pixel to the left
    int channels = ChartImage->channels();
    int bytewidth = this->ChartWidth*channels;
    for( int i = 0; i < this->ChartHeight; i++ )
    {
        // when the arrays are continuous,
        // the outer loop is executed only once
        uchar* ptr = ChartImage->ptr<uchar>(i);

        for( int j = 0; j < bytewidth-channels; j += channels )
        {
            ptr[j] = ptr[j + channels + 0];
            ptr[j + 1] = ptr[j + channels + 1];
            ptr[j + 2] = ptr[j + channels + 2];
        }
    }

    // Draw the new data points
    cv::Point2i top(this->ChartWidth-1, 0);
    cv::Point2i bottom(this->ChartWidth-1, this->ChartHeight-1);

    cv::line(*ChartImage, top, bottom, this->BackgroundColor);

    // Draw data1 from bottom to the top
    int y = DataQueue1[0];
    int scaledY = y*100./ColorThreshold1;

    int x = this->ChartWidth-1;
    if (scaledY > ChartHeight/3)
    {
        scaledY = ChartHeight/3;
    }
    cv::Point2i ytop(x,this->ChartHeight-scaledY);
    cv::line(*ChartImage, ytop, bottom, this->LineColorUnder1);


    // Draw data2 from bottom to the top
    y = DataQueue2[0];
    if (y > 0)
    {
        scaledY = y*100./ColorThreshold2;
        x = this->ChartWidth-1;
        if (scaledY > (this->ChartHeight-1-ChartHeight/3))
        {
            scaledY = (this->ChartHeight-1-ChartHeight/3);
        }

        ytop.x = x;
        ytop.y = 2*this->ChartHeight/3-scaledY;
        bottom.y = 2*this->ChartHeight/3-1;
        cv::line(*ChartImage, ytop, bottom, this->LineColorUnder2);

        if (y > this->ColorThreshold2)
        {
            cv::line(*ChartImage, ytop, bottom, this->LineColorOver2);
        }
    }

    if (l0 > 0)
    {
        int invL0 = this->ChartHeight-l0;
        ChartImage->at<cv::Vec3b>(invL0,x)[0] = 255;
        ChartImage->at<cv::Vec3b>(invL0,x)[1] = 255;
        ChartImage->at<cv::Vec3b>(invL0,x)[2] = 255;
    }

    if (l1 > 0)
    {
        int invL1 = this->ChartHeight-l1;
        ChartImage->at<cv::Vec3b>(invL1,x)[0] = 255;
        ChartImage->at<cv::Vec3b>(invL1,x)[1] = 255;
        ChartImage->at<cv::Vec3b>(invL1,x)[2] = 255;
    }

    return *ChartImage;
}

QString DoubleScrollingChart::GetTitle()
{
    return this->Title;
}

void DoubleScrollingChart::SetTitle(QString title)
{
    this->Title = title;
}

void DoubleScrollingChart::SetThresholds(double threshold1, double threshold2)
{
    this->ColorThreshold1 = threshold1;
    this->ColorThreshold2 = threshold2;
}

void DoubleScrollingChart::SetBackground(cv::Scalar color)
{
    this->BackgroundColor = color;
}

cv::Scalar DoubleScrollingChart::GetBackground()
{
    return this->BackgroundColor;
}

void DoubleScrollingChart::SetLineUnder(cv::Scalar color1, cv::Scalar color2)
{
    this->LineColorUnder1 = color1;
    this->LineColorUnder2 = color2;
}

void DoubleScrollingChart::SetLineOver(cv::Scalar color1, cv::Scalar color2)
{
    this->LineColorOver1 = color1;
    this->LineColorOver2 = color2;
}

void DoubleScrollingChart::SetVisualLineThresholds(int l0, int l1)
{
    if (l0 > 0)
    {
        this->l0 = ChartHeight/3;
    } else {
        l0 = 0;
    }

    if (l1 > 0)
    {
        this->l1 = ChartHeight/3*2;
    } else {
        l1 = 0;
    }
}

ScrollingTLChart::ScrollingTLChart(int width, int height, ViewInfo view)
{
    this->ChartWidth = width;
    this->ChartHeight = height;


    this->BackgroundColor = cv::Scalar::all(255);
    this->view = view;

    DataQueue.enqueue(tlTypeClasses::AMBIGIOUS);

    this->ChartImage = new cv::Mat(this->ChartHeight, this->ChartWidth, CV_8UC3, this->BackgroundColor);
}

void ScrollingTLChart::AddDataPoint(tlTypeClasses type)
{
    DataQueue.enqueue(type);
    DataQueue.dequeue();
}

tlTypeClasses ScrollingTLChart::GetDataPoint()
{
    tlTypeClasses dataPoint;
    if (!DataQueue.empty())
    {
        dataPoint = DataQueue.last();
    }

    return dataPoint;
}

ViewInfo ScrollingTLChart::GetView()
{
    return this->view;
}

cv::Mat ScrollingTLChart::GetChartImage()
{
    // Move all pixels 1 pixel to the left
    int channels = ChartImage->channels();
    int bytewidth = this->ChartWidth*channels;
    for (int i = 0; i < this->ChartHeight; i++)
    {
        // when the arrays are continuous,
        // the outer loop is executed only once
        uchar* ptr = ChartImage->ptr<uchar>(i);

        for (int j = 0; j < bytewidth - channels; j += channels)
        {
            ptr[j] = ptr[j + channels + 0];
            ptr[j + 1] = ptr[j + channels + 1];
            ptr[j + 2] = ptr[j + channels + 2];
        }
    }

    // Draw the new data points
    cv::Point2i top(this->ChartWidth - 1, 0);
    cv::Point2i bottom(this->ChartWidth - 1, this->ChartHeight - 1);

    cv::line(*ChartImage, top, bottom, this->BackgroundColor);

    tlTypeClasses tlType = DataQueue[0];
    int x = this->ChartWidth - 1;

    

    switch (tlType)
    {
    case tlTypeClasses::RED: {
        cv::Point2i ytop(x, (this->ChartHeight / 3) * 1);
        cv::Point2i ythres(x, (this->ChartHeight/3)*0 );
        cv::line(*ChartImage, ytop, ythres, cv::Scalar(0,0,255) );
        break;
    }
    case tlTypeClasses::YELLOW: {
        cv::Point2i ytop(x, (this->ChartHeight / 3) * 2);
        cv::Point2i ythres(x, (this->ChartHeight / 3)* 1 );
        cv::line(*ChartImage, ytop, ythres, cv::Scalar(0, 255, 255));
        break;
    }
    case tlTypeClasses::REDYELLOW : {
        // Red
        cv::Point2i ytop(x, (this->ChartHeight / 3) * 2);
        cv::Point2i ythresRed(x, (this->ChartHeight / 3)* 0);
        cv::line(*ChartImage, ytop, ythresRed, cv::Scalar(0, 0, 255));
        // Yellow

        cv::Point2i ythresYellow(x,  (this->ChartHeight / 3) * 1);
        cv::line(*ChartImage, ytop, ythresYellow, cv::Scalar(0, 255, 255));
        break;
    }
    case tlTypeClasses::GREEN: {
        cv::Point2i ytop(x, this->ChartHeight);
        cv::Point2i ythres(x, (this->ChartHeight / 3) * 2);
        cv::line(*ChartImage, ytop, ythres, cv::Scalar(0, 255, 0));
        break;
    }
    default:
        break;
    }

    return *ChartImage;
}

QString ScrollingTLChart::GetTitle()
{
    return this->Title;
}

void ScrollingTLChart::SetTitle(QString title)
{
    this->Title = title;
}
