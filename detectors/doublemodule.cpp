#include "doublemodule.h"


using namespace std;
using namespace cv;

using namespace ruba;


DoubleModule::DoubleModule(BaseDetectorParams baseParams, QWidget *parent, QString configFile)
    : AbstractCompoundDetector(configureBaseDetectorParams(baseParams, DOUBLEMODULE))
{
    // Find the configuration of the double module
    bool showGui = configFile.isEmpty();


    DetectorTypeSelectionDialog *tSDialog = new DetectorTypeSelectionDialog(parent, 2);
    tSDialog->setWindowTitle("Double Module Detector");

    // If we have input a configFile to the DoubleModule, do not show the DetectorTypeSelectionDialog.
    // Instead, prompt the dialog to determine which detectors type should populate the DoubleModule
    // according to the configFile
    bool goodConfig = false;

    if (showGui)
    {
        goodConfig = tSDialog->exec();
    } else {
        int res = tSDialog->LoadConfiguration(configFile);

        if (res == 0)
        {
            goodConfig = true;
        }
    }

    if (goodConfig)
    {
        chosenDetectors = tSDialog->getOutput();

        if (chosenDetectors.size() > 1)
        {
            std::vector<DType> detectors;

            for (int i = 0; i < chosenDetectors.size(); ++i)
            {
                detectors.push_back(DType(chosenDetectors[i]));
            }

            setDetectorCount(detectors);
        } else {
            IsInitialized = false;
        }

        configFile = tSDialog->getConfigurationFilename();
        delete tSDialog;

    } else {
        IsInitialized = false;
    }

    windowIcon = new QIcon("://icons/layers.png");

    if (IsInitialized)
    {
        LoadConfiguration(baseParams.initFrames, parent, configFile, showGui);
    }
}


DoubleModule::~DoubleModule()
{

}

void DoubleModule::InitLogs()
{
}

DetectorOutput DoubleModule::GetOutput()
{
    DetectorOutput outputStruct;

    if (this->IsInitialized)
    {
        outputStruct.Confidence.push_back(1);
    } else {
        outputStruct.Confidence.push_back(0);
    }

    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = IsTriggered;

    if (generalLog != nullptr)
    {
        if (generalLog->getLastEnteringTime() > generalLog->getLastLeavingTime())
        {
            outputStruct.LastChange.push_back(generalLog->getLastEnteringTime());
        }
        else {
            outputStruct.LastChange.push_back(generalLog->getLastLeavingTime());
        }
    }

    outputStruct.Timestamp = getCurrentTime();

    return outputStruct;
}

void DoubleModule::setDetectorSpecificDefaultParameters()
{
    InitLogSettings generalSettings = getGeneralLogSettings();
    disableAllDetectorsAt = getCurrentTime().addYears(100); // Disable this feature

    detectorInfo.clear();
    
    for (size_t i = 0; i < detectorParams.size(); ++i)
    {
        // Set common parameters
        detectorParams[i].DisableDetectorDuration = -1; // Time in ms. Deactivated pr. default

        generalSettings.entrySettings.eventDelayIntervals[detectorParams[i].DetectorIdText].createEventMaxDelay = 2000;
        generalSettings.entrySettings.eventDelayIntervals[detectorParams[i].DetectorIdText].createEventMinDelay = 0;
        generalSettings.entrySettings.intervalType = { LEAVING, ENTERING };
        generalSettings.entrySettings.maxTriggeredDuration[detectorParams[i].DetectorIdText] = 5000;
		detectorParams[i].EnableWrongDirection = false;
        detectorParams[i].DisableDetectorAfter = -1;
        

        detectorParams[i].views = getCurrentViews();
    }

    // Custom log header
    CustomLogHeader headerDetector1;
    headerDetector1.additionalTransitionText[Transition::ENTERING] = "Detector 1";
    headerDetector1.additionalTransitionText[Transition::LEAVING] = "Detector 1";
    CustomLogHeader headerDetector2;
    headerDetector2.additionalTransitionText[Transition::ENTERING] = "Detector 2";
    headerDetector2.additionalTransitionText[Transition::LEAVING] = "Detector 2";
    generalSettings.customLogHeader = { headerDetector1, headerDetector2 };

    setGeneralLogSettings(generalSettings);



    // Set flags of the DoubleDetector
    IsTriggered.resize(1);
    IsTriggered[0] = false;
}


LogInfoStruct DoubleModule::createNewEvent()
{
    LogInfoStruct newEvent;
    newEvent.logDataTime.resize(4);
    for (int i = 0; i < newEvent.logDataTime.size(); ++i)
    {
        newEvent.logDataTime[i] = QDateTime();
    }

    newEvent.logDataInt.resize(3);
    newEvent.logDataString.resize(1);
    newEvent.collateEventsUntil.resize(2);
    newEvent.collateEventsUntil[0] = QDateTime();
    newEvent.collateEventsUntil[1] = QDateTime();

    newEvent.maxActiveDuration.resize(2);
    newEvent.maxActiveDuration[0] = 0;
    newEvent.maxActiveDuration[1] = 0;

    return newEvent;
}

void DoubleModule::updateDetectorSpecificLogic()
{
    /****** Update the internal logic of the DoubleModule detector:  ******/

    IsTriggered[0] = false;
    for (auto i = 0; i < detectorInfo.size(); ++i)
    {
        if (detectorInfo[i].IsTriggered)
        {
            IsTriggered[0] = true;
        }
    }

    for (auto k = 0; k < detectorInfo.size(); ++k)
    {
        if (detectorInfo[k].IsTriggered && !getPreviousDetectorInfo()[k].IsTriggered)
        {
            // The detector has gone high. Inform the logger
            generalLog->updateLog(getCurrentTime(),
                frames,
                Transition::ENTERING,
                detectorInfo[k].getDetectorIdText(),
                maskOutlines[detectorInfo[k].getDetectorId()]);
        }
        else if (!detectorInfo[k].IsTriggered && getPreviousDetectorInfo()[k].IsTriggered)
        {
            // The detector has gone low. Inform the logger
            generalLog->updateLog(getCurrentTime(),
                frames,
                Transition::LEAVING,
                detectorInfo[k].getDetectorIdText(),
                maskOutlines[detectorInfo[k].getDetectorId()]);
        }
    }
}

QList<QString> DoubleModule::getDetectorSpecificExtendedDebugInfo()
{
    QList<QString> extDebugList;

    return extDebugList;
}
