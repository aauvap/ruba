#ifndef LEFTTURNINGCAR_H
#define LEFTTURNINGCAR_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>

#include "abstractcompounddetector.h"

class ruba::LeftTurningCar : public AbstractCompoundDetector
{

public:
    explicit LeftTurningCar(BaseDetectorParams baseParams, QWidget *parent = 0, QString configFile = "");

    ruba::DetectorOutput GetOutput();

private:
    void setDetectorSpecificDefaultParameters();

    QList<QString> getDetectorSpecificDebugInfo();
    QList<QString> getDetectorSpecificExtendedDebugInfo();
    void updateDetectorSpecificLogic();
    void setDetectorSpecificEnabledStates();

    // External flags that are accessible through a get function
    QDateTime EnteringConflictZone;
    QDateTime LeavingConflictZone;

};

#endif // LEFTTURNINGCAR_H
