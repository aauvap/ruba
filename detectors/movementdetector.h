#ifndef MOVEMENTDETECTOR_H
#define MOVEMENTDETECTOR_H


#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include <QDialog>
#include <QMessageBox>
#include <QFileInfoList>
#include <QFile>
#include <QFileDialog>
#include <QList>
#include <QDateTime>

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

#include <cmath>
#include <fstream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>

#include "ruba.h"
#include "abstractmultimodaldetector.h"
#include "scrollingchart.h"
#include "framestruct.h"
#include "ChartStruct.h"
#include "detectoroutputstruct.h"
#include "movementdetectorsinglemodal.h"


class ruba::MovementDetector : public AbstractMultiModalDetector{

public:
    // Constructor
    MovementDetector(BaseDetectorParams baseParams, std::vector<cv::Mat> masks, std::vector<std::vector<double>> minVecLengthThreshold,
                               std::vector<std::vector<cv::Point>> flowDirectionRange,
                               std::vector<std::vector<int>> triggerThreshold, bool UseScrollingChart,
                               int upperThresholdBoundMultiplier);
    
    QList<QString> getDetectorSpecificDebugInfo();
    void InitDetector();

    std::vector<cv::Mat *> GetChartImage();
    std::vector<QString*>  GetChartTitles();

    // Update the parameters individually
    void SetMinVecLengthThreshold(std::vector<std::vector<double> > minVecLengthThreshold);
    void SetFlowDirectionRange(std::vector<std::vector<cv::Point>> flowDirectionRange);
    void SetTriggerThreshold(std::vector<std::vector<int> > triggerThreshold);    

protected:
	virtual ChartColors initialiseChartColors();

private:

    bool IS_FIRST_RUN;
    std::vector<std::shared_ptr<ruba::MovementDetectorSingleModal> > MDetectors;

};


#endif // MOVEMENTDETECTOR_H
