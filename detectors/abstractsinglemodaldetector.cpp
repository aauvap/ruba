#include "abstractsinglemodaldetector.h"

using namespace std;
using namespace cv;

using namespace ruba;


AbstractSingleModalDetector::AbstractSingleModalDetector(BaseDetectorParams baseParams)
    : AbstractTrafficDetector(baseParams)
{
    if (!baseParams.initFrames.empty()) {
        view = baseParams.initFrames[0].viewInfo;
        frame = baseParams.initFrames[0];
    }

    Charts.clear();
}


AbstractSingleModalDetector::~AbstractSingleModalDetector()
{

}

void ruba::AbstractSingleModalDetector::updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput)
{
    if (frames.empty()) {
        return;
    }
    frame = frames[0];
    updateDetectorSpecifics();
}

void AbstractSingleModalDetector::SetMask(cv::Mat maskImage)
{
    mask = maskImage;

    InitDetector();

}


std::vector<ChartStruct> AbstractSingleModalDetector::GetChartStructs()
{

    vector<ChartStruct> outputCharts;
    for (size_t i = 0; i < Charts.size(); ++i)
    {
        ChartStruct chart;
        chart.Image = Charts[i].GetChartImage();
        chart.Title = Charts[i].GetTitle();
        chart.View = Charts[i].GetView();
        outputCharts.push_back(chart);
    }
    return outputCharts;
    

}

DetectorOutput AbstractSingleModalDetector::GetOutput()
{
    DetectorOutput outputStruct;

    outputStruct.Confidence = this->confidence;
    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = this->IsTriggered;
    outputStruct.LastChange = this->lastChange;
    outputStruct.Timestamp = getCurrentTime();

    return outputStruct;
}

void AbstractSingleModalDetector::SetChartLine(cv::Scalar colorUnder, Scalar colorOver)
{
    if (UseScrollingChart)
    {
        for (size_t i = 0; i < Charts.size(); ++i)
        {
            Charts[i].SetLineOver(colorOver);
            Charts[i].SetLineUnder(colorUnder);
        }
    }
}

void AbstractSingleModalDetector::SetChartBackground(cv::Scalar color)
{
    if (UseScrollingChart)
    {
        for (size_t i = 0; i < Charts.size(); ++i)
        {
            Charts[i].SetBackground(color);
        }
    }
}

int AbstractSingleModalDetector::GetChartData()
{
    return Charts[0].GetDataPoint();
}

double AbstractSingleModalDetector::GetChartThreshold()
{
    return Charts[0].GetThreshold();
}

void AbstractSingleModalDetector::SetUseScrollingChart(bool useScrollingChart)
{
    this->UseScrollingChart = useScrollingChart;
}

double AbstractSingleModalDetector::GetConfidence()
{
    return this->confidence[0];
}

std::vector<cv::Mat> AbstractSingleModalDetector::GetChartImage()
{
    if (UseScrollingChart)
    {
        vector<cv::Mat> tempImages;

        for (size_t i = 0; i < Charts.size(); ++i)
        {
            tempImages.push_back(Charts[i].GetChartImage());

        }
        return tempImages;

    }
    else
    {
        vector<cv::Mat> tempImages;
        tempImages.push_back(Mat());

        return tempImages;
    }
}

ViewInfo AbstractSingleModalDetector::getCurrentView()
{
    return view;
}
