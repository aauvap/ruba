#include "leftturningcar.h"

using namespace std;
using namespace cv;

using namespace ruba;


LeftTurningCar::LeftTurningCar(BaseDetectorParams baseParams, QWidget *parent, QString configFile)
    : AbstractCompoundDetector(configureBaseDetectorParams(baseParams, LEFTTURNINGCAR))
{
    // In order to identify left turning cars, we need 2 PresenceDetectors and 4 MovementDetector

    setDetectorCount(2, 4, 0, 0);

    windowIcon = new QIcon("://icons/arrow-turn-000-left.png");
    designImg.load("://illustrations/leftturningcars.png");

    LoadConfiguration(baseParams.initFrames, parent, configFile, false);
}

DetectorOutput LeftTurningCar::GetOutput()
{
    DetectorOutput outputStruct;

    if (this->IsInitialized)
    {
        outputStruct.Confidence.push_back(1);
    } else {
        outputStruct.Confidence.push_back(0);
    }

    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = IsTriggered;
    outputStruct.EnteringConflictZone = EnteringConflictZone;
    outputStruct.LeavingConflictZone = LeavingConflictZone;

    if (EnteringConflictZone > LeavingConflictZone)
    {
        outputStruct.LastChange.push_back(EnteringConflictZone);
    } else {
        outputStruct.LastChange.push_back(LeavingConflictZone);
    }

    outputStruct.Timestamp = getCurrentTime();


    return outputStruct;

}

void LeftTurningCar::setDetectorSpecificDefaultParameters()
{
    InitLogSettings generalSettings = getGeneralLogSettings();

    CustomLogHeader header;
    header.additionalTransitionText[Transition::ENTERING] = " CZ";
    header.additionalTransitionText[Transition::LEAVING] = " CZ";
    generalSettings.customLogHeader = { header }; // Entering CZ, Leaving CZ


    disableAllDetectorsAt = getCurrentTime();

    // Set detector specific thresholds
    for (size_t i = 0; i < detectorParams.size(); ++i)
    {
        if (((int)i == 1) && (detectorParams[i].DetectorType == PRESENCEDETECTOR))
        {
            detectorParams[i].DisableDetectorAfter = 300; // Wait for 300 ms before closing the E2
        }
        else {
            detectorParams[i].DisableDetectorAfter = 2000;
        }

        if ((i == 3 || i == 4 || i == 5) && (detectorParams[i].DetectorType == MOVEMENTDETECTOR))
        {
            detectorParams[i].DisableDetectorDuration = 1000; // F2 should disable all other detectors for x ms
        }
    }

    generalSettings.entrySettings.maxTriggeredDuration.clear(); // Feature not supported


    setGeneralLogSettings(generalSettings);


    // Set flags of the LeftTurningCarDetector
    IsTriggered.resize(1);
    IsTriggered[0] = false;
    EnteringConflictZone = QDateTime();
    LeavingConflictZone = QDateTime();
}

void LeftTurningCar::setDetectorSpecificEnabledStates()
{
    detectorInfo[0].IsEnabled = true; // The PresenceDetector E1 is always enabled
    if (detectorInfo.size() > 5)
    {
        detectorInfo[3].IsEnabled = true; // The MovementDetector F2 is always enabled
        detectorInfo[4].IsEnabled = true; // The MovementDetector F3 is always enabled
        detectorInfo[5].IsEnabled = true; // The MovementDetector F4 is always enabled
    }
}

void LeftTurningCar::updateDetectorSpecificLogic()
{
    /****** Update the internal logic of the LeftTurningCar detector:  ******/

    /***** Step 1: Based on the internal states of the detectors, set enable/disable flags of the detectors *****/

    // If PresenceDetector E1 is triggered, enable MovementDetector F1
    if (detectorInfo[0].IsTriggered == true)
    {
        detectorInfo[2].IsEnabled = true; // MovementDetector F1
        detectorInfo[2].DisableDetectorAt = getCurrentTime().addMSecs(detectorInfo[2].DisableDetectorAfter);
    }

    // If MovementDetector F1 is triggered, enable PresenceDetector E2
    if (detectorInfo[2].IsEnabled == true && detectorInfo[2].IsTriggered == true)
    {
        detectorInfo[1].IsEnabled = true; // PresenceDetector E2
        detectorInfo[1].DisableDetectorAt = getCurrentTime().addMSecs(detectorInfo[1].DisableDetectorAfter);
    }

    // If MovementDetector F2 is triggered, disable F1, E2 for x seconds
    if (detectorInfo[3].IsTriggered == true)
    {
        detectorInfo[1].DisableDetectorUntil = getCurrentTime().addMSecs(detectorInfo[3].DisableDetectorDuration);
        detectorInfo[2].DisableDetectorUntil = getCurrentTime().addMSecs(detectorInfo[3].DisableDetectorDuration);
    }

    // If MovementDetector F3 is triggered, disable F1, E2 for x seconds
    if (detectorInfo[4].IsTriggered == true)
    {
        detectorInfo[1].DisableDetectorUntil = getCurrentTime().addMSecs(detectorInfo[4].DisableDetectorDuration);
        detectorInfo[2].DisableDetectorUntil = getCurrentTime().addMSecs(detectorInfo[4].DisableDetectorDuration);
    }

    // If MovementDetector F4 is triggered, disable F1, E2 for x seconds
    if (detectorInfo[5].IsTriggered == true)
    {
        detectorInfo[1].DisableDetectorUntil = getCurrentTime().addMSecs(detectorInfo[5].DisableDetectorDuration);
        detectorInfo[2].DisableDetectorUntil = getCurrentTime().addMSecs(detectorInfo[5].DisableDetectorDuration);
    }


    /**** Step 2: Disable the detectors accordingly to the flags *****/
    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        if ((detectorInfo[i].DisableDetectorAt <= getCurrentTime()) && (detectorInfo[i].IsTriggered == false))
        {
            detectorInfo[i].IsEnabled = false;
        }
    }

    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        if (detectorInfo[i].DisableDetectorUntil > getCurrentTime())
        {
            detectorInfo[i].IsEnabled = false;
            detectorInfo[i].IsTriggered = false;
        }
    }

    if ((disableAllDetectorsAt <= getCurrentTime()))
    {
        for (size_t i = 1; i < detectorInfo.size(); ++i)
        {
            switch (i)
            {
            case 3:
                break;  // MovementDetector F2 is never disabled
            case 4:
                break;  // MovementDetector F3 is never disabled
            case 5:
                break;  // MovementDetector F4 is never disabled
            default:
                detectorInfo[i].IsEnabled = false;
            }
        }
    }

    /***** Step 3: Update the TrafficDetector and set the flags that may be accessed by the outside *****/

    if (detectorInfo[1].IsTriggered)
    {
        this->IsTriggered[0] = true;
    } else {
        this->IsTriggered[0] = false;
    }


    if (detectorInfo[1].IsEnabled == false && detectorInfo[1].IsTriggered)
    {
        this->EnteringConflictZone = getCurrentTime();
        // Entering the conflict zone

        generalLog->updateLog(getCurrentTime(), frames, Transition::ENTERING,
            detectorInfo[1].getDetectorIdText(), 
            maskOutlines[detectorInfo[1].getDetectorId()]);
    }

    if (detectorInfo[1].IsTriggered == true)
    {
        // Check if the state has changed
        if (getPreviousDetectorInfo()[1].IsTriggered == false)
        {
            generalLog->updateLog(getCurrentTime(), frames, Transition::ENTERING,
                detectorInfo[1].getDetectorIdText(),
                maskOutlines[detectorInfo[1].getDetectorId()]);
            this->EnteringConflictZone = getCurrentTime();
        }
    } else if (detectorInfo[1].IsTriggered == false)
    {
        // Check if the state has changed
        if (getPreviousDetectorInfo()[1].IsTriggered == true)
        {
            // If it has, a vehicle has just left the conflict zone
            generalLog->updateLog(getCurrentTime(), frames, Transition::LEAVING,
                detectorInfo[1].getDetectorIdText(),
                maskOutlines[detectorInfo[1].getDetectorId()]);
            this->LeavingConflictZone = getCurrentTime();

            this->detectorInfo[1].DisableDetectorAt = getCurrentTime().addMSecs(GetConsecutiveEventDelay()); // Disable detector E2 after the consecutive event delay
        }

    }
}

QList<QString> LeftTurningCar::getDetectorSpecificDebugInfo()
{
    QList<QString> debugList;

    debugList.push_back("Entering CZ: " + this->EnteringConflictZone.toString("hh:mm:ss:zzz"));
    debugList.push_back("Leaving CZ: " + this->LeavingConflictZone.toString("hh:mm:ss:zzz"));

    return debugList;
}

QList<QString> LeftTurningCar::getDetectorSpecificExtendedDebugInfo()
{
    QList<QString> extDebugList;

    return extDebugList;
}