#ifndef MASKHANDLEROUTPUTSTRUCT_H
#define MASKHANDLEROUTPUTSTRUCT_H

#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "ruba.h"

struct ruba::MaskHandlerOutputStruct{
    std::vector<cv::Mat> Masks;
    int DetectorType;
    int DetectorID;
    QString DetectorIdText;
    bool IsInitialized;
    std::vector<bool> IsMaskInitialized;
    QList<ViewInfo> views;
    std::vector<std::vector<int>> TriggerThreshold;
    std::vector<std::vector<cv::Point>> maskPoints;
    

	//// Detector timing
    int DisableDetectorDuration;
    int DisableDetectorAfter;

    // Detector specific parameters:
    // PresenceDetector:
    std::vector<cv::Mat> BackgroundImages;

    // MovementDetector
    std::vector<std::vector<cv::Point>> FlowRange;
    std::vector<std::vector<int>> RoadUserType;
    std::vector<std::vector<double>> MinVecLengthThreshold;
    int upperThresholdBoundMultiplier;
	bool EnableWrongDirection;

    // TrafficLight Detector
	std::vector<cv::Point> trafficLightPositions;
    int trafficLightTypeTrigger;

};


#endif // MASKHANDLEROUTPUTSTRUCT_H
