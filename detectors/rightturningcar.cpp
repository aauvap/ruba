#include <cmath>
#include "rightturningcar.h"
#include "opencvviewer.h"
#include "presencedetector.h"
#include "movementdetector.h"

using namespace std;
using namespace cv;

using namespace ruba;

RightTurningCar::RightTurningCar(BaseDetectorParams baseParams, QWidget *parent, QString configFile)
    : AbstractCompoundDetector(configureBaseDetectorParams(baseParams, RIGHTTURNINGCAR))
{
    setDetectorCount(2, 2, 1, 0);

    windowIcon = new QIcon("://icons/arrow-turn.png");
    designImg.load("://illustrations/rightturningcars.png");

    LoadConfiguration(baseParams.initFrames, parent, configFile, false);

	for (auto &detector : detectors)
	{
		if (detector->getDetectorType() == STATIONARYDETECTOR)
		{
			sDetector = static_cast<StationaryDetector*>(detector.get());
		}
	}
}

DetectorOutput RightTurningCar::GetOutput()
{
    DetectorOutput outputStruct;

    if (this->IsInitialized)
    {
        outputStruct.Confidence.push_back(1);
    } else {
        outputStruct.Confidence.push_back(0);
    }

    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = RIGHTTURNINGCAR;
    outputStruct.IsTriggered = IsTriggered;
    outputStruct.EnteringConflictZone = EnteringConflictZone;
    outputStruct.LeavingConflictZone = LeavingConflictZone;

    if (EnteringConflictZone > LeavingConflictZone)
    {
        outputStruct.LastChange.push_back(EnteringConflictZone);
    } else {
        outputStruct.LastChange.push_back(LeavingConflictZone);
    }

    outputStruct.Timestamp = getCurrentTime();


    return outputStruct;

}

void RightTurningCar::setDetectorSpecificDefaultParameters()
{
    InitLogSettings generalSettings = getGeneralLogSettings();

    CustomLogHeader header;
    header.additionalTransitionText[Transition::ENTERING] = " CZ";
    header.additionalTransitionText[Transition::LEAVING] = " CZ";
    generalSettings.customLogHeader = { header }; // Entering CZ, Leaving CZ

    QDateTime currentTime = getCurrentTime();

    disableAllDetectorsAt = currentTime.addYears(1);

    for (size_t i = 0; i < detectorParams.size(); ++i)
    {
        if (((int)i == 1) && (detectorParams[i].DetectorType == PRESENCEDETECTOR))
        {
            detectorParams[i].DisableDetectorAfter = 300; // Wait for 300 ms before closing the E2
        }
        else {
            detectorParams[i].DisableDetectorAfter = 2000;
        }

        if (((int)i == 3) && (detectorParams[i].DetectorType == MOVEMENTDETECTOR))
        {
            detectorParams[i].DisableDetectorDuration = 2000; // F2 should disable all other detectors for x ms
        }
    }

    generalSettings.entrySettings.maxTriggeredDuration.clear(); // Feature not supported


    setGeneralLogSettings(generalSettings);


    // Set flags of the RightCarDetector
    IsTriggered.resize(1);
    IsTriggered[0] = false;
    IsWaitingInS1 = false;
    EnteringConflictZone = QDateTime();
    LeavingConflictZone = QDateTime();
}

void RightTurningCar::setDetectorSpecificEnabledStates()
{
    detectorInfo[0].IsEnabled = true; // The PresenceDetector E1 is always enabled
    if (detectorInfo.size() == 5)
    {
        detectorInfo[3].IsEnabled = true; // The MovementDetector is always enabled for demonstration purposes
    }
}

void RightTurningCar::updateDetectorSpecificLogic()
{
    /****** Update the internal logic of the RightTurningCar detector:  ******/

    /***** Step 1: Based on the internal states of the detectors, set enable/disable flags of the detectors *****/
    QDateTime currentTime = getCurrentTime();

    // If PresenceDetector E1 is triggered, enable MovementDetector F1 and StationaryDetector S1
    if (detectorInfo[0].IsTriggered == true)
    {
        detectorInfo[2].IsEnabled = true; // MovementDetector F1
        detectorInfo[4].IsEnabled = true; // StationaryDetector S1

        // Automatically disable detectors after a fixed amount of time
        detectorInfo[2].DisableDetectorAt = currentTime.addMSecs(detectorInfo[2].DisableDetectorAfter);
        detectorInfo[4].DisableDetectorAt = currentTime.addMSecs(detectorInfo[4].DisableDetectorAfter);
    }


    // If MovementDetector F1 is triggered, enable PresenceDetector E2
    if (detectorInfo[2].IsEnabled == true && detectorInfo[2].IsTriggered == true)
    {
        detectorInfo[1].IsEnabled = true; // PresenceDetector E2
        detectorInfo[1].DisableDetectorAt = currentTime.addMSecs(detectorInfo[1].DisableDetectorAfter);

    }

    // If something is inside the StationaryDetector (ie., the edge detector is triggered, enable the
    // MovementDetector F1
    if (detectorInfo[4].IsEnabled && sDetector != nullptr && sDetector->GetIsPresenceDetectorTriggered())
    {
        detectorInfo[2].IsEnabled = true;
        detectorInfo[2].DisableDetectorAt = currentTime.addMSecs(detectorInfo[2].DisableDetectorAfter);
    }

    // If MovementDetector F2 is triggered, disable F1, E2, S1 for x seconds
    if (detectorInfo[3].IsTriggered == true)
    {
        detectorInfo[1].DisableDetectorUntil = currentTime.addMSecs(detectorInfo[3].DisableDetectorDuration);
        detectorInfo[2].DisableDetectorUntil = currentTime.addMSecs(detectorInfo[3].DisableDetectorDuration);
        detectorInfo[4].DisableDetectorUntil = currentTime.addMSecs(detectorInfo[3].DisableDetectorDuration);
    }

    /**** Step 2: Disable the detectors accordingly to the flags *****/
    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        if ((detectorInfo[i].DisableDetectorAt <= currentTime) && (detectorInfo[i].IsTriggered == false))
        {
            detectorInfo[i].IsEnabled = false;
        }
    }

    if ((disableAllDetectorsAt <= currentTime)) // && detectorInfo[0].IsTriggered == true))
    {
        for (size_t i = 1; i < detectorInfo.size(); ++i)
        {
            switch (i)
            {
            case 3:
                break;  // MovementDetector F2 is never disabled
            case 4: // StationaryDetector S1 may not be disabled when triggered
                if (detectorInfo[i].IsTriggered)
                {
                    detectorInfo[i].IsEnabled = true;
                }

            default:
                detectorInfo[i].IsEnabled = false;
            }
        }
    }

    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        if (detectorInfo[i].DisableDetectorUntil > currentTime)
        {
            detectorInfo[i].IsEnabled = false;
            detectorInfo[i].IsTriggered = false;
        }
    }

    /***** Step 3: Update the TrafficDetector and set the flags that may be accessed by the outside *****/

    if (detectorInfo[1].IsTriggered)
    {
        this->IsTriggered[0] = true;
    } else {
        this->IsTriggered[0] = false;
    }


    if (detectorInfo[1].IsEnabled == false && detectorInfo[1].IsTriggered)
    {
        this->EnteringConflictZone = currentTime;
        generalLog->updateLog(getCurrentTime(), frames, Transition::ENTERING,
            Utils::getDetectorAbbreviation(getDetectorType()),
            maskOutlines[detectorInfo[1].getDetectorId()]);

        this->LeavingConflictZone = QDateTime();
    }

    // If the StationaryDetector is triggered
    if (detectorInfo[4].IsTriggered == true)
    {
        this->IsWaitingInS1 = true;

        this->LeavingConflictZone = QDateTime();

        this->EnteringConflictZone = currentTime;
        generalLog->updateLog(getCurrentTime(), frames, Transition::ENTERING,
            detectorInfo[1].getDetectorIdText(),
            maskOutlines[detectorInfo[1].getDetectorId()]);

    } else {
        this->IsWaitingInS1 = false;
    }

    if (detectorInfo[1].IsTriggered == true)
    {
        // Check if the state has changed
        if (getPreviousDetectorInfo()[1].IsTriggered == false)
        {
            //Clear the LeavingConflictZone flag
            this->LeavingConflictZone = QDateTime();
            generalLog->updateLog(getCurrentTime(), frames, Transition::ENTERING,
                detectorInfo[1].getDetectorIdText(),
                maskOutlines[detectorInfo[1].getDetectorId()]);

        }
    } else if (detectorInfo[1].IsTriggered == false)
    {
        // Check if the state has changed
        if (getPreviousDetectorInfo()[1].IsTriggered == true)
        {
            // If it has, a vehicle has just left the conflict zone
            LeavingConflictZone = currentTime;
            generalLog->updateLog(getCurrentTime(), frames, Transition::LEAVING,
                detectorInfo[1].getDetectorIdText(),
                maskOutlines[detectorInfo[1].getDetectorId()]);

            detectorInfo[1].DisableDetectorAt = currentTime.addMSecs(GetConsecutiveEventDelay()); // Disable detector E2 after the consecutive event delay
        }

    }
}

QList<QString> RightTurningCar::getDetectorSpecificDebugInfo()
{
    QList<QString> debugList;

    debugList.push_back("Entering CZ: " + this->EnteringConflictZone.toString("hh:mm:ss:zzz"));
    debugList.push_back("Leaving CZ: " + this->LeavingConflictZone.toString("hh:mm:ss:zzz"));
    debugList.push_back("Waiting in S1: " + QString::number(this->IsWaitingInS1));


    return debugList;
}

QList<QString> RightTurningCar::getDetectorSpecificExtendedDebugInfo()
{
    QList<QString> extDebugList;

    return extDebugList;
}


