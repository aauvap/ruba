#ifndef ABSTRACTSINGLEMODALDETECTOR_H
#define ABSTRACTSINGLEMODALDETECTOR_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "abstracttrafficdetector.h"
#include "framestruct.h"
#include "ruba.h"
#include "ChartStruct.h"
#include "scrollingchart.h"
#include "detectoroutputstruct.h"


class ruba::AbstractSingleModalDetector : public AbstractTrafficDetector
{

public:
    AbstractSingleModalDetector(BaseDetectorParams baseParams);

    ~AbstractSingleModalDetector();

    ruba::DetectorOutput GetOutput();
    
    double GetConfidence();

    // Functions for reconfiguring the detector
    void SetMask(cv::Mat maskImage);

    // Functions for accessing the charts
    std::vector<ruba::ChartStruct> GetChartStructs();
    void SetUseScrollingChart(bool useScrollingChart);
    void SetChartLine(cv::Scalar colorUnder, cv::Scalar colorOver);
    void SetChartBackground(cv::Scalar color);
    int GetChartData();
    double GetChartThreshold();
    std::vector<cv::Mat> GetChartImage();
    virtual std::vector<QString*> GetChartTitles() = 0;

protected:
    virtual void updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput = std::vector<DetectorOutput>());
    virtual void updateDetectorSpecifics() = 0;

    ViewInfo getCurrentView();

    std::vector<double> confidence;
    QList<QString> debugInfo;
    bool UseScrollingChart;

    ruba::FrameStruct frame;
    std::vector<ScrollingChart> Charts;

    cv::Mat mask;

private:
    ViewInfo view;


};

#endif // ABSTRACTSINGLEMODALDETECTOR_H
