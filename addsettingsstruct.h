#ifndef ADDSETTINGSSTRUCT_H
#define ADDSETTINGSSTRUCT_H

#include "ruba.h"
#include "logger.h"

struct ruba::AdditionalSettingsStruct{
    InitLogSettings generalLogSettings;
    std::map<QString, InitLogSettings> individualLogSettings;

 //   std::vector<int> timingSettings;
 //   std::vector<qint64> timegaps;
	//int disableAllDetectorsAfter;
 //   int consecutiveEventDelay;
	//int discardEventsLessThan;
 //   int maxActiveDuration;

    // Path where the current detector configuration is saved
    QString configSaveFilePath;

    QString initialLogPath;

    // Number of detectors in configuration
    uint nbrPDetectors;
    uint nbrMDetectors;
    uint nbrSDetectors;
	uint nbrTLDetectors;

    QList<FamilyInfo> childDetectors;
};


#endif // ADDSETTINGSSTRUCT_H
