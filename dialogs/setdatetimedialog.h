#ifndef SETDATETIMEDIALOG_H
#define SETDATETIMEDIALOG_H

#include <opencv2/opencv.hpp>
#include <QDialog>
#include <QFileInfo>
#include <QInputDialog>
#include <QDateTime>
#include <QSettings>

#include "ruba.h"

namespace Ui {
class SetDateTimeDialog;
}

class SetDateTimeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetDateTimeDialog(QWidget *parent, const QString& filename, int dateTimeFormat,
        int userDefinedYear, bool forceNoGui, QString prevFilename = QString(),
        QDateTime prevDateTime = QDateTime());
    ~SetDateTimeDialog();

    int findStartTime(QDateTime& computedStartTime, int& dateTimeFormat);
	//int getDateTimeFormat();

private slots:
    void on_formatBox_currentIndexChanged(int val);
    void on_dateTimeEdit_dateTimeChanged(const QDateTime& datetime);


private:
    Ui::SetDateTimeDialog *ui;
    //void dateTimeFormatChanged(int index);

    int findStartTimeFromFile(QDateTime& computedDateTime);

    void getUserDefinedDateTime(QDate& tmpDate, QTime& tmpTime);
    int getDateTimeFromEndOfPreviousFile(QDate& tmpDate, QTime& tmpTime);

    int dateTimeFormat;
    int userDefinedYear;
    int userDefinedHour;
    bool forceNoGui;
    QString basename;
    QString prevFilename;
    QDateTime prevDateTime;

    QWidget* parent;
    bool isInitialized;

};

#endif // SETDATETIMEDIALOG_H
