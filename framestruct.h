#ifndef FRAMESTRUCT_H
#define FRAMESTRUCT_H

#include <QDateTime>
#include <QStandardItemModel>
#include <opencv2/opencv.hpp>
#include "ruba.h"
#include "viewinfo.h"

struct ruba::FrameStruct{
    cv::Mat Image;
    QDateTime Timestamp;
    QString Filename;
    ViewInfo viewInfo;
};

Q_DECLARE_METATYPE(cv::Mat)
Q_DECLARE_METATYPE(ruba::FrameStruct)


#endif // FRAMESTRUCT_H
