#include "abstractcompounddetector.h"

using namespace std;
using namespace cv;

using namespace ruba;


AbstractCompoundDetector::AbstractCompoundDetector(BaseDetectorParams baseParams)
    : AbstractTrafficDetector(baseParams)
{
    sDialog = NULL;

    addSettings.nbrTLDetectors = 0;
    addSettings.nbrPDetectors = 0;
    addSettings.nbrMDetectors = 0;
    addSettings.nbrSDetectors = 0;

    windowIcon = NULL;

    windowTitle = getDetectorName();
    addSettings.initialLogPath = getShortDetectorName().toLower().replace(" ", "") + ".yml";
    moduleTitle = getShortDetectorName();
}

void AbstractCompoundDetector::LoadConfiguration(vector<FrameStruct> initFrames, QWidget *parent, QString configFile, 
    bool showGui, std::vector<ruba::DetectorOutput> dOutput)
{
    try
    {
        if (getDetectorType() == INTERACTIONDETECTOR || getDetectorType() == INTERACTION410DETECTOR) {
            iDialog = new interactionselectiondialog(parent, dOutput, windowTitle, *windowIcon);

            iDialog->exec();

            if (iDialog->Accepted)
            {
                IsInitialized = true;

                // Set up logger, and get additional settings
                addSettings = iDialog->getAdditionalSettings();

                if (addSettings.childDetectors.size() == 2)
                {
                    childDetectors = addSettings.childDetectors;
                }
                else {
                    IsInitialized = false;
                }

                InitDetector();
            }

            delete iDialog;
        }
        else {
            if (!configFile.isEmpty())
            {
                addSettings.configSaveFilePath = configFile;
            }

            sDialog = new DetectorSelectionDialog(parent, initFrames, getDetectorType(),
                moduleTitle, windowTitle, *windowIcon, designImg,
                detectorParams, addSettings);

            bool goodConfig = false;

            if (showGui || configFile.isEmpty())
            {
                if (!configFile.isEmpty())
                {
                    QProgressDialog progress(QString(QObject::tr("Loading configuration")), QString(), 0, 0, parent);
                    progress.setMinimumDuration(2000);

                    sDialog->loadConfiguration(configFile);
                    progress.close();
                }

                goodConfig = sDialog->exec();
            }
            else {
                int result = sDialog->loadConfiguration(configFile);

                if (result == 0)
                {
                    goodConfig = true;
                }
            }

            if (goodConfig)
            {
                detectorParams = sDialog->getDetectorParameters();
                addSettings = sDialog->getAdditionalSettings();
              
                BaseDetectorParams newBaseParams;
                newBaseParams.initFrames = initFrames;
                newBaseParams.detectorID = getDetectorID();
                newBaseParams.detectorType = getDetectorType();
                reconfigureBaseDetectorParams(newBaseParams);

                reset(false); // Reset detector, but don't reset logs
                createLogs();

                IsInitialized = true;
            }
            else {
                IsInitialized = false;
            }

            delete sDialog;
        }
    }
    catch (cv::Exception e) {
        // We have encountered an unrecoverable error.
        // Set the error state accordingly and write an appropriate error message
        QString errorMessage = QString(QObject::tr("An unexpected error occurred and the detector was not created. \n\nPlease report the bug by using the details below:\nDetector: %1, id: %2\n"
            "Error err: %3\nError code: %4\nFunction: %5\nLine: %6\nMessage: %7"))
            .arg(getDetectorName())
            .arg(getDetectorID())
            .arg(QString::fromStdString(e.err))
            .arg(e.code)
            .arg(QString::fromStdString(e.func))
            .arg(e.line)
            .arg(QString::fromStdString(e.msg));

        IsInitialized = false;

        QMessageBox::warning(parent, QObject::tr("Failed to create detector"), errorMessage, QMessageBox::Ok);

    }
    catch (std::exception e)
    {
        // We have encountered an unrecoverable error, probably from the STL
        // Set the error state accordingly and write an appropriate error message
        QString errorMessage = QString(QObject::tr("An unexpected error occurred and the detector was not created.\n\n"
            "Please report the bug by using the details below:\nDetector: %1, id: %2\n"
            "Error: %3"))
            .arg(getDetectorName())
            .arg(getDetectorID())
            .arg(QString::fromStdString(e.what()));

        IsInitialized = false;

        QMessageBox::warning(parent, QObject::tr("Failed to create detector"), errorMessage, QMessageBox::Ok);
    }
}

void AbstractCompoundDetector::Reconfigure(vector<FrameStruct> initFrames, QWidget *parent)
{
    DetectorDiagnostics diagnostics = getDiagnostics();
    bool reconfigAllowed = false;

    switch (diagnostics.state) {
    case DiagnosticState::Working:
    {
        reconfigAllowed = true;
        break;
    }
    case DiagnosticState::Exception:
    {
        // The detector has encountered an exception and cannot be reconfigured.
        return;
    }
    case DiagnosticState::Error:
    {
        // The detector might be reconfigured - however, some settings might be lost. Inform accordingly
        int answer = QMessageBox::question(parent, QObject::tr("Reconfigure detector?"),
            QObject::tr("Some masks and background images might be lost when reconfiguring the detector under views "
                "and/or image resolutions different from when the detector was initially created.\n\n"
                "Make sure that you use the \"save as\" button to save the reconfigured detector "
                "in order to preserve the current detector configuration.\n\n"
                "Do you want to continue?"));

        if (answer == QMessageBox::Yes) {
            reconfigAllowed = true;
        }
    }
    }
    

    if (reconfigAllowed)
    {
        LoadConfiguration(initFrames, parent);
    }
}

void AbstractCompoundDetector::updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput)
{
    if (frames.empty())
    {
        return;
    }
    
    this->dOutput = dOutput;
	setDetectorSpecificEnabledStates();
    updateSubDetectors(frames);
    enforceMaxTriggeredDuration();

    // Update the individual detector logs
    updateIndividualLogs();

    updateDetectorSpecificLogic();

    UpdateChartProperties();
    
    // Make sure to save the logs if necessary
    generalLog->saveLog(getCurrentTime(), frames);

    for (auto individualLog : individualLogs)
    {
        individualLog.second->saveLog(getCurrentTime(), frames);
    }

    PrevIsTriggered = IsTriggered;

    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        // Update the triggered information of the previous detector info
        previousDetectorInfo[i].IsTriggered = detectorInfo[i].IsTriggered;
    }

}


void AbstractCompoundDetector::InitDetector()
{
    reset(true);
}

void ruba::AbstractCompoundDetector::setLogFileNamePostfix(int number)
{
	if (generalLog)
	{
		generalLog->setLogFileNamePostfix(number);
	}

	for (auto& individualLog : individualLogs)
	{
		if (individualLog.second)
		{
			individualLog.second->setLogFileNamePostfix(number);
		}
	}

	reset(true); // Finally, reset the logs to apply the changes to the file names
}

std::vector<std::pair<std::string, LogRole> > ruba::AbstractCompoundDetector::getLogFileNames()
{
	std::vector<std::pair<std::string, LogRole> > logFileNames;

	if (generalLog)
	{
		auto fileNames = generalLog->getFileNames();
		logFileNames.insert(std::end(logFileNames), std::begin(fileNames), std::end(fileNames));
	}

	for (auto& individualLog : individualLogs)
	{
		if (individualLog.second)
		{
			auto fileNames = individualLog.second->getFileNames();
			logFileNames.insert(std::end(logFileNames), std::begin(fileNames), std::end(fileNames));
		}
	}
	
	return logFileNames;
}

void ruba::AbstractCompoundDetector::closeAllLogs()
{
	if (generalLog)
	{
		generalLog->closeAllLogs();
	}

	for (auto& individualLog : individualLogs)
	{
		if (individualLog.second)
		{
			individualLog.second->closeAllLogs();
		}
	}
}

void ruba::AbstractCompoundDetector::createLogs()
{
    // Create general log
    generalLog = make_shared<Logger>(addSettings.generalLogSettings, 
        getDetectorAbbreviation(),
        getCurrentTime(), frames);

    // Create logs for individual detectors
    individualLogs.clear();

    for (auto individualSettings : addSettings.individualLogSettings)
    {
        individualLogs[individualSettings.first] = make_shared<Logger>(individualSettings.second,
            individualSettings.first, getCurrentTime(), frames);
    }
}

void ruba::AbstractCompoundDetector::updateIndividualLogs()
{
    if (!individualLogs.empty())
    {
        for (auto i = 0; i < detectorInfo.size(); ++i)
        {
            if ((detectorInfo[i].IsTriggered == true) && (previousDetectorInfo[i].IsTriggered == false))
            {
                auto it = individualLogs.find(detectorInfo[i].getDetectorIdText());
                if (it != individualLogs.end())
                {
                    it->second->updateLog(getCurrentTime(), frames, Transition::ENTERING,
                        it->first, maskOutlines[detectorInfo[i].getDetectorId()]);
                }
            }
            else if ((detectorInfo[i].IsTriggered == false) && (previousDetectorInfo[i].IsTriggered == true))
            {
                auto it = individualLogs.find(detectorInfo[i].getDetectorIdText());
                if (it != individualLogs.end())
                {
                    it->second->updateLog(getCurrentTime(), frames, Transition::LEAVING,
                        it->first, maskOutlines[detectorInfo[i].getDetectorId()]);
                }
            }
        }
    }
}

void ruba::AbstractCompoundDetector::enforceMaxTriggeredDuration()
{
    // If the detector is triggered, set the disableDetectorAt timestamp. 
    // If the detector has gone low, and thus is not triggered, clear the timestamp

    for (auto i = 0; i < detectorInfo.size(); ++i)
    {
        QString detectorIdText = detectorInfo[i].getDetectorIdText();
        auto it = addSettings.generalLogSettings.entrySettings.maxTriggeredDuration.find(detectorIdText);

        // Check if there exists a maxActiveDuration for this specific detector
        if (it != addSettings.generalLogSettings.entrySettings.maxTriggeredDuration.end())
        {
            if ((detectorInfo[i].IsTriggered == true) && (previousDetectorInfo[i].IsTriggered == false))
            {
                detectorInfo[i].DisableDetectorAt = getCurrentTime().addMSecs(it->second);
            }
            else if ((detectorInfo[i].IsTriggered == false) && (previousDetectorInfo[i].IsTriggered == true))
            {
                detectorInfo[i].DisableDetectorAt = QDateTime();
            }
        }
    }

    for (auto i = 0; i < detectorInfo.size(); ++i)
    {
        if ((detectorInfo[i].IsTriggered == true) && 
            detectorInfo[i].DisableDetectorAt.isValid() &&
            (detectorInfo[i].DisableDetectorAt < getCurrentTime()))
        {
            // The detector has been active for too long. Revert the triggered state
            detectorInfo[i].IsTriggered = false;
        }
    }
        
    
}

void ruba::AbstractCompoundDetector::reset(bool resetLogs)
{
    // Reset logs
    if (resetLogs)
    {
        if (generalLog != nullptr)
        {
            generalLog->reset(getCurrentTime(), frames);
        }

        for (auto individualLog : individualLogs)
        {
            individualLog.second->reset(getCurrentTime(), frames);
        }
    }

    // Reset triggered information
    IsTriggered.clear();
    IsTriggered.resize(1);
    PrevIsTriggered.clear();
    PrevIsTriggered.resize(1);
    detectorInfo.clear();

    // Clear the sub-detectors, and create them again
	detectors.clear();

    createDetectors();

    // Initialise detector information
    initialiseDetectorInfo();

	for (auto &detector : detectors)
	{
		detector->setChartStatus(true, false);
	}

    // Set the output masks and mask outlines
    maskOverlays.clear();
    for (int i = 0; i < detectorParams.size(); ++i)
    {
        vector<MaskOverlayStruct> tmpOverlay;
        vector<MaskOutlineStruct> tmpOutline;

        for (int j = 0; j < detectorParams[i].Masks.size(); ++j)
        {
            if (getCurrentViews().size() == detectorParams[i].Masks.size())
            {
                MaskOverlayStruct tmpMask; 
                MaskOutlineStruct tmpOutlineMask;
                tmpOutlineMask.View = tmpMask.View = getCurrentViews()[j];
                detectorParams[i].Masks[j].copyTo(tmpMask.Mask);
                tmpOutlineMask.MaskOutline = cv::Mat::zeros(detectorParams[i].Masks[j].size(), detectorParams[i].Masks[j].type());

                tmpOutlineMask.DetectorType = tmpMask.DetectorType = detectorParams[i].DetectorType;
                tmpOutlineMask.DetectorID = tmpMask.DetectorID = detectorParams[i].DetectorID;
                tmpMask.detectorIdText = detectorParams[i].DetectorIdText;

                if (detectorParams[i].DetectorType == PRESENCEDETECTOR)
                {
                    tmpOutlineMask.Color = tmpMask.Color = Scalar(255, 0, 0);
                }
                else if (detectorParams[i].DetectorType == MOVEMENTDETECTOR)
                {
                    tmpOutlineMask.Color = tmpMask.Color = Scalar(0, 0, 255);
                }
                else if (detectorParams[i].DetectorType == STATIONARYDETECTOR)
                {
                    tmpOutlineMask.Color = tmpMask.Color = Scalar(0, 255, 0);
                }
                else if (detectorParams[i].DetectorType == TRAFFICLIGHTDETECTOR)
                {
                    tmpOutlineMask.Color = tmpMask.Color = Scalar(0, 255, 255);
                }

                // Draw the outline
                if (detectorParams[i].maskPoints.size() > j && detectorParams[i].maskPoints[j].size() > 1)
                {
                    int sizeMultiplier = tmpOutlineMask.MaskOutline.cols / 640;

                    if (sizeMultiplier < 1)
                    {
                        sizeMultiplier = 1;
                    }
                    
                    std::vector<cv::Point> points = detectorParams[i].maskPoints[j];
                    points.push_back(points.front()); // Add the first point to ensure completeness of the outline

                    polylines(tmpOutlineMask.MaskOutline, points,
                        false, cv::Scalar::all(255), sizeMultiplier, LINE_AA);
                }

                tmpOverlay.push_back(tmpMask);
                tmpOutline.push_back(tmpOutlineMask);
            }
        }

        maskOverlays[detectorParams[i].DetectorID] = tmpOverlay;
        maskOutlines[detectorParams[i].DetectorID] = tmpOutline;
    }

    EngraveIdInMasks();


    previousDetectorInfo = detectorInfo;
    previousTime = QDateTime();

    checkCurrentViews(getCurrentViews());

}

void AbstractCompoundDetector::initialiseDetectorInfo()
{
    // Initialize the detector info
    detectorInfo.clear();

    for (auto detector : detectorParams)
    {
        // Initialize the detectorInfo for the detectors
        DetectorInfo tmpInfo(detector.DetectorType, detector.DetectorID, detector.DetectorIdText);
        
        // Detectors are disabled by default
        tmpInfo.IsEnabled = false;

        tmpInfo.IsTriggered = false;
        tmpInfo.IsWrongDirectionTriggered = false;
        tmpInfo.DisableDetectorAt = getCurrentTime().addYears(1);
        tmpInfo.DisableDetectorUntil = QDateTime();
        tmpInfo.DisableDetectorDuration = detector.DisableDetectorDuration; // Time in ms.

        detectorInfo.push_back(tmpInfo);
    }
}


void AbstractCompoundDetector::setDefaultParameters(std::vector<DType> detectors)
{
    detectorInfo.clear();
    detectorParams.clear();

    size_t nbrViews = frames.size();

    std::map<DType, int> detectorOccurences;

    for (auto i = 0; i < detectors.size(); ++i)
    {
        MaskHandlerOutputStruct mHandle;
        mHandle.DetectorType = detectors[i];
        mHandle.DetectorID = i;
        int detectorIdTextNumber = detectorOccurences[detectors[i]];
        detectorOccurences[detectors[i]]++;
        mHandle.DetectorIdText = Utils::getDetectorAbbreviation(detectors[i]) 
            + QString::number(detectorIdTextNumber + 1);
        mHandle.DisableDetectorDuration = -1; // Time in ms. Deactivated per default
        mHandle.DisableDetectorAfter = -1;
        mHandle.views = getCurrentViews();

        // Detector type specific initialisations

        switch (detectors[i])
        {
        case MOVEMENTDETECTOR:
            // Fall-through to stationary detector
        case STATIONARYDETECTOR:
            // Use defaults of the detectorSelectionDialog
            for (size_t j = 0; j < nbrViews; ++j)
            {
                mHandle.upperThresholdBoundMultiplier = 0;
            }
            break;
        case TRAFFICLIGHTDETECTOR:
            break;
        }

        detectorParams.push_back(mHandle);
    }

    // Set default log parameters
    addSettings.generalLogSettings.entrySettings.consecutiveEventDelay[QObject::tr("General")] = 300;
    addSettings.generalLogSettings.entrySettings.discardEventsLessThan[QObject::tr("General")] = 200;
    addSettings.generalLogSettings.sumInterval = 15;

    addSettings.generalLogSettings.saveEveryEventLog = false;
    addSettings.generalLogSettings.entrySettings.saveFrameOfEventSettings.clear();
    addSettings.generalLogSettings.saveSumOfEventsLog = false;

    setDetectorSpecificDefaultParameters();
    initialiseDetectorInfo();

}

std::vector<DetectorOutput> ruba::AbstractCompoundDetector::getDOutput()
{
    return dOutput;
}

void ruba::AbstractCompoundDetector::setDetectorCount(uint nbrPDetectors, 
    uint nbrMDetectors, 
    uint nbrSDetectors, 
    uint nbrTLDetectors)
{
    addSettings.nbrPDetectors = nbrPDetectors;
    addSettings.nbrMDetectors = nbrMDetectors;
    addSettings.nbrSDetectors = nbrSDetectors;
    addSettings.nbrTLDetectors = nbrTLDetectors;

    std::vector<DType> detectors(nbrPDetectors, PRESENCEDETECTOR);

    std::vector<DType> mDetectors(nbrMDetectors, MOVEMENTDETECTOR);
    detectors.insert(detectors.end(), mDetectors.begin(), mDetectors.end());

    std::vector<DType> sDetectors(nbrSDetectors, STATIONARYDETECTOR);
    detectors.insert(detectors.end(), sDetectors.begin(), sDetectors.end());

    std::vector<DType> tlDetectors(nbrTLDetectors, TRAFFICLIGHTDETECTOR);
    detectors.insert(detectors.end(), tlDetectors.begin(), tlDetectors.end());

    setDefaultParameters(detectors);    
}

void ruba::AbstractCompoundDetector::setDetectorCount(std::vector<DType> detectors)
{
    for (auto detector : detectors)
    {
        switch (detector)
        {
        case PRESENCEDETECTOR:
            addSettings.nbrPDetectors++;
            break;
        case MOVEMENTDETECTOR:
            addSettings.nbrMDetectors++;
            break;
        case STATIONARYDETECTOR:
            addSettings.nbrSDetectors++;
            break;
        case TRAFFICLIGHTDETECTOR:
            addSettings.nbrTLDetectors++;
            break;
        }
    }

    setDefaultParameters(detectors);
}


void ruba::AbstractCompoundDetector::getDetectorCount(uint& nbrPDetectors,
    uint& nbrMDetectors,
    uint& nbrSDetectors,
    uint& nbrTLDetectors)
{
    nbrPDetectors = addSettings.nbrPDetectors;
    nbrMDetectors = addSettings.nbrMDetectors;
    nbrSDetectors = addSettings.nbrSDetectors;
    nbrTLDetectors = addSettings.nbrTLDetectors;

    return;
}

void ruba::AbstractCompoundDetector::setGeneralLogSettings(const InitLogSettings & generalSettings)
{
    addSettings.generalLogSettings = generalSettings;
}

void AbstractCompoundDetector::EngraveIdInMasks()
{
    // Engraves the detectorID (e.g. E1, E2, etc.) in the masks of the detectors that are
    // returned by GetMasks()

    for (auto it = maskOverlays.begin(); it != maskOverlays.end(); ++it)
    {
        for (size_t j = 0; j < it->second.size(); ++j)
        {
            int fontFace = FONT_HERSHEY_PLAIN;
            double fontScale = 1;
            int thickness = 2;

            // Find the centre of the mask points:
            vector<vector<Point> > contours;
            vector<Vec4i> hierarchy;
            Point maskCenter;

            findContours(it->second[j].Mask, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0) );

            // Get the moments
            vector<Moments> mu(contours.size() );
            for( size_t k = 0; k < contours.size(); k++ )
            {
                mu[k] = moments( contours[k], false );
            }

            // Get the mass centres:
            vector<Point2f> mc( contours.size() );
            for( size_t k = 0; k < contours.size(); k++ )
            {
                mc[k] = Point2f( mu[k].m10/mu[k].m00 , mu[k].m01/mu[k].m00 );
            }

            if (!mc.empty())
            {
                maskCenter = mc[0];
            }
            // Displace the centre to make the text appear in the middle of the blob
            maskCenter.x = maskCenter.x - 7;
            maskCenter.y = maskCenter.y + 5;

            cv::putText(it->second[j].Mask, it->second[j].detectorIdText.toStdString(), maskCenter, fontFace, fontScale, Scalar(0,0,0), thickness, 8);
        }
    }

}

void ruba::AbstractCompoundDetector::updateSubDetectors(const std::vector<FrameStruct>& newFrames)
{
    frames = newFrames;

    // Update the detectors

	for (auto i = 0; i < detectors.size(); ++i)
	{
		if (i < detectorInfo.size())
		{
			// Update step. TODO: Move the detector specific update routines 
			// into the detectors themselves in order to avoid this silly switch
			switch (detectorInfo[i].getDetectorType())
			{
			case PRESENCEDETECTOR:
			{
				// Due to dynamic background updating algorithm, the
				// PresenceDetectors must always run
				detectors[i]->Update(frames);
				break;
			}
			case TRAFFICLIGHTDETECTOR:
			{
				// The TL detector is always updated
				detectors[i]->Update(frames);
				break;
			}
			case MOVEMENTDETECTOR:
			{
				if (detectorInfo[i].IsEnabled)
				{
					detectors[i]->Update(frames);
				}
				else {
					detectors[i]->ResetTriggeredHistory();
				}

				break;
			}

			default:
			{
				// Other detectors are only updated when they are enabled
				if (detectorInfo[i].IsEnabled)
				{
					detectors[i]->Update(frames);
				}
			}
			}

			// IsTriggered step

			DetectorOutput dOutput;
			dOutput = detectors[i]->GetOutput();

			if (dOutput.IsTriggered.size() > 0)
			{
				detectorInfo[i].IsTriggered = dOutput.IsTriggered[0];
			}
			else {
				detectorInfo[i].IsTriggered = false;
			}
		}
	}

    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        // Update the enabled information of the previous detector info
        previousDetectorInfo[i].IsEnabled = detectorInfo[i].IsEnabled;
    }

}

void ruba::AbstractCompoundDetector::setDetectorSpecificEnabledStates()
{
	for (auto &dInfo : detectorInfo)
	{
		dInfo.IsEnabled = true; // By default, enable all detectors.
	}
}

std::string AbstractCompoundDetector::stripFilename(std::string inputFilename) 
{
    std::size_t found = inputFilename.find_last_of("/\\");

    // Error handling in case there is no "." and it returns npos
    if (inputFilename.substr(found + 1).find_last_of(".") == std::string::npos) return inputFilename;
    else return inputFilename.substr(found + 1).substr(0, inputFilename.substr(found + 1).find_last_of("."));
}


void AbstractCompoundDetector::UpdateChartProperties()
{

    // Update the properties of the ScrollingCharts of the detectors.
    // If the detectors are enabled, the information on the chart should be visible - otherwise not.

	for (auto i = 0; i <  detectors.size(); ++i)
	{
		if (i < detectorInfo.size())
		{
			detectors[i]->setChartStatus(detectorInfo[i].IsEnabled, detectorInfo[i].IsTriggered);
		}
	}

 //   for (size_t i = 0; i < PDetectors.size(); ++i)
 //   {
 //       if (detectorInfo[i].IsEnabled)
 //       {
 //           if (detectorInfo[i].IsTriggered)
 //           {
 //               PDetectors[i].SetChartBackground(Scalar(255,200,200)); // Set the background
 //               PDetectors[i].SetChartLine(Scalar(255,100,100), Scalar(220,0,0)); // Show the line  w. triggered color
 //           } else {
 //               PDetectors[i].SetChartBackground(Scalar(255,220,220)); // Set the background
 //               PDetectors[i].SetChartLine(Scalar(255,100,100), Scalar(255,100,100)); // Show the line w.u. triggered color
 //           }

 //       } else {
 //           PDetectors[i].SetChartBackground(Scalar::all(255)); // Set the background to "neutral"
 //           PDetectors[i].SetChartLine(Scalar::all(150), Scalar::all(150)); // Set the line to "idle"
 //       }
 //   }

 //   for (size_t i = 0; i < MDetectors.size(); ++i)
 //   {
 //       if (detectorInfo[addSettings.nbrPDetectors+i].IsEnabled)
 //       {
 //           if (detectorInfo[addSettings.nbrPDetectors+i].IsTriggered)
 //           {
 //               MDetectors[i].SetChartBackground(Scalar(200,200,255)); // Set the background
 //               MDetectors[i].SetChartLine(Scalar(100,100,255), Scalar(0,0,220)); // Show the line w. triggered color
 //           } else {
 //               MDetectors[i].SetChartBackground(Scalar(220,220,255)); // Set the background
 //               MDetectors[i].SetChartLine(Scalar(100,100,255), Scalar(100,100,255)); // Show the line w.u. triggered color
 //           }
 //       } else {
 //           MDetectors[i].SetChartBackground(Scalar::all(255)); // Set the background to "neutral"
 //           MDetectors[i].SetChartLine(Scalar::all(255), Scalar::all(255)); // Set the line to "idle"
 //       }
 //   }

 //   for (size_t i = 0; i < SDetectors.size(); ++i)
 //   {
 //       if (detectorInfo[addSettings.nbrPDetectors+addSettings.nbrMDetectors+i].IsEnabled)
 //       {
 //           if (detectorInfo[addSettings.nbrPDetectors+addSettings.nbrMDetectors+i].IsTriggered)
 //           {
 //               SDetectors[i]->SetChartBackground(Scalar(200,255,200)); // Set the background
 //               SDetectors[i]->SetChartLine(Scalar(220,0,0), Scalar(220,0,0), Scalar(100,100,255), Scalar(0,0,220)); // Show the line
 //           } else {
 //               SDetectors[i]->SetChartBackground(Scalar(230,255,230)); // Set the background
 //               SDetectors[i]->SetChartLine(Scalar(220,100,100), Scalar(220,0,0), Scalar(100,100,255), Scalar(0,0,220)); // Show the line
 //           }

 //       } else {
 //           SDetectors[i]->SetChartBackground(Scalar::all(255)); // Set the background to "neutral"
 //           SDetectors[i]->SetChartLine(Scalar::all(150), Scalar::all(150), Scalar::all(150), Scalar::all(150)); // Set the line to "idle"
 //       }
 //   }
	//for (size_t i = 0; i < TLDetectors.size(); ++i)
	//{
	//	if (detectorInfo[addSettings.nbrPDetectors + addSettings.nbrMDetectors + addSettings.nbrSDetectors + i].IsEnabled)
	//	{
	//		if (detectorInfo[addSettings.nbrPDetectors + addSettings.nbrMDetectors+ addSettings.nbrSDetectors + i].IsTriggered)
	//		{
	//			TLDetectors[i].SetChartBackground(Scalar(200, 200, 255)); // Set the background
	//		}
	//		else {
	//			TLDetectors[i].SetChartBackground(Scalar(220, 220, 255)); // Set the background
	//		}
	//	}
	//	else {
	//		TLDetectors[i].SetChartBackground(Scalar::all(255)); // Set the background to "neutral"
	//	}
	//}
    
}

qint64 AbstractCompoundDetector::GetConsecutiveEventDelay()
{
	auto it = addSettings.generalLogSettings.entrySettings.consecutiveEventDelay.find(QObject::tr("General"));
	if (it != addSettings.generalLogSettings.entrySettings.consecutiveEventDelay.end())
	{
		return it->second;
	}
	else {
		return -1;
	}
}

std::vector<ChartStruct> AbstractCompoundDetector::GetChartStructs()
{
    vector<ChartStruct> charts;
    QString baseIdentifier = getDetectorAbbreviation() + QString::number(getDetectorID()) + " ";

	for (auto i = 0; i < detectors.size(); ++i)
	{
		vector<ChartStruct> tmpCharts = detectors[i]->GetChartStructs();
		QString detectorAbbrev = detectors[i]->getDetectorAbbreviation();
		;

		if (detectors[i]->getDetectorType() != MOVEMENTDETECTOR)
		{
			for (size_t j = 0; j < tmpCharts.size(); ++j)
			{
				tmpCharts[j].Title = baseIdentifier + detectorInfo[i].getDetectorIdText()
					+ QObject::tr(": View %1")
					.arg(tmpCharts[j].View.viewName());
				charts.push_back(tmpCharts[j]);
			}
		}
		else {
			// If MovementDetector, take speciel care of the "Right" and "Wrong" direction markers
			for (size_t j = 0; j < tmpCharts.size(); ++j)
			{
				QString identifier;

				if (tmpCharts.size() == 1)
				{
					identifier = baseIdentifier + detectorInfo[i].getDetectorIdText() + ": ";
				}
				else if (tmpCharts.size() == 2) {

					if (j == 0)
					{
						identifier = baseIdentifier + detectorInfo[i].getDetectorIdText() + "-C: ";
					}
					else {
						identifier = baseIdentifier + detectorInfo[i].getDetectorIdText() + "-W: ";
					}
				}

				tmpCharts[j].Title = identifier + QObject::tr("View %1")
					.arg(tmpCharts[j].View.viewName());
				charts.push_back(tmpCharts[j]);
			}
		}
	}

    return charts;

}

QList<QString> AbstractCompoundDetector::GetExtendedDebugInfo()
{
    QString detectorAbbrev = getDetectorAbbreviation();

    QList<QString> extDebugList;

    for (size_t i = 0; i < detectorInfo.size(); ++i)
    {
        QString subDetectorIdText = detectorInfo[i].getDetectorIdText();
        
        extDebugList.push_back(detectorAbbrev + " " + subDetectorIdText + "-E:" + QString::number(detectorInfo[i].IsEnabled));
        extDebugList.push_back(detectorAbbrev + " " + subDetectorIdText + "-T:" + QString::number(detectorInfo[i].IsTriggered));

        // Get debug info of specific sub-detector
		QList<QString> specificList;

		if (i < detectors.size())
		{
			specificList = detectors[i]->getDetectorSpecificDebugInfo();
		}

        /*switch (detectorInfo[i].getDetectorType()) {
        case PRESENCEDETECTOR: 
        {
            for (auto detector : PDetectors)
            {
                if (detector.getDetectorID() == detectorInfo[i].getDetectorId())
                {
                    specificList = detector.getDetectorSpecificDebugInfo();
                    break;
                }
            }
            break;
        }
        case MOVEMENTDETECTOR: 
        {
            for (auto detector : MDetectors)
            {
                if (detector.getDetectorID() == detectorInfo[i].getDetectorId())
                {
                    specificList = detector.getDetectorSpecificDebugInfo();
                    break;
                }
            }
            break;
        }
        case STATIONARYDETECTOR: 
        {
            for (auto detector : SDetectors)
            {
                if (detector->getDetectorID() == detectorInfo[i].getDetectorId())
                {
                    specificList = detector->getDetectorSpecificDebugInfo();
                    break;
                }
            }
            break;
        }
        case TRAFFICLIGHTDETECTOR: 
        {
            for (auto detector : MDetectors)
            {
                if (detector.getDetectorID() == detectorInfo[i].getDetectorId())
                {
                    specificList = detector.getDetectorSpecificDebugInfo();
                    break;
                }
            }
            break;
        }
        }*/

        // Insert specific detector number + type
        for (int i = 0; i < specificList.size(); ++i) {
            specificList[i].prepend(detectorAbbrev + " " + subDetectorIdText + ": ");
            extDebugList.push_back(specificList[i]);
        }        
    }

    QList<QString> specificInfo = getDetectorSpecificExtendedDebugInfo();

    for (int i = 0; i < specificInfo.size(); ++i)
    {
        extDebugList.push_back(detectorAbbrev + " " + specificInfo[i]);
    }

    return extDebugList;
}

QList<QString> ruba::AbstractCompoundDetector::GetEveryEventLogInfo()
{
    QList<QString> logInfo;
    
    // Get log info:
    if (generalLog != nullptr)
    {
        logInfo.append(generalLog->getDebugInfo());
    }

    return logInfo;
}

std::map<QString, QList<QString>> ruba::AbstractCompoundDetector::GetIndividualLogInfo()
{
    std::map<QString, QList<QString>> logInfo;

    // Get log info:
    for (auto log : individualLogs)
    {
        if (addSettings.individualLogSettings[log.first].saveEveryEventLog ||
            addSettings.individualLogSettings[log.first].saveSumOfEventsLog)
        {
            logInfo[log.first] = log.second->getDebugInfo();
        }
    }

    return logInfo;
}

std::vector<std::vector<MaskOverlayStruct>> AbstractCompoundDetector::GetMasks()
{
    std::vector<std::vector<MaskOverlayStruct>> masks;

    for (auto i = 0; i < detectorInfo.size(); ++i)
    {
        std::vector<MaskOverlayStruct> detectorMask = maskOverlays[detectorInfo[i].getDetectorId()];

        // Find out if the detector, that corresponds to the mask is enabled
        bool isEnabled, isTriggered;

        isEnabled = detectorInfo[i].IsEnabled;
        isTriggered = detectorInfo[i].IsTriggered;

        for (auto j = 0; j < detectorMask.size(); ++j)
        {
            if (isEnabled)
            {
                // The detector is enabled. Next, find out if the detector is triggered
                if (isTriggered)
                {
                    // The detector is triggered. Show in dark colors
                    detectorMask[j].Alpha = 0.65;

                }
                else {
                    // The detector is enabled, but is not triggered. Show in light colors
                    detectorMask[j].Alpha = 0.25;
                }
            }
            else
            {
                // The detector is disabled. The detector must be fully transparent
                detectorMask[j].Alpha = 0.0;
            }
        }

        masks.push_back(detectorMask);
    }

    return masks;
}

void AbstractCompoundDetector::SetUseScrollingChart(bool useScrollingChart)
{
	for (auto detector : detectors)
	{
		detector->SetUseScrollingChart(useScrollingChart);
	}

 //   for (size_t i = 0; i < PDetectors.size(); ++i)
 //   {
 //       PDetectors[i].SetUseScrollingChart(useScrollingChart);
 //   }

 //   for (size_t i = 0; i < MDetectors.size(); ++i)
 //   {
 //       MDetectors[i].SetUseScrollingChart(useScrollingChart);
 //   }

 //   for (size_t i = 0; i < SDetectors.size(); ++i)
 //   {
 //       SDetectors[i]->SetUseScrollingChart(useScrollingChart);
 //   }
	//for (size_t i = 0; i < TLDetectors.size(); ++i)
	//{
	//	TLDetectors[i].SetUseScrollingChart(useScrollingChart);
	//}

}

void AbstractCompoundDetector::SetLastFrameReached()
{
    // Force the log to be written
    generalLog->saveLog(getCurrentTime(), frames, true);

    for (auto individualLog : individualLogs)
    {
        individualLog.second->saveLog(getCurrentTime(), frames, true);
    }
}

void AbstractCompoundDetector::createDetectors()
{
    BaseDetectorParams individualBaseParams;
    individualBaseParams.initFrames = frames;

    for (int i = 0; i < detectorParams.size(); ++i)
    {
        // Initialize the detectors based on the parameters from the dialogue
        individualBaseParams.detectorType = detectorParams[i].DetectorType;

        if (detectorParams[i].DetectorType == PRESENCEDETECTOR)
        {
			individualBaseParams.detectorID = detectorParams[i].DetectorID;// = PDetectors.size();

            std::vector<int> minAreaCovered;

            for (auto j = 0; j < detectorParams[i].TriggerThreshold.size(); ++j) 
            {
                if (detectorParams[i].TriggerThreshold[j].size() > 0) {
                    minAreaCovered.push_back(detectorParams[i].TriggerThreshold[j][0]);
                }
            }

			detectors.push_back(make_shared<PresenceDetector>(individualBaseParams, minAreaCovered,
				detectorParams[i].Masks, detectorParams[i].BackgroundImages, true));
            /*PresenceDetector Detector(individualBaseParams, minAreaCovered,
                                      detectorParams[i].Masks, detectorParams[i].BackgroundImages, true);
            PDetectors.push_back(Detector);*/

        } else if (detectorParams[i].DetectorType == MOVEMENTDETECTOR)
        {
			individualBaseParams.detectorID = detectorParams[i].DetectorID;// = addSettings.nbrPDetectors + MDetectors.size();

			detectors.push_back(make_shared<MovementDetector>(individualBaseParams, detectorParams[i].Masks, detectorParams[i].MinVecLengthThreshold,
				detectorParams[i].FlowRange, detectorParams[i].TriggerThreshold, true,
				detectorParams[i].upperThresholdBoundMultiplier));
            /*MovementDetector Detector();
            MDetectors.push_back(Detector);*/
            
        } else if (detectorParams[i].DetectorType == STATIONARYDETECTOR)
        {
			individualBaseParams.detectorID = detectorParams[i].DetectorID;// = addSettings.nbrPDetectors + addSettings.nbrMDetectors + SDetectors.size();

			detectors.push_back(make_shared<StationaryDetector>(individualBaseParams, detectorParams[i], true));
            /*StationaryDetector *Detector = new StationaryDetector(individualBaseParams, detectorParams[i], true);
            SDetectors.push_back(Detector);*/
            
		}
		else if (detectorParams[i].DetectorType == TRAFFICLIGHTDETECTOR)
		{
			individualBaseParams.detectorID = detectorParams[i].DetectorID;// = addSettings.nbrPDetectors + addSettings.nbrMDetectors + addSettings.nbrSDetectors +

			detectors.push_back(make_shared<TrafficLightDetector>(individualBaseParams, detectorParams[i].BackgroundImages, detectorParams[i].Masks,
				detectorParams[i].trafficLightPositions, detectorParams[i].trafficLightTypeTrigger, true));
			//TrafficLightDetector Detector(individualBaseParams, detectorParams[i].BackgroundImages,detectorParams[i].Masks,
   //                                       tlPlacements,detectorParams[i].tlTypeTrigger, true);

   //         TLDetectors.push_back(Detector);
		}
    }
}

QString AbstractCompoundDetector::GetConfigSaveFilePath()
{
    return addSettings.configSaveFilePath;
}


int AbstractCompoundDetector::SaveCurrentConfiguration(QString savePath)
{
    if (!savePath.isEmpty())
    {
        addSettings.configSaveFilePath = savePath;
    }

    return DetectorSelectionDialog::SaveConfiguration(addSettings.configSaveFilePath, detectorParams, addSettings, getDetectorType(), getCurrentViews());
}

QList<QString> AbstractCompoundDetector::getDetectorSpecificDebugInfo()
{
    QList<QString> debugList;
    if (generalLog != nullptr)
    {
        debugList.push_back("Completed events: " + QString::number(generalLog->getCompletedEventCount()));
        debugList.push_back("Last Entering: " + generalLog->getLastEnteringTime().toString("hh:mm:ss:zzz"));
        debugList.push_back("Last Leaving: " + generalLog->getLastLeavingTime().toString("hh:mm:ss:zzz"));
    }

    return debugList;
}