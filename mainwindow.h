#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtConcurrentRun>
#include <QDateTime>
#include <QFutureWatcher>
#include <QGridLayout>
#include <QInputDialog>
#include <QItemSelectionModel>
#include <QList>
#include <QListView>
#include <QMainWindow>
#include <QtWidgets/QMdiSubWindow>
#include <QShortcut>
#include <QSplashScreen>
#include <QStringListModel>
#include <QTimer>
#include <QTreeWidgetItem>

#include <opencv2/opencv.hpp>

#include <exception>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "videohandler.h"
#include "detectorhandler.h"
#include "opencvviewer.h"
#include "maskoverlaystruct.h"
#include "rubabaseobject.h"
#include "version.h"
#include "debugprefstruct.h"
#include "dialogs/settingsdialog.h"
#include "processingThread.h"
#include "dialogs/multithreadedprocessingdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT;

public:
	explicit MainWindow(int devState = 0, QWidget *parent = 0);
    void maximizeSubWindows();
    ~MainWindow();

public slots:
    void cleanUpOnExit();
	void recieveTime(QDateTime time, double fps);
	void recieveFrame(cv::Mat frame);
	void displayFrames(const std::vector<ruba::FrameStruct>& frames, double fps);
	void recieveMessage(const QString& title, const QString& message, MessageType messageType);
	void recieveProgress(int value, int maxValue, const QString& message);

private slots:
	void displayTimerTick();

    void on_actionLoad_video_files_triggered();

	void on_actionLoad_video_stream_triggered();

    void on_StartPause_button_clicked();

    void on_detectorListView_doubleClicked(const QModelIndex &index);

    void on_actionSave_video_to_file_triggered();

    void on_actionRight_Turning_Car_triggered();

    void on_actionLeft_Turning_Car_triggered();

    void on_actionStraight_Going_Cyclist_triggered();

    void on_actionInteraction_triggered();

    void on_actionReset_layout_triggered();

    void on_actionStop_recording_triggered();

    void on_JumpToFrameButton_clicked();

    void on_ReconfigureDetectorButton_clicked();

    void on_DeleteDetectorButton_clicked();

    void deleteDetectorFromList();

    void on_ResetDetectorButton_clicked();

    void play();

    void pause();

    void on_ShowCurrentDebugWindowsButton_clicked(bool checked);

    void on_OverlayCurrentMasksButton_clicked(bool checked);

    void on_OverlayCurrentDebugInfoButton_clicked(bool checked);

    void on_OverlayCurrentExtDebugInfoButton_clicked(bool checked);

    void on_actionShow_video_triggered(bool checked);
    
    void on_actionOpen_changelog_triggered();

    void on_actionOverlay_timestamp_triggered(bool checked);

    void on_actionOverlay_debug_information_triggered(bool checked);

    void on_actionOverlay_ext_debug_information_triggered(bool checked);

    void on_actionOverlay_masks_triggered(bool checked);

    void on_actionShow_all_debug_windows_triggered(bool checked);

    void on_actionSingle_Module_triggered();

    void on_actionDouble_Module_triggered();

	void on_actionExclusive_Module_triggered();

    void on_actionLicense_triggered();

    void on_actionAbout_triggered();

    void on_actionAboutQt_triggered();

	void on_actionReport_Bug_triggered();

    void on_moveVideoUpButton_clicked();

    void on_moveVideoDownButton_clicked();

    void on_deleteVideosFromListButton_clicked();

    void on_addVideosToListButton_clicked();

    void on_showVideoPropertiesButton_clicked();

    void on_videoListView_doubleClicked(const QModelIndex &index);

    void on_actionAdjust_window_size_to_video_triggered();

    void on_actionTile_modality_windows_triggered();

    void on_actionEnable_thermal_modality_triggered();

    void on_actionEnable_RGB_modality_triggered();

    void on_actionSave_configuration_triggered();

    void on_actionLoad_configuration_triggered();

    void on_actionLoad_detector_triggered();

    void on_actionAnnotate_Ground_Truth_triggered();

    void on_actionReview_Log_File_triggered();

	void on_actionPerform_Multi_Threaded_Processing_triggered();

    void on_videoControl_SpeedSlider_valueChanged(int value);

	void on_videoControl_SpeedSlider_sliderPressed();

	void on_videoControl_SpeedSlider_sliderReleased();

    void on_videoControl_Prev5Frames_clicked();

    void on_videoControl_PrevFrame_clicked();

    void on_videoControl_NextFrame_clicked();

    void on_videoControl_Next5Frames_clicked();

	void on_videoControl_FiveSecPrev_clicked();

	void on_videoControl_OneSecPrev_clicked();

	void on_videoControl_OneSecForw_clicked();

	void on_videoControl_FiveSecForw_clicked();

    void on_actionTake_screenshot_triggered();

    void on_actionSettings_triggered();

    void subwindowClosingHandler(ViewInfo* view);

    void jumpToFrameAtTime(QDateTime requestedTime);

signals:
	void jumpToFrame(const QDateTime &requestedTime);

protected slots:
    void slotSelectionChanged(const QItemSelection &selected, const QItemSelection &prevSelected);

protected:
    void closeEvent(QCloseEvent *event);

private:
    void setUpLayout();
    void setMaximumVideoSpeed(int fps);
    void showActiveDetectorButtons(bool showDetectors, QModelIndex index);

    void setVideoFiles();

    // Functions for reading and writing settings and configurations
    void writeSettings(QString prefix);
    void readSettings(QString prefix);
    void saveConfiguration(QString filename);
    void loadConfiguration(QString filename, bool loadVideos, bool loadDetectors);
    void loadConfiguration(QString filename, bool loadVideos, bool loadDetectors, 
                           QSplashScreen &splash, bool showSplash);
    void saveCurrentConfiguration();
    void loadPreviousConfiguration(QSplashScreen &splash);

    QList<OpenCVViewer*> viewers;
    QList<QMdiSubWindow*> subWindows;

    int addView(ViewInfo newView);
    int removeView(ViewInfo view);

    Ui::MainWindow *ui;

    // Base path for saving screenshots
    QString screenshotPath;

	// Video Handler for video and time management - read-only!
	std::shared_ptr<ruba::VideoHandler> VHandler;

	QTimer DisplayTimer;

	std::shared_ptr<ProcessingThread> thread;
	std::shared_ptr<MultiThreadedProcessingDialog> multiThreadedProcessingDialog;

    void createDetector(int detectorType);
    
    bool IsInitialized;
    bool ShowVideo;
	int devState;
	bool playbackToBeResumed;
	bool showWarningBeforePlayback;

	QString recordingMessage;
    QList<ViewInfo> currentViews;
	bool windowInitialized;
};

#endif // MAINWINDOW_H
