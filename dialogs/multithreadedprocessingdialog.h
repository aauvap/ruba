#ifndef MULTITHREADEDPROCESSINGDIALOG_H
#define MULTITHREADEDPROCESSINGDIALOG_H

#include <QDateEdit>
#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QToolButton>
#include <QLayout>
#include <QSpinBox>
#include <QDialogButtonBox>
#include <QFileInfo>
#include <QTableWidget>
#include <QTimer>
#include <QTimeEdit>
#include <QThread>

#include "processingThread.h"
#include "utils.hpp"
#include "videohandler.h"
#include "detectorhandler.h"

class SingleThreadWidget : public QWidget
{
	Q_OBJECT;

public:
	SingleThreadWidget(QWidget* parent, int number);

	void setVideoModel(QStandardItemModel* model);
	void setDetectorModel(QStandardItemModel* model);
	void setDateTime(const QDateTime dateTime);
	void setIsFinished(bool value);

private:
	QDateEdit* dateEdit;
	QTimeEdit* timeEdit;

	QTreeView* videoFilesView;
	QTreeView* detectorsView;
	
	QGroupBox* overallGroupBox;
	int threadNumber;
};

class MultiThreadedProcessingDialog : public QDialog
{
	Q_OBJECT;

public:
	MultiThreadedProcessingDialog(QWidget* parent, QList<ViewInfo> views, 
		const std::vector<ruba::FrameStruct>& frames, std::shared_ptr<ruba::VideoHandler> vHandler);
	~MultiThreadedProcessingDialog();

	void setVideoFiles(const std::vector<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture> >& videoFiles);
	void setDetectors(const std::vector<std::shared_ptr<ruba::AbstractCompoundDetector> >& detectors);
	bool getStartedStatus() { return processingIsStarted; };

	public slots:
	void on_applyThreadsButton_clicked(bool checked);
	void playPauseButtonClicked();
	void displayTimerTick();
	void recieveMessage(const QString & title, const QString & message, MessageType messageType);

protected:
	void closeEvent(QCloseEvent* e);

private:
	void initialiseProcessingThreads();
	void combineLogFiles();

	QDialogButtonBox *buttonBox;
	
	QGroupBox* processingThreadsGroupBox;
	QSpinBox* threadCountSpinBox;
	QLabel* addMoreVideosInfo;
	QPushButton* applyThreadsButton;

	QGroupBox* playbackGroupBox;
	QToolButton* playPauseButton;

	QGroupBox* actualProcessingGroupBox;
	QHBoxLayout* actualProcessingLayout;
	QVBoxLayout* overallActualProcessingLayout;

	QLabel* videoPlaybackFpsIcon;
	QLabel* videoPlaybackFpsLabel;

	std::vector<std::shared_ptr<ProcessingThread> > threads;
	std::vector<SingleThreadWidget*> threadViews;
	std::vector<double> processingSpeeds;

	QGroupBox* combineLogFilesGroupBox;

	std::vector<std::shared_ptr<ruba::AbstractSynchronisedVideoCapture> > videoFiles;
	std::vector<std::shared_ptr<ruba::AbstractCompoundDetector> > detectors;

	QList<ViewInfo> views;
	std::vector<ruba::FrameStruct> frames;

	QWidget *parent;

	QTimer DisplayTimer;
	bool processingIsStarted;
	std::shared_ptr<ruba::VideoHandler> vHandler;

	QTableView* statusTable;
	QStandardItemModel* statusText;
};




#endif // MULTITHREADEDPROCESSINGDIALOG_H
