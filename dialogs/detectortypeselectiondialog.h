#ifndef DETECTORTYPESELECTIONDIALOG_H
#define DETECTORTYPESELECTIONDIALOG_H

#include <QDialog>
#include <vector>
#include <opencv2/opencv.hpp>
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include "ruba.h"

namespace Ui {
class DetectorTypeSelectionDialog;
}

class DetectorTypeSelectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DetectorTypeSelectionDialog(QWidget *parent = 0, int detectorCount = 2, bool exclusive = false);
    ~DetectorTypeSelectionDialog();

    QVector<int> getOutput();
    QString getConfigurationFilename();
    int LoadConfiguration(QString file);

private slots:
    void on_detectorButton11_toggled(bool checked);

    void on_detectorButton12_toggled(bool checked);

    void on_detectorButton13_toggled(bool checked);

	void on_detectorButton14_toggled(bool checked);

    void on_detectorButton21_toggled(bool checked);

    void on_detectorButton22_toggled(bool checked);

    void on_detectorButton23_toggled(bool checked);

	void on_detectorButton24_toggled(bool checked);

    void on_loadConfigButton_clicked();

private:
    Ui::DetectorTypeSelectionDialog *ui;
    void setUpLayout();
    void generateDetectorOrder();

    QVector<int> detectorOrder;
    int detectorCount;
    QVector<bool> detectorIsSelected;
    QString filename;
};

#endif // DETECTORTYPESELECTIONDIALOG_H
