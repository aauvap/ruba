#ifndef SETTINGS_DIALOG_H
#define SETTINGS_DIALOG_H

#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QStackedWidget>
#include <QPushButton>
#include <QLayout>
#include <QDialogButtonBox>

#include "version.h"
#include "abstractsettingspage.h"

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    SettingsDialog(QWidget* parent, int devState);

public slots:
    void changePage(QListWidgetItem* current, QListWidgetItem* previous);
    void saveSettingsAndClose();

private:
    void createIcons();

    QListWidget* contentsWidget;
    QStackedWidget* pagesWidget;

    QList<AbstractSettingsPage*> pages;
	int devState;

};

#endif

