#ifndef SETTINGSPAGES_H
#define SETTINGSPAGES_H

#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QProcess>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>
#include <QWidget>

#include "abstractsettingspage.h"

class GeneralPage : public AbstractSettingsPage
{
	Q_OBJECT

public: 
    GeneralPage(QWidget *parent = 0);
    virtual void saveSettings();

private slots:
    void restorePrevCheckBoxChecked();
    void styleButtonChanged();

private:
    QCheckBox* restorePrevCheckBox;
    QLabel *includeLabel;
    QCheckBox *includeVideosCheckBox;
    QCheckBox *includeDetectorsCheckBox;
    QProcess *updateProcess;

    QComboBox* styleComboBox;
};

class SynchronisationPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    SynchronisationPage(QWidget *parent = 0);
    virtual void saveSettings();

private slots:

void videoFpsRadioManualToggled();
void dateTimeFormatComboBoxIndexChanged();
void startTimeInFileNameToggled();

private:
    QRadioButton *startTimeInFileName;

    QDoubleSpinBox *videoFpsSpinBox;
    QRadioButton *videoFpsRadioManual;

    QComboBox *dateTimeFormatCombo;
    QCheckBox *useEndTimeCheckBox;

    QGroupBox *startTimeInFileNameGroup;
    QGroupBox *timestampsInLogFileGroup;

    QLineEdit *logFileFormatEdit;
};

class ProcessingPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    ProcessingPage(QWidget *parent = 0);
    virtual void saveSettings();

    private slots:

    void videoSpeedRadioManualToggled();
    void processEveryFramesRadioManualToggled();
    void processEveryFramesSpinBoxValueChanged();
    void warningLowResCheckBoxChecked();

private:
    QSpinBox *videoSpeedSpinBox;
    QRadioButton *videoSpeedRadioManual;
	QCheckBox *useGpuAccelerationCheckBox;

    QSpinBox *processEveryFramesSpinBox;
    QRadioButton *processEveryFramesRadioManual;

    QCheckBox *warningLowResCheckBox;
    QLabel *lowResLabel;
    QLabel *lowResWidthLabel;
    QLabel *lowResHeightLabel;

    QSpinBox *lowResWidthSpinBox;
    QSpinBox *lowResHeightSpinBox;
};

class DebugPage : public AbstractSettingsPage
{
    Q_OBJECT

public:
    DebugPage(QWidget *parent = 0);
    virtual void saveSettings();

private:
	QCheckBox *debugWindowForegroundMaskCheckBox;
};

class RecordingPage : public AbstractSettingsPage
{
	Q_OBJECT

public:
	RecordingPage(QWidget *parent = 0);
	virtual void saveSettings();

	private slots:
	void writeMultipleVideosRadioToggled();

private:
	QRadioButton* writeMultipleVideosRadio;
	QSpinBox* maxConsecutiveTimeDifferenceInMillisecSpinBox;
};

#endif // !SETTINGSPAGES_H

