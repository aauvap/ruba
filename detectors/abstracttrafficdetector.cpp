#include "abstracttrafficdetector.h"

using namespace std;
using namespace cv;

using namespace ruba;


AbstractTrafficDetector::AbstractTrafficDetector(BaseDetectorParams baseParams)
{
    detectorID = baseParams.detectorID;
    detectorType = baseParams.detectorType;

    IsInitialized = true;

    diagnostics.state = DiagnosticState::Working;

    IsTriggered.resize(0);
    currentTime = QDateTime();    

    frames = baseParams.initFrames;    

    for (auto i = 0; i < baseParams.initFrames.size(); ++i) {
        initFrameResolution.push_back(baseParams.initFrames[i].Image.size());
        views.push_back(baseParams.initFrames[i].viewInfo);
    } 

    initTriggeredHistory(views.size());
    IsTriggered.resize(views.size());
}

AbstractTrafficDetector::~AbstractTrafficDetector()
{

}

bool AbstractTrafficDetector::GetIsInitialized()
{
    return this->IsInitialized;
}

std::vector<bool> AbstractTrafficDetector::GetIsTriggered()
{
    return this->IsTriggered;
}

int AbstractTrafficDetector::getDetectorType()
{
    return this->detectorType;
}

int AbstractTrafficDetector::getDetectorID()
{
    return this->detectorID;
}

DetectorDiagnostics AbstractTrafficDetector::getDiagnostics()
{
    return this->diagnostics;
}

QString AbstractTrafficDetector::getDetectorAbbreviation()
{
    return Utils::getDetectorAbbreviation(this->detectorType);
}

QString ruba::AbstractTrafficDetector::getDetectorName()
{
    return Utils::getDetectorName(this->detectorType, true);
}

QString ruba::AbstractTrafficDetector::getShortDetectorName()
{
    return Utils::getDetectorName(this->detectorType, false);
}

QList<QString> AbstractTrafficDetector::GetDebugInfo()
{
    QString detectorAbbrev = getDetectorAbbreviation();

    QList<QString> debugList;
    if (!IsTriggered.empty())
    {
        debugList.push_back(detectorAbbrev + ": Triggered: " 
            + QString::number(IsTriggered[0]));
    }

    for (int i = 1; i < IsTriggered.size(); ++i)
    {
        debugList.push_back(detectorAbbrev + ": Triggered-" + QString::number(i+1)
            + ": " + QString::number(IsTriggered[i]));
    }

    QList<QString> specificInfo = getDetectorSpecificDebugInfo();

    for (int i = 0; i < specificInfo.size(); ++i)
    {
        debugList.push_back(detectorAbbrev + ": " + specificInfo[i]);
    }
    
    return debugList;
}

std::vector<QDateTime> AbstractTrafficDetector::GetLastChange()
{
    return this->lastChange;
}

void AbstractTrafficDetector::Update(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput)
{
    DiagnosticState prevState = diagnostics.state;

    if (prevState != DiagnosticState::Exception) {
        // Only continue if we have not encountered an unrecoverable error previously

        this->frames = frames;
        updateTime(frames);

        IsInitialized = false;


        if (reshapeUpdateStruct() && checkFrameResolution()) {
            // If the frames update struct does not fit, the detector is not in a working state. 
            // Deactivate it.
            IsInitialized = true;
        }

        if (IsInitialized) {
            diagnostics.state = DiagnosticState::Working;
            diagnostics.errorMessage = "";


            try {
                updateDetectorSpecifics(frames, dOutput);
            }
            catch (cv::Exception e) {
                // We have encountered an unrecoverable error.
                // Set the error state accordingly and write an appropriate error message
                diagnostics.state = DiagnosticState::Exception;
                diagnostics.errorMessage = QString(QObject::tr("An unexpected error occurred in %1 [%2] and the detector cannot recover. The detector should be deleted from the list of detectors and re-initialised.\n\nPlease report the bug by using the details below:\nError: %3\nError code: %4\nFunction: %5\nLine: %6\nMessage: "))
                    .arg(getDetectorName())
                    .arg(getDetectorID())
                    .arg(QString::fromStdString(e.err))
                    .arg(e.code)
                    .arg(QString::fromStdString(e.func))
                    .arg(e.line)
                    .arg(QString::fromStdString(e.msg));
            }
            catch (std::exception e)
            {
                // We have encountered an unrecoverable error, probably from the STL
                // Set the error state accordingly and write an appropriate error message
                diagnostics.state = DiagnosticState::Exception;
                diagnostics.errorMessage = QString(QObject::tr("An unexpected error occurred in %1 [%2] and the detector cannot recover. The detector should be deleted from the list of detectors and re-initialised.\n\nPlease report the bug by using the details below:\nError: %3"))
                    .arg(getDetectorName())
                    .arg(getDetectorID())
                    .arg(QString::fromStdString(e.what()));
            }

        }

    }

    diagnostics.errorStateChanged = (prevState != diagnostics.state);
}

void AbstractTrafficDetector::initTriggeredHistory(int dim)
{
    triggeredHistory.clear();

    for (int j = 0; j < dim; ++j)
    {
        QQueue<bool> tHistory;

        for (int i = 0; i < 10; ++i)
        {
            tHistory.enqueue(false);
        }

        triggeredHistory.push_back(tHistory);
    }
}

void AbstractTrafficDetector::ResetTriggeredHistory()
{
    triggeredHistory.clear();
    initTriggeredHistory(1);
}

std::vector<QQueue<bool> > AbstractTrafficDetector::GetTriggeredHistory()
{
    if (this->triggeredHistory.empty())
    {
        QQueue<bool> tmpQueue;
        tmpQueue.push_back(false);
        std::vector<QQueue<bool>> tHistory;
        tHistory.push_back(tmpQueue);

        return tHistory;
    }
    return this->triggeredHistory;
}


void AbstractTrafficDetector::AddParent(FamilyInfo parent)
{
 bool isInList = false;
 for (int i = 0; i < parentDetectors.size(); ++i)
 {
     if (parentDetectors[i].detectorID == parent.detectorID)
     {
         isInList = true;
     }
 }

 if (!isInList)
 {
     this->parentDetectors.push_back(parent);
 }
}

void AbstractTrafficDetector::RemoveParent(FamilyInfo parent)
{
 for (int i = 0; i < parentDetectors.size(); ++i)
 {
     if (parentDetectors[i].detectorID == parent.detectorID)
     {
         parentDetectors.removeAt(i);
     }
 }

}

bool AbstractTrafficDetector::HasParents()
{
 if (parentDetectors.empty())
 {
     return false;
 } else {
     return true;
 }
}

QList<FamilyInfo> AbstractTrafficDetector::GetChildren()
{
 return this->childDetectors;
}

bool AbstractTrafficDetector::HasChildren()
{
 if (childDetectors.empty())
 {
     return false;
 } else {
     return true;
 }
}

void AbstractTrafficDetector::updateTime(std::vector<FrameStruct> frames)
{
    // Update the detector time from the frameStructs provided.
    // This is done here to ensure uniformity throughout the detectors

    updateTime(frames[0]);
}

void AbstractTrafficDetector::updateTime(FrameStruct frame)
{
    // Update the detector time from the frameStructs provided.
    // This is done here to ensure uniformity throughout the detectors

    currentTime = frame.Timestamp;
}

// Check if the provided modalities correspond to the current modalities
bool AbstractTrafficDetector::checkCurrentViews(QList<ViewInfo> enabledViews)
{
    bool areRequiredViewsEnabled = true;
    QList<ViewInfo> missingViews;

    for (auto i = 0; i < views.size(); ++i)
    {
        bool modalityFound = false;

        for (int j = 0; j < enabledViews.size(); ++j)
        {
            if (views[i].compare(enabledViews[j]) == 0)
            {
                modalityFound = true;
                break;
            }
        }

        if (!modalityFound)
        {
            areRequiredViewsEnabled = false;
            missingViews.push_back(views[i]);
        }
    }

    
    if (!areRequiredViewsEnabled) {
        diagnostics.state = DiagnosticState::Error; 
        diagnostics.errorStateChanged = true;
        QString requiredModalitieDescr, missingModalitiesDescr;

        for (auto i = 0; i < views.size(); ++i) {
            requiredModalitieDescr += QObject::tr("View %1").arg(views[i].viewName()) + "\n";
        }

        for (auto i = 0; i < missingViews.size(); ++i) {
            missingModalitiesDescr += QObject::tr("View %1")
                .arg(missingViews[i].viewName()) + "\n";
        }

        diagnostics.errorMessage = QObject::tr("The %1 [%2] is temporarily disabled because the required views are currently not available. \n\n"
            "The detector requires that the following views are enabled:\n%3\n"
            "The following views are currently missing:\n%4\nPlease enable the required views and restart the video.")
            .arg(getDetectorName())
            .arg(getDetectorID())
            .arg(requiredModalitieDescr)
            .arg(missingModalitiesDescr);
    }



    return areRequiredViewsEnabled;
}

BaseDetectorParams ruba::AbstractTrafficDetector::configureBaseDetectorParams(BaseDetectorParams initBaseParams, int detectorType)
{
    initBaseParams.detectorType = detectorType;

    return initBaseParams;
}

void ruba::AbstractTrafficDetector::reconfigureBaseDetectorParams(BaseDetectorParams newBaseParams)
{
    frames = newBaseParams.initFrames;
    updateTime(frames);

    initFrameResolution.clear();
    views.clear();

    for (auto i = 0; i < newBaseParams.initFrames.size(); ++i) {
        initFrameResolution.push_back(newBaseParams.initFrames[i].Image.size());
        views.push_back(newBaseParams.initFrames[i].viewInfo);
    }

}

QDateTime ruba::AbstractTrafficDetector::getCurrentTime()
{
    return currentTime;
}

QList<ViewInfo> ruba::AbstractTrafficDetector::getCurrentViews()
{
    return views;
}

bool AbstractTrafficDetector::reshapeUpdateStruct()
{
    if (detectorType == GROUNDTRUTHANNOTATOR)
    {
        // The ground truth annoator runs under all views
        return true;
    }

    QList<ViewInfo> providedViews;

    for (size_t i = 0; i < frames.size(); ++i)
    {
        providedViews.push_back(frames[i].viewInfo);
    }

    if (!checkCurrentViews(providedViews))
    {
        return false;
    }

    // If the modalities of the provided FrameStruct contains all 
    // the required modalities, we may continue to reshape the update struct
    bool alreadyFits = true;

    for (int i = 0; i < views.size(); ++i)
    {
        if (views[i].compare(providedViews[i]) != 0)
        {
            alreadyFits = false;
        }
    }

    if (alreadyFits && (providedViews.size() == views.size()))
    {
        return true;
    }
    
    // Reshape the update struct
    vector<FrameStruct> newFrames;

    for (int i = 0; i < views.size(); ++i)
    {
        for (size_t j = 0; j < frames.size(); ++j)
        {
            if (frames[j].viewInfo.compare(views[i]) == 0)
            {
                newFrames.push_back(frames[j]);
                break;
            }
        }
    }

    frames = newFrames;

    return true;
}

bool ruba::AbstractTrafficDetector::checkFrameResolution()
{
    if (detectorType == GROUNDTRUTHANNOTATOR || detectorType == LOGFILEREVIEWER)
    {
        // The ground truth annotator and log file reviewer runs under all frame resolutions
        return true;
    }

    bool doesFrameResolutionFit = false;

    if (frames.size() == initFrameResolution.size()) {
        doesFrameResolutionFit = true;

        for (auto i = 0; i < initFrameResolution.size(); ++i) {
            if (frames[i].Image.size() != initFrameResolution[i]) {
                doesFrameResolutionFit = false;
            }
        }
    }


    if (!doesFrameResolutionFit) {
        QString requiredResText, providedResText;
        for (auto i = 0; i < initFrameResolution.size(); ++i) {
            if (views.size() > i) {
                requiredResText += QObject::tr("View %1: %2 x %3\n")
                    .arg(views[i].viewName())
                    .arg(initFrameResolution[i].width)
                    .arg(initFrameResolution[i].height);
            }
        }

        for (auto i = 0; i < initFrameResolution.size(); ++i) {
            if (views.size() > i) {
                providedResText += QObject::tr("View %1: %2 x %3\n")
                    .arg(frames[i].viewInfo.viewName())
                    .arg(frames[i].Image.size().width)
                    .arg(frames[i].Image.size().height);
            }
        }

        diagnostics.state = DiagnosticState::Error;
        diagnostics.errorMessage = QObject::tr(
            "The image resolution of one or more of the provided videos does not fit the configuration "
            "of the %1 [%2].\n\n"
            "Required image resolution of detector:\n%3\nProvided image resolution of current video:\n%4\n\n"
            "Use a video with the required image resolution or reconfigure the detector to resolve this problem.").
            arg(Utils::getDetectorName(detectorType)).
            arg(getDetectorID()).
            arg(requiredResText).
            arg(providedResText);
    }

    

    return doesFrameResolutionFit;
}


