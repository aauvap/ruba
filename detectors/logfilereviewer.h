#ifndef LOGFILEREVIEWER_H
#define LOGFILEREVIEWER_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "dialogs/logfilereviewerdialog.h"
#include "detectors/abstractcompounddetector.h"

class ruba::LogFileReviewer : public AbstractCompoundDetector
{

public:
    explicit LogFileReviewer(BaseDetectorParams baseParams, QWidget *parent);

    ~LogFileReviewer();

    ruba::DetectorOutput GetOutput();

    virtual std::vector<std::vector<MaskOverlayStruct> > GetMasks();

    void Reconfigure(std::vector<ruba::FrameStruct> initFrames, QWidget *parent);
    void SetLastFrameReached();

private slots:
    void validateShortCut(int eventCode);

signals:
    void emitShortCut(int eventCode);

private:
    virtual void setDetectorSpecificDefaultParameters();
    cv::Mat baseMask;

    virtual QList<QString> getDetectorSpecificExtendedDebugInfo();
    void updateDetectorSpecifics(std::vector<ruba::FrameStruct> frames, std::vector<DetectorOutput> dOutput);
    void updateDetectorSpecificLogic();

    LogFileReviewerDialog* lfrDialog;

};

#endif // LOGFILEREVIEWER_H
