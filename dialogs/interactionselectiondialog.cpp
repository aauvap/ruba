#include "interactionselectiondialog.h"
#include "ui_interactionselectiondialog.h"


using namespace cv;
using namespace std;
using namespace ruba;

interactionselectiondialog::interactionselectiondialog(QWidget *parent, std::vector<ruba::DetectorOutput> detectorInfo,
                                                       QString windowTitle, QIcon windowIcon) :
    QDialog(parent),
    ui(new Ui::interactionselectiondialog)
{
    ui->setupUi(this);

    this->setWindowTitle(windowTitle);
    this->setWindowIcon(windowIcon);

    this->detectorInfo = detectorInfo;

    checkSetUpLayout();
}

interactionselectiondialog::~interactionselectiondialog()
{
    delete ui;
}

void interactionselectiondialog::on_choose312Button_clicked(bool checked)
{
    this->ui->choose410Button->blockSignals(true);
    this->ui->choose410Button->setChecked(!checked);
    this->ui->choose410Button->blockSignals(false);
    detectorType = INTERACTIONDETECTOR;
    

    checkSetUpLayout();
}

void interactionselectiondialog::on_choose410Button_clicked(bool checked)
{
    this->ui->choose312Button->blockSignals(true);
    this->ui->choose312Button->setChecked(!checked);
    this->ui->choose312Button->blockSignals(false);
    detectorType = INTERACTION410DETECTOR;


    checkSetUpLayout();
}

void interactionselectiondialog::checkSetUpLayout()
{
    bool isVisible = true;

    if (!this->ui->choose312Button->isChecked() && !this->ui->choose410Button->isChecked())
    {
        isVisible = false;

        // Initialize detector
        this->requestedDetectorIDs.clear();
        this->requestedDetectorIDs.resize(2);
    } 

    ui->logSettingsGroupBox->setVisible(isVisible);
    ui->subDetectorsGroupBox->setVisible(isVisible);

    if (this->ui->choose312Button->isChecked())
    {
        this->ui->moduleText1->setText("Right Turning Car Module");
        this->ui->moduleBox1->clear();
        this->ui->moduleBox2->clear();

        rightTurningCarID.clear();
        straightGoingCyclistsID.clear();

        for (int i = 0; i < detectorInfo.size(); ++i)
        {
            if (detectorInfo[i].DetectorType == RIGHTTURNINGCAR)
            {
                rightTurningCarID.push_back(detectorInfo[i].DetectorID);
                this->ui->moduleBox1->addItem(Utils::getDetectorName(detectorInfo[i].DetectorType) + QString(" [%1]").arg(detectorInfo[i].DetectorID));
            }

            if (detectorInfo[i].DetectorType == STRAIGHTGOINGCYCLIST)
            {
                straightGoingCyclistsID.push_back(detectorInfo[i].DetectorID);
                this->ui->moduleBox2->addItem(Utils::getDetectorName(detectorInfo[i].DetectorType) + QString(" [%1]").arg(detectorInfo[i].DetectorID));
            }
        }
    }

    if (this->ui->choose410Button->isChecked())
    {
        this->ui->moduleText1->setText("Left Turning Car Module");
        this->ui->moduleBox1->clear();
        this->ui->moduleBox2->clear();

        leftTurningCarID.clear();
        straightGoingCyclistsID.clear();

        for (int i = 0; i < detectorInfo.size(); ++i)
        {
            if (detectorInfo[i].DetectorType == LEFTTURNINGCAR)
            {
                leftTurningCarID.push_back(detectorInfo[i].DetectorID);
                this->ui->moduleBox1->addItem(Utils::getDetectorName(detectorInfo[i].DetectorType) + QString(" [%1]").arg(detectorInfo[i].DetectorID));
            }

            if (detectorInfo[i].DetectorType == STRAIGHTGOINGCYCLIST)
            {
                straightGoingCyclistsID.push_back(detectorInfo[i].DetectorID);
                this->ui->moduleBox2->addItem(Utils::getDetectorName(detectorInfo[i].DetectorType) + QString(" [%1]").arg(detectorInfo[i].DetectorID));
            }
        }
    }

    if (this->ui->moduleBox1->count() == 0)
    {
        this->ui->moduleBox1->addItem("(No module initialized)");
        this->ui->moduleBox1->setEnabled(false);
    } else {
        this->ui->moduleBox1->setEnabled(true);
    }

    if (this->ui->moduleBox2->count() == 0)
    {
        this->ui->moduleBox2->addItem("(No module initialized)");
        this->ui->moduleBox2->setEnabled(false);
    } else {
        this->ui->moduleBox2->setEnabled(true);
    }

    if (ui->saveFrameEventPathLine->text().isEmpty())
    {
        QSettings settings;
        settings.beginGroup("detectorSelectionDialog");
        ui->saveFrameEventPathLine->setText(settings.value("detectorBaseDir", "").toString());
        settings.endGroup();
    }

    checkConfiguration();
}

void interactionselectiondialog::on_logFileSavePathButton1_clicked()
{
    QString filePath;
    QString logFileName;

    if (this->ui->logFileSavePathLine1->text().isEmpty() || (!this->ui->logFileSavePathLine1->text().compare("analytics.csv")))
    {
        if (this->ui->choose312Button->isChecked())
        {
            logFileName = "312_analytics.csv";
        } else if (this->ui->choose410Button->isChecked())
        {
            logFileName = "410_analytics.csv";
        }

    } else {
        logFileName = this->ui->logFileSavePathLine1->text();
    }

    filePath = QFileDialog::getSaveFileName(this, tr("Save traffic analytics"), tr(logFileName.toStdString().c_str()), tr("Log files (*.csv)"));

    if (!filePath.isEmpty())
    {
        this->ui->logFileSavePathLine1->setText(filePath);
    }
}

void interactionselectiondialog::on_logFileSavePathButton2_clicked()
{
    QString filePath;
    QString logFileName;

    if (this->ui->logFileSavePathLine2->text().isEmpty()  || !this->ui->logFileSavePathLine2->text().compare("counting_analytics.csv"))
    {
        if (this->ui->choose312Button->isChecked())
        {
            logFileName = "312_counting_analytics.csv";
        } else if (this->ui->choose410Button->isChecked())
        {
            logFileName = "410_counting_analytics.csv";
        }
    } else {
        logFileName = this->ui->logFileSavePathLine1->text();
    }

    filePath = QFileDialog::getSaveFileName(this, tr("Save traffic counting analytics"), tr(logFileName.toStdString().c_str()), tr("Log files (*.csv)"));

    if (!filePath.isEmpty())
    {
        this->ui->logFileSavePathLine2->setText(filePath);
    }
}

void interactionselectiondialog::on_saveFrameEventPathButton_clicked()
{
    QString filePath = QFileDialog::getExistingDirectory(this, tr("Select directory to save frames"),
        ui->saveFrameEventPathLine->text(), QFileDialog::ShowDirsOnly
        | QFileDialog::DontResolveSymlinks);

    if (!filePath.isEmpty())
    {
        ui->saveFrameEventPathLine->setText(filePath);
    }
}

AdditionalSettingsStruct interactionselectiondialog::getAdditionalSettings()
{
    addSettings.childDetectors.clear();

    if (this->detectorType == INTERACTIONDETECTOR)
    {
        if (!rightTurningCarID.empty())
        {
            FamilyInfo rtcDetector;
            rtcDetector.detectorID = this->ui->moduleBox1->currentIndex();
            rtcDetector.detectorType = RIGHTTURNINGCAR;
            addSettings.childDetectors.push_back(rtcDetector);
        }
    }
    else if (this->detectorType == INTERACTION410DETECTOR)
    {
        if (!leftTurningCarID.empty())
        {
            FamilyInfo ltcDetector;
            ltcDetector.detectorID = this->ui->moduleBox1->currentIndex();
            ltcDetector.detectorType = LEFTTURNINGCAR;
            addSettings.childDetectors.push_back(ltcDetector);
        }

    }


    if (!straightGoingCyclistsID.empty())
    {
        FamilyInfo sgcDetector;
        sgcDetector.detectorID = this->ui->moduleBox2->currentIndex();
        sgcDetector.detectorType = STRAIGHTGOINGCYCLIST;
        addSettings.childDetectors.push_back(sgcDetector);
    }

    return addSettings;
}

void interactionselectiondialog::checkConfiguration()
{
    // checkConfiguration detects if all all detectors + logs are initialized
    // and enables the saveConfiguration and OK button accordingly
    bool isConfigurationInitialized = true;

    if (!this->ui->moduleBox1->isEnabled() || !this->ui->moduleBox2->isEnabled())
    {
        isConfigurationInitialized = false;
    }


    if (this->ui->logEveryCheckBox1->isChecked() && (this->ui->logFileSavePathLine1->text().isEmpty()))
    {
        isConfigurationInitialized = false;
    }

    if (this->ui->logSumCheckBox->isChecked() && (this->ui->logFileSavePathLine2->text().isEmpty()))
    {
        isConfigurationInitialized = false;
    }

    if (ui->saveFrameEventCheckBox->isChecked() && ui->saveFrameEventPathLine->text().isEmpty())
    {
        isConfigurationInitialized = false;
    }


    ui->okButtonBox->setEnabled(isConfigurationInitialized);
    ui->saveConfigButton->setEnabled(isConfigurationInitialized);
    ui->saveConfigAsButton->setEnabled(isConfigurationInitialized);


}

void interactionselectiondialog::on_logEveryCheckBox1_toggled(bool checked)
{
    if (checked)
    {
        this->ui->logFileSavePathButton1->setVisible(true);
        this->ui->logFileSavePathLine1->setVisible(true);
    } else {
        this->ui->logFileSavePathButton1->setVisible(false);
        this->ui->logFileSavePathLine1->setVisible(false);
    }

    checkConfiguration();

}

void interactionselectiondialog::on_logSumCheckBox_toggled(bool checked)
{

    this->ui->logFileSavePathButton2->setVisible(checked);
    this->ui->logFileSavePathLine2->setVisible(checked);
    this->ui->logIntervalLabel->setVisible(checked);
    this->ui->logIntervalSpinBox->setVisible(checked);

    checkConfiguration();
}

void interactionselectiondialog::on_saveFrameEventCheckBox_toggled(bool checked)
{
    ui->saveFrameEventPathButton->setVisible(checked);
    ui->saveFrameEventPathLine->setVisible(checked);

    checkConfiguration();
}

void interactionselectiondialog::on_logFileSavePathLine1_textChanged(const QString &arg1)
{
    checkConfiguration();
}

void interactionselectiondialog::on_logFileSavePathLine2_textChanged(const QString &arg1)
{
    checkConfiguration();
}

void interactionselectiondialog::on_saveFrameEventPathLine_textChanged(const QString &arg1)
{
    checkConfiguration();
}



void interactionselectiondialog::on_loadConfigButton_clicked()
{
    loadConfiguration();
}

int interactionselectiondialog::loadConfiguration(QString filename)
{
    bool silentMode;

    if (filename.isEmpty())
    {
        QSettings settings;
        settings.beginGroup("detectorSelectionDialog");
        QString detectorBaseDir = settings.value("detectorBaseDir", "").toString();
        settings.endGroup();

        filename = QFileDialog::getOpenFileName(this, tr("Load configuration parameters"),
            detectorBaseDir, tr("Configuration files (*.yml)"));
        silentMode = false;
    }
    else {
        silentMode = true;
    }

    int nDetectorType;

    if (filename.contains(".yml"))
    {
        // Update the settings;
        QSettings settings;
        settings.beginGroup("detectorSelectionDialog");
        settings.setValue("detectorBaseDir", QFileInfo(filename).absolutePath());
        settings.endGroup();

        QProgressDialog progress("Loading configuration", QString(), 1, 100, this);
        progress.setWindowModality(Qt::WindowModal);
        progress.setMinimumDuration(3000);
        addSettings.configSaveFilePath = filename;

        FileStorage fs;

        try {
            fs.open(filename.toStdString(), FileStorage::READ);
        } catch (cv::Exception e) {
            progress.close();
            qDebug() << e.what();
            QMessageBox::warning(this, tr(this->windowTitle().toStdString().c_str()), "Unable to load file", QMessageBox::Ok);
        }

        // Check if the file contains any valid configuration
        nDetectorType = fs["detectorType"];

        if (!(nDetectorType == INTERACTIONDETECTOR || nDetectorType == INTERACTION410DETECTOR))
        {
            if (!silentMode)
            {
                progress.close();
                QString message = QString("Unable to load configuration from file.\n\nThe file does not contain the settings of an interaction detector");
                QMessageBox::warning(this, tr(this->windowTitle().toStdString().c_str()), message.toStdString().c_str(), QMessageBox::Ok);
            }
            return 1;
        }
        progress.setValue(20);

        detectorType = nDetectorType;
        QString paramName;
        QString basePrefix;

        if (nDetectorType == INTERACTIONDETECTOR)
        {
            basePrefix = "T-IAC312";
            this->ui->choose312Button->setChecked(true);
            this->ui->choose410Button->setChecked(false);
        } else if (nDetectorType == INTERACTION410DETECTOR)
        {
            basePrefix = "T-IAC410";
            this->ui->choose312Button->setChecked(false);
            this->ui->choose410Button->setChecked(true);
        }
        checkSetUpLayout();

        // Read the basic interaction parameters
        try
        {
            paramName = basePrefix + "-DetectorID";
            fs[paramName.toStdString()] >> detectorID;
            progress.setValue(30);

            paramName.clear();
            paramName = basePrefix + "-RequestedDetectorIDs";
            vector<int> regIDs;
            fs[paramName.toStdString()] >> regIDs;
            if (!regIDs.empty())
            {
                requestedDetectorIDs = regIDs;
            }
            progress.setValue(40);

            // Search for matching detector IDs
            int matchingIndex = 0;

            if (detectorType == INTERACTIONDETECTOR && (requestedDetectorIDs.size() == 2))
            {
                for (int i = 0; i < rightTurningCarID.size(); ++i)
                {
                    if (requestedDetectorIDs[0] == rightTurningCarID[i])
                    {
                        matchingIndex = i;
                    }
                }
            } else if (detectorType == INTERACTION410DETECTOR && (requestedDetectorIDs.size() == 2))
            {
                for (int i = 0; i < leftTurningCarID.size(); ++i)
                {
                    if (requestedDetectorIDs[0] == leftTurningCarID[i])
                    {
                        matchingIndex = i;
                    }
                }
            }

            this->ui->moduleBox1->setCurrentIndex(matchingIndex);
            matchingIndex = 0;

            if (requestedDetectorIDs.size() == 2)
            {
                for (int i = 0; i < straightGoingCyclistsID.size(); ++i)
                {
                    if (requestedDetectorIDs[0] == straightGoingCyclistsID[i])
                    {
                        matchingIndex = i;
                    }
                }
            }

            this->ui->moduleBox2->setCurrentIndex(matchingIndex);

            double tmpTimegap;
            paramName.clear();
            paramName = basePrefix + "-Timegap1";
            fs[paramName.toStdString()] >> tmpTimegap;
            this->ui->timegapBox1->setValue(tmpTimegap);
            progress.setValue(50);

            paramName.clear();
            paramName = basePrefix + "-Timegap2";
            fs[paramName.toStdString()] >> tmpTimegap;
            this->ui->timegapBox2->setValue(tmpTimegap);
            progress.setValue(60);

            // Read the configuration of the log files
            String logPath;
            paramName.clear();
            paramName = basePrefix + "-LogEveryPath";
            fs[paramName.toStdString()] >> logPath;

            if (!logPath.empty())
            {
                this->ui->logFileSavePathLine1->setText(QString::fromStdString(logPath));
            }
            progress.setValue(65);

            paramName.clear();
            paramName = basePrefix + "-LogSumPath";
            fs[paramName.toStdString()] >> logPath;

            if (!logPath.empty())
            {
                this->ui->logFileSavePathLine2->setText(QString::fromStdString(logPath));
            }

            fs["SaveFrameOfEventPath"] >> logPath;
            if (!logPath.empty())
            {
                ui->saveFrameEventPathLine->setText(QString::fromStdString(logPath));
            }

            progress.setValue(70);

            int logSumInterval;
            paramName.clear();
            paramName = basePrefix + "-LogSumInterval";
            fs[paramName.toStdString()] >> logSumInterval;
            if (logSumInterval > 0)
            {
                this->ui->logIntervalSpinBox->setValue(logSumInterval);
            }
            progress.setValue(80);


            // Read settings
            paramName.clear();
            paramName = basePrefix + "-LogSettings";
            vector<int> logSettings;
            fs[paramName.toStdString()] >> logSettings;
            progress.setValue(90);

            for (size_t i = 0; i < logSettings.size(); ++i)
            {
                switch (i)
                {
                case 0:
                    this->ui->logEveryCheckBox1->setChecked(logSettings[i]);
                    break;
                case 1:
                    this->ui->logSumCheckBox->setChecked(logSettings[i]);
                    break;
                }
            }

            bool saveFrameOfEvent = false;
            fs["SaveFrameOfEvent"] >> saveFrameOfEvent;
            ui->saveFrameEventCheckBox->setChecked(saveFrameOfEvent);

            progress.setValue(99);

        } catch (cv::Exception e){
            qDebug() << e.what();
        }

        progress.setValue(98);

        checkConfiguration();
        progress.setValue(99);

    } else {
        if (!filename.isEmpty())
        {
            if (!silentMode)
            {
                QMessageBox::warning(this, tr(this->windowTitle().toStdString().c_str()), "Unable to load configuration from file", QMessageBox::Ok);
            }

            return 1;
        }
    }

    return 0;
}
void interactionselectiondialog::on_saveConfigButton_clicked()
{
    if (!addSettings.configSaveFilePath.isEmpty())
    {
        if (detectorType == INTERACTIONDETECTOR)
        {
            basePrefix = "T-IAC312";
        }
        else if (detectorType == INTERACTION410DETECTOR)
        {
            basePrefix = "T-IAC410";
        }

        saveConfiguration(addSettings.configSaveFilePath);
    }
    else {
        on_saveConfigAsButton_clicked();
    }
}

void interactionselectiondialog::on_saveConfigAsButton_clicked()
{
    QString filename, defaultConfigFileName;

    if (detectorType == INTERACTIONDETECTOR)
    {
        basePrefix = "T-IAC312";
        defaultConfigFileName = "interaction312Settings.yml";
    }
    else if (detectorType == INTERACTION410DETECTOR)
    {
        basePrefix = "T-IAC410";
        defaultConfigFileName = "interaction410Settings.yml";
    }

    QSettings settings;
    settings.beginGroup("detectorSelectionDialog");
    QString detectorBaseDir = settings.value("detectorBaseDir", "").toString();
    settings.endGroup();

    detectorBaseDir = detectorBaseDir + "/" + defaultConfigFileName;


    filename = QFileDialog::getSaveFileName(this, tr("Save configuration parameters"), 
        detectorBaseDir, tr("Configration files (*.yml)"));

    saveConfiguration(filename);
    
}

int interactionselectiondialog::saveConfiguration(QString filename)
{
    // Save the parameters of the Interaction Detector
    FileStorage fs;

    if (filename.contains(".yml"))
    {
        QProgressDialog progress("Saving configuration", QString(), 1, 100, this);
        progress.setWindowModality(Qt::WindowModal);
        progress.setMinimumDuration(1500);

        // Update the settings;
        QSettings settings;
        settings.beginGroup("detectorSelectionDialog");
        settings.setValue("detectorBaseDir", QFileInfo(filename).absolutePath());
        settings.endGroup();

        addSettings.configSaveFilePath = filename;

        fs.open(filename.toStdString(), FileStorage::WRITE);

        QString paramName;

        fs << "detectorType"  << detectorType;

        progress.setValue(20);

        // Save the base interaction detector parameters
        paramName = basePrefix + "-RequestedDetectorIDs";
        fs << paramName.toStdString() << requestedDetectorIDs;
        progress.setValue(30);

        paramName.clear();
        paramName = basePrefix + "-DetectorID";
        fs << paramName.toStdString() << detectorID;
        progress.setValue(40);

        paramName.clear();
        paramName = basePrefix + "-Timegap1";
        fs << paramName.toStdString() << this->ui->timegapBox1->value();
        progress.setValue(60);

        paramName.clear();
        paramName = basePrefix + "-Timegap2";
        fs << paramName.toStdString() << this->ui->timegapBox2->value();
        progress.setValue(70);

        // Save the configuration of the log file(s)
        paramName.clear();
        paramName = basePrefix + "-LogEveryPath";
        fs << paramName.toStdString() << this->ui->logFileSavePathLine1->text().toStdString();

        progress.setValue(80);

        paramName.clear();
        paramName = basePrefix + "-LogSumPath";
        fs << paramName.toStdString() << this->ui->logFileSavePathLine2->text().toStdString();

        fs << "SaveFrameOfEventPath" << ui->saveFrameEventPathLine->text().toStdString();


        progress.setValue(90);

        paramName.clear();
        paramName = basePrefix + "-LogSumInterval";
        fs << paramName.toStdString() << this->ui->logIntervalSpinBox->value();

        if (this->ui->logEveryCheckBox1->isChecked() || this->ui->logSumCheckBox->isChecked())
        {
            // Store settings
            paramName.clear();
            paramName = basePrefix + "-LogSettings";
            vector<int> logSettings;

            logSettings.push_back(this->ui->logEveryCheckBox1->isChecked());
            logSettings.push_back(this->ui->logSumCheckBox->isChecked());

            fs << paramName.toStdString() << logSettings;
        }

        fs << "SaveFrameOfEvent" << ui->saveFrameEventCheckBox->isChecked();


        progress.setValue(99);


        fs.release();
    }

    return 0;
}

void interactionselectiondialog::on_moduleBox1_currentIndexChanged(int index)
{
    if (index >= 0)
    {
        if (detectorType == INTERACTIONDETECTOR && index < rightTurningCarID.size())
        {
            this->requestedDetectorIDs[0] = rightTurningCarID[index];
        } else if (detectorType == INTERACTION410DETECTOR && index < leftTurningCarID.size())
        {
            this->requestedDetectorIDs[0] = leftTurningCarID[index];
        }
    }
}

void interactionselectiondialog::on_moduleBox2_currentIndexChanged(int index)
{
    if (index >= 0 && (index < straightGoingCyclistsID.size()))
    {
        this->requestedDetectorIDs[1] = straightGoingCyclistsID[index];
    }
}
