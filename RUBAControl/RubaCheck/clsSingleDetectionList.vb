﻿Public Class clsSingleDetectionList
  Public Detections As List(Of clsSingleDetection)
  Public ImageFolder As String

  Private MyCSVFilePath As String



  Public Sub New(ByRef CSVFilePath As String, ByVal ImageFolder As String)


    Detections = New List(Of clsSingleDetection)
    MyCSVFilePath = CSVFilePath
    Me.ImageFolder = ImageFolder

    Call ReadCSVFile()

  End Sub



  Private Function ReadCSVFile() As Boolean
    Dim _FileIndex As Integer
    Dim _Line As String
    Dim _Fields() As String
    Dim _Detection As clsSingleDetection


    _FileIndex = FreeFile()
    Try
      FileOpen(_FileIndex, MyCSVFilePath, OpenMode.Input)
    Catch ex As Exception
      MsgBox("File seems to be already open by other program.")
      Return False
    End Try

    Try
      Do Until EOF(_FileIndex)
        _Line = LineInput(_FileIndex)
        If Not _Line.StartsWith("File;Date;Entering") Then
          _Fields = _Line.Split(";")
          _Detection = New clsSingleDetection

          With _Detection
            .File = _Fields(0)
            .DetectionDate = TextToShortDate(_Fields(1))
            .Entering1 = TextToFullDate(_Fields(2))
            .Exiting1 = TextToFullDate(_Fields(3))
            .Duration1 = CInt(_Fields(4))
            .ImageFileName = _Fields(5)
          End With
          Detections.Add(_Detection)
        End If
      Loop
      FileClose(_FileIndex)

    Catch ex As Exception
      MsgBox("Failed to read the data structure in the .csv file.")
      FileClose(_FileIndex)
      Return False
    End Try

    Return True
  End Function

End Class
