#ifndef ABSTRACTCOMPOUNDDETECTOR_H
#define ABSTRACTCOMPOUNDDETECTOR_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "ruba.h"
#include "abstracttrafficdetector.h"
#include "abstractmultimodaldetector.h"
#include "presencedetector.h"
#include "movementdetector.h"
#include "dialogs/interactionselectiondialog.h"
#include "trafficlightdetector.h"
#include "stationarydetector.h"
#include "detectorinfo.h"

#include "logger.h"
#include "addsettingsstruct.h"



class ruba::AbstractCompoundDetector : public AbstractTrafficDetector
{

public:
    AbstractCompoundDetector(BaseDetectorParams baseParams);

    virtual void Reconfigure(std::vector<ruba::FrameStruct> initFrames, QWidget *parent);
    ruba::DetectorOutput GetOutput() = 0;
    
    QList<QString> GetExtendedDebugInfo();
    QList<QString> GetEveryEventLogInfo();
    std::map<QString, QList<QString>> GetIndividualLogInfo();
    virtual std::vector<ruba::ChartStruct> GetChartStructs();
    virtual std::vector<std::vector<MaskOverlayStruct> > GetMasks();
    virtual void SetLastFrameReached();
    virtual qint64 GetConsecutiveEventDelay();
    void SetUseScrollingChart(bool useScrollingChart);
    void InitDetector();

	void setLogFileNamePostfix(int number);
	std::vector<std::pair<std::string, LogRole> > getLogFileNames();
	void closeAllLogs();

    void reset(bool resetLogs = true);

    QString GetConfigSaveFilePath();
    int SaveCurrentConfiguration(QString savePath = "");
    void LoadConfiguration(std::vector<ruba::FrameStruct> initFrame, QWidget *parent, QString configFile = "", 
        bool showGui = true, std::vector<ruba::DetectorOutput> dOutput = std::vector<ruba::DetectorOutput>());

protected:
    void updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput = std::vector<DetectorOutput>());
    void updateSubDetectors(const std::vector<FrameStruct>& newFrames);

    virtual void setDetectorSpecificEnabledStates();
    virtual void updateDetectorSpecificLogic() = 0;

    void EngraveIdInMasks();
    void UpdateChartProperties();    

    virtual void setDetectorSpecificDefaultParameters() = 0;
    virtual QList<QString> getDetectorSpecificDebugInfo();
    virtual QList<QString> getDetectorSpecificExtendedDebugInfo() = 0;

    std::vector<DetectorOutput> getDOutput();

    void setDetectorCount(uint nbrPDetectors, uint nbrMDetectors, uint nbrSDetectors,
        uint nbrTLDetectors);
    void setDetectorCount(std::vector<DType> detectors);
    void getDetectorCount(uint& nbrPDetectors, uint& nbrMDetectors, uint& nbrSDetectors,
        uint& nbrTLDetectors);

    void setGeneralLogSettings(const InitLogSettings& generalSettings);
    InitLogSettings getGeneralLogSettings() { return addSettings.generalLogSettings; }

    std::vector<ruba::DetectorInfo> getPreviousDetectorInfo() { return previousDetectorInfo; }

	std::vector<std::shared_ptr<ruba::AbstractMultiModalDetector> > detectors;

    std::vector<ruba::MaskHandlerOutputStruct> detectorParams;
    std::vector<ruba::DetectorInfo> detectorInfo;

	
	// Settings:
    QImage designImg;
    QIcon *windowIcon;

    // Engraved masks that are used for better visualization of the TrafficDetector
    std::map<int, std::vector<ruba::MaskOverlayStruct> > maskOverlays;
    std::map<int, std::vector<ruba::MaskOutlineStruct> > maskOutlines;


    // Internal flags and timestamps that helps the detector keep track of the flow
    QDateTime disableAllDetectorsAt;
    QDateTime previousTime;
    std::vector<bool> PrevIsTriggered;

    // Log environment for the compound detector
    std::shared_ptr<Logger> generalLog;

private:
    void createDetectors();
    void initialiseDetectorInfo();
    void setDefaultParameters(std::vector<DType> detectors);
    void createLogs();
    void updateIndividualLogs();
    void enforceMaxTriggeredDuration();

    ruba::AdditionalSettingsStruct addSettings;


    std::string stripFilename(std::string inputFilename);

    // Log environment for the individual detectors
    std::map<QString, std::shared_ptr<Logger> > individualLogs;

    DetectorSelectionDialog *sDialog;
    interactionselectiondialog *iDialog;

    // Previous detector output that might be needed for processing by this specific detector
    std::vector<DetectorOutput> dOutput;

    QString windowTitle;
    QString moduleTitle;

    std::vector<ruba::DetectorInfo> previousDetectorInfo;

};

#endif // ABSTRACTCOMPOUNDDETECTOR_H
