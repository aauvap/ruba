#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;
using namespace cv;
using namespace ruba;

MainWindow::MainWindow(int devState, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	windowInitialized = false;

    // Settings
    QCoreApplication::setOrganizationName("AAU");
    QCoreApplication::setOrganizationDomain("vap.aau.dk");
    QCoreApplication::setApplicationName("Road User Behaviour Analysis");

    QSettings settings;
    settings.beginGroup("general");
    QString style = settings.value("appearanceStyle", "").toString();

    if (!style.isEmpty())
    {
        QApplication::setStyle(style);
    }
    settings.endGroup();
	
	this->devState = devState;

	QString version(RUBA_VERSION);

    if (devState == RUBA_DEVSTATE)
    {
        QString build = QString(" (Build %1)")
            .arg(QLocale(QLocale::C).toDate(QString(__DATE__).simplified(), QLatin1String("MMM d yyyy")).toString("yyyy-MM-dd"));

        // Append the build number
		version += "-dev";
        version += build;
    }

    this->setWindowTitle("Road User Behaviour Analysis " + version);


    settings.beginGroup("dev");

    QSplashScreen splash;

    if (devState == RUBA_DEVSTATE) {
        // Developer edition. Set grey icon
        this->setWindowIcon(QIcon("://icons/RUBAnoText256dev.png"));
        splash.setPixmap(QPixmap("://icons/RUBAwithText400dev.png"));

        // Get the last developer edition opened. If the current is newer, 
        // show the change log
        int lastVersionMajor = settings.value("lastVersionMajor", 0).toInt();
        int lastVersionMinor = settings.value("lastVersionMinor", 0).toInt();
        int lastVersionRevision = settings.value("lastVersionRevision", 0).toInt();

        if (lastVersionMajor < RUBA_VERSION_MAJOR ||
            (lastVersionMajor == RUBA_VERSION_MAJOR && lastVersionMinor < RUBA_VERSION_MINOR) ||
            (lastVersionMajor == RUBA_VERSION_MAJOR && lastVersionMinor == RUBA_VERSION_MINOR && lastVersionRevision < RUBA_VERSION_REVISION)) {
            on_actionOpen_changelog_triggered();
        }

        settings.setValue("lastVersionMajor", RUBA_VERSION_MAJOR);
        settings.setValue("lastVersionMinor", RUBA_VERSION_MINOR);
        settings.setValue("lastVersionRevision", RUBA_VERSION_REVISION);
    }
    else {
        // Full release. Set colour icon
        this->setWindowIcon(QIcon("://icons/RUBAnoText256.png"));
        splash.setPixmap(QPixmap("://icons/RUBAwithText400.png"));
    }
    splash.show();
    splash.showMessage("Initializing...", Qt::AlignBottom + Qt::AlignCenter);
    settings.endGroup();

    // Initialize the videoHandler + DetectorHandler
	VHandler = make_shared<VideoHandler>(this, currentViews);
    std::shared_ptr<DetectorHandler> dHandler = make_shared<DetectorHandler>(this, currentViews);
    ui->detectorListView->setModel(dHandler->getDetectorListModel());
    ui->detectorListView->header()->setStretchLastSection(false);

    if (ui->detectorListView->header()->count() > 0) {
        ui->detectorListView->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    }

	int id = qRegisterMetaType<ruba::FrameStruct>();
	id = qRegisterMetaType<MessageType>();
	id = qRegisterMetaType<std::vector<ruba::FrameStruct>>();
	id = qRegisterMetaType<cv::Mat>();

	thread = make_shared<ProcessingThread>(VHandler, dHandler, this);
	connect(thread.get(), SIGNAL(sendFrame(cv::Mat)),
		this, SLOT(recieveFrame(cv::Mat)));
	connect(thread.get(), SIGNAL(sendTime(QDateTime, double)),
		this, SLOT(recieveTime(QDateTime, double)));
	connect(thread.get(), SIGNAL(renderedFrames(std::vector<ruba::FrameStruct>, double, QString)),
		this, SLOT(recieveFrames(std::vector<ruba::FrameStruct>, double, QString)));
	connect(thread.get(), SIGNAL(sendMessage(QString, QString, MessageType)),
		this, SLOT(recieveMessage(QString, QString, MessageType)));
	connect(thread.get(), SIGNAL(sendProgress(int, int, QString)),
		this, SLOT(recieveProgress(int, int, QString)));
	connect(this, SIGNAL(jumpToFrame(QDateTime)),
		thread.get(), SLOT(jumpToFrame(QDateTime)));
	

    // Initialize flags
    IsInitialized = false;
    ShowVideo = true;
	playbackToBeResumed = false;

    // Set the thermal modality enabled/disabled according to the settings
    settings.beginGroup("mainWindow");
    if (settings.value("enableRGB", 0).toBool())
    {
        ViewInfo info;
        info.modality = ImageModalities::RGB;
        info.viewId = 1;
        addView(info);
    }

    if (settings.value("enableThermal", 0).toBool())
    {   
        ViewInfo info;
        info.modality = ImageModalities::THERMAL;

        if (currentViews.empty())
        {
            info.viewId = 1;
        }
        else {
            info.viewId = 2;
        }

        addView(info);
    }

    settings.endGroup();

    // Fallback; If no modalities are enabled, enable the RGB modality
    if (currentViews.size() == 0)
    {
        ViewInfo info;
        info.modality = ImageModalities::RGB;
        info.viewId = 1;
        addView(info);
        ui->actionEnable_RGB_modality->setChecked(true);
    }

    // Set the slider position
    ui->videoControl_SpeedSlider->blockSignals(true);
    settings.beginGroup("video");
    int playbackFps = settings.value("playbackFps", 0).toInt();
	
	auto policy = ui->videoPlaybackFpsIcon->sizePolicy();
	policy.setRetainSizeWhenHidden(true);
	ui->videoPlaybackFpsIcon->setSizePolicy(policy);
	int baseHeight = ui->playbackControlsWaitBar->sizeHint().height();
	ui->videoPlaybackFpsIcon->setMinimumHeight(baseHeight);


	int delay = 0;

    if (playbackFps != 0)
    {
		delay = 1000 / playbackFps;
	}
	else {
		playbackFps = ui->videoControl_SpeedSlider->maximum(); // Set slider to max
	}

    ui->videoControl_SpeedSlider->setValue(playbackFps);
    ui->videoControl_SpeedSlider->blockSignals(false);
	thread->setTargetFps(playbackFps);

	// Initialize the timer
	DisplayTimer.setParent(this);
	DisplayTimer.setInterval(60); 
	connect(&DisplayTimer, SIGNAL(timeout()), this, SLOT(displayTimerTick()));
   

    // ****** Shortcuts ******
    // Set up delete Key on the listWidget
    QShortcut* s1 = new QShortcut(QKeySequence(Qt::Key_Delete), ui->detectorListView);
    connect(s1, SIGNAL(activated()), this, SLOT(deleteDetectorFromList()));

    QShortcut* s2 = new QShortcut(QKeySequence(Qt::Key_Space), this);
    connect(s2, SIGNAL(activated()), this, SLOT(on_StartPause_button_clicked()));

    QShortcut* s3 = new QShortcut(QKeySequence(Qt::Key_F11), this);
    connect(s3, SIGNAL(activated()), this, SLOT(on_actionReview_Log_File_triggered()));

	QShortcut* fiveSecPrev = new QShortcut(QKeySequence(Qt::Key_Q), this);
	connect(fiveSecPrev, SIGNAL(activated()), this, SLOT(on_videoControl_FiveSecPrev_clicked()));

	QShortcut* oneSecPrev = new QShortcut(QKeySequence(Qt::Key_W), this);
	connect(oneSecPrev, SIGNAL(activated()), this, SLOT(on_videoControl_OneSecPrev_clicked()));

	QShortcut* oneSecForw = new QShortcut(QKeySequence(Qt::Key_E), this);
	connect(oneSecForw, SIGNAL(activated()), this, SLOT(on_videoControl_OneSecForw_clicked()));

	QShortcut* fiveSecForw = new QShortcut(QKeySequence(Qt::Key_R), this);
	connect(fiveSecForw, SIGNAL(activated()), this, SLOT(on_videoControl_FiveSecForw_clicked()));

	
    this->ui->timeEdit->setDateTime(QDateTime::currentDateTime());
    this->ui->dateEdit->setDateTime(QDateTime::currentDateTime());	
     
    // Set up signal from the detector list view:
    connect(this->ui->detectorListView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(slotSelectionChanged(const QItemSelection &, const QItemSelection &)));
    setUpLayout();

    // Load the previous configuration, if possible
    splash.showMessage("Checking configuration...", Qt::AlignBottom + Qt::AlignCenter);
    loadPreviousConfiguration(splash);
    splash.finish(this);

    // Save default window settings
    writeSettings("default");

    // Read window settings
    readSettings("");
	windowInitialized = true;
	showWarningBeforePlayback = false;

	ui->playbackControlsWaitBar->hide();
	ui->videoPlaybackFpsLabel->hide();
	ui->videoPlaybackFpsIcon->hide();
}

MainWindow::~MainWindow()
{
    saveCurrentConfiguration();
    writeSettings("");

	cleanUpOnExit();
    delete ui;
    cv::destroyAllWindows();
}

void MainWindow::displayTimerTick()
{
	double fps = thread->getProcessingSpeed();
	std::vector<ruba::FrameStruct> frames;
	CaptureStatus status;
	
	bool success = thread->getCurrentFrames(frames, status);

	std::vector<ruba::ChartStruct> charts;
	bool debugWindowsSuccess = thread->getDetectorCharts(charts);

	if (success)
	{
		displayFrames(frames, fps);
		thread->unlockDisplayFrames();

		if (debugWindowsSuccess && !charts.empty())
		{
			for (auto& chart : charts)
			{ 
				cv::imshow(chart.Title.toStdString(), chart.Image);
			}
		}

		QApplication::processEvents();
	}

	if (status != CaptureStatus::OK)
	{
		pause();
		setUpLayout();
	}

	
}

void MainWindow::recieveTime(QDateTime time, double fps)
{
	this->ui->dateEdit->setDateTime(time);
	this->ui->timeEdit->setDateTime(time);

	if (fps != 0 && !this->ui->videoControl_SpeedSlider->isSliderDown())
	{
		QString playbackString = QString::number(fps, 'f', 0) + " frames/sec";
		this->ui->videoPlaybackFpsLabel->setText(playbackString);
	}

}

void MainWindow::recieveFrame(cv::Mat frame)
{
	if (!frame.empty())
	{
		viewers[0]->setImage(frame);
	}
}

void MainWindow::displayFrames(const std::vector<ruba::FrameStruct>& frames, double fps)
{
	if (ShowVideo)
	{
		for (int i = 0; i < viewers.size(); ++i)
		{
			if (i < frames.size())
			{
				viewers[i]->setImage(frames[i].Image);
			}
		}
	}

	if (!frames.empty())
	{
		this->ui->dateEdit->setDateTime(frames[0].Timestamp);
		this->ui->timeEdit->setDateTime(frames[0].Timestamp);
	}

	if (fps != 0 && !this->ui->videoControl_SpeedSlider->isSliderDown())
	{
		QString playbackString = QString::number(fps, 'f', 2) + " frames/sec";
		this->ui->videoPlaybackFpsLabel->show();
		this->ui->videoPlaybackFpsLabel->setText(playbackString);
		ui->detectorListView->repaint();
	}


}

void MainWindow::recieveMessage(const QString & title, const QString & message, MessageType messageType)
{
	
	if (windowInitialized)
	{
		switch (messageType)
		{
		case Information:
			QMessageBox::information(this, title, message);
			break;
		case Warning:
			QMessageBox::warning(this, title, message);
			break;
		case Critical:
			QMessageBox::critical(this, title, message);
			break;
		case Question:
			QMessageBox::question(this, title, tr("Not yet implemented"));
			break;
		case MessageBar:
			ui->statusBar->showMessage(message, 0);
		}
	}
}

void MainWindow::recieveProgress(int value, int maxValue, const QString & message)
{
	bool updateFromFrame = false;
	
	if (maxValue > 10)
	{
		if (ui->playbackControlsWaitBar->maximum() != maxValue)
		{
			ui->playbackControlsWaitBar->show();
			ui->playbackControlsWaitBar->setMaximum(maxValue);

			auto policy = ui->videoPlaybackFpsIcon->sizePolicy();
			policy.setRetainSizeWhenHidden(false);
			ui->videoPlaybackFpsIcon->setSizePolicy(policy);
		}

		ui->playbackControlsWaitBar->setValue(value);
		QApplication::processEvents();
		ui->statusBar->showMessage(message, 1000);

		if (maxValue - 1 <= value)
		{
			updateFromFrame = true;
		}
	}
	else {
		updateFromFrame = true;
	}

	if (updateFromFrame)
	{
		auto policy = ui->videoPlaybackFpsIcon->sizePolicy();
		policy.setRetainSizeWhenHidden(true);
		ui->videoPlaybackFpsIcon->setSizePolicy(policy);

		ui->playbackControlsWaitBar->hide();
		ui->playbackControlsWidget->setEnabled(true);

		// Grab the current frame
		auto vHandler = thread->getVideoHandler();

		if (vHandler != nullptr)
		{
			vector<FrameStruct> updateStruct = thread->getVideoHandler()->getCurrentFrame();
			displayFrames(updateStruct, 0);
		}		

		setVideoFiles();

		if (playbackToBeResumed)
		{
			play();
		}
	}
}

void MainWindow::maximizeSubWindows()
{
    if (subWindows.size() == 1)
    {
        subWindows[0]->showMaximized();
    }
    else {
        ui->mainArea->tileSubWindows();
    }
}

void MainWindow::on_actionReset_layout_triggered()
{
    readSettings("default");
}


void MainWindow::writeSettings(QString prefix)
{
    QSettings settings;
    settings.beginGroup("mainWindow-130");
    settings.setValue(prefix + "geometry", saveGeometry());
    settings.setValue(prefix + "windowState", saveState());
    settings.endGroup();
}

void MainWindow::readSettings(QString prefix)
{
    QSettings settings;
    settings.beginGroup("mainWindow-130");
	
	if (settings.contains(prefix + "geometry"))
	{
		restoreGeometry(settings.value(prefix + "geometry").toByteArray());
		restoreState(settings.value(prefix + "windowState").toByteArray());
	}
    settings.endGroup();
}

void MainWindow::saveConfiguration(QString filename)
{
    FileStorage fs;
    fs.open(filename.toStdString(), FileStorage::WRITE);

    fs << "rubaVersion" << std::vector<int>{RUBA_VERSION_MAJOR, RUBA_VERSION_MINOR, RUBA_VERSION_REVISION};

    string entryPrefix = "";
    // Save the current modalities

    RubaBaseObject::saveViewsToFile(fs, "MainWindow", currentViews);

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();
	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!dHandler || !vHandler)
	{
		return;
	}

	vHandler->saveToFile(fs, entryPrefix);
	dHandler->saveToFile(fs);
    fs.release();

}

void MainWindow::saveCurrentConfiguration()
{
    // Function that supports the saving of the current configuration to a .yml or similar file
    // such that next time, the user does not have to initialize the whole thing.
    
    QString configPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);

    if (!QDir(configPath).exists()) {
        QDir().mkpath(configPath);
    }

    QDir configDir(configPath);

    saveConfiguration(configDir.absoluteFilePath("prevConfiguration.yml"));
}

void MainWindow::loadConfiguration(QString filename, bool loadVideos, bool loadDetectors)
{
    QSplashScreen splash;
    loadConfiguration(filename, loadVideos, loadDetectors, splash, false);

}

void MainWindow::loadConfiguration(QString filename, bool loadVideos, bool loadDetectors,
    QSplashScreen &splash, bool showSplash)
{
    QProgressDialog progress("Loading configuration...", "Cancel", 0, 100, this);

    if (showSplash)
    {
        progress.reset();
    }
    else {
        progress.setWindowModality(Qt::WindowModal);
        progress.show();
    }

    try {
        FileStorage fs;
        fs.open(filename.toStdString(), FileStorage::READ);

        if (!showSplash && progress.wasCanceled())
        {
            return;
        }

        if (fs.isOpened())
        {
            // Check version
            vector<int> savedRubaVersion;
            fs["rubaVersion"] >> savedRubaVersion;

            bool oldVersion = false;

            if (savedRubaVersion.empty()) {
                oldVersion = true;
            }
            else if (savedRubaVersion.size() > 2 &&
                ((savedRubaVersion[0] == 1 && savedRubaVersion[1] < 2) ||
                    savedRubaVersion[0] < 1))
            {
                oldVersion = true;

            }

            if (oldVersion)
            {
                if (!showSplash)
                {
                    QMessageBox::information(this, tr("Old configuration file"),
                        tr("The configuration file was created using an older version of RUBA and is no longer supported."),
                        QMessageBox::Ok);
                }
                fs.release();
                return;
            }

            // Check if the configuration file is actually a detector
            int nDetectorType = fs["detectorType"];

            if (nDetectorType > 0)
            {
                // We are trying to load a detector. Inform the user
                QMessageBox::information(this, tr("Detector file was found"),
                    tr("The file is not a configuration file, but a detector file containing a %1.\nLoad the detector in \"Detectors -> Load detectors\" instead.")
                    .arg(Utils::getDetectorName(nDetectorType)),
                    QMessageBox::Ok);
                
                fs.release();
                return;
            }


            // Load views
            QList<ViewInfo> newViews = RubaBaseObject::getViewsFromFileStorage(fs,
                "MainWindow");

            // Disable the current views and enable new views
            QList<ViewInfo> tmpCurrentViews = currentViews;

            for (auto i = 0; i < tmpCurrentViews.size(); ++i)
            {
                removeView(tmpCurrentViews[i]);
            }

            for (auto j = 0; j < newViews.size(); ++j)
            {
                addView(newViews[j]);
            }

            if (loadVideos)
            {
                thread->getVideoHandler()->loadFromFile(fs );
                setVideoFiles();
            }

            if (loadDetectors)
            {
				std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();
				std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

				if (!dHandler || !vHandler)
				{
					return;
				}
				
				vector<FrameStruct> frames = vHandler->getCurrentFrame();
                dHandler->loadFromFile(fs, frames, progress, splash, showSplash);
            }
        }
        fs.release();
    }
    catch (cv::Exception e)
    {
        // Could not load configuration file.

        if (!showSplash) {
            QMessageBox::information(this, QObject::tr("Failed to load file"), QObject::tr("Failed to load configuration file. The following error occurred: \n") +
                QObject::tr("File: ") + e.file.c_str() + "\n" +
                QObject::tr("Error code: ") + e.code + "\n" +
                QObject::tr("Message: ") + e.what(),
                QMessageBox::Ok);

        }
    }


    setUpLayout();
}


void MainWindow::loadPreviousConfiguration(QSplashScreen &splash)
{
    //// Function that supports the loading of the previous configuration to a .yml or similar file
    QSettings settings;
    settings.beginGroup("general");

    bool loadPrevious = settings.value("restorePrevConfig", 1).toBool();
    bool loadVideos = settings.value("restorePrevConfig-includeVideos", 1).toBool();
    bool loadDetectors = settings.value("restorePrevConfig-includeDetectors", 0).toBool();

    if (loadPrevious)
    {
        QString configPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
        QDir configDir(configPath);

        if (QFile(configDir.absoluteFilePath("prevConfiguration.yml")).exists()) {
            loadConfiguration(configDir.absoluteFilePath("prevConfiguration.yml"), loadVideos, loadDetectors, splash, true);
        }
    }
}

void MainWindow::on_actionSave_configuration_triggered()
{
    QSettings settings;
    settings.beginGroup("mainWindow");
    QString configurationDir = settings.value("configurationDir").toString();

    QString filename = QFileDialog::getSaveFileName(this, tr("Save configuration"),
        configurationDir, tr("Configuration files (*.yml)"));
    
    if (filename.isEmpty())
    {
        return;
    }

    saveConfiguration(filename);

    // Update the path in the settings
    QFileInfo file(filename);    
    settings.setValue("configurationDir", file.path());
    settings.endGroup();

}

void MainWindow::on_actionLoad_configuration_triggered()
{
    QSettings settings;
    settings.beginGroup("mainWindow");
    QString configurationDir = settings.value("configurationDir").toString();
    settings.endGroup();

    QString filename = QFileDialog::getOpenFileName(this, tr("Load configuration"),
        configurationDir, tr("Configuration files (*.yml)"));

    if (filename.isEmpty())
    {
        return;
    }

    loadConfiguration(filename, true, true);

    // Update the path in the settings
    QFileInfo file(filename);
    settings.setValue("configurationDir", file.path());
    settings.endGroup();
}

void MainWindow::on_actionLoad_detector_triggered()
{
	bool isRunning = thread->isRunning();
	
	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();
	if (!dHandler)
	{
		return;
	}

	dHandler->loadDetector(thread->getVideoHandler()->getCurrentFrame());
    setUpLayout();

	if (isRunning)
	{
		thread->startProcessing();
	}
}

int MainWindow::addView(ViewInfo newView)
{
    int foundAtIndex = RubaBaseObject::findViewInList(newView, currentViews);

    // If found, the view has already been added. Return
    if (foundAtIndex >= 0) {
        return 1;
    }

    // Otherwise, add the view
    currentViews.append(newView);

	bool isRunning = thread->isRunning();


	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();
	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!dHandler || !vHandler)
	{
		return 1;
	}

	dHandler->addView(newView);
	vHandler->addView(newView);

    // Create a new SubWindow for the modality
    QMdiSubWindow* subwindow = new QMdiSubWindow(this);
    subwindow->setAttribute(Qt::WA_DeleteOnClose);
    subwindow->setWindowTitle(tr("View %1").arg(newView.viewName()));
    subwindow->setWindowIcon(QIcon("://icons/" + Utils::GetModalityName(newView.modality) + "-icon-small.png"));

    OpenCVViewer* viewer = new OpenCVViewer(this, newView);

    // Connect the closing signal of the OpenCVViewer
    connect(viewer, SIGNAL(widgetClosing(ImageModalities*)), this, SLOT(subwindowClosingHandler(ImageModalities*)));

    viewer->setImage(cv::Mat());
    viewers.push_back(viewer);
    subwindow->setWidget(viewer);
    ui->mainArea->addSubWindow(subwindow);
    subWindows.append(subwindow);
    subwindow->show();

    if (currentViews.size() == 1 && subWindows.size() == 1)
    {
        subWindows[0]->showMaximized();
    }
    else {
        ui->mainArea->tileSubWindows();
    }

    // Set the menu checked/unchecked
    if (newView.modality == RGB)
    {
        ui->actionEnable_RGB_modality->blockSignals(true);
        ui->actionEnable_RGB_modality->setChecked(true);
        ui->actionEnable_RGB_modality->blockSignals(false);
    }
    else if (newView.modality == THERMAL)
    {
        ui->actionEnable_thermal_modality->blockSignals(true);
        ui->actionEnable_thermal_modality->setChecked(true);
        ui->actionEnable_thermal_modality->blockSignals(false);
    }

    setUpLayout();

    QSettings settings;
    settings.beginGroup("mainWindow");
    QString settingName = "enable" + Utils::GetModalityName(newView.modality);
    settings.setValue(settingName, true);
    settings.endGroup();

	if (isRunning)
	{
		thread->startProcessing();
	}

    return 0;
}

int MainWindow::removeView(ViewInfo view)
{
    int foundAtIndex = RubaBaseObject::findViewInList(view, currentViews);

    // If found, remove the view
    if (foundAtIndex >= 0) {
        currentViews.removeAt(foundAtIndex);
    }

	bool isRunning = thread->isRunning();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();
	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!dHandler || !vHandler)
	{
		return 1;
	}

	dHandler->removeView(view);
	vHandler->removeView(view);


    // Disable the modality from the settings panel
    if (view.modality == RGB)
    {
        ui->actionEnable_RGB_modality->blockSignals(true);
        ui->actionEnable_RGB_modality->setChecked(false);
        ui->actionEnable_RGB_modality->blockSignals(false);
    }
    else if (view.modality == THERMAL) {
        ui->actionEnable_thermal_modality->blockSignals(true);
        ui->actionEnable_thermal_modality->setChecked(false);
        ui->actionEnable_thermal_modality->blockSignals(false);
    }

    // Delete the corresponding viewer
    delete viewers.takeAt(foundAtIndex);

    // Remove the sub window from the mainArea but don't delete it - 
    // this should be taken care of by the WA_DeleteOnClose property
    ui->mainArea->removeSubWindow(subWindows[foundAtIndex]);
    subWindows.takeAt(foundAtIndex);

    if (currentViews.size() == 1 && subWindows.size() == 1)
    {
        subWindows[0]->showMaximized();
    }
    else {
        ui->mainArea->tileSubWindows();
    }

    setUpLayout();

    // Set the menu checked/unchecked
    if (view.modality == RGB)
    {
        ui->actionEnable_RGB_modality->blockSignals(true);
        ui->actionEnable_RGB_modality->setChecked(false);
        ui->actionEnable_RGB_modality->blockSignals(false);
    }
    else if (view.modality == THERMAL)
    {
        ui->actionEnable_thermal_modality->blockSignals(true);
        ui->actionEnable_thermal_modality->setChecked(false);
        ui->actionEnable_thermal_modality->blockSignals(false);
    }

    QSettings settings;
    settings.beginGroup("mainWindow");
    QString settingName = "enable" + Utils::GetModalityName(view.modality);
    settings.setValue(settingName, false);
    settings.endGroup();

	if (isRunning)
	{
		thread->startProcessing();
	}

    return 0;
    
}

void MainWindow::setUpLayout()
{
	bool isRunning = thread->isRunning();
	ui->playbackControlsWaitBar->hide();

	if (VHandler)
	{
		IsInitialized = VHandler->getIsInitialised();
	}    

    this->ui->actionLoad_video_files->setEnabled(true);
	this->ui->actionLoad_video_stream->setEnabled(true);

    // Set up buttons according to the state of the isInitialized flag
    this->ui->actionSave_video_to_file->setEnabled(IsInitialized);
    ui->actionTake_screenshot->setEnabled(IsInitialized);

    QModelIndex index;
    showActiveDetectorButtons(IsInitialized, index);


    // Detectors
	this->ui->actionLoad_detector->setEnabled(IsInitialized);
    this->ui->actionLeft_Turning_Car->setEnabled(IsInitialized);
    this->ui->actionRight_Turning_Car->setEnabled(IsInitialized);
    this->ui->actionStraight_Going_Cyclist->setEnabled(IsInitialized);
    //this->ui->actionInteraction->setEnabled(IsInitialized);
    this->ui->actionSingle_Module->setEnabled(IsInitialized);
    this->ui->actionDouble_Module->setEnabled(IsInitialized);
	this->ui->actionExclusive_Module->setEnabled(IsInitialized);
	this->ui->actionAnnotate_Ground_Truth->setEnabled(IsInitialized);
	this->ui->actionReview_Log_File->setEnabled(IsInitialized);
	this->ui->actionPerform_Multi_Threaded_Processing->setEnabled(IsInitialized);

    // Playback buttons
    this->ui->playPauseGroupBox->setEnabled(IsInitialized);
    this->ui->jumpToFrameGroupBox->setEnabled(IsInitialized);

    if (!IsInitialized)
    {
        this->ui->videoPlaybackFpsIcon->hide();
        ui->videoPlaybackFpsLabel->setText("");
    }

    // The videoControl are only enabled whenever we are initialized and the video is not playing
    bool enabledState = false;

    if (IsInitialized && !isRunning)
    {
        enabledState = true;
    }
    
    ui->videoControlFramesGroupBox->setEnabled(enabledState);
    ui->videoControlSecondsGroupBox->setEnabled(enabledState);

    // Debug overlay buttons
    this->ui->actionOverlay_timestamp->setEnabled(IsInitialized);
    this->ui->actionShow_all_debug_windows->setEnabled(IsInitialized);
    this->ui->actionOverlay_debug_information->setEnabled(IsInitialized);
    this->ui->actionOverlay_ext_debug_information->setEnabled(IsInitialized);
    this->ui->actionOverlay_masks->setEnabled(IsInitialized);

    // Enable the delete button if there are videos in the listView

    if (ui->videoListView->currentIndex().row() >= 0)
    {
        this->ui->deleteVideosFromListButton->setEnabled(true);
    }
    else {
        this->ui->deleteVideosFromListButton->setEnabled(false);
    }

    this->ui->moveVideoDownButton->setEnabled(IsInitialized);
    this->ui->moveVideoUpButton->setEnabled(IsInitialized);
    ui->showVideoPropertiesButton->setEnabled(IsInitialized);

    // Window Layout
    ui->actionAdjust_window_size_to_video->setEnabled(IsInitialized);

    // Layout of videoListView
    this->ui->videoListView->header()->setStretchLastSection(false);

    if (ui->videoListView->header()->count() > 1) {
        ui->videoListView->header()->setSectionResizeMode(0, QHeaderView::Stretch);

        for (auto i = 1; i < ui->videoListView->header()->count(); ++i) {
            ui->videoListView->header()->setSectionResizeMode(i, QHeaderView::Fixed);
        }
    }

    // Layout of detectorListView
    if (ui->detectorListView->header()->count() > 1) {
        ui->detectorListView->header()->setSectionResizeMode(0, QHeaderView::Stretch);

        for (auto i = 1; i < ui->detectorListView->header()->count(); ++i) {
            ui->detectorListView->header()->setSectionResizeMode(i, QHeaderView::Fixed);
        }
    }
}

void MainWindow::setVideoFiles()
{
	bool isRunning = thread->isRunning();

	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!vHandler)
	{
		return;
	}

    this->ui->videoListView->setModel(vHandler->getVideoListModel());
	//vHandler->pause();
	vector<FrameStruct> updateStructs;
	CaptureStatus status;
		
	bool success = thread->getCurrentFrames(updateStructs, status);

	if (!success)
	{
		return;
	}

	displayFrames(updateStructs, 0);
	thread->unlockDisplayFrames();

    int index = vHandler->getCurrentFileIndex();
    QModelIndex idx = ui->videoListView->model()->index(index, 0);
    ui->videoListView->setCurrentIndex(idx); 

    IsInitialized = vHandler->getIsInitialised();
    setUpLayout();

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_actionLoad_video_files_triggered()
{
    bool isRunning = thread->isRunning();

    QApplication::processEvents();

    QSettings settings;
    settings.beginGroup("video");
    QString videoHomeDir = settings.value("videoHomeDir", "").toString();
	settings.endGroup();

	// Get the current VideoSyncType
	settings.beginGroup("synchronisation");
	VideoSyncType videoSyncType = (VideoSyncType)settings.value("videoSyncType", 0).toInt();
	settings.endGroup();


    videoHomeDir = thread->getVideoHandler()->setVideoFiles(videoHomeDir, videoSyncType);
    setVideoFiles();
    
    settings.setValue("videoHomeDir", videoHomeDir);
    settings.endGroup();


	if (isRunning)
	{
		thread->startProcessing();
	}

	ui->addVideosToListButton->setEnabled(true);
}

void MainWindow::on_actionLoad_video_stream_triggered()
{
	bool isRunning = thread->isRunning();

	QApplication::processEvents();

	QSettings settings;
	settings.beginGroup("video");
	QString streamLastPath = settings.value("streamLastPath", "").toString();
	settings.endGroup();

	QString videoHomeDir = thread->getVideoHandler()->setVideoFiles(streamLastPath, CAPTUREFROMSTREAM);

	if (!videoHomeDir.isEmpty())
	{
		setVideoFiles();

		settings.setValue("streamLastPath", videoHomeDir);
		settings.endGroup();


		setMaximumVideoSpeed(thread->getVideoHandler()->getFrameRate() * 1.2);
		play();
	}
	else {
		if (isRunning)
		{
			play();
		}
	}

	ui->addVideosToListButton->setEnabled(false);
}

void MainWindow::on_StartPause_button_clicked()
{
    if (IsInitialized)
    {
        if (thread->isRunning())
        {
			pause();
        } else {
			play();
        }
    }
}


void MainWindow::on_detectorListView_doubleClicked(const QModelIndex &index)
{
    on_ReconfigureDetectorButton_clicked();
}


void MainWindow::closeEvent(QCloseEvent *event)
{
	thread->quit();
    cv::destroyAllWindows();

}

void MainWindow::on_actionSave_video_to_file_triggered()
{
	RecordingState recordingState = thread->getRecordingState();

	if (recordingState != NOTINITIALISED)
	{
		if (recordingState == RECORDING)
		{
			// We want to pause the current recording
			thread->pauseRecording();
			this->ui->actionSave_video_to_file->setIcon(QIcon("://icons/control-record.png"));
			this->ui->actionSave_video_to_file->setChecked(false);
			this->ui->actionSave_video_to_file->setText("Resume recording");
			this->ui->actionSave_video_to_file->setToolTip("Resume recording");

			ui->statusBar->showMessage("Recording paused", 20000); // Message shown for 20 seconds
		}
		else {
			// We want to resume the current recording
			thread->resumeRecording();
			this->ui->actionSave_video_to_file->setIcon(QIcon("://icons/control-pause-record.png"));
			this->ui->actionSave_video_to_file->setChecked(true);
			this->ui->actionSave_video_to_file->setText("Pause recording");
			this->ui->actionSave_video_to_file->setToolTip("Pause recording");

			ui->statusBar->showMessage(recordingMessage, 0);
			this->ui->actionStop_recording->setEnabled(true);
		}
	}
	else {
		QSettings settings;
		settings.beginGroup("mainWindow");
		QString videoRecordDir = settings.value("videoRecordDir", "").toString();

		QString videoPath = QFileDialog::getExistingDirectory(this, QObject::tr("Select video recording directory"), videoRecordDir);

		if (!videoPath.isEmpty())
		{
			settings.setValue("videoRecordDir", videoPath);
			
		}
		settings.endGroup();

		
		thread->startRecording(videoPath);

		recordingMessage = "Initialized recording";
		ui->statusBar->showMessage(recordingMessage, 0);

		this->ui->actionSave_video_to_file->setIcon(QIcon("://icons/control-pause-record.png"));
		this->ui->actionSave_video_to_file->setText("Pause recording");
		this->ui->actionSave_video_to_file->setToolTip("Pause recording");
		this->ui->actionSave_video_to_file->setChecked(true);
		this->ui->actionStop_recording->setEnabled(true);

	}
}

void MainWindow::on_actionStop_recording_triggered()
{
	pause();
	thread->stopRecording();

    this->ui->actionStop_recording->setEnabled(false);
    this->ui->actionSave_video_to_file->setIcon(QIcon("://icons/control-record.png"));
    this->ui->actionSave_video_to_file->setChecked(false);

    this->ui->actionSave_video_to_file->setText("Record video to file");
    this->ui->actionSave_video_to_file->setToolTip("Record video to file");

    ui->statusBar->showMessage("Stopped recording", 12000); // Show message for 12 seconds

}

void MainWindow::on_actionTake_screenshot_triggered()
{
	vector<FrameStruct> frames;
	CaptureStatus status;
	bool success = thread->getCurrentFrames(frames, status);

	if (!success)
	{
		thread->unlockDisplayFrames();
		return;
	}
	
    // Save the current frame on a proper location
    // Right now, the "proper location" will be where the video is located
    if (screenshotPath.isEmpty())
    {
        QSettings settings;
        settings.beginGroup("mainWindow");
        QString screenshotDir = settings.value("screenshotDir", "").toString();


        QString tmpPath = QFileDialog::getExistingDirectory(this, tr("Select screenshot directory"), screenshotDir);

        if (tmpPath.isEmpty())
        {
			thread->unlockDisplayFrames();
			return;
        }
        else {
            screenshotPath = tmpPath;

            // Find the absolute path of the file name to update the screenshotDir
            QFileInfo info(screenshotPath);
            screenshotDir = info.absolutePath();
            settings.setValue("screenshotDir", screenshotDir);

        }

        settings.endGroup();
    }

    QString filenames;

    for (size_t i = 0; i < frames.size(); ++i)
    {
        QString viewName = tr("View%1").arg(frames[i].viewInfo.viewId);
        QString savePath = screenshotPath + "/";
        QString filename = frames[i].Timestamp.toString("yyyy-MM-dd-HH-mm-ss.zzz") + "-" +
            viewName + ".png";

        if (i > 0)
        {
            filenames.append(", ");
        }
        filenames = filenames + "\"" + filename + "\"";

        savePath.append(filename);
        cv::imwrite(savePath.toStdString(), frames[i].Image);
    }

	thread->unlockDisplayFrames();

	QString plural = frames.size() == 1 ? "" : "s";

    QString statusMessage = tr("Screenshot") + plural + " " + filenames + tr(" saved in ") + screenshotPath;
    ui->statusBar->showMessage(statusMessage, 20000); // Message shown for 20 seconds
}



void MainWindow::cleanUpOnExit()
{
	thread->quit();
	thread->releaseVideo();
}

void MainWindow::on_actionSingle_Module_triggered()
{    
   createDetector(SINGLEMODULE);
}

void MainWindow::on_actionDouble_Module_triggered()
{
    createDetector(DOUBLEMODULE);
}

void MainWindow::on_actionExclusive_Module_triggered()
{
	createDetector(EXCLUSIVEMODULE);
}

void MainWindow::on_actionRight_Turning_Car_triggered()
{
    createDetector(RIGHTTURNINGCAR);
}

void MainWindow::on_actionLeft_Turning_Car_triggered()
{
    createDetector(LEFTTURNINGCAR);
}

void MainWindow::on_actionStraight_Going_Cyclist_triggered()
{
    createDetector(STRAIGHTGOINGCYCLIST);
}

void MainWindow::on_actionInteraction_triggered()
{
    createDetector(INTERACTIONDETECTOR);
}

void MainWindow::on_actionAnnotate_Ground_Truth_triggered()
{
    createDetector(GROUNDTRUTHANNOTATOR);
}

void MainWindow::on_actionReview_Log_File_triggered()
{	
	createDetector(LOGFILEREVIEWER);
}

void MainWindow::on_actionPerform_Multi_Threaded_Processing_triggered()
{
	vector<FrameStruct> updateStructs;
	CaptureStatus status;
	auto vHandler = thread->getVideoHandler();

	if (!vHandler)
	{
		return;
	}

	int currentFileIndex = vHandler->getCurrentFileIndex();

	bool success = thread->getCurrentFrames(updateStructs, status);

	if (!success)
	{
		thread->unlockDisplayFrames();
		return;
	}

	pause();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();
	
	if (dHandler && vHandler)
	{
		// Disable message handling for the main window while we perform multi-threaded analysis
		windowInitialized = false;

		multiThreadedProcessingDialog = std::make_shared<MultiThreadedProcessingDialog>(this, currentViews,
			updateStructs, vHandler);

		std::vector<std::shared_ptr<AbstractCompoundDetector> > detectors;
		dHandler->getDetectors(detectors);
		multiThreadedProcessingDialog->setDetectors(detectors);

		std::vector<std::shared_ptr<AbstractSynchronisedVideoCapture> > videos;
		vHandler->getVideoFiles(videos);
		multiThreadedProcessingDialog->setVideoFiles(videos);

		multiThreadedProcessingDialog->exec();

		// Finished processing - reset and roll back video
		if (multiThreadedProcessingDialog->getStartedStatus())
		{
			vHandler->setCurrentVideo(currentFileIndex);
			setVideoFiles();
			showWarningBeforePlayback = true;
		}
		
		multiThreadedProcessingDialog.reset();
		
		// Then set the message handling to default
		windowInitialized = true;
	}

	thread->unlockDisplayFrames();

}


void MainWindow::createDetector(int detectorType)
{
	bool isRunning = thread->isRunning();
	thread->pause();
	thread->quit();
    

	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!vHandler)
	{
		return;
	}

    vector<FrameStruct> frames = vHandler->getCurrentFrame();
	bool noFrames = false;

	if (frames.empty())
	{
		noFrames = true;
	}

    if (!frames.empty()  && frames[0].Image.empty())
    {
        frames = vHandler->getNextFrame();

		if (frames.empty() || (!frames.empty() && frames[0].Image.empty()))
		{
			noFrames = true;
		}
    }

	if (noFrames)
	{
		QMessageBox::information(this, tr("No video file"),
			tr("Cannot create detector without a corresponding video frame. Please load one or "
				"more videos and retry."));
		return;
	}

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();
	if (!dHandler)
	{
		return;
	}
	
	dHandler->createDetector(detectorType, frames);

    ui->detectorListView->setModel(dHandler->getDetectorListModel());

    if (ui->detectorListView->header()->count() > 0) {
        ui->detectorListView->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    }

	setUpLayout();

    if (isRunning)
    {
        thread->startProcessing();
    }

    
}

void MainWindow::on_JumpToFrameButton_clicked()
{
    // Disable the playback controls as long as we are jumping to the frame
    ui->playbackControlsWidget->setEnabled(false);

	playbackToBeResumed = thread->isRunning();

    if (playbackToBeResumed)
    {
        pause();
    }

    // Call the VideoHandlers jumpToFrame with no parameters, thus invoking the GUI
	QDateTime time = ui->timeEdit->dateTime();
	
	jumptoframedialog d(this, time);

	if (d.exec())
	{
		// If Ok button was pressed, jump to the requested dateTime
		emit jumpToFrame(d.GetOutput());
	}
	else {
		ui->playbackControlsWidget->setEnabled(true);
	}
}

void MainWindow::jumpToFrameAtTime(QDateTime requestedTime)
{
    // Disable the playback controls as long as we are jumping to the frame
    ui->playbackControlsWidget->setEnabled(false);

	playbackToBeResumed = thread->isRunning();
	if (playbackToBeResumed)
	{
		pause();
	}

	emit jumpToFrame(requestedTime);
}


void MainWindow::on_ReconfigureDetectorButton_clicked()
{
    bool isRunning = thread->isRunning();

    if (IsInitialized)
    {
		std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();
		std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

		if (!dHandler || !vHandler)
		{
			return;
		}

        // We want to have index of the oldest ancestor (detector) - not the children (detectorInfo)
        QModelIndex ancestor = Utils::getAncestor(ui->detectorListView->currentIndex());
        if (ancestor.column() == 0 && ancestor.row() >= 0)
        {
            // If the first column is selected, reconfigure the detector
			dHandler->reconfigureDetector(ancestor.row(), vHandler->getCurrentFrame());
        }
        else if (ancestor.row() >= 0) {
            // Otherwise, show eventual error messages
			dHandler->checkDetectorDiagnostics(ancestor.row(), true);
        }
    }
    else {
        QMessageBox::information(this, tr("Incomplete views"), tr("Not all views have a corresponding video. Please specify video files for all views and retry"), QMessageBox::Ok);
    }


    if (isRunning)
    {
		thread->startProcessing();
    }
}

void MainWindow::showActiveDetectorButtons(bool showDetectors, QModelIndex index)
{
	bool isRunning = thread->isRunning();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (!dHandler)
	{
		return;
	}

    DebugPrefStruct debugPref = dHandler->getDebugPref();
	int detectorCount = dHandler->getDetectorCount();

    if (showDetectors && detectorCount > 0)
    {
        this->ui->ReconfigureDetectorButton->setEnabled(true);
        this->ui->DeleteDetectorButton->setEnabled(true);
        this->ui->ResetDetectorButton->setEnabled(true);
        this->ui->ShowCurrentDebugWindowsButton->setEnabled(true);
        this->ui->OverlayCurrentMasksButton->setEnabled(true);
        this->ui->OverlayCurrentDebugInfoButton->setEnabled(true);
        this->ui->OverlayCurrentExtDebugInfoButton->setEnabled(true);
    } else if (!showDetectors)
    {
        this->ui->ReconfigureDetectorButton->setEnabled(false);

        this->ui->ShowCurrentDebugWindowsButton->setEnabled(false);
        this->ui->OverlayCurrentMasksButton->setEnabled(false);
        this->ui->OverlayCurrentDebugInfoButton->setEnabled(false);
        this->ui->OverlayCurrentExtDebugInfoButton->setEnabled(false);
        this->ui->ResetDetectorButton->setEnabled(false);


        if (detectorCount > 0)
        {
            // If detector is selected, allow the detector to be deleted
            this->ui->DeleteDetectorButton->setEnabled(true);
        } else {
            this->ui->DeleteDetectorButton->setEnabled(false);
        }
    }

    int row = Utils::getAncestor(index).row();

    
    if (showDetectors == true && row >= 0)
    {
        if (debugPref.ShowDebugWindows.size() > row)
        {
            this->ui->ShowCurrentDebugWindowsButton->setChecked(debugPref.ShowDebugWindows[row]);
        }

        if (debugPref.OverlayMasks.size() > row)
        {
            this->ui->OverlayCurrentMasksButton->setChecked(debugPref.OverlayMasks[row]);
        }

        if (debugPref.OverlayDebugInfo.size() > row)
        {
            this->ui->OverlayCurrentDebugInfoButton->setChecked(debugPref.OverlayDebugInfo[row]);
        }

        if (debugPref.OverlayExtDebugInfo.size() > row)
        {
            this->ui->OverlayCurrentExtDebugInfoButton->setChecked(debugPref.OverlayExtDebugInfo[row]);
        }
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}


void MainWindow::on_DeleteDetectorButton_clicked()
{
    // Show message box asking the user if the object should be deleted
    int ret = QMessageBox::question(this, tr("Confirm deletion"), tr("Do you want to delete the detector?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);

    if (ret == QMessageBox::Yes)
    {
        deleteDetectorFromList();
    }
}

void MainWindow::deleteDetectorFromList()
{
	bool isRunning = thread->isRunning();

    int currentRow = Utils::getAncestor(ui->detectorListView->currentIndex()).row();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->deleteDetectorFromList(currentRow);
	}

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_ResetDetectorButton_clicked()
{
	bool isRunning = thread->isRunning();
	
	// Show message box asking the user if the object should be deleted
    int ret = QMessageBox::question(this, tr("Confirm reset"), 
        tr("Do you want to reset this detector? \n\n"
            "All log files that are created by this detector will be overwritten, "
            "and all stored event frames from this detector will be deleted. Proceed?"), 
            QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);

	if (ret == QMessageBox::Yes)
	{
		int currentRow = Utils::getAncestor(ui->detectorListView->currentIndex()).row();
		std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

		if (dHandler)
		{
			dHandler->resetDetector(currentRow);
		}
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::play()
{
	if (showWarningBeforePlayback)
	{
		int status = QMessageBox::question(this, tr("Playback might delete log files"),
			tr("You have recently finished an analysis using multi-threaded processing. If you resume playback, you might risk resetting your log files. \nAre you sure that you wat to resume playback?"));
		
		if (status == QMessageBox::No)
		{
			return;
		}
		else {
			showWarningBeforePlayback = false;
		}
	}


    this->ui->StartPause_button->setIcon(QIcon("://icons/control-pause.png"));

    ui->videoPlaybackFpsIcon->show();
	ui->videoPlaybackFpsLabel->show();


	DisplayTimer.start();

	if (!thread->isRunning())
	{
		VHandler->play();
		thread->startProcessing();
	}

	setUpLayout();

}

void MainWindow::pause()
{
    this->ui->StartPause_button->setIcon(QIcon("://icons/control.png"));
	DisplayTimer.stop();

	while(thread->isRunning())
	{
		thread->pause();
		thread->quit();
	}

	ui->videoPlaybackFpsLabel->setText("");
	ui->videoPlaybackFpsLabel->hide();
	ui->videoPlaybackFpsIcon->hide();

	VHandler->pause();
	setUpLayout();
}


void MainWindow::on_ShowCurrentDebugWindowsButton_clicked(bool checked)
{
	bool isRunning = thread->isRunning();

	int index = Utils::getAncestor(ui->detectorListView->currentIndex()).row();
    
	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setShowDebugWindows(index, checked);
	}

    if (!checked)
    {
        this->ui->actionShow_all_debug_windows->blockSignals(true);
        this->ui->actionShow_all_debug_windows->setChecked(false);
        this->ui->actionShow_all_debug_windows->blockSignals(false);
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_OverlayCurrentMasksButton_clicked(bool checked)
{
	bool isRunning = thread->isRunning();
    int index = Utils::getAncestor(ui->detectorListView->currentIndex()).row();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setOverlayMasks(index, checked);
	}

    if (!checked && dHandler)
    {
        this->ui->actionOverlay_masks->blockSignals(true);
        this->ui->actionOverlay_masks->setChecked(false);
        this->ui->actionOverlay_masks->blockSignals(false);
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_OverlayCurrentDebugInfoButton_clicked(bool checked)
{
    int index = Utils::getAncestor(ui->detectorListView->currentIndex()).row();
	bool isRunning = thread->isRunning();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setOverlayDebugInformation(index, checked);
	}

    if (!checked)
    {
        this->ui->actionOverlay_debug_information->blockSignals(true);
        this->ui->actionOverlay_debug_information->setChecked(false);
        this->ui->actionOverlay_debug_information->blockSignals(false);
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_OverlayCurrentExtDebugInfoButton_clicked(bool checked)
{
    int index = Utils::getAncestor(ui->detectorListView->currentIndex()).row();
	bool isRunning = thread->isRunning();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setOverlayExtDebugInformation(index, checked);
	}

    if (!checked)
    {
        this->ui->actionOverlay_ext_debug_information->blockSignals(true);
        this->ui->actionOverlay_ext_debug_information->setChecked(false);
        this->ui->actionOverlay_ext_debug_information->blockSignals(false);
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_actionOverlay_timestamp_triggered(bool checked)
{
	bool isRunning = thread->isRunning();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setOverlayTimestamp(checked);
	}

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_actionOverlay_debug_information_triggered(bool checked)
{
	bool isRunning = thread->isRunning();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setOverlayDebugInformation(-1, checked);
	}

    // Change button states at the list view
    if (this->ui->ReconfigureDetectorButton->isEnabled())
    {
        showActiveDetectorButtons(true, this->ui->detectorListView->currentIndex());
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_actionOverlay_ext_debug_information_triggered(bool checked)
{
	bool isRunning = thread->isRunning();
	
	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setOverlayExtDebugInformation(-1, checked);
	}

    // Change button states at the list view
    if (this->ui->ReconfigureDetectorButton->isEnabled())
    {
        showActiveDetectorButtons(true, this->ui->detectorListView->currentIndex());
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_actionOverlay_masks_triggered(bool checked)
{
	bool isRunning = thread->isRunning();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setOverlayMasks(-1, checked);
	}

    // Change button states at the list view
    if (this->ui->ReconfigureDetectorButton->isEnabled())
    {
        showActiveDetectorButtons(true, this->ui->detectorListView->currentIndex());
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_actionShow_all_debug_windows_triggered(bool checked)
{
	bool isRunning = thread->isRunning();

	std::shared_ptr<DetectorHandler> dHandler = thread->getDetectorHandler();

	if (dHandler)
	{
		dHandler->setShowDebugWindows(-1, checked);
	}

    // Change button states at the list view
    if (this->ui->ReconfigureDetectorButton->isEnabled())
    {
        showActiveDetectorButtons(true, this->ui->detectorListView->currentIndex());
    }

	if (isRunning)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_actionShow_video_triggered(bool checked)
{
    ShowVideo = checked;

    if (!ShowVideo)
    {
        for (int i = 0; i < viewers.size(); ++i)
        {
            viewers[i]->clear(); // Show blank image
        }
    }
    else {
		bool isRunning = thread->isRunning();

		std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

		if (!vHandler)
		{
			return;
		}

        vector<FrameStruct> updateStructs = vHandler->getCurrentFrame();
		displayFrames(updateStructs, 0);

		if (isRunning)
		{
			thread->startProcessing();
		}
    }
}

void MainWindow::on_actionOpen_changelog_triggered()
{
    QDialog* licenseBox = new QDialog(this);
    licenseBox->setWindowTitle(tr("Changelog"));

    QVBoxLayout* licenseLayout = new QVBoxLayout();

    QTextEdit* txtEdit = new QTextEdit();
    txtEdit->setReadOnly(true);
    licenseLayout->addWidget(txtEdit);

    auto* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    licenseLayout->addWidget(buttonBox);
    licenseBox->setLayout(licenseLayout);

    QFile inputFile(":/changelog.txt");

    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readAll();
            txtEdit->setText(line);
        }
        inputFile.close();
    }

    connect(buttonBox, SIGNAL(accepted()), licenseBox, SLOT(accept()));

    licenseBox->setMinimumWidth(500);
    licenseBox->setMinimumHeight(450);
    licenseBox->exec();
}


void MainWindow::slotSelectionChanged(const QItemSelection & selected, const QItemSelection & prevSelected)
{
    // The selection changed in the detectorListView. Update the buttons below the view

    if (IsInitialized && !selected.isEmpty() && selected.indexes().first().isValid())
    {
        showActiveDetectorButtons(true, selected.indexes().first());
    } else {
        QModelIndex idx;
        showActiveDetectorButtons(false, idx);
    }

}

void MainWindow::on_actionLicense_triggered()
{
    QDialog* licenseBox = new QDialog(this);
    licenseBox->setWindowTitle(tr("RUBA License"));

    QVBoxLayout* licenseLayout = new QVBoxLayout();

    QTextEdit* txtEdit = new QTextEdit();
    txtEdit->setReadOnly(true);
    licenseLayout->addWidget(txtEdit);

    auto* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    licenseLayout->addWidget(buttonBox);
    licenseBox->setLayout(licenseLayout);

    QFile inputFile(":/license.md.html");

    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readAll();
            txtEdit->setHtml(line);
        }
        inputFile.close();
    }
    
    connect(buttonBox, SIGNAL(accepted()), licenseBox, SLOT(accept()));

    licenseBox->setMinimumWidth(500);
    licenseBox->setMinimumHeight(450);
    licenseBox->exec();
}

void MainWindow::on_actionAbout_triggered()
{
	QString descriptionA = QString("RUBA is a joint effort between the <a href=\"http://www.vap.aau.dk/\">Visual Analysis of People Lab</a> and the <a href=\"https://vbn.aau.dk/en/organisations/forskningsgruppen-for-trafik-civil\">Traffic Safety Research Group</a> at Aalborg University, Denmark.<br>") +
                         QString("<br>Created by Chris Holmberg Bahnsen, Tanja Kidholm Osmann Madsen, and Morten Bornø Jensen, Aalborg University.<br>") +
                         QString("<br>The user manual is available <a href=\"https://bitbucket.org/aauvap/ruba/wiki/Home\">online</a>.");
	QString descriptionB("<br><hr>Some icons by Yusuke Kamiyamane. All rights reserved. Icons licensed under a Creative Commons Attribution 3.0 License.");
    QString presenceLegal("<hr>The presence detector uses the SuBSENSE change detection algorithm developed by Pierre-Luc St-Charles, Ecole Polytechnique de Montreal. Please click the menu <b>Window | RUBA License</b> for additional license information.");
    QString qtLegal("This program uses the <a href=\"https://www.qt.io/\">Qt cross-platform application and UI framework</a> licensed under LGPL. Please click the menu <b>Window | About Qt</b> for more information about the license and the specific Qt version used");

	QMessageBox::about(this, "Road User Behaviour Analysis " + QString(RUBA_VERSION), QString("<span>%1&nbsp;&nbsp;%2%3<hr>%4").arg(descriptionA).arg(descriptionB).arg(presenceLegal).arg(qtLegal));
    

}

void MainWindow::on_actionAboutQt_triggered() {
    QMessageBox::aboutQt(this, "About Qt");
}

void MainWindow::on_actionReport_Bug_triggered()
{
	QString description("Report bugs or enhancements at:<br>");
	QString hyperlinkText("https://bitbucket.org/aauvap/ruba/issues/new");

	QMessageBox::about(this, "Road User Behaviour Analysis " + QString(RUBA_VERSION), QString("<span>%1&nbsp;&nbsp;</span><a href=\"%2\">%2</a>").arg(description).arg(hyperlinkText));
}

void MainWindow::on_moveVideoUpButton_clicked()
{
    if (!ui->videoListView->selectionModel()->selectedRows().empty())
    {
		std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

		if (!vHandler)
		{
			return;
		}
		
		int selectedRow = ui->videoListView->selectionModel()->selectedRows()[0].row();
        QModelIndex idx = ui->videoListView->model()->index(selectedRow-1,0);

		vHandler->moveItem(selectedRow, selectedRow-1);
        ui->videoListView->setCurrentIndex(idx);
    }
}

void MainWindow::on_moveVideoDownButton_clicked()
{
    if (!ui->videoListView->selectionModel()->selectedRows().empty())
    {
		std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

		if (!vHandler)
		{
			return;
		}

        int selectedRow = ui->videoListView->selectionModel()->selectedRows()[0].row();
        QModelIndex idx = ui->videoListView->model()->index(selectedRow+1,0);

		vHandler->moveItem(selectedRow, selectedRow+1);
        ui->videoListView->setCurrentIndex(idx);
    }

}

void MainWindow::on_deleteVideosFromListButton_clicked()
{
    bool isRunning = thread->isRunning();

    QList<int> selectedIndices;

    for (int i = 0; i < ui->videoListView->selectionModel()->selectedRows().size(); ++i)
    {
        selectedIndices.append(ui->videoListView->selectionModel()->selectedRows()[i].row());
    }

	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!vHandler)
	{
		return;
	}

    vHandler->removeVideoFiles(selectedIndices);

    ui->videoListView->setModel(thread->getVideoHandler()->getVideoListModel());

	bool isInitialised = thread->getVideoHandler()->getIsInitialised();

    if (isRunning && isInitialised)
    {
        thread->startProcessing();
    }

    if (!isInitialised)
    {
        IsInitialized = false;
        setUpLayout();
        viewers[0]->setImage(cv::Mat());
    }
}

void MainWindow::on_addVideosToListButton_clicked()
{
	bool isRunning = thread->isRunning();

    QSettings settings;
    settings.beginGroup("video");
    QString videoHomeDir = settings.value("videoHomeDir", "").toString();
	settings.endGroup();

	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!vHandler)
	{
		return;
	}

	// Get the current VideoSyncType
	settings.beginGroup("synchronisation");
	VideoSyncType videoSyncType = (VideoSyncType)settings.value("videoSyncType", 0).toInt();
	settings.endGroup();

    videoHomeDir = vHandler->addVideoFiles(videoHomeDir, videoSyncType);

    settings.setValue("videoHomeDir", videoHomeDir);
    settings.endGroup();

    std::vector<ruba::FrameStruct> currentFrames = vHandler->getCurrentFrame();

    if (currentFrames.empty() || (!currentFrames.empty() && currentFrames[0].Image.empty()))
    {
        // Open the first video in the list if the current frame is empty, or there are currently not
        // loaded any videos
		vHandler->setCurrentVideo(0);

        IsInitialized = vHandler->getIsInitialised();
    }
    
    setVideoFiles();

	if (isRunning && IsInitialized)
	{
		thread->startProcessing();
	}
}

void MainWindow::on_showVideoPropertiesButton_clicked()
{
    if (ui->videoListView->currentIndex().isValid())
    {
        int index = ui->videoListView->currentIndex().row();

		bool isRunning = thread->isRunning();

		std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

		if (!vHandler)
		{
			return;
		}

		vHandler->showVideoInformation(index);

		if (isRunning)
		{
			thread->startProcessing();
		}
    }
}

void MainWindow::on_videoListView_doubleClicked(const QModelIndex &index)
{
    bool isRunning = thread->isRunning();

	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!vHandler)
	{
		return;
	}

	ui->videoFilesWidget->setEnabled(false);

	vHandler->setCurrentVideo(index.row());
    IsInitialized = vHandler->getIsInitialised();

    setVideoFiles();

    ui->videoFilesWidget->setEnabled(true);

    if (IsInitialized && isRunning)
    {
		thread->startProcessing();
    } 


}

void MainWindow::setMaximumVideoSpeed(int fps)
{
    QSettings settings;
    settings.beginGroup("video");
    settings.setValue("playbackFps", fps);
    settings.endGroup();

    // Set the GUI
    ui->videoControl_SpeedSlider->blockSignals(true);	
	int targetFps = -1;

    if (fps == 0)
    {
        // If we have marked that the system should run "As fast as possible", 
        ui->videoControl_SpeedSlider->setValue(ui->videoControl_SpeedSlider->maximum());
        ui->videoControl_SpeedSlider->setToolTip(tr(
            "Desired processing speed (Currently running as fast as possible)"));
		ui->videoPlaybackFpsLabel->setText(tr("Desired processing speed: fastest"));
    }
    else {
        ui->videoControl_SpeedSlider->setValue(fps);
        ui->videoControl_SpeedSlider->setToolTip(tr(
            "Desired processing speed (Current: %1 fps)").arg(fps));
		ui->videoPlaybackFpsLabel->setText(tr("Desired processing speed: %1 fps").arg(fps));
		targetFps = fps;
    }
    ui->videoControl_SpeedSlider->blockSignals(false);
    thread->setTargetFps(targetFps);
}

void MainWindow::on_actionAdjust_window_size_to_video_triggered()
{
	std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

	if (!vHandler)
	{
		return;
	}

    vector<FrameStruct> frames = vHandler->getCurrentFrame();

    if (!frames.empty() && !frames[0].Image.empty())
    {
        int chromeWidth = 214;
        int chromeHeight = 102;

        ui->mainArea->resize(frames[0].Image.cols + 4, frames[0].Image.rows + 4);

        this->resize(frames[0].Image.cols + chromeWidth, frames[0].Image.rows + chromeHeight);

        on_actionTile_modality_windows_triggered();
    }
}

void MainWindow::on_actionTile_modality_windows_triggered()
{
    this->ui->mainArea->tileSubWindows();
}

void MainWindow::on_actionEnable_thermal_modality_triggered()
{
    if (ui->actionEnable_thermal_modality->isChecked())
    {
        ViewInfo info;
        info.modality = ImageModalities::THERMAL;
        info.viewId = currentViews.size()+1;
        addView(info);
    } else
    {
        // Fetch the View that corresponds to the thermal modality
        for (auto i = 0; i < currentViews.size(); ++i)
        {
            if (currentViews[i].modality == ImageModalities::THERMAL)
            {
                removeView(currentViews[i]);
                return;
            }
        }
    }
}

void MainWindow::on_actionEnable_RGB_modality_triggered()
{
    if (ui->actionEnable_RGB_modality->isChecked())
    {
        ViewInfo info;
        info.modality = ImageModalities::RGB;
        info.viewId = currentViews.size() + 1;
        addView(info);
    } else {
        // Fetch the View that corresponds to the RGB modality
        for (auto i = 0; i < currentViews.size(); ++i)
        {
            if (currentViews[i].modality == ImageModalities::RGB)
            {
                removeView(currentViews[i]);
                return;
            }
        }
    }
}

void MainWindow::subwindowClosingHandler(ViewInfo* view)
{
    removeView(*view);
}

    
void MainWindow::on_videoControl_SpeedSlider_valueChanged(int value)
{
    int newFps = value;	

    if (value == ui->videoControl_SpeedSlider->maximum())
    {
        // If the slider has reached the maximum value, let the program run
        // as fast as possible - which is marked as zero
        newFps = 0;
	}
    
    setMaximumVideoSpeed(newFps);
}

void MainWindow::on_videoControl_SpeedSlider_sliderPressed()
{
	this->ui->videoPlaybackFpsLabel->show();
}

void MainWindow::on_videoControl_SpeedSlider_sliderReleased()
{
	this->ui->videoPlaybackFpsLabel->hide();
}

void MainWindow::on_videoControl_Prev5Frames_clicked()
{
    if (!thread->isRunning())
    {
        // Disable the playback controls as long as we are jumping to the frame
        ui->playbackControlsWidget->setEnabled(false);
        ui->playbackControlsWaitBar->show();

		std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

		if (!vHandler)
		{
			ui->playbackControlsWaitBar->hide();
			ui->playbackControlsWidget->setEnabled(true);
			return;
		}

        long currentFrame = vHandler->getFrameNbr();
		vHandler->jumpToPrevFrame(5); // Jump back five frames

        // And then grab the current frame
        vector<FrameStruct> updateStruct = thread->getVideoHandler()->getCurrentFrame();
		displayFrames(updateStruct, 0);

		std::vector<ruba::ChartStruct> charts;
		bool debugWindowsSuccess = thread->getDetectorCharts(charts);
		if (debugWindowsSuccess && !charts.empty())
		{
			for (auto& chart : charts)
			{
				cv::imshow(chart.Title.toStdString(), chart.Image);
			}
		}

        // And then enable the controls again
        ui->playbackControlsWaitBar->hide();
        ui->playbackControlsWidget->setEnabled(true);
    }
}

void MainWindow::on_videoControl_PrevFrame_clicked()
{
    if (!thread->isRunning())
    {
        // Disable the playback controls as long as we are jumping to the frame
        ui->playbackControlsWidget->setEnabled(false);
        ui->playbackControlsWaitBar->show();

		std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

		if (!vHandler)
		{
			ui->playbackControlsWaitBar->hide();
			ui->playbackControlsWidget->setEnabled(true);
			return;
		}

        long currentFrame = vHandler->getFrameNbr();
		vHandler->jumpToPrevFrame(1); // Jump back one frame

        // And then grab the current frame
        vector<FrameStruct> updateStruct = thread->getVideoHandler()->getCurrentFrame();
		displayFrames(updateStruct, 0);

		std::vector<ruba::ChartStruct> charts;
		bool debugWindowsSuccess = thread->getDetectorCharts(charts);
		if (debugWindowsSuccess && !charts.empty())
		{
			for (auto& chart : charts)
			{
				cv::imshow(chart.Title.toStdString(), chart.Image);
			}
		}

        // And then enable the controls again
        ui->playbackControlsWaitBar->hide();
        ui->playbackControlsWidget->setEnabled(true);
    }
}

void MainWindow::on_videoControl_NextFrame_clicked()
{
    if (!thread->isRunning())
    {
		std::vector<FrameStruct> frames;
		bool success = thread->processNextFrame(frames);

		std::vector<ruba::ChartStruct> charts;
		bool debugWindowsSuccess = thread->getDetectorCharts(charts);

		if (success)
		{
			displayFrames(frames, 0);
			thread->unlockDisplayFrames();

			if (debugWindowsSuccess && !charts.empty())
			{
				for (auto& chart : charts)
				{
					cv::imshow(chart.Title.toStdString(), chart.Image);
				}
			}
		}
    }
        
}

void MainWindow::on_videoControl_Next5Frames_clicked()
{
    if (!thread->isRunning())
    {
        std::vector<FrameStruct> frames;

		std::shared_ptr<VideoHandler> vHandler = thread->getVideoHandler();

		if (!vHandler)
		{
			return;
		}

        // Grab the next four frames and throw them out
        for (int i = 0; i < 4; ++i)
        {
            frames = vHandler->getNextFrame();
        }

        // And show frame number 5:
		bool success = thread->processNextFrame(frames);

		std::vector<ruba::ChartStruct> charts;
		bool debugWindowsSuccess = thread->getDetectorCharts(charts);

		if (success)
		{
			displayFrames(frames, 0);
			thread->unlockDisplayFrames();

			if (debugWindowsSuccess && !charts.empty())
			{
				for (auto& chart : charts)
				{
					cv::imshow(chart.Title.toStdString(), chart.Image);
				}
			}
		}
    }
}

void MainWindow::on_videoControl_FiveSecPrev_clicked()
{
	if (!thread->isRunning())
	{
		auto vHandler = this->thread->getVideoHandler();

		if (vHandler)
		{
			jumpToFrameAtTime(vHandler->getCurrentTime().addSecs(-5));
		}
	}
}

void MainWindow::on_videoControl_OneSecPrev_clicked()
{
	
	if (!thread->isRunning())
	{
		auto vHandler = this->thread->getVideoHandler();

		if (vHandler)
		{
			jumpToFrameAtTime(vHandler->getCurrentTime().addSecs(-1));
		}
	}
}

void MainWindow::on_videoControl_OneSecForw_clicked()
{
	if (!thread->isRunning())
	{
		auto vHandler = this->thread->getVideoHandler();

		if (vHandler)
		{
			jumpToFrameAtTime(vHandler->getCurrentTime().addSecs(1));
		}
	}
}

void MainWindow::on_videoControl_FiveSecForw_clicked()
{
	if (!thread->isRunning())
	{
		auto vHandler = this->thread->getVideoHandler();

		if (vHandler)
		{
			jumpToFrameAtTime(vHandler->getCurrentTime().addSecs(5));
		}
	}
}

void MainWindow::on_actionSettings_triggered()
{
    SettingsDialog dialog(this, this->devState);
    dialog.exec();

    // Update the playbackFps/maximumVideospeed
    QSettings settings;
    settings.beginGroup("video");
    int fps = settings.value("playbackFps", 0).toInt();
    settings.endGroup();

    setMaximumVideoSpeed(fps);
}
