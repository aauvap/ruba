#include <cmath>
#include "stationarydetector.h"
#include "opencvviewer.h"
#include "presencedetector.h"
#include "movementdetector.h"


using namespace std;
using namespace cv;

using namespace ruba;



StationaryDetector::StationaryDetector(BaseDetectorParams baseParams, ruba::MaskHandlerOutputStruct initParams, 
                                       bool useScrollingChart)
    : AbstractMultiModalDetector(configureBaseDetectorParams(baseParams, STATIONARYDETECTOR),
		initialiseChartColors())
{
    // In order to identify stationary objects, we need one PresenceDetector and one MovementDetector
    PDetectors.clear();
    MDetectors.clear();

    masks = initParams.Masks;

    this->UseScrollingChart = useScrollingChart;

    if (!initParams.TriggerThreshold.empty())
    {
        // Initialize the detectors based on the parameters from the input
        // Shape the triggerThreshold to fit the presenceDetector and movementDetectors
        vector<int> minCoveredArea;
        vector<vector<int> > movementThreshold;

        for (size_t i = 0; i < initParams.TriggerThreshold.size(); ++i)
        {
            minCoveredArea.push_back(initParams.TriggerThreshold[i][0]);

            vector<int> tempT;
            tempT.push_back(initParams.TriggerThreshold[i][1]);
            movementThreshold.push_back(tempT);
        }
        
        PresenceDetector EDetector(baseParams, minCoveredArea, initParams.Masks, initParams.BackgroundImages, true);
        PDetectors.push_back(EDetector);


        // Make a copy of the mask for the MovementDetector
        vector<Mat> flowMasks;

        for (size_t i = 0; i < initParams.Masks.size(); ++i)
        {
            Mat tmpMask;
            initParams.Masks[i].copyTo(tmpMask);
            flowMasks.push_back(tmpMask);
        }

        initParams.upperThresholdBoundMultiplier = 2;
        MovementDetector FDetector(baseParams, flowMasks, initParams.MinVecLengthThreshold, initParams.FlowRange,
            movementThreshold, true, initParams.upperThresholdBoundMultiplier);
        MDetectors.push_back(FDetector);

        for (size_t i = 0; i < initParams.Masks.size(); ++i)
        {
            DoubleScrollingChart* chart = new DoubleScrollingChart(300,300);
            chart->SetThresholds(PDetectors[0].GetChartThresholds()[i], MDetectors[0].GetChartThresholds()[i]);
            chart->SetVisualLineThresholds(MDetectors[0].GetChartThresholds()[i], PDetectors[0].GetChartThresholds()[i]);
            chart->SetTitle("StationaryDetector " + Utils::GetModalityName(initParams.views[i].modality));
            DoubleCharts.push_back(chart);
        }

        InitDetector();
        IsInitialized = true;

    } else {
        IsInitialized = false;
    }

}


StationaryDetector::~StationaryDetector()
{

}

bool StationaryDetector::GetIsPresenceDetectorTriggered()
{
    return isPresenceDetectorTriggered;
}



DetectorOutput StationaryDetector::GetOutput()
{
    DetectorOutput outputStruct;

    if (this->IsInitialized)
    {
        outputStruct.Confidence.push_back(1);
    } else {
        outputStruct.Confidence.push_back(0);
    }

    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = IsTriggered;
    outputStruct.EnteringConflictZone = QDateTime();
    outputStruct.LeavingConflictZone = QDateTime();

    outputStruct.Timestamp = getCurrentTime();


    return outputStruct;

}

void StationaryDetector::InitDetector()
{
    initTriggeredHistory(1);
    isPrevPresenceDetectorTriggered = false;

    // Set the background colour of the chart
    for (size_t i = 0; i < DoubleCharts.size(); ++i)
    {
        DoubleCharts[int(i)]->SetBackground(Scalar(220,255,220)); // Light green
    }


    // Set flags of the Detector
    confidence.clear();
    IsTriggered.clear();
    lastChange.clear();

    confidence.push_back(0);
    IsTriggered.push_back(false);
    lastChange.push_back(QDateTime());

    if (!PDetectors.empty())
    {
        PDetectors[0].SetMasks(masks);
    }

    if (!MDetectors.empty())
    {
        MDetectors[0].SetMasks(masks);
    }
}


void StationaryDetector::updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput)
{
    
    bool edgeDetectorStateChanged = false;
    bool isMovementDetectorTriggered = false;

    // Update the detectors
    PDetectors[0].Update(frames);
    isPresenceDetectorTriggered = PDetectors[0].GetIsTriggered()[0];

    if (isPresenceDetectorTriggered)
    {
        MDetectors[0].Update(frames);
        isMovementDetectorTriggered = MDetectors[0].GetIsTriggered()[0];

        if (!isPrevPresenceDetectorTriggered)
        {
            // When the edge detector has just returned to the triggered state,
            //set the flow detector state inactive, due to the dynamic updating algorithm
            edgeDetectorStateChanged = true;
        }
    }

    if (UseScrollingChart)
    {
        vector<int> eData, fData;
        eData = PDetectors[0].GetChartData();
        fData = MDetectors[0].GetChartData();

        if (isPresenceDetectorTriggered)
        {
            if (eData.size() == fData.size())
            {
                for (size_t i = 0; i < eData.size(); ++i)
                {
                    DoubleCharts[i]->AddDataPoints(eData[i], fData[i]);
                }
            }
        } else
        {
            for (size_t i = 0; i < eData.size(); ++i)
            {
                DoubleCharts[i]->AddDataPoints(eData[i], 0);
            }
        }
    }

    if (isPresenceDetectorTriggered && !isMovementDetectorTriggered && !edgeDetectorStateChanged)
    {
        // If the edgeDetector is triggered and the flowDetector is not triggered, the
        // stationary detector is triggered.
        // This means that an object is inside the zone but is not moving
        IsTriggered[0] = true;
    } else {
        IsTriggered[0] = false;
    }

    triggeredHistory[0].dequeue();
    triggeredHistory[0].enqueue(IsTriggered[0]);


    isPrevPresenceDetectorTriggered = isPresenceDetectorTriggered;
}

QList<QString> StationaryDetector::getDetectorSpecificDebugInfo()
{
    QList<QString> debugList;
    return debugList;
}

std::vector<ChartStruct> StationaryDetector::GetChartStructs()
{
    vector<ChartStruct> charts;
    for (size_t i = 0; i < DoubleCharts.size(); ++i)
    {
        ChartStruct chart = ChartStruct();
        chart.Image = this->DoubleCharts[i]->GetChartImage();
        chart.Title = this->DoubleCharts[i]->GetTitle();
        charts.push_back(chart);
    }

    return charts;
}

void StationaryDetector::SetUseScrollingChart(bool useScrollingChart)
{
    if (!PDetectors.empty())
    {
        PDetectors[0].SetUseScrollingChart(useScrollingChart);
    }

    if (!MDetectors.empty())
    {
        MDetectors[0].SetUseScrollingChart(useScrollingChart);
    }
}

ChartColors ruba::StationaryDetector::initialiseChartColors()
{
	ChartColors chartColors;
	chartColors.backgroundColors.enabledTriggeredColor = cv::Scalar(200, 255, 200);
	chartColors.backgroundColors.enabledNotTriggeredColor = cv::Scalar(230, 255, 230);
	chartColors.backgroundColors.notEnabledColor = cv::Scalar::all(255);

	chartColors.lineUnderColors.enabledTriggeredColor = cv::Scalar(220, 0, 0);
	chartColors.lineUnderColors.enabledNotTriggeredColor = cv::Scalar(220, 100, 100);
	chartColors.lineUnderColors.notEnabledColor = cv::Scalar::all(150);

	chartColors.lineOverColors.enabledTriggeredColor = cv::Scalar(100, 100, 255);
	chartColors.lineOverColors.enabledNotTriggeredColor = cv::Scalar(0, 0, 220);
	chartColors.lineOverColors.notEnabledColor = cv::Scalar::all(150);

	return chartColors;
}

void ruba::StationaryDetector::setChartStatus(bool enabled, bool triggered)
{
	ChartColors colors = getChartColors();

	for (auto &chart : DoubleCharts)
	{
		if (enabled)
		{
			if (triggered)
			{
				chart->SetBackground(colors.backgroundColors.enabledTriggeredColor);
				chart->SetLineOver(cv::Scalar(220, 0, 0), cv::Scalar(100, 100, 255));
				chart->SetLineUnder(cv::Scalar(220, 0, 0), cv::Scalar(0, 0, 220));
				
			}
			else {
				chart->SetBackground(colors.backgroundColors.enabledNotTriggeredColor);
				chart->SetLineOver(cv::Scalar(220, 0, 0), cv::Scalar(100, 100, 255));
				chart->SetLineUnder(cv::Scalar(220, 100, 100), cv::Scalar(0, 0, 220));
			}
		}
		else {
			chart->SetBackground(colors.backgroundColors.notEnabledColor);
			chart->SetLineOver(cv::Scalar::all(150), cv::Scalar::all(150));
			chart->SetLineUnder(cv::Scalar::all(150), cv::Scalar::all(150));
		}
	}
}

std::vector<QString*> StationaryDetector::GetChartTitles()
{
    if (UseScrollingChart)
    {
        vector<QString*> tempTitles;

        for (size_t i = 0; i < Charts.size(); ++i)
        {
            QString* tempWindowName = new QString("Stationary Detector");;
            tempTitles.push_back(tempWindowName);
        }
        return tempTitles;

    }
    else
    {
        vector<QString*> tempTitles;
        tempTitles.push_back(NULL);

        return tempTitles;
    }
}