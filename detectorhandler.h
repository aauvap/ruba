#ifndef DETECTORHANDLER_H
#define DETECTORHANDLER_H




#include <opencv2/opencv.hpp>
#include <vector>

#include <QApplication>
#include <QBrush>
#include <QColor>
#include <QWidget>
#include <QSplashScreen>
#include <QStandardItemModel>

#include "debugprefstruct.h"
#include "ruba.h"
#include "rubabaseobject.h"
#include "utils.hpp"
#include "viewinfo.h"
#include "detectors/abstractcompounddetector.h"

#include "detectors/rightturningcar.h"
#include "detectors/leftturningcar.h"
#include "detectors/straightgoingcyclist.h"
#include "detectors/singlemodule.h"
#include "detectors/doublemodule.h"
#include "detectors/exclusivemodule.h"
#include "detectors/groundtruthannotator.h"
#include "detectors/interactiondetector.h"
#include "detectors/logfilereviewer.h"



class ruba::DetectorHandler : public QWidget
{
	Q_OBJECT;

public:
    DetectorHandler(QWidget *parent, QList<ViewInfo> views);

    int loadFromFile(cv::FileStorage& fs, std::vector<ruba::FrameStruct> frames, 
                     QProgressDialog &progress, QSplashScreen &splash, bool showSplash);
    int saveToFile(cv::FileStorage& fs);

    void loadDetector(std::vector<ruba::FrameStruct> frames);
	void getDetectors(std::vector<std::shared_ptr<ruba::AbstractCompoundDetector> >& detectors);

    // Universal method to create an arbitrary detector inherited from AbstractCompoundDetector
    // and with known detectorType
    void createDetector(int detectorType, std::vector<ruba::FrameStruct> frames, 
        const QString &filePath = "");

    // Reconfigure detector using the DetectorSelectionDialog
    void reconfigureDetector(int index, std::vector<ruba::FrameStruct> frames);

    void resetDetector(int index);

    void deleteDetectorFromList(int index);

    void paintOverlay(std::vector<FrameStruct>& frames);

    void lastFrameReached();
    void update(std::vector<ruba::FrameStruct> &frames);
    int getDetectorCount();

	std::vector<ruba::ChartStruct> getCharts();
	
	void refreshDetectorInformationList();
	void refreshDetectorInformationModel();

    QStandardItemModel* getDetectorListModel();
    ruba::DebugPrefStruct getDebugPref();

    // Handles for interfacing with the debug preferences
    void setOverlayMasks(int index, bool checked);
    void setOverlayDebugInformation(int index, bool checked);
    void setOverlayExtDebugInformation(int index, bool checked);
    void setShowDebugWindows(int index, bool checked);
    void setOverlayTimestamp(bool checked);

    int addView(ViewInfo view);
    int removeView(ViewInfo view);

    void checkDetectorDiagnostics(int index, bool forceShowDialog = false);

signals:
	void sendMessage(const QString& title, const QString& message, MessageType messageType);

private:
    void setDependency(int index);
    void removeDependency(int index);

    int requestDetectorId();
    int removeDetectorId(int detectorId);

    // Determines the detector type by opening and reading the type from the file
    int determineDetectorType(QString filePath);

    // Add detector to the existing list
    void addDetector(std::shared_ptr<ruba::AbstractCompoundDetector> newDetector, int detectorId, QString moduleText);

    void addInteractionDetector(BaseDetectorParams baseParams, QString configFile);

    void updateDetectorInformation(const QList<QString>& infoList, QStandardItem* item,
        bool shrinkToFit = false);

    // Internal list of detectors
    QList<std::shared_ptr<ruba::AbstractCompoundDetector>> TDetectors;

    // External list of detectors for visual output
    QStandardItemModel* detectorListModel;

    // List of corresponding detectors ID's
    QList<int> detectorIds;

	// Debug info
	std::vector<QList<QString> > debugInfoList;
	std::vector<QList<QString> > extendedDebugInfoList;
	std::vector<QList<QString> > everyEventLogInfoList;
	std::vector<std::map<QString, QList<QString> > > individualLogInfoList;
	std::vector<DetectorDiagnostics> tDetectorDiagnostics;
	

    // Debug preferences
    void addDebugPref();
    void removeDebugPref(int index);
    ruba::DebugPrefStruct debugPref;


    // Settings
    bool UpdateDebugInfo;
    QList<ViewInfo> currentViews;

    QBrush enabledBrush;
    QBrush disabledBrush;

   

    // Pointer to the main GUI window
    QWidget* parent;
};

#endif // DETECTORHANDLER_H
