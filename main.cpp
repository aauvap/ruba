#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	QCommandLineParser parser;
	QCommandLineOption devOption("d", QApplication::translate("main", "Open RUBA in developer mode"));

	parser.addOption(devOption);
	parser.process(a);

    MainWindow w(parser.isSet(devOption) ? 1 : 0);
    w.show();
    w.maximizeSubWindows();


   int returnCode = a.exec();
    w.cleanUpOnExit();

    return returnCode;
}
