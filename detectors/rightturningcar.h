#ifndef RIGHTTURNINGCAR_H
#define RIGHTTURNINGCAR_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>

#include "abstractcompounddetector.h"

class ruba::RightTurningCar : public AbstractCompoundDetector
{

public:
    explicit RightTurningCar(BaseDetectorParams baseParams, QWidget *parent = 0, QString configFile = "");

    ruba::DetectorOutput GetOutput();


private:
    void setDetectorSpecificDefaultParameters();

    QList<QString> getDetectorSpecificDebugInfo();
    QList<QString> getDetectorSpecificExtendedDebugInfo();
    void updateDetectorSpecificLogic();
    void setDetectorSpecificEnabledStates();

    // External flags that are accessible through a get function
    bool IsWaitingInS1;
    QDateTime EnteringConflictZone;
    QDateTime LeavingConflictZone;

	StationaryDetector* sDetector;

};

#endif // RIGHTTURNINGCAR_H
