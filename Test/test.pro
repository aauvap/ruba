QT += widgets testlib

SOURCES = ../dialogs/test/test_logsettingsdialog.cpp \
    ../dialogs/logsettingsdialog.cpp \
    ../utils.cpp

HEADERS = ../dialogs/logsettingsdialog.h \
    ../utils.hpp


# install
target.path = "test"
INSTALLS += target

win32{
    INCLUDEPATH += C:/Tools/opencv3/build/include
    #INCLUDEPATH += $$(OPENCV_INCLUDE)

    DEPENDPATH += C:/Tools/opencv3/build/include
    #DEPENDPATH += $$(OPENCV_INCLUDE)

    # Debug version
    # Remember to add "make install" as a build step under projects, to copy the dll files
    win32:CONFIG(debug, debug|release) {
        LIBS += -L$$(OPENCV_LIB)/ -lopencv_core310d
        LIBS += -L$$(OPENCV_LIB)/ -lopencv_imgproc310d
        LIBS += -L$$(OPENCV_LIB)/ -lopencv_highgui310d
        LIBS += -L$$(OPENCV_LIB)/ -lopencv_video310d
        LIBS += -L$$(OPENCV_BIN)/ -lopencv_videoio310d
        LIBS += -L$$(OPENCV_BIN)/ -lopencv_imgcodecs310d
        LIBS += -lQt5Concurrent

        dllfiles.path    = $$OUT_PWD/debug
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_core310d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_imgproc310d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_highgui310d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_ffmpeg310.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_video310d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_videoio310d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_imgcodecs310d.dll
        INSTALLS        += dllfiles
    }

    # Release version
    # Remember to add "make install" as a build step under projects, to copy the dll files
    win32:CONFIG(release, debug|release) {
        LIBS += -L$$(OPENCV_LIB)/Release -lopencv_core310
        LIBS += -L$$(OPENCV_LIB)/Release -lopencv_imgproc310
        LIBS += -L$$(OPENCV_LIB)/Release -lopencv_highgui310
        LIBS += -L$$(OPENCV_LIB)/Release -lopencv_video310
        LIBS += -L$$(OPENCV_BIN)/Release -lopencv_videoio310
        LIBS += -L$$(OPENCV_BIN)/Release -lopencv_imgcodecs310
        LIBS += -lQt5Concurrent

        dllfiles.path    = $$OUT_PWD/release
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_core310.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_imgproc310.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_highgui310.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_ffmpeg310.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_video310.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_videoio310.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_imgcodecs310.dll
        INSTALLS        += dllfiles
    }
}
