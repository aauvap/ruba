#ifndef TRAFFICLIGHTDETECTORSINGLEMODAL_H
#define TRAFFICLIGHTDETECTORSINGLEMODAL_H


#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include <QtCore>
#include <QDialog>
#include <QMessageBox>
#include <QFileInfoList>
#include <QFile>
#include <QFileDialog>
#include <QList>
#include <QDateTime>

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

#include <cmath>
#include <fstream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/opencv.hpp>

#include "ruba.h"
#include "abstractsinglemodaldetector.h"
#include "scrollingchart.h"
#include "framestruct.h"
#include "ChartStruct.h"
#include "detectoroutputstruct.h"
#include "utils.hpp"

#include <cassert>

#include <ctype.h>



class ruba::TrafficLightDetectorSingleModal : public AbstractSingleModalDetector {

public: 
	// Constructor
	TrafficLightDetectorSingleModal(BaseDetectorParams baseParams, cv::Mat maskimage, bool UseScrollingChart, std::vector<cv::Point> tlPlacements, int tlTypeTrigger);

	ruba::DetectorOutput GetOutput();
	void InitDetector();
    
	std::vector<QString*>  GetChartTitles();

	// Update the parameters individually

	void SetBackground(cv::Mat backImage);
    
private:
    void extractColors();
	
	void extractContours();
	void extractROI();
    void calcROIHisto(cv::Mat histInput);

    void updateDetectorSpecifics();

    // Here we go ---------------
    void tlColorAnalysis();
    void tlDifferenceImageAnalysis();
    void tlStructuralAnalysis();
    void tlStructuralMotionAnalysis();
    void tlStructuralNearestAnalysis();
    void tlStructuralValPositionAnalysis();
    void tlStructuralPrediction();
    void updateClassParameters();
    void finalClassification();
    
    cv::Mat prepareImage(cv::Mat inputImage);
    cv::Point warpTlPoint(cv::Point pointToConvert, cv::Mat tMat);

    cv::Mat FinalDiffImage;
    cv::Mat differenceImage, thresholdedDiffImage;
    cv::Mat prevFrameRoi;

    bool motionFound;
    bool trustNearest;
    float redHeightAvg = 0;
    float yellowHeightAvg = 0;
    float greenHeightAvg = 0;
    float lastYVal;
    int filterTrigger = 99;
    int whatClassMotion = 3; // 0 = red, 1= yellow, 2 = green, 3 = unknown(default). 
    tlTypeClasses whatClassValPosition = tlTypeClasses::AMBIGIOUS;
    int whatClassNearest = 3;
    int whatClassPredict = 3;
    int mcX;
    int valAtRed;
    int valAtYellow;
    int valAtGreen;
    int tlTypeTrigger;
    float avgRedVal = NULL;
    float avgYellowVal = NULL;
    float avgGreenVal = NULL;
    int redYellowCheck = NULL;
    std::deque<int> tlHistory;

    std::vector<float> finalDetections;
    std::vector<float> states;
    cv::Mat yChannel, CbChannel, CrChannel;

    bool updateAverages;
    bool highConf;
    bool detectionCritSatisfied;

    int finalDetectionID = 3;

    // Here we go ---------------
    std::vector<cv::Point> localTlPlacements;
    cv::Point convertedRedTlPlacement, convertedYellowTlPlacement, convertedGreenTlPlacement;
    cv::Point warpedRedPlacement, warpedYellowPlacement, warpedGreenPlacement;
    

    QList<QString> getDetectorSpecificDebugInfo();
    std::vector<cv::Point> getValsAtTls();

	std::vector<double> minVecLengthThreshold;
	double leastMinVecLengthThreshold;

	std::vector<cv::Point> flowDirectionRange;
	std::vector<int> triggerThreshold;
    double TriggerThreshold;
	std::vector<int> upperThresholdBound;

	std::vector<double> chartHeightMultiplier;

	std::vector<int> nbrFlowVecInsideRange; // Number of flow vectors inside a predefined range
	std::vector<int> nbrFlowVec; // Number of flow vectors inside a fixed angular range from 0 to 359 degrees, inside 360 bins
	std::vector<double> flowVecHistInsideRange; // Histogram of flow vectors inside a predefined range
	std::vector<double> flowVecHist; // Histogram of flow vectors inside a fixed angular range from 0 to 359 range, inside 360 bins

	cv::Mat flow;
	cv::Mat roi, prevRoi;
	cv::Mat filledContourMask;
    cv::Mat roiColor;

	cv::Rect boundRect;
    cv::Mat thresholdedMask;

	bool IS_FIRST_RUN;

	cv::Rect ROI;

    // TEMP
    std::vector<cv::Mat> channels;
    
    cv::Mat histImageFinal;

    // Start of Video Stabilizator
    void stabiliseFrame();
    void initStabilisation();

    cv::Mat cur, cur_grey;
    cv::Mat prev, prev_grey;

    int vert_border;
    int k;

    cv::Mat last_T;
    cv::Mat prev_grey_, cur_grey_;

    struct TransformParam
    {
        TransformParam() {}
        TransformParam(double _dx, double _dy, double _da) {
            dx = _dx;
            dy = _dy;
            da = _da;
        }

        double dx;
        double dy;
        double da; // angle
    };
    struct Trajectory
    {
        Trajectory() {}
        Trajectory(double _x, double _y, double _a) {
            x = _x;
            y = _y;
            a = _a;
        }
        // "+"
        friend Trajectory operator+(const Trajectory &c1, const Trajectory  &c2) {
            return Trajectory(c1.x + c2.x, c1.y + c2.y, c1.a + c2.a);
        }
        //"-"
        friend Trajectory operator-(const Trajectory &c1, const Trajectory  &c2) {
            return Trajectory(c1.x - c2.x, c1.y - c2.y, c1.a - c2.a);
        }
        //"*"
        friend Trajectory operator*(const Trajectory &c1, const Trajectory  &c2) {
            return Trajectory(c1.x*c2.x, c1.y*c2.y, c1.a*c2.a);
        }
        //"/"
        friend Trajectory operator/(const Trajectory &c1, const Trajectory  &c2) {
            return Trajectory(c1.x / c2.x, c1.y / c2.y, c1.a / c2.a);
        }
        //"="
        Trajectory operator =(const Trajectory &rx) {
            x = rx.x;
            y = rx.y;
            a = rx.a;
            return Trajectory(x, y, a);
        }

        double x;
        double y;
        double a; // angle
    };
    // Step 1 - Get previous to current frame transformation (dx, dy, da) for all frames
    std::vector <TransformParam> prev_to_cur_transform; // previous to current
                                                       // Accumulated frame to frame transform
    double a = 0;
    double x = 0;
    double y = 0;
    // Step 2 - Accumulate the transformations to get the image trajectory
    std::vector <Trajectory> trajectory; // trajectory at all frames
                                        //
                                        // Step 3 - Smooth out the trajectory using an averaging window
    std::vector <Trajectory> smoothed_trajectory; // trajectory at all frames
    Trajectory X;//posteriori state estimate
    Trajectory	X_;//priori estimate
    Trajectory P;// posteriori estimate error covariance
    Trajectory P_;// priori estimate error covariance
    Trajectory K;//gain
    Trajectory z;//actual measurement
    double pstd = 3e-3;//can be changed
    double cstd = 0.25;//can be changed
    Trajectory Q;// process noise covariance
    Trajectory R;// measurement noise covariance 

    //extern Trajectory Q = (pstd, pstd, pstd);// process noise covariance
    //extern Trajectory R = (cstd, cstd, cstd);// measurement noise covariance 

    // Step 4 - Generate new set of previous to current transform, such that the trajectory ends up being the same as the smoothed trajectory
    std::vector <TransformParam> new_prev_to_cur_transform;
    // Step 5 - Apply the new transformation to the video
    //cap.set(CV_CAP_PROP_POS_FRAMES, 0);
    cv::Mat T;
    

    const int HORIZONTAL_BORDER_CROP = 0; // In pixels. Crops the border to reduce the black borders from stabilisation being too noticeable.

                                          // vector from prev to cur
    std::vector <cv::Point2f> prev_corner, cur_corner;
    std::vector <cv::Point2f> prev_corner2, cur_corner2;
    std::vector <uchar> status;
    std::vector <float> err;

    cv::Mat stabRoiColor;
    // End of Video Stabilizator
    

};


#endif // TRAFFICLIGHTDETECTORSINGLEMODAL_H
