#ifndef PRESENCEDETECTORSINGLEMODAL_H
#define PRESENCEDETECTORSINGLEMODAL_H

#include <QDateTime>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <QList>
#include <QDateTime>

#include "abstractsinglemodaldetector.h"

#ifdef _WIN32
#include "detectors/subsense/litiv/video.hpp"
#else
#include "subsense/BackgroundSubtractorSuBSENSE.h"
#endif

#include "detectoroutputstruct.h"
#include "ChartStruct.h"
#include "framestruct.h"
#include "ruba.h"
#include "scrollingchart.h"




class ruba::PresenceDetectorSingleModal : public AbstractSingleModalDetector
{
public:
    PresenceDetectorSingleModal(BaseDetectorParams baseParams, int minCoveredArea, cv::Mat mask, 
        cv::Mat backImage, bool usechart);

    ~PresenceDetectorSingleModal();

    void InitDetector();

    std::vector<QString*>  GetChartTitles();

private:
    void updateDetectorSpecifics();
    QList<QString> getDetectorSpecificDebugInfo(void);

    int minAreaCovered;
    int maskArea;

    BackgroundSubtractorSuBSENSE bgs;
    cv::Mat backImage;

    double chartHeightMultiplier;

    cv::Rect ROI;
};

#endif // PRESENCEDETECTORSINGLEMODAL_H
