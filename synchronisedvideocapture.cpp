#include "synchronisedvideocapture.h"

using namespace std;
using namespace cv;
using namespace ruba;

ruba::AbstractSynchronisedVideoCapture::AbstractSynchronisedVideoCapture(QWidget * parent, QList<ViewInfo> views)
    : RubaBaseObject(parent, views)
{
    prevStatus = CaptureStatus::OK;
}

ruba::AbstractSynchronisedVideoCapture::AbstractSynchronisedVideoCapture(QWidget * parent, cv::FileStorage & fs, std::string entryPrefix)
    : RubaBaseObject(parent, fs, entryPrefix)
{
    prevStatus = CaptureStatus::OK;
}

//! Saves the current configuration to the OpenCV FileStorage provided by fs
int ruba::AbstractSynchronisedVideoCapture::saveToFile(cv::FileStorage & fs, const std::string& entryPrefix)
{
    int retVal = saveViewsToFile(fs, entryPrefix);
    retVal += saveSpecificParametersToFile(fs, entryPrefix);

    return retVal;
}


QDateTime ruba::AbstractSynchronisedVideoCapture::getCurrentTime() const
{
	if (!frames.empty())
	{
		return frames[0].Timestamp;
	}
	else {
		return QDateTime();
	}


}

//! Show the video properties of the current synchronised video
/*!
    Creates a QDialog that is shown to the user with both general and 
    specific information of the current video.
    Calls its child classes with getSpecificVideoProperties to get additional information
*/
int ruba::AbstractSynchronisedVideoCapture::showVideoProperties()
{
    QGroupBox *generalGroup = new QGroupBox(tr("General"));

    QDateTime startTime = getStartTime();
	QLabel *startTimeInfo = new QLabel();

    if (startTime.isValid())
	{
		startTimeInfo->setText(startTime.toString(
			"dd-MM-yyyy HH:mm:ss.zzz"));
	}
	else {
		startTimeInfo->setText(tr("Not available"));
	}

    QLabel *endTimeInfo = new QLabel();
    QDateTime estimatedEndTime = getEndTime();

    if (startTime.msecsTo(estimatedEndTime) > 0)
    {
        // The estimated end time is after the start time. Good
        endTimeInfo->setText(estimatedEndTime.toString(
            "dd-MM-yyyy HH:mm:ss.zzz"));
    }
    else {
        // The estimated end time is before or equal to the start time. 
        // Something is wrong
        endTimeInfo->setText(tr("Not available"));
    }

    QLabel *videoTypeInfo = new QLabel();
    QLabel *syncInfo = new QLabel();
    
    switch (syncType)
    {
    case CAPTUREFROMSTREAM:
    {
        videoTypeInfo->setText(tr("Stream"));
        syncInfo->setText(tr("Continuous"));
        break;
    }
    case TIMESTAMPSINLOGFILE:
    {
        videoTypeInfo->setText(tr("File"));
        syncInfo->setText(tr("Time stamps in log file"));
        break;
    }
    case STARTTIMEINFILENAME:
    {
        videoTypeInfo->setText(tr("File"));
        syncInfo->setText(tr("Start time in file name"));
        break;
    }
    default:
    {
        videoTypeInfo->setText(tr("Undefined"));
        syncInfo->setText(tr("Undefined"));
    }
    }

    // Layout of the "general" group
    QFormLayout *generalLayout = new QFormLayout();
    generalLayout->addRow(tr("Start time"), startTimeInfo);
    generalLayout->addRow(tr("End time"), endTimeInfo);
    generalLayout->addRow(tr("Video type"), videoTypeInfo);
    generalLayout->addRow(tr("Synchronisation"), syncInfo);

    QList<InfoLabel> addionalGeneralProperties = getAdditionalGeneralProperties();

    for (auto i = 0; i < addionalGeneralProperties.size(); ++i)
    {
        generalLayout->addRow(addionalGeneralProperties[i].info,
            addionalGeneralProperties[i].label);
    }

    generalGroup->setLayout(generalLayout);

    // Main layout
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(generalGroup);

    // Get specific properties from the child classes
    QList<QWidget*> specificProperties = getSpecificVideoProperties();

    for (auto i = 0; i < specificProperties.size(); ++i)
    {
        mainLayout->addWidget(specificProperties[i]);
    }

    VideoInformationDialog infoDialog(getParent(), mainLayout);
    infoDialog.exec();

    return 0;
}


/*! Constructor to use video capture from video files that are synchronised explicitly. 
    The frame rate of the video and the file name are used to deduct the time stamp of each frame
*/
ruba::SynchronisedVideoCaptureFromFile::SynchronisedVideoCaptureFromFile(
    QWidget* parent, QList<ViewInfo> views, QList<InitFileProperties>& properties)
    : AbstractSynchronisedVideoCapture(parent, views)
{
    dateTimeFormat = -1;
    userDefinedYear = -1;
    frameNbr = -1;
    syncType = VideoSyncType::STARTTIMEINFILENAME;
    prevStatus = CaptureStatus::OK;
    
    initialise(properties);


}

ruba::SynchronisedVideoCaptureFromFile::SynchronisedVideoCaptureFromFile(QWidget * parent, 
    cv::FileStorage & fs, std::string entryPrefix)
    : AbstractSynchronisedVideoCapture(parent, fs, entryPrefix)
{
    dateTimeFormat = -1;
    userDefinedYear = -1;
    frameNbr = -1;
    syncType = VideoSyncType::STARTTIMEINFILENAME;
    prevStatus = CaptureStatus::OK;

    loadFromFile(fs, entryPrefix);
}

int ruba::SynchronisedVideoCaptureFromFile::loadFromFile(cv::FileStorage& fs, const std::string& entryPrefix)
{
    // Load the MultiViewFileProperties, if available

    if (fs.isOpened())
    {
        // Get the common parameters for all the views
        std::string maxStartTime, minEndTime;
        int duration;

        fs[entryPrefix + "-maxStartTime"] >> maxStartTime;
        fs[entryPrefix + "-minEndTime"] >> minEndTime;
        fs[entryPrefix + "-autoDetectFps"] >> fileProperties.autoDetectFps;
        fs[entryPrefix + "-videosAreSynchronised"] >> fileProperties.videosAreSynchronised;
        fs[entryPrefix + "-duration"] >> duration;

        fileProperties.maxStartTime = QDateTime::fromString(
            QString::fromStdString(maxStartTime), "yyyy-MM-dd HH-mm-ss.zzz");
        fileProperties.minEndTime = QDateTime::fromString(
            QString::fromStdString(minEndTime), "yyyy-MM-dd HH-mm-ss.zzz");
        fileProperties.duration = duration;       
        
        string videofile, startTime, endTime;
        cv::Size videoResolution;
        double fps, frameCount;
        int i = 0;

        QList<ViewInfo> views = getCurrentViews();

        // Try to get the file info of the first view
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoFile"] >> videofile;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-StartTime"] >> startTime;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-EndTime"] >> endTime;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoResolution"] >> videoResolution;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-Fps"] >> fps;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-FrameCount"] >> frameCount;

        while (!videofile.empty() && !startTime.empty() && !endTime.empty() && views.size() > i)
        {
            QApplication::processEvents();
            FileProperties prop;

            prop.videofile = QString::fromStdString(videofile);
            prop.startTime = QDateTime::fromString(QString::fromStdString(startTime), "yyyy-MM-dd HH-mm-ss.zzz");
            prop.endTime = QDateTime::fromString(QString::fromStdString(endTime), "yyyy-MM-dd HH-mm-ss.zzz");
            prop.videoResolution = videoResolution;
            prop.fps = fps;
            prop.frameCount = frameCount;
            prop.viewInfo = views[i];
            fileProperties.viewFiles.append(prop);

            FrameStruct frame;
            frame.Filename = prop.videofile;
            frame.viewInfo = prop.viewInfo;
            frames.push_back(frame);

            i++;

            // Try to fetch the following views
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoFile"] >> videofile;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-StartTime"] >> startTime;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-EndTime"] >> endTime;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoResolution"] >> videoResolution;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-Fps"] >> fps;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-FrameCount"] >> frameCount;
        }
    }

    return 0;
}

QDateTime ruba::SynchronisedVideoCaptureFromFile::getCurrentFrameTime()
{
    QDateTime startTime = fileProperties.maxStartTime;

    return startTime.addMSecs((frameNbr * 1000) / fileProperties.viewFiles[0].fps);
}

int ruba::SynchronisedVideoCaptureFromFile::saveSpecificParametersToFile(cv::FileStorage& fs, 
    const std::string& entryPrefix)
{
    // Save the multiViewFileProperties, excluding the ViewInfo. The views have been written by the
    // abstract base class AbstractSynchronisedVideoCapture before calling this method

    if (fs.isOpened())
    {
        // Save the common parameters of MultiViewFileProperties
        fs << entryPrefix + "-maxStartTime" << fileProperties.maxStartTime.toString(
            "yyyy-MM-dd HH-mm-ss.zzz").toStdString();
        fs << entryPrefix + "-minEndTime" << fileProperties.minEndTime.toString(
            "yyyy-MM-dd HH-mm-ss.zzz").toStdString();
        fs << entryPrefix + "-autoDetectFps" << fileProperties.autoDetectFps;
        fs << entryPrefix + "-videosAreSynchronised" << fileProperties.videosAreSynchronised;
        fs << entryPrefix + "-duration" << (int)fileProperties.duration;

        // Save the parameters of each view
        for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
        {
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoFile" << fileProperties.viewFiles[i].videofile.toStdString();
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-StartTime" << fileProperties.viewFiles[i].
                startTime.toString("yyyy-MM-dd HH-mm-ss.zzz").toStdString();
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-EndTime" << fileProperties.viewFiles[i].
                endTime.toString("yyyy-MM-dd HH-mm-ss.zzz").toStdString();
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoResolution" << fileProperties.viewFiles[i].videoResolution;
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-Fps" << fileProperties.viewFiles[i].fps;
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-FrameCount" << fileProperties.viewFiles[i].frameCount;
        }

        return 0;
    }
    else
    {
        return 1;
    }

}


int ruba::SynchronisedVideoCaptureFromFile::initialise(QList<InitFileProperties>& newProperties)
{
    // Step 0: Construct fileProperties

    for (auto i = 0; i < newProperties.size(); ++i)
    {
        FileProperties prop;
        prop.videofile = newProperties[i].videofile;
        prop.viewInfo = newProperties[i].viewInfo;

        fileProperties.viewFiles.append(prop);

        FrameStruct frame;
        frame.Filename = newProperties[i].videofile;
        frame.viewInfo = newProperties[i].viewInfo;
        frames.push_back(frame);
    }

    frameNbr = 0;

    if (fileProperties.viewFiles.empty())
    {
        return 1;
    }

    // Step 1: Extract start time from file
    if (newProperties.size() > 0)
    {
        QDateTime startTime;
        if (findStartTimeFromFile(newProperties[0].videofile, startTime, newProperties[0]) == 0)
        {
            for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
            {
                QApplication::processEvents();

                // We assume that all files of corresponding views have the same start time as the first file
                // provided
                fileProperties.viewFiles[i].startTime = startTime;
            }

            fileProperties.maxStartTime = startTime;
        }
    }

    // Step 2: Open VideoCapture for each file
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i) 
    {
        QApplication::processEvents();

        try {
            fileProperties.viewFiles[i].cap.open(fileProperties.viewFiles[i].videofile.toStdString());
        } 
        catch (cv::Exception e)
        {
			QString title, message;
			Utils::getWarningMessage(getParent(), e, title, message);
			sendMessage(title, message, MessageType::Warning);
        }
    }

    // Step 3: Extract resolution, frame count from file
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        QApplication::processEvents();
        cv::Size res;

        try {
            fileProperties.viewFiles[i].videoResolution.height = 
                fileProperties.viewFiles[i].cap.get(CAP_PROP_FRAME_HEIGHT);
            fileProperties.viewFiles[i].videoResolution.width = 
                fileProperties.viewFiles[i].cap.get(CAP_PROP_FRAME_WIDTH);

            fileProperties.viewFiles[i].frameCount = 
                fileProperties.viewFiles[i].cap.get(CAP_PROP_FRAME_COUNT);
            
        }
        catch (cv::Exception e)
        {
			QString title, message;
			Utils::getWarningMessage(getParent(), e, title, message);
			sendMessage(title, message, MessageType::Warning);
        }
    }

    // Step 4: Extract frame rate from file
    int retVal = getFrameRateFromFiles();

    if (retVal != 0)
    {
        return 1;
    }

    // Step 6: Get end time from file and calculate duration
    fileProperties.minEndTime = fileProperties.maxStartTime.addMSecs(
        (fileProperties.viewFiles[0].frameCount * 1000) / fileProperties.viewFiles[0].fps);

    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        QApplication::processEvents();
        fileProperties.viewFiles[i].endTime = fileProperties.minEndTime;
    }
    fileProperties.duration = fileProperties.maxStartTime.msecsTo(fileProperties.minEndTime);

    // Step 7: Close VideoCapture for each file
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        QApplication::processEvents();

        try {
            fileProperties.viewFiles[i].cap.release();
        }
        catch (cv::Exception e)
        {
			QString title, message;
			Utils::getWarningMessage(getParent(), e, title, message);
			sendMessage(title, message, MessageType::Warning);
        }
    }

    return 0;
}

//! Private: Find the start time and date from the filename using regular expressions and specified date and time formats. If necessary, ask the user
/*!
\param filename file from which the data and time should be extracted
\param startTime returned start date and time
\param prevFilename previous file in list. If date and time format is defined as USERDEFINED_USE_END_TIME_AS_START_TIME, we use the end time of the previous file
to find the start time of the current file
\param prevDateTime start date and time of prevFilename, which is extracted prior to calling this method
*/
int ruba::SynchronisedVideoCaptureFromFile::findStartTimeFromFile(const QString& filename, QDateTime& startTime,
    InitFileProperties initProperties)
{
    // Update the dateTimeFormat from the settings
    QSettings settings;
    settings.beginGroup("video");
    dateTimeFormat = settings.value("dateTimeFormat", 0).toInt();
    settings.endGroup();

    if (initProperties.prevDateTime.isValid()) {
        userDefinedYear = initProperties.prevDateTime.date().year();
    }
    else {
        userDefinedYear = -1;
    }

    // This might require user interaction if the date and time can not be found automatically. Start a widget
    SetDateTimeDialog dtDialog(getParent(), filename, dateTimeFormat, userDefinedYear,
        false, initProperties.prevFilename, initProperties.prevDateTime);

    dtDialog.findStartTime(startTime, dateTimeFormat);
    userDefinedYear = startTime.date().year();

    // Save the dateTimeFormat to settings
    settings.beginGroup("video");
    settings.setValue("dateTimeFormat", dateTimeFormat);
    settings.endGroup();

    return 0;
}

//! Checks whether or not the provided timestamp is within the video file(s) located at index in VideoList
/*!
\param timestamp the timestamp to search for
\return 0 = timestamp is found within the provided file, 1 = timestamp was not found inside file
*/
int SynchronisedVideoCaptureFromFile::isTimestampWithinFile(QDateTime timestamp, int &requestedFrameNbr)
{
    if (fileProperties.viewFiles.empty())
    {
        return 1;
    }

    // Get start time of the file
    QDateTime startTime = fileProperties.maxStartTime;

    // Figure out if the requested datetime is earlier than the current startTime. 
    // If it is, the timespan will be negative
    qint64 timespan = startTime.msecsTo(timestamp);

    if (timespan < 0 || startTime.isNull() || timestamp.isNull())
    {
        // If the requested time is earlier than the current start time, 
        // it is definitely not within the file
        return 1;
    }

    // We now have both the start time and the frame rate of the file(s) at index. 
    // Calculate the estimated duration is ms of the file
    qint64 duration = fileProperties.duration;
    double fps = fileProperties.viewFiles[0].fps;

    // If the estimated duration of the video is larger than the calculated timespan 
    // from the startTime to the requested time, we have a match
    if (duration > timespan)
    {
        // Calculate the frame number equivalent to the requested time
        requestedFrameNbr = timespan * (fps / 1000.);

        return 0;
    }
    else {
        return 1;
    }
}

int ruba::SynchronisedVideoCaptureFromFile::getFrameRateFromFiles(bool noWarning)
{
    QSettings settings;
    settings.beginGroup("video");
    double settingsFps = settings.value("videoFps", 0).toDouble();
    settings.endGroup();

    fileProperties.videosAreSynchronised = false;

    if (settingsFps == 0) // Auto-detect
    {
        double autoDetectedFps = 0;
        vector<QString> tmpFilelist;

        for (size_t i = 0; i < fileProperties.viewFiles.size(); ++i)
        {
            // Automatically detect the frame rate from the video
            double tmpFps;

            if (fileProperties.viewFiles[i].cap.isOpened()) 
            {
                tmpFps = fileProperties.viewFiles[i].cap.get(CAP_PROP_FPS);
            }
            else {
                VideoCapture cap(fileProperties.viewFiles[i].videofile.toStdString());
                tmpFps = cap.get(CAP_PROP_FPS);
            }

            tmpFilelist.push_back(fileProperties.viewFiles[i].videofile);

            // Set the frame rate in the property pages
            fileProperties.viewFiles[i].fps = tmpFps;
            

            if (autoDetectedFps == 0)
            {
                // This is the first time we have extracted the frame rate. Update the tmpFps
                autoDetectedFps = tmpFps;
            }
            else if (autoDetectedFps != tmpFps)
            {
                // We have updated the auto-detected frame rate, and extracted a frame rate for a new modality.
                // Turns out, that the frame rates are non-equal. Return an error message regarding this.

                if (!noWarning)
                {
                    QString firstFilename = QFileInfo(tmpFilelist[0]).fileName();
                    QString currentFilename = QFileInfo(tmpFilelist[1]).fileName();

                    QString errorText = tr("The frame rates of the video files are not equal. This means that dual playback of both modalities will result"
                        " in erroneous synchronisation. \n\nPlease synchronise the videos and retry.\nAlternatively, manually set the frame rate"
                        " in Menu -> File -> Settings\n\nAuto-detected frame rates:\n"
                        "%1: \t%2 fps\n"
                        "%3: \t%4 fps").arg(firstFilename, QString::number(autoDetectedFps),
                            currentFilename, QString::number(tmpFps));
                    emit sendMessage(tr("Uneven frame rate"), errorText, MessageType::Warning);
                }

                return 1;
            }
        }

        if (autoDetectedFps > 0)
        {
            fileProperties.videosAreSynchronised = true;
        }
    }
    else {
        for (size_t i = 0; i < fileProperties.viewFiles.size(); ++i)
        {
            // Manually set the frame rate
            fileProperties.viewFiles[i].fps = settingsFps;
        }

        fileProperties.videosAreSynchronised = true;
    }

    return 0;
}

std::vector<FrameStruct> ruba::SynchronisedVideoCaptureFromFile::getCurrentFrames(CaptureStatus& status)
{
    status = prevStatus;

    return frames;
}

std::vector<FrameStruct> ruba::SynchronisedVideoCaptureFromFile::getNextFrames(CaptureStatus& status)
{
    bool success = true;
    status = CaptureStatus::OK;

    if (fileProperties.viewFiles.size() != frames.size())
    {
        frames.resize(fileProperties.viewFiles.size());
    }

    if (!fileProperties.viewFiles.empty())
    {
        // Check if the first capture is opened. If not, open all captures
        if (!fileProperties.viewFiles[0].cap.isOpened())
        {
           
            // Check if the frame rates of the files are equal
            QSettings settings;
            settings.beginGroup("video");
            double settingsFps = settings.value("videoFps", 0).toDouble();
            settings.endGroup();

            double prevFps = -1;

            if (settingsFps == 0)
            {
                fileProperties.autoDetectFps = true;
            }
            else {
                fileProperties.autoDetectFps = false;
            }
            
            for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
            {
                if (settingsFps == 0) // AUTO-detect fps
                {
                    if (prevFps >= 0 && prevFps != fileProperties.viewFiles[i].fps)
                    {
                        getFrameRateFromFiles(true); // Show warning
                    }
                    else if (fileProperties.viewFiles[i].fps > 0){
                        prevFps = fileProperties.viewFiles[i].fps;
                    }
					else {
						getFrameRateFromFiles(true); // Show warning
					}
                } 
                else 
                {
                    // Update the fps with the current settings value
                    fileProperties.viewFiles[i].fps = settingsFps;
                }
            }


            QList<QString> failedCaptureFilenames;
			for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
			{
				bool opened = false;
				int maxAttempts = 2;
				int currentAttempts = 0;

				while (!opened && currentAttempts < maxAttempts)
				{
					opened = fileProperties.viewFiles[i].cap.open(
							fileProperties.viewFiles[i].videofile.toStdString());
					currentAttempts++;
					
					if (!opened)
					{
						// Try again in 200 msecs - it might be that the disk is not ready yet
						QThread::msleep(200);
					}
				}

                if (!opened)
                {
                    failedCaptureFilenames.push_back(fileProperties.viewFiles[i].videofile);
                }
            }

            if (!failedCaptureFilenames.empty())
            {
                QString errorText = tr("Unable to open the following video files:\n");

                for (int i = 0; i < failedCaptureFilenames.size(); ++i)
                {
                    errorText.append(tr("%1\n")
                        .arg(failedCaptureFilenames[i]));
                }

                errorText.append(tr("\n\nPlease check the file path of the video and check if the video codec is supported on your computer"));

                emit sendMessage(tr("File error"), errorText, MessageType::Warning);
                success = false;
                status = VIDEONOTFOUND;
            }

            frameNbr = -1;
        }

        // The captures are already open. Read the next frames
        if (success)
        {
            for (int i = 0; i < fileProperties.viewFiles.size(); ++i)
            {
                // Get the filename of the video according to the current view
                if (fileProperties.viewFiles[i].videofile.isEmpty())
                {
                    ViewInfo view = fileProperties.viewFiles[i].viewInfo;
                    QString errorText = tr("No video files specified for view %1. \n\nPlease specify video files for all views and retry.")
                        .arg(view.viewName());
                    emit sendMessage(tr("File error"), errorText, MessageType::Warning);

                    success = false;
                }


				try {
					if (!fileProperties.viewFiles[i].cap.read(frames[i].Image))
					{
						success = false;
					}
				}
				catch (cv::Exception e)
				{
					QString title, message;
					Utils::getWarningMessage(getParent(), e, title, message);
					sendMessage(title, message, MessageType::Warning);
					status = CaptureStatus::VIDEONOTFOUND;

					success = false;
				}
            }
        }
        

        if (success)
        {
            // We have successfully read the frame. Now, update the time
            frameNbr++;
            QDateTime time = getCurrentFrameTime();

            for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
            {
                frames[i].Timestamp = time;
            }
        }
        else if (status != CaptureStatus::VIDEONOTFOUND){
            // We have likely reached the end of the video file. 
            status = CaptureStatus::NOMOREFRAMES;
        }
    }

    prevStatus = status;

    return getCurrentFrames(status);
}


//! If any video in the current list contains the requestedTime, jump to that particular video at the corresponding frame
/*!
\param requestedTime the date and time which is searched for
\return 0 = the requestedTime is found and we have jumped to the corresponding point in the video, 1 = requestedTime was not found
*/
int ruba::SynchronisedVideoCaptureFromFile::jumpToFrame(QDateTime requestedTime)
{
    int requestedFrameNumber = -1;

    int retVal = isTimestampWithinFile(requestedTime, requestedFrameNumber);

    if (retVal == 0 && requestedFrameNumber >= 0)
    {
        return jumpToFrame(requestedFrameNumber);
    }
    else {
        return 1;
    }
}

int ruba::SynchronisedVideoCaptureFromFile::jumpToFrame(int requestedFrameNumber)
{
    if (requestedFrameNumber < 0)
    {
        return 1;
    }

    // Check the requestedFrameNumber. If it is greater than the current frame number, proceed.
    // Otherwise, we need to reset the video to go to the requested position
    bool resetVideo = false;

    if (requestedFrameNumber < frameNbr)
    {
        resetVideo = true;
    }

    // Check if videos are opened
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        if (!fileProperties.viewFiles[i].cap.isOpened())
        {
            QApplication::processEvents();
            fileProperties.viewFiles[i].cap.open(fileProperties.viewFiles[i].videofile.toStdString());
            
            // Reset the frame number
            frameNbr = -1;
        }
    }

    if (resetVideo)
    {
        for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
        {
            // Because OpenCV's VideoCapture set(CAP_PROP_POS_FRAMES) method is not accurate
            // on all video formats, we need to release the video and re-open it to 
            // make sure that we start at frame 0
            QApplication::processEvents();

            fileProperties.viewFiles[i].cap.release();
            fileProperties.viewFiles[i].cap.open(fileProperties.viewFiles[i].videofile.toStdString());
        }

        frameNbr = -1;
    }    

    try {
        int initialFrameNbr = frameNbr;

		int frameDiff = requestedFrameNumber - frameNbr;

        while (frameNbr < requestedFrameNumber)
        {
			int displayFrameNbr = (frameNbr > 0) ? frameNbr : 0;
			int displayRequestedFrameNumber = (requestedFrameNumber > 0) ? requestedFrameNumber : 0;

			emit sendProgress(frameNbr - initialFrameNbr, frameDiff, QObject::tr("Retrieving frames (frame %1 of %2)")
				.arg(displayFrameNbr)
				.arg(displayRequestedFrameNumber));

            //if (progress.wasCanceled())
            //{
            //    // Retrieve the current frame, and return
            //    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
            //    {
            //        fileProperties.viewFiles[i].cap.retrieve(frames[i].Image);
            //        frames[i].Timestamp = getCurrentFrameTime();

            //    }
            //        
            //    return prevStatus;
            //}

            for (auto viewFile : fileProperties.viewFiles)
            {
                if (!viewFile.cap.grab())
                {
                    prevStatus = CaptureStatus::NOMOREFRAMES;
                    break;
                }
            }

            frameNbr++;
        }
    }
    catch (cv::Exception e)
    {
		QString title, message;
		Utils::getWarningMessage(getParent(), e, title, message);
		sendMessage(title, message, MessageType::Warning);
        return 1;
    }

    if (prevStatus == CaptureStatus::OK)
    {
        // We have reached the desired frame. Now, read it
        for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
        {
            fileProperties.viewFiles[i].cap.retrieve(frames[i].Image);
            frames[i].Timestamp = getCurrentFrameTime();
            
        }
    }

    return 0;

}

QList<QWidget*> ruba::SynchronisedVideoCaptureFromFile::getSpecificVideoProperties() const
{
    QList<QWidget*> specificProperties;

    // Generate information of each view
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        QString viewName = tr("View %1")
            .arg(fileProperties.viewFiles[i].viewInfo.viewName());
        QGroupBox *viewGroup = new QGroupBox(viewName);

        // File name
        QLabel *fileNameInfo = new QLabel(
            QFileInfo(fileProperties.viewFiles[i].videofile).fileName());

        // Frame rate
        QLabel *frameRateInfo = new QLabel(
            tr("%1 frames/sec").arg(fileProperties.viewFiles[i].fps));

        // Image width
        QLabel *imageWidthInfo = new QLabel(QString::number(
            fileProperties.viewFiles[i].videoResolution.width));

        // Image height
        QLabel *imageHeightInfo = new QLabel(QString::number(
            fileProperties.viewFiles[i].videoResolution.height));

        // Layout of the group
        QFormLayout *viewLayout = new QFormLayout();
        viewLayout->addRow(tr("File name"), fileNameInfo);
        viewLayout->addRow(tr("Frame rate"), frameRateInfo);
        viewLayout->addRow(tr("Image width"), imageWidthInfo);
        viewLayout->addRow(tr("Image height"), imageHeightInfo);
        viewGroup->setLayout(viewLayout);

        specificProperties.append(viewGroup);
    }


    return specificProperties;
}

QList<InfoLabel> ruba::SynchronisedVideoCaptureFromFile::getAdditionalGeneralProperties() const
{
    QLabel *frameRateDetectionInfo = new QLabel();

    if (fileProperties.autoDetectFps)
    {
        frameRateDetectionInfo->setText(tr("Auto-detect"));
    }
    else {
        frameRateDetectionInfo->setText(tr("Manual"));
    }

    InfoLabel info1;
    info1.info = tr("Frame rate detection");
    info1.label = frameRateDetectionInfo;

    QLabel *frameRateInfo = new QLabel();
    if (fileProperties.videosAreSynchronised && !fileProperties.viewFiles.empty())
    {
        frameRateInfo->setText(tr("%1 frames/sec").arg(fileProperties.viewFiles[0].fps));
    }
    else {
        frameRateInfo->setText(tr("Not synchronised"));
    }

    InfoLabel info2;
    info2.info = tr("Frame rate");
    info2.label = frameRateInfo;
    
    QList<InfoLabel> additionalProperties{ info1, info2 };


    return additionalProperties;
}

int ruba::SynchronisedVideoCaptureFromFile::viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews)
{
    return 0;
}

QList<VideoResolutionProperties> ruba::SynchronisedVideoCaptureFromFile::getVideoResolution() const
{
    QList<VideoResolutionProperties> resolutionProperties;

    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        VideoResolutionProperties resProp;
        resProp.resolution = fileProperties.viewFiles[i].videoResolution;
        resProp.videoPath = fileProperties.viewFiles[i].videofile;

        resolutionProperties.append(resProp);
    }

    return resolutionProperties;
}

double ruba::SynchronisedVideoCaptureFromFile::getFrameRate() const
{
    if (fileProperties.viewFiles.empty())
    {
        return 0.0;
    }

    return fileProperties.viewFiles[0].fps;
}

qint64 ruba::SynchronisedVideoCaptureFromFile::getFrameNumberAtEndTime() const
{
    if (!fileProperties.viewFiles.isEmpty())
    {
        return (qint64)fileProperties.viewFiles[0].frameCount;
    }
    else {
        return qint64();
    }
}

int ruba::SynchronisedVideoCaptureFromFile::releaseVideo()
{
    // Release all videos in the list
    try 
    {
        for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
        {
            fileProperties.viewFiles[i].cap.release();
        }
    }
    catch (cv::Exception e)
    {
        qDebug() << e.what();
        return 1;
    }

    return 0;
}


/*! Constructor to use video capture from video files with timestamps in a separate file

*/
ruba::SynchronisedVideoCaptureFromFileTimestamps::SynchronisedVideoCaptureFromFileTimestamps(
    QWidget* parent, QList<ViewInfo> views, QList<InitFilePropertiesTimestamps> properties)
    : AbstractSynchronisedVideoCapture(parent, views)
{
    syncType = VideoSyncType::TIMESTAMPSINLOGFILE;
    videoIsReleased = false;

    int retVal = initialise(properties);

    if (retVal == 0)
    {
        prevStatus = CaptureStatus::OK;
    }
    else {
        prevStatus = CaptureStatus::SYNCHRONISATIONERROR;
    }
}

ruba::SynchronisedVideoCaptureFromFileTimestamps::SynchronisedVideoCaptureFromFileTimestamps(
    QWidget * parent, cv::FileStorage & fs, std::string entryPrefix)
    : AbstractSynchronisedVideoCapture(parent, fs, entryPrefix)
{
    syncType = VideoSyncType::TIMESTAMPSINLOGFILE;
    videoIsReleased = false;
    
    int retVal = loadFromFile(fs, entryPrefix);

    if (retVal == 0)
    {
        prevStatus = CaptureStatus::OK;
    }
    else {
        prevStatus = CaptureStatus::SYNCHRONISATIONERROR;
    }

    // Initialise the frameInfo
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        SyncLog info;
        info.frameNbr = -1;
        info.timestamp = QDateTime();
        frameInfo.push_back(info);
    }
}

ruba::SynchronisedVideoCaptureFromFileTimestamps::~SynchronisedVideoCaptureFromFileTimestamps()
{
    if (!fileProperties.viewFiles.empty())
    {
        if ((syncLookupFile.fileName() != fileProperties.viewFiles.first().timestampsFile) &&
            syncLookupFile.exists())
        {
            // Delete the lookup file
            syncLookupFile.close();
            syncLookupFile.remove();
        }
    }
}

//! Private: Retrieves frame at either the requested time or the requested frame number.
/*!
    Retrieves synchronised frames from the available views at either the requested time or at the requested frame number.
    If requestedTime and requestedFrameNumber are left to their default values, we extract the next frame

    \param requestedTime time for which the requested frames should be either equal to or greater than. If default, use the requestedFrameNumber
    \param requestedFrameNumber If requestedTime is null or invalid, get the frames that equals the requestedFrameNumber
    \sa jumpToFrame, getNextFrames, getCurrentFrames
*/

CaptureStatus ruba::SynchronisedVideoCaptureFromFileTimestamps::retrieveFrameAt(QDateTime requestedTime, int requestedFrameNumber)
{
    CaptureStatus status = CaptureStatus::OK;

    if (fileProperties.viewFiles.size() != frames.size())
    {
        frames.resize(fileProperties.viewFiles.size());
    }

    if (!fileProperties.videosAreSynchronised)
    {
        return CaptureStatus::SYNCHRONISATIONERROR;
    }

    if (prevStatus == CaptureStatus::SYNCHRONISATIONERROR)
    {
        return prevStatus;
    }

    if (frameInfo.isEmpty())
    {
        return CaptureStatus::SYNCHRONISATIONERROR;
    }

    // Check if we are already at the requested time
    if (!requestedTime.isNull() && requestedTime == frameInfo[0].timestamp)
    {
        return CaptureStatus::OK;
    }

    // And make a similar check for the requested frame number
    if (requestedFrameNumber >= 0 && requestedFrameNumber == frameInfo[0].frameNbr)
    {
        return CaptureStatus::OK;
    }

    // Check the requestedTime and requestedFrameNumber. If they are larger than the current frame, proceed.
    // Otherwise, we need to reset the video to go to the requested position
    bool resetVideo = false;

    if (!requestedTime.isNull() && requestedTime < frameInfo[0].timestamp)
    {
        resetVideo = true;
    }

    if (requestedFrameNumber >= 0 && requestedFrameNumber < frameInfo[0].frameNbr)
    {
        resetVideo = true;
    }

    if (resetVideo)
    {
        for (auto i = 0; i < frameInfo.size(); ++i)
        {
            frameInfo[i].frameNbr = -1;
            frameInfo[i].timestamp = QDateTime();
        }

        for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
        {
            if (fileProperties.viewFiles[i].cap.isOpened())
            {
                // Because OpenCV's VideoCapture set(CAP_PROP_POS_FRAMES) method is not accurate
                // on all video formats, we need to release the video and re-open it to 
                // make sure that we start at frame 0

                fileProperties.viewFiles[i].cap.release();
                fileProperties.viewFiles[i].cap.open(fileProperties.viewFiles[i].videofile.toStdString());
            }
        }

        syncLookupStream.seek(0);

        // Reset the previous status
        prevStatus = CaptureStatus::OK;
    }

    if (prevStatus == CaptureStatus::NOMOREFRAMES)
    {
        // We have no more frames to extract and have chosen not to reset the video. Thus, there is nothing more to do.
        return prevStatus;
    }

    // Make sure that the requestedTime and requestedFrameNumber have usable values going forward
    if (requestedTime.isNull())
    {
        // We did not provide a usable requestedTime. Use the maxStartTime as a constraint instead.
        requestedTime = fileProperties.maxStartTime;    
    }
    else if (requestedTime >= fileProperties.maxStartTime && requestedTime <= fileProperties.minEndTime)
    {
        // We did specify a usable requestedTime. This means that the value of requestedFrameNumber 
        // is irrelevant. Thus, reset it.
        requestedFrameNumber = -1;
    }

    if (requestedFrameNumber < 0)
    {
        requestedFrameNumber = -1;
    }

    QApplication::processEvents();


    if (!fileProperties.viewFiles.empty())
    {
        // Check if the first capture is opened. If not, open all captures
        if (!fileProperties.viewFiles[0].cap.isOpened())
        {
            QList<QString> failedCaptureFilenames;
            for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
            {
                bool opened = fileProperties.viewFiles[i].cap.open(
                    fileProperties.viewFiles[i].videofile.toStdString());

                QApplication::processEvents();

                if (!opened)
                {
                    failedCaptureFilenames.push_back(fileProperties.viewFiles[i].videofile);
                }
            }

            // Generate synchronised look-up table
            generateSynchronisedLookupTable();

            // Initialise the frame information
            for (auto i = 0; i < frameInfo.size(); ++i)
            {
                frameInfo[i].frameNbr = -1;
                frameInfo[i].timestamp = QDateTime();
            }

            if (!failedCaptureFilenames.empty())
            {
                QString errorText = tr("Unable to open the following video files:\n");

                for (int i = 0; i < failedCaptureFilenames.size(); ++i)
                {
                    errorText.append(tr("%1\n")
                        .arg(failedCaptureFilenames[i]));
                }

                errorText.append(tr("\n\nPlease check the file path of the video and check if the video codec is supported on your computer"));

                emit sendMessage(tr("File error"), errorText, MessageType::Warning);
                status = VIDEONOTFOUND;
            }
        }

        // The captures are already open. Check the file information
        if (status == CaptureStatus::OK)
        {
            for (int i = 0; i < fileProperties.viewFiles.size(); ++i)
            {
                // Get the filename of the video according to the current view
                if (fileProperties.viewFiles[i].videofile.isEmpty())
                {
                    ViewInfo view = fileProperties.viewFiles[i].viewInfo;
                    QString errorText = tr("No video files specified for view %1. \n\nPlease specify video files for all views and retry.")
                        .arg(view.viewName());
					emit sendMessage(tr("File error"), errorText, MessageType::Warning);

                    status = CaptureStatus::VIDEONOTFOUND;
                }
            }
        }

        if (status == CaptureStatus::OK)
        {
            // Open the master view (the first view) and retrieve the next frame. 
            // The date time of this frame must be within the interval defined by the maximum start time and 
            // minimum end time of the combined views
            QList<SyncLog> desiredFrameInfo = frameInfo;

            status = getTimestampFromFileSyncFile(requestedTime, requestedFrameNumber, desiredFrameInfo);

            // We have now retrieved the frame numbers of the first view and corresponding (slave) views.
            // Now, get the frames from the views 
		
			if (status == CaptureStatus::OK)
			{

				for (auto i = 0; i < frameInfo.size(); ++i)
				{
					int frameDiff = desiredFrameInfo[i].frameNbr - frameInfo[i].frameNbr;

					while (desiredFrameInfo[i].frameNbr > frameInfo[i].frameNbr)
					{
						int displayFrameNbr = frameDiff - (desiredFrameInfo[i].frameNbr - frameInfo[i].frameNbr) + 1 ;
						int displayDesiredFrameNbr = frameDiff;

						emit sendProgress(displayFrameNbr, displayDesiredFrameNbr,
							QObject::tr("Retrieving frames in view %1 (frame %2 of %3)")
							.arg(fileProperties.viewFiles[i].viewInfo.viewId)
							.arg(displayFrameNbr)
							.arg(displayDesiredFrameNbr));

						// Should be re-implemented via signals interface instead
						//if (progress.wasCanceled()) 
						//{
						//    // Retrieve the frames that are grabbed, get the timestamp, and cancel
						//    syncLookupStream.seek(0); // Reset the lookup stream, as we need to go back in time to get the timestamp

						//    status = getTimestampFromFileSyncFile(QDateTime(), frameInfo[i].frameNbr, desiredFrameInfo);
						//    frameInfo = desiredFrameInfo;

						//    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
						//    {
						//        if (i < frameInfo.size())
						//        {
						//            fileProperties.viewFiles[i].cap.retrieve(frames[i].Image);
						//            frames[i].Timestamp = frameInfo[i].timestamp;
						//        }
						//    }

						//    return status;
						//}

						// Read the next frame of view i
						if (!fileProperties.viewFiles[i].cap.grab())
						{
							status = CaptureStatus::NOMOREFRAMES;
							break;
						}

						// Update the current frame number
						frameInfo[i].frameNbr++;
					}
					// Otherwise, we have already read the corresponding frame
					// It resides in the frames vector, so nothing more to do right now
				}
			}

            // If we have got this far, the desired frames have been extracted. Thus, 
            // replace the frameInfo with the desiredFrameInfo, and update the information
            // in the frameStruct
            if (status == CaptureStatus::OK)
            {
                frameInfo = desiredFrameInfo;

                for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
                {
                    if (i < frameInfo.size())
                    {
                        fileProperties.viewFiles[i].cap.retrieve(frames[i].Image);
                        frames[i].Timestamp = frameInfo[i].timestamp;
                    }
                }
            }

        }
    }

    // If the video has been released in an event loop since we called the method, return a proper code
    if (videoIsReleased)
    {
        status = CaptureStatus::VIDEOISRELEASED;
    }

    prevStatus = status;
    return status;
}

CaptureStatus ruba::SynchronisedVideoCaptureFromFileTimestamps::getTimestampFromFileSyncFile(QDateTime & requestedTime, int requestedFrameNumber, QList<SyncLog>& desiredFrameInfo)
{
    CaptureStatus status = CaptureStatus::OK;

    if (desiredFrameInfo.size() < 1)
    {
        return CaptureStatus::SYNCHRONISATIONERROR;
    }

    desiredFrameInfo[0].timestamp = fileProperties.maxStartTime.addYears(-1);
    desiredFrameInfo[0].frameNbr = -1;
    
    while (desiredFrameInfo[0].timestamp < requestedTime ||
        desiredFrameInfo[0].frameNbr < requestedFrameNumber)
    {
        // Inform the user that we are still running, even if it might take some time
        QApplication::processEvents();

        // Read the synchronisation information to get the frame numbers of corresponding views 
        // that corresponds to the frame of the master view
        if (!syncLookupStream.atEnd())
        {
            QString csvLine = syncLookupStream.readLine();

            QList<SyncLog> retrievedFrameInfo = getFrameInfoFromString(csvLine);

            if (retrievedFrameInfo.size() == frameInfo.size())
            {
                desiredFrameInfo = retrievedFrameInfo;
            }
            else {
                status = CaptureStatus::SYNCHRONISATIONERROR;
                break;
            }
        }
        else {
            status = CaptureStatus::NOMOREFRAMES;
            break;
        }

        if (desiredFrameInfo[0].timestamp > fileProperties.minEndTime)
        {
            // We have reached the end of the synchronised video. Inform the video handler
            qDebug() << "Desired frame info:" << desiredFrameInfo[0].timestamp.toString("yyyy MM dd hh:mm:ss.zzz");
            qDebug() << "Minimum end time:" << fileProperties.minEndTime.toString("yyyy MM dd hh:mm:ss.zzz");

            status = CaptureStatus::NOMOREFRAMES;
            break;
        }
    }

    return status;
}



std::vector<FrameStruct> ruba::SynchronisedVideoCaptureFromFileTimestamps::getCurrentFrames(CaptureStatus & status)
{
    status = prevStatus;

    return frames;
}

std::vector<FrameStruct> ruba::SynchronisedVideoCaptureFromFileTimestamps::getNextFrames(CaptureStatus & status)
{
    videoIsReleased = false; // Reset the flag

    status = retrieveFrameAt(QDateTime(), -1); // Get the next frame

    return getCurrentFrames(status);
}

int ruba::SynchronisedVideoCaptureFromFileTimestamps::jumpToFrame(QDateTime requestedTime)
{
    if (requestedTime >= fileProperties.maxStartTime && requestedTime <= fileProperties.minEndTime)
    {
        videoIsReleased = false; // Reset the flag
        
        CaptureStatus status = retrieveFrameAt(requestedTime, -1);

        if (status == CaptureStatus::OK)
        {
            return 0;
        }
        else {
            return 1;
        }
    }
    else {
        // The requested time was not in the specified range
        return 1;
    }
}

int ruba::SynchronisedVideoCaptureFromFileTimestamps::jumpToFrame(int requestedFrameNumber)
{
    if (requestedFrameNumber >= 0)
    {
        videoIsReleased = false; // Reset the flag
        
        CaptureStatus status = retrieveFrameAt(QDateTime(), requestedFrameNumber);
        
        if (status == CaptureStatus::OK)
        {
            return 0;
        }
        else {
            return 1;
        }
    }
    else 
    {
        // The requested frame number was invalid
        return 1;
    }

}

qint64 ruba::SynchronisedVideoCaptureFromFileTimestamps::getFrameNumber() const
{
    if (frameInfo.empty())
    {
        return -1;
    }
    else {
        return frameInfo[0].frameNbr;
    }
}

int ruba::SynchronisedVideoCaptureFromFileTimestamps::releaseVideo()
{
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        try {
            fileProperties.viewFiles[i].cap.release();
        }
        catch (cv::Exception e)
        {
			QString title, message;
			Utils::getWarningMessage(getParent(), e, title, message);
			sendMessage(title, message, MessageType::Warning);
        }
    }

    syncLookupFile.close();

    if (!fileProperties.viewFiles.empty())
    {
        if ((syncLookupFile.fileName() != fileProperties.viewFiles.first().timestampsFile) && 
            syncLookupFile.exists())
        {
            // Delete the lookup file
            syncLookupFile.remove();
        }
    }
    

    // Reset the frame numbers
    for (auto i = 0; i < frameInfo.size(); ++i)
    {
        frameInfo[i].frameNbr = -1;
        frameInfo[i].timestamp = QDateTime();
    }

    // Reset the status
    prevStatus = CaptureStatus::OK;
    videoIsReleased = true;

    return 0;
}

QList<VideoResolutionProperties> ruba::SynchronisedVideoCaptureFromFileTimestamps::getVideoResolution() const
{
    QList<VideoResolutionProperties> resProps;

    for (auto viewFile : fileProperties.viewFiles)
    {
        VideoResolutionProperties res;
        res.resolution = viewFile.videoResolution;
        res.videoPath = viewFile.videofile;

        resProps.push_back(res);
    }

    return resProps;
}

double ruba::SynchronisedVideoCaptureFromFileTimestamps::getFrameRate() const
{
	if (!fileProperties.viewFiles.isEmpty())
	{
		return fileProperties.viewFiles[0].fps;
	}

	return 0.;
}


QList<QWidget*> ruba::SynchronisedVideoCaptureFromFileTimestamps::getSpecificVideoProperties() const
{
    QList<QWidget*> specificProperties;

    // Generate information of each view
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        QString viewName = tr("View %1")
            .arg(fileProperties.viewFiles[i].viewInfo.viewName());

        QGroupBox *viewGroup = new QGroupBox(viewName);

        // File name
        QLabel *fileNameInfo = new QLabel(
            QFileInfo(fileProperties.viewFiles[i].videofile).fileName());

        // Image width
        QLabel *imageWidthInfo = new QLabel(QString::number(
            fileProperties.viewFiles[i].videoResolution.width));

        // Image height
        QLabel *imageHeightInfo = new QLabel(QString::number(
            fileProperties.viewFiles[i].videoResolution.height));

        // Log file
        QLabel *logFileInfo = new QLabel(
            QFileInfo(fileProperties.viewFiles[i].timestampsFile).fileName());

        // Start time
        QLabel *startTimeInfo = new QLabel(fileProperties.viewFiles[i].
            startTime.toString("dd-MM-yyyy HH:mm:ss.zzz"));

        // End time
        QLabel *endTimeInfo = new QLabel(fileProperties.viewFiles[i].
            endTime.toString("dd-MM-yyyy HH:mm:ss.zzz"));

        QLabel *currentFrameNbr = NULL;
        QLabel *currentFrameTime = NULL;

        // Current frame number and time, if the video is opened
        if (!frameInfo.empty() && frameInfo[0].frameNbr >= 0)
        {
            if (i < frameInfo.size())
            {
                currentFrameNbr = new QLabel(QString::number(frameInfo[i].frameNbr));
                currentFrameTime = new QLabel(frameInfo[i].timestamp.toString("dd-MM-yyyy HH:mm:ss.zzz"));
            }
        }

        // Layout of the group
        QFormLayout *viewLayout = new QFormLayout();
        viewLayout->addRow(tr("File name"), fileNameInfo);
        viewLayout->addRow(tr("Image width"), imageWidthInfo);
        viewLayout->addRow(tr("Image height"), imageHeightInfo);
        viewLayout->addRow(tr("Log file"), logFileInfo);
        viewLayout->addRow(tr("Start time of view"), startTimeInfo);
        viewLayout->addRow(tr("End time of view"), endTimeInfo);

        if (currentFrameNbr != NULL)
        {
            viewLayout->addRow(tr("Current frame number"), currentFrameNbr);
            viewLayout->addRow(tr("Current frame time"), currentFrameTime);
        }

        viewGroup->setLayout(viewLayout);

        specificProperties.append(viewGroup);
    }


    return specificProperties;
}

QList<InfoLabel> ruba::SynchronisedVideoCaptureFromFileTimestamps::getAdditionalGeneralProperties() const
{
    // Currently, the master view is hard coded as view 1

    QLabel *masterViewInfo = new QLabel();
    QList<ViewInfo> currentViews = getCurrentViews();


    if (currentViews.empty())
    {
        masterViewInfo->setText("No view enabled");
    }
    else {
        masterViewInfo->setText(tr("View %1")
            .arg(currentViews[0].viewName()));
    }

    InfoLabel info1;
    info1.info = tr("Master view");
    info1.label = masterViewInfo;

    QList<InfoLabel> additionalProperties{ info1 };


    return additionalProperties;
}

int ruba::SynchronisedVideoCaptureFromFileTimestamps::viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews)
{
    // We deliberately don't respond to changes in the views
    return 0;
}

int ruba::SynchronisedVideoCaptureFromFileTimestamps::saveSpecificParametersToFile(cv::FileStorage & fs, const std::string& entryPrefix)
{
    // Save the multiViewFileProperties, excluding the ViewInfo. The views have been written by the
    // abstract base class AbstractSynchronisedVideoCapture before calling this method

    if (fs.isOpened())
    {
        // Save the common parameters of MultiViewFilePropertiesTimestamps
        fs << entryPrefix + "-maxStartTime" << fileProperties.maxStartTime.toString(
            "yyyy-MM-dd HH-mm-ss.zzz").toStdString();
        fs << entryPrefix + "-minEndTime" << fileProperties.minEndTime.toString(
            "yyyy-MM-dd HH-mm-ss.zzz").toStdString();
        fs << entryPrefix + "-videosAreSynchronised" << fileProperties.videosAreSynchronised;
        fs << entryPrefix + "-duration" << (int)fileProperties.duration;
        fs << entryPrefix + "-frameNumberAtMinEndTime" << (int)fileProperties.frameNumberAtMinEndTime;

        // Save the parameters of each view
        for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
        {
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoFile" << fileProperties.viewFiles[i].videofile.toStdString();
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-StartTime" << fileProperties.viewFiles[i].
                startTime.toString("yyyy-MM-dd HH-mm-ss.zzz").toStdString();
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-EndTime" << fileProperties.viewFiles[i].
                endTime.toString("yyyy-MM-dd HH-mm-ss.zzz").toStdString();
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoResolution" << fileProperties.viewFiles[i].videoResolution;
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-Fps" << fileProperties.viewFiles[i].fps;
            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-FrameCount" << fileProperties.viewFiles[i].frameCount;

            fs << entryPrefix + "-ViewFile-" + std::to_string(i) + "-TimestampsFile" << fileProperties.viewFiles[i].
                timestampsFile.toStdString();
        }

        return 0;
    }
    else
    {
        return 1;
    }
}

int ruba::SynchronisedVideoCaptureFromFileTimestamps::loadFromFile(cv::FileStorage& fs, const std::string& entryPrefix)
{
    // Load the MultiViewFilePropertiesTimestamps, if available

    if (fs.isOpened())
    {
        // Get the common parameters for all the views
        std::string maxStartTime, minEndTime;
        int duration, frameNumberAtMinEndTime;

        fs[entryPrefix + "-maxStartTime"] >> maxStartTime;
        fs[entryPrefix + "-minEndTime"] >> minEndTime;
        fs[entryPrefix + "-videosAreSynchronised"] >> fileProperties.videosAreSynchronised;
        fs[entryPrefix + "-duration"] >> duration;
        fs[entryPrefix + "-frameNumberAtMinEndTime"] >> frameNumberAtMinEndTime;

        fileProperties.maxStartTime = QDateTime::fromString(
            QString::fromStdString(maxStartTime), "yyyy-MM-dd HH-mm-ss.zzz");
        fileProperties.minEndTime = QDateTime::fromString(
            QString::fromStdString(minEndTime), "yyyy-MM-dd HH-mm-ss.zzz");
        fileProperties.duration = duration;
        fileProperties.frameNumberAtMinEndTime = frameNumberAtMinEndTime;

        if (fileProperties.maxStartTime.isNull() || fileProperties.minEndTime.isNull())
        {
            return 1;
        }

        string videofile, startTime, endTime, timestampsFile;
        cv::Size videoResolution;
        double fps;
        int i = 0;
        double frameCount;

        QList<ViewInfo> views = getCurrentViews();

        // Try to get the file info of the first view
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoFile"] >> videofile;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-StartTime"] >> startTime;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-EndTime"] >> endTime;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoResolution"] >> videoResolution;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-TimestampsFile"] >> timestampsFile;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-Fps"] >> fps;
        fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-FrameCount"] >> frameCount;

        while (!videofile.empty() && !startTime.empty() && !endTime.empty() && views.size() > i)
        {
            FilePropertiesTimestamps prop;
            QApplication::processEvents();

            prop.videofile = QString::fromStdString(videofile);
            prop.startTime = QDateTime::fromString(QString::fromStdString(startTime), "yyyy-MM-dd HH-mm-ss.zzz");
            prop.endTime = QDateTime::fromString(QString::fromStdString(endTime), "yyyy-MM-dd HH-mm-ss.zzz");
            prop.videoResolution = videoResolution;
            prop.timestampsFile = QString::fromStdString(timestampsFile);
            prop.fps = fps;
            prop.frameCount = frameCount;
            prop.viewInfo = views[i];
            fileProperties.viewFiles.append(prop);

            FrameStruct frame;
            frame.Filename = prop.videofile;
            frame.viewInfo = prop.viewInfo;
            frames.push_back(frame);

            if (prop.startTime.isNull() || prop.endTime.isNull())
            {
                return 1;
            }

            i++;

            // Try to fetch the following views
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoFile"] >> videofile;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-StartTime"] >> startTime;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-EndTime"] >> endTime;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-VideoResolution"] >> videoResolution;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-TimestampsFile"] >> timestampsFile;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-Fps"] >> fps;
            fs[entryPrefix + "-ViewFile-" + std::to_string(i) + "-FrameCount"] >> frameCount;

        }
    }

    return 0;
}


int ruba::SynchronisedVideoCaptureFromFileTimestamps::initialise(QList<InitFilePropertiesTimestamps> newProperties)
{
    // Step 1: Construct fileProperties

    for (auto i = 0; i < newProperties.size(); ++i)
    {
        FilePropertiesTimestamps prop;
        prop.videofile = newProperties[i].videofile;
        prop.viewInfo = newProperties[i].viewInfo;
        prop.timestampsFile = newProperties[i].timestampsFile;

        fileProperties.viewFiles.append(prop);

        FrameStruct frame;
        frame.Filename = newProperties[i].videofile;
        frame.viewInfo = newProperties[i].viewInfo;
        frames.push_back(frame);
    }

    // Step 2: Extract start and stop time for each view 
    // and the common maxStartTime and minEndTime
    QString brokenLogFiles;
    fileProperties.videosAreSynchronised = true;
    fileProperties.maxStartTime = QDateTime();
    fileProperties.minEndTime = QDateTime();

    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        // Open the log file for each view
        if (fileProperties.viewFiles[i].timestampsFile ==
            fileProperties.viewFiles[i].videofile)
        {
            // Either invalid video or log file. Return
            return 1;
        }


        QFile file(fileProperties.viewFiles[i].timestampsFile);

        if (!file.open(QIODevice::ReadOnly))
        {
            brokenLogFiles += tr("%1\n").arg(fileProperties.viewFiles[i].timestampsFile);
            continue;
        }

        QTextStream in(&file);
        
        // Get the start time
        QList<SyncLog> startSync = getFrameInfoFromString(in.readLine());
        QDateTime startTime = startSync.empty() ? QDateTime() : startSync[0].timestamp;

        // Get the number of rows in the log file, i.e. the number of time stamps
        int rowCount = 1;
        QString line;

        while (!in.atEnd()) {
            line = in.readLine();
            rowCount++;
        }

        QString lastLine = in.readLine();
        lastLine = lastLine.isEmpty() ? line : lastLine;

        QList<SyncLog> endSync = getFrameInfoFromString(lastLine);
        QDateTime endTime = endSync.empty() ? QDateTime() : endSync[0].timestamp;

        file.close();


        if (startTime.msecsTo(endTime) < 0)
        {
            // End time is before start time. 
            // Set the times  equal and issue a warning below
            endTime = startTime;
        }

        // Set the end and start time in the properties
        fileProperties.viewFiles[i].startTime = startTime;
        fileProperties.viewFiles[i].endTime = endTime;
        fileProperties.viewFiles[i].frameCount = rowCount;
            
        if (startTime == endTime)
        {
            emit sendMessage(tr("Error reading time stamps"),
                tr("Could not retrieve the start and end time of the video using the log file. "
                    "Please check the log file and retry.\n\n"
                    "Video file: \n%1\n"
                    "Log file with timestamps: \n%2\n\n"
                    "First time stamp in file: %3\n"
                    "Last time stamp in file: %4\n")
                .arg(fileProperties.viewFiles[i].videofile)
                .arg(fileProperties.viewFiles[i].timestampsFile)
                .arg(startTime.toString("dd-MM-yyyy HH:mm:ss"))
                .arg(startTime.toString("dd-MM-yyyy HH:mm:ss")),
                MessageType::Warning);

            return 1;
        }


        // Update the maxStartTime and minEndTime if necessary
        if (fileProperties.maxStartTime.isNull())
        {
            fileProperties.maxStartTime = startTime;
        }

        if (fileProperties.minEndTime.isNull())
        {
            fileProperties.minEndTime = endTime;
            fileProperties.frameNumberAtMinEndTime = rowCount;
        }


        if (fileProperties.maxStartTime.msecsTo(startTime) > 0)
        {
            // Start time is after maxStartTime. 
            fileProperties.maxStartTime = startTime;
        }

        if (fileProperties.minEndTime.msecsTo(endTime) < 0)
        {
            // End time is before minEndTime
            fileProperties.minEndTime = endTime;
            fileProperties.frameNumberAtMinEndTime = rowCount;
        }

    }

    if (!brokenLogFiles.isEmpty())
    {
        // We were having trouble reading some of the log files. Issue a warning and 
        // handle the error gracefully
        fileProperties.videosAreSynchronised = false;

		emit sendMessage(tr("Error reading time stamps"),
			tr("Could not open the following log files:\n%1")
			.arg(brokenLogFiles), MessageType::Warning);

        return 1;
    }

    // Step 3: Calculate the common duration of the times
    fileProperties.duration = fileProperties.maxStartTime.msecsTo(fileProperties.minEndTime);


    // Step 4: Open VideoCapture for each file
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        try {
            fileProperties.viewFiles[i].cap.open(fileProperties.viewFiles[i].videofile.toStdString());
        }
        catch (cv::Exception e)
        {
			QString title, message;
			Utils::getWarningMessage(getParent(), e, title, message);
			sendMessage(title, message, MessageType::Warning);
        }
    }

    // Step 5: Extract resolution and frame rate
    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        cv::Size res;

        try {
            fileProperties.viewFiles[i].videoResolution.height =
                fileProperties.viewFiles[i].cap.get(CAP_PROP_FRAME_HEIGHT);
            fileProperties.viewFiles[i].videoResolution.width =
                fileProperties.viewFiles[i].cap.get(CAP_PROP_FRAME_WIDTH);
            fileProperties.viewFiles[i].fps = 
                fileProperties.viewFiles[i].cap.get(CAP_PROP_FPS);
        }
        catch (cv::Exception e)
        {
			QString title, message;
			Utils::getWarningMessage(getParent(), e, title, message);
			sendMessage(title, message, MessageType::Warning);
        }
    }

    // Step 6: Close VideoCapture for each file
    releaseVideo();

    // Step 7: Initialise the frame numbers    
    frameInfo.clear();

    for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
    {
        SyncLog info;
        info.frameNbr = -1;
        info.timestamp = QDateTime();

        frameInfo.push_back(info);
    }
   
    
    return 0;
}

int ruba::SynchronisedVideoCaptureFromFileTimestamps::generateSynchronisedLookupTable()
{
    if (fileProperties.viewFiles.empty())
    {
        return 1;
    }

	emit sendProgress(0, 0, tr("Generating synchronised lookup table"));

    if (fileProperties.viewFiles.size() == 1)
    {
        // We only have one view, which means that we can use the log file of the view as the synchronised lookup table.
        // Thus, there is no need to progress any further - we have the table already.

        syncLookupFile.setFileName(fileProperties.viewFiles.first().timestampsFile);
        syncLookupFile.open(QIODevice::ReadOnly);
        syncLookupStream.setDevice(&syncLookupFile);

        return 0;
    }
    else {
        // We have more than one view, which means that we need to write the synchronised lookup table ourselves. 
        // First, get a temporary, writeable location to save the lookup table
        QString lookupPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);

        if (!QDir(lookupPath).exists()) {
            QDir().mkpath(lookupPath);
        }

        if (fileProperties.viewFiles.size() > 1)
        {
            QDir lookupDir(lookupPath);
            QString lookupFile = QFileInfo(fileProperties.viewFiles.first().timestampsFile).baseName();
            lookupFile += QDateTime::currentDateTime().toString("hh-mm-ss-zzz") + ".csv";
            syncLookupFile.setFileName(lookupDir.absoluteFilePath(lookupFile));
            syncLookupFile.open(QIODevice::WriteOnly);
        }
    }


    syncLookupStream.setDevice(&syncLookupFile);

    // Open view 1 as the master view that controls the clock
    QFile masterView(fileProperties.viewFiles[0].timestampsFile);
    masterView.open(QIODevice::ReadOnly);
    QTextStream masterStream(&masterView);

    //// Open additional views as slave views which must follow the master clock
    std::vector<QFile*> slaveViews;
    std::vector<QTextStream*> slaveStreams;
    std::vector<qint64> frameNbrs;
    
    // For each slave stream, define a List of SyncLog objects that contains information for temporary frames:
    // time gap in ms, to the current master frame
    // time stamp, QDateTime
    // frame number
    std::vector<QList<SyncLog> > syncInfo;

    for (auto i = 1; i < fileProperties.viewFiles.size(); ++i)
    {
        slaveViews.push_back(new QFile(fileProperties.viewFiles[i].timestampsFile));
        slaveViews[i-1]->open(QIODevice::ReadOnly);
        slaveStreams.push_back(new QTextStream(slaveViews[i-1]));

        QList<SyncLog> slaveSyncInfo;
        syncInfo.push_back(slaveSyncInfo);

        frameNbrs.push_back(-1);
    }

    // Create file correspondences between the master view and slave views
    // For each frame in the master, get the corresponding frame in the slaves. The frame numbers start at 0
    // For three views, the file looks like the following:
    // View 1                            & View 2                                                              & View 3
    // timeOfFrame1InView1; frameNbr1    ; correspondingTimeOfFrameForView2 ; correspondingFrameNbrForView2    ; correspondingTimeOfFrameForView2; correspondingFrameNbrForView3
    // timeOfFrame2InView1; frameNbr2    ; correspondingTimeOfFrameForView2 ; correspondingFrameNbrForView2    ; correspondingTimeOfFrameForView2; correspondingFrameNbrForView3
    // ....                              ; ...                              ; ....                             , ....                            ; ...

    int frameNbr = -1;

    while (!masterStream.atEnd())
    {
        QList<SyncLog> masterSync = getFrameInfoFromString(masterStream.readLine());
        
        QDateTime masterTime = masterSync.empty() ? QDateTime() : masterSync[0].timestamp;
        frameNbr++;

		emit sendProgress(0, 0, QObject::tr("Synchronising frame %1").arg(frameNbr));

        if (masterTime >= fileProperties.maxStartTime)
        {
            // Only proceed if the extracted time agrees with the common, maximum start time

            QString syncRow = QString("%1").arg(frameNbr, 5, 10, QChar('0')) + " " + masterTime.toString("yyyy MM dd HH:mm:ss.zzz") + ";";

            for (int i = 0; i < slaveStreams.size(); ++i)
            {
                // Find the corresponding frame of view i
                bool correspondingFrameFound = false;

                SyncLog bestMatch;
                bestMatch.frameNbr = -1;
                bestMatch.timestamp = masterTime.addYears(1); // Hopefully, this is not the best match. Otherwise, we have other problems with the synchronisation

                while (!correspondingFrameFound && !slaveStreams[i]->atEnd())
                {
                    // Go through the previously extracted syncLogs to see 
                    // if they fit the master time
                    if (!syncInfo[i].empty())
                    {
                        bestMatch = syncInfo[i].first();
                    }

                    for (int j = 0; j < syncInfo[i].size(); ++j)
                    {
                        if (abs(masterTime.msecsTo(syncInfo[i][j].timestamp))
                            <= abs(masterTime.msecsTo(bestMatch.timestamp)))
                        {
                            bestMatch = syncInfo[i][j];
                        }
                        else {
                            // Frame j-- was a better match than frame j. This means that from now on,
                            // the time gap will only increase. Thus, use the previous frame as the best match (it is already set)
                            // and cease the search
                            correspondingFrameFound = true;
                            break;
                        }
                    }

                    if (!correspondingFrameFound)
                    {
                        // We did not find the best match from the previously extracted frames. Thus, extract the time stamp
                        // of the next frame from the log file
                        // Read the next time stamp of view i

                        QList<SyncLog> slaveSync = getFrameInfoFromString(slaveStreams[i]->readLine());
                        QDateTime timestamp = slaveSync.empty() ? QDateTime() : slaveSync[0].timestamp;

                        frameNbrs[i]++;

                        SyncLog currentFrame;
                        currentFrame.frameNbr = frameNbrs[i];
                        currentFrame.timestamp = timestamp;

                        syncInfo[i].append(currentFrame);

                        if (syncInfo[i].size() > 4)
                        {
                            // If the List contains more than four elements, use it as a queue
                            syncInfo[i].removeFirst();
                        }
                        // Otherwise, just append to the list

                        // Check if the extracted frame has a lower time gap than the previous best match
                        if (abs(masterTime.msecsTo(syncInfo[i].last().timestamp))
                            < abs(masterTime.msecsTo(bestMatch.timestamp)))
                        {
                            // The current frame of view i fits better than the previous.
                            // This means that we should continue the while loop and see if the next frame 
                            // fits even better
                            bestMatch = currentFrame;
                        }
                        else {
                            // The current frame of view i does not fit better than the previous frame. 
                            // Thus, use the current bestMatch and cease the search
                            correspondingFrameFound = true;
                        }
                    }
                }

                // Append the time and frame number of best match of view i
                syncRow += QString("%1").arg(bestMatch.frameNbr, 5, 10, QChar('0')) + " "
                    + bestMatch.timestamp.toString("yyyy MM dd HH:mm:ss.zzz") + ";";
            }

            syncLookupStream << syncRow << "\n";
        }
    }

    for (auto i = 1; i < fileProperties.viewFiles.size(); ++i)
    {
        delete slaveViews[i-1];
        delete slaveStreams[i-1];
    }

    // Close the look up table and open it for reading
    syncLookupFile.close();
    syncLookupFile.open(QIODevice::ReadOnly);
    syncLookupStream.setDevice(&syncLookupFile);

    return 0;
}

//QDateTime ruba::SynchronisedVideoCaptureFromFileTimestamps::convertLogTime(QString logEntry)
//{
//    // Supports log file like: 00000 2016 08 25 12:02:16.215
//    // frameNbr year month day hour minute second.millisecond
//    QRegExp rxlen("([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+) ([0-9]+):([0-9]+):([0-9]+).([0-9]+)*");
//
//    int pos = rxlen.indexIn(logEntry);
//
//    QDateTime logTime;
//
//    if (pos > -1)
//    {
//        // The regular expression suits the string!
//        QString year = rxlen.cap(2);
//        QString month = rxlen.cap(3);
//        QString day = rxlen.cap(4);
//        QString hour = rxlen.cap(5);
//        QString minute = rxlen.cap(6);
//        QString second = rxlen.cap(7);
//        QString millisecond = rxlen.cap(8);
//
//        logTime.setDate(QDate(year.toInt(), month.toInt(), day.toInt()));
//        logTime.setTime(QTime(hour.toInt(), minute.toInt(), second.toInt(), millisecond.toInt()));
//    }
//
//    return logTime;
//}

QList<SyncLog> ruba::SynchronisedVideoCaptureFromFileTimestamps::getFrameInfoFromString(QString csvLine)
{
    QList<QString> syncLogEntries = csvLine.split(";");
    QList<SyncLog> newFrameInfo;

    if (!syncLogEntries.isEmpty())
    {
        for (auto i = 0; i < syncLogEntries.size(); ++i)
        {
            if (!syncLogEntries[i].isEmpty())
            {
                SyncLog info;
                QStringList parsedFrameDateTime = syncLogEntries[i].split(" ");
                QStringList dateTimeString = parsedFrameDateTime;
                dateTimeString.removeFirst();

                info.timestamp = QDateTime::fromString(dateTimeString.join(" "), "yyyy MM dd HH:mm:ss.zzz"); // The first five x'es correspond to the frame number
                info.frameNbr = parsedFrameDateTime.size() > 0 ? parsedFrameDateTime.first().toInt() : 0;

                qDebug() << "Frame " << info.frameNbr << " at: " << info.timestamp.toString("yyyy MM dd HH:mm:ss.zzz");

                newFrameInfo.push_back(info);
            }
        }
    }

    return newFrameInfo;
}

ruba::SynchronisedVideoCaptureFromStream::SynchronisedVideoCaptureFromStream(QWidget * parent, 
	QList<ViewInfo> views, QList<InitFileProperties> properties)
	: AbstractSynchronisedVideoCapture(parent, views)
{
	syncType = VideoSyncType::CAPTUREFROMSTREAM;
	videoIsReleased = true;

	initialise(properties);
}

ruba::SynchronisedVideoCaptureFromStream::SynchronisedVideoCaptureFromStream(QWidget * parent,
	cv::FileStorage & fs, std::string entryPrefix)
	: AbstractSynchronisedVideoCapture(parent, fs, entryPrefix)
{
	syncType = VideoSyncType::CAPTUREFROMSTREAM;
	videoIsReleased = true;

	loadFromFile(fs, entryPrefix);
}

ruba::SynchronisedVideoCaptureFromStream::~SynchronisedVideoCaptureFromStream()
{
	// TODO: Release the stream
}

std::vector<FrameStruct> ruba::SynchronisedVideoCaptureFromStream::getCurrentFrames(CaptureStatus & status)
{
	status = prevStatus;

	return frames;
}

std::vector<FrameStruct> ruba::SynchronisedVideoCaptureFromStream::getNextFrames(CaptureStatus & status)
{
	bool success = true;
	status = CaptureStatus::OK;

	if (fileProperties.viewFiles.size() != frames.size())
	{
		frames.resize(fileProperties.viewFiles.size());
	}

	if (frameInfo.size() != fileProperties.viewFiles.size())
	{
		frameInfo.resize(fileProperties.viewFiles.size());
	}

	if (!fileProperties.viewFiles.empty())
	{
		// Check if the first capture is opened. If not, open all captures
		if (!fileProperties.viewFiles[0].cap.isOpened())
		{
			videoIsReleased = false;
			
			QList<QString> failedCaptureFilenames;
			for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
			{
				bool opened = fileProperties.viewFiles[i].cap.open(
					fileProperties.viewFiles[i].videofile.toStdString());
				fileProperties.viewFiles[i].startTime = QDateTime::currentDateTime();

				if (!opened)
				{
					failedCaptureFilenames.push_back(fileProperties.viewFiles[i].videofile);
					frameInfo[i].frameNbr = -1;
				}
				else {
					frameInfo[i].frameNbr = fileProperties.viewFiles[i].cap.get(CAP_PROP_POS_FRAMES);
					
				}
			}

			if (!failedCaptureFilenames.empty())
			{
				QString errorText = tr("Unable to open the following video files:\n");

				for (int i = 0; i < failedCaptureFilenames.size(); ++i)
				{
					errorText.append(tr("%1\n")
						.arg(failedCaptureFilenames[i]));
				}

				errorText.append(tr("\n\nPlease check the file path of the video and check if the video codec is supported on your computer"));

				emit sendMessage(tr("File error"), errorText, MessageType::Warning);
				success = false;
				status = VIDEONOTFOUND;
			}
		}

		// The captures are already open. Read the next frames
		if (success)
		{
			for (int i = 0; i < fileProperties.viewFiles.size(); ++i)
			{
				// Get the filename of the video according to the current view
				if (fileProperties.viewFiles[i].videofile.isEmpty())
				{
					ViewInfo view = fileProperties.viewFiles[i].viewInfo;
					QString errorText = tr("No video files specified for view %1. \n\nPlease specify video files for all views and retry.")
						.arg(view.viewName());
					emit sendMessage(tr("File error"), errorText, MessageType::Warning);

					status = CaptureStatus::VIDEONOTFOUND;
					success = false;
				}


				try {
					if (!fileProperties.viewFiles[i].cap.read(frames[i].Image))
					{
						success = false;
					}
				}
				catch (cv::Exception e)
				{
					QString title, message;
					Utils::getWarningMessage(getParent(), e, title, message);
					sendMessage(title, message, MessageType::Warning);
					status = CaptureStatus::VIDEONOTFOUND;

					success = false;
				}

				frames[i].Timestamp = QDateTime::currentDateTime();

			}
		}
	}

	prevStatus = status;

	return getCurrentFrames(status);
}

qint64 ruba::SynchronisedVideoCaptureFromStream::getFrameNumber() const
{
	return qint64();
}

int ruba::SynchronisedVideoCaptureFromStream::releaseVideo()
{
	// Release all videos in the list
	try
	{
		for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
		{
			fileProperties.viewFiles[i].cap.release();
		}
	}
	catch (cv::Exception e)
	{
		qDebug() << e.what();
		return 1;
	}

	return 0;
}

QList<VideoResolutionProperties> ruba::SynchronisedVideoCaptureFromStream::getVideoResolution() const
{
	QList<VideoResolutionProperties> resolutionProperties;

	for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
	{
		VideoResolutionProperties resProp;
		resProp.resolution = fileProperties.viewFiles[i].videoResolution;
		resProp.videoPath = fileProperties.viewFiles[i].videofile;

		resolutionProperties.append(resProp);
	}

	return resolutionProperties;
}

double ruba::SynchronisedVideoCaptureFromStream::getFrameRate() const
{
	if (!fileProperties.viewFiles.isEmpty())
	{
		return fileProperties.viewFiles[0].fps;
	}

	return 0.;
}

qint64 ruba::SynchronisedVideoCaptureFromStream::getFrameNumberAtEndTime() const
{
	return -1;
}

QList<QWidget*> ruba::SynchronisedVideoCaptureFromStream::getSpecificVideoProperties() const
{
	QList<QWidget*> specificProperties;

	// Generate information of each view
	for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
	{
		QString viewName = tr("View %1")
			.arg(fileProperties.viewFiles[i].viewInfo.viewName());

		QGroupBox *viewGroup = new QGroupBox(viewName);

		// File name
		QLabel *fileNameInfo = new QLabel(
			fileProperties.viewFiles[i].videofile);

		// Image width
		QLabel *imageWidthInfo = new QLabel(QString::number(
			fileProperties.viewFiles[i].videoResolution.width));

		// Image height
		QLabel *imageHeightInfo = new QLabel(QString::number(
			fileProperties.viewFiles[i].videoResolution.height));


		// Layout of the group
		QFormLayout *viewLayout = new QFormLayout();
		viewLayout->addRow(tr("File name"), fileNameInfo);
		viewLayout->addRow(tr("Image width"), imageWidthInfo);
		viewLayout->addRow(tr("Image height"), imageHeightInfo);

		viewGroup->setLayout(viewLayout);

		specificProperties.append(viewGroup);
	}


	return specificProperties;
}

QList<InfoLabel> ruba::SynchronisedVideoCaptureFromStream::getAdditionalGeneralProperties() const
{
	if (!fileProperties.viewFiles.empty())
	{
		InfoLabel fpsInfo;
		fpsInfo.info = tr("Frame rate");
		fpsInfo.label = new QLabel(tr("%1 frames/sec").arg(fileProperties.viewFiles[0].fps));

		return QList<InfoLabel>{fpsInfo};
	}
	
	return QList<InfoLabel>();
}

int ruba::SynchronisedVideoCaptureFromStream::viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews)
{
	return 0;
}

int ruba::SynchronisedVideoCaptureFromStream::saveSpecificParametersToFile(cv::FileStorage & fs, const std::string & entryPrefix)
{
	return 0;
}

int ruba::SynchronisedVideoCaptureFromStream::initialise(QList<InitFileProperties> newProperties)
{
	// Step 1: Construct fileProperties
	for (auto i = 0; i < newProperties.size(); ++i)
	{
		FileProperties prop;
		prop.videofile = newProperties[i].videofile;
		prop.viewInfo = newProperties[i].viewInfo;

		fileProperties.viewFiles.append(prop);

		FrameStruct frame;
		frame.Filename = newProperties[i].videofile;
		frame.viewInfo = newProperties[i].viewInfo;
		frames.push_back(frame);
	}

	// Step 2: Try to fetch the video
	for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
	{
		try {
			fileProperties.viewFiles[i].cap.open(fileProperties.viewFiles[i].videofile.toStdString());
		}
		catch (cv::Exception e)
		{
			QString title, message;
			Utils::getWarningMessage(getParent(), e, title, message);
			sendMessage(title, message, MessageType::Warning);

			prevStatus = CaptureStatus::VIDEONOTFOUND;
			return 1;
		}

		if (!fileProperties.viewFiles[i].cap.isOpened())
		{
			prevStatus = CaptureStatus::VIDEONOTFOUND;
			sendMessage(tr("Video error"), tr("Could not open video at URL:\n%1")
						.arg(fileProperties.viewFiles[i].videofile), 
						MessageType::Warning);

			return 1;
		}
	}

	// Step 3: Extract resolution and frame rate
	for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
	{
		cv::Size res;

		try {
			fileProperties.viewFiles[i].videoResolution.height =
				fileProperties.viewFiles[i].cap.get(CAP_PROP_FRAME_HEIGHT);
			fileProperties.viewFiles[i].videoResolution.width =
				fileProperties.viewFiles[i].cap.get(CAP_PROP_FRAME_WIDTH);
			fileProperties.viewFiles[i].fps =
				fileProperties.viewFiles[i].cap.get(CAP_PROP_FPS);
		}
		catch (cv::Exception e)
		{
			QString title, message;
			Utils::getWarningMessage(getParent(), e, title, message);
			sendMessage(title, message, MessageType::Warning);
		}
	}

	// Step 4: Close VideoCapture for each file
	releaseVideo();

	// Step 5: Initialise the frame numbers    
	frameInfo.clear();

	for (auto i = 0; i < fileProperties.viewFiles.size(); ++i)
	{
		SyncLog info;
		info.frameNbr = -1;
		info.timestamp = QDateTime();

		frameInfo.push_back(info);
	}

	return 0;
}

int ruba::SynchronisedVideoCaptureFromStream::loadFromFile(cv::FileStorage & fs, const std::string & entryPrefix)
{
	return 0;
}
