
#include <QtTest/QtTest>
#include "../logsettingsdialog.h"

class LogSettingsDialogTest : public QObject
{
public:
    Q_OBJECT

private slots:
    void run();

private:
    void compareInitLogSettings(const InitLogSettings& firstSettings,
        const InitLogSettings& secondSettings);
};


void LogSettingsDialogTest::compareInitLogSettings(const InitLogSettings& firstSettings,
        const InitLogSettings& secondSettings)
{
    QCOMPARE(firstSettings.everyEventLogPath, secondSettings.everyEventLogPath);
    QCOMPARE(firstSettings.saveEveryEventLog, secondSettings.saveEveryEventLog);
    QCOMPARE(firstSettings.saveFrameOfEventPath, secondSettings.saveFrameOfEventPath);
    QCOMPARE(firstSettings.saveSumOfEventsLog, secondSettings.saveSumOfEventsLog);
    QCOMPARE(firstSettings.sumInterval, secondSettings.sumInterval);
    QCOMPARE(firstSettings.sumOfEventsLogPath, secondSettings.sumOfEventsLogPath);


    // Test the entry settings
    QCOMPARE(firstSettings.entrySettings.consecutiveEventDelay,
        secondSettings.entrySettings.consecutiveEventDelay);
    QCOMPARE(firstSettings.entrySettings.discardEventsLessThan,
        secondSettings.entrySettings.discardEventsLessThan);

    for (auto it : firstSettings.entrySettings.eventDelayIntervals)
    {
        auto secondIt = secondSettings.entrySettings.eventDelayIntervals.find(it.first);

        if (secondIt != secondSettings.entrySettings.eventDelayIntervals.end())
        {
            QCOMPARE(it.second.createEventMaxDelay,
                secondIt->second.createEventMaxDelay);
        }

        //  Don't compare the minimum delay as this feature is currently not supported in RUBA
        /*QCOMPARE(it.second.createEventMinDelay,
            secondSettings.entrySettings.eventDelayIntervals[it.first].createEventMinDelay);*/
    }

    QCOMPARE(firstSettings.entrySettings.intervalType,
        secondSettings.entrySettings.intervalType);
    QCOMPARE(int(firstSettings.entrySettings.maxActiveDuration.size()),
        int(secondSettings.entrySettings.maxActiveDuration.size()));

    QCOMPARE(firstSettings.entrySettings.maxActiveDuration,
        secondSettings.entrySettings.maxActiveDuration);
    QCOMPARE(secondSettings.entrySettings.saveFrameOfEventSettings,
        secondSettings.entrySettings.saveFrameOfEventSettings);
}

void LogSettingsDialogTest::run()
{

    // Generate set of log settings for the general, compound detectors and
    // the individual detectors
    InitLogSettings generalSettings;
    std::map<QString, InitLogSettings> individualDetectorSettings;

    generalSettings.everyEventLogPath = "TestPath";
    generalSettings.saveEveryEventLog = true;
    generalSettings.saveFrameOfEventPath = "SavePath9k";
    generalSettings.saveSumOfEventsLog = false;
    generalSettings.sumInterval = 10;
    generalSettings.sumOfEventsLogPath = "Test22";
    generalSettings.entrySettings.consecutiveEventDelay = 389;
    generalSettings.entrySettings.discardEventsLessThan = 897;

    CombinedEventDelays delays;
    delays.createEventMaxDelay = 1610;
    delays.createEventMinDelay = 45;
    generalSettings.entrySettings.eventDelayIntervals["Detector2"] = delays;
    generalSettings.entrySettings.intervalType = std::vector<Transition>{ LEAVING, ENTERING};
    generalSettings.entrySettings.maxActiveDuration["General"] = 385;
    generalSettings.entrySettings.maxActiveDuration["Detector2"] = 1258;
    generalSettings.entrySettings.saveFrameOfEventSettings["Detector2"] = std::set<Transition>{ LEAVING };

    individualDetectorSettings["Detector2"] = generalSettings;
    individualDetectorSettings["Detector2"].everyEventLogPath = "Individual test path";
    individualDetectorSettings["Detector2"].entrySettings.saveFrameOfEventSettings["Detector2"] = std::set<Transition>{ ENTERING };
    individualDetectorSettings["Detector2"].entrySettings.intervalType = std::vector<Transition>{ ENTERING };
    individualDetectorSettings["Detector3"] = individualDetectorSettings["Detector2"];

        
    // Check that setLogsettings and getLogSettings are compatible when setting and getting 
    LogSettingsDisplayOptions displayOptions;
    displayOptions.showCollateEventsWithin = true;
    displayOptions.showDeleteEventsSmallerThan = true;
    displayOptions.showMaxTriggeredDuration = true;

    LogSettingsDialog testDialog(new QWidget(), 
        displayOptions, 
        QList<QString> {"Detector2", "Detector3"}, "testPath");

    testDialog.setLogSettings(generalSettings, individualDetectorSettings);

    InitLogSettings retrievedGeneralSettings;
    std::map<QString, InitLogSettings> retrievedIndividualDetectorSettings;

    testDialog.getLogSettings(retrievedGeneralSettings,
        retrievedIndividualDetectorSettings);

    compareInitLogSettings(generalSettings, retrievedGeneralSettings);
    

    for (auto loadedSettings : retrievedIndividualDetectorSettings)
    {
        auto it = individualDetectorSettings.find(loadedSettings.first);

        if (it!= individualDetectorSettings.end())
        {
            compareInitLogSettings(it->second,
                retrievedIndividualDetectorSettings[loadedSettings.first]);
        }
        else {
            QCOMPARE(loadedSettings.first, individualDetectorSettings.begin()->first);
        }
    }

    // Check compatibility when saving and loading
    LogSettingsDialog testLoad(new QWidget(),
        displayOptions,
        QList<QString> {"Detector2", "Detector3"}, "testPath");
    
    cv::FileStorage fsWrite("test.yml", cv::FileStorage::WRITE);
    
    testDialog.saveToFile(fsWrite);
    fsWrite.release();
   
    cv::FileStorage fsRead("read.yml", cv::FileStorage::READ);
    testLoad.loadFromFile(fsRead);

    //InitLogSettings loadedGeneralSettings;
    //std::map<QString, InitLogSettings> loadedIndividualDetectorSettings;

    //testLoad.getLogSettings(loadedGeneralSettings, loadedIndividualDetectorSettings);

    //compareInitLogSettings(generalSettings, loadedGeneralSettings);

    //for (auto loadedSettings : loadedIndividualDetectorSettings)
    //{
    //    auto it = individualDetectorSettings.find(loadedSettings.first);

    //    if (it != individualDetectorSettings.end())
    //    {
    //        compareInitLogSettings(it->second,
    //            loadedIndividualDetectorSettings[loadedSettings.first]);
    //    }
    //    else {
    //        QCOMPARE(loadedSettings.first, individualDetectorSettings.begin()->first);
    //    }
    //}
    

}

QTEST_MAIN(LogSettingsDialogTest)
#include "test_logsettingsdialog.moc"

