#include "detectorhandler.h"
#include "QDebug"

using namespace cv;
using namespace std;
using namespace ruba;

DetectorHandler::DetectorHandler(QWidget *parent, QList<ViewInfo> views)
{
    currentViews = views;
    this->parent = parent;

    detectorListModel = new QStandardItemModel();

    this->debugPref.OverlayTimestamp = false;
    UpdateDebugInfo = true;

    QStandardItem* item = new QStandardItem("Sample");
    enabledBrush = item->foreground();
    disabledBrush = item->foreground();
    disabledBrush.setColor(QColor(100, 100, 100));
}

int DetectorHandler::saveToFile(cv::FileStorage& fs)
{
    // Save the file names of the current detectors
    if (fs.isOpened())
    {
        // Create temporary vector of filenames to save
        vector<string> filenames;
        vector<int> overlayMasks, showDebugWindows, 
            overlayDebugInfo, overlayExtDebugInfo;

        for (int i = 0; i < TDetectors.size(); ++i)
        {
            filenames.push_back(TDetectors[i]->GetConfigSaveFilePath().toStdString());

            // Save the debug preferences. Create separate vectors that FileStorage may
            // save - i.e., convert to int
            if (debugPref.OverlayMasks.size() > i)
            {
                overlayMasks.push_back(debugPref.OverlayMasks[i]);
            }

            if (debugPref.ShowDebugWindows.size() > i)
            {
                showDebugWindows.push_back(debugPref.ShowDebugWindows[i]);
            }

            if (debugPref.OverlayDebugInfo.size() > i)
            {
                overlayDebugInfo.push_back(debugPref.OverlayDebugInfo[i]);
            }

            if (debugPref.OverlayExtDebugInfo.size() > i)
            {
                overlayExtDebugInfo.push_back(debugPref.OverlayExtDebugInfo[i]);
            }
        }

        // Write the information to the FileStream
        fs << "DHandler-filenames" << filenames;
        fs << "DHandler-debugPref-overlayMasks" << overlayMasks;
        fs << "DHandler-debugPref-showDebugWindows" << showDebugWindows;
        fs << "DHandler-debugPref-overlayDebugInfo" << overlayDebugInfo;
        fs << "DHandler-debugPref-overlayExtDebugInfo" << overlayExtDebugInfo;
        fs << "DHandler-debugPref-overlayTimestamp" << debugPref.OverlayTimestamp;
           
    }

    return 0;
}

void DetectorHandler::loadDetector(vector<FrameStruct> frames)
{
    QSettings settings;
    settings.beginGroup("detectorSelectionDialog");
    QString detectorBaseDir = settings.value("detectorBaseDir", "").toString();
    settings.endGroup();

    QStringList filenames = QFileDialog::getOpenFileNames(parent, tr("Load detectors"),
        detectorBaseDir, tr("Configuration files (*.yml)"));

    if (filenames.isEmpty())
    {
        return;
    }

    QProgressDialog progress("Loading detector(s)..", "Cancel", 0, 
        filenames.size(), this);
    progress.setWindowModality(Qt::WindowModal);
    QApplication::processEvents();

    for (int i = 0; i < filenames.size(); ++i)
    {
        progress.setValue(i);
        QApplication::processEvents();

         createDetector(-1, frames, filenames[i]);

        if (progress.wasCanceled())
        {
            break;
        }
    }

    if (!filenames.empty() && filenames[0].isEmpty())
    {
        // Update the path in the settings
        QFileInfo file(filenames[0]);
        settings.beginGroup("detectorSelectionDialog");
        settings.setValue("detectorBaseDir", file.path());
        settings.endGroup();
    }
}

void ruba::DetectorHandler::getDetectors(std::vector<std::shared_ptr<ruba::AbstractCompoundDetector> >& detectors)
{
	detectors.clear();

	for (auto& detector : TDetectors)
	{
		detectors.push_back(detector);
	}
}

int DetectorHandler::loadFromFile(cv::FileStorage& fs, vector<FrameStruct> frames, 
                     QProgressDialog &progress, QSplashScreen &splash, bool showSplash)
{
    if (fs.isOpened())
    {
        QList<QString> filenames;
        vector<int> overlayMasks, showDebugWindows,
            overlayDebugInfo, overlayExtDebugInfo, tmpDetectorIds;

        filenames = Utils::readQStringListFromFileStorage(fs, "DHandler-filenames");

        fs["DHandler-debugPref-overlayMasks"] >> overlayMasks;
        fs["DHandler-debugPref-showDebugWindows"] >> showDebugWindows;
        fs["DHandler-debugPref-overlayDebugInfo"] >> overlayDebugInfo;
        fs["DHandler-debugPref-overlayExtDebugInfo"] >> overlayExtDebugInfo;
        fs["DHandler-debugPref-overlayTimestamp"] >> debugPref.OverlayTimestamp;

        // Clear the current detectors and debug preferences
        TDetectors.clear();
        debugPref.OverlayMasks.clear();
        debugPref.ShowDebugWindows.clear();
        debugPref.OverlayDebugInfo.clear();
        debugPref.OverlayExtDebugInfo.clear();
        detectorIds.clear();
        detectorListModel->clear();

        if (!showSplash)
        {
            progress.setLabelText("Loading detectors");
            progress.setMaximum(filenames.size());
            progress.setValue(0);
        }

        // Load the detectors from the file name
        for (int i = 0; i < filenames.size(); ++i)
        {
            if (!filenames[i].isEmpty())
            {
                if (!showSplash)
                {
                    progress.setValue(i);
                    QApplication::processEvents();

                    if (progress.wasCanceled())
                    {
                        return 0;
                    }
                }
                else {
                    splash.showMessage(QString("Loading previous configuration - detector %1 of %2").arg(i + 1).arg(filenames.size()),
                        Qt::AlignBottom + Qt::AlignCenter);
                }

                createDetector(-1, frames, filenames[i]);

                // Set the debug preferences
                if ((overlayMasks.size() > i) && (debugPref.OverlayMasks.size() > i))
                {
                    debugPref.OverlayMasks[i] = overlayMasks[i];
                }

                if ((showDebugWindows.size() > i) && (debugPref.ShowDebugWindows.size() > i))
                {
                    debugPref.ShowDebugWindows[i] = showDebugWindows[i];
                }

                if ((overlayDebugInfo.size() > i) && (debugPref.OverlayDebugInfo.size() > i))
                {
                    debugPref.OverlayDebugInfo[i] = overlayDebugInfo[i];
                }

                if ((overlayExtDebugInfo.size() > i) && (debugPref.OverlayExtDebugInfo.size() > i))
                {
                    debugPref.OverlayExtDebugInfo[i] = overlayExtDebugInfo[i];
                }
            }
        }
    }

    return 0;
}

void DetectorHandler::paintOverlay(std::vector<FrameStruct>& frames)
{
    if (debugPref.OverlayTimestamp)
    {
        int fontFace = FONT_HERSHEY_PLAIN;
        double fontScale = 1;
        int thickness = 1;
        cv::Point textOrg(400, 15);

        for (size_t i = 0; i < frames.size(); ++i)
        {
            cv::putText(frames[i].Image, frames[i].Timestamp.toString("dd.MM.yyyy HH:mm:ss.zzz").toStdString(), textOrg, fontFace, fontScale, cv::Scalar::all(255), thickness, 8);
        }
    }

    cv::Point textOrg(10, 10);

    int fontFace = FONT_HERSHEY_PLAIN;
    double fontScale = 1;
    int thickness = 1;

    for (int i = 0; i < TDetectors.size(); ++i)
    {
        if (debugPref.OverlayDebugInfo.size() > i && debugPref.OverlayDebugInfo[i])
        {
            QList<QString> list = TDetectors[i]->GetDebugInfo();
            for (int j = 0; j < list.size(); ++j)
            {
                textOrg.y += 15;
                std::string text = list[j].toStdString();

                // Put the same text on all modalities
                for (size_t k = 0; k < frames.size(); ++k)
                {
                    cv::putText(frames[k].Image, text, textOrg, fontFace, fontScale, cv::Scalar::all(255), thickness, 8);
                }
            }
        }
    }

    for (int i = 0; i < TDetectors.size(); ++i)
    {
        if (debugPref.OverlayExtDebugInfo.size() > i && debugPref.OverlayExtDebugInfo[i])
        {
            QList<QString> list = TDetectors[i]->GetExtendedDebugInfo();
            for (int j = 0; j < list.size(); ++j)
            {
                textOrg.y += 15;
                std::string text = list[j].toStdString();

                // Put the same text on all modalities
                for (size_t k = 0; k < frames.size(); ++k)
                {
                    cv::putText(frames[k].Image, text, textOrg, fontFace, fontScale, cv::Scalar::all(255), thickness, 8);
                }
            }
        }
    }

    for (int i = 0; i < TDetectors.size(); ++i)
    {
        if (debugPref.OverlayMasks.size() > i && debugPref.OverlayMasks[i])
        {
            if ((TDetectors[i]->GetMasks().size() != 0) && 
                (TDetectors[i]->getDiagnostics().state == DiagnosticState::Working))
            {
                vector<vector<MaskOverlayStruct>> maskOverlays = TDetectors[i]->GetMasks();

                for (int j = 0; j < maskOverlays.size(); ++j)
                {
                    for (int k = 0; k < maskOverlays[j].size(); ++k)
                    {
                        // Put the masks on the corresponding modality
                        for (size_t l = 0; l < frames.size(); ++l)
                        {
                            if ((frames[l].viewInfo.compare(maskOverlays[j][k].View) == 0) && 
                                (frames[l].Image.size() == maskOverlays[j][k].Mask.size()))
                            {
                                frames[l].Image = cvUtils::colorOverlay(maskOverlays[j][k].Color, float(maskOverlays[j][k].Alpha),
                                    frames[l].Image, maskOverlays[j][k].Mask);
                            }
                        }
                    }
                }
            }
        }
    }
}

void DetectorHandler::lastFrameReached()
{
    for (int i = 0; i < TDetectors.size(); ++i)
    {
        TDetectors[i]->SetLastFrameReached();
    }
}

void DetectorHandler::update(std::vector<ruba::FrameStruct> &frames)
{
    // Perform sanity check on the frames provided. 
    // Only update if all images are non-empty
    if (frames.empty())
    {
        return;
    }

    for (size_t j = 0; j < frames.size(); ++j)
    {
        if (frames[j].Image.empty())
        {
            return;
        }
    }

    // Update the TrafficDetectors
    for (int i = 0; i < TDetectors.size(); ++i)
    {
        TDetectors[i]->Update(frames);
        checkDetectorDiagnostics(i);
    }

    // Get the output of the TrafficDetectors to feed the InteractionDetectors
    vector<DetectorOutput> dOutput;
    for (int i = 0; i < TDetectors.size(); ++i)
    {
        dOutput.push_back(TDetectors[i]->GetOutput());
    }

    // Update the InteractionDetectors amongst the TrafficDetectors
    for (int i = 0; i < TDetectors.size(); ++i)
    {
        int detectorType = TDetectors[i]->getDetectorType();

        if ((detectorType == INTERACTIONDETECTOR)
            || (detectorType == INTERACTION410DETECTOR))
        {
            TDetectors[i]->Update(frames, dOutput);

            checkDetectorDiagnostics(i);
        }
    }
}


int DetectorHandler::getDetectorCount()
{
    return TDetectors.size();
}

std::vector<ruba::ChartStruct> ruba::DetectorHandler::getCharts()
{
	std::vector<ruba::ChartStruct> returnCharts;
	
	// Show debug windows, if enabled
	for (int i = 0; i < TDetectors.size(); ++i)
	{
		if (debugPref.ShowDebugWindows.size() > i && debugPref.ShowDebugWindows[i])
		{
			std::vector<ruba::ChartStruct> charts = TDetectors[i]->GetChartStructs();
			returnCharts.insert(std::end(returnCharts), std::begin(charts), std::end(charts));
		}
	}

	return returnCharts;
}

void ruba::DetectorHandler::refreshDetectorInformationList()
{
	debugInfoList.resize(TDetectors.size());
	extendedDebugInfoList.resize(TDetectors.size());
	everyEventLogInfoList.resize(TDetectors.size());
	individualLogInfoList.resize(TDetectors.size());
	tDetectorDiagnostics.resize(TDetectors.size());

	for (auto i = 0; i < TDetectors.size(); ++i)
	{
		tDetectorDiagnostics[i] = TDetectors[i]->getDiagnostics();
		debugInfoList[i] = TDetectors[i]->GetDebugInfo();
		extendedDebugInfoList[i] = TDetectors[i]->GetExtendedDebugInfo();
		everyEventLogInfoList[i] = TDetectors[i]->GetEveryEventLogInfo();
		individualLogInfoList[i] = TDetectors[i]->GetIndividualLogInfo();
	}
}


void ruba::DetectorHandler::refreshDetectorInformationModel()
{
	// Update the information in the detector information dock

	for (auto i = 0; i < TDetectors.size(); ++i)
	{
		while (detectorListModel->rowCount() < TDetectors.size())
		{
			// Insert new row
			QList<QStandardItem*> newRow;
			QStandardItem* detectorName = new QStandardItem(TDetectors[i]->getDetectorName() + QString(" [%1]").arg(TDetectors[i]->getDetectorID()));
			QStandardItem* diagnosticIcon = new QStandardItem(QIcon(), "");
			newRow.push_back(detectorName);
			newRow.push_back(diagnosticIcon);
			detectorListModel->appendRow(newRow);
		}

		// Get the parent item that corresponds to this detector
		QStandardItem *infoItem = detectorListModel->item(i);

		if (tDetectorDiagnostics.size() == TDetectors.size())
		{
			DetectorDiagnostics diagnostics = tDetectorDiagnostics[i];

			// Mark by bold font if the detector is triggered
			if ((diagnostics.state == DiagnosticState::Working) && (detectorListModel->rowCount() > i))
			{
				QFont font(detectorListModel->item(i)->font());

				if (TDetectors[i]->GetIsTriggered()[0])
				{
					font.setBold(true);
				}
				else {
					font.setBold(false);
				}

				infoItem->setFont(font);
				infoItem->setForeground(enabledBrush);
			}
		}

		if (debugInfoList.size() == TDetectors.size())
		{
			// Write debug info
			updateDetectorInformation(debugInfoList[i], infoItem);

			// Write extended debug info
			if (infoItem->rowCount() < debugInfoList[i].size() + 1)
			{
				infoItem->appendRow(new QStandardItem(tr("Extended information")));
			}

			if (extendedDebugInfoList.size() == TDetectors.size())
			{
				QStandardItem* extendedDebugInfoHeader = infoItem->child(debugInfoList[i].size());
				updateDetectorInformation(extendedDebugInfoList[i], extendedDebugInfoHeader, true);
			}

			// Write every event log info
			if (infoItem->rowCount() < debugInfoList[i].size() + 2)
			{
				infoItem->appendRow(new QStandardItem(tr("Log")));
			}

			if (everyEventLogInfoList.size() == TDetectors.size())
			{
				QStandardItem* everyEventLogInfoHeader = infoItem->child(debugInfoList[i].size() + 1);
				updateDetectorInformation(everyEventLogInfoList[i], everyEventLogInfoHeader, true);
			}

			// Write individual log info
			if (individualLogInfoList.size() == TDetectors.size())
			{
				if (!individualLogInfoList[i].empty())
				{
					if (infoItem->rowCount() < debugInfoList[i].size() + 3)
					{
						infoItem->appendRow(new QStandardItem(tr("Individual detector logs")));
					}

					QStandardItem* overallIndividualLogInfoHeader = infoItem->child(debugInfoList[i].size() + 2);

					int individualItemNumber = 0;

					for (auto logInfo : individualLogInfoList[i])
					{
						individualItemNumber++;
						if (overallIndividualLogInfoHeader->rowCount() < individualItemNumber)
						{
							overallIndividualLogInfoHeader->appendRow(new QStandardItem(logInfo.first));
						}

						updateDetectorInformation(logInfo.second, overallIndividualLogInfoHeader->child(individualItemNumber - 1), true);
					}
				}
			}
		}
	}
}

QStandardItemModel* DetectorHandler::getDetectorListModel()
{
    return detectorListModel;
}

void DetectorHandler::createDetector(int detectorType, vector<FrameStruct> frames, const QString& filePath)
{
    // If file path is supplied, get the corresponding detector type
    if (!filePath.isEmpty())
    {
        detectorType = determineDetectorType(filePath);

        if (detectorType == -1)
        {
            return;
        }
    }

    BaseDetectorParams baseParams;
    baseParams.initFrames = frames;
    baseParams.detectorID = requestDetectorId();

    switch (detectorType)
    {
    case RIGHTTURNINGCAR:
    {
        std::shared_ptr<RightTurningCar> d = std::make_shared<RightTurningCar>(baseParams,  parent, filePath);
        addDetector(d, baseParams.detectorID, "Right Turning Car");
        break;
    }
    case STRAIGHTGOINGCYCLIST:
    {
        std::shared_ptr<StraightGoingCyclist> d = std::make_shared<StraightGoingCyclist>(baseParams, parent, filePath);
        addDetector(d, baseParams.detectorID, "Straight Going Cyclist");
        break;
    }
    case LEFTTURNINGCAR:
    {
        std::shared_ptr<LeftTurningCar> d = std::make_shared<LeftTurningCar>(baseParams, parent, filePath);
        addDetector(d, baseParams.detectorID, "Left Turning Car");
        break;
    }
    case INTERACTIONDETECTOR:
    {
        addInteractionDetector(baseParams, filePath);
        break;
    }
    case INTERACTION410DETECTOR:
    {
        addInteractionDetector(baseParams, filePath);
        break;
    }
    case SINGLEMODULE:
    {
        std::shared_ptr<SingleModule> d = std::make_shared<SingleModule>(baseParams, parent, filePath);
        addDetector(d, baseParams.detectorID, "Single Module");
        break;
    }
    case DOUBLEMODULE:
    {
        std::shared_ptr<DoubleModule> d = std::make_shared<DoubleModule>(baseParams, parent, filePath);
        addDetector(d, baseParams.detectorID, "Double Module");
        break;
    }
    case GROUNDTRUTHANNOTATOR:
    {
        std::shared_ptr<GroundTruthAnnotator> d = std::make_shared<GroundTruthAnnotator>(baseParams, parent);
        addDetector(d, baseParams.detectorID, "Ground Truth Annotator");
        break;
    }
    case LOGFILEREVIEWER:
	{
		std::shared_ptr<LogFileReviewer> d = std::make_shared<LogFileReviewer>(baseParams, parent);
		addDetector(d, baseParams.detectorID, tr("Log File Reviewer"));
		break;
	}
	case EXCLUSIVEMODULE:
	{
		std::shared_ptr<ExclusiveModule> d = std::make_shared<ExclusiveModule>(baseParams, parent, filePath);
		addDetector(d, baseParams.detectorID, tr("Exclusive Module"));
		break;
    }
    default:

        if (detectorType == 0)
        {
            // We have encountered a file that does not contain detectors. 
            // Investigate if the file was created using RUBA:

            FileStorage fs;
            try {
                fs.open(filePath.toStdString(), FileStorage::READ);
            }
            catch (cv::Exception e) {
                qDebug() << e.what();
				emit sendMessage(tr(this->windowTitle().toStdString().c_str()),
                    "Unable to load file: " + filePath, MessageType::Warning);
                return;
            }

            if (!fs.isOpened())
            {
                return;
            }

            vector<int> rubaVersion;
            fs["rubaVersion"] >> rubaVersion;
            fs.release();

            if (rubaVersion.empty())
            {
                // We don't know the origin of the file. Display a proper error message
                QString errorText = tr("The file format is not supported by RUBA and does not contain a detector. \nFile path: ") + filePath;
                emit sendMessage(tr("Unknown file format"), errorText, MessageType::Information);
            }
            else {
                // The file is not a detector file, but a configuration file
                emit sendMessage(tr("Configuration file was found"),
                    tr("The file is not a detector file, but a configuration file containing videos and detectors from a previous analysis. "
                        "Load the configuration in \"File -> Load configuration\" instead."), MessageType::Information);
            }
        }
        else {
            // We have encountered an unknown detector type - maybe from a future version of RUBA
            emit sendMessage(tr("Unknown detector type"),
                tr("RUBA could not recognise the detector type in the file: \n%1").arg(filePath), MessageType::Information);
        }
    }

    UpdateDebugInfo = true;
}

void DetectorHandler::addDetector(std::shared_ptr<AbstractCompoundDetector> newDetector, int detectorId, QString moduleText)
{
    if (newDetector->GetIsInitialized())
    {
        TDetectors.push_back(newDetector);

        // Update the detector list. The list is composed as
        // (DependencyIcon) DetectorName **** (DiagnosticIcon) ****
        QList<QStandardItem*> newRow;
        QStandardItem* detectorName = new QStandardItem(moduleText + QString(" [%1]").arg(detectorId));
        QStandardItem* diagnosticIcon = new QStandardItem(QIcon(), "");
		detectorName->appendRow(new QStandardItem(""));
        newRow.push_back(detectorName);
        newRow.push_back(diagnosticIcon);
		

        detectorListModel->appendRow(newRow);

        checkDetectorDiagnostics(TDetectors.size() - 1);
        addDebugPref();
    }
    else {
        removeDetectorId(detectorId);
    }

}


void DetectorHandler::addInteractionDetector(BaseDetectorParams baseParams, QString configFile)
{
    vector<QString> detectorNames;
    vector<DetectorOutput> detectorParams;
    QVector<qint64> consecutiveEventDelays;

    for (int i = 0; i < TDetectors.size(); ++i)
    {
        detectorNames.push_back(this->detectorListModel->item(i)->text());
        detectorParams.push_back(TDetectors[i]->GetOutput());
        consecutiveEventDelays.push_back(TDetectors[i]->GetConsecutiveEventDelay());
    }

    std::shared_ptr<InteractionDetector> d = std::make_shared<InteractionDetector>(baseParams, this, detectorNames, detectorParams,
        consecutiveEventDelays, configFile, true);

    if (d->GetIsInitialized())
    {
        TDetectors.push_back(d);

        // Update the detector list
        QStandardItem* item;

        if (d->getDetectorType() == INTERACTIONDETECTOR)
        {
            item = new QStandardItem(QIcon("://illustrations/emptyicon.png"), QString("312 Interaction [%1]").arg(baseParams.detectorID));
        }
        else if (d->getDetectorType() == INTERACTION410DETECTOR)
        {
            item = new QStandardItem(QIcon("://illustrations/emptyicon.png"), QString("410 Interaction [%1]").arg(baseParams.detectorID));
        }

        detectorListModel->appendRow(item);
        addDebugPref();

        setDependency(TDetectors.size() - 1);
    }
    else {
        removeDetectorId(baseParams.detectorID);
    }
}

void ruba::DetectorHandler::updateDetectorInformation(const QList<QString>& infoList, QStandardItem * item, bool shrinkToFit)
{
    // Grow list to fit
    while (item->rowCount() < infoList.size())
    {
        item->appendRow(new QStandardItem(""));
    }

    if (shrinkToFit)
    {
        while (item->rowCount() > infoList.size())
        {
            item->removeRow(item->rowCount() - 1);
        }
    }

    // Set text
    for (int j = 0; j < infoList.size(); ++j)
    {
        item->child(j)->setText(infoList[j]);
    }
}

void DetectorHandler::deleteDetectorFromList(int index)
{
    if (index >= 0 && (index < TDetectors.size()))
    {
        if (TDetectors[index]->HasParents())
        {
            // Another detector depend on the current detector and thus the current detector
            // cannot be deleted
            return;
        }

        removeDependency(index);
        removeDetectorId(TDetectors[index]->getDetectorID());

        detectorListModel->removeRow(index);

        TDetectors.removeAt(index);

        // Remove the detector debugging preferences
        removeDebugPref(index);
    }
}

int DetectorHandler::determineDetectorType(QString filePath)
{
    FileStorage fs;

    if (filePath.contains(".yml"))
    {
        int detectorType;
        try {
            fs.open(filePath.toStdString(), FileStorage::READ);
        }
        catch (cv::Exception e) {
            qDebug() << e.what();
            emit sendMessage(tr(this->windowTitle().toStdString().c_str()), 
                "Unable to load file: " + filePath, MessageType::Warning);
            return -1;
        }

        if (!fs.isOpened())
        {
            return -1;
        }

        fs["detectorType"] >> detectorType;
        fs.release();
        return detectorType;
    }
    else {
        fs.release();
        return -1;
    }

    return -1;
}

void DetectorHandler::reconfigureDetector(int index, std::vector<FrameStruct> frames)
{
    if (index >= 0 && (index < TDetectors.size()))
    {
        checkDetectorDiagnostics(index, true);
                
        TDetectors[index]->Reconfigure(frames, this);
    }
}

void ruba::DetectorHandler::resetDetector(int index)
{
    if ((index >= 0) && (index < TDetectors.size()))
    {
        TDetectors[index]->reset(true);
    }
}

void DetectorHandler::addDebugPref()
{
    // Inserts the debug preferences at the end of the list based on the existing
    if (debugPref.OverlayDebugInfo.empty())
    {
        // No debug preferences stored. Use defaults (disabled)
        debugPref.OverlayDebugInfo.push_back(false);
        debugPref.OverlayExtDebugInfo.push_back(false);
        debugPref.OverlayMasks.push_back(false);
        debugPref.ShowDebugWindows.push_back(false);
    }
    else {
        debugPref.OverlayDebugInfo.push_back(debugPref.OverlayDebugInfo[debugPref.OverlayDebugInfo.size() - 1]);
        debugPref.OverlayExtDebugInfo.push_back(debugPref.OverlayExtDebugInfo[debugPref.OverlayExtDebugInfo.size() - 1]);
        debugPref.OverlayMasks.push_back(debugPref.OverlayMasks[debugPref.OverlayMasks.size() - 1]);
        debugPref.ShowDebugWindows.push_back(debugPref.ShowDebugWindows[debugPref.ShowDebugWindows.size() - 1]);
    }
}

void DetectorHandler::removeDebugPref(int index)
{
    // Remove the debug preferences at the current index

    if (debugPref.OverlayMasks.size() > index)
    {
        debugPref.OverlayMasks.remove(index);
    }

    if (debugPref.ShowDebugWindows.size() > index)
    {
        debugPref.ShowDebugWindows.remove(index);
    }

    if (debugPref.OverlayDebugInfo.size() > index)
    {
        debugPref.OverlayDebugInfo.remove(index);
    }

    if (debugPref.OverlayExtDebugInfo.size() > index)
    {
        debugPref.OverlayExtDebugInfo.remove(index);
    }
}

DebugPrefStruct DetectorHandler::getDebugPref()
{
    return debugPref;
}

void DetectorHandler::checkDetectorDiagnostics(int index, bool forceShowDialog)
{
    // Send the updated modalities to the detector to see it is 
    // initialized under the current modalities
    TDetectors[index]->checkCurrentViews(currentViews);


    DetectorDiagnostics diagnostics = TDetectors[index]->getDiagnostics();

    if (diagnostics.errorStateChanged || forceShowDialog) {
        // Set detector enabled in the list according to the enabledState
        switch (diagnostics.state) {
        case DiagnosticState::Error:
        {
            // An error occurred which may be recovered by the user. Display warning
            // and show icon on detector
            detectorListModel->item(index)->setForeground(disabledBrush);
            QStandardItem* icon = detectorListModel->item(index, 1);
            icon->setIcon(QIcon("://icons/exclamation-white.png"));

            emit sendMessage(tr("Detector is currently disabled"), diagnostics.errorMessage,
                MessageType::Warning);

            break;
        }
        case DiagnosticState::Exception:
        {
            QStandardItem* icon = detectorListModel->item(index, 1);
            icon->setIcon(QIcon("://icons/exclamation-red.png"));

            detectorListModel->item(index)->setForeground(disabledBrush);
            emit sendMessage(tr("An error occurred"), diagnostics.errorMessage,
                MessageType::Critical);


            break;
        }
        case DiagnosticState::Working:
        {

            QStandardItem* icon = detectorListModel->item(index, 1);
            icon->setIcon(QIcon("://illustrations/emptyicon.png"));

            detectorListModel->item(index)->setForeground(enabledBrush);

            break;
        }
        default:
        {
            // Should never happen
            detectorListModel->item(index)->setForeground(disabledBrush);

            QStandardItem* icon = detectorListModel->item(index, 1);
            icon->setIcon(QIcon("://icons/exclamation-red.png"));

			emit sendMessage(tr("Something occurred"), diagnostics.errorMessage,
                MessageType::Critical);
        }
        }
    }
}
    

void DetectorHandler::setDependency(int index)
{
    if (TDetectors[index]->HasChildren())
    {
        FamilyInfo newParent;
        newParent.detectorID = TDetectors[index]->getDetectorID();
        newParent.detectorType = TDetectors[index]->getDetectorType();

        QList<FamilyInfo> children = TDetectors[index]->GetChildren();

        // Find the appropiate children in the Traffic Detectors and add the
        // corresponding parent
        for (int i = 0; i < TDetectors.size(); ++i)
        {
            for (int j = 0; j < children.size(); ++j)
            {
                if (TDetectors[i]->getDetectorID() == children[j].detectorID)
                {
                    TDetectors[i]->AddParent(newParent);

                    QStandardItem* icon = detectorListModel->item(i, 0);
                    icon->setIcon(QIcon("://illustrations/linkicon.png"));
                }
            }
        }

    }
}

void DetectorHandler::removeDependency(int index)
{
    if (TDetectors[index]->HasChildren())
    {
        FamilyInfo parent;
        
        parent.detectorID = TDetectors[index]->getDetectorID();
        parent.detectorType = TDetectors[index]->getDetectorType();

        QList<FamilyInfo> children = TDetectors[index]->GetChildren();

        // Find the appropiate children in the Traffic Detectors and add the
        // corresponding parent
        for (int i = 0; i < TDetectors.size(); ++i)
        {
            for (int j = 0; j < children.size(); ++j)
            {
                if (TDetectors[i]->getDetectorID() == children[j].detectorID)
                {
                    TDetectors[i]->RemoveParent(parent);

                    if (!TDetectors[i]->HasParents())
                    {
                        QStandardItem* icon = detectorListModel->item(i, 0);
                        icon->setIcon(QIcon("://illustrations/emptyicon.png"));
                    }
                }
            }
        }

    }
}

int DetectorHandler::requestDetectorId()
{
    int proposedId = 1;
    bool uniqueDetectorId = false;

    while (!uniqueDetectorId)
    {
        if (detectorIds.contains(proposedId))
        {
            ++proposedId;
        }
        else {
            uniqueDetectorId = true;
            detectorIds.append(proposedId);
        }
    }

    return proposedId;
}


int DetectorHandler::removeDetectorId(int detectorId)
{
    int indicesRemoved = detectorIds.removeAll(detectorId);

    return indicesRemoved;
}

void DetectorHandler::setOverlayMasks(int index, bool checked)
{
    if (index >= debugPref.OverlayMasks.size() || index == -1)
    {
        debugPref.OverlayMasks.resize(detectorListModel->rowCount());
    }

    if (index < TDetectors.size())
    {
        if (index < debugPref.OverlayMasks.size() && index >= 0)
        {
            debugPref.OverlayMasks[index] = checked;
        }
    }

    if (index == -1)
    {
        // Special case: If index == -1, update all indices
        for (int i = 0; i < debugPref.OverlayMasks.size(); ++i)
        {
            debugPref.OverlayMasks[i] = checked;
        }
    }
}

void DetectorHandler::setOverlayDebugInformation(int index, bool checked)
{
    if (index >= debugPref.OverlayDebugInfo.size() || index == -1)
    {
        debugPref.OverlayDebugInfo.resize(detectorListModel->rowCount());
    }

    if (index < TDetectors.size())
    {
        if (index < debugPref.OverlayDebugInfo.size() && index >= 0)
        {
            debugPref.OverlayDebugInfo[index] = checked;
        }
    }

    if (index == -1)
    {
        // Special case: If index == -1, update all indices
        for (int i = 0; i < debugPref.OverlayDebugInfo.size(); ++i)
        {
            debugPref.OverlayDebugInfo[i] = checked;
        }
    }
}

void DetectorHandler::setOverlayExtDebugInformation(int index, bool checked)
{
    if (index >= debugPref.OverlayExtDebugInfo.size() || index == -1)
    {
        debugPref.OverlayExtDebugInfo.resize(detectorListModel->rowCount());
    }    

    if (index < TDetectors.size())
    {
        if (index < debugPref.OverlayExtDebugInfo.size() && index >= 0)
        {
            debugPref.OverlayExtDebugInfo[index] = checked;
        }
    }

    if (index == -1)
    {
        // Special case: If index == -1, update all indices
        for (int i = 0; i < debugPref.OverlayExtDebugInfo.size(); ++i)
        {
            debugPref.OverlayExtDebugInfo[i] = checked;
        }
    }
}

void DetectorHandler::setShowDebugWindows(int index, bool checked)
{
    if (index >= debugPref.ShowDebugWindows.size() || index == -1)
    {
        debugPref.ShowDebugWindows.resize(detectorListModel->rowCount());
    }

    if (index < TDetectors.size())
    {
        if (index < debugPref.ShowDebugWindows.size() && index >= 0)
        {
            debugPref.ShowDebugWindows[index] = checked;
            TDetectors[index]->SetUseScrollingChart(checked);
        }
    }

    if (index == -1 && (debugPref.ShowDebugWindows.size() == TDetectors.size()))
    {
        // Special case: If index == -1, update all indices
        for (int i = 0; i < debugPref.ShowDebugWindows.size(); ++i)
        {
            debugPref.ShowDebugWindows[i] = checked;
            TDetectors[i]->SetUseScrollingChart(checked);
        }
    }
}

void DetectorHandler::setOverlayTimestamp(bool checked)
{
    debugPref.OverlayTimestamp = checked;
}

int DetectorHandler::addView(ViewInfo view)
{
    // Try to find the modality in the current modalities

    for (int i = 0; i < currentViews.size(); ++i)
    {
        if (currentViews[i].compare(view) == 0)
        {
            // The modality is already enabled. Return
            return 0;
        }
    }

    // Enable the modality
    currentViews.append(view);


    for (auto i = 0; i < TDetectors.size(); ++i) {
        checkDetectorDiagnostics(i, true);
    }

    return 0;
}

int DetectorHandler::removeView(ViewInfo view)
{
    int foundAtIndex = -1;

    // Find the index of the modality in the list of modalities
    for (int i = 0; i < currentViews.size(); ++i)
    {
        if (currentViews[i].compare(view) == 0)
        {
            foundAtIndex = i;
            break;
        }
    }

    currentViews.removeAt(foundAtIndex);

    for (auto i = 0; i < TDetectors.size(); ++i) {
        checkDetectorDiagnostics(i, true);
    }

    return 0;
}