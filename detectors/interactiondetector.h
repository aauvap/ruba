#ifndef INTERACTIONDETECTOR_H
#define INTERACTIONDETECTOR_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "abstractcompounddetector.h"

typedef enum {
    d312,
    d410,
    Flexible
} InteractionType;


class ruba::InteractionDetector : public AbstractCompoundDetector
{

public:
    explicit InteractionDetector(BaseDetectorParams baseParams, QWidget *parent, std::vector<QString> detectorNames, std::vector<ruba::DetectorOutput> detectorParams,
                                 QVector<qint64> consecutiveEventDelays, QString configFile, bool showGui);
    ruba::DetectorOutput GetOutput();

    std::vector<std::vector<ruba::MaskOverlayStruct>> GetMasks();
    qint64 GetConsecutiveEventDelay();
    void SetLastFrameReached();
    void SetUseScrollingChart(bool useScrollingChart);

private:

    void UpdateChartProperties();
    void setDetectorSpecificDefaultParameters();

    QList<QString> getDetectorSpecificDebugInfo();
    QList<QString> getDetectorSpecificExtendedDebugInfo();
    void updateDetectorSpecificLogic();
    std::vector<bool> getDetectorSpecificEnabledStates();

    InteractionType interactionType;

	
    // Settings
    int rightTurningCarID, leftTurningCarID, straightGoingCyclistID;


    // External flags that are accessible through a get function
    ruba::FrameStruct frame;
    QDateTime lastChange;


    // Internal flags and timestamps that helps the detector keep track of the flow
    std::vector<bool> PrevIsTriggered;
    ruba::DetectorOutput turningCarOutput;
    ruba::DetectorOutput straightGoingCyclistOutput;

    qint64 carBeforeCyclistMaxDuration; // The maximum duration (in ms) that is allowed from a car leaves the conflict zone
                                        // until a cyclist enters the conflict zone, if a conflict should be detected
    qint64 cyclistBeforeCarMaxDuration; // The maximum duration (in ms) that is allowed from a cyclist leaves the conflict zone
                                        // until a car enters the conflict zone, if a conflict should be detected

    QDateTime carBeforeCyclistUntil;
    QDateTime cyclistBeforeCarUntil;
    QDateTime carBeforeCyclistTimestamp;
    QDateTime cyclistBeforeCarTimestamp;

    qint64 cyclistEnteringConsecutiveEventDelay; // Minimum amount of time between consecutive trigger events. Events within this time are collated
    QDateTime cyclistEnteringCollateEventsUntil;

    qint64 carEnteringConsecutiveEventDelay; // Minimum amount of time between consecutive trigger events. Events within this time are collated
    QDateTime carEnteringCollateEventsUntil;


};

#endif // INTERACTIONDETECTOR_H
