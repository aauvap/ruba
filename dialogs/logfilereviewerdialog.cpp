#include "dialogs/logfilereviewerdialog.h"
#include "ui_logfilereviewerdialog.h"

LogFileReviewerDialog::LogFileReviewerDialog(QWidget *parent, const std::vector<ruba::FrameStruct>& initFrames) :
    QDialog(parent),
    ui(new Ui::LogFileReviewerDialog)
{
    this->parent = parent;

	logFileProcessor = std::make_shared<LogFileProcessor>(this, 0, 0);

    ui->setupUi(this);
	ui->sourceFileViewer->setModel(logFileProcessor->getSourceTable());
    ui->sourceFileViewer->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->filteredFileViewer->setModel(logFileProcessor->getFilteredTable());
    installEventFilter(this);

	logFileProcessor->enableContinuousPlayback(ui->enableContinuousPlaybackCheckBox->isChecked());

	dataChanged = false;
    // Configure the auto save timer
    autoSaveTimer.setParent(this);
    autoSaveTimer.setInterval(5000); // Run every five seconds
    connect(&autoSaveTimer, SIGNAL(timeout()), this, SLOT(autoSave()));

    // Configure shortcuts that communicate directly with MainWindow
    playbackShortcuts[PlaybtnShortcut::playPause] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_Space, this));
    playbackShortcuts[PlaybtnShortcut::prev5Frames] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_A, this));
    playbackShortcuts[PlaybtnShortcut::prevFrame] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_S, this));
    playbackShortcuts[PlaybtnShortcut::nextFrame] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_D, this));
    playbackShortcuts[PlaybtnShortcut::next5Frames] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_F, this));

	playbackShortcuts[PlaybtnShortcut::fiveSecPrev] = std ::shared_ptr<QShortcut>(new QShortcut(Qt::Key_Q, this));
	playbackShortcuts[PlaybtnShortcut::oneSecPrev] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_W, this));
	playbackShortcuts[PlaybtnShortcut::oneSecForw] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_E, this));
	playbackShortcuts[PlaybtnShortcut::fiveSecForw] = std::shared_ptr<QShortcut>(new QShortcut(Qt::Key_R, this));


	// Connect Main GUI and LogFileReviewerDialog
    QObject::connect(playbackShortcuts[PlaybtnShortcut::playPause].operator ->(),SIGNAL(activated()), this, SLOT(on_playPauseButton_pressed()));
    QObject::connect(playbackShortcuts[PlaybtnShortcut::prev5Frames].operator ->(),SIGNAL(activated()), parent, SLOT(on_videoControl_Prev5Frames_clicked()));
    QObject::connect(playbackShortcuts[PlaybtnShortcut::prevFrame].operator ->(),SIGNAL(activated()), parent, SLOT(on_videoControl_PrevFrame_clicked()));
    QObject::connect(playbackShortcuts[PlaybtnShortcut::nextFrame].operator ->(),SIGNAL(activated()), parent, SLOT(on_videoControl_NextFrame_clicked()));
    QObject::connect(playbackShortcuts[PlaybtnShortcut::next5Frames].operator ->(),SIGNAL(activated()), parent, SLOT(on_videoControl_Next5Frames_clicked()));
    QObject::connect(this, SIGNAL(jumpToFrame(QDateTime)), parent, SLOT(jumpToFrameAtTime(QDateTime)));
    QObject::connect(this, SIGNAL(play()), parent, SLOT(play()));
    QObject::connect(this, SIGNAL(pause()), parent, SLOT(pause()));

	// Connect LogFileProcessor and Main GUI

	qRegisterMetaType<PlaybackSource>();

	// Connect LogFileProcessor and LogFileReviewerDialog
	QObject::connect(logFileProcessor.get(), SIGNAL(updateCurrentEntryUi(QStandardItemModel*, PlaybackSource, int, QDateTime, QDateTime, qint64)), 
		this, SLOT(updateCurrentEntryUi(QStandardItemModel*, PlaybackSource, int, QDateTime, QDateTime, qint64)));
	QObject::connect(logFileProcessor.get(), SIGNAL(setProgressBar(qint64)), this, SLOT(setProgressBar(qint64)));
	QObject::connect(logFileProcessor.get(), SIGNAL(setTimeLabel(QString)), this, SLOT(setTimeLabel(QString)));
	QObject::connect(logFileProcessor.get(), SIGNAL(currentRowChanged(QModelIndex)), this, SLOT(currentRowChanged(QModelIndex)));
	QObject::connect(logFileProcessor.get(), SIGNAL(updateCurrentEntryItem(QModelIndex)), this, SLOT(updateCurrentEntryItem(QModelIndex)));
	QObject::connect(logFileProcessor.get(), SIGNAL(clearRowSelection()), this, SLOT(clearRowSelection()));

    QObject::connect(logFileProcessor.get(), SIGNAL(play()), this, SLOT(startPlayback()));
    QObject::connect(logFileProcessor.get(), SIGNAL(pause()), this, SLOT(pausePlayback()));



    isPlaying = false;

    ui->currentEntryEventView->setModel(logFileProcessor->getCurrentEntryEventTable());
    ui->currentEntryInfoView->setModel(logFileProcessor->getCurrentEntryInfoTable());

	logFileProcessor->getCurrentEntryEventTable()->setHorizontalHeaderLabels(QStringList{ QString("Seconds to event") });

    // Remember settings
    QSettings settings;
    settings.beginGroup("logfilereviewer");

    ui->startEarlierSpinBox->setValue(settings.value("startEarlierSpinBox", 0).toInt());
    ui->endAfterSpinBox->setValue(settings.value("endAfterSpinBox", 0).toInt());
    ui->enableContinuousPlaybackCheckBox->setChecked(settings.value("enableContinuousPlaybackCheckBox", 1).toBool());

    settings.endGroup();
}

bool LogFileReviewerDialog::eventFilter(QObject* object, QEvent* event)
{
    // Some code thanks to http://www.qtforum.org/article/19166/can-t-stop-delete-key-on-qtableview.html

    if (event->type() != QEvent::KeyPress)
    {
        // default processing for non-keystrokes
        return false;
    }

    QKeyEvent* pKeyEvent = reinterpret_cast<QKeyEvent*>(event);
    qDebug() << "Hello from key event";
    switch (pKeyEvent->key())
    {
    case Qt::Key_Delete:
    {
        deleteSelected();
        event->accept();
        return true;
    }
    case Qt::Key_Insert:
    {
        insertSelected();
        event->accept();
        return true;
    }
    }

    qDebug() << "Passing event " << pKeyEvent->key();
    return QObject::eventFilter(object, event);
}



LogFileReviewerDialog::~LogFileReviewerDialog()
{
    // Remember settings
    QSettings settings;
    settings.beginGroup("logfilereviewer");

    settings.setValue("startEarlierSpinBox", ui->startEarlierSpinBox->value());
    settings.setValue("endAfterSpinBox", ui->endAfterSpinBox->value());
    settings.setValue("enableContinuousPlaybackCheckBox", ui->enableContinuousPlaybackCheckBox->isChecked());

    settings.endGroup();
    
    delete ui;
}

void LogFileReviewerDialog::updateTime(QDateTime time)
{
	logFileProcessor->updateTime(time, isPlaying);
}

int LogFileReviewerDialog::openSourceLog(const QString& filename)
{
    if(!filename.isEmpty() && filename.contains(".csv"))
    {
        QFile file(filename);

        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {

            QTextStream fileStr(&file);

            // Read header of .csv-file
            QString line = fileStr.readLine();
            headerLabels = line.split(';');
			logFileProcessor->setHeaderLabels(headerLabels);

            if (headerLabels.size() == 0) {
                qDebug() << "Log headers of " << filename << " are empty";
                return 1;
            }

            logFileProcessor->getSourceTable()->setHorizontalHeaderLabels(headerLabels);
			logFileProcessor->getFilteredTable()->setHorizontalHeaderLabels(headerLabels);

            // Clear table entries, if any
			logFileProcessor->getSourceTable()->setRowCount(0);

            QStringList sourceList;
            int rowNumber = 0;

            // Read the rest of the log file
            while (!fileStr.atEnd()) {
                line = fileStr.readLine();
                sourceList = (line.split(';'));

                // Insert the entries in the table
                for (int i = 0; i < std::min(sourceList.size(), headerLabels.size()); ++i)
                {
					QStandardItem *item = new QStandardItem();

                    if (!sourceList[i].isEmpty()) {
						item->setText(sourceList[i]);
					}

					logFileProcessor->getSourceTable()->setItem(rowNumber, i, item);
                }

                rowNumber++;
            }

			// Post-process the entries such that only one timestamps per row is allowed
			auto sourceTable = logFileProcessor->getSourceTable();
			QMap<QDateTime, DateTimeEventInfo> sortedEvents;

			for (int i = 0; i < sourceTable->rowCount(); ++i)
			{
				// Retrieve the current row from the table
				QList<QStandardItem*> row;

				for (auto j = 0; j < sourceTable->columnCount(); ++j)
				{
					if (sourceTable->item(i, j) != nullptr)
					{
						row.push_back(sourceTable->item(i, j));
					}
		
				}

				EntryInterval parsedRowInfo(row, 0, 0, i, headerLabels);
				auto eventDateTimes = parsedRowInfo.getEntryEventDateTimes();
				auto entryInfo = parsedRowInfo.getEntryInfo();

				for (auto& eventDateTime : eventDateTimes)
				{
					int dateTimeColumnNumber = headerLabels.indexOf(eventDateTime.first);

					auto eventInfo = DateTimeEventInfo(eventDateTime.second, eventDateTime.first, entryInfo, i, dateTimeColumnNumber);
					sortedEvents.insert(eventDateTime.second, eventInfo);
				}
			}

			// Iterate through the map to populate the new, sorted table
			auto sortedTable = logFileProcessor->getSortedSourceTable();
            sortedTable->clear();
			sortedTable->setHorizontalHeaderLabels(headerLabels);

			QMapIterator<QDateTime, DateTimeEventInfo> it(sortedEvents);
			while (it.hasNext())
			{
				it.next();
				
				auto dateTime = it.key();
				auto eventInfo = it.value();

				// Populate the row
				QList<QStandardItem*> row;

				for (auto j = 0; j < headerLabels.size(); ++j)
				{
					// Try to find the right information that fits to the current header
					QStandardItem* item = new QStandardItem();

					if (headerLabels[j] == eventInfo.getHeader())
					{
						item->setText(eventInfo.getDateTime().toString("HH:mm:ss.zzz"));
					}
					else {
						// Try to find one of the non-time stamp information that fits
						// the current header
						bool matchFound = false;

						auto additionalEventInfo = eventInfo.getAdditionalEntryInfo();

						for (auto k = 0; k < additionalEventInfo.size(); k++)
						{
							if (additionalEventInfo[k].first == headerLabels[j])
							{
								matchFound = true;
								item->setText(additionalEventInfo[k].second);
							}
						}
					}

					row.push_back(item);
				}

				sortedTable->appendRow(row);
			}

        } else {
            qDebug() << file.errorString();
            return 1;
        }
    }
    return 0;
}

int LogFileReviewerDialog::openFilteredLog(const QString & filename)
{
    if (!filename.isEmpty() && filename.contains(".csv"))
    {
        QFile file(filename);

        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {

            QTextStream fileStr(&file);

            // Read header of .csv-file
            QString line = fileStr.readLine();
            QStringList headerLabels = line.split(';');

            if (headerLabels.size() == 0) {
                qDebug() << "Log headers of " << filename << " are empty";
                return 1;
            }

			logFileProcessor->getFilteredTable()->setHorizontalHeaderLabels(headerLabels);
			logFileProcessor->setHeaderLabels(headerLabels);

            // Clear table entries, if any
			logFileProcessor->getFilteredTable()->setRowCount(0);

            QStringList filteredList;
            int rowNumber = 0;

            // Read the rest of the log file
            while (!fileStr.atEnd()) {
                line = fileStr.readLine();
                filteredList = (line.split(';'));

                // Insert the entries in the table
                for (int i = 0; i < std::min(filteredList.size(), headerLabels.size()); ++i)
                {
                    if (!filteredList[i].isEmpty()) {
                        QStandardItem *item = new QStandardItem(filteredList[i]);
						logFileProcessor->getFilteredTable()->setItem(rowNumber, i, item);
                    }
                }

                rowNumber++;
            }

        }
        else {
            qDebug() << file.errorString();
            return 1;
        }
    }
    return 0;
}

void LogFileReviewerDialog::saveFilteredLog()
{
    // Overwrites the existing log file and replaces it with the new log table

    QString savePath = ui->filteredFilePath->text();
    QFile file(savePath);

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        // Write header of .csv-file
        for (int i = 0; i < headerLabels.size(); ++i)
        {
            stream << headerLabels[i] << ";";
        }
        stream << endl;


        for(int i = 0; i < logFileProcessor->getFilteredTable()->rowCount(); ++i)
        {
            for (int j = 0; j < logFileProcessor->getFilteredTable()->columnCount(); ++j)
            {
                QStandardItem* item = logFileProcessor->getFilteredTable()->item(i, j);
                QString itemText = "";

                if (item != NULL)
                {
                    itemText = item->text();
                }

                stream << itemText + ";";
            }
            stream << endl;
        }

        stream.flush();
        file.close();
    }

    ui->filteredFileViewer->setFocus();
}

void LogFileReviewerDialog::deleteSelected()
{
    // Get current index
    QModelIndexList indexes = ui->filteredFileViewer->selectionModel()->selectedRows();

    while (!indexes.isEmpty())
    {
		logFileProcessor->getFilteredTable()->removeRows(indexes.last().row(), 1);
        indexes.removeLast();
    }

    dataChanged = true;
}

void LogFileReviewerDialog::insertSelected()
{
    // Get current index
    QModelIndexList indexes = ui->sourceFileViewer->selectionModel()->selectedRows();

    while (!indexes.isEmpty())
    {
        QList<QStandardItem*> copiedRow;

        for (auto i = 0; i < logFileProcessor->getSourceTable()->columnCount(); ++i)
        {
            QString text = logFileProcessor->getSourceTable()->item(indexes.first().row(), i) ? logFileProcessor->getSourceTable()->item(indexes.first().row(), i)->text() : "";
            copiedRow.append(new QStandardItem(text));
        }

		logFileProcessor->getFilteredTable()->appendRow(copiedRow);
        indexes.removeFirst();
    }

    dataChanged = true;
}

void LogFileReviewerDialog::on_loadSourceFileButton_clicked()
{
	QSettings settings;
	settings.beginGroup("logfiles");
	QString logFileDir = settings.value("logFileDir", "").toString();
	settings.endGroup();

    QString filename = QFileDialog::getOpenFileName(this, tr("Open existing log file"), logFileDir, tr("Log files (*.csv)"));

    int returnCode = openSourceLog(filename);

    if (returnCode == 0) {
		QSettings settings;
		QString logFileDir = QFileInfo(filename).absolutePath();
		settings.beginGroup("logfiles");
		settings.setValue("logFileDir", logFileDir);
		settings.endGroup();

        ui->sourceFilePath->setText(filename);
    }
}

void LogFileReviewerDialog::on_loadFilteredButton_clicked()
{
    if (dataChanged)
    {
        int reply = QMessageBox::question(this, "Save current log", "Do you want to save the filtered log?\nIf not, changes to the filtered log will be lost.",
            QMessageBox::Yes | QMessageBox::No);

        if (reply == QMessageBox::Yes) {
            if (ui->sourceFilePath->text().isEmpty()) {
                on_saveFilteredAsButton_clicked();
            }
            else {
                on_saveFilteredButton_clicked();
            }
        }
    }

	QSettings settings;
	settings.beginGroup("logfiles");
	QString logFileDir = settings.value("logFileDir", "").toString();
	settings.endGroup();

    QString filename = QFileDialog::getOpenFileName(this, tr("Open existing log file"), logFileDir, tr("Log files (*.csv)"));

    int returnCode = openFilteredLog(filename);

    if (returnCode == 0) {
		QSettings settings;
		QString logFileDir = QFileInfo(filename).absolutePath();
		settings.beginGroup("logfiles");
		settings.setValue("logFileDir", logFileDir);
		settings.endGroup();

        ui->filteredFilePath->setText(filename);
        ui->autoSaveCheckBox->setEnabled(true); // Enable the auto-save check box
        ui->saveFilteredButton->setEnabled(true);
    }
}

void LogFileReviewerDialog::on_saveFilteredButton_clicked()
{
    if (ui->filteredFilePath->text().isEmpty()) {
        on_saveFilteredAsButton_clicked();
    }
    else {
        saveFilteredLog();
    }
}

void LogFileReviewerDialog::on_saveFilteredAsButton_clicked()
{
	QSettings settings;
	settings.beginGroup("logfiles");
	QString logFileDir = settings.value("logFileDir", "").toString();
	settings.endGroup();

    QString filename = QFileDialog::getSaveFileName(this, tr("Save log file as"), logFileDir, tr("Log files (*.csv)"));

    if (!filename.isEmpty())
    {
		QSettings settings;
		QString logFileDir = QFileInfo(filename).absolutePath();
		settings.beginGroup("logfiles");
		settings.setValue("logFileDir", logFileDir);
		settings.endGroup();

        ui->filteredFilePath->setText(filename);
        ui->autoSaveCheckBox->setEnabled(true); // Enable the auto-save check box
        ui->saveFilteredButton->setEnabled(true);

        saveFilteredLog();
    }

    ui->filteredFileViewer->setFocus();
}

void LogFileReviewerDialog::on_autoSaveCheckBox_clicked(bool checked)
{
    if (checked)
    {
        autoSaveTimer.start();
    }
    else
    {
        autoSaveTimer.stop();
    }
}

void LogFileReviewerDialog::setProgressBar(qint64 msecsFromStartTime)
{
	ui->progressBar->setValue(msecsFromStartTime);
}

void LogFileReviewerDialog::setTimeLabel(const QString & timeLabel)
{
	ui->currentTimeLabel->setText(timeLabel);
}

void LogFileReviewerDialog::currentRowChanged(QModelIndex index)
{
	ui->currentEntryEventView->setCurrentIndex(index);
}

void LogFileReviewerDialog::clearRowSelection()
{
	ui->currentEntryEventView->clearSelection();
}

void LogFileReviewerDialog::updateCurrentEntryItem(const QModelIndex & index)
{
	ui->currentEntryEventView->update(index);
}

void LogFileReviewerDialog::autoSave()
{
    if (dataChanged)
    {
        qDebug() << "AutoSave is saving...";
        dataChanged = false;
        saveFilteredLog();
    } else {
        qDebug() << "AutoSave has skipped...";
    }
}

void LogFileReviewerDialog::on_enableContinuousPlaybackCheckBox_clicked(bool checked)
{
	logFileProcessor->enableContinuousPlayback(ui->enableContinuousPlaybackCheckBox->isChecked());
}

void LogFileReviewerDialog::on_startEarlierSpinBox_valueChanged()
{
	logFileProcessor->setStartEarlier(ui->startEarlierSpinBox->value());
}

void LogFileReviewerDialog::on_endAfterSpinBox_valueChanged()
{
	logFileProcessor->setEndAfter(ui->endAfterSpinBox->value());
}

void LogFileReviewerDialog::on_sourceFileViewer_doubleClicked(const QModelIndex & index)
{
	// Find out if the oneTimeStampPerRowRadioButton is toggled. If so, we should show the sorted source log
	PlaybackSource desiredPlaybackTable = PlaybackSource::SourceFile;

	if (ui->oneTimeStampPerRowRadioButton->isChecked())
	{
		desiredPlaybackTable = SortedSourceFile;
	}

    if (currentPlaybackTable != desiredPlaybackTable)
    {
        currentPlaybackTable = desiredPlaybackTable;
    }

	logFileProcessor->setCurrentPlaybackTable(desiredPlaybackTable);
	logFileProcessor->setCurrentEntry(index.row());
	updateCurrentEntryUi(logFileProcessor->getCurrentPlaybackTable(), desiredPlaybackTable, index.row(),
		logFileProcessor->getStartTime(), logFileProcessor->getEndTime(), logFileProcessor->getInterval());
		
}

void LogFileReviewerDialog::on_filteredFileViewer_doubleClicked(const QModelIndex & index)
{
    if (currentPlaybackTable != FilteredFile)
    {
		currentPlaybackTable = FilteredFile;
    }

	logFileProcessor->setCurrentPlaybackTable(FilteredFile);
	logFileProcessor->setCurrentEntry(index.row());
	updateCurrentEntryUi(logFileProcessor->getCurrentPlaybackTable(), SourceFile, index.row(),
		logFileProcessor->getStartTime(), logFileProcessor->getEndTime(), logFileProcessor->getInterval());
}

void LogFileReviewerDialog::on_progressBar_sliderReleased()
{
    QDateTime desiredDateTime = logFileProcessor->getStartTime().addMSecs(ui->progressBar->value());

    emit jumpToFrame(desiredDateTime);
}

void LogFileReviewerDialog::on_playPauseButton_pressed()
{
    if (isPlaying)
    {
        pausePlayback();
    }
    else {
        startPlayback();
    }
}

void LogFileReviewerDialog::pausePlayback()
{
    isPlaying = false;
    ui->playPauseButton->setIcon(QIcon("://icons/control.png"));
    emit pause();
}

void LogFileReviewerDialog::startPlayback()
{
    isPlaying = true;
    ui->playPauseButton->setIcon(QIcon("://icons/control-pause.png"));
    emit play();
}


void LogFileReviewerDialog::on_deleteLogEntryButton_pressed()
{
    deleteSelected();
}

void LogFileReviewerDialog::on_insertLogEntryButton_pressed()
{
    insertSelected();
}

void LogFileReviewerDialog::on_filteredTable_rowsInserted(const QModelIndex & parent, int first, int last)
{
    dataChanged = true;
}

void LogFileReviewerDialog::on_oneTimeStampPerRowRadioButton_toggled()
{
	if (ui->oneTimeStampPerRowRadioButton->isChecked())
	{
		ui->sourceFileViewer->setModel(logFileProcessor->getSortedSourceTable());
	}
	else {
		ui->sourceFileViewer->setModel(logFileProcessor->getSourceTable());
	}
}


void LogFileReviewerDialog::updateCurrentEntryUi(QStandardItemModel* table, PlaybackSource view, 
	int rowNumber, const QDateTime& startTime, const QDateTime& endTime, qint64 intervalInMsecs)
{
    // Set the UI information
    ui->currentTimeLabel->setText(startTime.toString("hh:mm:ss.zzz"));
    ui->endTimeLabel->setText(endTime.toString("hh:mm:ss.zzz"));
    ui->progressBar->setMaximum(intervalInMsecs);
    ui->progressBar->setValue(0);

    QString groupBoxInfo;

	switch (currentPlaybackTable){
	case PlaybackSource::SourceFile:
	{
		groupBoxInfo = "Original log: ";
		break;
	}
	case PlaybackSource::SortedSourceFile:
	{
		groupBoxInfo = "Original sorted log: ";
		break;
	}
	case PlaybackSource::FilteredFile:
		groupBoxInfo = "Filtered log: ";
		break;
	}

    groupBoxInfo += QString("Entry %1 of %2").arg(rowNumber+1).arg(table->rowCount());
    ui->currentEntryGroupBox->setTitle(groupBoxInfo);

    // Highlight the current row in the table
    QModelIndex index = table->index(rowNumber, 0);

	switch (view)
	{
	case PlaybackSource::SourceFile:
	{
		ui->sourceFileViewer->setCurrentIndex(index);
		break;
	}
	case PlaybackSource::FilteredFile:
	{
		ui->filteredFileViewer->setCurrentIndex(index);
		break;
	}
	case PlaybackSource::SortedSourceFile:
		ui->sourceFileViewer->setCurrentIndex(index);
		break;
	}

    // Jump to the start time of the interval
    // Temporarily set the isCreatingEntry flag to stop is from creating yet another entry 
    // if we cannot find the requested time in the video list
	logFileProcessor->updateTime(startTime, true);
    emit jumpToFrame(startTime);
}

EntryInterval::EntryInterval()
{
    startTime = QDateTime();
    endTime = QDateTime();

    rowNumber = -1;

    timestampFormat = dateTimeFormat::yyyy_MM_DD_hh_mm_ss_zzz;
}

EntryInterval::EntryInterval(const QList<QStandardItem*>& row, int secondsBefore, int secondsAfter, int rowNumber, QStringList headerLabels)
{
	QStringList formattedRow;

	for (auto& item : row)
	{
		formattedRow.push_back(item->text());
	}

	timestampFormat = dateTimeFormat::yyyy_MM_DD_hh_mm_ss_zzz;
	setInterval(row, secondsBefore, secondsAfter, rowNumber, headerLabels);
}

EntryInterval::EntryInterval(const QStringList & row, int secondsBefore, int secondsAfter, int rowNumber, QStringList headerLabels)
{
	startTime = QDateTime();
	endTime = QDateTime();

	rowNumber = -1;

	timestampFormat = dateTimeFormat::yyyy_MM_DD_hh_mm_ss_zzz;

	setInterval(row, secondsBefore, secondsAfter, rowNumber, headerLabels);
}

void EntryInterval::setInterval(const QList<QStandardItem*>& row, int secondsBefore, int secondsAfter, int rowNumber, QStringList headerLabels)
{
	QStringList formattedRow;
	
	for (auto &item : row)
	{
		formattedRow.push_back(item->text());
	}

	setInterval(formattedRow, secondsBefore, secondsAfter, rowNumber, headerLabels);
}

void EntryInterval::setInterval(const QStringList row, int secondsBefore, int secondsAfter, int rowNumber, QStringList headerLabels)
{
	this->headerLabels = headerLabels;

	getIntervalFromRow(row, startTime, endTime);

	startTime = startTime.addSecs(secondsBefore * (-1));
	endTime = endTime.addSecs(secondsAfter);

	qDebug() << "StartTime: " << startTime.toString("yyyy MM dd hh:mm:ss.zzz");
	qDebug() << "EndTime: " << endTime.toString("yyyy MM dd hh:mm:ss.zzz");
	qDebug() << "rowNumber: " << rowNumber;

	this->rowNumber = rowNumber;
}

void EntryInterval::update(const QDateTime & currentTime, qint64 & msecsFromStartTime, 
    std::vector<std::pair<QString, QString> >& timeToEvents,
    std::vector<std::pair<QString, QString> >& entryInfo, 
    int& lastEventNumber)
{
    msecsFromStartTime = startTime.msecsTo(currentTime);
    entryInfo = this->entryInfo;

    timeToEvents.resize(entryEventDateTimes.size());
    std::pair<int, qint64> closestPreviousEvent = std::make_pair(-1, -1);

    for (auto i = 0; i < entryEventDateTimes.size(); ++i)
    {
        qint64 msecsToEvent = currentTime.msecsTo(entryEventDateTimes[i].second);
        qint64 secondsToEvent = currentTime.secsTo(entryEventDateTimes[i].second);
        qint64 displayMsecsToEvent = abs(msecsToEvent - (secondsToEvent * 1000));
        QString secondsSign = ((msecsToEvent < 0) && (secondsToEvent >= 0)) ? "-" : "";

        timeToEvents[i] = std::make_pair(entryEventDateTimes[i].first, QString("%1%2.%3")
            .arg(secondsSign)
            .arg(secondsToEvent)
            .arg(displayMsecsToEvent));

        if (msecsToEvent <= 0)
        {
            // The event is currently happening, or is in the past

            if (closestPreviousEvent.first == -1)
            {
                closestPreviousEvent = std::make_pair(i, msecsToEvent);
            }

            if (msecsToEvent > closestPreviousEvent.second)
            {
                // Event is closer. Remember, we are operating on negative numbers
                closestPreviousEvent = std::make_pair(i, msecsToEvent);
            }
        }
    }

    lastEventNumber = closestPreviousEvent.first;
}

bool EntryInterval::isDateTimeInInterval(const QDateTime & currentTime) const
{
    return ((startTime <= currentTime) && (currentTime <= endTime));
}

bool EntryInterval::isDateTimeBeforeInterval(const QDateTime& currentTime) const
{
    return currentTime < startTime;
}

bool EntryInterval::isDateTimeAfterInterval(const QDateTime & currentTime) const
{
	return currentTime > endTime;
}

void EntryInterval::getIntervalFromRow(const QStringList& row, QDateTime & earliestTime, QDateTime & latestTime)
{
    earliestTime = QDateTime();
    latestTime = QDateTime();
    entryEventDateTimes.clear();
    entryInfo.clear();

    QDate currentDate;
    
    for (int i = 0; i < row.count(); ++i)
    {
        QDateTime time = getDateTimeFromEntry(row[i], timestampFormat, true, currentDate);

        if (!time.isValid())
        {
            // Try to get the date instead
            QDate date = getDateFromEntry(row[i]);

            if (date.isValid())
            {
                currentDate = date;
            }
        }

        if (time.isValid())
        {
            if (!earliestTime.isValid())
            {
                earliestTime = QDateTime(time);
            }

            if (!latestTime.isValid())
            {
                latestTime = QDateTime(time);
            }

            if (time < earliestTime)
            {
                earliestTime = QDateTime(time);
            }

            if (time > latestTime)
            {
                latestTime = QDateTime(time);
            }

            QString label = headerLabels.size() > i ? headerLabels[i] : "";

            entryEventDateTimes.push_back(std::make_pair(label, time));
        }
        else {
            QString label = headerLabels.size() > i ? headerLabels[i] : "";

            entryInfo.push_back(std::make_pair(label, row[i]));
        }
    }

    return;
}

QDateTime EntryInterval::getDateTimeFromEntry(const QString& text, dateTimeFormat format, bool recursive,
    const QDate& latestDate)
{
    // Try the default dateTimeFormat
    QDateTime entryTime;
    
    switch (format)
    {
	case dateTimeFormat::hh_mm_ss_zzz:
    {
        QTime time = QTime::fromString(text, "HH:mm:ss.zzz");

        if (!time.isValid() && recursive)
        {
            format = yyyy_MM_DD_hh_mm_ss_zzz;
            entryTime = getDateTimeFromEntry(text, format, true, latestDate);
        }
        else if (time.isValid() && latestDate.isValid())
        {
            // Set the date
            entryTime.setDate(latestDate);
            entryTime.setTime(time);
        }

        break;
    }
	case dateTimeFormat::yyyy_MM_DD_hh_mm_ss_zzz:
    {
        entryTime = QDateTime::fromString(text, "yyyy MM dd HH:mm:ss.zzz");

        if (!entryTime.isValid() && recursive)
        {
            format = hh_mm_ss;
            entryTime = getDateTimeFromEntry(text, format, true, latestDate);
        }
        break;
    }
	case dateTimeFormat::hh_mm_ss:
    {
        QTime time = QTime::fromString(text, "HH:mm:ss");

        if (!time.isValid() && recursive)
        {
            format = hh_mm_ss_zzz;
            entryTime = getDateTimeFromEntry(text, format, false, latestDate);
        }
        else if (time.isValid() && latestDate.isValid())
        {
            // Set the date
            entryTime.setDate(latestDate);
            entryTime.setTime(time);
        }

        break;
    }
    }

    if (entryTime.isValid())
    {
        timestampFormat = format;
    }
    
    return entryTime;
}

QDate EntryInterval::getDateFromEntry(const QString& text)
{
    auto date = QDate::fromString(text, "dd-MM-yyyy");

	if (!date.isValid())
	{
		date = QDate::fromString(text, "yyyy MM dd");
	}

	return date;

}

LogFileProcessor::LogFileProcessor(QObject *parent, int startEarlier, int endAfter)
	: QObject(parent)
{
	this->startEarlier = startEarlier;
	this->endAfter = endAfter;

	sourceTable = new QStandardItemModel();
	filteredTable = new QStandardItemModel();
	sortedSourceTable = new QStandardItemModel();

	sourceTable->setRowCount(0);
	filteredTable->setRowCount(0);
	sortedSourceTable->setRowCount(0);

	isCreatingEntry = false;

	currentEntryEventTable = new QStandardItemModel();
	currentEntryInfoTable = new QStandardItemModel();

	currentPlaybackTable = nullptr;
}

void LogFileProcessor::updateTime(QDateTime time, bool isPlaying)
{
	currentTime = time;
	previousTime = currentTime;

	if (isPlaying)
	{
		if (!currentEntry.isDateTimeInInterval(time) && !isCreatingEntry && !currentEntry.isDateTimeBeforeInterval(time))
		{
            qDebug() << "Current entry start: " << currentEntry.getStartTime().toString("dd-MM-yyyy hh:mm:ss.zzz");
            qDebug() << "Current entry end: " << currentEntry.getEndTime().toString("dd-MM-yyyy hh:mm:ss.zzz");
            qDebug() << "Current time: " << time.toString("dd-MM-yyyy hh:mm:ss.zzz");
			isCreatingEntry = true;

			if ((currentPlaybackTable == nullptr) || (currentTable == Undefined))
			{
				// Revert to default
				currentPlaybackTable = sourceTable;
				currentTable = SourceFile;
			}

			bool success = false;
			int currentRow = currentEntry.getRowNumber();

			while (!success)
			{
				// Create a new interval
				currentRow = currentEntry.getRowNumber();

				if ((currentEntry.getRowNumber() + 1) < sourceTable->rowCount())
				{
					currentRow++;
				}
				else {
					// We have an empty table, or we have traversed the entire table. Pause the playback
					emit pause();
					return;
				}

				success = setCurrentEntry(currentRow);
			}


			emit updateCurrentEntryUi(currentPlaybackTable, currentTable, currentRow,
				currentEntry.getStartTime(), currentEntry.getEndTime(), 
				currentEntry.getIntervalInMsecs());

		}
		else {
			// The current time is inside the interval. Update the GUI
			isCreatingEntry = false;

			qint64 msecsFromStartTime;
			std::vector<std::pair<QString, QString> > timeToEvents;
			std::vector<std::pair<QString, QString> > entryInfo;
			int closestEvent;

			currentEntry.update(currentTime, msecsFromStartTime, timeToEvents, entryInfo, closestEvent);

			// Update the current entry info
			emit setProgressBar(msecsFromStartTime);
			emit setTimeLabel(currentTime.toString("hh:mm:ss.zzz"));
			//ui->progressBar->setValue(msecsFromStartTime);
			//ui->currentTimeLabel->setText(currentTime.toString("hh:mm:ss.zzz"));

			// Grow to fit
			while (currentEntryEventTable->rowCount() < timeToEvents.size())
			{
				currentEntryEventTable->appendRow(new QStandardItem());
			}

			// Shrink to fit
			while (currentEntryEventTable->rowCount() > timeToEvents.size())
			{
				currentEntryEventTable->removeRow(currentEntryEventTable->rowCount() - 1);
			}

			QStringList headers;

			for (auto i = 0; i < timeToEvents.size(); ++i)
			{
				currentEntryEventTable->item(i)->setText(timeToEvents[i].second);
				emit updateCurrentEntryItem(currentEntryEventTable->index(i, 0));
				
				headers.push_back(timeToEvents[i].first);
			}

			currentEntryEventTable->setVerticalHeaderLabels(headers);



			if (closestEvent >= 0 && closestEvent < currentEntryEventTable->rowCount())
			{
				// Highlight the current row in the table
				QModelIndex index = currentEntryEventTable->index(closestEvent, 0);
				//ui->currentEntryEventView->setCurrentIndex(index);
				emit currentRowChanged(index);
			}
			else {
				//ui->currentEntryEventView->clearSelection();
				emit clearRowSelection();
			}

			headers.clear();


			// Grow to fit
			while (currentEntryInfoTable->rowCount() < entryInfo.size())
			{
				currentEntryInfoTable->appendRow(new QStandardItem());
			}

			// Shrink to fit
			while (currentEntryInfoTable->rowCount() > entryInfo.size())
			{
				currentEntryInfoTable->removeRow(currentEntryInfoTable->rowCount() - 1);
			}

			for (auto i = 0; i < entryInfo.size(); ++i)
			{
				currentEntryInfoTable->item(i)->setText(entryInfo[i].second);
				headers.push_back(entryInfo[i].first);
			}

			currentEntryInfoTable->setVerticalHeaderLabels(headers);
		}
	}
}

void LogFileProcessor::enableContinuousPlayback(bool value)
{
	continuousPlayBack = value;
}

void LogFileProcessor::setCurrentPlaybackTable(PlaybackSource source)
{
	currentTable = source;

	switch (source)
	{
	case PlaybackSource::SourceFile:
		currentPlaybackTable = sourceTable;
		break;
	case PlaybackSource::SortedSourceFile:
		currentPlaybackTable = sortedSourceTable;
		break;
	case PlaybackSource::FilteredFile:
		currentPlaybackTable = filteredTable;
		break;
	}
}

bool LogFileProcessor::setCurrentEntry(int currentRow)
{
	// Retrieve the current row from the table
	QList<QStandardItem*> row;

	for (auto i = 0; i < currentPlaybackTable->columnCount(); ++i)
	{
		if (currentPlaybackTable->item(currentRow, i) != 0)
		{
			row.push_back(currentPlaybackTable->item(currentRow, i));
		}
	}

	// Set the interval from the chosen row
	previousEntry = currentEntry;
	currentEntry.setInterval(row, startEarlier, endAfter, currentRow, headerLabels);

	if (continuousPlayBack)
	{
		// Check if the current time is in the interval of the new, current entry
		if (currentEntry.isDateTimeInInterval(currentTime))
		{
			currentEntry.setStartTime(currentTime);
		}
		else if (currentEntry.isDateTimeAfterInterval(currentTime) && 
			currentEntry.getStartTime() >= previousEntry.getStartTime())
		{
			// The current entry has already ended. This means that it
			// contains timestamps that are equal to the last event or
			// that it has ended before the last entry
			// Therefore, waste no time playing back this item.
			return false;
		}
	}

	return true;
}

DateTimeEventInfo::DateTimeEventInfo(const QDateTime & dateTime, 
									 const QString & header, 
									 const std::vector<std::pair<QString, QString>>& additionalEntryInfo, 
									 const int rowNumber, 
									 const int dateTimeColumnNumber)
{
	this->dateTime = dateTime;
	this->header = header;
	this->additionalEntryInfo = additionalEntryInfo;
	this->rowNumber = rowNumber;
	this->dateTimeColumnNumber = dateTimeColumnNumber;
}
