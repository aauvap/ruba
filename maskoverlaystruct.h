#ifndef MASKOVERLAYSTRUCT_H
#define MASKOVERLAYSTRUCT_H

#include <opencv2/opencv.hpp>
#include "ruba.h"
#include "viewinfo.h"

struct ruba::MaskOverlayStruct{
    cv::Mat Mask;
	double Alpha;
	cv::Scalar Color;
	int DetectorType;
    int DetectorID;
    QString detectorIdText;
    ViewInfo View;
};

struct ruba::MaskOutlineStruct {
    cv::Mat MaskOutline;
    double Alpha;
    cv::Scalar Color;
    int DetectorType;
    int DetectorID;
    ViewInfo View;
};


#endif // MASKOVERLAYSTRUCT_H
