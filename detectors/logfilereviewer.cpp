#include "detectors/logfilereviewer.h"


using namespace std;
using namespace cv;
using namespace ruba;


LogFileReviewer::LogFileReviewer(BaseDetectorParams baseParams, QWidget *parent)
    : AbstractCompoundDetector(configureBaseDetectorParams(baseParams, LOGFILEREVIEWER))
{
    this->IsInitialized = true;

    lfrDialog = new LogFileReviewerDialog(parent, baseParams.initFrames);
    lfrDialog->updateTime(getCurrentTime());

    lfrDialog->show();

    IsTriggered.clear();
    IsTriggered.push_back(false);
}


LogFileReviewer::~LogFileReviewer()
{
    lfrDialog->close();
    delete lfrDialog;
}


DetectorOutput LogFileReviewer::GetOutput()
{
    DetectorOutput outputStruct;
    return outputStruct;
}

void LogFileReviewer::setDetectorSpecificDefaultParameters()
{
}

void LogFileReviewer::SetLastFrameReached()
{
    // Do nothing.
}

void LogFileReviewer::Reconfigure(vector<FrameStruct> initFrames, QWidget *parent)
{
    lfrDialog->show();
    lfrDialog->raise();

}

void LogFileReviewer::updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput)
{
    lfrDialog->updateTime(getCurrentTime());
}

void LogFileReviewer::updateDetectorSpecificLogic()
{
    // Do nothing. The call to this detector is overwritten by the updateDetectorSpecifics method
}

QList<QString> LogFileReviewer::getDetectorSpecificExtendedDebugInfo()
{
    QList<QString> extDebugList;

    return extDebugList;
}

std::vector<std::vector<MaskOverlayStruct> > LogFileReviewer::GetMasks()
{
    return std::vector<std::vector<MaskOverlayStruct> >();
}




