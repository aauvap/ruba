#include "logger.h"
#include <QDebug>

using namespace cv;
using namespace std;
using namespace ruba;

LogEvent::LogEvent(const QDateTime &enteringTime,
    const std::vector<FrameStruct> &currentFrames,
    const QString &detectorIdText,
    const std::vector<Transition> &saveFrameOfEventSettings,
    const std::vector<ruba::MaskOutlineStruct>& maskOutline)
{
    this->enteringTime = enteringTime;
    this->detectorIdText = detectorIdText;
    this->maskOutline = maskOutline;

    this->leavingTime = QDateTime();

    this->saveFrameOfEventSettings = saveFrameOfEventSettings;

	bool enteringOrOverlap = (std::find(this->saveFrameOfEventSettings.begin(),
		this->saveFrameOfEventSettings.end(), ENTERING) !=
		this->saveFrameOfEventSettings.end() ||
		std::find(this->saveFrameOfEventSettings.begin(),
			this->saveFrameOfEventSettings.end(), OVERLAP) !=
		this->saveFrameOfEventSettings.end());

    if (enteringOrOverlap)
    {
        // We have specified that we want to save a frame of the 
        // entering event
        std::vector<FrameStruct> enteringEvent = currentFrames;

        for (auto i = 0; i < currentFrames.size(); ++i)
        {
            enteringEvent[i].Image = currentFrames[i].Image.clone();
        }

        eventFrames[ENTERING] = enteringEvent;
    }

    qDebug() << "Creating event for detector " << this->detectorIdText << "at: " << this->enteringTime.toString("yyyy MM dd hh:mm:ss.zzz");
}


void LogEvent::setLeavingTime(const QDateTime &leavingTime,
    const std::vector<FrameStruct>& currentFrames)
{
    qDebug() << "Setting LeavingTime in entry " << detectorIdText;
    qDebug() << "Entering time: " << this->enteringTime.toString("yyyy MM dd hh:mm:ss.zzz") << " Old leaving time:" << this->leavingTime.toString("yyyy MM dd hh:mm:ss.zzz");

    this->leavingTime = leavingTime;
    qDebug() << "New leaving time:" << this->leavingTime.toString("yyyy MM dd hh:mm:ss.zzz");

	bool leaving = std::find(this->saveFrameOfEventSettings.begin(),
		this->saveFrameOfEventSettings.end(), LEAVING) !=
		this->saveFrameOfEventSettings.end();

    if (leaving)
    {
        // We have specified that we want to save a frame of the 
        // leaving event
        eventFrames[LEAVING] = currentFrames;

        for (auto i = 0; i < currentFrames.size(); ++i)
        {
            eventFrames[LEAVING][i].Image = currentFrames[i].Image.clone();
        }
    }
}

void LogEvent::resetLeavingTime()
{
    qDebug() << "Resetting LeavingTime in entry " << detectorIdText;
    qDebug() << "Entering time: " << this->enteringTime.toString("yyyy MM dd hh:mm:ss.zzz") << " Old leaving time:" << this->leavingTime.toString("yyyy MM dd hh:mm:ss.zzz");

    this->leavingTime = QDateTime();

    if (std::find(this->saveFrameOfEventSettings.begin(),
        this->saveFrameOfEventSettings.end(), LEAVING) !=
        this->saveFrameOfEventSettings.end()) {
        // Reset the frameStruct
        eventFrames[LEAVING].clear();
    }
}

void LogEvent::getEventFrames(std::map<Transition, std::vector<FrameStruct> >& eventFrames)
{
    // Engrave the mask outline in the event frame
    for (auto it = this->eventFrames.begin(); it != this->eventFrames.end(); ++it)
    {
        for (auto i = 0; i < it->second.size(); ++i)
        {
            if (maskOutline.size() > i)
            {
                it->second[i].Image = cvUtils::colorOverlay(maskOutline[i].Color, 0.3, it->second[i].Image, maskOutline[i].MaskOutline);
            }
        }
    }

    eventFrames = this->eventFrames;

    return;
}

LogEntry::LogEntry(const InitEntrySettings& entrySettings,
    const QDateTime &enteringTime,
    const QFileInfo &filename,
    const std::vector<FrameStruct>& currentFrames,
    const std::vector<ruba::MaskOutlineStruct>& maskOutline,
    const QString& detectorIdText, 
    const QString& compoundDetectorIdText)
{
    this->startTime = enteringTime;
    this->filename = filename;
    this->settings = entrySettings;
    this->compoundDetectorIdText = compoundDetectorIdText;

    state = LogEntryState::OPEN;
    
    checkSaveFrameOfEventSettings(detectorIdText);

    // Create the fist event candidate
    events.push_back(std::make_shared<LogEvent>(enteringTime, 
        currentFrames, 
        detectorIdText,
        settings.saveFrameOfEventSettings[detectorIdText],
        maskOutline));
}

void LogEntry::checkSaveFrameOfEventSettings(const QString& detectorIdText)
{
    if (settings.saveFrameOfEventSettings.count(compoundDetectorIdText) == 1)
    {
        // There exists a setting for the compound detector. Use the first 
        // entry from here to deduct, whether the entering or leaving frame
        // should be saved
        qDebug() << settings.saveFrameOfEventSettings[compoundDetectorIdText].size();

        if (settings.saveFrameOfEventSettings[compoundDetectorIdText].size() >
            events.size())
        {
            settings.saveFrameOfEventSettings[detectorIdText] = 
                std::vector<Transition>{ settings.saveFrameOfEventSettings[compoundDetectorIdText]
                [events.size()] };
        } else {
            settings.saveFrameOfEventSettings[detectorIdText] = std::vector<Transition>();
        }
    } else {
        // The entry does not exist. Create empty entry
        settings.saveFrameOfEventSettings[detectorIdText] = std::vector<Transition>();
    }
    
}

int LogEntry::getConsecutiveEventDelay(const QString & detectorIdText)
{
	int consecutiveEventDelay = 0;

	auto cIt = settings.consecutiveEventDelay.find(detectorIdText);
	if (cIt != settings.consecutiveEventDelay.end())
	{
		// We have defined a consecutiveEventDelay for this detector
		consecutiveEventDelay = cIt->second;
	}
	else if (settings.consecutiveEventDelay.size() == 1)
	{
		// Just use the global setting instead
		consecutiveEventDelay = settings.consecutiveEventDelay.begin()->second;
	}

	return consecutiveEventDelay;
}

int LogEntry::getDiscardEventsLessThan(const QString & detectorIdText)
{
	int discardEventsLessThan = 0;

	auto cIt = settings.discardEventsLessThan.find(detectorIdText);
	if (cIt != settings.discardEventsLessThan.end())
	{
		// We have defined a consecutiveEventDelay for this detector
		discardEventsLessThan = cIt->second;
	}
	else if (settings.discardEventsLessThan.size() == 1)
	{
		// Just use the global setting instead
		discardEventsLessThan = settings.discardEventsLessThan.begin()->second;
	}

	return discardEventsLessThan;
}


void LogEntry::updateEntry(const QDateTime & currentTime, 
    const Transition & transition, 
    const std::vector<FrameStruct>& currentFrames,
    const QString & detectorIdText, 
    const std::vector<ruba::MaskOutlineStruct>& maskOutline,
    LogInsertionPosition & eventPos)
{
    eventPos = LogInsertionPosition::NONE;

    if (state == LogEntryState::CLOSED)
    {
        // The log is finished. Return
        return;
    }

    // Step 1: The detector is triggered and the state is ENTERING
    // a) If no events exist, create a new event
    // b) If currentTime is within the consecutiveEventDelay of an existing event 
    //    with the same detectorIdText, we should concatenate this event and thus
    //    reset the LeavingTime
    // c) If we did not reset the LeavingTime, check if we are allowed to 
    //    generate a new event
    if (transition == ENTERING)
    {
        // a) If no events exist, create a new event
        if (events.empty())
        {
            // Create a new event candidate
            checkSaveFrameOfEventSettings(detectorIdText);
            
            events.push_back(std::make_shared<LogEvent>(currentTime, 
                currentFrames, 
                detectorIdText, 
                settings.saveFrameOfEventSettings[detectorIdText],
                maskOutline));
            eventPos = LogInsertionPosition::FIRST;
        }

        // b): Check if we are inside the consecutiveEventDelay
        bool leavingTimeReset = false;
        for (auto i = 0; i < events.size(); ++i)
        {
            if (events[i]->getDetectorIdText() == detectorIdText)
            {
				int consecutiveEventDelay = getConsecutiveEventDelay(events[i]->getDetectorIdText());

                // Check if the currentTime is within the consecutiveEventDelay
                if (currentTime <= events[i]->getLeavingTime().addMSecs(consecutiveEventDelay))
                {
                    auto it = settings.maxTriggeredDuration.find(events[i]->getDetectorIdText());
                    bool resetLeavingTime = false;

                    if (it != settings.maxTriggeredDuration.end())
                    {
                        // We have defined a maxTriggeredDuration for this detector. See if this constraint is satisfied
                        if (events[i]->duration() < it->second)
                        {
                            resetLeavingTime = true;
                        }
                    }
                    else if (settings.maxTriggeredDuration.size() == 1)
                    {
                        // We have a single maxTriggeredDuration setting. Use it as the global
                        if (events[i]->duration() < settings.maxTriggeredDuration.begin()->second)
                        {
                            resetLeavingTime = true;
                        }
                    }
                    else {
                        // No maxTriggeredConstraint. Reset the leavingTime
                        resetLeavingTime = true;
                    }

                    if (resetLeavingTime)
                    {
                        // Reset the leavingTime
                        events[i]->resetLeavingTime();
                        leavingTimeReset = true;
                        eventPos = LogInsertionPosition(i);
                    }
                }
            }
        }

        // c): Check if we are allowed to add a new event to the sequence
        if (!leavingTimeReset && (events.size() < settings.intervalType.size() && !events.empty()))
        {
            bool currentDetectorIdInEvents = false;
            for (auto oneEvent : events)
            {
                if (oneEvent->getDetectorIdText() == detectorIdText)
                {
                    currentDetectorIdInEvents = true;
                }
            }

            if (!currentDetectorIdInEvents)
            {
                // The existing events must not contain the current detector

                // Only proceed if the number of events is below the maximum number of events                  
                LogEvent *lastEvent = events.back().get(); // Get the last event
                QDateTime lastEventIntervalStart;
				QDateTime lastEventIntervalEnd;

                // Get the start time of the interval of the last event
                switch (settings.intervalType[events.size() - 1])
                {
				case Transition::ENTERING:
                    lastEventIntervalStart = lastEvent->getEnteringTime();
                    break;
                case Transition::LEAVING:
                    lastEventIntervalStart = lastEvent->getLeavingTime();
                    break;
				case Transition::OVERLAP:
					lastEventIntervalStart = lastEvent->getEnteringTime().addMSecs(settings.overlapTimingBuffer * (-1));
					lastEventIntervalEnd = lastEvent->getLeavingTime().isValid() ? lastEvent->getLeavingTime().addMSecs(settings.overlapTimingBuffer) : currentTime.addMSecs(settings.overlapTimingBuffer);
                }

                qDebug() << "LastEventIntervalStart " << lastEventIntervalStart.toString("yyyy MM dd HH:mm:ss.zzz");

                // And get the start time of the interval of the current event. 
                // If we measure the time from ENTERING/LEAVING - ENTERING, the 
                // currentTime must be within the limits set by the createEventMinDelay and createEventMaxDelay
                //
                // If we measure the time from ENTERING/LEAVING - LEAVING, the currentTime
                // must be within the limits of createEventMaxDelay
                bool createNewEvent = false;

                switch (settings.intervalType[events.size()])
                {
                case Transition::ENTERING:
                {
                    if ((currentTime > lastEventIntervalStart.addMSecs(settings.eventDelayIntervals[lastEvent->getDetectorIdText()].createEventMinDelay)) &&
                        (currentTime < lastEventIntervalStart.addMSecs(settings.eventDelayIntervals[lastEvent->getDetectorIdText()].createEventMaxDelay)))
                    {
                        createNewEvent = true;
                    }
                    break;
                }
                case Transition::LEAVING:
                {
                    if (currentTime < lastEventIntervalStart.addMSecs(settings.eventDelayIntervals[lastEvent->getDetectorIdText()].createEventMaxDelay))
                    {
                        createNewEvent = true;
                    }
                    break;
                }
				case Transition::OVERLAP:
				{
					if (currentTime >= lastEventIntervalStart && currentTime <= lastEventIntervalEnd)
					{
						// We have an overlap in time
						createNewEvent = true;
					}
				}
                }

                if (createNewEvent)
                {
                    // Create a new event candidate
                    checkSaveFrameOfEventSettings(detectorIdText);

                    events.push_back(std::make_shared<LogEvent>(currentTime,
                        currentFrames,
                        detectorIdText,
                        settings.saveFrameOfEventSettings[detectorIdText],
                        maskOutline));
                    eventPos = LogInsertionPosition(events.size()-1);
                }
            }
        }

    }

    // Step 3: If the transition is LEAVING, check the following:
    // a) If the current detectorIdText matches with any of the events, check if the leavingTime has been set. If not, update the leavingTime of the event
    if (transition == LEAVING)
    {
        for (auto i = 0; i < events.size(); ++i)
        {
            if (events[i]->getLeavingTime().isNull() && events[i]->getDetectorIdText() == detectorIdText)
            {
                events[i]->setLeavingTime(currentTime, currentFrames);
                eventPos = LogInsertionPosition(i);
                break;
            }
        }
    }

    qDebug() << "Entry at time " << currentTime.toString("yyyy MM dd HH:mm:ss.zzz");
    
    for (auto i = 0; i < events.size(); ++i)
    {
        qDebug() << i << ": Entering: " << events[i]->getEnteringTime().toString("yyyy MM dd HH:mm:ss.zzz");
        qDebug() << i << ": Leaving: " << events[i]->getLeavingTime().toString("yyyy MM dd HH:mm:ss.zzz");
    }


}

QString LogEntry::getEntryAsString() const
{
    QString entry = filename.fileName() + ";" + startTime.toString("yyyy MM dd") + ";";

    if (events.empty())
    {
        return entry;
    }

    for (auto singleEvent : events)
    {
        entry += singleEvent->getEnteringTime().toString("yyyy MM dd HH:mm:ss.zzz") + ";";
        entry += singleEvent->getLeavingTime().toString("yyyy MM dd HH:mm:ss.zzz") + ";";
        entry += QString::number(singleEvent->duration()) + ";";
    }

    // Get time gaps between entries
    for (auto i = 0; i < events.size(); ++i)
    {
        for (auto j = i+1; j < events.size(); ++j)
        {
            entry += QString::number(getEventTimegap(*events[i], *events[j],
                std::vector<Transition>{settings.intervalType[i], 
                settings.intervalType[j]})) + ";" ;
        }
    }

    if (events.size() > 1)
    {
        // Get the detectorIdText of the first detector and save it to the log
        entry += events[0]->getDetectorIdText() + ";";
    }

    // Return the completed entry
    return entry;
}

std::map<ViewInfo, cv::Mat> LogEntry::getEntryFrames()
{
    if (events.empty())
    {
        return std::map<ViewInfo, cv::Mat>();
    }
    
    size_t savedFrames = 0;

    // Count the number of frames to be saved

    for (auto i = 0; i < events.size(); ++i)
    {
        std::map<Transition, std::vector<FrameStruct>> singleEventFrames;
        events[i]->getEventFrames(singleEventFrames);

        savedFrames += singleEventFrames.size();
    }

    // Find the dimensions of the new rectangle to be made for the combined Frame
    // Combined frames are formed in a 2-by-2 pattern, for example for six frames
    // frame0 frame1 
    // frame2 frame3
    // frame4 frame5

    int widthMultiplier = savedFrames > 1 ? 2 : 1;
    int heightMultiplier = (static_cast<int>(savedFrames) % 2) == 0 ? static_cast<int>(savedFrames) / 2 : static_cast<int>(savedFrames) / 2 + 1;

    // Create container for the combined frames
    std::map<ViewInfo, cv::Mat> combinedViewFrames;

    std::map<Transition, std::vector<FrameStruct>> singleEventFrame;
    events.front()->getEventFrames(singleEventFrame);
    
    if (!singleEventFrame.empty())
    {
        for (auto frame : singleEventFrame.begin()->second)
        {
            Mat combinedImgForView = Mat(Size(frame.Image.cols * widthMultiplier,
                frame.Image.rows * heightMultiplier), frame.Image.type());
            combinedViewFrames[frame.viewInfo] = combinedImgForView;
        }
    }

    int frameNumber = 0;

    for (auto i = 0; i < events.size(); ++i)
    {
        std::map<Transition, std::vector<FrameStruct>> singleEventFrames;
        events[i]->getEventFrames(singleEventFrames);

        auto it = singleEventFrames.begin();

        while (it != singleEventFrames.end())
        {
            // Get position of the frames (0-indexed)
            int posX = frameNumber % 2;
            int posY = frameNumber / 2;

            // Go through the different views of this particular transition
            for (auto frame : it->second)
            {
                Mat cutOutFrame(combinedViewFrames[frame.viewInfo], Rect(posX * frame.Image.cols,
                    posY * frame.Image.rows, frame.Image.cols, frame.Image.rows));
                frame.Image.copyTo(cutOutFrame);
            }
                
            frameNumber++;
            it++;
        }
    }

    return combinedViewFrames;
}

QDateTime LogEntry::getEarliestEnteringTime()
{
    if (!events.empty())
    {
        return events.front()->getEnteringTime();
    }
    else {
        return QDateTime();
    }
}

QList<QString> LogEntry::getDebugInfo() const
{
    QList<QString> info;

    for (auto& singleEvent : events)
    {
		if (singleEvent)
		{
			QString eventInfo = singleEvent->getDetectorIdText() + ":";
			eventInfo += " Entering: " + singleEvent->getEnteringTime().toString("hh:mm:ss.zzz");
			eventInfo += " Leaving: " + singleEvent->getLeavingTime().toString("hh:mm:ss.zzz");
			eventInfo += " Duration: " + QString::number(singleEvent->duration());

			info.push_back(eventInfo);
		}
    }

    return info;
}

int LogEntry::getEventTimegap(const LogEvent & firstEvent, 
    const LogEvent & secondEvent, 
    const std::vector<Transition>& intervalType)
{
    assert(intervalType.size() > 1);

    QDateTime firstTransition;
    QDateTime secondTransition;

    // Get the appropriate transition for the first event
    switch (intervalType[0])
    {
    case ENTERING:
        firstTransition = firstEvent.getEnteringTime();
        break;
    case LEAVING:
        firstTransition = firstEvent.getLeavingTime();
        break;
	case OVERLAP:
		// Compute the overlap between the two events
		if (firstEvent.getLeavingTime() < secondEvent.getLeavingTime())
		{
			return secondEvent.getEnteringTime().msecsTo(firstEvent.getLeavingTime());
		}
		else {
			return secondEvent.duration();
		}
		
    }

    // Get the appropriate transition for the second event
    switch (intervalType[1])
    {
    case ENTERING:
        secondTransition = secondEvent.getEnteringTime();
        break;
    case LEAVING:
        secondTransition = secondEvent.getLeavingTime();
        break;
    }

    return firstTransition.msecsTo(secondTransition);
}

void LogEntry::checkEntryState(const QDateTime & currentTime, 
    const std::vector<FrameStruct> &currentFrames,
    LogEntryState &state)
{    
    state = this->state;

    if ((state == LogEntryState::CLOSED) ||
        (state == LogEntryState::INVALID))
    {
        return;
    }

    // Delete uncompleted event candidates for which the time has run out
    // and no additional event is detected
    // Check if the uncompleted event is outside the createEventMaxDelay. 
    // This only applies for log entries with multiple events,
    QDateTime maximumCompletedTime = QDateTime();

    if (settings.intervalType.size() > 1 && !events.empty()
        && (events.size() != settings.intervalType.size()))
    {
        QDateTime maximumCompletedTime;

        switch (settings.intervalType[events.size() - 1])
        {
        case ENTERING:
			if (settings.eventDelayIntervals[events.back()->getDetectorIdText()].createEventMaxDelay >= 0)
			{
				maximumCompletedTime = events.back()->getEnteringTime().addMSecs(
					settings.eventDelayIntervals[events.back()->getDetectorIdText()].createEventMaxDelay);
			}
            break;
        case LEAVING:
			if (settings.eventDelayIntervals[events.back()->getDetectorIdText()].createEventMaxDelay >= 0)
			{
				maximumCompletedTime = events.back()->getLeavingTime().addMSecs(
					settings.eventDelayIntervals[events.back()->getDetectorIdText()].createEventMaxDelay);
			}
            break;
		case OVERLAP:
			maximumCompletedTime = events.back()->getLeavingTime().addMSecs(settings.overlapTimingBuffer);
        }        

        if (maximumCompletedTime.isValid() && (maximumCompletedTime < currentTime))
        {
            // Invalidate the entry
            this->state = state = LogEntryState::INVALID;
            qDebug() << "Invalidating entry at maximumCompletedTime: " << maximumCompletedTime.toString("yyyy MM dd hh:mm:ss.zzz");
            return;
        }
    }

    // Check if the duration of the events is greater than the discardEventsLessThan threshold
    for (auto singleEvent : events)
    {
		int consecutiveEventDelay = getConsecutiveEventDelay(singleEvent->getDetectorIdText());

        if (singleEvent->getEnteringTime().isValid() && singleEvent->getLeavingTime().isValid()
            && (singleEvent->getLeavingTime().addMSecs(consecutiveEventDelay) < currentTime))
        {
			int discardEventsLessThan = getDiscardEventsLessThan(singleEvent->getDetectorIdText());
			
			// The event is completed and the current time is outside the consecutiveEventDelay.
            // Check if the duration is greater than the minimum threshold

            if (singleEvent->duration() < discardEventsLessThan)
            {
                this->state = state = LogEntryState::INVALID;
                qDebug() << "Invalidating entry at duration: " << QString::number(singleEvent->duration());
                return;
            }
        }
    }

    // Check if an object has been detected for "too long", i.e. the detector is triggered and
    // the time since EnteringTime is above the MaxTriggeredDuration time.
    // If this is the case, force update the log by clearing the collateEventsUntil time stamp and
    // adding the current time as leaving the conflict zone.
    
    for (auto i = 0; i < events.size(); ++i)
    {
        auto it = settings.maxTriggeredDuration.find(events[i]->getDetectorIdText());

        
        if ((it == settings.maxTriggeredDuration.end()) && 
            (settings.maxTriggeredDuration.size() == 1))
        {
            // We have a single maxTriggeredDuration setting. Use it as the global
            it = settings.maxTriggeredDuration.begin();
        }

        if (it != settings.maxTriggeredDuration.end())
        {            
            if ((!events[i]->getLeavingTime().isValid()) &&
                (events[i]->getEnteringTime().addMSecs(it->second)
                <= currentTime))
            {
                // Close the event by setting the leaving time to the current time
                qDebug() << "Closing event due to maxTriggeredDuration at: " + currentTime.toString("yyyy MM dd HH:mm:ss.zzz");
                events[i]->setLeavingTime(currentTime, currentFrames);
            }
        }

    }

    // Check if the events are finished, thus closing the entry
    bool allEventsFinished = true;

    for (auto singleEvent : events)
    {
		int consecutiveEventDelay = getConsecutiveEventDelay(singleEvent->getDetectorIdText());

        if (!singleEvent->getEnteringTime().isValid() || !singleEvent->getLeavingTime().isValid()
            || !(singleEvent->getLeavingTime().addMSecs(consecutiveEventDelay) < currentTime))
        {
            allEventsFinished = false;
        }
    }

    if (events.size() < settings.intervalType.size())
    {
        allEventsFinished = false;
    }

    if (allEventsFinished)
    {
        this->state = state = LogEntryState::CLOSED;
    }
}

SumLog::SumLog(const QString & sumOfEventsLogPath, int sumInterval,
    const QDateTime & currentTime, const QFileInfo & currentFile)
{
    fileHandle.setFileName(sumOfEventsLogPath);

    this->sumInterval = sumInterval;

    currentEntry.count = 0;

	QDate currentDate = currentTime.date();
	int startHour = currentTime.time().hour();
	int startMinute = 0;
		
	if (sumInterval <= 60)
	{
		int quotient = currentTime.time().minute() / sumInterval;
		startMinute = sumInterval * quotient;
	}

    currentEntry.countStart = QDateTime(currentDate, QTime(startHour, startMinute, 0, 0));
    currentEntry.countEnd = currentEntry.countStart.addSecs(sumInterval * 60); // The sum interval is given in minutes;
	
	qDebug() << currentEntry.countStart.toString("yyyy-MM-dd-HH-mm-sss.zzz");
	qDebug() << currentEntry.countEnd.toString("yyyy-MM-dd-HH-mm-sss.zzz");

    currentEntry.file = currentFile;

    // Write the log header
    if (fileHandle.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&fileHandle);
        stream.setFieldAlignment(QTextStream::AlignLeft);

        stream << "File;Date;TimeStart;TimeEnd;Detections;" << endl;

        stream.flush();
    }
}

void SumLog::increment(const QDateTime & currentTime, const QFileInfo & currentFile)
{
    update(currentTime, currentFile);

    // Increment the existing event
    currentEntry.count++;
}

void SumLog::update(const QDateTime & currentTime, const QFileInfo & currentFile)
{
    bool updateEntry = false;

    if (!currentEntry.countEnd.isValid())
    {
        updateEntry = true;
    }

    if (currentEntry.countEnd.isValid() && (currentTime >= currentEntry.countEnd))
    {
        saveCurrentEntry();
        updateEntry = true;
    }

    if (updateEntry)
    {
        // Create a new log entry based on the new occurrence
        currentEntry.count = 0;

        if (currentEntry.countEnd.isValid())
        {
            currentEntry.countStart = currentEntry.countEnd;
        }
        else {
			QDate currentDate = currentTime.date();
			int startHour = currentTime.time().hour();
			int startMinute = 0;

			if (sumInterval <= 60)
			{
				int quotient = currentTime.time().minute() / sumInterval;
				startMinute = sumInterval * quotient;
			}

			currentEntry.countStart = QDateTime(currentDate, QTime(startHour, startMinute, 0, 0));			
        }

        currentEntry.countEnd = currentEntry.countStart.addSecs(sumInterval * 60); // The sum interval is given in minutes
        currentEntry.file = currentFile;
        return;
    }

}

//! Saves the current log entry and resets it
void SumLog::saveCurrentEntry()
{
    if (!fileHandle.isOpen())
    {
        fileHandle.open(QIODevice::WriteOnly | QIODevice::Text);
    }

    if (fileHandle.isOpen())
    {
        QTextStream stream(&fileHandle);
        stream.setFieldAlignment(QTextStream::AlignLeft);

        stream << currentEntry.file.fileName() + ";"
            << currentEntry.countStart.date().toString("yyyy MM dd") + ";"
            << currentEntry.countStart.toString("yyyy MM dd HH:mm:ss.zzz") + ";"
            << currentEntry.countEnd.toString("yyyy MM dd HH:mm:ss.zzz") + ";"
            << QString::number(currentEntry.count) + ";" << endl;

        stream.flush();
    }
}

void SumLog::closeLog()
{
	if (fileHandle.isOpen())
	{
		fileHandle.close();
	}
}

std::string SumLog::getFileName()
{
	if (!fileHandle.fileName().isEmpty())
	{
		return fileHandle.fileName().toStdString();
	}

	return "";
}

ruba::Logger::Logger(const InitLogSettings & settings, 
    const QString& compoundDetectorIdText,
    const QDateTime& currentTime,
    const std::vector<FrameStruct>& currentFrames)
{
    this->settings = settings;
    this->compoundDetectorIdText = compoundDetectorIdText;
    completedEventCounter = 0;
    lastEnteringTime = QDateTime();
    lastLeavingTime = QDateTime();

    // Check that the provided settings are valid, i.e. that they refer to 
    // the same detector names

    for (auto eventDelay : settings.entrySettings.eventDelayIntervals)
    {
        QString detectorName = eventDelay.first;
        
        availableDetectorIdTexts.insert(detectorName);
    }
    
    if (settings.saveEveryEventLog)
    {
        initialiseEveryEventLog();
    }

    if (settings.saveSumOfEventsLog)
    {
        QString currentFile;

        if (currentFrames.empty())
        {
            return;
        }
        else {
            currentFile = currentFrames[0].Filename;
        }

        sumLog = std::make_shared<SumLog>(settings.sumOfEventsLogPath, 
            settings.sumInterval,
            currentTime, 
            currentFile);
    }
    else {
        sumLog = nullptr;
    }    
}

void ruba::Logger::updateLog(const QDateTime & currentTime, 
    const std::vector<FrameStruct>& currentFrames,
    const Transition & transition, 
    const QString & detectorIdText,
    const std::vector<ruba::MaskOutlineStruct>& maskOutlines)
{
    QString currentFile;

    if (currentFrames.empty())
    {
        return;
    }
    else {
        currentFile = currentFrames[0].Filename;
    }
    
    QList<LogInsertionPosition> entryPos;

    // Update the entries
    for (auto i = 0; i < everyEventEntries.size(); ++i)
    {
        entryPos.push_back(LogInsertionPosition());

        everyEventEntries[i]->updateEntry(currentTime,
            transition,
            currentFrames,
            detectorIdText,
            maskOutlines,
            entryPos.back());
    }

    if (transition == ENTERING)
    {
        // If the transition is ENTERING, check if we have updated 
        // an event that is the first event in an logEntry:
        bool firstEventUpdated = false;
        lastEnteringTime = currentTime;

        for (auto pos : entryPos)
        {
            if (pos == LogInsertionPosition::FIRST)
            {
                firstEventUpdated = true;
            }
        }

        if (!firstEventUpdated)
        {
            // If that is not the case, create a new LogEntry
            everyEventEntries.push_back(std::make_shared<LogEntry>(settings.entrySettings,
                currentTime,
                currentFile,
                currentFrames,
                maskOutlines,
                detectorIdText,
                compoundDetectorIdText));
        }
    }
    else if (transition == LEAVING)
    {
        lastLeavingTime = currentTime;
    }
}

void ruba::Logger::saveLog(const QDateTime & currentTime, 
    const std::vector<FrameStruct>& currentFrames, 
    bool forceSave)
{
    QString currentFile;
    
    if (currentFrames.empty())
    {
        return;
    }
    else {
        currentFile = currentFrames[0].Filename;
    }

    auto it = everyEventEntries.begin();

    while (it != everyEventEntries.end())
    {
        LogEntryState state;

        it->get()->checkEntryState(currentTime, currentFrames, state);

        if (state == LogEntryState::CLOSED)
        {
            completedEventCounter++;

            // Save the entry to the log
            if (settings.saveEveryEventLog && !everyEventLog.isOpen())
            {
                // Re-open the log
                everyEventLog.open(QIODevice::Append | QIODevice::Text);
                QTextStream stream(&everyEventLog);
                stream.setFieldAlignment(QTextStream::AlignLeft);
            }

            QString firstSavedFramePath;

            if (settings.entrySettings.saveFrameOfEventSettings.size() > 0)
            {
                // Save the frame that corresponds to the entry
                std::map<ViewInfo, cv::Mat> entryFrames = it->get()->getEntryFrames();
                

                if (!entryFrames.empty())
                {
                    QString transitionText;
					Transition lastTransition = Transition::ENTERING;

                    for (auto& interval : settings.entrySettings.intervalType)
                    {
						if (lastTransition != OVERLAP)
						{
							// We only want to write "Overlap" once
							transitionText += getTransitionAsString(interval) + "-";
							lastTransition = interval;
						}
                    }

                    QString baseText = 
                        compoundDetectorIdText + "-" + QString("%1").arg(completedEventCounter, 6, 10, QChar('0')) + "-" +
                        it->get()->getEarliestEnteringTime().toString("yyyy-MM-dd-HH-mm-ss.zzz") + "-" +
                        transitionText;


                    for (auto frame : entryFrames)
                    {
                        QString specificText =  baseText + "View-" +
                            QString::number(frame.first.viewId) + ".png";

                        if (firstSavedFramePath.isEmpty())
                        {
                            firstSavedFramePath = specificText;
                        }

                        QString fullFilename = settings.saveFrameOfEventPath + "/" + specificText;
                        cv::imwrite(fullFilename.toStdString(), frame.second);
                        savedFramesPaths.push_back(fullFilename);
                    }
                }
            }

            if (settings.saveEveryEventLog && everyEventLog.isOpen())
            {
                QTextStream stream(&everyEventLog);
                stream.setFieldAlignment(QTextStream::AlignLeft);

                firstSavedFramePath = firstSavedFramePath.isEmpty() ? ";" : firstSavedFramePath.append(";"); // Append a semicolon to the entry if it exists

                stream << it->get()->getEntryAsString() + firstSavedFramePath;

                if (!customInfo.empty())
                {
                    for (auto line : customInfo)
                    {
                        stream << line << ";";
                    }
                }

                stream << endl;

                stream.flush();
            }

            

            if (settings.saveSumOfEventsLog && sumLog != nullptr)
            {
                sumLog->increment(it->get()->getEarliestEnteringTime(), currentFile);
            }
        }

        if (state == LogEntryState::INVALID || state == LogEntryState::CLOSED)
        {
            if (state == LogEntryState::CLOSED)
            {
                // The entry is completed. Move it to the completed queue
                completedEveryEventEntries.push_back(std::shared_ptr<LogEntry>());
                it->swap(completedEveryEventEntries.back());

                if (completedEveryEventEntries.size() > 3)
                {
                    // Only store the last three completed entries
                    completedEveryEventEntries.pop_front();
                }
            }
            
            // Delete the entry from the list
            it = everyEventEntries.erase(it);
        }
        else {
            // Increment the iterator
            it++;
        }
    }

    if (settings.saveSumOfEventsLog && sumLog != nullptr)
    {
        sumLog->update(currentTime, currentFile);
    }


    if (forceSave && settings.saveSumOfEventsLog && sumLog != nullptr)
    {
        sumLog->saveCurrentEntry();
    }
}

void ruba::Logger::reset(const QDateTime& currentTime, 
    const std::vector<ruba::FrameStruct>& currentFrames)
{
    // Reset the log and delete any frames that are saved in this instance
    completedEventCounter = 0;
    lastEnteringTime = QDateTime();
    lastLeavingTime = QDateTime();

    if (settings.saveEveryEventLog)
    {
        initialiseEveryEventLog();
    }

    for (auto framePath : savedFramesPaths)
    {
        QFile::remove(framePath);
    }

    savedFramesPaths.clear();
    everyEventEntries.clear();
    completedEveryEventEntries.clear();

    QFileInfo currentFile;

    if (currentFrames.empty())
    {
        return;
    }
    else {
        currentFile = currentFrames[0].Filename;
    }

    if (settings.saveSumOfEventsLog)
    {
        // Create a new instance of the SumLog. The old instance is automatically destructed
        sumLog = make_shared<SumLog>(settings.sumOfEventsLogPath,
            settings.sumInterval,
            currentTime,
            currentFile);
    }

}

void ruba::Logger::setLogFileNamePostfix(int number)
{
	settings.everyEventLogPath = settings.everyEventLogPath.replace(QString(".csv"), QString("-%1.csv").arg(number));
	settings.sumOfEventsLogPath = settings.sumOfEventsLogPath.replace(QString(".csv"), QString("-%1.csv").arg(number));

	// A reset should be performed from the owner of the log to fully replace the old logs
}

void ruba::Logger::closeAllLogs()
{
	if (everyEventLog.isOpen())
	{
		everyEventLog.close();
	}

	if (sumLog)
	{
		sumLog->closeLog();
	}
}

std::vector<std::pair<std::string, LogRole> > ruba::Logger::getFileNames()
{
	std::vector<std::pair<std::string, LogRole> > filenames;

	if (!everyEventLog.fileName().isEmpty())
	{
		filenames.push_back(std::pair<std::string, LogRole>(everyEventLog.fileName().toStdString(), LogRole::EventTypeLog));
	}

	if (sumLog && !sumLog->getFileName().empty())
	{
		filenames.push_back(std::pair<std::string, LogRole>(sumLog->getFileName(), LogRole::SumTypeLog));
	}

	return filenames;
}

qint64 ruba::Logger::getCompletedEventCount()
{
    return this->completedEventCounter;
}

QDateTime ruba::Logger::getLastEnteringTime()
{
    return lastEnteringTime;
}

QDateTime ruba::Logger::getLastLeavingTime()
{
    return lastLeavingTime;
}

QString ruba::Logger::getTransitionAsString(Transition state)
{
    switch (state)
    {
    case Transition::ENTERING:
        return "Entering";
    case Transition::LEAVING:
        return "Leaving";
	case Transition::OVERLAP:
		return "Overlap";
    default:
        return QString();
    }
}

QList<QString> ruba::Logger::getDebugInfo() const
{
    // Return a list of the log entries
    QList<QString> info;


    for (auto i = completedEveryEventEntries.size(); i > 0; --i)
    {
        QList<QString> entryInfo = completedEveryEventEntries[i-1]->getDebugInfo();

        for (auto j = 0; j < entryInfo.size(); ++j)
        {
            QString ordinalSuffix = "";

            if (entryInfo.size() > 1)
            {
                // Only show "2nd, 3rd", etc. if there are more than one event in the entry
                ordinalSuffix = ": " + QString::number(j+1) + Utils::getOrdinalSuffix(j+1);
            }

            info.push_back("Completed " + 
                QString::number(completedEventCounter - completedEveryEventEntries.size() + i) 
                + ordinalSuffix + ": " + entryInfo[j]);
        }
    }

    for (auto i = 0; i < everyEventEntries.size(); ++i)
    {
        QList<QString> entryInfo = everyEventEntries[i]->getDebugInfo();

        for (auto singleEvent : entryInfo)
        {
            info.push_back("Entry " + QString::number(i + 1) + ": " + singleEvent);
        }
    }


    return info;
}



void ruba::Logger::initialiseEveryEventLog()
{
    // Write the log header for the every event log
    if (everyEventLog.isOpen())
    {
        everyEventLog.close();
    }

    everyEventLog.setFileName(settings.everyEventLogPath);

    if (everyEventLog.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&everyEventLog);
        stream.setFieldAlignment(QTextStream::AlignLeft);

        QString header = "File;Date;";

		auto numberLogEvents = settings.entrySettings.intervalType.size() > 1 ?
			settings.entrySettings.intervalType.size() : 1;

        for (auto i = 0; i < numberLogEvents; ++i)
        {
            header += "Entering";
            header += (settings.customLogHeader.size() > i)
                ? " " + settings.customLogHeader[i].additionalTransitionText[ENTERING] + ";"
                : ";";

            header += "Leaving";
            header += (settings.customLogHeader.size() > i)
                ? " " + settings.customLogHeader[i].additionalTransitionText[LEAVING] + ";"
                : ";";

            header += "Duration;";
        }

        // Get time gaps between entries
        for (auto i = 0; i < settings.entrySettings.intervalType.size(); ++i)
        {
            for (auto j = i+1; j < settings.entrySettings.intervalType.size(); ++j)
            {
				if (!settings.entrySettings.intervalType.empty() &&
					settings.entrySettings.intervalType.front() == Transition::OVERLAP) 
				{
					header += QString("Overlap;");
				}
				else 
				{
					header += QString("Time Gap %1-%2;").arg(i + 1).arg(j + 1);
				}
            }
        }

        // Write "FirstTriggered" if we allow multiple events
        if (settings.entrySettings.intervalType.size() > 1)
        {
            header += "First Triggered;";
        }

        // Write "Frame" if we save the event frame(s)
        if (!settings.entrySettings.saveFrameOfEventSettings.empty())
        {
            header += "Frame;";
        }

        stream << header;
        stream << endl;
        stream.flush();
    }
}
