﻿Public Class clsMergingSingleProject
  Public SingleList1 As clsSingleDetectionList
  Public SingleList2 As clsSingleDetectionList

  Private MyDoubleDetections As List(Of clsDoubleDetection)


  Public Sub New(ByRef CSVFilePath1 As String,
                 ByVal ImageFolder1 As String,
                 ByRef CSVFilePath2 As String,
                 ByVal ImageFolder2 As String)

    SingleList1 = New clsSingleDetectionList(CSVFilePath1, ImageFolder1)
    SingleList2 = New clsSingleDetectionList(CSVFilePath2, ImageFolder2)
    MyDoubleDetections = New List(Of clsDoubleDetection)

  End Sub

  Public Sub Merge(ByVal OutputFilePath As String, ByVal OutputImageFolder As String)
    Dim _DoubleDetection As clsDoubleDetection
    Dim _i As Integer
    Dim _Bitmap1, _Bitmap2, _OutputBitmap As Bitmap
    Dim _g As Graphics
    Dim _FileIndex As String


    _i = 0
    MyDoubleDetections.Clear()


    _FileIndex = FreeFile()
    If File.Exists(OutputFilePath) Then File.Delete(OutputFilePath)
    For Each _ImageFile As String In Directory.GetFiles(OutputImageFolder)
      File.Delete(_ImageFile)
    Next

    FileOpen(_FileIndex, OutputFilePath, OpenMode.Output)
        PrintLine(_FileIndex, "File;Date;Entering Detector 1;Leaving Detector 1;Duration;Entering Detector 2;Leaving Detector 2;Duration;Time Gap 1-2;First Triggered;Frame")

        For Each _Detection1 In SingleList1.Detections
      For Each _Detection2 In SingleList2.Detections
        If (_Detection1.Entering1 < _Detection2.Exiting1.AddSeconds(3)) And (_Detection1.Exiting1.AddSeconds(3) > _Detection2.Entering1) Then
          _DoubleDetection = New clsDoubleDetection
          _i = _i + 1

          With _DoubleDetection
            .File = _Detection1.File
            .DetectionDate = _Detection1.DetectionDate
            .Entering1 = _Detection1.Entering1
            .Exiting1 = _Detection1.Exiting1
            .Duration1 = _Detection1.Duration1
            .Entering2 = _Detection2.Entering1
            .Exiting2 = _Detection2.Exiting1
            .Duration2 = _Detection2.Duration1
            .TimeGap = 1



            _Bitmap1 = New Bitmap(SingleList1.ImageFolder & _Detection1.ImageFileName)
            _Bitmap2 = New Bitmap(SingleList2.ImageFolder & _Detection2.ImageFileName)
            _OutputBitmap = New Bitmap(_Bitmap1.Width + _Bitmap2.Width, CInt(Math.Max(_Bitmap1.Height, _Bitmap2.Height)))
            _g = Graphics.FromImage(_OutputBitmap)

            If .Entering1 < .Entering2 Then
              .FirstTriggered = "D1"
              _g.DrawImage(_Bitmap1, 0, 0, _Bitmap1.Width, _Bitmap1.Height)
              _g.DrawImage(_Bitmap2, _Bitmap1.Width, 0, _Bitmap2.Width, _Bitmap2.Height)

              .ImageFileName = "DD2-" & _i.ToString("000000") & "-" &
                             .Entering1.Year.ToString & "-" & .Entering1.Month.ToString("00") & "-" & .Entering1.Day.ToString("00") & "-" &
                             .Entering1.Hour.ToString("00") & "-" & .Entering1.Minute.ToString("00") & "-" & .Entering1.Second.ToString("00") & "." & .Entering1.Millisecond.ToString("000") & ".png"

            Else
              .FirstTriggered = "D2"
              _g.DrawImage(_Bitmap2, 0, 0, _Bitmap2.Width, _Bitmap2.Height)
              _g.DrawImage(_Bitmap1, _Bitmap2.Width, 0, _Bitmap1.Width, _Bitmap1.Height)

              .ImageFileName = "DD2-" & _i.ToString("000000") & "-" &
                             .Entering2.Year.ToString & "-" & .Entering2.Month.ToString("00") & "-" & .Entering2.Day.ToString("00") & "-" &
                             .Entering2.Hour.ToString("00") & "-" & .Entering2.Minute.ToString("00") & "-" & .Entering2.Second.ToString("00") & "." & .Entering2.Millisecond.ToString("000") & ".png"

            End If


            _OutputBitmap.Save(OutputImageFolder & .ImageFileName, Imaging.ImageFormat.Png)

            PrintLine(_FileIndex, .File & ";" &
                              .DetectionDate.Year.ToString & " " & .DetectionDate.Month.ToString("00") & " " & .DetectionDate.Day.ToString("00") & ";" &
                              .Entering1.Year.ToString & " " & .Entering1.Month.ToString("00") & " " & .Entering1.Day.ToString("00") & " " & .Entering1.Hour.ToString("00") & ":" & .Entering1.Minute.ToString("00") & ":" & .Entering1.Second.ToString("00") & "." & .Entering1.Millisecond.ToString("000") & ";" &
                              .Exiting1.Year.ToString & " " & .Exiting1.Month.ToString("00") & " " & .Exiting1.Day.ToString("00") & " " & .Exiting1.Hour.ToString("00") & ":" & .Exiting1.Minute.ToString("00") & ":" & .Exiting1.Second.ToString("00") & "." & .Exiting1.Millisecond.ToString("000") & ";" &
                              .Duration1.ToString & ";" &
                              .Entering2.Year.ToString & " " & .Entering2.Month.ToString("00") & " " & .Entering2.Day.ToString("00") & " " & .Entering2.Hour.ToString("00") & ":" & .Entering2.Minute.ToString("00") & ":" & .Entering2.Second.ToString("00") & "." & .Entering2.Millisecond.ToString("000") & ";" &
                              .Exiting2.Year.ToString & " " & .Exiting2.Month.ToString("00") & " " & .Exiting2.Day.ToString("00") & " " & .Exiting2.Hour.ToString("00") & ":" & .Exiting2.Minute.ToString("00") & ":" & .Exiting2.Second.ToString("00") & "." & .Exiting2.Millisecond.ToString("000") & ";" &
                              .Duration2.ToString & ";" &
                              .TimeGap.ToString & ";" &
                              .FirstTriggered & ";" &
                              .ImageFileName)

            _OutputBitmap.Dispose()
            _Bitmap1.Dispose()
            _Bitmap2.Dispose()
            _g.Dispose()
          End With

          MyDoubleDetections.Add(_DoubleDetection)
        End If
      Next
    Next
    FileClose(_FileIndex)


  End Sub


End Class
