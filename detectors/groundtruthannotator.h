#ifndef GROUNDTRUTHANNOTATOR_H
#define GROUNDTRUTHANNOTATOR_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "dialogs/groundtruthannotatordialog.h"
#include "detectors/abstractcompounddetector.h"

class ruba::GroundTruthAnnotator : public AbstractCompoundDetector
{

public:
    explicit GroundTruthAnnotator(BaseDetectorParams baseParams, QWidget *parent);

    ~GroundTruthAnnotator();

    ruba::DetectorOutput GetOutput();

    virtual std::vector<std::vector<MaskOverlayStruct> > GetMasks();

    void Reconfigure(std::vector<ruba::FrameStruct> initFrames, QWidget *parent);
    void SetLastFrameReached();

private slots:
    void validateShortCut(int eventCode);

signals:
    void emitShortCut(int eventCode);

private:
    virtual void setDetectorSpecificDefaultParameters();
    cv::Mat baseMask;

    virtual QList<QString> getDetectorSpecificDebugInfo();
    virtual QList<QString> getDetectorSpecificExtendedDebugInfo();
    void updateDetectorSpecifics(std::vector<ruba::FrameStruct> frames, std::vector<DetectorOutput> dOutput);
    void updateDetectorSpecificLogic();

    GroundTruthAnnotatorDialog* gtDialog;

};

#endif // GROUNDTRUTHANNOTATOR_H
