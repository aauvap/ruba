#include <QtWidgets>
#include "settingspages.h"
#include "../ruba.h"


GeneralPage::GeneralPage(QWidget * parent)
    : AbstractSettingsPage(parent)
{
    QGroupBox *initGroup = new QGroupBox(tr("Initialisation"));

    restorePrevCheckBox = new QCheckBox(
        tr("Restore previous configuration when the program starts"));
    includeLabel = new QLabel(tr("Include:"));
    includeVideosCheckBox = new QCheckBox(
        tr("Videos"));
    includeDetectorsCheckBox = new QCheckBox(
        tr("Detectors"));

    connect(restorePrevCheckBox,
        SIGNAL(stateChanged(int)),
        this, SLOT(restorePrevCheckBoxChecked()));


    QVBoxLayout *initLayout = new QVBoxLayout();
    initLayout->addWidget(restorePrevCheckBox);

    QVBoxLayout *includeLayout = new QVBoxLayout();
    includeLayout->addWidget(includeLabel);
    includeLayout->addWidget(includeVideosCheckBox);
    includeLayout->addWidget(includeDetectorsCheckBox);

    QHBoxLayout *displacedBox = new QHBoxLayout();
    displacedBox->insertSpacing(0, 15);
    displacedBox->insertLayout(-1, includeLayout);   

    initLayout->insertLayout(-1, displacedBox);

    initGroup->setLayout(initLayout);

    // Style sheet
    QGroupBox* appearanceGroup = new QGroupBox(tr("Appearance"));
    QVBoxLayout* appearanceLayout = new QVBoxLayout();

    auto styleKeys = QStyleFactory::keys();
    styleComboBox = new QComboBox();

    for (auto const& styleKey : styleKeys)
    {
        if (styleKey == "Windows")
        {
            // The "Windows" style is quite bad
            continue;
        }

        styleComboBox->addItem(styleKey);

        if (styleKey.toLower() == QApplication::style()->objectName().toLower())
        {
            styleComboBox->setCurrentText(styleKey);
        }
    }   

    qDebug() << QApplication::style()->objectName();

    connect(styleComboBox,
        SIGNAL(currentIndexChanged(int)),
        this, SLOT(styleButtonChanged()));

    QHBoxLayout* setStyleLayout = new QHBoxLayout();
    setStyleLayout->addWidget(new QLabel(tr("Window style")));
    setStyleLayout->addWidget(styleComboBox);

    appearanceLayout->addLayout(setStyleLayout);
    appearanceGroup->setLayout(appearanceLayout);

    QVBoxLayout *mainLayout = new QVBoxLayout();

    mainLayout->addWidget(initGroup);
    mainLayout->addWidget(appearanceGroup);

    mainLayout->insertStretch(-1);
    setLayout(mainLayout);   

 
    QSettings settings;
    settings.beginGroup("general");

    restorePrevCheckBox->setChecked(settings.value(
        "restorePrevConfig", 1).toBool());
    includeVideosCheckBox->setChecked(settings.value(
        "restorePrevConfig-includeVideos", 1).toBool());
    includeDetectorsCheckBox->setChecked(settings.value(
            "restorePrevConfig-includeDetectors", 0).toBool());
    settings.endGroup();
    restorePrevCheckBoxChecked();

}

void GeneralPage::restorePrevCheckBoxChecked()
{
    bool checked = restorePrevCheckBox->isChecked();
    includeLabel->setEnabled(checked);
    includeVideosCheckBox->setEnabled(checked);
    includeDetectorsCheckBox->setEnabled(checked);
}

void GeneralPage::saveSettings()
{
    QSettings settings;
    settings.beginGroup("general");
    settings.setValue("restorePrevConfig",
        restorePrevCheckBox->isChecked());
    settings.setValue("restorePrevConfig-includeVideos",
        includeVideosCheckBox->isChecked());
    settings.setValue("restorePrevConfig-includeDetectors",
        includeDetectorsCheckBox->isChecked());
    settings.endGroup();
}

void GeneralPage::styleButtonChanged()
{
    QSettings settings;
    settings.beginGroup("general");
    settings.setValue("appearanceStyle", styleComboBox->currentText());
    settings.endGroup();


    QApplication::setStyle(styleComboBox->currentText());
}


SynchronisationPage::SynchronisationPage(QWidget * parent)
    : AbstractSettingsPage(parent)
{
    // Choose synchronisation type
    QGroupBox *overallSyncGroup = new QGroupBox(tr("Video synchronisation method"));

    startTimeInFileName = new QRadioButton(tr(
        "Start time of each video is encoded in the file name."));
    QRadioButton *timestampsInLogFile = new QRadioButton(tr(
        "Time stamps of each video frame are provided in a separate log file"));

    connect(startTimeInFileName,
        SIGNAL(toggled(bool)),
        this, SLOT(startTimeInFileNameToggled()));

    QVBoxLayout *overallSyncLayout = new QVBoxLayout();
    overallSyncLayout->addWidget(startTimeInFileName);
    overallSyncLayout->addWidget(timestampsInLogFile);

    overallSyncGroup->setLayout(overallSyncLayout);

    // Start time in file name
    startTimeInFileNameGroup = new QGroupBox(tr("Start time in file name"));
    // Frame rate
    QGroupBox *frameRateGroup = new QGroupBox(tr("Frame rate"));

    QRadioButton *videoFpsRadioAuto = new QRadioButton(tr("Auto-detect frame rate"));
    videoFpsRadioManual = new QRadioButton(tr("Manually specify frame rate"));

    connect(videoFpsRadioManual,
        SIGNAL(toggled(bool)),
        this, SLOT(videoFpsRadioManualToggled()));


    videoFpsSpinBox = new QDoubleSpinBox();
    videoFpsSpinBox->setSuffix(tr(" frames/sec"));
    videoFpsSpinBox->setMinimum(1);
    videoFpsSpinBox->setMaximum(9999);
    videoFpsSpinBox->setSingleStep(0.01);

    QHBoxLayout *videoFpsAutoLayout = new QHBoxLayout();
    videoFpsAutoLayout->addWidget(videoFpsRadioAuto);

    QHBoxLayout *videoFpsManualLayout = new QHBoxLayout();
    videoFpsManualLayout->addWidget(videoFpsRadioManual);
    videoFpsManualLayout->addWidget(videoFpsSpinBox);

    QVBoxLayout *videoFpsLayout = new QVBoxLayout();
    videoFpsLayout->addLayout(videoFpsAutoLayout);
    videoFpsLayout->addLayout(videoFpsManualLayout);

    frameRateGroup->setLayout(videoFpsLayout);

    QGroupBox *formatGroup = new QGroupBox(tr("Date and time"));

    QLabel *dateTimeFormatLabel = new QLabel(tr("Date and time format"));
    dateTimeFormatCombo = new QComboBox();
    dateTimeFormatCombo->addItem("MM-dd-HH");
    dateTimeFormatCombo->addItem("yyyy-MM-dd");
    dateTimeFormatCombo->addItem("yyyyMMdd-HH");
    dateTimeFormatCombo->addItem("yyyy-MM-dd-HH");
    dateTimeFormatCombo->addItem("yyyyMMdd-HH-mm-ss");
	dateTimeFormatCombo->addItem("yyyy-MM-dd-HH-ss.zzz");
	dateTimeFormatCombo->addItem("ddMMyyyy");
    dateTimeFormatCombo->addItem(tr("User defined"));

    connect(dateTimeFormatCombo,
        SIGNAL(currentIndexChanged(int)),
        this, SLOT(dateTimeFormatComboBoxIndexChanged()));

    QHBoxLayout *dateTimeFormatLayout = new QHBoxLayout();
    dateTimeFormatLayout->addWidget(dateTimeFormatLabel);
    dateTimeFormatLayout->addWidget(dateTimeFormatCombo);

    useEndTimeCheckBox = new QCheckBox(tr(
        "Use end time of previous video as start time of next video"));
    QHBoxLayout *useEndTimeLayout = new QHBoxLayout();
    useEndTimeLayout->insertSpacing(0, 15);
    useEndTimeLayout->addWidget(useEndTimeCheckBox);

    QVBoxLayout *formatLayout = new QVBoxLayout();
    formatLayout->addLayout(dateTimeFormatLayout);
    formatLayout->addLayout(useEndTimeLayout);
    formatGroup->setLayout(formatLayout);

    // Combine layouts to group box:
    QVBoxLayout *startTimeInFileNameLayout = new QVBoxLayout();
    startTimeInFileNameLayout->addWidget(frameRateGroup);
    startTimeInFileNameLayout->addWidget(formatGroup);
    startTimeInFileNameGroup->setLayout(startTimeInFileNameLayout);

    // Time stamps in log file
    timestampsInLogFileGroup = new QGroupBox(tr("Time stamps in log file"));

    // Time stamp in log file layout
    QGroupBox *logFileFormatGroup = new QGroupBox(tr("Log file"));
    QLabel *logFileFormatLabel = new QLabel(tr("File type of the log file"));
    logFileFormatEdit = new QLineEdit();
    
    QHBoxLayout *logFileFormatLayout = new QHBoxLayout();
    logFileFormatLayout->addWidget(logFileFormatLabel);
    logFileFormatLayout->addWidget(logFileFormatEdit);
    logFileFormatGroup->setLayout(logFileFormatLayout);

    // Combine layouts to group box
    QVBoxLayout *timestampsInLogFileLayout = new QVBoxLayout();
    timestampsInLogFileLayout->addWidget(logFileFormatGroup);
    timestampsInLogFileLayout->addStretch();

    timestampsInLogFileGroup->setLayout(logFileFormatLayout);
    
    // Combine all the layouts
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(overallSyncGroup);
    mainLayout->addWidget(startTimeInFileNameGroup);
    mainLayout->addWidget(timestampsInLogFileGroup);
    setLayout(mainLayout);

    // Update the values according to the settings stored
    QSettings settings;
    settings.beginGroup("video");

    // Frame rate
    double videoFps = settings.value("videoFps", 0).toDouble();

    if (videoFps == 0)
    {
        videoFpsRadioAuto->setChecked(true);
    }
    else {
        videoFpsRadioManual->setChecked(true);
    }

    videoFpsSpinBox->setValue(videoFps);
    videoFpsRadioManualToggled();
    

    int dateTimeFormat = settings.value("dateTimeFormat", 0).toInt();
    switch (dateTimeFormat)
    {
    case USERDEFINED:
    {
        dateTimeFormatCombo->setCurrentIndex(USERDEFINED);
        break;
    }
    case USERDEFINED_USE_END_TIME_AS_START_TIME:
    {
        dateTimeFormatCombo->setCurrentIndex(USERDEFINED);
        useEndTimeCheckBox->setChecked(true);
        break;
    }
    default:
        dateTimeFormatCombo->setCurrentIndex(dateTimeFormat);
    }
    dateTimeFormatComboBoxIndexChanged();

    // Settings for timestamps in log file
    settings.endGroup();
    settings.beginGroup("synchronisation");
    QString logFileExtension = settings.value("logFileExtension", ".log").toString();
    logFileFormatEdit->setText(logFileExtension);

    // General VideoSyncType
    VideoSyncType videoSyncType = (VideoSyncType)settings.value("videoSyncType", 0).toInt();

    if (videoSyncType == VideoSyncType::STARTTIMEINFILENAME)
    {
        startTimeInFileName->setChecked(true);
    }
    else
    {
        timestampsInLogFile->setChecked(true);
    }

    startTimeInFileNameToggled();


    settings.endGroup();
}

void SynchronisationPage::videoFpsRadioManualToggled()
{
    bool checked = videoFpsRadioManual->isChecked();

    videoFpsSpinBox->setEnabled(checked);
}

void SynchronisationPage::dateTimeFormatComboBoxIndexChanged()
{
    QString text = dateTimeFormatCombo->currentText();

    if (text == tr("User defined"))
    {
        useEndTimeCheckBox->setEnabled(true);
    }
    else {
        useEndTimeCheckBox->setEnabled(false);
        useEndTimeCheckBox->setChecked(false);
    }    
}

void SynchronisationPage::startTimeInFileNameToggled()
{
    bool checked = startTimeInFileName->isChecked();

    startTimeInFileNameGroup->setEnabled(checked);
    timestampsInLogFileGroup->setEnabled(!checked);
    

}

void SynchronisationPage::saveSettings()
{
    QSettings settings;
    settings.beginGroup("video");

    // Video Fps
    if (videoFpsRadioManual->isChecked())
    {
        settings.setValue("videoFps",videoFpsSpinBox->value());
    }
    else {
        settings.setValue("videoFps", 0);
    }
    

    // Date and the format
    int dateTimeFormat = dateTimeFormatCombo->currentIndex();

    if (dateTimeFormat == USERDEFINED)
    {
        // Check if checkbox is checked or not
        if (useEndTimeCheckBox->isChecked())
        {
            dateTimeFormat = USERDEFINED_USE_END_TIME_AS_START_TIME;
        }
    }

    settings.setValue("dateTimeFormat", dateTimeFormat);
    settings.endGroup();

    // Settings for timestamps in log file
    settings.beginGroup("synchronisation");
    qDebug() << logFileFormatEdit->text();
    settings.setValue("logFileExtension", logFileFormatEdit->text());

    if (startTimeInFileName->isChecked())
    {
        settings.setValue("videoSyncType", STARTTIMEINFILENAME);
    }
    else 
    {
        settings.setValue("videoSyncType", TIMESTAMPSINLOGFILE);
    }

    settings.endGroup();
}

ProcessingPage::ProcessingPage(QWidget * parent)
    : AbstractSettingsPage(parent)
{
    QGroupBox *playbackGroup = new QGroupBox(tr("Playback speed"));

    QRadioButton *videoSpeedRadioAuto = new QRadioButton(tr("Play video as fast as possible"));
    videoSpeedRadioManual = new QRadioButton(tr("Set maximum speed"));

    videoSpeedSpinBox = new QSpinBox();
    videoSpeedSpinBox->setSuffix(tr(" frames/sec"));
    videoSpeedSpinBox->setMinimum(1);
    videoSpeedSpinBox->setMaximum(400);
    videoSpeedSpinBox->setSingleStep(1);

    connect(videoSpeedRadioManual,
        SIGNAL(toggled(bool)),
        this, SLOT(videoSpeedRadioManualToggled()));

    QHBoxLayout *playbackAutoLayout = new QHBoxLayout();
    playbackAutoLayout->addWidget(videoSpeedRadioAuto);

    QHBoxLayout *playbackManualLayout = new QHBoxLayout();
    playbackManualLayout->addWidget(videoSpeedRadioManual);
    playbackManualLayout->addWidget(videoSpeedSpinBox);

	useGpuAccelerationCheckBox = new QCheckBox(tr("Use GPU acceleration"));

    QVBoxLayout *playbackLayout = new QVBoxLayout();
    playbackLayout->addLayout(playbackAutoLayout);
    playbackLayout->addLayout(playbackManualLayout);
	playbackLayout->addWidget(useGpuAccelerationCheckBox);
    playbackGroup->setLayout(playbackLayout);

    // Skip frames
    QGroupBox *skipFramesGroup = new QGroupBox(tr("Skip frames"));
    QRadioButton *processEveryFramesRadioAuto = new QRadioButton(tr("Process every frame"));
    processEveryFramesRadioManual = new QRadioButton(tr("Only process every"));

    processEveryFramesSpinBox = new QSpinBox();
    processEveryFramesSpinBox->setSuffix(tr("nd frame"));
    processEveryFramesSpinBox->setMinimum(2);
    processEveryFramesSpinBox->setMaximum(60);
    processEveryFramesSpinBox->setSingleStep(1);

    connect(processEveryFramesRadioManual,
        SIGNAL(toggled(bool)),
        this, SLOT(processEveryFramesRadioManualToggled()));

    connect(processEveryFramesSpinBox,
        SIGNAL(valueChanged(int)),
        this, SLOT(processEveryFramesSpinBoxValueChanged()));

    QHBoxLayout *processEveryFrameLayout = new QHBoxLayout();
    processEveryFrameLayout->addWidget(processEveryFramesRadioAuto);

    QHBoxLayout *skipProcessingLayout = new QHBoxLayout();
    skipProcessingLayout->addWidget(processEveryFramesRadioManual);
    skipProcessingLayout->addWidget(processEveryFramesSpinBox);

    QVBoxLayout *skipFramesLayout = new QVBoxLayout();
    skipFramesLayout->addLayout(processEveryFrameLayout);
    skipFramesLayout->addLayout(skipProcessingLayout);
    skipFramesGroup->setLayout(skipFramesLayout);

    // Resolution
    QGroupBox *resolutionGroup = new QGroupBox(tr("Resolution"));

    warningLowResCheckBox = new QCheckBox(tr(
        "Warn when importing low resolution videos"));
    lowResLabel = new QLabel(tr(
        "Low resolution videos are below:"));
    lowResWidthLabel = new QLabel(tr(
        "Width"));
    lowResHeightLabel = new QLabel(tr(
        "Height"));

    connect(warningLowResCheckBox,
        SIGNAL(stateChanged(int)),
        this, SLOT(warningLowResCheckBoxChecked()));

    lowResWidthSpinBox = new QSpinBox();
    lowResWidthSpinBox->setMinimum(1);
    lowResWidthSpinBox->setMaximum(9999);
    lowResWidthSpinBox->setSingleStep(1);
    lowResWidthSpinBox->setSuffix(tr(" pixels"));

    lowResHeightSpinBox = new QSpinBox();
    lowResHeightSpinBox->setMinimum(1);
    lowResHeightSpinBox->setMaximum(9999);
    lowResHeightSpinBox->setSingleStep(1);
    lowResHeightSpinBox->setSuffix(tr(" pixels"));

    QHBoxLayout *lowResWidthLayout = new QHBoxLayout();
    lowResWidthLayout->addWidget(lowResWidthLabel);
    lowResWidthLayout->addWidget(lowResWidthSpinBox);
    QHBoxLayout *lowResHeightLayout = new QHBoxLayout();
    lowResHeightLayout->addWidget(lowResHeightLabel);
    lowResHeightLayout->addWidget(lowResHeightSpinBox);

    QVBoxLayout *lowResSettingsLayout = new QVBoxLayout();
    lowResSettingsLayout->addWidget(lowResLabel);
    lowResSettingsLayout->addLayout(lowResWidthLayout);
    lowResSettingsLayout->addLayout(lowResHeightLayout);

    QHBoxLayout *displacedLowResSettingsLayout = new QHBoxLayout();
    displacedLowResSettingsLayout->addSpacing(15);
    displacedLowResSettingsLayout->addLayout(lowResSettingsLayout);

    QVBoxLayout *resolutionLayout = new QVBoxLayout();
    resolutionLayout->addWidget(warningLowResCheckBox);
    resolutionLayout->addLayout(displacedLowResSettingsLayout);
    resolutionGroup->setLayout(resolutionLayout);

    // Combine all the layouts
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(playbackGroup);
    mainLayout->addWidget(skipFramesGroup);
    mainLayout->addWidget(resolutionGroup);
    mainLayout->addStretch();
    setLayout(mainLayout);

    // Update the values according to the settings stored
    QSettings settings;
    settings.beginGroup("video");
    int playbackFps = settings.value("playbackFps", 0).toInt();

    if (playbackFps >= 400 || playbackFps <= 0)
    {
        // If the value is above or equal to 400, we interpret this
        // "as fast as possible", which is value 0
        playbackFps = 0;
        videoSpeedRadioAuto->setChecked(true);
    }
    else {
        videoSpeedRadioManual->setChecked(true);
    }

    videoSpeedSpinBox->setValue(playbackFps);
    videoSpeedRadioManualToggled();

	// GPU Acc.
	useGpuAccelerationCheckBox->setChecked(settings.value("useUmat", 1).toBool());

    // Skip frames
    int skipEveryXFrame = settings.value("processEveryXFrame", 0).toInt();

    if (skipEveryXFrame < 2)
    {
        processEveryFramesRadioAuto->setChecked(true);
    }
    else {
        processEveryFramesRadioManual->setChecked(true);
        processEveryFramesSpinBox->setValue(skipEveryXFrame);
        processEveryFramesSpinBoxValueChanged();
    }

    processEveryFramesRadioManualToggled();

    warningLowResCheckBox->setChecked(settings.value(
        "lowResWarning", 1).toBool());
    lowResWidthSpinBox->setValue(settings.value(
        "lowResWidth", 320).toInt());
    lowResHeightSpinBox->setValue(settings.value(
        "lowResHeight", 160).toInt());
    warningLowResCheckBoxChecked();

    settings.endGroup();
}

void ProcessingPage::videoSpeedRadioManualToggled()
{
    bool checked = videoSpeedRadioManual->isChecked();

    videoSpeedSpinBox->setEnabled(checked);
}

void ProcessingPage::processEveryFramesRadioManualToggled()
{
    bool checked = processEveryFramesRadioManual->isChecked();

    processEveryFramesSpinBox->setEnabled(checked);
}

void ProcessingPage::processEveryFramesSpinBoxValueChanged()
{
    int processEvery = processEveryFramesSpinBox->value();

    switch (processEvery)
    {
    case 2:
        processEveryFramesSpinBox->setSuffix("nd frame");
        break;
    case 3:
        processEveryFramesSpinBox->setSuffix("rd frame");
        break;
    default:
        processEveryFramesSpinBox->setSuffix("th frame");
        break;
    }
}

void ProcessingPage::warningLowResCheckBoxChecked()
{
    bool checked = warningLowResCheckBox->isChecked();

    lowResLabel->setEnabled(checked);
    lowResWidthLabel->setEnabled(checked);
    lowResHeightLabel->setEnabled(checked);
    lowResWidthSpinBox->setEnabled(checked);
    lowResHeightSpinBox->setEnabled(checked);
}

void ProcessingPage::saveSettings()
{
    QSettings settings;
    settings.beginGroup("video");

    if (videoSpeedRadioManual->isChecked())
    {
        settings.setValue("playbackFps", videoSpeedSpinBox->value());
    }
    else {
        settings.setValue("playbackFps", 0);
    }

	settings.setValue("useUmat", useGpuAccelerationCheckBox->isChecked());


    // Skip frames
    if (processEveryFramesRadioManual->isChecked())
    {
        settings.setValue("processEveryXFrame", processEveryFramesSpinBox->value());
    }
    else {
        settings.setValue("processEveryXFrame", 0);
    }

    settings.setValue("lowResWarning",
        warningLowResCheckBox->isChecked());
    settings.setValue("lowResWidth", lowResWidthSpinBox->value());
    settings.setValue("lowResHeight", lowResHeightSpinBox->value());

    settings.endGroup();
}

DebugPage::DebugPage(QWidget * parent)
    : AbstractSettingsPage(parent)
{
	QGroupBox *presenceStatsGroup = new QGroupBox(tr("Presence detector"));
	debugWindowForegroundMaskCheckBox = new QCheckBox(tr("Show foreground mask"));

	QVBoxLayout *presenceStatsLayout = new QVBoxLayout();
	presenceStatsLayout->addWidget(debugWindowForegroundMaskCheckBox);

	presenceStatsGroup->setLayout(presenceStatsLayout);

    QVBoxLayout *mainLayout = new QVBoxLayout();
	mainLayout->addWidget(presenceStatsGroup);

    mainLayout->insertStretch(-1);
    setLayout(mainLayout);

    QSettings settings;
    settings.beginGroup("debug");


	debugWindowForegroundMaskCheckBox->setChecked(settings.value(
		"showForegroundMaskDebugWindow", 0).toBool());
    settings.endGroup();
}

void DebugPage::saveSettings()
{
    QSettings settings;

	settings.setValue("showForegroundMaskDebugWindow",
		debugWindowForegroundMaskCheckBox->isChecked());

    settings.endGroup();
}


RecordingPage::RecordingPage(QWidget * parent)
	: AbstractSettingsPage(parent)
{
	// Skip frames
	QGroupBox *recordGroup = new QGroupBox(tr("Record video"));
	QRadioButton *writeSingleVideoRadio = new QRadioButton(tr("Record to a single file"));
	writeMultipleVideosRadio = new QRadioButton(tr("Create a new recording for each \"jump\" of at least"));

	maxConsecutiveTimeDifferenceInMillisecSpinBox = new QSpinBox();
	maxConsecutiveTimeDifferenceInMillisecSpinBox->setSuffix(tr(" msec"));
	maxConsecutiveTimeDifferenceInMillisecSpinBox->setMinimum(100);
	maxConsecutiveTimeDifferenceInMillisecSpinBox->setMaximum(9999999);
	maxConsecutiveTimeDifferenceInMillisecSpinBox->setSingleStep(100);

	connect(writeMultipleVideosRadio,
		SIGNAL(toggled(bool)),
		this, SLOT(writeMultipleVideosRadioToggled()));


	QHBoxLayout *writeSingleVideoLayout = new QHBoxLayout();
	writeSingleVideoLayout->addWidget(writeSingleVideoRadio);

	QHBoxLayout *writeMultipleVideosLayout = new QHBoxLayout();
	writeMultipleVideosLayout->addWidget(writeMultipleVideosRadio);
	writeMultipleVideosLayout->addWidget(maxConsecutiveTimeDifferenceInMillisecSpinBox);

	QVBoxLayout *recordLayout = new QVBoxLayout();
	recordLayout->addLayout(writeSingleVideoLayout);
	recordLayout->addLayout(writeMultipleVideosLayout);
	recordGroup->setLayout(recordLayout);

	// Combine all the layouts
	QVBoxLayout *mainLayout = new QVBoxLayout();
	mainLayout->addWidget(recordGroup);
	mainLayout->addStretch();
	setLayout(mainLayout);

	// Update the values according to the settings stored
	QSettings settings;
	settings.beginGroup("recording");
	bool writeMultipleVideo = settings.value("writeMultipleVideo", 0).toBool();

	int maxConsecutiveTimeDifferenceInMillisec = settings.value("maxConsecutiveTimeDifferenceInMillisec", 1000).toInt();

	writeMultipleVideosRadio->setChecked(writeMultipleVideo);
	writeSingleVideoRadio->setChecked(!writeMultipleVideo);
	maxConsecutiveTimeDifferenceInMillisecSpinBox->setValue(maxConsecutiveTimeDifferenceInMillisec);

	
	writeMultipleVideosRadioToggled();

	settings.endGroup();
}

void RecordingPage::writeMultipleVideosRadioToggled()
{
	bool checked = writeMultipleVideosRadio->isChecked();
	maxConsecutiveTimeDifferenceInMillisecSpinBox->setEnabled(checked);
}

void RecordingPage::saveSettings()
{
	QSettings settings;
	settings.beginGroup("recording");
	settings.setValue("writeMultipleVideo",
		writeMultipleVideosRadio->isChecked());
	settings.setValue("maxConsecutiveTimeDifferenceInMillisec",
		maxConsecutiveTimeDifferenceInMillisecSpinBox->value());
	settings.endGroup();
}