﻿Module modCommon

  Public Function TextToFullDate(ByVal TextDate As String) As Date
    Dim _Year As Integer
    Dim _Month As Integer
    Dim _Day As Integer
    Dim _Hours As Integer
    Dim _Minutes As Integer
    Dim _Seconds As Integer
    Dim _MilliSeconds As Integer
    Dim _OutputDate As Date

    _Year = Integer.Parse(TextDate.Substring(0, 4))
    _Month = Integer.Parse(TextDate.Substring(5, 2))
    _Day = Integer.Parse(TextDate.Substring(8, 2))

    _Hours = Integer.Parse(TextDate.Substring(11, 2))
    _Minutes = Integer.Parse(TextDate.Substring(14, 2))
    _Seconds = Integer.Parse(TextDate.Substring(17, 2))
    _MilliSeconds = Integer.Parse(TextDate.Substring(20, 3))

    _OutputDate = New Date(_Year, _Month, _Day, _Hours, _Minutes, _Seconds, _MilliSeconds)
    Return _OutputDate
  End Function

  Public Function TextToShortDate(ByVal TextDate As String) As Date
    Dim _Year As Integer
    Dim _Month As Integer
    Dim _Day As Integer
    Dim _OutputDate As Date

    _Year = Integer.Parse(TextDate.Substring(0, 4))
    _Month = Integer.Parse(TextDate.Substring(5, 2))
    _Day = Integer.Parse(TextDate.Substring(8, 2))

    _OutputDate = New Date(_Year, _Month, _Day)
    Return _OutputDate
  End Function


  Public Function IsBetweenOrdered(ByVal Value As Integer, ByVal Threshold1 As Integer, ByVal Threshold2 As Integer) As Boolean
    If (Value >= Threshold1) And (Value <= Threshold2) Then
      Return True
    Else
      Return False
    End If
  End Function
  Public Function IsBetweenNoOrder(ByVal Value As Integer, ByVal Threshold1 As Integer, ByVal Threshold2 As Integer) As Boolean
    Dim _a, _b As Integer


    _a = Threshold1
    _b = Threshold2
    If _a > _b Then Swap(_a, _b)
    Return IsBetweenOrdered(Value, _a, _b)

  End Function


  Public Function IsBetweenOrdered(ByVal Value As Double, ByVal Threshold1 As Double, ByVal Threshold2 As Double) As Boolean
    If (Value >= Threshold1) And (Value <= Threshold2) Then
      Return True
    Else
      Return False
    End If
  End Function

  Public Sub Swap(ByRef a As Integer, ByRef b As Integer)
    Dim _c As Integer


    _c = a
    a = b
    b = _c
  End Sub


End Module
