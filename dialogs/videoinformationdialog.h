#ifndef VIDEOINFORMATIONDIALOG_H
#define VIDEOINFORMATIONDIALOG_H

#include <QDialog>
#include <QFormLayout>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QLayout>
#include <QDialogButtonBox>
#include <QFileInfo>

#include "utils.hpp"

class VideoInformationDialog : public QDialog
{
    Q_OBJECT;

public:
    VideoInformationDialog(QWidget* parent, QVBoxLayout* layout);

    public slots:

private:
    QDialogButtonBox *buttonBox;
};


#endif // VIDEOINFORMATIONDIALOG_H