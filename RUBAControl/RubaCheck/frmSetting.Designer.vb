﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Two = New System.Windows.Forms.Button()
        Me.One = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Two
        '
        Me.Two.Location = New System.Drawing.Point(12, 12)
        Me.Two.Name = "Two"
        Me.Two.Size = New System.Drawing.Size(117, 52)
        Me.Two.TabIndex = 0
        Me.Two.Text = "Two detectors"
        Me.Two.UseVisualStyleBackColor = True
        '
        'One
        '
        Me.One.Location = New System.Drawing.Point(12, 97)
        Me.One.Name = "One"
        Me.One.Size = New System.Drawing.Size(117, 52)
        Me.One.TabIndex = 1
        Me.One.Text = "One detector"
        Me.One.UseVisualStyleBackColor = True
        '
        'frmSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(143, 172)
        Me.Controls.Add(Me.One)
        Me.Controls.Add(Me.Two)
        Me.Name = "frmSetting"
        Me.Text = "Setting"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Two As Button
    Friend WithEvents One As Button
End Class
