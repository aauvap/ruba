﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGapWindow
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
        Me.txtUpperThreshold = New System.Windows.Forms.TextBox()
        Me.txtLowerThreshold = New System.Windows.Forms.TextBox()
        Me.lblTimeGap = New System.Windows.Forms.Label()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtUpperThreshold
        '
        Me.txtUpperThreshold.Location = New System.Drawing.Point(174, 62)
        Me.txtUpperThreshold.Name = "txtUpperThreshold"
        Me.txtUpperThreshold.Size = New System.Drawing.Size(69, 20)
        Me.txtUpperThreshold.TabIndex = 0
        Me.txtUpperThreshold.Text = "2000"
        '
        'txtLowerThreshold
        '
        Me.txtLowerThreshold.Location = New System.Drawing.Point(12, 62)
        Me.txtLowerThreshold.Name = "txtLowerThreshold"
        Me.txtLowerThreshold.Size = New System.Drawing.Size(69, 20)
        Me.txtLowerThreshold.TabIndex = 1
        Me.txtLowerThreshold.Text = "0"
        '
        'lblTimeGap
        '
        Me.lblTimeGap.AutoSize = True
        Me.lblTimeGap.Location = New System.Drawing.Point(87, 65)
        Me.lblTimeGap.Name = "lblTimeGap"
        Me.lblTimeGap.Size = New System.Drawing.Size(71, 13)
        Me.lblTimeGap.TabIndex = 2
        Me.lblTimeGap.Text = "< Time Gap ≤"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(174, 124)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(69, 25)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 124)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "*Not relevant for"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 141)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(107, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "RUBA Combine input"
        '
        'frmGapWindow
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(256, 163)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.lblTimeGap)
        Me.Controls.Add(Me.txtLowerThreshold)
        Me.Controls.Add(Me.txtUpperThreshold)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmGapWindow"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Time Gap interval"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtUpperThreshold As TextBox
  Friend WithEvents txtLowerThreshold As TextBox
  Friend WithEvents lblTimeGap As Label
  Friend WithEvents btnOK As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
