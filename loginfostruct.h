#ifndef LOGINFOSTRUCT_H
#define LOGINFOSTRUCT_H

#include <QDateTime>
#include <QVector>
#include <QFileInfo>
#include <opencv2/opencv.hpp>
#include "ruba.h"

struct ruba::LogInfoStruct{
	QFileInfo filename;
	QDate date;
    QVector<QDateTime> logDataTime;
    QVector<double> logDataDouble;
    QVector<double> logDataInt;
    QVector<QString> logDataString;
	
    QVector<QDateTime> collateEventsUntil;
	QVector<int> maxActiveDuration;
	
	QDateTime CreateEventAtMin;
	QDateTime CreateEventAtMax;

    QVector<QVector<FrameStruct> > logFrames;
};


#endif // LOGINFOSTRUCT_H
