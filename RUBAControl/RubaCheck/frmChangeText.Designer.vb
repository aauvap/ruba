﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangeText
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ChangeText = New System.Windows.Forms.Button()
        Me.OutputName = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(31, 99)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(74, 20)
        Me.TextBox6.TabIndex = 6
        Me.TextBox6.Text = "6"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(111, 99)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(74, 20)
        Me.TextBox7.TabIndex = 7
        Me.TextBox7.Text = "7"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(193, 99)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(74, 20)
        Me.TextBox8.TabIndex = 8
        Me.TextBox8.Text = "8"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(275, 99)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(74, 20)
        Me.TextBox9.TabIndex = 9
        Me.TextBox9.Text = "9"
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(355, 99)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(74, 20)
        Me.TextBox10.TabIndex = 10
        Me.TextBox10.Text = "10"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(355, 46)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(74, 20)
        Me.TextBox5.TabIndex = 5
        Me.TextBox5.Text = "5"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(193, 46)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(74, 20)
        Me.TextBox3.TabIndex = 3
        Me.TextBox3.Text = "3"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(275, 46)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(74, 20)
        Me.TextBox4.TabIndex = 4
        Me.TextBox4.Text = "4"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(112, 46)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(74, 20)
        Me.TextBox2.TabIndex = 2
        Me.TextBox2.Text = "2"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(31, 46)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(74, 20)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = "1"
        '
        'ChangeText
        '
        Me.ChangeText.Location = New System.Drawing.Point(30, 126)
        Me.ChangeText.Name = "ChangeText"
        Me.ChangeText.Size = New System.Drawing.Size(75, 27)
        Me.ChangeText.TabIndex = 11
        Me.ChangeText.Text = "Done"
        Me.ChangeText.UseVisualStyleBackColor = True
        '
        'OutputName
        '
        Me.OutputName.Location = New System.Drawing.Point(144, 12)
        Me.OutputName.Name = "OutputName"
        Me.OutputName.Size = New System.Drawing.Size(150, 20)
        Me.OutputName.TabIndex = 34
        Me.OutputName.Text = "output.csv"
        '
        'frmChangeText
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(459, 175)
        Me.Controls.Add(Me.OutputName)
        Me.Controls.Add(Me.ChangeText)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.TextBox8)
        Me.Controls.Add(Me.TextBox9)
        Me.Controls.Add(Me.TextBox10)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Name = "frmChangeText"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents TextBox10 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents ChangeText As Button
    Friend WithEvents OutputName As TextBox
End Class
