#ifndef SINGLEMODULE_H
#define SINGLEMODULE_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>
#include "dialogs/detectortypeselectiondialog.h"
#include "abstractcompounddetector.h"

class ruba::SingleModule : public AbstractCompoundDetector
{

public:
    explicit SingleModule(BaseDetectorParams baseParams, QWidget *parent = 0, QString configFile = "");

    ruba::DetectorOutput GetOutput();

private:
    void setDetectorSpecificDefaultParameters();

    QList<QString> getDetectorSpecificExtendedDebugInfo();
    void updateDetectorSpecificLogic();
};

#endif // SINGLEMODULE_H
