#include "groundtruthannotator.h"




using namespace std;
using namespace cv;
using namespace ruba;


GroundTruthAnnotator::GroundTruthAnnotator(BaseDetectorParams baseParams, QWidget *parent)
    : AbstractCompoundDetector(configureBaseDetectorParams(baseParams, GROUNDTRUTHANNOTATOR))
{
    this->IsInitialized = true;

    gtDialog = new GroundTruthAnnotatorDialog(parent, baseParams.initFrames);

    gtDialog->show();

    IsTriggered.clear();
    IsTriggered.push_back(false);
}


GroundTruthAnnotator::~GroundTruthAnnotator()
{
    gtDialog->close();
    delete gtDialog;
}


DetectorOutput GroundTruthAnnotator::GetOutput()
{
    DetectorOutput outputStruct;
    return outputStruct;
}

void GroundTruthAnnotator::setDetectorSpecificDefaultParameters()
{
}

void GroundTruthAnnotator::SetLastFrameReached()
{
    // Do nothing.
}

void GroundTruthAnnotator::Reconfigure(vector<FrameStruct> initFrames, QWidget *parent)
{
    gtDialog->show();
    gtDialog->raise();

}

void GroundTruthAnnotator::updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput)
{
    gtDialog->updateFrame(frames);
}

void GroundTruthAnnotator::updateDetectorSpecificLogic()
{
    // Do nothing. The call to this detector is overwritten by the updateDetectorSpecifics method
}


QList<QString> GroundTruthAnnotator::getDetectorSpecificDebugInfo()
{
    return gtDialog->getLatestTimestamps();
}

QList<QString> GroundTruthAnnotator::getDetectorSpecificExtendedDebugInfo()
{
    QList<QString> extDebugList;

    return extDebugList;
}

std::vector<std::vector<MaskOverlayStruct> > GroundTruthAnnotator::GetMasks()
{
    return gtDialog->GetMasks();
}




