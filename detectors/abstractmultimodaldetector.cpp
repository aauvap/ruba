#include "abstractmultimodaldetector.h"

using namespace std;
using namespace cv;

using namespace ruba;


AbstractMultiModalDetector::AbstractMultiModalDetector(BaseDetectorParams baseParams, ChartColors chartColors)
    : AbstractTrafficDetector(baseParams)
{
	this->chartColors = chartColors;
}

void AbstractMultiModalDetector::Reconfigure()
{

}

void AbstractMultiModalDetector::SetMasks(std::vector<cv::Mat> maskImages)
{
    if (maskImages.size() != masks.size())
    {
        return;
    }

    // TODO: This function does currently not support multi-modal
    for (size_t i = 0; i < maskImages.size(); ++i)
    {
        maskImages[i].copyTo(this->masks[i]);
    }

    InitDetector();

}

std::vector<ChartStruct> AbstractMultiModalDetector::GetChartStructs()
{
    vector<ChartStruct> outputCharts;

    for (size_t i = 0; i < singleModalDetectors.size(); ++i)
    {
        vector<ChartStruct> tmpCharts = singleModalDetectors[i]->GetChartStructs();

        outputCharts.insert(outputCharts.end(), tmpCharts.begin(), tmpCharts.end());
    }

    return outputCharts;
}

vector<int> AbstractMultiModalDetector::GetChartData()
{
    std::vector<int> data;

    for (size_t i = 0; i < singleModalDetectors.size(); ++i)
    {
        data.push_back(singleModalDetectors[i]->GetChartData());
    }

    return data;
}

std::vector<double> AbstractMultiModalDetector::GetChartThresholds()
{
    std::vector<double> thresholds;

    for (size_t i = 0; i < singleModalDetectors.size(); ++i)
    {
        thresholds.push_back(singleModalDetectors[i]->GetChartThreshold());
    }

    return thresholds;
}

void ruba::AbstractMultiModalDetector::setChartStatus(bool enabled, bool triggered)
{
	for (auto detector : singleModalDetectors)
	{
		if (enabled)
		{
			if (triggered)
			{
				detector->SetChartBackground(chartColors.backgroundColors.enabledTriggeredColor);
				detector->SetChartLine(chartColors.lineUnderColors.enabledTriggeredColor, 
					chartColors.lineOverColors.enabledTriggeredColor);
			}
			else {
				detector->SetChartBackground(chartColors.backgroundColors.enabledNotTriggeredColor);
				detector->SetChartLine(chartColors.lineUnderColors.enabledNotTriggeredColor,
					chartColors.lineOverColors.enabledNotTriggeredColor);
			}
		}
		else {
			detector->SetChartBackground(chartColors.backgroundColors.notEnabledColor);
			detector->SetChartLine(chartColors.lineUnderColors.notEnabledColor, 
				chartColors.lineOverColors.notEnabledColor);
		}
	}
}

DetectorOutput AbstractMultiModalDetector::GetOutput()
{
    DetectorOutput outputStruct;

    outputStruct.Confidence = this->confidence;
    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = this->IsTriggered;
    outputStruct.LastChange = this->lastChange;
    outputStruct.Timestamp = getCurrentTime();

    return outputStruct;   
}

void AbstractMultiModalDetector::SetUseScrollingChart(bool useScrollingChart)
{
    for (size_t i = 0; i < singleModalDetectors.size(); ++i)
    {
        singleModalDetectors[i]->SetUseScrollingChart(useScrollingChart);
    }

    this->UseScrollingChart = useScrollingChart;
}

double AbstractMultiModalDetector::GetConfidence()
{
    return this->confidence[0];
}

/*! Default update handling. If child classes wish to do something else, they must simply override
    the method
*/
void AbstractMultiModalDetector::updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput)
{
    if (frames.empty()) {
        return;
    }

    confidence[0] = 0;

    if (frames.size() != singleModalDetectors.size())
    {
        IsInitialized = false;
        return;
    }

    for (size_t i = 0; i < singleModalDetectors.size(); ++i)
    {
        singleModalDetectors[i]->Update(std::vector<FrameStruct>(1, frames[i]));

        DetectorOutput unimodal = singleModalDetectors[i]->GetOutput();

        // Update parameters
        for (size_t j = 0; j < unimodal.Confidence.size(); ++j)
        {
            if (confidence.size() > j)
            {
                confidence[j] = confidence[j] + unimodal.Confidence[j];
            }
        }

        if (unimodal.LastChange[0] > lastChange[0])
        {
            lastChange = unimodal.LastChange;
        }
    }

    // TODO: Find more clever way to update the confidence
    confidence[0] = confidence[0] / singleModalDetectors.size();

    if (confidence[0] > 0.5)
    {
        IsTriggered[0] = true;
    }
    else {
        IsTriggered[0] = false;
    }

    triggeredHistory[0].dequeue();
    triggeredHistory[0].enqueue(IsTriggered[0]);
}