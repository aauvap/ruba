#ifndef ABSTRACTMULTIMODALDETECTOR_H
#define ABSTRACTMULTIMODALDETECTOR_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>

#include <memory>


#include "abstracttrafficdetector.h"
#include "abstractsinglemodaldetector.h"
#include "framestruct.h"
#include "ruba.h"
#include "ChartStruct.h"
#include "scrollingchart.h"
#include "detectoroutputstruct.h"
#include "presencedetectorsinglemodal.h"
#include "movementdetectorsinglemodal.h"
#include "trafficlightdetectorsinglemodal.h"

struct BaseColors {
	cv::Scalar enabledTriggeredColor;
	cv::Scalar enabledNotTriggeredColor;
	cv::Scalar notEnabledColor;
};

struct ChartColors {
	BaseColors backgroundColors;
	BaseColors lineUnderColors;
	BaseColors lineOverColors;
};



class ruba::AbstractMultiModalDetector : public AbstractTrafficDetector
{

public:
    AbstractMultiModalDetector(BaseDetectorParams baseParams, ChartColors chartColors);

    virtual void Reconfigure();
    ruba::DetectorOutput GetOutput();
    double GetConfidence();

    void SetLastFrameReached();
    qint64 GetConsecutiveEventDelay();

    // Functions for reconfiguring the detector
    void SetMasks(std::vector<cv::Mat> maskImages);

    // Functions for accessing the charts
    std::vector<ruba::ChartStruct> GetChartStructs();
    void SetUseScrollingChart(bool useScrollingChart);
    

    std::vector<int> GetChartData();
    std::vector<double> GetChartThresholds();
    std::vector<cv::Mat*> GetChartImage();
    virtual std::vector<QString*> GetChartTitles() = 0;
	virtual void setChartStatus(bool enabled, bool triggered);

protected:
    virtual void updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput = std::vector<DetectorOutput>());
	virtual ChartColors initialiseChartColors() = 0;
	virtual ChartColors getChartColors() { return chartColors; };


    std::vector<double> confidence;
    QList<QString> debugInfo;
    bool UseScrollingChart;
    std::vector<ScrollingChart> Charts;

    std::vector<cv::Mat> masks;
    std::vector<std::shared_ptr<ruba::AbstractSingleModalDetector> > singleModalDetectors;

private:
	ChartColors chartColors;
};

#endif // ABSTRACTMULTIMODALDETECTOR_H
