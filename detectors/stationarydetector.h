#ifndef STATIONARYDETECTOR_H
#define STATIONARYDETECTOR_H

#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include <opencv2/opencv.hpp>

#include "abstractmultimodaldetector.h"
#include "ruba.h"
#include "framestruct.h"
#include "ChartStruct.h"
#include "scrollingchart.h"
#include "maskoverlaystruct.h"
#include "utils.hpp"


class ruba::StationaryDetector : public AbstractMultiModalDetector
{

public:
    StationaryDetector(BaseDetectorParams baseParams, ruba::MaskHandlerOutputStruct initParams,
                                bool useScrollingChart);

    ~StationaryDetector();

    ruba::DetectorOutput GetOutput();
   
    std::vector<ruba::ChartStruct> GetChartStructs();

    void SetUseScrollingChart(bool useScrollingChart);
    std::vector<QString*> GetChartTitles();
	virtual void setChartStatus(bool enabled, bool triggered);

    void InitDetector();
    bool GetIsPresenceDetectorTriggered();
    QList<QString> getDetectorSpecificDebugInfo();


protected:
	virtual ChartColors initialiseChartColors();

private:
    virtual void updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput = std::vector<DetectorOutput>());
    std::vector<DoubleScrollingChart*> DoubleCharts;

    std::vector<ruba::PresenceDetector> PDetectors;
    std::vector<ruba::MovementDetector> MDetectors;


    bool isPresenceDetectorTriggered;
    bool isPrevPresenceDetectorTriggered;
};

#endif // STATIONARYDETECTOR_H
