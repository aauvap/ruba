#ifndef ABSTRACTSETTINGSPAGE_H
#define ABSTRACTSETTINGSPAGE_H
#include <QWidget>

class AbstractSettingsPage : public QWidget
{  

public:
    AbstractSettingsPage(QWidget *parent = 0);


    virtual void saveSettings();
    
};



#endif // !ABSTRACTSETTINGSPAGE_H

