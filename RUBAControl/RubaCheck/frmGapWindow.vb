﻿Public Class frmGapWindow


  Public LowerThreshold As Integer
  Public UpperThreshold As Integer

  Private Sub txtLowerThreshold_TextChanged(sender As Object, e As EventArgs) Handles txtLowerThreshold.TextChanged

    If Integer.TryParse(txtLowerThreshold.Text, LowerThreshold) Then
      txtLowerThreshold.ForeColor = Color.Black
      btnOK.Enabled = True
    Else
      txtLowerThreshold.ForeColor = Color.Red
      btnOK.Enabled = False
    End If
  End Sub

  Private Sub txtUpperThreshold_TextChanged(sender As Object, e As EventArgs) Handles txtUpperThreshold.TextChanged

    If Integer.TryParse(txtUpperThreshold.Text, UpperThreshold) Then
      txtUpperThreshold.ForeColor = Color.Black
      btnOK.Enabled = True
    Else
      txtUpperThreshold.ForeColor = Color.Red
      btnOK.Enabled = False
    End If
  End Sub

  Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
    Me.Hide()
  End Sub
End Class