#include "trafficlightdetector.h"
#include <QDebug>

using namespace cv;
using namespace std;
using namespace ruba;


TrafficLightDetector::TrafficLightDetector(BaseDetectorParams baseParams, std::vector<cv::Mat> backimages, std::vector<cv::Mat> maskimages, 
                                           std::vector<cv::Point> tlPlacements, int tlTypeTrigger, bool usechart)
    : AbstractMultiModalDetector(configureBaseDetectorParams(baseParams, TRAFFICLIGHTDETECTOR),
		initialiseChartColors())
{
   
    
    for (size_t i = 0; i < maskimages.size(); ++i)
    {
        if (!maskimages[i].data && maskimages[i].empty()) {
           
            return;
        }
        
        BaseDetectorParams newBaseParams;
        newBaseParams.detectorID = 100 + i;
        newBaseParams.detectorType = MOVEMENTDETECTOR;
        newBaseParams.initFrames = vector<FrameStruct>(1, baseParams.initFrames[i]);


        shared_ptr<TrafficLightDetectorSingleModal> tl = make_shared<TrafficLightDetectorSingleModal>(newBaseParams, maskimages[i], usechart, tlPlacements, tlTypeTrigger);
        TLDetectors.push_back(tl);
        singleModalDetectors.push_back(tl);
    }

    this->IsTriggered.resize(1);
    IS_FIRST_RUN = true;


    for (size_t i = 0; i < maskimages.size(); ++i)
    {
        ScrollingTLChart* chart = new ScrollingTLChart(300, 300, getCurrentViews()[i]);
        chart->SetTitle("TLDetector " + Utils::GetModalityName(getCurrentViews()[i].modality));
        TLCharts.push_back(chart);
    }
}

/*
if (masks.size() != minVecLengthThreshold.size()
    || flowDirectionRange.size() != minVecLengthThreshold.size()
    || triggerThreshold.size() != minVecLengthThreshold.size())
{
    isInitialized = false;
    return;
}
*/
//cv::m


DetectorOutput TrafficLightDetector::GetOutput()
{
    DetectorOutput outputStruct;

    outputStruct.Confidence = this->confidence;
    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = this->IsTriggered;
    outputStruct.LastChange = this->lastChange;
    outputStruct.Timestamp = getCurrentTime();

    outputStruct.tlDetectionClass = this->tlDetectionClass;

    outputStruct.valAtTlIndices = this->tlClassVals;

    return outputStruct;
}

void TrafficLightDetector::InitDetector()
{
    for (size_t i = 0; i < TLDetectors.size(); ++i)
    {
        TLDetectors[i]->InitDetector();
    }

}

ChartColors ruba::TrafficLightDetector::initialiseChartColors()
{
	ChartColors chartColors;
	chartColors.backgroundColors.enabledTriggeredColor = cv::Scalar(200, 200, 255);
	chartColors.backgroundColors.enabledNotTriggeredColor = cv::Scalar(220, 220, 255);
	chartColors.backgroundColors.notEnabledColor = cv::Scalar::all(255);

	chartColors.lineUnderColors.enabledTriggeredColor = cv::Scalar(220, 0, 0);
	chartColors.lineUnderColors.enabledNotTriggeredColor = cv::Scalar(220, 100, 100);
	chartColors.lineUnderColors.notEnabledColor = cv::Scalar::all(150);

	chartColors.lineOverColors.enabledTriggeredColor = cv::Scalar(100, 100, 255);
	chartColors.lineOverColors.enabledNotTriggeredColor = cv::Scalar(0, 0, 220);
	chartColors.lineOverColors.notEnabledColor = cv::Scalar::all(150);

	return chartColors;
}



void TrafficLightDetector::updateDetectorSpecifics(std::vector<FrameStruct> frames, std::vector<DetectorOutput> dOutput)
{
    // To do later

    tlDetectionClass = tlTypeClasses::AMBIGIOUS;

    QList<ViewInfo> views = getCurrentViews();

    for (size_t i = 0; i < TLDetectors.size(); ++i)
    {
        TLDetectors[i]->Update(std::vector<FrameStruct>(1, frames[i]));
        
        DetectorOutput unimodal = TLDetectors[i]->GetOutput();

        if (views.size() > i)
        {
            frames[i].viewInfo = views[i];
        }
        else {
            // If there exists a mismatch, create an empty ViewInfo
            frames[i].viewInfo = ViewInfo();
        }
        
        
        if (i == 0)
        {
            tlDetectionClass = unimodal.tlDetectionClass;
        }
        else if (tlDetectionClass == unimodal.tlDetectionClass) {
            tlDetectionClass = unimodal.tlDetectionClass;
        }
        else {
            tlDetectionClass = tlTypeClasses::AMBIGIOUS;
        }

        this->tlClassVals = unimodal.valAtTlIndices;

        //IsTriggered.clear()
        // Update parameters
        if (unimodal.IsTriggered[0]) {
            this->IsTriggered[0] = true;
        }
        else {
            this->IsTriggered[0] = false;
        }
        

        //triggeredHistory[0].dequeue();
        //triggeredHistory[0].enqueue(IsTriggered[0]);

        /*if (unimodal.LastChange[0] > lastChange[0])
        {
            lastChange = unimodal.LastChange;
        }*/
    }

    if (UseScrollingChart)
    {
        TLCharts[0]->AddDataPoint(tlDetectionClass);
    }

  
}

QList<QString> TrafficLightDetector::getDetectorSpecificDebugInfo()
{
    QList<QString> debugList;
    debugList.push_back(Utils::getTLStateName(tlDetectionClass));

    int localValAtRed = 0;
    int localValAtYellow = 0;
    int localValAtGreen = 0;
   
    if (tlClassVals.size() > 2)
    {
        if (tlClassVals[0] > 0 && tlClassVals[0] < 256) localValAtRed = tlClassVals[0];
        if (tlClassVals[1] > 0 && tlClassVals[1] < 256) localValAtYellow = tlClassVals[1];
        if (tlClassVals[2] > 0 && tlClassVals[2] < 256) localValAtGreen = tlClassVals[2];
    }

    debugList.push_back("valAtRed:" + QString::number(localValAtRed));
    debugList.push_back("valAtYellow:" + QString::number(localValAtYellow));
    debugList.push_back("valAtGreen:" + QString::number(localValAtGreen));

    
    return debugList;
}

std::vector<ChartStruct> TrafficLightDetector::GetChartStructs()
{
    vector<ChartStruct> charts;
    for (size_t i = 0; i < TLCharts.size(); ++i)
    {
        ChartStruct chart = ChartStruct();
        chart.Image = this->TLCharts[i]->GetChartImage();
        chart.Title = this->TLCharts[i]->GetTitle();
        chart.View = this->TLCharts[i]->GetView();
        charts.push_back(chart);
    }

    return charts;
}

void TrafficLightDetector::SetUseScrollingChart(bool useScrollingChart)
{
	this->UseScrollingChart = useScrollingChart;
}

std::vector<QString*> TrafficLightDetector::GetChartTitles()
{
    std::vector<QString*> debug;
    return debug;
}