#include <cmath>
#include "interactiondetector.h"
#include "opencvviewer.h"

using namespace std;
using namespace cv;

using namespace ruba;

InteractionDetector::InteractionDetector(BaseDetectorParams baseParams, QWidget *parent, std::vector<QString> detectorNames, std::vector<DetectorOutput> detectorParams,
                                         QVector<qint64> consecutiveEventDelays, QString configFile, bool showGui)
    : AbstractCompoundDetector(configureBaseDetectorParams(baseParams, INTERACTIONDETECTOR))
{
    windowIcon = new QIcon("://icons/star.png");

    LoadConfiguration(baseParams.initFrames, parent, configFile, showGui, detectorParams);

    // Configure the consecutiveEventDelays
    for (int i = 0; i < detectorParams.size(); ++i)
    {
        // Find the consecutive event delay of the car detector
        if (interactionType == InteractionType::d312)
        {
            if (detectorParams[i].DetectorType == RIGHTTURNINGCAR)
            {
                carEnteringConsecutiveEventDelay = consecutiveEventDelays[i];
            }
        }
        else if (interactionType == InteractionType::d410)
        {
            if (detectorParams[i].DetectorType == LEFTTURNINGCAR)
            {
                carEnteringConsecutiveEventDelay = consecutiveEventDelays[i];
            }
        }

        if (detectorParams[i].DetectorType == STRAIGHTGOINGCYCLIST)
        {
            cyclistEnteringConsecutiveEventDelay = consecutiveEventDelays[i];
        }
    }
}


DetectorOutput InteractionDetector::GetOutput()
{
    DetectorOutput outputStruct;

    if (this->IsInitialized)
    {
        outputStruct.Confidence.push_back(1);
    } else {
        outputStruct.Confidence.push_back(0);
    }

    outputStruct.DetectorID = getDetectorID();
    outputStruct.DetectorType = getDetectorType();
    outputStruct.IsTriggered = IsTriggered;
    outputStruct.EnteringConflictZone = QDateTime();
    outputStruct.LeavingConflictZone = QDateTime();

    outputStruct.LastChange.push_back(lastChange);

    outputStruct.Timestamp = frame.Timestamp;


    return outputStruct;
}

void InteractionDetector::setDetectorSpecificDefaultParameters()
{
    //if (childDetectors.empty()) {
    //    IsInitialized = false;
    //    return;
    //}
    //else {
    //    if (childDetectors[0].detectorType == RIGHTTURNINGCAR) {
    //        interactionType = d312;
    //        rightTurningCarID = childDetectors[0].detectorID;
    //    }
    //    else if (childDetectors[0].detectorType == LEFTTURNINGCAR) {
    //        interactionType = d410;
    //        leftTurningCarID = childDetectors[0].detectorID;
    //    }
    //}

    //if (AddSettings.timegaps.size() <= 2) {
    //    IsInitialized = false;
    //    return;
    //}

    //carBeforeCyclistMaxDuration = AddSettings.timegaps[0];
    //cyclistBeforeCarMaxDuration = AddSettings.timegaps[1];
    //
    //carBeforeCyclistUntil = QDateTime();
    //cyclistBeforeCarUntil = QDateTime();

    //carBeforeCyclistTimestamp = QDateTime();
    //cyclistBeforeCarTimestamp = QDateTime();

    //cyclistEnteringCollateEventsUntil = QDateTime();
    //carEnteringCollateEventsUntil = QDateTime();

    //// Initialize IsTriggered vector
    //IsTriggered.clear();
    //IsTriggered.resize(1);
    //triggeredCount.resize(1);
    //triggeredCount[0] = 0;
}

void InteractionDetector::updateDetectorSpecificLogic()
{
    //std::vector<DetectorOutput> dOutput = getDOutput();

    //if (!dOutput.empty()) {
    //    IsTriggered[0] = false;

    //    // Extract the relevant detector output structs:

    //    for (int i = 0; i < dOutput.size(); ++i)
    //    {
    //        // Extract the detector output of the right/left turning car detector
    //        if (interactionType == InteractionType::d312)
    //        {
    //            if (dOutput[i].DetectorID == rightTurningCarID
    //                && dOutput[i].DetectorType == RIGHTTURNINGCAR)
    //            {
    //                turningCarOutput = dOutput[i];
    //            }

    //        }
    //        else if (interactionType == InteractionType::d410
    //            && dOutput[i].DetectorType == LEFTTURNINGCAR)
    //        {
    //            if (dOutput[i].DetectorID == leftTurningCarID)
    //            {
    //                turningCarOutput = dOutput[i];
    //            }
    //        }

    //        if (dOutput[i].DetectorID == straightGoingCyclistID
    //            && dOutput[i].DetectorType == STRAIGHTGOINGCYCLIST)
    //        {
    //            straightGoingCyclistOutput = dOutput[i];
    //        }
    //    }

    //    // Identify if the timestamp of the extracted detector outputs are equivalent.
    //    // If this is not the case, we have encountered an error

    //    if (turningCarOutput.Timestamp != straightGoingCyclistOutput.Timestamp ||
    //        turningCarOutput.Timestamp != getCurrentTime())
    //    {
    //        return;
    //    }

    //    // If the leavingConflictZone flag has just been set, activate the detector
    //    if (turningCarOutput.LeavingConflictZone == turningCarOutput.Timestamp)
    //    {
    //        carBeforeCyclistUntil = turningCarOutput.Timestamp.addMSecs(carBeforeCyclistMaxDuration);
    //        carBeforeCyclistTimestamp = turningCarOutput.Timestamp;

    //        if (AddSettings.logSettings.SaveFrameOfEvent)
    //        {
    //            eventFrames.resize(frames.size());

    //            // Update the current eventFrames
    //            for (size_t i = 0; i < frames.size(); ++i)
    //            {
    //                eventFrames[i].Filename = frames[i].Filename;
    //                frames[i].Image.copyTo(eventFrames[i].Image);
    //                eventFrames[i].viewInfo = frames[i].viewInfo;
    //                eventFrames[i].Timestamp = frames[i].Timestamp;
    //            }
    //        }
    //    }

    //    if (straightGoingCyclistOutput.LeavingConflictZone == straightGoingCyclistOutput.Timestamp)
    //    {
    //        cyclistBeforeCarUntil = straightGoingCyclistOutput.Timestamp.addMSecs(cyclistBeforeCarMaxDuration);
    //        cyclistBeforeCarTimestamp = straightGoingCyclistOutput.Timestamp;

    //        if (AddSettings.logSettings.SaveFrameOfEvent)
    //        {
    //            eventFrames.resize(frames.size());

    //            // Update the current eventFrames
    //            for (size_t i = 0; i < frames.size(); ++i)
    //            {
    //                eventFrames[i].Filename = frames[i].Filename;
    //                frames[i].Image.copyTo(eventFrames[i].Image);
    //                eventFrames[i].viewInfo = frames[i].viewInfo;
    //                eventFrames[i].Timestamp = frames[i].Timestamp;
    //            }
    //        }
    //    }

    //    // Check if an appropriate road user has entered the conflict zone within the timestamp

    //    // Check if a car has entered the conflict zone after a cyclist
    //    if (turningCarOutput.IsTriggered[0] &&
    //        turningCarOutput.EnteringConflictZone > cyclistBeforeCarTimestamp  &&
    //        turningCarOutput.EnteringConflictZone < cyclistBeforeCarUntil &&
    //        turningCarOutput.EnteringConflictZone > carEnteringCollateEventsUntil)
    //    {
    //        // A turning car has entered the conflict within the maximum time after a
    //        // straight going cyclist has left it. Write the event information to the log
    //        IsTriggered[0] = true;
    //        lastChange = getCurrentTime();
    //        carEnteringCollateEventsUntil = getCurrentTime().addMSecs(carEnteringConsecutiveEventDelay);

    //        // Log the timestamp
    //        EveryEventLogInfo[0].logDataTime.clear();
    //        EveryEventLogInfo[0].logDataTime.push_back(cyclistBeforeCarTimestamp);

    //        // Log the TimegapInMsec
    //        EveryEventLogInfo[0].logDataInt.clear();
    //        EveryEventLogInfo[0].logDataInt.push_back(cyclistBeforeCarTimestamp.msecsTo(turningCarOutput.EnteringConflictZone));

    //        // Log the "WhoIsFirst" entry
    //        EveryEventLogInfo[0].logDataString.clear();
    //        EveryEventLogInfo[0].logDataString.push_back("Cyclist");

    //        // Log the InteractionType entry,
    //        if (interactionType == InteractionType::d312)
    //        {
    //            EveryEventLogInfo[0].logDataString.push_back("312");
    //        }
    //        else if (interactionType == InteractionType::d410)
    //        {
    //            EveryEventLogInfo[0].logDataString.push_back("410");
    //        }

    //        cyclistBeforeCarTimestamp = QDateTime();
    //        cyclistBeforeCarUntil = QDateTime();
    //    }

    //    UpdateLog();

    //    // Check if a cyclist has entered the conflict zone within the timestamp
    //    if (straightGoingCyclistOutput.IsTriggered[0] &&
    //        straightGoingCyclistOutput.EnteringConflictZone > carBeforeCyclistTimestamp  &&
    //        straightGoingCyclistOutput.EnteringConflictZone < carBeforeCyclistUntil &&
    //        straightGoingCyclistOutput.EnteringConflictZone > cyclistEnteringCollateEventsUntil)
    //    {
    //        // A straight going cyclist has entered the conflict within the maximum time after a
    //        // turning car has left it. Write the event information to the log
    //        IsTriggered[0] = true;
    //        lastChange = getCurrentTime();
    //        cyclistEnteringCollateEventsUntil = getCurrentTime().addMSecs(cyclistEnteringConsecutiveEventDelay);

    //        // Log the timestamp
    //        EveryEventLogInfo[0].logDataTime.clear();
    //        EveryEventLogInfo[0].logDataTime.push_back(carBeforeCyclistTimestamp);

    //        // Log the TimegapInMsec
    //        EveryEventLogInfo[0].logDataInt.clear();
    //        EveryEventLogInfo[0].logDataInt.push_back(carBeforeCyclistTimestamp.msecsTo(straightGoingCyclistOutput.EnteringConflictZone));

    //        // Log the "WhoIsFirst" entry
    //        EveryEventLogInfo[0].logDataString.clear();
    //        EveryEventLogInfo[0].logDataString.push_back("Car");

    //        // Log the InteractionType entry,
    //        if (interactionType == d312)
    //        {
    //            EveryEventLogInfo[0].logDataString.push_back("312");
    //        }
    //        else if (interactionType == d410)
    //        {
    //            EveryEventLogInfo[0].logDataString.push_back("410");
    //        }

    //        carBeforeCyclistTimestamp = QDateTime();
    //        carBeforeCyclistUntil = QDateTime();
    //    }


    //    UpdateLog();

    //    PrevIsTriggered = IsTriggered;
    //}
}


QList<QString> InteractionDetector::getDetectorSpecificDebugInfo()
{
    QList<QString> debugList;

    debugList.push_back("LastChange: " + this->lastChange.toString("hh:mm:ss:zzz"));

    return debugList;
}

QList<QString> InteractionDetector::getDetectorSpecificExtendedDebugInfo()
{
    QList<QString> extDebugList;

    return extDebugList;
}

std::vector<std::vector<MaskOverlayStruct> > InteractionDetector::GetMasks()
{
    vector<vector<MaskOverlayStruct>> maskOverlays;

    return maskOverlays;
}

qint64 InteractionDetector::GetConsecutiveEventDelay()
{

    return 0;
}

void InteractionDetector::SetUseScrollingChart(bool useScrollingChart)
{
    // The InteractionDetector does not use scrolling charts. Do nothing
}

void InteractionDetector::SetLastFrameReached()
{
    // Force the log to be written
    //UpdateLog(true);
}

