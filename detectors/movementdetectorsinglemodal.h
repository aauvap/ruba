#ifndef MOVEMENTDETECTORSINGLEMODAL_H
#define MOVEMENTDETECTORSINGLEMODAL_H


#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include <QDialog>
#include <QMessageBox>
#include <QFileInfoList>
#include <QFile>
#include <QFileDialog>
#include <QList>
#include <QDateTime>

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

#include <cmath>
#include <fstream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>

#include "ruba.h"
#include "abstractsinglemodaldetector.h"
#include "scrollingchart.h"
#include "framestruct.h"
#include "ChartStruct.h"
#include "detectoroutputstruct.h"
#include "utils.hpp"


class ruba::MovementDetectorSingleModal : public AbstractSingleModalDetector{

public:
    // Constructor
    MovementDetectorSingleModal(BaseDetectorParams baseParams, cv::Mat mask, std::vector<double> minVecLengthThreshold,
                               std::vector<cv::Point> flowDirectionRange,
                               std::vector<int> triggerThreshold, bool UseScrollingChart,
                               int upperThresholdBoundMultiplier);
    ~MovementDetectorSingleModal();

    void InitDetector();

    std::vector<QString*>  GetChartTitles();

    // Update the parameters individually
    void SetMinVecLengthThreshold(std::vector<double> minVecLengthThreshold);
    void SetFlowDirectionRange(std::vector<cv::Point> flowDirectionRange);
    void SetTriggerThreshold(std::vector<int> triggerThreshold);

private:
    void applyOpticalFlowAlgorithm();
    void generateOpticalFlowHistogram();
    void determinePresence();
    void extractContours();
    void extractROI();
    void drawOptFlowMap(const cv::Mat& flow, cv::Mat& cflowmap, int step, double scale, const cv::Scalar& color);

    QList<QString> getDetectorSpecificDebugInfo();
    void updateDetectorSpecifics();

    std::vector<double> minVecLengthThreshold;
    double leastMinVecLengthThreshold;

    std::vector<cv::Point> flowDirectionRange;
    std::vector<int> triggerThreshold;
    std::vector<int> upperThresholdBound;

    std::vector<double> chartHeightMultiplier;

    std::vector<int> nbrFlowVecInsideRange; // Number of flow vectors inside a predefined range
    std::vector<int> nbrFlowVec; // Number of flow vectors inside a fixed angular range from 0 to 359 degrees, inside 360 bins
    std::vector<double> flowVecHistInsideRange; // Histogram of flow vectors inside a predefined range
    std::vector<double> flowVecHist; // Histogram of flow vectors inside a fixed angular range from 0 to 359 range, inside 360 bins

    cv::Mat flow;
    cv::Mat roi, prevRoi;
    cv::Mat filledContourMask;

	/* Similar opticalflow mats in transparent openCL api*/
	cv::UMat uflow;
	cv::UMat uroi, uprevRoi;

    cv::Rect boundRect;

    bool IS_FIRST_RUN;
};


#endif // MOVEMENTDETECTORSINGLEMODAL_H
