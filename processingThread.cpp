#include "processingThread.h"

ProcessingThread::ProcessingThread(std::shared_ptr<ruba::VideoHandler> vHandler,
	std::shared_ptr<ruba::DetectorHandler> dHandler, QObject *parent)
	: QThread(parent)
{
	abort = false;
	this->VHandler = vHandler;
	this->DHandler = dHandler;
	this->parent = parent;
	currentFps = 0;
	elapsedMsCount = 0;
	elapsedCount = 0;

	connect(vHandler.get(), SIGNAL(sendMessage(QString, QString, MessageType)), this, SIGNAL(sendMessage(QString, QString, MessageType)));
	connect(vHandler.get(), SIGNAL(sendProgress(int, int, QString)), this, SIGNAL(sendProgress(int, int, QString)));
	connect(dHandler.get(), SIGNAL(sendMessage(QString, QString, MessageType)), this, SIGNAL(sendMessage(QString, QString, MessageType)));

	vSequenceWriter = nullptr;

	delay = 0;
	theoreticalDelay = 0;
	remainderDelay = 0;
	remainderInterval = 1;
	enableDiagnostics = false;
}


ProcessingThread::~ProcessingThread()
{
	mutex.lock();
	abort = true;
	mutex.unlock();

	wait();
}

void ProcessingThread::startProcessing()
{
	bool lock = mutex.tryLock(20);
	if (lock)
	{
		abort = false;
		mutex.unlock();
		elapsedTimer.start();

		start(NormalPriority);
	}
}

std::shared_ptr<ruba::DetectorHandler>  ProcessingThread::getDetectorHandler()
{
	bool lock = mutex.tryLock(500);
	if (lock)
	{
		abort = true;
		mutex.unlock();
		return this->DHandler;
	} else 
	{
		return nullptr;
	}
}

std::shared_ptr<ruba::VideoHandler>  ProcessingThread::getVideoHandler()
{
	bool lock = mutex.tryLock(500);
	if (lock)
	{
		abort = true;
		mutex.unlock();
		return VHandler;
	}
	else
	{
		return nullptr;
	}
}

bool ProcessingThread::getCurrentFrames(std::vector<ruba::FrameStruct>& frames, CaptureStatus& status)
{
	bool lock = displayFramesMutex.tryLock(20);
	status = CaptureStatus::OK;

	if (lock)
	{
		// Check if frames of VideoHandler are newer - but only if we are not processing anything
		if (!isRunning())
		{
			std::vector<ruba::FrameStruct> videoFrames = VHandler->getCurrentFrame();
			status = VHandler->getCaptureStatus();

			if (videoFrames.size() == displayFrames.size() && !displayFrames.empty())
			{
				if (videoFrames[0].Timestamp != displayFrames[0].Timestamp)
				{
					displayFrames = videoFrames;
				}
			}
		}

		if (displayFrames.empty())
		{
			std::vector<ruba::FrameStruct> videoFrames = VHandler->getCurrentFrame();
			displayFrames = videoFrames;
		}

		frames = displayFrames;
		
		if (detectorInfoMutex.tryLock(20))
		{
			// We need to guard this step with a mutex as we are accessing 
			// the log information which may change at any time during processing
			DHandler->refreshDetectorInformationModel();
			detectorInfoMutex.unlock();
		}

		VHandler->isExternalVideoListChanged(); // Invoke a GUI update if we have changed the shown video list

	} 

	return lock;
}

bool ProcessingThread::processNextFrame(std::vector<ruba::FrameStruct>& frames)
{
	bool lock = mutex.tryLock(20);
	if (lock)
	{
		std::vector<ruba::FrameStruct> updateStructs = VHandler->getNextFrame();
		updateFromFrame(updateStructs);
		mutex.unlock();

		lock = displayFramesMutex.tryLock(20);
		if (lock)
		{
			frames = displayFrames;
		}
		else {
			return false;
		}
	} 
	else
	{
		return false;
	}

	return true;
}

void ProcessingThread::unlockDisplayFrames()
{
	displayFramesMutex.unlock();
}

bool ProcessingThread::getDetectorCharts(std::vector<ruba::ChartStruct>& charts)
{
	bool lock = detectorChartsMutex.tryLock(20);
	if (lock)
	{
		charts = DHandler->getCharts();
		detectorChartsMutex.unlock();
	}

	return lock;
}

bool ProcessingThread::pause()
{
	bool lock = mutex.tryLock(500);
	if (lock)
	{
		abort = true;
		mutex.unlock();
	}

	return lock;
}

void ProcessingThread::jumpToFrame(QDateTime requestedTime)
{
	bool lock = mutex.tryLock(2000);
	if (lock)
	{
		abort = true;

		VHandler->jumpToFrame(requestedTime);
        std::vector<ruba::FrameStruct> frames = VHandler->getCurrentFrame();
        updateFromFrame(frames);
		mutex.unlock();
		sendProgress(0, 0, "Completed");
	}
}


void ProcessingThread::releaseVideo()
{
	QMutexLocker locker(&mutex);

	VHandler->Release();
}

RecordingState ProcessingThread::getRecordingState()
{
	RecordingState state;

	if (vSequenceWriter == nullptr)
	{
		state = NOTINITIALISED;
	}
	else if (vSequenceWriter->isRecording())
	{
		state = RECORDING;
	} 
	else if (!vSequenceWriter->isRecording())
	{
		state = PAUSED;
	}

	return state;
}

void ProcessingThread::startRecording(QString videoPath)
{
	QSettings settings;
	settings.beginGroup("recording");
	bool writeMultipleVideos = settings.value("writeMultipleVideo", 0).toBool();
	int maxConsecutiveTimeDifferenceInMillisec = settings.value(
		"maxConsecutiveTimeDifferenceInMillisec", 1000).toInt();
	settings.endGroup();

	QMutexLocker locker(&mutex);
	QList<ViewInfo> videoViews = VHandler->getCurrentViews();
	double frameRate = VHandler->getFrameRate();

    vSequenceWriter = std::make_shared<VideoSequenceWriter>(videoViews, videoPath,
		frameRate, writeMultipleVideos, maxConsecutiveTimeDifferenceInMillisec);
	vSequenceWriter->startRecording();

}

void ProcessingThread::resumeRecording()
{
	if (vSequenceWriter != nullptr)
	{
		vSequenceWriter->startRecording();
	}
}

void ProcessingThread::pauseRecording()
{
	if (vSequenceWriter != nullptr)
	{
		vSequenceWriter->pauseRecording();
	}
}

void ProcessingThread::stopRecording()
{
	if (vSequenceWriter != nullptr)
	{
		vSequenceWriter->stopRecording();
        vSequenceWriter.reset();
	}
}

void ProcessingThread::setTargetFps(int targetFps)
{
	this->targetFps = targetFps;

	bool lock = delayMutex.tryLock(500);

	if (lock)
	{
		historicalDelays.clear();
		historicalRemainderDelays.clear();

		if (targetFps > 0)
		{
			theoreticalDelay = 1000 / targetFps;
			delay = theoreticalDelay;

			remainderDelay = 1000 % targetFps;
			remainderInterval = std::max(1, std::min(targetFps / 10, 10));
			remainderDelay = remainderDelay / remainderInterval;
		}
		else {
			delay = 0;
			theoreticalDelay = 0;
			remainderDelay = 0;
			remainderInterval = 1;
		}

		delayMutex.unlock();
	}
}

void ProcessingThread::run()
{
	while (true)
	{
		std::vector<ruba::FrameStruct> updateStructs;
		if (!VHandler->getIsInitialised())
		{
			return;
		}

		mutex.lock();
		if (abort)
		{
			abort = false;
			mutex.unlock();
			return;
		}

		updateStructs = VHandler->getNextFrame();
		

		QSettings settings;
		settings.beginGroup("video");
		int processEveryXFrame = settings.value("processEveryXFrame", 0).toInt();

		// Decide if we should skip the current frame for processing. 
		// If processEveryXFrame is set to either 0 or 1, we process every frame
		// If processEveryXFrame is above 1, we skip every n'th frame
		if (processEveryXFrame > 1 && (framesSkipped < (processEveryXFrame - 1)))
		{
			framesSkipped++;
		}
		else {
			framesSkipped = 0;
			updateFromFrame(updateStructs);
		}
		mutex.unlock();

		qint64 elapsedTime;
		currentFps = computeElapsedInfo(elapsedTime);

		if (delay > 0 || remainderDelay > 0)
		{
			int combinedDelay = delay;

			if (elapsedCount % remainderInterval == 0)
			{
				combinedDelay += remainderDelay;
			}

			msleep(combinedDelay);
		}
	}
}

void ProcessingThread::updateFromFrame(std::vector<ruba::FrameStruct> &updateStructs)
{
	bool imageIsEmpty = false;
	QString message;

	if (updateStructs.empty())
	{
		return;
	}

	if (updateStructs[0].Image.empty())
	{
		imageIsEmpty = true;
	}

	if (imageIsEmpty)
	{
		// We have reached the end of the available files. Stop the timer, write all logs, and terminate
		ProcessTimer.stop();
		DHandler->lastFrameReached();

		abort = true;
		return;
	}

	DHandler->update(updateStructs);
	DHandler->paintOverlay(updateStructs);

	// Do not wait to lock the mutex. If inacessible, we will try to update for the next frame
	if (detectorInfoMutex.tryLock()) 
	{
		DHandler->refreshDetectorInformationList();
		detectorInfoMutex.unlock();
	}
	
	if (vSequenceWriter != nullptr && vSequenceWriter->isRecording())
	{
		QString recordingMessage;
		vSequenceWriter->update(updateStructs, message);
	}


	if(displayFramesMutex.tryLock())
	{
		displayFrames.clear();

		for (auto& updateStruct : updateStructs)
		{
			displayFrames.push_back(updateStruct);
			displayFrames.back().Image = updateStruct.Image.clone();
		}

		displayFramesMutex.unlock();
	}
}

double ProcessingThread::computeElapsedInfo(qint64& elapsedTime)
{
	elapsedTime = elapsedTimer.elapsed();

	if (elapsedTime > 0)
	{
		elapsedMsCount = elapsedTime;
		elapsedCount++;

		if (elapsedMsCount > 1000)
		{
			currentFps = 1000. / elapsedMsCount * elapsedCount;

			bool lock = delayMutex.tryLock(20);

			if (lock && targetFps > 0)
			{
				double divisionFps = std::max(1., 1000. / elapsedMsCount * elapsedCount);

				double processingDuration = std::max(0., (double)elapsedMsCount - (delay * elapsedCount + (elapsedCount / remainderInterval) * remainderDelay));
				int adjustedDelay = theoreticalDelay;

				if (processingDuration > 0)
				{
					adjustedDelay = std::max(0., theoreticalDelay - (processingDuration / std::max(1, elapsedCount)));
				}

				if (historicalDelays.size() >= 10)
				{
					historicalDelays.pop_front();
				}

				historicalDelays.push_back(adjustedDelay);
				delay = std::round(average(historicalDelays));

				if (delay < 5)
				{
					// Delays under 5 ms are sometimes rounded to 5 ms when requested (especially on Windows) - thus discard
					// and rely on the remainder delay
					delay = 0;
				}

				int adjustedRemainder = 1000 - (targetFps * processingDuration / elapsedCount + (long)delay*(long)targetFps);
				adjustedRemainder = std::max(0, adjustedRemainder / (targetFps / remainderInterval));

				if (historicalRemainderDelays.size() >= 10)
				{
					historicalRemainderDelays.pop_front();
				}

				historicalRemainderDelays.push_back(adjustedRemainder);
				remainderDelay = std::round(average(historicalRemainderDelays));


				if (enableDiagnostics)
				{
					qDebug() << "Delays: (M, e) " << adjustedDelay << "," << delay << " rem: (M,e) " << adjustedRemainder << "," << remainderDelay;

					if (!diagnostiscs.isOpen())
					{
						QString tempPath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
						emit sendMessage(tr("Log created"), tempPath, MessageType::Warning);

						if (!QDir(tempPath).exists()) {
							QDir().mkpath(tempPath);
						}

						QDir tempDir(tempPath);
						diagnostiscs.setFileName(tempDir.absoluteFilePath("processingSpeed.csv"));

						diagnostiscs.open(QIODevice::Append);
						diagnosticsStream = new QTextStream(&diagnostiscs);
						diagnosticsStream->operator<<(QString("ElapsedMsCount;elapsedCount;targetFps;currentFps;divisionFps;processingDuration;adjustedDelay;delay;adjustedRemainder;remainderDelay")) << endl;
					}

					diagnosticsStream->operator<<(QString::number(elapsedMsCount) + ";" + QString::number(elapsedCount) + ";" + QString::number(targetFps) + ";" + QString::number(currentFps) + ";" + QString::number(divisionFps) + ";" +
						QString::number(processingDuration) + ";" + QString::number(adjustedDelay) + ";" + QString::number(delay) + ";" + QString::number(adjustedRemainder) + ";" + QString::number(remainderDelay)) << endl;
				}

				delayMutex.unlock();
			}

			elapsedTimer.restart();
			elapsedCount = 0;
			elapsedMsCount = 0;
		}
	}

	return currentFps;
}

double ProcessingThread::average(const std::list<double>& list)
{
	return std::accumulate(list.begin(), list.end(), 0.0) / (double)list.size();
}
