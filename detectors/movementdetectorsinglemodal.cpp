#include "movementdetectorsinglemodal.h"
#include <QDebug>

using namespace cv;
using namespace std;
using namespace ruba;


MovementDetectorSingleModal::MovementDetectorSingleModal(BaseDetectorParams baseParams, Mat mask, std::vector<double> minVecLengthThreshold, std::vector<cv::Point> flowDirectionRange,
                                                std::vector<int> triggerThreshold, bool UseScrollingChart, int upperThresholdBoundMultiplier)
    : AbstractSingleModalDetector(configureBaseDetectorParams(baseParams, MOVEMENTDETECTOR))
{
    // Update parameters according to input
    this->mask = mask;

    // Make sure that the parameters are equally sized
    auto minParamSize = std::min(minVecLengthThreshold.size(), std::min(flowDirectionRange.size(), triggerThreshold.size()));

    this->minVecLengthThreshold = minVecLengthThreshold;
    this->flowDirectionRange = flowDirectionRange;
    this->triggerThreshold = triggerThreshold;

    for (auto i = minParamSize; i < this->minVecLengthThreshold.size(); ++i)
    {
        this->minVecLengthThreshold.pop_back(); //
    }

    for (auto i = minParamSize; i < this->flowDirectionRange.size(); ++i)
    {
        this->flowDirectionRange.pop_back(); //
    }

    for (auto i = minParamSize; i < this->triggerThreshold.size(); ++i)
    {
        this->triggerThreshold.pop_back(); //
    }
    
    leastMinVecLengthThreshold = 10000;

    for (size_t i = 0; i < minVecLengthThreshold.size(); ++i)
    {
        if (minVecLengthThreshold[i] < leastMinVecLengthThreshold)
        {
            leastMinVecLengthThreshold = minVecLengthThreshold[i];
        }
    }

    this->UseScrollingChart = UseScrollingChart;

    // Initialize the confidence, flags and last change vectors
    for (size_t i = 0; i < this->flowDirectionRange.size(); ++i)
    {
        this->confidence.push_back(0);
        this->IsTriggered.push_back(false);
        this->lastChange.push_back(QDateTime());
    }

	this->confidence.push_back(0);
	this->IsTriggered.push_back(false);

    // Set the upper threshold boundary, i.e. the threshold above anything will have
    // a confidence of 1
    this->upperThresholdBound.clear();

    for (size_t i = 0; i < this->triggerThreshold.size(); ++i)
    {
        upperThresholdBound.push_back(this->triggerThreshold[i]*upperThresholdBoundMultiplier);
    }

    // Update the default internal parameters
    IS_FIRST_RUN = true;

    this->Charts.clear();

    for (size_t i = 0; i < this->flowDirectionRange.size(); ++i)
    {
        chartHeightMultiplier.push_back(double(300)/upperThresholdBound[i]);
        ScrollingChart Chart = ScrollingChart(300,300, getCurrentView());
        Chart.SetThreshold(this->triggerThreshold[i]*chartHeightMultiplier[i]);
        Chart.SetVisualLineThresholds(this->triggerThreshold[i]*chartHeightMultiplier[i],0);
        Chart.SetTitle("MovementDetector");

        Charts.push_back(Chart);
    }

    
}

MovementDetectorSingleModal::~MovementDetectorSingleModal()
{
}

void MovementDetectorSingleModal::InitDetector()
{
    confidence.clear();
    IsTriggered.clear();
    lastChange.clear();

    for (size_t i = 0; i < this->flowDirectionRange.size(); ++i)
    {
        confidence.push_back(0);
        IsTriggered.push_back(false);
        lastChange.push_back(QDateTime());
    }

	IsTriggered.push_back(false);
	confidence.push_back(0);


    IS_FIRST_RUN = true;
}

void MovementDetectorSingleModal::SetMinVecLengthThreshold(vector<double> minVecLengthThreshold)
{
    this->minVecLengthThreshold.clear();

    for (size_t i = 0; i < minVecLengthThreshold.size(); ++i)
    {
        if (minVecLengthThreshold[i] >= 0) {
            this->minVecLengthThreshold.push_back(minVecLengthThreshold[i]);
        } else {
            this->minVecLengthThreshold.push_back(0);
            cout << "minVecLengthThreshold must be greater than or equal to 0";
        }
    }
}


void MovementDetectorSingleModal::SetFlowDirectionRange(vector<cv::Point> flowDirectionRange)
{
    this->flowDirectionRange.clear();

    if (flowDirectionRange.size() > 0)
    {
        if (flowDirectionRange[0].x != flowDirectionRange[0].y) {
            this->flowDirectionRange.push_back(flowDirectionRange[0]);
        } else {
            cout << "The flow direction range must span a range greater than 0";
        }
    }
}


void MovementDetectorSingleModal::SetTriggerThreshold(vector<int> triggerThreshold)
{
    if (triggerThreshold.size() > 0)
    {
        this->triggerThreshold = triggerThreshold;
    }

}

QList<QString> MovementDetectorSingleModal::getDetectorSpecificDebugInfo()
{
    return this->debugInfo;
}

void MovementDetectorSingleModal::updateDetectorSpecifics()
{
	QSettings settings;
	settings.beginGroup("video");


    if (IS_FIRST_RUN) {
        // Extract contours for the masks
        cvtColor(frame.Image, frame.Image, COLOR_BGR2GRAY);

        extractContours();
        IS_FIRST_RUN = false;

        initTriggeredHistory(int(minVecLengthThreshold.size()));

		extractROI();
		if (settings.value("useUmat", 1).toBool()) 
		{ 
			uroi.copyTo(uprevRoi);
		}
		else {
			roi.copyTo(prevRoi);
		}
		
    } else {
		extractROI();

        if (settings.value("useUmat", 1).toBool())
        {
            cvtColor(uroi, uroi, COLOR_BGR2GRAY);
        }
        else {
            cvtColor(roi, roi, COLOR_BGR2GRAY);
        }
	}


    
    applyOpticalFlowAlgorithm();

    determinePresence();

	if (settings.value("useUmat", 1).toBool())
	{
		swap(uprevRoi, uroi);
	}
	else {
		swap(prevRoi, roi);
	}

	settings.endGroup();

}

void MovementDetectorSingleModal::extractContours()
{
    /* Extract the contours from the ROI of the image. These are used for generating the
     * bounding box that is used in extractROI */

    vector<Vec4i> hierarchy;
    vector<vector<Point>> contours;
    Mat tempMask = Mat::zeros(frame.Image.rows, frame.Image.cols, CV_8UC1);

    // Find the contours of the region of interest
    Mat thresholdedMask;

    threshold(mask, thresholdedMask, 100, 255, THRESH_BINARY);
    findContours(thresholdedMask, contours, hierarchy, RETR_EXTERNAL,
                 CHAIN_APPROX_SIMPLE, Point(0, 0));

    drawContours(tempMask, contours, -1, Scalar::all(255), FILLED, 8, vector<Vec4i>(), 0, Point());
    tempMask.copyTo(filledContourMask);

    boundRect = boundingRect(Mat(contours[0]));
}

void MovementDetectorSingleModal::extractROI()
{
    /* Use the bounding box of the contours to create a limited view of the current frame,
     * and use the mask to filter out pixels outside the mask. */

    Mat tmpRoi;
    Mat tmpImg = frame.Image(boundRect);
    Mat tmpRoiMask = filledContourMask(boundRect);

    tmpImg.copyTo(tmpRoi, tmpRoiMask);

	QSettings settings;
	settings.beginGroup("video");

	if (settings.value("useUmat", 1).toBool())
	{
		tmpRoi.copyTo(uroi);
	}
	else {
		tmpRoi.copyTo(roi);

	}

	settings.endGroup();
}

void MovementDetectorSingleModal::applyOpticalFlowAlgorithm()
{
    // Apply the optical flow algorithm and call the histogram function

	QSettings settings;
	settings.beginGroup("video");

	if (settings.value("useUmat", 1).toBool() && uprevRoi.cols > 0)
	{
		uflow = UMat(uroi.size(), CV_32FC2);
		calcOpticalFlowFarneback(uprevRoi, uroi, uflow, 0.5, 3, 15, 5, 5, 1.1, OPTFLOW_FARNEBACK_GAUSSIAN);
		uflow.copyTo(flow);
	}
	else if (prevRoi.cols > 0) {
		calcOpticalFlowFarneback(prevRoi, roi, flow, 0.5, 3, 15, 5, 5, 1.1, OPTFLOW_FARNEBACK_GAUSSIAN);
	}

    // Uncomment these four lines to visualize the flow
    //Mat colorFlow; // For showing further debug information on screen
    //cvtColor(prevRoi, colorFlow, COLOR_GRAY2BGR);
    //drawOptFlowMap(flow, colorFlow, 16, 8, Scalar(0,255,0));
    //imshow("Optical flow ",colorFlow);
    //waitKey(1);
	
    settings.endGroup();

    generateOpticalFlowHistogram();

    
	
}

void MovementDetectorSingleModal::generateOpticalFlowHistogram()
{
    // Initialize vectors
    nbrFlowVec.clear();
    nbrFlowVec.resize(360);
    nbrFlowVecInsideRange.clear();
    nbrFlowVecInsideRange.resize(flowDirectionRange.size());

    flowVecHist.clear();
    flowVecHist.resize(360);
    flowVecHistInsideRange.clear();
    flowVecHistInsideRange.resize(flowDirectionRange.size());

    double flowVecSum = 0;
    vector<double> flowVecSumCustomMinLength;
    flowVecSumCustomMinLength.resize(flowDirectionRange.size());

    for (size_t i = 0; i < nbrFlowVecInsideRange.size(); ++i)
    {
        nbrFlowVecInsideRange[i] = 0;
        flowVecHistInsideRange[i] = 0;
        flowVecSumCustomMinLength[i] = 0;
    }

    for (size_t i = 0; i < nbrFlowVec.size(); ++i)
    {
        nbrFlowVec[i] = 0;
        flowVecHist[i] = 0;
    }
       
    for(int y = 0 ; y < flow.rows; ++y)
    {
        for(int x = 0 ; x < flow.cols; ++x)
        {
            double velx = flow.at<cv::Point2f>(y,x).x;
            double vely = -flow.at<cv::Point2f>(y,x).y;

            float length = (float)sqrt( pow(velx,2) + pow(vely,2) );
            double speed = sqrt(pow(velx, 2) + pow(vely, 2));

            // If the vector is big enough, we want to classify it
            if (length > leastMinVecLengthThreshold) {

                float angle = fastAtan2( vely, velx );

                if (int(angle) < 360)
                {
                    nbrFlowVec[int(angle)] = nbrFlowVec[int(angle)] + 1;
                    /*flowVecHist[int(angle)] = flowVecHist[int(angle)] + length;*/
                    flowVecSum = flowVecSum + length;
                }

                // Find the corresponding bin to put the vectors
                for (size_t i = 0; i < flowDirectionRange.size(); ++i)
                {
                    if (length > minVecLengthThreshold[i])
                    {
                        flowVecSumCustomMinLength[i] = flowVecSumCustomMinLength[i] + length;

                        if (flowDirectionRange[i].x < flowDirectionRange[i].y) {
                            // Is the interval "normal", ie. the interval is e.g. from 20 to 40 degrees
                            if ((angle >= flowDirectionRange[i].x) && (angle < flowDirectionRange[i].y)) {
                                nbrFlowVecInsideRange[i]++;
                                flowVecHistInsideRange[i] = flowVecHistInsideRange[i] + length;

                            }
                        } else {
                            // Otherwise, we are going from 360 to zero, for instance if we have a interval
                            // from 300 to 50 degrees
                            if ((angle >= flowDirectionRange[i].x) || (angle < flowDirectionRange[i].y)) {
                                nbrFlowVecInsideRange[i]++;
                                flowVecHistInsideRange[i] = flowVecHistInsideRange[i] + length;
                            }
                        }
                    }

                }

            }
        }
    }

    //// Visualise histogram
    //Mat visHis = Mat::zeros(Size(360, 400), CV_8UC3) + Scalar::all(255);
   
    //for (auto i = 0; i < flowVecHist.size(); ++i) 
    //{
    //    Scalar lineColor = Scalar(140,140,140);

    //    if (flowDirectionRange[0].x < flowDirectionRange[0].y) 
    //    {
    //        if ((i >= flowDirectionRange[0].x) && (i < flowDirectionRange[0].y))
    //        {
    //            lineColor = Scalar(0, 0, 255);
    //            line(visHis, Point(i, 0), Point(i, visHis.size().height - 1), Scalar(230, 230, 255));
    //        }
    //    }
    //    else 
    //    {
    //        if ((i >= flowDirectionRange[0].x) || (i < flowDirectionRange[0].y))
    //        {
    //            lineColor = Scalar(0, 0, 255);
    //            line(visHis, Point(i, 0), Point(i, visHis.size().height - 1), Scalar(200, 200, 255));
    //        }
    //    }

    //    

    //    line(visHis, Point(i, visHis.size().height - 1), Point(i, visHis.size().height - 1 - int(flowVecHist[i])), lineColor);

    //}

    // Compute the weighted histograms
    for (size_t i = 0; i < flowVecHistInsideRange.size(); ++i)
    {
        if (flowVecSumCustomMinLength[i] > 0)
        {
            flowVecHistInsideRange[i] = flowVecHistInsideRange[i] / flowVecSumCustomMinLength[i];
        }
    }

    if (UseScrollingChart) {
        // Update the scrolling chart to show debug information, one chart pr. histogram

        // For debugging
        for (size_t i = 0; i < Charts.size(); ++i)
        {
            Charts[i].AddDataPoint((int)(nbrFlowVecInsideRange[i]*chartHeightMultiplier[i]));
        }
    }


}

void MovementDetectorSingleModal::determinePresence()
{
    debugInfo.clear();
    // Update the confidence quotient
    if (nbrFlowVecInsideRange.size()) // Does the data exist?
    {
        for (size_t j = 0; j < nbrFlowVecInsideRange.size(); ++j)
        {
            if (nbrFlowVecInsideRange[j] > (upperThresholdBound[j]))
            {
                // If the number of flow vector is double of the trigger threshold, clamp
                // the confidence to 1
                confidence[j+1] = 1;

            }else if (nbrFlowVecInsideRange[j] <= triggerThreshold[j])
            {
                // If the number of flow vectors are below (or equal) the trigger threshold,
                // the confidence is between 0 and 0.5
                confidence[j+1] = double(nbrFlowVecInsideRange[j])/double(triggerThreshold[j])*0.5;
            } else if (nbrFlowVecInsideRange[j] > triggerThreshold[j])
            {
                // If the number of flow vectors are between the trigger threshold and the upper bound,
                // the confidence is between 0.5 and 1
                confidence[j+1] = double((nbrFlowVecInsideRange[j]- triggerThreshold[j]))/double((upperThresholdBound[j] - triggerThreshold[j]))*0.5+0.5;

            }else {
                // In the unlikely case that the number of flow vectors is negative, the confidence is zero, for sure
                confidence[j+1] = 0;

            }

            // If the confidence is below 0 or above 1, correct it
            if (confidence[j+0] < 0) {
                confidence[j+1] = 0;
            } else if(confidence[j+1] > 1) {
                confidence[j+1] = 1;
            }

            // Update the debug information
            stringstream debugText;
            debugText << getDetectorID() << " Confidence(" << j << "): " << confidence[j+1];
            debugInfo.push_back(QString::fromStdString(debugText.str()));

        }
    }

    // If the confidence is above the threshold, the area is active. Set the appropriate flag
    if (nbrFlowVecInsideRange.size())
    {
        for (size_t j = 1; j < confidence.size(); ++j)
        {
            if (confidence[j] >= 0.5)
            {
                if (!IsTriggered[j])
                {
                    // If the flag is not currently set, set it and register a change
                    lastChange[j-1] = frame.Timestamp;
                    IsTriggered[j] = true;
                }
            } else {
                if (IsTriggered[j])
                {
                    // If the flag is set, lower it and register a change
                    lastChange[j-1] = frame.Timestamp;
                    IsTriggered[j] = false;
                }
            }

            stringstream debugText;
            debugText << "M" << getDetectorID() << " LastChange(" << j << "): " << lastChange[j-1].toString("hh:mm:ss:zzz").toStdString();
            debugInfo.push_back(QString::fromStdString(debugText.str()));

            triggeredHistory[j-1].dequeue();
            triggeredHistory[j-1].enqueue(IsTriggered[j]);

        }
    }
	
	// Set the triggered flag if the MovementDetector has been
	// triggered in >= 4 of the last 10 frames and the Flow Detector is triggered
	if (GetIsTriggered()[1] &&
		GetTriggeredHistory()[0].count(true) >= 4)
	{
		IsTriggered[0] = true;
		confidence[0] = confidence[1];
	}
	else
	{
		IsTriggered[0] = false;
		
		if (confidence[1] > 0.5)
		{
			// The confidence suggests that we should be triggered, but the temporal consistency says no
			confidence[0] > 0.49;
		}
		else {
			confidence[0] = confidence[1];
		}
	}

	


}

void MovementDetectorSingleModal::drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step, double scale, const Scalar& color)
{
    Mat tmpFlow = flow.clone();

    if (scale != 1) {
        Mat rFlowMap;
        resize(cflowmap, rFlowMap, Size(cflowmap.size().width*scale, cflowmap.size().height*scale), 0, 0, cv::INTER_CUBIC);
        cflowmap = rFlowMap.clone();

        Mat rFlow;
        resize(flow, rFlow, Size(flow.size().width*scale, flow.size().height*scale), 0, 0, cv::INTER_CUBIC);
        tmpFlow = rFlow.clone();
    }
    
    
    Scalar colour = color;
    for(int y = 0; y < cflowmap.rows; y += step)
        for(int x = 0; x < cflowmap.cols; x += step)
        {
            const Point2f& fxy = tmpFlow.at<Point2f>(y, x)*scale;
            Point q = Point(x,y);
            Point p = Point(cvRound(x+fxy.x), cvRound(y+fxy.y));

            line(cflowmap, p, q, colour);
        }
}

std::vector<QString*> MovementDetectorSingleModal::GetChartTitles()
{
    if (UseScrollingChart)
    {
        vector<QString*> tempTitles;

        for (size_t i = 0; i < Charts.size(); ++i)
        {
            QString* tempWindowName = new QString("Range ");
            tempWindowName->append(QString("%1: %2 to %3 deg").
                                   arg(i).arg(flowDirectionRange[i].x).arg(flowDirectionRange[i].y));
            tempTitles.push_back(tempWindowName);
        }
        return tempTitles;

    }
    else
    {
        vector<QString*> tempTitles;
        tempTitles.push_back(NULL);

        return tempTitles;
    }
}

