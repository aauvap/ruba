#include "presencedetectorsinglemodal.h"

using namespace std;
using namespace cv;
using namespace ruba;


PresenceDetectorSingleModal::PresenceDetectorSingleModal(BaseDetectorParams baseParams, int minAreaCovered, cv::Mat mask, 
    cv::Mat backImage, bool usechart)
    : AbstractSingleModalDetector(configureBaseDetectorParams(baseParams, PRESENCEDETECTOR))
{


    IsTriggered.clear();
    IsTriggered.push_back(false);

    lastChange.clear();
    lastChange.resize(1);

    confidence.resize(1);
    confidence[0] = 0;

    this->minAreaCovered = minAreaCovered;

    if (this->minAreaCovered > 100) {
        this->minAreaCovered = 100;
    }
    else if (this->minAreaCovered < 0) {
        this->minAreaCovered = 0;
    }

    this->mask = mask;
    maskArea = countNonZero(mask);
    this->backImage = backImage;

    int chartWidth = 300;
    int chartHeight = 300;

    UseScrollingChart = true;
    ScrollingChart Chart = ScrollingChart(chartWidth, chartHeight, getCurrentView());
    chartHeightMultiplier = chartHeight / 100;
    
    Chart.SetThreshold(minAreaCovered * chartHeightMultiplier);
    Chart.SetTitle("PresenceDetector");
    Chart.SetVisualLineThresholds(minAreaCovered * chartHeightMultiplier, 0);
    Charts.push_back(Chart);

    initTriggeredHistory(1);

    InitDetector();

    IsInitialized = true;
}

void PresenceDetectorSingleModal::InitDetector()
{
    maskArea = countNonZero(mask);
    
    // Define the region of interest 
    auto minimalRoi = boundingRect(mask);

    // Because the change detection algorithm is very slow on high resolution 
    // images, we constrain the size of the image to be analyzed. 
    // We do so by calculating the ROI from the mask and then extending it
    // in every direction. 
    // As a starting point, we expand the ROI by adding half the width in the 
    // left and right side and by adding half the height in the top and bottom
    // 
    // We define a minimum width and height of 40 % of the image resolution
    // in order to stabilize the change detection algorithm
    
    ROI = minimalRoi;

    if (minimalRoi.width < frame.Image.cols * 0.2) {
        minimalRoi.width = frame.Image.cols * 0.2;
    }

    if (minimalRoi.height < frame.Image.rows * 0.2) {
        minimalRoi.height = frame.Image.rows * 0.2;
    }

    ROI.x = minimalRoi.x - minimalRoi.width / 2;
    if (ROI.x < 0) {
        ROI.x = 0;
    }

    ROI.width = minimalRoi.width * 2;

    if (ROI.x + ROI.width > frame.Image.cols) {
        ROI.width = frame.Image.cols - ROI.x - 1;
    }

    ROI.y = minimalRoi.y - minimalRoi.width / 2;
    if (ROI.y < 0) {
        ROI.y = 0;
    }

    ROI.height = minimalRoi.height * 2;
    if (ROI.y + ROI.height > frame.Image.rows) {
        ROI.height = frame.Image.rows - ROI.y - 1;
    }

    // Check for size and out of bounds
    if (ROI.width < (frame.Image.cols * 0.4 - 3)) {
        // If the width is less than required, move 
        // the starting point untill it fits
        // Subtract by three to take into account the rounding error
        ROI.width = frame.Image.cols * 0.4;
        ROI.x = frame.Image.cols - ROI.width - 1;
    }

    if (ROI.height < (frame.Image.rows * 0.4 - 3)) {
        ROI.height = frame.Image.rows * 0.4;
        ROI.y = frame.Image.rows - ROI.height - 1;
    }

    Mat roiBackImage = backImage(ROI).clone();    
    Mat roiMask = mask(ROI).clone();

    bgs.initialize(roiBackImage, roiMask);
}

PresenceDetectorSingleModal::~PresenceDetectorSingleModal() {
}



QList<QString> PresenceDetectorSingleModal::getDetectorSpecificDebugInfo(void)
{
    QList<QString> debugList;
    debugList.push_back(QString::number(getDetectorID()) + " Current ratio: " + QString::number(this->confidence[0]));
    debugList.push_back(QString::number(getDetectorID()) + " LastChange: " + this->lastChange[0].toString());
    return debugList;
}


std::vector<QString*> PresenceDetectorSingleModal::GetChartTitles()
{
    if (UseScrollingChart)
    {
        vector<QString*> tempTitles;

        QString* tempWindowName = new QString("Chart 1");
        tempTitles.push_back(tempWindowName);

        return tempTitles;

    }
    else
    {
        vector<QString*> tempTitles;
        tempTitles.push_back(NULL);

        return tempTitles;
    }
}

void PresenceDetectorSingleModal::updateDetectorSpecifics()
{
    Mat fgMask;
    Mat roiImage = frame.Image(ROI).clone();

    bgs.apply(roiImage, fgMask);

	QSettings settings;
	settings.beginGroup("debug");

	if (settings.value("showForegroundMaskDebugWindow", 0).toBool())
	{
		cv::imshow("Foreground mask", fgMask);
	}

    // Count number of pixels set as foreground
    int fgArea = countNonZero(fgMask);

    // Calculate foreground ratio to total mask
    int fgRatio = (fgArea * 100) / maskArea;

    // If this is above the minCoveredArea threshold, the detector is triggered
    confidence[0] = (double)fgRatio / double(minAreaCovered) * 0.5;

    if (fgRatio >= minAreaCovered) {
        if (IsTriggered[0] == false) {
            lastChange[0] = frame.Timestamp;
        }

        IsTriggered[0] = true;

    }
    else {
        if (IsTriggered[0] == true) {
            lastChange[0] = frame.Timestamp;
        }

        IsTriggered[0] = false;
    }

    if (UseScrollingChart) {
        Charts[0].AddDataPoint(fgRatio * chartHeightMultiplier);
    }
    
}