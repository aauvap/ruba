#include "rubabaseobject.h"

using namespace cv;
using namespace std;
using namespace ruba;

ruba::RubaBaseObject::RubaBaseObject(QWidget* parent, QList<ViewInfo> views)
{
    this->parent = parent;
    this->views = views;
}

ruba::RubaBaseObject::RubaBaseObject(QWidget * parent, cv::FileStorage & fs, const std::string &entryPrefix)
{
    this->parent = parent;
    this->views = getViewsFromFileStorage(fs, entryPrefix);
}

QList<ViewInfo> ruba::RubaBaseObject::getViewsFromFileStorage(cv::FileStorage & fs, const std::string& entryPrefix)
{
    QList<ViewInfo> loadedViews;

    if (fs.isOpened())
    {
        string viewName;
        int viewId;
        int modality;
        int i = 0;

        // Try to get the first view
        fs[entryPrefix + "-View-" + std::to_string(i) + "-ViewName"] >> viewName;
        fs[entryPrefix + "-View-" + std::to_string(i) + "-ViewId"] >> viewId;
        fs[entryPrefix + "-View-" + std::to_string(i) + "-Modality"] >> modality;

        while (!viewName.empty() && viewId >= 0 && modality >= 0)
        {
            ViewInfo view;
            view.viewId = viewId;
            view.modality = (ImageModalities)modality;
            loadedViews.append(view);

            i++;

            // Try to fetch the following views
            fs[entryPrefix + "-View-" + std::to_string(i) + "-ViewName"] >> viewName;
            fs[entryPrefix + "-View-" + std::to_string(i) + "-ViewId"] >> viewId;
            fs[entryPrefix + "-View-" + std::to_string(i) + "-Modality"] >> modality;
        }
    }

    return loadedViews;
}

int ruba::RubaBaseObject::saveViewsToFile(cv::FileStorage & fs, const std::string& entryPrefix)
{
    return saveViewsToFile(fs, entryPrefix, views);
}

int ruba::RubaBaseObject::saveViewsToFile(cv::FileStorage & fs, const std::string& entryPrefix, 
    const QList<ViewInfo>& views)
{
    if (fs.isOpened())
    {
        for (auto i = 0; i < views.size(); ++i)
        {
            string entryName = entryPrefix + "-View-" + std::to_string(i) + "-ViewName";
            fs << entryName << views[i].viewName().toStdString();
            entryName = entryPrefix + "-View-" + std::to_string(i) + "-ViewId";
            fs << entryName << (int)views[i].viewId;
            entryName = entryPrefix + "-View-" + std::to_string(i) + "-Modality";
            fs << entryName << (int)views[i].modality;
        }

        return 0;
    }
    else {
        return 1;
    }
}

int ruba::RubaBaseObject::addView(ViewInfo newView)
{
    int foundAtIndex = findViewInList(newView);

    // If found, the view has already been added. Return
    if (foundAtIndex >= 0) {
        return 1;
    }

    // Otherwise, add the view
    views.append(newView);
    
    return viewsHaveChanged(QList<ViewInfo>({ newView }), QList<ViewInfo>());
}

int ruba::RubaBaseObject::removeView(ViewInfo view)
{
    int foundAtIndex = findViewInList(view);

    // If found, remove the view
    if (foundAtIndex >= 0) {
        views.removeAt(foundAtIndex);

        return viewsHaveChanged(QList<ViewInfo>(), QList<ViewInfo>({ view }));
    }
    else {
        return 1;
    }

    return 0;
}

int ruba::RubaBaseObject::findViewInList(const ViewInfo & view)
{
    return findViewInList(view, views);
}

int ruba::RubaBaseObject::findViewInList(const ViewInfo & view, const QList<ViewInfo>& viewList)
{
    int foundAtIndex = -1;

    // Find the index of the view in the list of views
    for (auto i = 0; i < viewList.size(); ++i)
    {
        if (viewList[i].compare(view) == 0)
        {
            foundAtIndex = i;
            break;
        }
    }

    return foundAtIndex;
}


int ruba::RubaBaseObject::setViews(QList<ViewInfo> newViews)
{
    this->views = views;

    QList<ViewInfo> addedViews;
    QList<ViewInfo> removedViews;

    // Find out which views should be added and which should be removed 
    // from the existing list

    for (auto i = 0; i < newViews.size(); ++i)
    {
        bool viewFound = false;

        for (auto j = 0; j < views.size(); ++j)
        {
            if (newViews[i].compare(views[j]) == 0) 
            {
                viewFound = true;
                break;
            }
        }

        // If the view was not found, it will be added.
        if (!viewFound) 
        {
            addedViews.append(newViews[i]);
        }
    }

    for (auto i = 0; i < views.size(); ++i)
    {
        bool viewFound = false;

        for (auto j = 0; j < newViews.size(); ++j)
        {
            if (views[i].compare(newViews[j]) == 0)
            {
                viewFound = true;
                break;
            }
        }

        // If the view was not found, we should report it as removed
        if (!viewFound) 
        {
            removedViews.append(views[i]);
        }
    }

    views = newViews;
    return viewsHaveChanged(addedViews, removedViews);
}
