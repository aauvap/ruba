#ifndef VIEWINFO_H
#define VIEWINFO_H

#include <opencv2/opencv.hpp>

#include <QDateTime>

#include "ruba.h"
#include "utils.hpp"

class ViewInfo {

public:

    QString viewName() const { return QString("%1 (%2)").arg(viewId).arg(Utils::GetModalityName(modality)); };
    uint viewId;
    ImageModalities modality;
    // Additional information related to the particular view, such as the calibration, 
    // internal camera parameters, etc.


    int compare(const ViewInfo &anotherView) const;
    friend const bool operator < (ViewInfo const& lhs, ViewInfo const& rhs);
    
};

Q_DECLARE_METATYPE(ImageModalities)
Q_DECLARE_METATYPE(ViewInfo)



#endif // VIEWINFO_H