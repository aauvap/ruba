#ifndef SYNCVIDEOCAPTURE_H
#define SYNCVIDEOCAPTURE_H

#include <opencv2/opencv.hpp>
#include <memory>
#include <vector>
#include <queue>

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QSettings>
#include <QSharedMemory>
#include <QStandardPaths>
#include <QThread>


#include "framestruct.h"
#include "ruba.h"
#include "rubabaseobject.h"
#include "utils.hpp"
#include "viewinfo.h"

#include "dialogs/setdatetimedialog.h"
#include "dialogs/videoinformationdialog.h"


enum CaptureStatus {
    OK,
    NOMOREFRAMES,
    VIDEONOTFOUND,
    SYNCHRONISATIONERROR,
    VIDEOISRELEASED,
} ;

struct InitStreamProperties {
    QString streamPath;
    ViewInfo viewInfo;
};

struct InitFileProperties {
    QString videofile;
    ViewInfo viewInfo;

    QString prevFilename;
    QDateTime prevDateTime;
};



struct InitFilePropertiesTimestamps {
    QString videofile;
    ViewInfo viewInfo;
    QString timestampsFile;
};

struct VideoResolutionProperties {
    QString videoPath;
    cv::Size resolution;
};


struct FileProperties {
    QString videofile;
    ViewInfo viewInfo;
    cv::Size videoResolution;
    QDateTime startTime;
    QDateTime endTime;

    double fps;
    double frameCount;

    cv::VideoCapture cap;
};

struct FilePropertiesTimestamps {
    QString videofile;
    ViewInfo viewInfo;
    cv::Size videoResolution;
    QDateTime startTime;
    QDateTime endTime;

    double fps;
    double frameCount;
    QString timestampsFile;

    cv::VideoCapture cap;
};

struct MultiViewFileProperties {
    QList<FileProperties> viewFiles;

    QDateTime maxStartTime;
    QDateTime minEndTime;
    qint64 duration;

    bool autoDetectFps;
    bool videosAreSynchronised;
};

struct MultiViewFilePropertiesTimestamps {
    QList<FilePropertiesTimestamps> viewFiles;

    QDateTime maxStartTime;
    QDateTime minEndTime;
    qint64 duration;
    qint64 frameNumberAtMinEndTime;

    bool videosAreSynchronised;

};

struct InfoLabel {
    QString info;
    QLabel* label;
};

struct SyncLog {
    QDateTime timestamp;
    qint64 frameNbr;
};


class ruba::AbstractSynchronisedVideoCapture : public RubaBaseObject
{
	Q_OBJECT

public:
    AbstractSynchronisedVideoCapture(QWidget* parent, QList<ViewInfo> views);
    AbstractSynchronisedVideoCapture(QWidget* parent, cv::FileStorage& fs,
        std::string entryPrefix);

    int saveToFile(cv::FileStorage& fs, const std::string& entryPrefix);

    virtual std::vector<FrameStruct> getCurrentFrames(CaptureStatus& status) = 0;
    virtual std::vector<FrameStruct> getNextFrames(CaptureStatus& status) = 0;
    virtual CaptureStatus getCaptureStatus() const { return prevStatus; }

    virtual int jumpToFrame(QDateTime requestedTime) = 0;
    virtual int jumpToFrame(int requestedFrameNumber) = 0;

    virtual qint64 getFrameNumber() const = 0;
	virtual QDateTime getCurrentTime() const;

    virtual int releaseVideo() = 0;

    int showVideoProperties();
    virtual QList<VideoResolutionProperties> getVideoResolution() const = 0;
    virtual double getFrameRate() const = 0;
    virtual QDateTime getStartTime() const = 0;
    virtual QDateTime getEndTime() const = 0;
    virtual qint64 getFrameNumberAtEndTime() const = 0;

    VideoSyncType getSyncType() const { return syncType; }

signals:
	void sendMessage(const QString& title, const QString& message, MessageType messageType);
	void sendProgress(int value, int maxValue, const QString& message);

protected:
    virtual QList<QWidget*> getSpecificVideoProperties() const = 0;
    virtual QList<InfoLabel> getAdditionalGeneralProperties() const = 0;
    virtual int viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews) = 0;
    virtual int saveSpecificParametersToFile(cv::FileStorage& fs, const std::string& entryPrefix) = 0;

    VideoSyncType syncType;
    CaptureStatus prevStatus;

    std::vector<ruba::FrameStruct> frames;
};

class ruba::SynchronisedVideoCaptureFromFile : public AbstractSynchronisedVideoCapture
{
public:
    SynchronisedVideoCaptureFromFile(QWidget* parent, QList<ViewInfo> views,
        QList<InitFileProperties>& properties);
    SynchronisedVideoCaptureFromFile(QWidget* parent, cv::FileStorage& fs,
        std::string entryPrefix);

    virtual std::vector<FrameStruct> getCurrentFrames(CaptureStatus& status);
    virtual std::vector<FrameStruct> getNextFrames(CaptureStatus& status);

    virtual int jumpToFrame(QDateTime requestedTime);
    virtual int jumpToFrame(int requestedFrameNumber);

    virtual qint64 getFrameNumber() const { return frameNbr; }

    virtual int releaseVideo();

    virtual QList<VideoResolutionProperties> getVideoResolution() const;
    virtual double getFrameRate() const;
    virtual QDateTime getStartTime() const { return fileProperties.maxStartTime; }
    virtual QDateTime getEndTime() const { return fileProperties.minEndTime; }
    virtual qint64 getFrameNumberAtEndTime() const;


protected:
    virtual QList<QWidget*> getSpecificVideoProperties() const;
    virtual QList<InfoLabel> getAdditionalGeneralProperties() const;
    virtual int viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews);
    virtual int saveSpecificParametersToFile(cv::FileStorage& fs, const std::string& entryPrefix);

private:
    int initialise(QList<InitFileProperties>& newProperties);

    int isTimestampWithinFile(QDateTime timestamp, int &requestedFrameNbr);

    int getFrameRateFromFiles(bool noWarning = false);

    int findStartTimeFromFile(const QString& filename, QDateTime& startTime,
        InitFileProperties initProperties);

    int loadFromFile(cv::FileStorage& fs, const std::string& entryPrefix);

    QDateTime getCurrentFrameTime();


    MultiViewFileProperties fileProperties;

    int dateTimeFormat;
    int userDefinedYear;

    qint64 frameNbr;
};

class ruba::SynchronisedVideoCaptureFromFileTimestamps : public AbstractSynchronisedVideoCapture
{
public:
    SynchronisedVideoCaptureFromFileTimestamps(QWidget* parent, QList<ViewInfo> views,
        QList<InitFilePropertiesTimestamps> properties);
    SynchronisedVideoCaptureFromFileTimestamps(QWidget* parent, cv::FileStorage& fs,
        std::string entryPrefix);

    ~SynchronisedVideoCaptureFromFileTimestamps();
   

    virtual std::vector<FrameStruct> getCurrentFrames(CaptureStatus& status);
    virtual std::vector<FrameStruct> getNextFrames(CaptureStatus& status);

    virtual int jumpToFrame(QDateTime requestedTime);
    virtual int jumpToFrame(int requestedFrameNumber);

    virtual qint64 getFrameNumber() const;

    virtual int releaseVideo();

    virtual QList<VideoResolutionProperties> getVideoResolution() const;
    virtual double getFrameRate() const;
    virtual QDateTime getStartTime() const { return fileProperties.maxStartTime; }
    virtual QDateTime getEndTime()  const { return fileProperties.minEndTime; }
    virtual qint64 getFrameNumberAtEndTime() const { return fileProperties.frameNumberAtMinEndTime; }

protected:
    virtual QList<QWidget*> getSpecificVideoProperties() const;
    virtual QList<InfoLabel> getAdditionalGeneralProperties() const;
    virtual int viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews);
    virtual int saveSpecificParametersToFile(cv::FileStorage& fs, const std::string& entryPrefix);


private:
    int initialise(QList<InitFilePropertiesTimestamps> newProperties);
    int generateSynchronisedLookupTable();

    QList<SyncLog> getFrameInfoFromString(QString csvLine);
    int loadFromFile(cv::FileStorage& fs, const std::string& entryPrefix);

    CaptureStatus retrieveFrameAt(QDateTime requestedTime, int requestedFrameNumber);

    CaptureStatus getTimestampFromFileSyncFile(QDateTime& requestedTime, int requestedFrameNumber,
        QList<SyncLog>& desiredFrameInfo);

    MultiViewFilePropertiesTimestamps fileProperties;

    QList<SyncLog> frameInfo;

    QFile syncLookupFile;
    QTextStream syncLookupStream;

    bool videoIsReleased;
};

class ruba::SynchronisedVideoCaptureFromStream : public AbstractSynchronisedVideoCapture
{
public:
	SynchronisedVideoCaptureFromStream(QWidget* parent, QList<ViewInfo> views,
		QList<InitFileProperties> properties);
	SynchronisedVideoCaptureFromStream(QWidget* parent, cv::FileStorage& fs,
		std::string entryPrefix);

	~SynchronisedVideoCaptureFromStream();


	virtual std::vector<FrameStruct> getCurrentFrames(CaptureStatus& status);
	virtual std::vector<FrameStruct> getNextFrames(CaptureStatus& status);

	virtual int jumpToFrame(QDateTime requestedTime) { return -1; };
	virtual int jumpToFrame(int requestedFrameNumber) { return -1; };

	virtual qint64 getFrameNumber() const;

	virtual int releaseVideo();

	virtual QList<VideoResolutionProperties> getVideoResolution() const;
	virtual double getFrameRate() const;
	virtual QDateTime getStartTime() const { return fileProperties.maxStartTime; }
	virtual QDateTime getEndTime()  const { return fileProperties.minEndTime; }
	virtual qint64 getFrameNumberAtEndTime() const;

protected:
	virtual QList<QWidget*> getSpecificVideoProperties() const;
	virtual QList<InfoLabel> getAdditionalGeneralProperties() const;
	virtual int viewsHaveChanged(QList<ViewInfo> addedViews, QList<ViewInfo> removedViews);
	virtual int saveSpecificParametersToFile(cv::FileStorage& fs, const std::string& entryPrefix);


private:
	int initialise(QList<InitFileProperties> newProperties);
	int loadFromFile(cv::FileStorage& fs, const std::string& entryPrefix);

	MultiViewFileProperties fileProperties;

	std::vector<SyncLog> frameInfo;

	bool videoIsReleased;
};


#endif // SYNCVIDEOCAPTURE_H