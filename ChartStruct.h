#ifndef CHARTSTRUCT_H
#define CHARTSTRUCT_H

#include <QString>
#include <opencv2/opencv.hpp>
#include "ruba.h"
#include "viewinfo.h"

struct ruba::ChartStruct{
    cv::Mat Image;
    QString Title;
    ViewInfo View;
};

#endif // CHARTSTRUCT_H
