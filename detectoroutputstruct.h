#ifndef DETECTOROUTPUTSTRUCT_H
#define DETECTOROUTPUTSTRUCT_H

#include <QDateTime>
#include "ruba.h"

struct ruba::DetectorOutput{
    std::vector<double> Confidence;
    int DetectorID;
    int DetectorType;
    tlTypeClasses tlDetectionClass;
    std::vector<int> valAtTlIndices;
    std::vector<bool> IsTriggered;
    std::vector<QDateTime> LastChange;
    QDateTime EnteringConflictZone;
    QDateTime LeavingConflictZone;
    QDateTime Timestamp;
};


#endif // DETECTOROUTPUTSTRUCT_H
