#--------------------------------------------------#
#                                                  #
# Project created by QtCreator 2013-09-24T15:02:32 #
#                                                  #
#--------------------------------------------------#

QT       += core gui \
            concurrent \
            widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RUBA
TEMPLATE = app
#For deployment of application
CONFIG   += app_bundle

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.14
QMAKE_CXXFLAGS += -std=c++14
#CONFIG += C++1z

#Has no effect
#CONFIG += warn_off
#QMAKE_CFLAGS_WARN_ON -= -W4
#QMAKE_CFLAGS_WARN_ON += -W3

SOURCES += main.cpp\
        mainwindow.cpp \
    detectors/presencedetector.cpp \
    detectors/presencedetectorsinglemodal.cpp \
    scrollingchart.cpp \
    detectors/movementdetector.cpp \
    detectors/movementdetectorsinglemodal.cpp \
    videohandler.cpp \
    opencvviewer.cpp \
    cvutilities.cpp \
    detectors/rightturningcar.cpp \
    dialogs/detectorselectiondialog.cpp \
    logger.cpp \
    viewinfo.cpp \
    aspectratiopixmaplabel.cpp \
    synchronisedvideocapture.cpp \
    dialogs/jumptoframedialog.cpp \
    detectors/straightgoingcyclist.cpp \
    dialogs/interactionselectiondialog.cpp \
    detectors/interactiondetector.cpp \
    detectors/stationarydetector.cpp \
    detectors/abstractcompounddetector.cpp \
    detectors/abstractsinglemodaldetector.cpp \
    detectors/abstractmultimodaldetector.cpp \
    detectors/abstracttrafficdetector.cpp \
    dialogs/dualimageviewer.cpp \
    detectors/leftturningcar.cpp \
    detectors/singlemodule.cpp \
    dialogs/detectortypeselectiondialog.cpp \
    detectorhandler.cpp \
    detectors/trafficlightdetector.cpp \
    detectors/trafficlightdetectorsinglemodal.cpp \
    detectors/doublemodule.cpp \
    utils.cpp \
    rubabaseobject.cpp \
    processingThread.cpp \
    dialogs/logsettingsdialog.cpp \
    dialogs/setdatetimedialog.cpp \
    dialogs/videoinformationdialog.cpp \
    dialogs/settingsdialog.cpp \
    dialogs/settingspages.cpp \
    dialogs/abstractsettingspage.cpp \
    dialogs/groundtruthannotatordialog.cpp \
    dialogs/logfilereviewerdialog.cpp \
    dialogs/multithreadedprocessingdialog.cpp \
    detectors/groundtruthannotator.cpp \
    detectors/exclusivemodule.cpp \
    detectors/logfilereviewer.cpp \
    detectors/subsense/BackgroundSubtractorLBSP.cpp \
    detectors/subsense/BackgroundSubtractorSuBSENSE.cpp \
    detectors/subsense/LBSP.cpp

HEADERS  += mainwindow.h \
    detectors/presencedetector.h \
    detectors/presencedetectorsinglemodal.h \
    scrollingchart.h \
    detectors/movementdetector.h \
    detectors/movementdetectorsinglemodal.h \
    videohandler.h \
    framestruct.h \
    detectoroutputstruct.h \
    opencvviewer.h \
    cvutilities.h \
    ChartStruct.h \
    viewinfo.h \
    aspectratiopixmaplabel.h \
    synchronisedvideocapture.h \
    detectors/rightturningcar.h \
    trafficdetectoroutputstruct.h \
    dialogs/detectorselectiondialog.h \
    maskhandleroutputstruct.h \
    ruba.h \
    rubabaseobject.h \
    maskoverlaystruct.h \
    detectorinfo.h \
    loginfostruct.h \
    logger.h \
    logsettingsstruct.h \
    processingThread.h \
    dialogs/jumptoframedialog.h \
    detectors/straightgoingcyclist.h \
    dialogs/interactionselectiondialog.h \
    detectors/interactiondetector.h \
    detectors/stationarydetector.h \
    detectors/abstractcompounddetector.h \
    detectors/abstractsinglemodaldetector.h \
    detectors/abstractmultimodaldetector.h \
    detectors/abstracttrafficdetector.h \
    detectors/exclusivemodule.h \
    addsettingsstruct.h \
    dialogs/dualimageviewer.h \
    detectors/leftturningcar.h \
    debugprefstruct.h \
    detectors/singlemodule.h \
    dialogs/detectortypeselectiondialog.h \
    detectorhandler.h \
    detectors/trafficlightdetector.h \
    detectors/trafficlightdetectorsinglemodal.h \
    detectors/doublemodule.h \
    utils.hpp \
    dialogs/logsettingsdialog.h \
    dialogs/setdatetimedialog.h \
    dialogs/videoinformationdialog.h \
    dialogs/settingsdialog.h \
    dialogs/settingspages.h \
    dialogs/abstractsettingspage.h \
    dialogs/groundtruthannotatordialog.h \
    dialogs/logfilereviewerdialog.h \
    dialogs/multithreadedprocessingdialog.h \
    detectors/groundtruthannotator.h \
    detectors/logfilereviewer.h \
    detectors/subsense/BackgroundSubtractorLBSP.h \
    detectors/subsense/BackgroundSubtractorSuBSENSE.h \
    detectors/subsense/LBSP.h \
    detectors/subsense/DistanceUtils.h \
    detectors/subsense/RandUtils.h


FORMS    += mainwindow.ui \
    dialogs/detectorselectiondialog.ui \
    dialogs/interactionselectiondialog.ui \
    dialogs/dualimageviewer.ui \
    dialogs/detectortypeselectiondialog.ui \
    dialogs/setdatetimedialog.ui \
    dialogs/groundtruthannotatordialog.ui \
    dialogs/logfilereviewerdialog.ui

win32{
    INCLUDEPATH += C:/Tools/opencv3/build/include
    #INCLUDEPATH += $$(OPENCV_INCLUDE)

    DEPENDPATH += C:/Tools/opencv3/build/include
    #DEPENDPATH += $$(OPENCV_INCLUDE)

    # Debug version
    # Remember to add "make install" as a build step under projects, to copy the dll files
    win32:CONFIG(debug, debug|release) {
        LIBS += -L$$(OPENCV_LIB)/Debug -lopencv_core300d
        LIBS += -L$$(OPENCV_LIB)/Debug -lopencv_imgproc300d
        LIBS += -L$$(OPENCV_LIB)/Debug -lopencv_highgui300d
        LIBS += -L$$(OPENCV_LIB)/Debug -lopencv_video300d
        LIBS += -L$$(OPENCV_BIN)/Debug -lopencv_videoio300d
        LIBS += -L$$(OPENCV_BIN)/Debug -lopencv_imgcodecs300d
        LIBS += -lQt5Concurrent

        dllfiles.path    = $$OUT_PWD/debug
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_core300d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_imgproc300d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_highgui300d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_ffmpeg300.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_video300d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_videoio300d.dll
        dllfiles.files  += $$(OPENCV_BIN)/Debug/opencv_imgcodecs300d.dll
        INSTALLS        += dllfiles
    }

    # Release version
    # Remember to add "make install" as a build step under projects, to copy the dll files
    win32:CONFIG(release, debug|release) {
        LIBS += -L$$(OPENCV_LIB)/Release -lopencv_core300
        LIBS += -L$$(OPENCV_LIB)/Release -lopencv_imgproc300
        LIBS += -L$$(OPENCV_LIB)/Release -lopencv_highgui300
        LIBS += -L$$(OPENCV_LIB)/Release -lopencv_video300
        LIBS += -L$$(OPENCV_BIN)/Release -lopencv_videoio300
        LIBS += -L$$(OPENCV_BIN)/Release -lopencv_imgcodecs300
        LIBS += -lQt5Concurrent

        dllfiles.path    = $$OUT_PWD/release
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_core300.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_imgproc300.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_highgui300.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_ffmpeg300.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_video300.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_videoio300.dll
        dllfiles.files  += $$(OPENCV_BIN)/Release/opencv_imgcodecs300.dll
        INSTALLS        += dllfiles
    }
}

!win32{
    LIBS += -L/usr/local/lib \
        -lopencv_core \
        -lopencv_imgproc \
        -lopencv_highgui \
        -lopencv_video \
        -lopencv_videoio \
        -lopencv_imgcodecs \
        -lopencv_features2d

    INCLUDEPATH += /usr/local/include
}

OTHER_FILES +=

RESOURCES += \
    ressources.qrc
